var Crawler = require("crawler");
var url = require('url');
var fs = require('fs');
var express = require("express");
var app = express();
var cron = require('cron');

var c = new Crawler({
    maxConnections: 10,
    callback: function (error, res, done) {
        if (error) {
            console.log(error);
        } else {
            var $ = res.$;

            String.prototype.capitalizeFirstLetter = function () {
                return this.charAt(0).toUpperCase() + this.slice(1);
            }

            var newestTopicsTitle = [];
            var newestTopicsCat = [];
            var newestTopicsTime = [];
            var newestTopicsURL = [];
            var APIArr = [];
            var limit = 2;

            $(".ipsWidget_NewestTopics .dt-forum-info-table__title").each(function (i, elm) {
                newestTopicsTitle.push($(this).text());
            });

            $(".ipsWidget_NewestTopics .dt-forum-info-table__in a").each(function (i, elm) {
                newestTopicsCat.push($(this).text());
            });

            $(".ipsWidget_NewestTopics .dt-show-for-tablet time").each(function (i, elm) {
                newestTopicsTime.push($(this).text());
            });

            $(".ipsWidget_NewestTopics .dt-forum-info-table__title").each(function (i, elm) {
                newestTopicsURL.push($(this).attr('href'));
            });

            for (var i = 0; i < limit; i++) {
                APIArr.push({
                    type: 'topic',
                    title: newestTopicsTitle[i].capitalizeFirstLetter(),
                    url: newestTopicsURL[i],
                    time: newestTopicsTime[i],
                    cat: newestTopicsCat[i],
                });
            }

            var newestPostTitle = [];
            var newestPostCat = [];
            var newestPostTime = [];
            var newestPostURL = [];
            var newestPostArr = [];

            $(".ipsWidget_LatestPosts .dt-forum-info-table__title").each(function (i, elm) {
                newestPostTitle.push($(this).text());
            });

            $(".ipsWidget_LatestPosts .dt-forum-info-table__in a").each(function (i, elm) {
                newestPostCat.push($(this).text());
            });

            $(".ipsWidget_LatestPosts .dt-show-for-tablet time").each(function (i, elm) {
                newestPostTime.push($(this).text());
            });

            $(".ipsWidget_LatestPosts .dt-forum-info-table__title").each(function (i, elm) {
                newestPostURL.push($(this).attr('href'));
            });

            for (var i = 0; i < limit; i++) {
                APIArr.push({
                    type: 'post',
                    title: newestPostTitle[i].capitalizeFirstLetter(),
                    url: newestPostURL[i],
                    time: newestPostTime[i],
                    cat: newestPostCat[i]
                });
            }

            fs.writeFile("API.json", JSON.stringify(APIArr), "utf8", function (err) {
                if (err) throw err;
                console.log('file saved');
            });
        }
        done();
    }
});

c.queue('https://forum.telekom.sk/');

var job = new cron.CronJob({
  cronTime: '0 * * * *',
  onTick: function() {
    c.queue('https://forum.telekom.sk/');
  },
  start: false
});

job.start();

app.set('port', process.env.PORT || 8080);

app.get('/', function (req, res) {
    fs.readFile("API.json", 'utf8', function (err, data) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        res.end(data);
    });
});

app.listen(app.get('port'));