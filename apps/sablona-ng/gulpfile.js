var gulp        = require('gulp');
var sourcemaps      = require('gulp-sourcemaps');
var tsc             = require('gulp-typescript');
var tsProject       = tsc.createProject('tsconfig.json');
var config          = require('./gulp.config')();
var browserSync     = require('browser-sync');
var superstatic     = require('superstatic');
var includer        = require('gulp-html-ssi');
var es              = require('event-stream');
var sass            = require('gulp-sass');
var globalTemplates = '../../global-assets/templates/*.html';
var localTemplates  = './html-ssi/index.html';


gulp.task('htmlSSI', function() {
       es.merge(gulp.src(globalTemplates), gulp.src(localTemplates))
        .pipe(includer())
        .pipe(gulp.dest('./server/'));
});

gulp.task('sass', function () {
    return gulp.src('./app/assets/sass/*.scss')
                .pipe(sass({outputStyle: 'compact'}))
                .pipe(sourcemaps.init())
                .pipe(sass.sync().on('error', sass.logError))
                .pipe(sourcemaps.write('/maps'))
                .pipe(gulp.dest('./server/assets/css/'))
                .pipe(browserSync.stream());
});

gulp.task('compile-ts', function() {
    var sourceTsFiles = [
        config.allTs,
        config.typings
    ];

    var tsResult = gulp
        .src(sourceTsFiles)
        .pipe(sourcemaps.init())
        .pipe(tsc(tsProject));

        return tsResult.js
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(config.tsOutputPath));
});


gulp.task('serve', ['htmlSSI','sass','compile-ts'], function() {

    gulp.watch([config.allTs], ['compile-ts']);

    browserSync({
        port: 3000,
        file: ['index.html', '**/*.js'],
        injectChanges: true,
        logFileChanges: false,
        logLevel: 'silent',
        notify: true,
        reloadDelay: 0,
        server: {
            baseDir: ["./server/", "./../../global-assets/"],
            middleware: superstatic({debug: false})
        }
    });

});

gulp.task('default', ['serve']);