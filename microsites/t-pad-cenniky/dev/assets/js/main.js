var
  $kioskBack = $(".kiosk-back"),
  $cennikyMain = $("#cenniky-main"),

  pdfUrlShow = '',
  pdfUrlEmail = '',

  pdfUrlID = '';
pdfCategoryName = '';
pdfName = '';

$pdfOpen = $("#pdf-open"),
  $menuItem = $(".menu-item"),

  $emailSend = $(".email-send"),
  $emailThx = $(".email-thx"),
  $formEmail = $("#form-email"),
  $emailAddress = $("#email-address"),

  $cennikyB2c = $("#cenniky-b2c"),
  $cennikyB2b = $("#cenniky-b2b"),
  $cennikyVop = $("#cenniky-vop"),
  $cennikyHw = $("#cenniky-hw"),

  $cennikyMainB2c = $("#cenniky-b2c-main"),
  $cennikyMainB2b = $("#cenniky-b2b-main"),
  $cennikyMainVop = $("#cenniky-vop-main"),
  $cennikyMainHw = $("#cenniky-hw-main"),

  $cennikyMainHeader = $("#header-cenniky-main"),
  $cennikyB2cHeader = $("#header-cenniky-b2c"),
  $cennikyB2bHeader = $("#header-cenniky-b2b"),
  $cennikyVopHeader = $("#header-cenniky-vop"),
  $cennikyHwHeader = $("#header-cenniky-hw"),

  $kioskHome = $("#kiosk-home"),
  $kioskHomeImg = $("#kiosk-home-img"),
  $kioskHomeImgSelected = $("#kiosk-home-img-selected"),

  $kioskTelekom = $("#kiosk-telekom"),
  $kioskTelekomImg = $("#kiosk-telekom-img"),
  $kioskTelekomImgSelected = $("#kiosk-telekom-img-selected"),

  $kioskShop = $("#kiosk-shop"),
  $kioskShopImg = $("#kiosk-shop-img"),
  $kioskShopImgSelected = $("#kiosk-shop-img-selected"),

  $kioskPokrytie = $("#kiosk-pokrytie"),
  $kioskPokrytieImg = $("#kiosk-pokrytie-img"),
  $kioskPokrytieImgSelected = $("#kiosk-pokrytie-img-selected"),

  $telekomApp = $("#kiosk-telekom-app"),
  $telekomAppImg = $("#kiosk-telekom-app-img"),
  $telekomAppImgSelected = $("#kiosk-telekom-app-img-selected"),

  $kioskLsb = $("#kiosk-lsb"),
  $kioskLsbImg = $("#kiosk-lsb-img"),
  $kioskLsbImgSelected = $("#kiosk-lsb-img-selected"),

  $kioskCenniky = $("#kiosk-cenniky"),
  $kioskCennikyImg = $("#kiosk-cenniky-img"),
  $kioskCennikyImgSelected = $("#kiosk-cenniky-img-selected");

var dataURL = "https://backvm.telekom.sk/www/pdf/";

if (window.location.hostname === "localhost") {
  dataURL = "http://backvm.telekom.localhost/pdf/";
}

var redirectUrl = false;
var redirectUrlMain = '/kiosk';

var $kioskIframe = $("#selfcare-iframe");
var $kioskHomeContent = $("#selfcare-homepage");
var $kioskCennikyContent = $("#selfcare-cenniky");

function removeCookies() {
  $.ajax({
    url: 'https://www.telekom.sk/ideathon/cookies.php',
    type: "GET",
    async: true,
    crossDomain: true,
    dataType: "json",
    success: function (data) {
      console.log('OK');
    },
    error: function (error) {
      console.log('error');
    }
  });

}

function clearCookie(name, value, domain, path) {
  var domain2 = domain || document.domain;
  var path2 = path || "/";
  var value2 = value || "";

  var cookieName = name + "=" + value2 + "; expires=" + +new Date + "; domain=" + domain2 + "; path=" + path2;
  console.log('cookie: ' + cookieName);
  document.cookie = cookieName;
};

function updateClock() {
  var currentTime = new Date();
  var optionsDate = {month: 'long', day: 'numeric', year: 'numeric'};
  var optionsTime = {hour: 'numeric', minute: 'numeric', second: 'numeric'};

  $("#kiosk-date").html(currentTime.toLocaleString('sk-SK', optionsDate));
  $("#kiosk-time").html(currentTime.toLocaleString('sk-SK', optionsTime));

}

function getParameterByName(name) {
  var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
  return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
}

function sendGaView(shopCode, gaPage) {
  ga("wbTracker.send", "pageview", '/predajne/selfcare/' + gaPage + '/?id=' + shopCode);
}

$(document).ready(function () {
  var shopCode = getParameterByName('id');
  console.log(shopCode);

  if (shopCode == '01.NR.TMSK') {
    $kioskShop.show();
  }

  $formEmail.parsley();

  // showCenniky();
  // showVop();

  interval = setInterval(function () {
    removeCookies();
  }, 180 * 1000);

  $(document).on("ready", ".ada-popup-banner", function () {
    console.log("banner showed upp");
    $('.ada-popup-banner').hide();
  });

  setTimeout(function () {
    $('#cookies-information').hide();
  }, 1000);

  setTimeout(function () {
    $('.ada-popup-banne').hide();
  }, 5000);

  setInterval('updateClock()', 1000);

  /*    $(document).idleTimeout({
        redirectUrl: redirectUrl,      // redirect to this url on logout. Set to "redirectUrl: false" to disable redirect

        // idle settings
        idleTimeLimit: 20,           // 'No activity' time limit in seconds. 1200 = 20 Minutes
        idleCheckHeartbeat: 2,       // Frequency to check for idle timeouts in seconds

        // customCallback: true,       // set to false for no customCallback
        customCallback: function () {    // define optional custom js function
          console.log('refresh');
          // removeCookies();
        },

        // configure which activity events to detect
        // http://www.quirksmode.org/dom/events/
        // https://developer.mozilla.org/en-US/docs/Web/Reference/Events
        activityEvents: 'click keypress scroll wheel mousewheel mousemove touchmove touchstart touchend', // separate each event with a space

        // warning dialog box configuration
        enableDialog: false,           // set to false for logout without warning dialog

        // server-side session keep-alive timer
        sessionKeepAliveTimer: 10,   // ping the server at this interval in seconds. 600 = 10 Minutes. Set to false to disable pings
        sessionKeepAliveUrl: window.location.href // set URL to ping - does not apply if sessionKeepAliveTimer: false
      });*/

  $('#header').hide();

  $(".iframe").fancybox({
    maxWidth: 1430,
    maxHeight: 960,
    fitToView: true,
    width: '90%',
    height: '90%',
    autoSize: true,
    closeClick: true,
    openEffect: 'none',
    closeEffect: 'none',
    afterShow: function (instance, current) {
      var $this = $(this);
      console.info($this.attr('href'));
    }
  });

  $(".suhlas").fancybox({
    maxWidth: 800,
    fitToView: true,
    width: '90%',
    height: '90%',
    autoSize: true,
    closeClick: true,
    openEffect: 'none',
    closeEffect: 'none',

    afterShow: function (instance, current) {
      removeCookiesPhp();
    }
  });

  $(".cennik").fancybox({
    maxWidth: 800,
    fitToView: true,
    width: '90%',
    height: '100%',
    autoSize: true,
    closeClick: true,
    openEffect: 'none',
    closeEffect: 'none',
    wrapCSS: "fancy-cennik",

    afterShow: function (instance, current) {
      pdfUrlEmail = this.element.context.attributes['data-id']['nodeValue'];
      pdfCategoryName = this.element.context.attributes['data-category']['nodeValue'];
      pdfName = this.element.context.innerText;

      pdfUrlShow = dataURL + '?id=' + pdfUrlEmail + '#toolbar=0&navpanes=0&zoom=120';
      console.log(pdfUrlShow);

      $pdfOpen.attr('href', pdfUrlShow);

      console.log(pdfUrlEmail);
      console.log(pdfCategoryName);
      console.log(pdfName);

      $(".fancybox-inner").unbind();
    },

    afterClose: function (instance, current) {
      if ($(".fancy-cennik .fancybox-inner").height() > 300) {
        $(".fancy-cennik .fancybox-inner").height($(".fancy-cennik .fancybox-inner").height() - 130);
      }
      $emailSend.hide();
      $emailThx.hide();
      $emailAddress.val('');
    }
  });

  $("#pdf-email").on('click', function () {
    if ($(".fancy-cennik .fancybox-inner").height() < 300) {
      $(".fancy-cennik .fancybox-inner").height($(".fancy-cennik .fancybox-inner").height() + 130);
    }

    $emailThx.hide();
    $emailSend.show();
    $emailAddress.val('@');
  });

  $menuItem.on('click', function (e) {
    e.preventDefault();

    var pageId = $(this).attr('id');
    var pageName = $(this).attr('data-page');

    sendGaView(shopCode, pageName);
    console.log(pageId);
  });

  $formEmail.submit(function (e) {
    // $("#send-email").on('click', function () {
    e.preventDefault();

    $.ajax({
      url: 'https://backvm.telekom.sk/www/pdf/email2.php',
      type: "GET",
      async: true,
      data: {
        pdf: pdfUrlEmail,
        email: $emailAddress.val()
      },
      crossDomain: true,
      dataType: "json",
      success: function (data) {
        console.log('OK');
      },
      error: function (error) {
        console.log('error');
      }
    });

    $emailSend.hide();
    $emailThx.show();
  });

  $cennikyMainB2c.click(function () {
    $cennikyMainHeader.hide();
    $cennikyB2cHeader.show();
    $cennikyB2bHeader.hide();
    $cennikyHwHeader.hide();
    $cennikyVopHeader.hide();

    $cennikyMain.hide();
    $cennikyB2c.show();
    $cennikyB2b.hide();
    $cennikyHw.hide();
    $cennikyVop.hide();

  });

  $cennikyMainB2b.click(function () {
    $cennikyMainHeader.hide();
    $cennikyB2cHeader.hide();
    $cennikyB2bHeader.show();
    $cennikyHwHeader.hide();
    $cennikyVopHeader.hide();

    $cennikyMain.hide();
    $cennikyB2c.hide();
    $cennikyB2b.show();
    $cennikyHw.hide();
    $cennikyVop.hide();

  });

  $cennikyMainHw.click(function () {
    $cennikyMainHeader.hide();
    $cennikyB2cHeader.hide();
    $cennikyB2bHeader.hide();
    $cennikyHwHeader.show();
    $cennikyVopHeader.hide();

    $cennikyMain.hide();
    $cennikyB2c.hide();
    $cennikyB2b.hide();
    $cennikyHw.show();
    $cennikyVop.hide();

  });

  $cennikyMainVop.click(function () {
    showVop();
  });

  $kioskBack.click(function () {
    showCenniky()
  });

  function showVop() {
    $cennikyMainHeader.hide();
    $cennikyB2cHeader.hide();
    $cennikyB2bHeader.hide();
    $cennikyHwHeader.hide();
    $cennikyVopHeader.show();

    $cennikyMain.hide();
    $cennikyB2c.hide();
    $cennikyB2b.hide();
    $cennikyHw.hide();
    $cennikyVop.show();
  }

  $kioskHome.click(function () {
    hideAll();
    showAll();

    $kioskHomeImgSelected.show();
    $kioskHomeImg.hide();

    $kioskHomeContent.show();
    $kioskIframe.hide();
    $kioskCennikyContent.hide();

    redirectUrl = false;
  });

  $kioskTelekom.click(function () {
    hideAll();
    showAll();

    $kioskTelekomImgSelected.show();
    $kioskTelekomImg.hide();

    $kioskIframe.show();

    redirectUrl = redirectUrlMain;

  });

  $kioskLsb.click(function () {
    hideAll();
    showAll();

    $kioskLsbImgSelected.show();
    $kioskLsbImg.hide();

    $kioskIframe.show();

    redirectUrl = redirectUrlMain;

  });

  $kioskShop.click(function () {
    hideAll();
    showAll();

    $kioskShopImgSelected.show();
    $kioskShopImg.hide();

    $kioskIframe.show();
    // $kioskIframe.attr("src", "https://st.fixdistribution.sk/");

    redirectUrl = redirectUrlMain;

  });

  $kioskPokrytie.click(function () {
    hideAll();
    showAll();

    $kioskPokrytieImgSelected.show();
    $kioskPokrytieImg.hide();

    $kioskIframe.show();
    // $kioskIframe.attr("src", "https://www.telekom.sk/wiki/mapa-pokrytia");

    redirectUrl = redirectUrlMain;

  });

  $telekomApp.click(function () {
    hideAll();
    showAll();

    $telekomAppImgSelected.show();
    $telekomAppImg.hide();

    $kioskIframe.show();
    // $kioskIframe.attr("src", "https://www.telekom.sk/telekom-aplikacia");

    redirectUrl = redirectUrlMain;

  });

  $kioskCenniky.click(function () {
    showCenniky();

    redirectUrl = false;
  });

  function showCenniky() {
    hideAll();
    showAll();

    $kioskCennikyContent.show();

    $kioskIframe.hide();
    $kioskCennikyImgSelected.show();
    $kioskCennikyImg.hide();

    $cennikyMainHeader.show();
    $cennikyB2cHeader.hide();
    $cennikyB2bHeader.hide();
    $cennikyHwHeader.hide();
    $cennikyVopHeader.hide();

    $cennikyMain.show();
    $cennikyB2c.hide();
    $cennikyB2b.hide();
    $cennikyHw.hide();
    $cennikyVop.hide();

  }

  function hideAll() {
    $kioskIframe.hide();
    $telekomAppImgSelected.hide();
    $kioskHomeImgSelected.hide();
    $kioskTelekomImgSelected.hide();
    $kioskPokrytieImgSelected.hide();
    $kioskCennikyImgSelected.hide();
    $kioskLsbImgSelected.hide();
    $kioskShopImgSelected.hide();

    $kioskHomeContent.hide();
    $kioskCennikyContent.hide();
  }

  function showAll() {
    $telekomAppImg.show();
    $kioskTelekomImg.show();
    $kioskShopImg.show();
    $kioskPokrytieImg.show();
    $kioskCennikyImg.show();
    $kioskLsbImg.show();
    $kioskHomeImg.show();

  }
});