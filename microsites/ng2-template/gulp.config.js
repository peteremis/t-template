module.exports = function () {
    var config = {
            allTs: './app/**/*.ts',
            templateTs: './app/templates/*.ts',
            typings: './typings/**/*.d.ts',
            tsOutputPath: './app/',
            cmsFolder: '7845296'
    }

    return config;
}