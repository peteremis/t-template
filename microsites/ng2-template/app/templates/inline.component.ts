import { Component } from '@angular/core';

@Component({
    selector: 'inline-template',
    template: `
         <section id="inline-component">
		    <div class="container-fixed">
		        <div class="hero__content">
		            <h2>Inline Template</h2>
		        </div>
		    </div>
		</section>`
})
export class InlineComponent {}