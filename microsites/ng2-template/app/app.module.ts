import { BrowserModule } from '@angular/platform-browser';
import { NgModule }      from '@angular/core';

import { AppComponent }   from './app.component';

import { InlineComponent } from './templates/inline.component';
import { ExternalComponent } from './templates/external.component';
import { RelativeComponent } from './templates/relative.component';

@NgModule({
    declarations: [
        AppComponent,
        InlineComponent,
        ExternalComponent,
        RelativeComponent,
    ],
    imports:      [
        BrowserModule
    ],
    bootstrap:    [
        AppComponent
    ]
})
export class AppModule { }