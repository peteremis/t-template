import { Component } from '@angular/core';

@Component({
  selector: 'my-app',
  templateUrl: './app/app.component.html',
  styleUrls: ['./app/app.component.css']
  // template: `
  // 		<inline-template></inline-template>
  //       <hr/>
  //       <external-template></external-template>
  //       <hr/>
  //       <external-relative></external-relative>`
})

export class AppComponent { }