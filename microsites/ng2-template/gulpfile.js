var gulp            = require('gulp'),
    sourcemaps      = require('gulp-sourcemaps'),
    typescript      = require('gulp-typescript'),
    tsconfig        = typescript.createProject('tsconfig.json'),
    config          = require('./gulp.config')(),
    browserSync     = require('browser-sync'),
    reload          = browserSync.reload,
    superstatic     = require('superstatic'),
    includer        = require('gulp-html-ssi'),
    es              = require('event-stream'),
    sass            = require('gulp-sass'),
    globalTemplates = '../../global-assets/templates/*.html',
    localTemplates  = './html-ssi/index.html';

var concat = require('gulp-concat');
var Builder = require('systemjs-builder');
var uglify = require('gulp-uglify');   
var rename = require("gulp-rename");

var gulpif = require('gulp-if');
var args = require('yargs').argv;
var del = require('del');

var src = 'src/';
var dist = 'dist/';
var server = 'server/';

//Base tasks html + css + ts/js

gulp.task('htmlSSI', function() {
       es.merge(gulp.src(globalTemplates), gulp.src(localTemplates))
        .pipe(includer())
        .pipe(gulp.dest('./server/'));
});

gulp.task('sass', function () {
    return gulp.src('./app/assets/sass/*.scss')
                .pipe(sass({outputStyle: 'compact'}))
                .pipe(sourcemaps.init())
                .pipe(sass.sync().on('error', sass.logError))
                .pipe(sourcemaps.write('/maps'))
                .pipe(gulp.dest('./server/assets/css/'))
                .pipe(browserSync.stream());
});

gulp.task('compile-ts', function() {
    var sourceTsFiles = [
        config.allTs,
        config.templateTs,
        config.typings
    ];

    var tsResult = gulp.src(sourceTsFiles)
        .pipe(sourcemaps.init())
        .pipe(typescript(tsconfig));

        return tsResult.js
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(config.tsOutputPath));
});

//Static server
gulp.task('browser-sync', ['sass','compile-ts','htmlSSI'], function() {
   browserSync({
        port: 3000,
        file: ['index.html', '**/*.js'],
        injectChanges: true,
        logFileChanges: false,
        logLevel: 'silent',
        notify: true,
        reloadDelay: 0,
        server: {
            baseDir: ["./server/", "./../../global-assets/"],
            middleware: superstatic({debug: false})
        }
    });
});

//Novy zaklad: gulp watch
gulp.task('watch', ['browser-sync'], function () {
    //css
    gulp.watch('./app/assets/sass/*.scss', ['sass']);
    gulp.watch('./app/*.css').on('change', reload);
    //js
    gulp.watch([config.allTs], ['compile-ts']);
    gulp.watch([config.templateTs], ['compile-ts']);
    gulp.watch('./app/*.js').on('change', reload);
    //html
    gulp.watch('./html-ssi/*.html', ['htmlSSI']);
    gulp.watch('./app/*.html').on('change', reload);
    gulp.watch('./app/templates/*.html').on('change', reload);
    gulp.watch('./server/*.html').on('change', reload);
    //img
    gulp.src('app/assets/img/**')
    .pipe(gulp.dest('./server/assets/img'));
});

gulp.task('default', ['watch']);

//
// Bundle dependencies into vendors file
gulp.task('bundle:libs', function () {
    return gulp.src([
        'node_modules/zone.js/dist/zone.js',
        'node_modules/core-js/client/shim.min.js',
        'node_modules/reflect-metadata/Reflect.js',
        'node_modules/systemjs/dist/system.src.js',
        'systemjs.config.js'])
        .pipe(concat('vendors.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('public/assets/js'));
});

// Generate systemjs-based builds
gulp.task('bundle:js', function() {
  var builder = new Builder('.', 'systemjs.config.js');
  return builder.buildStatic('app', 'public/_dist/app.js');
});

// Minify JS bundle
gulp.task('minify:js', function() {
  return gulp
    .src('public/_dist/app.js')
    .pipe(uglify())
    .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest('public/assets/js'));
});

//any library referenced directly in HTML into a vendors.min.js
//any library referenced through system.config.js plus app code into an app.min.js
// gulp.task('scripts', ['compile:ts', 'bundle:js', 'bundle:libs', 'minify:js']);
gulp.task('scripts', ['bundle:js', 'bundle:libs']);



//////////////////////// NEW ////////////////////

gulp.task('build-ts', function () {
    return gulp.src(src + 'app/**/*.ts')
        .pipe(gulpif(!args.production, sourcemaps.init()))
        .pipe(typescript(tsconfig))
        .pipe(gulpif(!args.production, sourcemaps.write()))
        .pipe(gulp.dest(dist + 'app'));
});

gulp.task('build-copy', function () {
    gulp.src([src + 'app/**/*.html', src + 'app/**/*.css'])
        .pipe(gulp.dest(dist + 'app'));

    gulp.src([src + 'index.html'])
        .pipe(gulp.dest(dist));

    //server 
    gulp.src([src + 'app/**/*.html', src + 'app/**/*.css'])
        .pipe(gulp.dest(server + 'app'));
    gulp.src([src + 'index.html'])
        .pipe(gulp.dest(server));

    return gulp.src([src + 'systemjs.config.js'])
        .pipe(gulp.dest(dist));
});

//Clean
gulp.task('clean', function() {
   del([dist + '/**/*.html', dist + '/**/*.css'], dist + 'app');
});

//vendor
gulp.task('vendor', function() {
    del([dist + '/vendor/**/*']);

    gulp.src(['node_modules/@angular/**'])
        .pipe(gulp.dest(dist + 'vendor/@angular'));

    //ES6 Shim
    gulp.src('node_modules/es6-shim/**')
        .pipe(gulp.dest(dist + '/vendor/es6-shim/'));

    //reflect metadata
    gulp.src('node_modules/reflect-metadata/**')
        .pipe(gulp.dest(dist + '/vendor/reflect-metadata/'));

    //rxjs
    gulp.src('node_modules/rxjs/**')
        .pipe(gulp.dest(dist + '/vendor/rxjs/'));

    //systemjs
    gulp.src('node_modules/systemjs/**')
        .pipe(gulp.dest(dist + '/vendor/systemjs/'));
    
    // moment
    gulp.src('node_modules/moment/**')
        .pipe(gulp.dest(dist + '/vendor/moment/'));

    //zonejs
    return gulp.src('node_modules/zone.js/**')
        .pipe(gulp.dest(dist + '/vendor/zone.js/'));
});

gulp.task('build', ['build-ts', 'build-copy']);


