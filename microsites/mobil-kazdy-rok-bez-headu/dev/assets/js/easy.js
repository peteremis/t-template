$(document).ready(function () {
    $("#activate-vp1, #activate-vp2, #activate-vp3, #activate-vp4, #activate-vp5, #activate-vp6, #activate-vp7, #activate-vp8, #activate-vp9, #activate-vp10, #activate-vp11, #activate-vp12, #activate-vp13, #activate-vp14, #activate-vp15, #activate-vp16, #activate-vp17, #activate-vp18, #activate-vp19, #activate-vp20, #activate-vp21, #activate-vp22, #activate-vp23, #activate-vp24, #activate-vp25, #activate-vp26, #activate-vp27, #activate-vp28, #activate-vp29, #activate-vp30, #activate-vp31, #activate-vp32, #activate-vp33, #activate-vp34, #activate-vp35, #activate-vp36, #activate-vp37, #activate-vp38, #activate-vp39, #activate-vp40, #activate-vp41, #activate-vp42, #activate-vp43, #activate-vp44, #activate-vp45, #activate-vp46, #activate-vp47, #activate-vp48, #activate-vp49, #activate-vp50, #activate-vp51, #activate-vp52, #activate-vp53, #activate-vp54, #activate-vp55, #activate-vp56, #activate-vp57, #activate-vp58, #activate-vp59, #activate-vp60, #activate-tb1, #activate-tb2, #activate-tb3").fancybox({
        'hideOnContentClick': true
    });

    loadPhones();
    loadDetail();
});

function loadPhones() {

    $("#product-holder").empty();

    var detail = {};
    var html = '';

    $.ajax({
        type: "get",
        async: false,
        crossDomain: true,
        dataType: "json",
        data: "",
        //url: 'https://www.telekom.sk/documents/10179/453831/detail.json',
        url: 'https://m.telekom.sk/wb/vypredaj3/detail.php',
        success: function (phone) {
            detail = phone;
        },
        error: function () {
            //alert("Chyba pri posielani ... skus este raz!");
        }
    });

    $.ajax({
        type: "get",
        async: false,
        crossDomain: true,
        dataType: "json",
        data: "",
        //url: 'https://www.telekom.sk/documents/10179/462443/phones.json',
        url: 'https://m.telekom.sk/wb/vypredaj3/phones.php',
        success: function (phones) {
            html = generatePhones(phones, detail);
            $(html).appendTo("#product-holder");

        },
        error: function () {
            //alert("Chyba pri posielani ... skus este raz!");
        }
    });

    html = '';

    $.ajax({
        type: "get",
        async: false,
        crossDomain: true,
        dataType: "json",
        data: "",
        //url: 'https://www.telekom.sk/documents/10179/462443/phones.json',
        url: 'https://m.telekom.sk/wb/vypredaj3/tablets.php',
        success: function (phones) {

            html = generatePhones(phones, detail);
            $(html).appendTo("#product-holder");
        },
        error: function () {
            //alert("Chyba pri posielani ... skus este raz!");
        }
    });
}

function generatePhones(phones, detail) {
    var html = '';
    var button_order = '';
    var button_vypr = '';
    var button_xsmini = '';
    var xsmini = 0;

    phones.sort(function (a, b) {
        return parseFloat(b.xsmini) - parseFloat(a.xsmini);
    });

    phones.sort(function (a, b) {
        return parseFloat(b.available) - parseFloat(a.available);
    });

    phones.sort(function (a, b) {
        return parseFloat(a.order) - parseFloat(b.order);
    });


    for (var a = 0; a < phones.length; a++) {

        xsmini = 0;

        var idPhone = phones[a].id;

        var results = [];
        var deviceID = '';
        var deviceID2 = '';
        var deviceurl = '';
        var searchField = 'id';
        var searchVal = idPhone;
        for (var i = 0; i < detail.length; i++) {
            if (detail[i][searchField] == searchVal) {
                deviceID = detail[i][searchField];
                if (detail[i].peckaOrder !== undefined) {
                    
                    deviceurl = detail[i].peckaOrder.toString();

                    button_order = '<div class="btn-order">' + '<a class="button button_lima" href="' + deviceurl + '"><span>KÚPIŤ</span></a>' + '</div>';
                    button_vypr = '<div class="btn-order">' + '<div class="vypredane">VYPREDANÉ</div>' + '</div>';

                    console.log(button_order);
                } else {

                    //console.log(phones[a].url);
                    button_order = '<div class="btn-order">' + '<a class="button button_lima" href="' + phones[a].url + '"><span>KÚPIŤ</span></a>' + '</div>';
                    button_vypr = '<div class="btn-order">' + '<div class="vypredane">VYPREDANÉ</div>' + '</div>';
                }
            }
        }

        //console.log(results);




        /*var xsminiDetailUrl = detail[idPhone].peckaOrder;
		button_xsmini = '<div class="btn-order">' + '<a class="button button_lima" href="' + xsminiDetailUrl + '"><span>VIAC INFO</span></a>' + '</div>';
*/
        if (phones[a].xsmini == 1) {
            //button_order = button_xsmini;
            xsmini = 1;
        }

        if (phones[a].available != 1) {
            button_order = button_vypr;
        }

        if (phones[a].visible == 1) {
            html +=
                '<div class="slide"><div class="phone">' +
                '<div class="images">';

            if (xsmini == 1) {
                html += '<img class="img-ec" src="/documents/10179/689416/ec_vypredaj-mobilov.png">';
            } else {
                html += '<img class="img-ec" src="/documents/10179/689416/ec_vypredaj-mobilov.png">';
            }

            var detailPrice = $.grep(detail, function (e) {
                return e.id == phones[a].id;
            });

            html += '<img class="img-phone" src="' + detailPrice[0].image + '">' +
                '' +
                '</div>' +
                '<h3>' + phones[a].name + '</h3>' +
                '<div class="desc">' + phones[a].description['desc1'] + '</div>' +
                '<div class="desc">' + phones[a].description['desc2'] + '</div>' +
                '<div class="desc">' + phones[a].description['desc3'] + '</div>';

            if (xsmini == 1) {
                html += '<div class="price">';
                html += '<div class="price1">' + detailPrice[0].pecka + ',00 €' + '</div></div>';
            } else {
                html += '<div class="price">' +
                    '<div class="price1">' + phones[a].price1 + '</div>' +
                    '<div class="price2">' + phones[a].price2 + '</div>' +
                    '<div class="price-eur">€</div>' +
                    '</div>';
            }

            html = html + button_order +
                '<div class="more-info"><a href="#' + phones[a].id + '" id="activate-' + phones[a].id + '">Viac info</a></div></div></div>';

            button_order = '';
        }

    }

    return html;
}

function loadDetail() {

    $("#detail").empty();

    $.ajax({
        type: "get",
        async: false,
        crossDomain: true,
        dataType: "json",
        data: "",
        //url: 'https://www.telekom.sk/documents/10179/462443/detail.json',
        url: 'https://m.telekom.sk/wb/vypredaj3/detail.php',
        success: function (phones) {
            var html = "";
            for (var a = 0; a < phones.length; a++) {

                xsmini = 0;

                if (typeof phones[a].xsminiOne !== 'undefined') {
                    xsmini = 1;
                }

                html +=
                    '<div class="detail-lightbox" id="' + phones[a].id + '">' +
                    '<div class="detail-header">' +
                    '<h2>' + phones[a].name + '</h2>' +
                    '</div>' +
                    '<div class="detail-desc">' +
                    '<div class="images-detail">';

                if (xsmini == 1) {
                    html += '<img class="detail-ec" src="https://www.telekom.sk/documents/10179/462918/vypredaj_ec_xsimini.png">';
                } else {
                    html += '<img class="detail-ec" src="https://www.telekom.sk/documents/10179/462918/vypredaj_ec.png">';
                }

                html += '<div class="detail-img"><img src="' + phones[a].image + '"></div>' +
                    '</div>' +
                    '<div class="detail-text">' +
                    '<table class="detail-table">';

                var detail = phones[a].description;

                for (var b = 0; b < detail.length; b++) {
                    html +=
                        '<tr>' +
                        '<td><div class="odrazka"></div></td>' +
                        '<td>' + detail[b].desc[0] + '</td>' +
                        '<td class="row-2">' + detail[b].desc[1] + '</td>' +
                        '</tr>';
                }
                html +=
                    '</table>' +
                    '</div>' +
                    '</div>' +
                    '</div>';
            }

            $(html).appendTo("#detail");
        },
        error: function () {
            //alert("Chyba pri posielani ... skus este raz!");
        }
    });

}