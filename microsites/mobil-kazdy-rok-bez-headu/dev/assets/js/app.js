var globalUser = {},
$startpage = null,
$startpageVerification = null,
$standardProlong = null,
$standardProlongSoft = null,
$standardProlongSoftNonHw = null,
$standardProlongSoftNonHwThankYou = null,
$numberTransfer = null,
$transferNotAllowed = null,
$transferNotAllowedThankYou = null,
$easyProlong = null,
$mobilEveryYear = null,
$happyLooners = null,
$wrongSegment = null,
test = false;

jQuery(document).ready(function ($) {

    //PAGE SWITCHER
    //PAGE LIST: [startpage, cant-check, startpage-verification, standard-prolong, standard-prolong-soft, standard-prolong-soft-non-hw, standard-prolong-soft-non-hw-thank-you, number-transfer, transfer-not-allowed, transfer-not-allowed-thank-you, easy-prolong, mobil-every-year, happy-looners]
    $('.startpage').css('display', 'block');

    $('head').append("<meta name='og:image' content='https://www.telekom.sk/documents/10179/623804/sagan.jpg'>");
    $('head').append("<meta name='og:title' content='Vymeňte svoj starý mobil za nový už po ruku! S paušálmi Happy'>");
    $('head').append("<meta name='og:url' content='https://www.telekom.sk/mobilkazdyrok'>");
    $('head').append("<meta name='og:description' content='Po novom už nemusíte čakať 24 mesiacov na nový telefón. Môžete ho mať každý rok. '>");

    var $cmbConfirm = '',
    $discountText = $('.discount-text'),
    $discountMainText = $('.discount-main-text'),
    $cantCheck = $('.cant-check'),
    analyticsUrl = '/mobilkazdyrok',
    analyticsTitle = 'Mobil každý rok',
    shortUrl = '',
    analyticsNextUrl = '',
    analyticsNextTitle = '';

    $startpage = $('.startpage');
    $startpageVerification = $('.startpage-verification');
    $standardProlong = $('.standard-prolong');
    $standardProlongSoft = $('.standard-prolong-soft');
    $standardProlongSoftNonHw = $('.standard-prolong-soft-non-hw');
    $standardProlongSoftNonHwThankYou = $('.standard-prolong-soft-non-hw-thank-you');
    $numberTransfer = $('.number-transfer');
    $transferNotAllowed = $('.transfer-not-allowed');
    $transferNotAllowedThankYou = $('.transfer-not-allowed-thank-you');
    $easyProlong = $('.easy-prolong');
    $mobilEveryYear = $('.mobil-every-year');
    $wrongSegment = $('.wrong-segment'),
    $happyLooners = $('.happy_looners');

    var switchPage = function (currentPage, nextPage) {
        currentPage.hide();
        nextPage.show();
    };

    var History = window.History;
    if (History.enabled) {
        var newurl;
        if (window.location.host === 'm.telekom.sk') {
            newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + '?zdroj=mtelekom';
        } else {
            newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + '?zdroj=app';
        }

        State = History.getState();
        History.pushState({
            urlPath: newurl
        },'' ,newurl);
        document.title = 'Mobil každý rok - Telekom';
    } else {
        return false;
    }

    // Establish Variables
    var State = History.getState(),
    $log = $('#log'),
    prolongType = '',
    ratePlan = '',
    choosenPhone = '',
    buyURL = '',
    otherHW = '',
    easyPhones = '',
    phonenumberWithZero = '';

    // Log Initial State
    //History.log('initial:', State.data, State.title, State.url);

    // Bind to State Change
    History.Adapter.bind(window, 'statechange', function () { // Note: We are using statechange instead of popstate
        // Log the State
        var State = History.getState(); // Note: We are using History.getState() instead of event.state
        //History.log('statechange:', State.data, State.title, State.url);
    });

    var tokenValue = '',
    msisdnValue = '',
    errorMsg = '',
    prolongType = '';

    if (sessionStorage.getItem('validatedAs') != '' && sessionStorage.getItem('validatedAs') != null && sessionStorage.getItem('validatedAs') != undefined) {

        displayPhones('easy', 'easy-prolong', ratePlan);
        displayPhones('happy', 'standard-prolong', ratePlan);

        if (sessionStorage.getItem('prolongType') != '' && sessionStorage.getItem('prolongType') != null && sessionStorage.getItem('prolongType') != undefined) {
            if (sessionStorage.getItem('prolongType') === 'soft') {
                setSoftText(sessionStorage.getItem('sale'));
            } else if (sessionStorage.getItem('prolongType') === 'happy_looners') {
                setLoonersText(sessionStorage.getItem('sale'));
            }

        }

        if (sessionStorage.getItem('msisdn') != '' && sessionStorage.getItem('msisdn') != null && sessionStorage.getItem('msisdn') != undefined) {
            $('#number-transfer-link').attr("href", "https://www.telekom.sk/mam-zaujem/prenos-cisla-bonus-200/formular" + "&msisdn=" + sessionStorage.getItem('msisdn'));
        }

        switchPage($('.content-page:visible'), window[(sessionStorage.getItem('validatedAs'))]);
    }

    $('<div class="reset-holder"><a class="link link_dark reset-number" href="#">Zmeniť číslo</a></div>').prependTo('.board-holder .board:first-of-type');

    function checkIfPlaceHolder() {
        var placeholder = $('#msisdn').eq(0).attr('placeholder');
        var inputVal = $('#msisdn').eq(0).val();

        if (placeholder == inputVal) {
            return true;
        } else {
            return false;
        }
    }

    function checkButton() {

        if (test) {
            $('#msisdn-btn').removeClass('btn_disabled');
            $('#msisdn-btn').prop("disabled", false);
        } else {
            if (!checkIfPlaceHolder()) {
                if ($('#msisdn').eq(0).val().length >= 10) {
                    $('#msisdn-btn').removeClass('btn_disabled');
                    $('#msisdn-btn').prop("disabled", false);
                } else {
                    if (!$('#msisdn').hasClass('btn_disabled')) {
                        $('#msisdn-btn').addClass('btn_disabled');
                        $('#msisdn-btn').prop("disabled", true);
                    }
                }
            }
        }

    }

    $('.reset-number').on('click', function (e) {
        e.preventDefault();
        switchPage($('.content-page:visible'), $startpage);
        sessionStorage.removeItem('validatedAs');
        sessionStorage.removeItem('prolongType');
        sessionStorage.removeItem('sale');
        sessionStorage.removeItem('msisdn');
        $('#msisdn-btn').addClass('btn_disabled');
        $('#msisdn-btn').prop("disabled", true);
        checkButton();
    });

    $("#msisdn").focus();

    if (document.location.hostname == "localhosta") {
        tokenValue = '123';
        msisdnValue = '111222333';

        displayPhones('easy', 'easy-prolong', '');
        displayPhones('happy', 'standard-prolong', ratePlan);

        //switchPage($startpage, $standardProlongSoft);
        //switchPage($startpage, $numberTransfer);
        //switchPage($startpage, $standardProlong);
        switchPage($startpage, $easyProlong);
        //switchPage($startpage, $cantCheck);
    }

    $("table.prop label:before").after("padding-right: 10px;padding-left: 10px;");

    $("#code").keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });

    $("#msisdn").keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });

    $("#msisdn").keyup(function (event) {

        checkButton();


        if (event.keyCode == 13) {
            $("#msisdn-btn").click();
        }
    });

    $("#code").keyup(function (event) {
        if (event.keyCode == 13) {
            $("#code-btn").click();
        }
    });

    $('#msisdnForm').submit(function (e) {
        e.preventDefault();

        //$(".vykup-loader").show();
        $("#ajaxLoadingSpinnerOverlay").show();

        History.pushState({
            state: 1,
            rand: Math.random()
        }, '', '#' + shortUrl);

        $.ajax({
            type: "GET",
            async: true,
            crossDomain: true,
            dataType: "json",
            data: "msisdn=" + $("#msisdn").val(),
            url: 'https://m.telekom.sk/vykup/api/check-mobile2',

            success: function (d) {
                //$(".vykup-loader").hide();
                $("#ajaxLoadingSpinnerOverlay").hide();

                sessionStorage.setItem('msisdn', $("#msisdn").val());

                msisdnValue = sessionStorage.getItem('msisdn');

                phonenumberWithZero = msisdnValue;

                if (phonenumberWithZero.charAt(0) == '9') {
                    phonenumberWithZero = '0' + msisdnValue;
                }

                //console.log(phonenumberWithZero);

                $("#msisdn").val('');
                var phonenumberWithZero = phonenumberWithZero.replace(/^(.{4})(.{3})(.*)$/, "$1 $2 $3");

                $('.current-phone-number').html(phonenumberWithZero);


                if (d.response == 'ok') {
                    tokenValue = d.token;
                    sessionStorage.setItem('token', tokenValue);
                    switchPage($startpage, $startpageVerification);
                    $("#code").focus();
                } else if (d.response == 'over_prolong') {
                    $('#vykup-over').hide();
                    $('#over-prolong').show();

                } else if (d.response == 'before_prolong') {
                    $('#msisdnForm').hide();
                    $('#before-prolong').show();
                    $('#step1-response2').show().html(d.desc);

                } else if (d.response == 'other_operator') {
                    tokenValue = d.token;

                    analyticsUrl = '/mobilkazdyrok/prenos-cisla-bonus';
                    analyticsTitle = 'Prenos čísla';
                    sendGaPageView(analyticsUrl, analyticsTitle);

                    switchPage($startpage, $numberTransfer);
                    sessionStorage.setItem('validatedAs', '$numberTransfer');

                } else {
                    $('#change-number-desc').show().html(d.desc);
                }

            },
            error: function () {
                //$(".vykup-loader").hide();
                $("#ajaxLoadingSpinnerOverlay").hide();

                errorMsg = 'Momentálne nie je možné overiť mobilné číslo. Skúste to prosím neskôr.';
                $('#step1 .error-msg').show().html(errorMsg);
            }
        });

    });

    function showCodeForm() {
        $('#step1').hide();
        $('#step2').show();
        $("#code").val("").focus();
        $('#step1 .error').hide();
    }

    $('#codeForm').submit(function (e) {

        analyticsUrl = '/mobilkazdyrok/overovaci-kod';
        analyticsTitle = 'Overovací kód';
        sendGaPageView(analyticsUrl, analyticsTitle);

        e.preventDefault();

        //$(".vykup-loader").show();
        $("#ajaxLoadingSpinnerOverlay").show();

        globalUser.tokenValue = tokenValue;
        globalUser.msisdnValue = msisdnValue;

        $.ajax({
            type: "GET",
            async: true,
            crossDomain: true,
            dataType: "json",
            data: "msisdn=" + msisdnValue + "&token=" + tokenValue + "&code=" + $('#code').val(),
            url: 'https://m.telekom.sk/vykup/api/check-code2',

            success: function (d) {

                //console.log(d);

                $('#code').val('');
                $('#step2 #code').removeClass('input_error');
                $('#step2 .error-msg').html('');

                ratePlan = d.rateplan_code;

                displayPhones('easy', 'easy-prolong', ratePlan);
                displayPhones('happy', 'standard-prolong', ratePlan);


                //$(".vykup-loader").hide();
                $("#ajaxLoadingSpinnerOverlay").hide();

                if (d.response == 'ok') {

                    var shortUrl = 'hladaj-telefon';

                    analyticsUrl = '/mobilkazdyrok/hladaj-telefon';
                    analyticsTitle = 'Hľadaj telefón';
                    sendGaPageView(analyticsUrl, analyticsTitle);

                    History.pushState({
                        state: 3,
                        rand: Math.random()
                    }, '', '#' + shortUrl);

                    showPhones(d.pricevat);

                    switchPage($startpageVerification, $mobilEveryYear);
                    sessionStorage.setItem('validatedAs', '$mobilEveryYear');

                } else {

                    //console.log(d.code);

                    if (d.code == 'wrong_code') {
                        analyticsUrl = '/mobilkazdyrok/nespravny-kod';
                        analyticsTitle = 'Nesprávny kód';
                        sendGaPageView(analyticsUrl, analyticsTitle);

                        $('#step2 #code').addClass('input_error');
                        $('#step2 .error-msg').show().html('Zadaný overovací kód nie je správny.');

                    } else if (d.code == 'other_operator') {
                        tokenValue = d.token;

                        analyticsUrl = '/mobilkazdyrok/prenos-cisla-bonus';
                        analyticsTitle = 'Prenos čísla';
                        sendGaPageView(analyticsUrl, analyticsTitle);

                        switchPage($startpageVerification, $numberTransfer);

                        switchPage($startpage, $numberTransfer);
                        sessionStorage.setItem('validatedAs', '$numberTransfer');
                        $('#number-transfer-link').attr("href", "https://www.telekom.sk/mam-zaujem/prenos-cisla-bonus/formular?kampan=mobilkazdyrok" + "&msisdn=" + sessionStorage.getItem('msisdn'));

                    } else if (d.code == 'wrong_token') {
                        analyticsUrl = '/mobilkazdyrok/nespravny-token';
                        analyticsTitle = 'Nesprávny token';
                        sendGaPageView(analyticsUrl, analyticsTitle);

                        $('#step2 .error-msg').show().html('Nie je možné overiť overovací kód. Skúste to prosím neskôr.');

                    } else if (d.code == 'over_prolong') {

                        analyticsUrl = '/mobilkazdyrok/prolongacia';
                        analyticsTitle = 'Prolongácia';
                        sendGaPageView(analyticsUrl, analyticsTitle);

                        switchPage($startpageVerification, $standardProlong);
                        sessionStorage.setItem('validatedAs', '$standardProlong');

                    } else if (d.code == 'prepaid') {
                        analyticsUrl = '/mobilkazdyrok/easy';
                        analyticsTitle = 'Easy';
                        sendGaPageView(analyticsUrl, analyticsTitle);

                        switchPage($startpageVerification, $easyProlong);
                        sessionStorage.setItem('validatedAs', '$easyProlong');

                    } else if (d.code == 'wrong_segment') {
                        analyticsUrl = '/mobilkazdyrok/firemny';
                        analyticsTitle = 'Firemný zákazník';
                        sendGaPageView(analyticsUrl, analyticsTitle);

                        switchPage($startpageVerification, $wrongSegment);
                        sessionStorage.setItem('validatedAs', '$wrongSegment');

                    } else if (d.code == 'no_prolong') {
                        analyticsUrl = '/mobilkazdyrok/nema-narok';
                        analyticsTitle = 'Nemá nárok na akciu';
                        sendGaPageView(analyticsUrl, analyticsTitle);

                        $('#step2 #code').addClass('input_error');
                        $('#step2 .error-msg').show().html('Ľutujeme, aktuálne nemáte nárok na nový mobil.');

                    } else if (d.code == 'telekom_emp') {
                        analyticsUrl = '/mobilkazdyrok/telekom-zamestnanec';
                        analyticsTitle = 'Telekom zamestnanec';
                        sendGaPageView(analyticsUrl, analyticsTitle);

                        $('#step2 #code').addClass('input_error');
                        $('#step2 .error-msg').show().html('Nárok na nový služobný telefón si môžeš overiť na intranete alebo v mobilnej aplikácii Telekom.');

                    } else if (d.code == 'nonhw') {
                        analyticsUrl = '/mobilkazdyrok/nonhw-tohw';
                        analyticsTitle = 'Non HW to HW';
                        sendGaPageView(analyticsUrl, analyticsTitle);

                        prolongType = 'nonhw';
                        switchPage($startpageVerification, $standardProlongSoftNonHw);
                        sessionStorage.setItem('validatedAs', '$standardProlongSoftNonHw');

                    } else if (d.code == 'early_prolong') {
                        analyticsUrl = '/mobilkazdyrok/soft-migration';
                        analyticsTitle = 'Soft migrácia';
                        sendGaPageView(analyticsUrl, analyticsTitle);

                        prolongType = 'soft';

                        setSoftText(d.sale);

                        switchPage($startpageVerification, $standardProlongSoft);
                        sessionStorage.setItem('validatedAs', '$standardProlongSoft');
                        sessionStorage.setItem('prolongType', prolongType);
                        sessionStorage.setItem('sale', d.sale);

                    } else if (d.code == 'other_telekom') {
                        analyticsUrl = '/mobilkazdyrok/ups-nevyhodnoteny';
                        analyticsTitle = 'Nevyhodnotený';
                        sendGaPageView(analyticsUrl, analyticsTitle);

                        switchPage($startpageVerification, $cantCheck);

                    } else if (d.code == 'happy_looners') {
                        analyticsUrl = '/mobilkazdyrok/looners';
                        analyticsTitle = 'Looners';
                        prolongType = 'happy_looners';
                        sendGaPageView(analyticsUrl, analyticsTitle);

                        setLoonersText(d.sale);

                        switchPage($startpageVerification, $happyLooners);
                        sessionStorage.setItem('validatedAs', '$happyLooners');
                        sessionStorage.setItem('prolongType', prolongType);
                        sessionStorage.setItem('sale', d.sale);
                    } else {
                        analyticsUrl = '/mobilkazdyrok/chyba';
                        analyticsTitle = 'Chyba';
                        sendGaPageView(analyticsUrl, analyticsTitle);

                        $('#step2 .error-msg').show().html('Ľutujeme, aktuálne nemáte nárok na nový mobil.');
                    }

                    if (prolongType === '' || prolongType == undefined || prolongType == null) {
                        sessionStorage.setItem('prolongType', d.code);
                    } else {
                        sessionStorage.setItem('prolongType', prolongType);
                    }

                }


            },
            error: function () {
                //$(".vykup-loader").hide();
                $("#ajaxLoadingSpinnerOverlay").hide();
                $('#step2 .error-msg').show().html('Nie je možné overiť overovací kód. Skúste to prosím neskôr.');
            }
        });
    });

    function soft30() {
        $discountMainText.html('Áno, máte nárok na nový mobil,<br />ak si vezmete niektorý z paušálov Happy. Navyše získate:');
        $discountText.html('ŠPECIÁLNU ZĽAVU 30 %<br />na paušál happy!');
    }

    function soft10() {
        $discountMainText.html('Áno, máte nárok na nový mobil,<br />ak si vezmete niektorý z paušálov Happy. Navyše získate:');
        $discountText.html('ŠPECIÁLNU ZĽAVU 10 %<br />na paušál happy!');
    }

    function softDefault() {
        $discountMainText.html('Áno, máte nárok na nový mobil.<br />Váš aktuálny program však nie je možné predĺžiť.<br/>So svojim číslom ' + msisdnValue + ' môžete predčasne prejsť na niektorý<br/>z paušálov Happy a získať tak');
        $discountText.html('NEOBMEDZENÉ VOLANIA DO PEVNEJ<br />AJ MOBILNEJ SIETE TELEKOM.');
    }

    function setSoftText(sale) {
        if (sale == '30%') {
            soft30();
        } else if (sale == '10%') {
            soft10();
        } else {
            softDefault();
        }
    }

    function looners30() {
        $discountMainText.html('Áno, máte nárok na nový mobil.<br />Navyše pre vaše telefónne číslo ' + msisdnValue + ' získate:');
        $discountText.html('ŠPECIÁLNU ZĽAVU 30 %<br />na paušál happy!');
    }

    function looners20() {
        $discountMainText.html('Áno, máte nárok na nový mobil.<br />Navyše pre vaše telefónne číslo ' + msisdnValue + ' získate:');
        $discountText.html('ŠPECIÁLNU ZĽAVU 20 %<br />na paušál happy!');
    }

    function looners10() {
        $discountMainText.html('Áno, máte nárok na nový mobil.<br />Navyše pre vaše telefónne číslo ' + msisdnValue + ' získate:');
        $discountText.html('ŠPECIÁLNU ZĽAVU 10 %<br />na paušál happy!');
    }

    function looners2() {
        $discountMainText.html('Áno, máte nárok na nový mobil.<br />Navyše pre vaše telefónne číslo ' + msisdnValue + ' získate:');
        $discountText.html('ŠPECIÁLNU ZĽAVU 2 €<br />Z MESAČNÉHO POPLATKU NA PAUŠÁL HAPPY!');
    }

    function setLoonersText(sale) {
        if (sale == '30%') {
            looners30();
        } else if (sale == '20%') {
            looners20();
        } else if (sale == '10%') {
            looners10();
        } else if (sale == '2 €') {
            looners2();
        }
    }


    function showPhones(price) {

        $('#info-spoplatnenie').html(price);

    }

    function loadHappyPhone() {

        $.ajax({
            type: "get",
            async: false,
            crossDomain: true,
            dataType: "json",
            data: "",
            url: 'https://m.telekom.sk/shop/phones_mkr.php',
            success: function (phone) {
                happyPhone = phone;
            },
            error: function () {

            }
        });
    }


    function loadEasyPhones() {

        $.ajax({
            type: "get",
            async: false,
            crossDomain: true,
            dataType: "json",
            data: "",
            url: 'https://m.telekom.sk/wb/vypredaj3/detail.php',
            success: function (phones) {
                easyPhonesDetail = phones;

            },
            error: function () {

            }
        });

        $.ajax({
            type: "get",
            async: false,
            crossDomain: true,
            dataType: "json",
            data: "",
            url: 'https://m.telekom.sk/wb/vypredaj3/phones.php',
            success: function (phones) {
                /// ??? VISIBLE == 1
                easyPhones = phones;

            },
            error: function () {}
        });
    }

    function displayPhones(prolongType, pageClass, ratePlan) {


        var $title = $('.' + pageClass + ' .prod-title'),
        $lightboxTitle = $('.' + pageClass + ' .pi__title'),
        $lightboxDesc = $('.' + pageClass + ' .pi__lightbox ul'),
        lightboxMaxDesc = 8,
        $buyBtn = $('.' + pageClass + ' .btn-buy'),
        $otherHWBtn = $('.' + pageClass + ' .btn-choose-other'),
        $mainFeat = $('.' + pageClass + ' .main-feat'),
        $prodImg = $('.' + pageClass + ' .prod-img');


        if (prolongType == 'easy') {
            loadEasyPhones();

            var randomNum = Math.floor(Math.random() * easyPhones.length - 1),
            phoneDetail = null,
            phonedetailImg = '',
            detailedDescEasy = '';
            choosenPhone = easyPhones[randomNum];

            $title.text(choosenPhone.name.toLowerCase());
            $lightboxTitle.text(choosenPhone.name.toLowerCase());
            //$buyBtn.attr('href', choosenPhone.url);

            $mainFeat.html(choosenPhone.description.desc1 + '<br/>' + choosenPhone.description.desc2 + '<br/>' + choosenPhone.description.desc3);

            for (var i = 0; i < easyPhonesDetail.length; i++) {
                if (easyPhonesDetail[i].id == choosenPhone.id) {
                    phoneDetail = easyPhonesDetail[i];
                    phonedetailImg = phoneDetail.image;
                    $buyBtn.attr('href', phoneDetail.peckaOrder + '?kampan=mobikazdyrok');
                }
            }

            $prodImg.attr('src', phonedetailImg);
            //console.log('IMG: ' + phonedetailImg);

            if (lightboxMaxDesc > phoneDetail.description.length) {
                lightboxMaxDesc = phoneDetail.description.length;
            }

            //console.log('LIMIT: ' + lightboxMaxDesc);

            for (var i = 0; i < lightboxMaxDesc - 1; i++) {
                detailedDescEasy += '<li>';
                for (var j = 0; j < phoneDetail.description[i].desc.length; j++) {
                    detailedDescEasy += phoneDetail.description[i].desc[j];
                }
                detailedDescEasy += '</li>'
            }

            $(detailedDescEasy).appendTo($lightboxDesc);

        } else if (prolongType == 'happy') {

            loadHappyPhone();

            var choosenPhone = happyPhone,
            //detailArr = choosenPhone.description.split(','),
            detailArr = choosenPhone.description,
            specArr = choosenPhone.spec.split(','),
            newSpec = choosenPhone.newspec,
            detailedDesc = '';


            //https://www.telekom.sk/objednavka/-/scenario/e-shop/hw-happy/vyber-hw?vyberrp=xl&zariadenie=lg_k8_lte&vybersl=PROLONGATION

            buyURL = 'https://www.telekom.sk/objednavka/-/scenario/e-shop/hw-happy/vyber-hw?vyberrp=' + ratePlan + '&zariadenie=' + choosenPhone.id + '&vybersl=PROLONGATION&kampan=mobikazdyrok';
            otherHW = 'https://www.telekom.sk/mob/objednavka/-/scenario/e-shop/hw-happy/vyber-hw?vybersl=PROLONGATION&vyberrp=' + ratePlan + '&kampan=mobikazdyrok';

            $title.text(choosenPhone.title.toLowerCase());
            $lightboxTitle.text(choosenPhone.title.toLowerCase());
            $buyBtn.attr('href', buyURL);
            $otherHWBtn.attr('href', otherHW);
            //console.log('IMG: ' + choosenPhone.image);
            $prodImg.attr('src', choosenPhone.image);

            //$mainFeat.html(detailArr[0] + '<br/>' + detailArr[1] + '<br/>' + detailArr[2]);
            $mainFeat.html(detailArr);

            if (lightboxMaxDesc > specArr.length) {
                lightboxMaxDesc = specArr.length;
            }

            /*for (var i = 0; i < lightboxMaxDesc - 1; i++) {
            detailedDesc += '<li>';
            detailedDesc += specArr[i];
            detailedDesc += '</li>'
        }*/

        for (key in newSpec) {
            detailedDesc += '<li>';
            detailedDesc += key + ': ' + newSpec[key];
            detailedDesc += '</li>';

            //console.log(newSpec);
        }

        /*for (var i = 0; i < newSpec.length; i++) {
        detailedDesc += '<li>';
        detailedDesc += newSpec[i];
        detailedDesc += '</li>'
    }*/


    $(detailedDesc).appendTo($lightboxDesc);
}

}

$('.change-number').on('click', function () {
    switchPage($startpageVerification, $startpage);
    $('#code').val('');
    $('#step2 #code').removeClass('input_error');
    $('#step2 .error-msg').html('');
});


$('.open_cmb').click(function (e, secClass) {

    e.preventDefault();

    if ($(this).hasClass('cmb__sps')) {
        //console.log('sps');
        $cmbConfirm = $standardProlongSoft;
        prolongType = 'sps';

        analyticsUrl = '/mobilkazdyrok/soft-migration/potvrdenie';
        analyticsTitle = 'Soft migrácia - potvrdenie';
        analyticsNextUrl = '/mam-zaujem/mobilkazdyrok/soft-migration';
        analyticsNextTitle = 'Soft migrácia - mám záujem';

        sendGaPageView(analyticsUrl, analyticsTitle);

    } else if ($(this).hasClass('cmb__spsnh')) {
        //console.log('spsnh');
        $cmbConfirm = $standardProlongSoftNonHw;
        prolongType = 'spsnh';

        analyticsUrl = '/mobilkazdyrok/nonhw-tohw/potvrdenie';
        analyticsTitle = 'Non HW to HW - potvrdenie';
        analyticsNextUrl = '/mam-zaujem/mobilkazdyrok/nonhw-tohw';
        analyticsNextTitle = 'Non HW to HW - mám záujem';

        sendGaPageView(analyticsUrl, analyticsTitle);

    } else if ($(this).hasClass('cmb__happy_looners')) {
        $cmbConfirm = $happyLooners;
        prolongType = 'happy_looners';

        analyticsUrl = '/mobilkazdyrok/looner/potvrdenie';
        analyticsTitle = 'Looner - potvrdenie';
        analyticsNextUrl = '/mam-zaujem/mobilkazdyrok/looner';
        analyticsNextTitle = 'Looner - mám záujem';

        sendGaPageView(analyticsUrl, analyticsTitle);

    } else if ($(this).hasClass('cmb__easy')) {

        $cmbConfirm = $easyProlong;
        prolongType = 'easy';

        //console.log('easy');

        analyticsUrl = '/mobilkazdyrok/easy/easy-s-mobilom/potvrdenie';
        analyticsTitle = 'Easy s mobilom - potvrdenie';
        analyticsNextUrl = '/mam-zaujem/mobilkazdyrok/easy/easy-s-mobilom';
        analyticsNextTitle = 'Easy s mobilom - mám záujem';

        sendGaPageView(analyticsUrl, analyticsTitle);

    } else if ($(this).hasClass('cmb__easy_happy')) {
        $cmbConfirm = $easyProlong;
        prolongType = 'easy_happy';

        analyticsUrl = '/mobilkazdyrok/easy/migracia/potvrdenie';
        analyticsTitle = 'Easy migrácia - potvrdenie';
        analyticsNextUrl = '/mam-zaujem/mobilkazdyrok/easy/migracia';
        analyticsNextTitle = 'Easy migrácia - mám záujem';

        sendGaPageView(analyticsUrl, analyticsTitle);

    } else if ($(this).hasClass('cmb__prolong')) {
        $cmbConfirm = $standardProlong;
        prolongType = 'prolong';

        analyticsUrl = '/mobilkazdyrok/prolong/potvrdenie';
        analyticsTitle = 'Prolongácia - potvrdenie';
        analyticsNextUrl = '/mam-zaujem/mobilkazdyrok/prolong';
        analyticsNextTitle = 'Prolongácia - mám záujem';

        sendGaPageView(analyticsUrl, analyticsTitle);

    } else if ($(this).hasClass('cmb__prenos')) {
        $cmbConfirm = $numberTransfer;
        prolongType = 'prenos';

        analyticsUrl = '/mobilkazdyrok/prenos-cisla-bonus/potvrdenie';
        analyticsTitle = 'Prenos čísla - potvrdenie';
        analyticsNextUrl = '/mam-zaujem/mobilkazdyrok/prenos-cisla-bonus';
        analyticsNextTitle = 'Prenos čísla - mám záujem';

        sendGaPageView(analyticsUrl, analyticsTitle);

    } else {
        $cmbConfirm = $cantCheck;
        prolongType = 'other';

        analyticsUrl = '/mobilkazdyrok/ups-nevyhodnoteny/potvrdenie';
        analyticsTitle = 'Nevyhodnotený - potvrdenie';
        analyticsNextUrl = '/mam-zaujem/mobilkazdyrok/ups-nevyhodnoteny';
        analyticsNextTitle = 'Nevyhodnotený - mám zaujem';

        sendGaPageView(analyticsUrl, analyticsTitle);

    }

    var $cmbParent = $('.cmb__' + prolongType).closest('.fixed-column').find('.board-holder .board:first-of-type'),
    $cmbParentBorder = $('.cmb__' + prolongType).closest('.with-border'),
    $cmbCloneClass = prolongType + '__clone',
    $cmbClone = $($cmbCloneClass);

    //console.log($cmbParent);
    //console.log('MSISDN: ' + msisdnValue);

    $('.cmb_msisdn').val(sessionStorage.getItem('msisdn'));

    $('.cmb__lightbox').addClass($cmbCloneClass).insertAfter($cmbParent).show();

    if (prolongType == 'easy_happy') {
        //$cmbParentBorder.css('background-color', '#fff');
    }

    $cmbParent.hide();

    $("html, body").animate({
        scrollTop: 0
    }, "slow");

});

$('.cmb__lightbox-close').on('click', function (e) {

    e.preventDefault();
    var $cmbParent = $('.cmb__' + prolongType).closest('.fixed-column').find('.board-holder .board:first-of-type'),
    $cmbParentBorder = $('.cmb__' + prolongType).closest('.with-border'),
    $cmbCloneClass = prolongType + '__clone',
    $cmbClone = $($cmbCloneClass);

    $('.cmb__lightbox').insertAfter('.cmb-lightbox__holder').hide();

    if (prolongType == 'easy_happy') {
        //$cmbParentBorder.css('background-color', 'transparent');
    }

    $cmbParent.show();
});

$('.cmb_button').on('click', function (e) {
    sendGaPageView(analyticsNextUrl, analyticsNextTitle);
    event.preventDefault();
    sendCmbRequest();
});


function sendCmbRequest() {

    $("#ajaxLoadingSpinnerOverlay").show();

    var title = 'Mám záujem';

    History.pushState({
        state: 5,
        rand: Math.random()
    }, '', '#mam-zaujem');

    var errorMsg = 'Nebolo možné odoslať email. Skúste to prosím neskôr.';

    $.ajax({
        type: "GET",
        async: true,
        crossDomain: true,
        dataType: "json",
        data: "msisdn=" + sessionStorage.getItem('msisdn') + "&type=" + sessionStorage.getItem('prolongType') + "&token=" + sessionStorage.getItem('token'),
        url: 'https://m.telekom.sk/vykup/api/contact-me2',

        success: function (d) {
            //$(".vykup-loader").hide();
            $("#ajaxLoadingSpinnerOverlay").hide();
            //console.log(d.response);
            if (d.response == 'ok') {
                switchPage($cmbConfirm, $standardProlongSoftNonHwThankYou);
                //console.log($cmbConfirm);
            } else {
                $cmbConfirm.find('.error-msg').show().html(errorMsg);
            }
        },
        error: function () {
            $("#ajaxLoadingSpinnerOverlay").hide();
            $('#over-prolong .error').show().html(errorMsg);
        }
    });
}

$('.go-step1').click(function (event) {
    event.preventDefault();
    $('.step1-response').hide();
    $('#msisdnForm').show();
    $("#msisdn").val("").focus();
});

$('.go-step2').click(function (event) {
    event.preventDefault();
    $('#step1').show();
    $("#step2").hide();
    $("#msisdn").val("").focus();
    $("#code").val("");
    $("#codeForm .error").hide();
});

$('#change-phone').click(function (event) {
    event.preventDefault();

    analyticsUrl = '/mobilkazdyrok/hladaj-telefon/zmena';
    analyticsTitle = 'Zmena mobilu';
    sendGaPageView(analyticsUrl, analyticsTitle);

    $(".prop").empty();

    $('.result').hide();
    $('#phone-main').hide();
    $(".row-1-background").show();
    $('#search-main').show();
    $("#find").val("").focus();
});

$('#btn-print').click(function (event) {
    event.preventDefault();
    savePhoneLog('print');

    analyticsUrl = '/mobilkazdyrok/hladaj-telefon/print';
    analyticsTitle = 'Tlač';
    sendGaPageView(analyticsUrl, analyticsTitle);

    $(".phone").printThis({
        debug: true,
        importCSS: true,
        importStyle: false,
        printContainer: true
    });
});

function savePhoneLog(type) {

    $.ajax({
        type: "GET",
        async: true,
        crossDomain: true,
        dataType: "json",
        data: "msisdn=" + sessionStorage.getItem('msisdn') + "&token=" + sessionStorage.getItem('token') + "&phone=" + global.phoneName + "&" + type + "=1",
        url: 'https://m.telekom.sk/vykup/api/phone-log',

        success: function (d) {},
        error: function () {}
    });

}

$('#btn-reserve').click(function (event) {
    savePhoneLog('rezervacia');
});

$('#btn-shop').click(function (event) {
    savePhoneLog('predajne');
});

$('#btn-shop').qtip({
    //show: 'click',
    //hide: 'unfocus',
    content: 'Zoznam predajní',
    style: {
        classes: 'btn-tooltip'
    },
    position: {
        my: 'top center',
        at: 'bottom center'
    }
});

$('#btn-mail').qtip({
    //show: 'click',
    //hide: 'unfocus',
    content: 'Poslať na e-mail',
    style: {
        classes: 'btn-tooltip'
    },
    position: {
        my: 'top center',
        at: 'bottom center'
    }
});

$('#btn-print').qtip({
    //show: 'click',
    //hide: 'unfocus',
    content: 'Vytlačiť',
    style: {
        classes: 'btn-tooltip'
    },
    position: {
        my: 'top center',
        at: 'bottom center'
    }
});

function Popup(data) {
    var mywindow = window.open('', 'my div', 'height=400,width=600');
    mywindow.document.write('<html><head><title>my div</title>');
    /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
    mywindow.document.write('</head><body >');
    mywindow.document.write(data);
    mywindow.document.write('</body></html>');

    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10

    mywindow.print();
    mywindow.close();

}

});
