var global = new Object();
global.data = "";
global.phoneData = "";
global.phoneImage = "";
$(function () {
    $('.change-phone-link').on('click', function (e) {
        e.preventDefault();
        switchPage('phone-detail', 'mobil-every-year');
    });
});

if (window.top != window.self) {
    $('.row-1-background').css("background-image", "none");
}


function switchPage(currentPage, nextPage) {
    $('.' + currentPage).hide();
    $('.' + nextPage).show();
}

if (sessionStorage.getItem('validatedAs') === '$mobilEveryYear') {

    switchPage('phone-detail', 'mobil-every-year');
}


function sendGaPageView(url, title) {
    if (document.location.hostname == "localhost") {
        //console.log('Analytics page: ' + url);
        //console.log('Analytics title: ' + title);
    } else if (typeof ga == 'function') {
        ga('create', 'UA-55238177-1', 'auto');
        ga('set', {
            page: url,
            title: title
        });
        ga('send', 'pageview');
    }
}

function findPhone(x) {
    if (x.length < 2) {
        $(".result").empty().hide();
        return;
    }

    if (global.data == "") {
        //console.log(x);
        var url = 'https://m.telekom.sk/wb/vykup/phones.php';

        $(".vykup-loader").show();

        $.ajax({
            async: true,
            crossDomain: true,
            dataType: "json",
            url: url,
            success: function (data) {
                $(".vykup-loader").hide();

                global.data = data;
                showResult(x);
            },
            error: function () {
                $(".vykup-loader").hide();
                alert("Nepodarilo sa načítať zoznam telefónov, skúste to prosím neskôr.");
            }
        });
    } else {
        // uz mame data natiahnute
        showResult(x);
    }
}

function showResult(find) {
    var x = "";
    $(".result").empty().show();
    $.each(global.data, function (i, val) {

        if ((val.identifier.toLowerCase()).indexOf(find.toLowerCase()) > -1) {
            x += "<div class='row' onclick='showPhone(\"" + val.id + "\", \"" + val.image + "\", \"" + val.identifier + "\")'>" + val.identifier + "</div>";
        }
    });
    $(".result").html(x);
}

function showPhone(x, image, phoneName) {


    switchPage('mobil-every-year', 'phone-detail');

    global.phoneImage = image;
    global.phoneName = phoneName;

    var url = 'https://m.telekom.sk/wb/vykup/phone.php?id=' + x;
    $.ajax({
        async: true,
        crossDomain: true,
        dataType: "json",
        url: url,
        success: function (data) {
            $(".vykup-loader").hide();

            global.phoneData = data;

            showPhoneDetail();
        },
        error: function () {
            $(".vykup-loader").hide();
            alert("Nepodarilo sa načítať informácie o mobilnom telefóne, skúste to prosím neskôr.");
        }
    });

}

function showPhoneDetail() {

    analyticsUrl = '/mobilkazdyrok/hladaj-telefon/mobil/';
    analyticsTitle = 'Hľadaj telefón';

    var phoneLower = global.phoneName.toLowerCase().trim();
    var phonex = phoneLower.split(' ');
    var phonea = phonex[0];

    var re = new RegExp(phonea, "g");
    var phoneb = phoneLower.replace(re, '').trim();

    sendGaPageView(analyticsUrl + phonea + '/' + phoneb, 'Hľadaj telefón - ' + global.phoneName);

    History.pushState({
        state: 3,
        rand: Math.random()
    }, '', '#' + global.phoneName);

    $.ajax({
        type: "GET",
        async: true,
        crossDomain: true,
        dataType: "json",
        data: "msisdn=" + globalUser.msisdnValue + "&token=" + globalUser.tokenValue + "&phone=" + global.phoneName,
        url: 'https://m.telekom.sk/vykup/api/phone-log',

        success: function (d) {},
        error: function () {}
    });

    $(".prop").empty();
    var a = global.phoneData;
    var html = "";
    //$(".phoneName").html(a.device.description);
    $(".phoneName").html(global.phoneName);
    $("#price").html(a.device.price);
    $(".image").html("<img src='" + global.phoneImage + "'/>");
    html += "<tr class='t1'><td>Zánovné zariadenie <span><img class=\"iconinfo\" id=\"infoNew\" src=\"https://www.telekom.sk/documents/10179/355205/icon_info_grey.png\"/></span></td>";
    html += "<td><label for='id_newish_0'><input type='radio' onchange='calc()' checked id='id_newish_0' checked name='newish' value='" + (0) + "'>&nbsp;Nie</label>";
    html += "<label for='id_newish_1'><input type='radio' onchange='calc()' id='id_newish_1' name='newish' value='" + a.device.states.state[0].options.option.price_plus_minus + "'>&nbsp;Áno</label></td></tr>";

    //if (a.device.states.state[1].options.option)

    var infokey = '';
    for (var x = 0; x < (a.device.states.state[1].options.option).length; x++) {

        //console.log(a.device.states.state[1].options.option);

        html += "<tr class='t2'>";
        if (a.device.states.state[1].options.option[x].type == "list") {

            html += "<td>" + a.device.states.state[1].options.option[x].description + " <span><img class=\"iconinfo\" id=\"" + a.device.states.state[1].options.option[x].primary_key + "\" src=\"https://www.telekom.sk/documents/10179/355205/icon_info_grey.png\"/></span></td>";
            html += "<td><select onchange='calc()' id='id_" + a.device.states.state[1].options.option[x].primary_key + "' name='" + a.device.states.state[1].options.option[x].primary_key + "'>";
            for (var s = 0; s < (a.device.states.state[1].options.option[x].suboptions.suboption).length; s++) {
                html += "<option value='" + a.device.states.state[1].options.option[x].suboptions.suboption[s].price_plus_minus + "'>" + a.device.states.state[1].options.option[x].suboptions.suboption[s].description + "</option>";
            }
            html += "</select></td>";
        } else if (a.device.states.state[1].options.option[x].type == "boolean") {
            html += "<td>" + a.device.states.state[1].options.option[x].description + " <span><img class=\"iconinfo\" id=\"" + a.device.states.state[1].options.option[x].primary_key + "\" src=\"https://www.telekom.sk/documents/10179/355205/icon_info_grey.png\"/></span></td>";
            html += "<td><label for='id_" + a.device.states.state[1].options.option[x].suboptions.suboption[0].primary_key + "_0'><input onchange='calc()'  type='radio' id='id_" + a.device.states.state[1].options.option[x].suboptions.suboption[0].primary_key + "_0' checked name='" + a.device.states.state[1].options.option[x].suboptions.suboption[0].primary_key + "' value='" + a.device.states.state[1].options.option[x].suboptions.suboption[0].price_plus_minus + "'>&nbsp;" + a.device.states.state[1].options.option[x].suboptions.suboption[0].description + "</label>";
            html += "<label for='id_" + a.device.states.state[1].options.option[x].suboptions.suboption[0].primary_key + "_1'><input onchange='calc()' type='radio' id='id_" + a.device.states.state[1].options.option[x].suboptions.suboption[0].primary_key + "_1'  name='" + a.device.states.state[1].options.option[x].suboptions.suboption[0].primary_key + "' value='" + a.device.states.state[1].options.option[x].suboptions.suboption[1].price_plus_minus + "'>&nbsp;" + a.device.states.state[1].options.option[x].suboptions.suboption[1].description + "</label></td>";
            //html += "<label for='id_" + a.device.states.state[1].options.option[x].suboptions.suboption[0].primary_key + "_1'><div class='radio'><span class='checked'><input onchange='calc()' type='radio' id='id_" + a.device.states.state[1].options.option[x].suboptions.suboption[0].primary_key + "_1'  name='" + a.device.states.state[1].options.option[x].suboptions.suboption[0].primary_key + "' value='" + a.device.states.state[1].options.option[x].suboptions.suboption[1].price_plus_minus + "'>" + a.device.states.state[1].options.option[x].suboptions.suboption[1].description + "</span></div></label></td>";
        }
        html += "</tr>";
    }

    $(html).appendTo(".prop");

    $("#search-main").hide();
    $(".row-1-background").hide();
    $("#phone-main").show();

    $('#infoNew').qtip({
        style: {
            classes: 'info-tooltip'
        },
        content: 'Nepoužívaný prístroj s nerozbaleným príslušenstvom v kompletnom balení - ubehli najviac 3 mesiace od dátumu zakúpenia prvým koncovým používateľom.'
    });

    $('#D7496566-0EED-E011-A49C-00123F6845A3').qtip({
        style: {
            classes: 'info-tooltip'
        },
        content: 'Ak sa prístroj dá zapnúť a reaguje na zapínacie tlačidlo a pri zapínaní nehlási žiadnu chybovú hlášku, zvoľte <span class="bold">Áno</span>. V opačnom prípade zvoľte <span class="bold">Nie</span>.'
    });


    $('#8DBB385D-DC2B-E111-9947-00123F6845A3').qtip({
        style: {
            classes: 'info-tooltip'
        },
        content: 'Ak je možné prístroj bez komplikácií úplne nabiť, pri nabíjaní indikuje nabíjanie a pripojenú nabíjačku, zvoľte <span class="bold">Áno</span>. V opačnom prípade, a taktiež v prípade, že prístroj hlási akúkoľvek chybovú hlášku, zvoľte <span class="bold">Nie</span>.'
    });

    //wifi
    $('#2447B97D-432B-E511-80D1-005056B65EA0, #6729E9EE-452B-E511-80D1-005056B65EA0').qtip({
        style: {
            classes: 'info-tooltip'
        },
        content: 'Ak je možné pripojiť sa s prístrojom k sieti Wi-Fi a načítať webovú stránku, zvoľte <span class="bold">Áno</span>. V opačnom prípade zvoľte <span class="bold">Nie</span>. Ak prístroj nemá Wi-Fi, zvoľte <span class="bold">Áno</span>.'
    });

    $('#DD496566-0EED-E011-A49C-00123F6845A3').qtip({
        style: {
            classes: 'info-tooltip'
        },
        content: 'Ak prístroj umožňuje prichádzajúce a odchádzajúce hovory, ak sa prístroj pripojí k sieti operátora v SR a prístroj indikuje prítomnosť SIM karty a má signál, zvoľte <span class="bold">Áno</span>. V opačnom prípade zvoľte <span class="bold">Nie</span>.'
    });

    $('#4347B97D-432B-E511-80D1-005056B65EA0, #8629E9EE-452B-E511-80D1-005056B65EA0').qtip({
        style: {
            classes: 'info-tooltip'
        },
        content: 'Zvoľte stav krytu Vášho prístroja, podľa kategórií uvedených nižšie:' +
            '<br>a) <span class="bold">Bez poškodenia</span> – Kryt prístroja nevykazuje žiadne poškodenie.' +
            '<br>b) <span class="bold">Drobné poškodenie – škrabance</span> – Na kryte prístroja sú malé škrabance/odery, ktoré zásadne nezhoršujú optický vzhľad prístroja.' +
            '<br>c) <span class="bold">Stredne veľké poškodenie – ryhy</span> – Na kryte prístroja sú ryhy (hlbšie, zjavné na dotyk) alebo väčší počet škrabancov/oderov, ktoré zhoršujú optický vzhľad prístroja.' +
            '<br>d) <span class="bold">Veľké poškodenie – mechanické</span> – Kryt prístroja nesie stopy po páde, má praskliny alebo ošúpané/odreté rohy, prípadne niektoré jeho časti chýbajú. Celkový stav prístroja je zlý.'
    });

    $('#E5496566-0EED-E011-A49C-00123F6845A3').qtip({
        style: {
            classes: 'info-tooltip'
        },
        content: 'Zvoľte stav LCD – dotykovej obrazovky Vášho prístroja, podľa kategórií uvedených nižšie:' +
            '<br>a) <span class="bold">Bez poškodenia</span> – LCD prístroja nevykazuje žiadne poškodenia.' +
            '<br>b) <span class="bold">Drobné poškodenie – škrabance /odery</span> – na LCD prístroja sú malé ryhy, ktoré na dotyk nie sú zjavné (majú vlásočnicový charakter) a optický vzhľad prístroja ani možnosť jeho ovládania (dotykové LCD) zásadne nezhoršujú.' +
            '<br>c) <span class="bold">Veľké poškodenie – mechanické</span> – na LCD prístroja sú praskliny či hlboké ryhy, ktoré sú zjavné na dotyk. Stav LCD zhoršuje manipuláciu s prístrojom (či už z hľadiska ovládania alebo zhoršenej alebo obmedzenej viditeľnosti v prípade prasklín). ' +
            '<br>d) <span class="bold">Mapy – škvrny na pozadí (oxidácia)</span> – LCD je zasiahnuté vlhkosťou. Po zapnutí prístroja sú na úvodnej obrazovke (je nutné, aby obrazovka bola biela) zreteľné mapy či sivé škvrny priamo na LCD.'
    });

    $('#90E07AF3-CD8D-E111-9825-00123F6845A3').qtip({
        style: {
            classes: 'info-tooltip'
        },
        content: 'V prípade, že je prístroj silne znečistený od prachu a ďalších nečistôt a toto znečistenie je viditeľné voľným okom, zvoľte <span class="bold">Silne zaprášený</span>. V opačnom prípade zvoľte <span class="bold">Bez prachu a nečistôt</span>.'
    });

    //fotoaparat
    $('#9847B97D-432B-E511-80D1-005056B65EA0, #DB29E9EE-452B-E511-80D1-005056B65EA0').qtip({
        style: {
            classes: 'info-tooltip'
        },
        content: 'Ak je v prístroji možné spustiť fotoaparát a fotoaparát reaguje korektne a prístroj zhotovené fotografie ukladá, zvoľte <span class="bold">Áno</span>. V opačnom prípade zvoľte <span class="bold">Nie</span>. Ak prístroj nemá fotoaparát, zvoľte <span class="bold">Áno</span>.'
    });

    $('#FF98288B-E7E0-4545-9630-C5D964792300, #A1329927-B4E3-4E57-8918-F73C16263665').qtip({
        style: {
            classes: 'info-tooltip'
        },
        content: 'Ak klávesnica prístroja reaguje korektne (fungujú všetky klávesy) a fungujú všetky ostatné tlačidlá na prístroji, zvoľte <span class="bold">Áno</span>. V opačnom prípade zvoľte <span class="bold">Nie</span>. Ak prístroj nemá klávesnicu a všetky ostatné tlačidlá na prístroji sú funkčné, zvoľte <span class="bold">Áno</span>.'
    });

    $('#DA496566-0EED-E011-A49C-00123F6845A3, #7B88EF83-432B-E511-80D1-005056B65EA0, #89A5B3FD-607F-4A37-8DF3-E001C6DE61D5').qtip({
        style: {
            classes: 'info-tooltip'
        },
        content: 'Ak je k prístroju priložená nabíjačka, ktorá je plne funkčná a nie je nejako poškodená, zvoľte <span class="bold">Áno</span>. V opačnom prípade zvoľte <span class="bold">Nie</span>.'
    });



}

function calc() {
    var price = parseFloat(global.phoneData.device.price);


    if (parseInt($('input[name=newish]:checked').val()) == 0) {
        $(".t2").show();
        $('.prop input[type=radio]').each(function () {
            if ($(this).prop('checked')) {
                price += parseFloat($(this).val());
            }
        });
        $(".prop select option:selected").each(function () {
            price += parseFloat($(this).val());
        });
    } else {
        $(".t2").hide();
        price += parseFloat($('input[name=newish]:checked').val());
    }

    if (price < 0) price = 0;

    $("#price").html(Math.round(price * 100) / 100);
}
