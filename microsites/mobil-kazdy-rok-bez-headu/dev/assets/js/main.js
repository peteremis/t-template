$(function () {
    $('.info-startpage').qtip({
        content: {
            text: 'Pre overenie vášho telefónneho čísla vám pošleme overovací SMS kód.'
        },
        position: {
            my: 'bottom center',
            at: 'top center'
        },
        style: {
            classes: 'tooltip'
        }
    });
    $('.info-standard-prolong-step1').qtip({
        content: {
            text: 'Krok 1:<br/>Vyberte si nový mobil'
        },
        position: {
            my: 'left center',
            at: 'right center'
        },
        style: {
            classes: 'tooltip'
        }
    });
    $('.info-standard-prolong-step2').qtip({
        content: {
            text: 'Krok 2:<br/>Potrebujete poradiť s ponukou? Zavoláme Vám.'
        },
        position: {
            my: 'left center',
            at: 'right center'
        },
        style: {
            classes: 'tooltip'
        }
    });
    $('.info-standard-prolong-step3, .info-number-transfer-step3').qtip({
        content: {
            text: 'Objednávku vám zadarmo doručíme do 1 dňa.'
        },
        position: {
            my: 'left center',
            at: 'right center'
        },
        style: {
            classes: 'tooltip'
        }
    });
    $('.info-number-transfer-step1').qtip({
        content: {
            text: 'Krok 1:<br/>Stačí vyplniť základné údaje.'
        },
        position: {
            my: 'left center',
            at: 'right center'
        },
        style: {
            classes: 'tooltip'
        }
    });
    $('.info-number-transfer-step2').qtip({
        content: {
            text: 'Krok 2:<br/>Zabudnite na starosti. Všetko vybavíme za vás!'
        },
        position: {
            my: 'left center',
            at: 'right center'
        },
        style: {
            classes: 'tooltip'
        }
    });

    $('.tooltip-battery').qtip({
        content: {
            text: 'Darujeme vám externú batériu, ktorá slúži ako nabíjačka mobilu či tabletu aj tam, kde nie je elektrická zásuvka.<br/>'+
            'Ak sa vám vybije, môžete si ju nabiť sami alebo vám ju vymeníme za nabitú v niektorom z našich Telekom centier až do konca roku 2017.'
        },
        position: {
            my: 'right center',
            at: 'left center'
        },
        style: {
            classes: 'tooltip'
        }
    });

    var $lightboxActivator = $('.more-info-lightbox'),
    $lightboxElem = $('.board__lightbox'),
    $lightboxClose = $('.lightbox-close');

    $lightboxActivator.on('click', function(e) {
        e.preventDefault();
        $lightboxElem.css('display', 'block');
    });

    $lightboxClose.on('click', function(e) {
        e.preventDefault();
        $lightboxElem.css('display', 'none');
    });

});
