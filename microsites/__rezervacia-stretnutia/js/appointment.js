var config = {
  startDay: new Date(getDateFormat(new Date(),'yyyy-mm-dd')),
  shop: 0
};
//addCalendar();

var messages = {
  'error_showReservation':      'Vážený zákazník, ospravedlňujeme sa, na Vami vybranú predajňu nie je možné rezervovať termín.<br />Skúste prosím neskôr.',
  'error_getShop':              'Vážený zákazník, ospravedlňujeme sa, nastala chyba pri načítavaní údajov o predajniach.<br />Skúste prosím neskôr.',
  'success_createReservation':  'Vážený zákazník, rezervácia stretnutia bola úspešná. Onedlho Vám zašleme SMS s rezervačným PIN kódom. Prosím uschovajte si ho a pri príchode do predajne zadajte PIN kód na obrazovke vyvolávacieho systému a vezmite si lístok. Prvý voľný predajca sa Vám bude venovať. Rezervovaný termín je pre Vás garantovaný najviac 20 minút po objednanom čase.<br />Tešíme sa na Vašu návštevu.',
  'error_appointmentRejected':  'Vážený zákazník, požadovaný termín je už rezervovaný.<br />Zvoľte prosím iný termín.',
  'error_createReservation':    'Vážený zákazník, ospravedlňujeme sa, nastala chyba pri vytváraní rezervácie.<br />Skúste prosím neskôr.'
}

var urlJSON = {
  'getshopsandservices':  '/pb/onlineshop/getshopsandservices',
  'getappointments':      '/pb/onlineshop/getappointments/',
  'createappointment':    '/pb/onlineshop/createappointment',
  'getopeninghours':      '/pb/onlineshop/getopeninghours/'  
}

var formValue = {
  email: ''
};

getShops();

$('#selShops').change(changeShop);

$('#create_reservation > button').click(createReservation);

$('input.check').focusout(checkForm);


// functions

function addCalendar()
{
  var dayInWeek = new Date();
  config.startHour = getMinHour(config.openHours);
  config.endHour = getMaxHour(config.openHours);
  
  var dayName = ['Ne','Po','Ut','St','Št','Pi','So'];
  
  var addDiv = '<div style="margin-bottom: 5px;">'
    + '<div class="header">';

  for (var i = 0; i < 7; i++)
  {
    var month = (dayInWeek.getMonth() + 1);
    if (month < 10) month = '0' + month;
    var day = dayInWeek.getDate();
    if (day < 10) day = '0' + day;     
    var getDate = dayInWeek.getFullYear() + '-' + month + '-' + day; 
    
    var numberDay = dayInWeek.getDay();
    if (numberDay == 0) numberDay = 7;
    var addClass = (i == 0) ? ' selected-day' : '';
    
    addDiv += '<div class="day' + addClass + '" data-day="' + getDate  + '" style="display: inline-block;" >'
      + '<div>' + dayName[dayInWeek.getDay()] + '</div><div style="font-size: 16px;">' + day + '.' + month + '.' + '</div>'
      + "</div>";  
    
    dayInWeek.setDate(dayInWeek.getDate() + 1);
  }
  addDiv += '</div><div class="body">';

  for (var i = config.startHour; i <= config.endHour; i++)
  {
    var timeWithZero = (i < 10) ? '0' + i : i;
    
    addDiv += '<div class="time-cont select-enable" data-time="' + i + '">'
      + '<span class="time">' + timeWithZero + ':00' + '</span>'
      + '<span class="text">Voľný</span>'
      + '</div>';
  }
  
  addDiv += "</div></div>";   
  
  $('#calendar').html(addDiv);
}

function addReservation()
{
  if (formValue.shop == 0) return;
  
  $('#calendar .body .time-cont')
    .removeClass('select-enable')
    .removeClass('reserved')
    .addClass('select-disable');
  
  $('#res_val_sub1').hide(200);
  $('#calendar .body .time-cont.selected').removeClass('selected');
  
  var date_text = $('#calendar .header .selected-day').attr('data-day');
  var date_selected = new Date(date_text);
  var day = date_selected.getDay();
  if (day == 0) day = 7;
  
  if (typeof config.openHours[day] !== 'undefined')
  {
    var min = Math.ceil(config.openHours[day]['start'] / 100);
    var max = Math.ceil(config.openHours[day]['end'] / 100);
    //console.log(min + ' - ' + max);
    $.ajax(
    {
      url: urlJSON.getappointments + formValue.shop + '/' + date_text,
      type: 'get',
      dataType: 'json',
      async: false,
      complete: function()
      {
        $('#res_val').show(200);
      },
      success: function(json)
      {
        if (json['ns0:getAppointmentsMessageOut']['ns0:Body'] != null)
        {
          $.each( json['ns0:getAppointmentsMessageOut']['ns0:Body']['ns0:appointment'], function( key, value )
          {
            var dateRes = '';
            if (value['ns0:appointmentDateTime'] == undefined)
              dateRes = value;
            else
              dateRes = value['ns0:appointmentDateTime'];
           
            var dateSplit = dateRes.split('T'); 
            var hour = parseInt(dateSplit[1].substring(0,(dateSplit[1].indexOf(':'))));
  
            var comp = $('#calendar .body .time-cont[data-time="' + hour + '"]');
  
            comp.removeClass('select-disable')
              .addClass('reserved')
              .unbind( "click")
              .attr('data-original-title','Obsadený');
          });
        }
        $('#calendar .body .select-disable').removeClass('select-disable').addClass('select-enable');
        var index = $('#calendar .header .day').index($('#calendar .header .selected-day'));
        
        if (index == 0)
        {
          var d = new Date();
          var hour = d.getHours();      
          $('#calendar .body .time-cont').each(function(){
            if (parseInt($(this).attr('data-time')) <= parseInt(hour))
              $(this).removeClass('select-enable').addClass('select-disable');
          });
        }
        
        $('#calendar .body .reserved.select-disable').removeClass('reserved');
        $('#calendar .body .select-enable .text').text('Voľný');
        $('#calendar .body .reserved .text').text('Obsadený');
        $('#calendar .body .select-disable .text').text('Nedostupný');
        
        $('#calendar .body .select-enable').each(function(){
          var time = $(this).attr('data-time'); 
          if (time < min || time > max)
            $(this).removeClass('select-enable').addClass('select-disable').find('.text').text('Nedostupný');
        });
        
      },
      error: function(xhr, ajaxOptions, thrownError)
      {
        //alert('error');
        //console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
      }
    });
  }
  else
  {
    $('#calendar .body .select-disable .text').text('Nedostupný');
  }
}
$(document).on('click', '#calendar .header .day', function () {
  $('#calendar .header .selected-day').removeClass('selected-day');
  $(this).addClass('selected-day');
  $('#calendar .body i.fa-check').remove();
  $('#calendar .body .time-cont .text').show();
  
  addReservation();
});

$(document).on('click', '#calendar .body .select-enable', function () {
  if (!$(this).hasClass('selected'))
  {
    $('.create-message').hide();
    $('#calendar .selected').removeClass('selected');
    $(this).addClass('selected');
    $('#calendar .body i.fa-check').remove();
    $(this).append('<i class="fa fa-check"></i>');
    $('#calendar .body .time-cont .text').show();
    $(this).find('.text').hide();
    
    formValue.date = $('#calendar .header .selected-day').attr('data-day') + 'T' + $(this).attr('data-time') + ':00:00';   
      
    var selectDay = $('#calendar .header .selected-day').attr('data-day').split('-'); 
    var addText = selectDay[2] + '.' + selectDay[1] + '.' + selectDay[0] 
      + ' od ' + $(this).attr('data-time') + ':00'
      + ' do ' + (parseInt($(this).attr('data-time')) + 1) + ':00'
      + ' h';
    
    $('#date_reservation').text(addText);
    $('#shop_reservation').text($('#selShops option:selected').text());
    $('#res_val_sub1').show(200);
  }
});

function changeShop()
{
  var shopId = $(this).val();
  formValue.shop = shopId;
  $('#res_val').hide(200);
  $('.load-shop').show(200);
  window.setTimeout(function(){
    $('#res_val_sub1').hide();
    if (shopId > 0)
    {
      
      $.ajax(
      {
        url: urlJSON.getopeninghours + shopId,
        type: 'get',
        dataType: 'json',
        async: false,
        complete: function()
        {
           
        },
        success: function(json)
        {
          var tmpOut = [];
          $.each( json, function( key, value )
          {
            tmpOut[value['DAY']] = {
              'start': value['START_FIRST'],
              'end': value['START_LAST'] 
            };
          });
          config.openHours = tmpOut;
          
          addCalendar();
          $('#calendar .selected').removeClass('selected');
          addReservation();
          $('.load-shop').hide(200);
          $('#res_val').show(200);          
        },
        error: function(xhr, ajaxOptions, thrownError)
        {
          $('.load-shop').hide();
          modalError(messages.error_showReservation);
        }
      });
    }
    else
      $('.load-shop').hide(200);
    
  },210);

}

function createReservation()
{
  $('.create-message').hide(200);
  if (checkForm($('#mobile')) & checkForm($('#mail')))
  {
    $('.alert-message').remove();
  
    $('#create_reservation').hide();
    $('#create_timer').show();
         
    var businessCustomer = false;
    var serviceId = 17;
    if ($('input[name=typeCustomer]:checked').val() == 'ICO')
    {
      businessCustomer = true;
      serviceId = 18;
    }

    var jsonData = '{'
      + '"msisdn": "' + formValue.mobile + '",' 
      + '"shopCode": ' + formValue.shop + ','
      + '"date": "' + formValue.date + '",'
      + '"businessCustomer": ' + businessCustomer + ','
      + '"email": "' + formValue.email + '",'
      + '"description": "' + $('#desc').val() + '",'
      + '"serviceId": ' + serviceId
      + '}';
    //console.log(jsonData);
    $.ajax(
    {
      url: urlJSON.createappointment,
      type: 'POST',
      data: jsonData,
      dataType: 'json',
      contentType: "application/json",
      complete: function()
      {
        $('#create_timer').hide();
        $('#create_reservation').show();      
        $('#res_val_sub1 div.alert-danger').remove();
      },
      success: function(json)
      {
        $('#calendar i.fa-check').remove();
        $('#res_val_sub1').hide();
        $('#calendar .selected').removeClass('selected').removeClass('select-enable').addClass('reserved').unbind( "click").find('.text').text('Obsadený').show();
        modalAction('success',messages.success_createReservation);
      },
      error: function(xhr, ajaxOptions, thrownError)
      {

        if (typeof xhr.responseJSON !== 'undefined' && typeof xhr.responseJSON['ns:createAppointmentFaultMessage'] !== 'undefined')
        {
          if (xhr.responseJSON['ns:createAppointmentFaultMessage']['ns1:Body']['ns1:message'] == 'Appointment creation failed. Rejected by IWD.')
          {
              modalAction('error',messages.error_appointmentRejected);
              $('#calendar .selected').removeClass('selected').text('Obsadený');
              addReservation();       
          }
          else
          {
              modalAction('error', messages.error_createReservation);        
          }
        }
        else
        {
            modalAction('error', messages.error_createReservation);        
        }
        
        //console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
      }
    });
  }
}

function modalAction(action,text)
{
  if (action == 'success')
  {
    $('#error_mess').hide();
    $('#success_mess .cont').html(text);
    $('#success_mess').show();     
  }
  else if (action == 'error')
  {
    $('#success_mess').hide();
    $('#error_mess .cont').html(text);
    $('#error_mess').css('display','table');
  }
}

function checkForm(comp)
{
  if ($(this).attr('id') != undefined)
  {
    comp = $(this);
  }
  
  var errorMessage = '<div class="alert-message">$1</div>';

  comp.siblings('div.alert-message').remove();
  
  if (comp.attr('id') == 'mobile')
  {
    var mobile = comp.val();
    mobile = mobile.replace(/ |\/|-|\./g, '');

    if (mobile != parseInt(mobile) || mobile.length > 10 || mobile.length < 9)
    {
      comp.parent().addClass('has-error');
      comp.parent().append(errorMessage.replace('$1','Nesprávny formát telefónneho čísla'));
      return false;
    }
    else
    {
      formValue.mobile = (mobile.length == 10) ? mobile.substr(1) : mobile;
      comp.parent().removeClass('has-error');
    }
  }
  
  if (comp.attr('id') == 'mail')
  {
    var mail = comp.val();
    if (mail.length > 0)
    {
      var filter = /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/i;
      if (!filter.test(mail))
      {
        comp.parent().addClass('has-error');
        comp.parent().append(errorMessage.replace('$1','Nesprávny formát e-mailu'));
        return false;
      }
      else
      {
        formValue.email = mail;
        comp.parent().removeClass('has-error');
      }
    }
    else
    {
      if (comp.parent().hasClass('has-error'))
        comp.parent().removeClass('has-error');
    }
  }  
  return true;
}

function getShops()
{

  $.ajax(
  {
    url: urlJSON.getshopsandservices,
    type: 'get',
    dataType: 'json',
    complete: function()
    {
      $('.timer').hide();
      $('.cont-reservation').show();
    },
    success: function(json)
    {
      //$.each( json['ns0:getShopsAndServicesMessageOut']['ns0:Body'], function( key, value )
      if (json['ns0:getShopsAndServicesMessageOut']['ns0:Body'] != null) {
        $.each( json['ns0:getShopsAndServicesMessageOut']['ns0:Body']['ns0:shop'], function( key, value )
        {
          $('#selShops').append('<option value="'+value['ns0:id']+'">'+value['ns0:name']+'</option>').trigger('refresh');
        });
      
        // order options
        var options = $("#selShops option");         
        options.detach().sort(function(a,b) {
            var at = $(a).text();
            var bt = $(b).text();         
            return (at > bt) ? 1 : ((at < bt) ? -1 : 0);
        });
        options.appendTo("#selShops");
        $('#selShops').val('0').trigger('refresh');
      } else {
        modalError(messages.error_getShop);
      }
      
    },
    error: function(xhr, ajaxOptions, thrownError)
    {
      modalError(messages.error_getShop);
      //console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
    }
  });
}

function getHour(index)
{
  var out = config.startHour + index;
  if (out < 10) out = '0' + out;
  out += ':00'; 
  return out;
}

function getDateFormat(date,format)
{
  var day = date.getDate();
  if (day < 10 && format.indexOf('dd'))
    day = '0' + day;
  var month = date.getMonth() + 1;
  if (month < 10 && format.indexOf('mm'))
    month = '0' + month;
  var year = date.getFullYear();
  
  format = format.replace('yyyy',year);  
  format = format.replace('m',month);
  format = format.replace('d',day);
  
  format = format.replace(/m|d|y/g, '');  
  return format;
}

function getMinHour(hour)
{
  var min = 1000;
  for (var i = 1; i < hour.length; i++)
  {
    if (hour[i].start < min)
      min = hour[i].start; 
  }

  return Math.ceil(min / 100);  
}

function getMaxHour(hour)
{
  var max = 1600;
  for (var i = 1; i < hour.length; i++)
  {
    if (hour[i].end > max)
      max = hour[i].end; 
  }

  return Math.floor(max / 100);  
}

function getOpeningHours(shopId)
{


}

function modalError(text)
{
  $('#myModal .modal-body div').html(text);
  $('#myModal').modal('show');
}