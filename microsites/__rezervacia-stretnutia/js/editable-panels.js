$(document).ready(function(){
	$(".panel.editable .panel-action-link").not(".cfe-text-disabled").click(function(e){
		e.preventDefault();
		showEditPanels($(this).parents(".panel"));
	});
 $(".panel.editable .btn[data-cancel-edit=true]").click(function(e){
	 e.preventDefault();
	 hideEditPanels($(this).parents(".panel"));
 });
});

function showEditPanels(panel){  	  
	  panel.find(".panel-heading [data-edit-mode=false]").fadeOut(300, function(){
	   panel.find(".panel-heading [data-edit-mode=true]").fadeIn(300);
	  });
	  panel.find("[data-edit-mode=false]").slideUp(300, function(){
	   panel.find("[data-edit-mode=true]").slideDown(300);
	  });
}

function hideEditPanels(panel){	  
	  panel.find(".panel-heading [data-edit-mode=true]").fadeOut(300, function(){
	   panel.find(".panel-heading [data-edit-mode=false]").fadeIn(300);
	  });
	  panel.find("[data-edit-mode=true]").slideUp(300, function(){
	   panel.find("[data-edit-mode=false]").slideDown(300);
	  });
}

