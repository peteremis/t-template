$('.button-number').click(function(){
  var val = $('#pin_val').val();
  
  if ($(this).attr('data-action') == 'add')
  {
    val += $(this).children('span').text();
    $('#pin_val').val(val);
  }
  else if ($(this).attr('data-action') == 'del' && val.length > 0)
  {
    val = val.substr(0,(val.length -1));
    $('#pin_val').val(val);
  }
  else if ($(this).attr('data-action') == 'send' && val.length > 0)
  {
    if (val.length < 4) return;

    $('#myModal .modal-footer').hide();
    $('#myModal .modal-body p').text('').hide();
    $('#myModal .timer').show();

    var urlAjax = '/pb/onlineshop/checkappointment/' + val;
    var returnStatus = 0;
    $.ajax({
        url: urlAjax,
        type: 'GET',
        dataType: 'json',
        beforeSend: function()
        {
          $('#myModal').modal('show');
        },
        complete: function()
        {
  		    $('#pin_val').val('');
          
          //if (returnStatus > 3 || returnStatus == 0) returnStatus = 1;
          
          var message;
          if (returnStatus == 1)
            message = 'Vaša požiadavka bola zaregistrovaná, prosím zoberte si poradový lístok. Predajca sa Vám bude onedlho venovať.';
          else if (returnStatus == 2)
            message = 'Vážený zákazník, ospravedlňujeme sa, ale nepodarilo sa nám nájsť stretnutie pre Vami zadaný rezervačný kód.<br />Prosím zoberte si poradový lístok.';
          else if (returnStatus == 3)
            message = 'Vážený zákazník, ospravedlňujeme sa, rezervácia je platná 10 minút pred a 20 minút po čase dohodnutého stretnutia.<br />Prosím zoberte si poradový lístok.';
          else
            message = 'Vážený zákazník, ospravedlňujeme sa, žial rezervačný systém aktuálne nie je k dispozícii. Zadajte rezervačný kód ešte raz, alebo si zoberte poradový lístok. Ďakujem za pochopenie';                        
            
          $('#myModal .timer').hide();
          $('#myModal .modal-footer').show();
          $('#myModal .modal-body p').html(message).show();

        },
        success: function(json)
        {
          console.log(json);
          if (typeof json['ns0:checkAppointmentMessageOut'] !== 'undefined')
          {
            returnStatus = 1;
          }
          else if (typeof json['ns:checkAppointmentFaultMessage'] !== 'undefined')
          {
            var message = json['ns:checkAppointmentFaultMessage']['ns1:Body']['ns1:message'];
            if (message == 'Appointment not found')
              returnStatus = 2;
            else if (message == 'Appointment expired')
              returnStatus = 3;
            else
              returnStatus = 4;
          }
          else
            returnStatus = 5;            
        },
        error: function(xhr, ajaxOptions, thrownError)
        {
          returnStatus = 6;
          console.log(thrownError);
          console.log(xhr.statusText);
          console.log(xhr.responseText);
        }
    }); 
  }
});