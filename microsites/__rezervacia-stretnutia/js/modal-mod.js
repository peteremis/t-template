/* ===== MODAL TWEAK FOR BS BLUR CODED BY JESUS HIMSELF ===== */

$(document).ready(function(){
	$(".modal").each(function(){
		modalToBody($(this));
	});
});

function modalToBody($modal) {
	$("body").append($modal.clone());
	$modal.remove();

	// SELECTRIC FOR SELECTS AFTERLOAD FIX
	$("select.form-control", $modal).selectric();
}