$(function () {
    $('.tooltip-1').qtip({
        content: {
            text: 'Služba sa aktivuje spárovaním tabletu s pevnou linkou. Tablet tak bude zvoniť súčasne s pevnou linkou a hovor môžete prijať, aj keď ste mimo domova.<br/>' +
                'Pre aktiváciu služby nás kontaktujte na zákazníckej linke 0800 123 456 alebo na predajných miestach Slovak Telekomu.'
        },
        position: {
            my: 'bottom center',
            at: 'top center'
        }
    });

    $('.tooltip-2').qtip({
        content: {
            text: 'K Easy karte sa dá dokúpiť dátový balík – SURF 1000 s 1 GB dát za 5,99 €. Platnosť balík je max. 30 dní od jeho aktivácie alebo do vyčerpania dát.'
        },
        position: {
            my: 'bottom center',
            at: 'top center'
        }
    });

    $('.tooltip-3').qtip({
        content: {
            text: 'Externá batéria 2600 mAh s výstupom DC5V/1A slúži ako dobijáčka vášho mobilu či tabletu aj tam, kde nie je dostupná elektrická zásuvka.'
        },
        position: {
            my: 'bottom center',
            at: 'top center'
        }
    });

    $('.tooltip-4').qtip({
        content: {
            text: 'HUAWEI MEDIAPAD T1 8.0 PRO<br/>' +
                '■ očarujúci elegantný dizajn<br/>' +
                '■ 8“ IPS HD displej MediaPad T1 8.0 s rozlíšením 1280 x 800<br/>' +
                '■ Verne zobrazené živé farby<br/>' +
                '■ 4 jadrový procesor, 1,2 GHz procesor Qualcomm<br/>' +
                '■ OS Android 4.3 Jelly Bean<br/>' +
                '■ Vysokokapacitná batéria 4800 mAh<br/>' +
                '■ 4G LTE technológia'
        },
        position: {
            my: 'right center',
            at: 'left center'
        }
    });
    
    $(".scroll-to").click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });
});