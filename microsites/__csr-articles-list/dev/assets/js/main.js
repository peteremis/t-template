function selectCategory(categ, data) {
  if (categ != "") {
    var articles = [];

    var i = 0;
    data.forEach(function (itemData) {
      if (itemData.categoryId === categ) {
        articles[i] = itemData;
        i++;
      }
    });
  } else {
    articles = data;
  }
  return articles;
}

function fetchArticles(myUrl) {
  return new Promise(function (resolve) {
    $.ajax({
      url: myUrl,
      cache: false,
      async: true,
      crossDomain: true,
      type: "GET",
      dataType: "json",
      success: function (data) {
        resolve(data);
      },
    });
  });
}

function htmlArticles(number, myObject) {
  var htmlContent = "";
  if (number > myObject.length) {
    a = myObject.length;
  } else {
    a = number;
  }
  for (i = 0; i < a; i++) {
    htmlContent =
      htmlContent +
      "<div class='blog col-sm-12 col-md-6 col-lg-4'><a href='" +
      myObject[i].url +
      "'>" +
      "<div class='blog_item' style='background-image: url(" +
      myObject[i].imageUrl +
      ");'>" +
      "<div class='blog_item__category'>" +
      "<div class='router-link-exact-active is-active'>" +
      myObject[i].categoryName +
      "</div>" +
      "</div>" +
      "<div class='blog_item__details'>" +
      "<h3 class='blog_title'>" +
      myObject[i].title +
      "</h3>" +
      "<div class='blog_meta'>" +
      "<p class='blog_meta__author'>" +
      myObject[i].author +
      "</p>" +
      "<p class='blog_meta__divider'>■</p>" +
      "<p class='blog_meta__date'>" +
      myObject[i].date +
      "</p>" +
      "<p class='blog'>" +
      "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA4AAAAOCAYAAAAfSC3RAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTM4IDc5LjE1OTgyNCwgMjAxNi8wOS8xNC0wMTowOTowMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTcgKFdpbmRvd3MpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjNFMzIwMTU5MzE2MzExRTc5NTdBRDhCRUE4NDY4MEQzIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjNFMzIwMTVBMzE2MzExRTc5NTdBRDhCRUE4NDY4MEQzIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6M0UzMjAxNTczMTYzMTFFNzk1N0FEOEJFQTg0NjgwRDMiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6M0UzMjAxNTgzMTYzMTFFNzk1N0FEOEJFQTg0NjgwRDMiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz5vRQBnAAAAn0lEQVR42mL8//8/AzmACY+cJhDbkKNRFOiaVUDaB6ssyKm48L9//0KB+BGQ7Y8uh1cjFIdANfuSqhGEQTY/BNJ+pGoEORtk810g25skjVBcDNT8EYi1SdGkCdRwCYinA9mcxGrShjpzNhAzE+tUkE13gRikiYnYwAFpugPEs4BsRmKjQxcUBVBNLOjy+JIcPxAvZmRkTAPSf9AlAQIMAGJru4n56GMSAAAAAElFTkSuQmCC'/>" +
      "</p></div></div></div></a></div>";
  }
  return htmlContent;
}
var postsData = [];
fetchArticles(myJsonUrl).then(function (myData) {
  postsData = myData;
});

$(document).ready(function () {
  setTimeout(function () {
    $("#posts-contents").html(
      htmlArticles(myNumber, selectCategory(myCategory, postsData))
    );
  }, 1000);
});
