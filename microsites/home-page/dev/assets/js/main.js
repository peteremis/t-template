$(function() {
    $('.tabs-menu a').click(function (event) {
        event.preventDefault();
        if (!$(this).parent().hasClass('current') && !$(this).parent().hasClass('isVariant')) {
            toggleTabs();
        }

        $(this).parent().addClass('current');
        $(this).parent().siblings().removeClass('current');
        var tab = $(this).attr('href'),
        parent = $(this).closest('ul').parent().parent();
        parent.find('.tab-content').not(tab).css('display', 'none');
        $(tab).fadeIn();
    });

    var toggleTabs = function () {
        $('.tab__variant').toggleClass('selected');
    };

    /* FROM TABS TO ACCORDION */
    var $accordion = $('.accordion'),
    $heads = $('.tabs-menu a'),
    $contents = $('.tab-content'),
    itemCount = $($heads).length - 1,
    accordionCode = '<div class="item">';

    for (let i = 0; i <= itemCount; i++) {
        accordionCode += '<div class="heading">' + $($heads[i]).text() + '</div>';
        accordionCode += '<div class="content">' + $($contents[i]).html() + '</div></div>';

        if (i !== itemCount) {
            accordionCode += '<div class="item">';
        }
    }

    $(accordionCode).appendTo($accordion);

    var $items = $('.accordion .item');

    //SET OPENED ITEM
    $($items[0]).addClass('open');

    /* ACCORDION */

    $('.accordion .item .heading').click(function(e) {

        var clickedHead = e.target;
        var $item = $(clickedHead).closest('.item');
        var isOpen = $item.hasClass('open');
        var $content = $item.find('.content');

        if(isOpen) {
            $content.slideUp(200);
            $item.removeClass('open');
        } else {
            $content.slideDown(200);
            $item.addClass('open');
        }

    });

    $('.accordion .item.open').find('.content').slideDown(200);

    /* COPY TILES */

    var $descTiles = $('#desc-tiles'),
        $mobTiles = $('#mob-tiles'),
        descTiles = $descTiles.html();

        $mobTiles.html(descTiles);
});
