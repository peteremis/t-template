$(function() {

    var defaultLabelText = 'Zadajte svoje telefónne číslo:',
    defaultInputPlaceholder = '09 _ _ _ _ _ _ _ _',
    smsVerifLabelText = 'Overovací kód z SMS',
    smsVerifInputPlaceholder = '_ _ _ _',
    currentFormAction = '',
    msisdn = '',
    token = '',
    validatedAs = '',
    employeeText = 'Nárok na nový služobný telefón si môžeš overiť na intranete alebo v mobilnej aplikácii Telekom.',
    notAllowedText = 'Ľutujeme, aktuálne nemáte nárok na nový mobil.',
    testEnv = false,
    $div2blink = $(".box__mkr");

    var $progressCubes = $('#ajaxLoadingSpinnerOverlay'),
    $errorHolders = $('.mkr__error');

    function blinkBg() {
        $div2blink.css("border-color", "#d0d0d0").delay(400).animate({ borderColor: "#e20074"}, 200).animate({ borderColor: "#d0d0d0"}, 1500);
    }

    function showLoader() {
        $progressCubes.show();
    }

    function checkEnv() {
        if (window.location.hostname === 'localhost') {
            testEnv = true;
        }
    }

    function hideLoader() {
        $progressCubes.hide();
    }

    function getInputVal(clickedBtn) {
        return $(clickedBtn).siblings('.mkr__input').val();
    }

    function showErrorMsg(clickedBtn, msgText) {
        var $errorHolder = $(clickedBtn).siblings('.mkr__error');
        $errorHolder.text(msgText);

        $errorHolder.show(500).delay(3000).fadeOut(function() {
            clearErrorMsgHolders();
        });
    }

    function clearErrorMsgHolders() {
        $errorHolders.text('');
    }

    function changeInputPlaceholder(clickedBtn, text) {
        var $input = $(clickedBtn).siblings('.mkr__input');
        $input.attr('placeholder', text).val('').focus().blur();
    }

    function changeInputLabel(clickedBtn, text) {
        var $label = $(clickedBtn).siblings('.input-label');
        $label.text(text);
    }

    function changeFormComponents(clickedBtn, labelText, placeholderText) {
        changeInputLabel(clickedBtn, labelText);
        changeInputPlaceholder(clickedBtn, placeholderText);
    }

    function changeFormToSmsVerif(clickedBtn) {
        changeFormComponents(clickedBtn, smsVerifLabelText, smsVerifInputPlaceholder);
    }

    function changeFormToDefault(clickedBtn) {
        changeFormComponents(clickedBtn, defaultLabelText, defaultInputPlaceholder);
    }

    function setBoxDataParam(clickedBtn, param) {
        var $box = $(clickedBtn).closest('.box__mkr');
        $box.data('action', param);
    }

    function getBoxDataParam(clickedBtn) {
        var $box = $(clickedBtn).closest('.box__mkr');
        return $box.data('action');
    }

    function getValidatedAsValue(prolongType) {
        switch (prolongType) {
            case 'prepaid':
            return '$easyProlong'
            break;
            case 'other_operator':
            return '$numberTransfer'
            break;
            case 'over_prolong':
            return '$standardProlong'
            break;
            case 'prepaid':
            return '$easyProlong'
            break;
            case 'wrong_segment':
            return '$wrongSegment'
            break;
            case 'nonhw':
            return '$standardProlongSoftNonHw'
            break;
            case 'early_prolong':
            return '$standardProlongSoft'
            break;
            case 'happy_looners':
            return '$happyLooners'
            break;
            case 'ok':
            return '$mobilEveryYear'
            break;
        }
    }

    function disableBtn(clickedBtn) {
        var $clickedBtn = $(clickedBtn);
        if (!$clickedBtn.hasClass('btn_disabled')) {
            $clickedBtn.addClass('btn_disabled');
        }
    }

    function enableBtn(clickedBtn) {
        var $clickedBtn = $(clickedBtn);
        if ($clickedBtn.hasClass('btn_disabled')) {
            $clickedBtn.removeClass('btn_disabled');
        }
    }

    function init() {
        checkEnv();

        if (!testEnv) {
            validateInput();
        } else {
            enableBtn($('.mkr__submit'));
        }
        clearErrorMsgHolders();
    }

    function validateInput() {
        $('.mkr__input').on('keyup', function(e) {
            var inputValue = e.target.value,
            input = e.target,
            $siblingBtn = $(input).siblings('.mkr__submit');
            currentFormAction = getBoxDataParam(e.target);

            if (currentFormAction === 'smsVerif') {
                if (inputValue.match(/^\d{4}$/)) {
                    enableBtn($siblingBtn);
                } else {
                    disableBtn($siblingBtn);
                }
            } else {
                if (inputValue.match(/^\d{10}$/)) {
                    enableBtn($siblingBtn);
                } else {
                    disableBtn($siblingBtn);
                }
            }

        });
    }

    $('.mkr__submit').on('click', function(e) {
        currentFormAction = getBoxDataParam(e.target);

        if ($(e.target).hasClass('btn_disabled')) {
            return false;
        }

        if (currentFormAction === 'default' || currentFormAction === undefined) {
            defaultFormAction(e)
        } else if (currentFormAction === 'smsVerif') {
            smsVerifFormAction(e);
        }
    });

    function defaultFormAction(e) {
        var clickedTarget = e.target;

        e.preventDefault();
        showLoader();

        msisdn = getInputVal(clickedTarget);

        $.ajax({
            type: "GET",
            async: true,
            crossDomain: true,
            dataType: "json",
            data: "msisdn=" + msisdn,
            url: 'https://m.telekom.sk/vykup/api/check-mobile2',

            success: function (d) {
                hideLoader();
                if (d.response === 'ok') {
                    token = d.token;
                    setBoxDataParam(clickedTarget, 'smsVerif');
                    changeFormToSmsVerif(clickedTarget);
                    blinkBg();
                    disableBtn(clickedTarget);
                }
            },
            error: function() {
                hideLoader();
                showErrorMsg(clickedTarget, 'Momentálne nie je možné overiť mobilné číslo. Skúste to prosím neskôr.');
            }
        });
    }

    function smsVerifFormAction(e) {
        var clickedTarget = e.target,
        code = getInputVal(clickedTarget);

        e.preventDefault();
        showLoader();

        $.ajax({
            type: "GET",
            async: true,
            crossDomain: true,
            dataType: "json",
            data: "msisdn=" + msisdn + "&token=" + token + "&code=" + code,
            url: 'https://m.telekom.sk/vykup/api/check-code2',

            success: function (d) {

                var responseCode = d.code;

                validatedAs = getValidatedAsValue(responseCode);

                hideLoader();

                switch (responseCode) {
                    case 'telekom_emp':
                    showErrorMsg(clickedTarget, employeeText);
                    changeFormToDefault(clickedTarget);
                    setBoxDataParam(clickedTarget, 'default');
                    break;
                    case 'no_prolong':
                    showErrorMsg(clickedTarget, notAllowedText);
                    changeFormToDefault(clickedTarget);
                    setBoxDataParam(clickedTarget, 'default');
                    break;
                    default:
                    localStorage.setItem('msisdn', msisdn);
                    localStorage.setItem('token', token);
                    localStorage.setItem('sale', d.sale);
                    localStorage.setItem('prolongType', d.code);
                    localStorage.setItem('validatedAs', validatedAs);
                    window.location.href = "https://www.telekom.sk/mobilkazdyrok/";
                }

            },
            error: function() {
                hideLoader();
            }
        });
    }

    init();
});
