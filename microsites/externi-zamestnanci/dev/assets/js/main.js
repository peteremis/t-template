var loopCounter = 0;
$('script').load(function () {
    loopCounter++;
    if (loopCounter <= 1) {
        var lastInput = $('.last-input').closest('li');
        lastInput.addClass('last-input-wrapper');

        var lastLabel = $('.last-label').closest('li');
        lastLabel.addClass('last-label-wrapper');
        
        var lastLabel2 = $('.last-label-2').closest('li');
        lastLabel2.addClass('last-label-wrapper');

        var firstLabelParent = $('.first-label').closest('li');
        var colsHTML = '<div class="form-col-wrapper"><div class="form-col-6"></div><div class="form-col-6"></div></div>';
        $(colsHTML).insertBefore(firstLabelParent);

        var firstHalf = $('.form-col-left').closest('li');
        $(firstHalf).appendTo($('.form-col-6')[0]);

        var secondtHalf = $('.form-col-right').closest('li');
        $(secondtHalf).appendTo($('.form-col-6')[1]);


        var labelMove = $('.label-move'),
            formField = null,
            labelMoveTotal = labelMove.length;


        for (var i = 0; i < labelMoveTotal; i++) {
            formField = labelMove[i].closest('.formFieldRenderField');
            $(labelMove[i]).appendTo(formField);
        }
    }
});