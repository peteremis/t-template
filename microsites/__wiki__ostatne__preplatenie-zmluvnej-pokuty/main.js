$(function () {

  var $pokuta = $('.pokuta-file input');
  var $pokutaError = $('.pokuta-file .formFieldValidationResult');
  var $formMain = $('form.dynamicFormMainForm');
  var $button = $('.tlacidlo_odoslat');
  var $file = $('.fileUploadedItem');

  var validation = '<span class="error error-file">Toto pole je povinné.</span>';
  $pokutaError.append(validation);

  if ($file.length) {
    console.log('exists');
    $pokutaError.hide();
  } else {
    $button.prop("disabled", true);
  }

  $pokuta.prop('required', true);

  $button.click(function (e) {
    e.preventDefault();

    // do ajax now
    console.log("submitted");
  });

  $('.pokuta-file').on('change', 'input', function () {
    console.log("upload");
    $pokutaError.hide();
    $button.prop("disabled", false);
  });

});