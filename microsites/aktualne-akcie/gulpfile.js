var gulp            = require('gulp'),
    sass            = require('gulp-sass'),
    sourcemaps      = require('gulp-sourcemaps'),
    uglify          = require('gulp-uglify'),
    browserSync     = require('browser-sync').create(),
    reload          = browserSync.reload,
    includer        = require('gulp-html-ssi'),
    runSequence     = require('run-sequence'),
    es              = require('event-stream'),
    replace         = require('gulp-replace');
    globalTemplates = '../../global-assets/templates/*.html',
    localTemplates  = 'dev/templates/*.html';


gulp.task('htmlSSI', function() {
       es.merge(gulp.src(globalTemplates), gulp.src(localTemplates))
        .pipe(includer())
        .pipe(gulp.dest('dev/server/'));
});


gulp.task('sass', function () {
    return gulp.src('dev/assets/sass/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass.sync().on('error', sass.logError))
        .pipe(sass({outputStyle: 'compact'}))
        .pipe(sourcemaps.write('/maps'))
        .pipe(gulp.dest('dev/server/assets/css'));
});

gulp.task('js', function () {
    return gulp.src('dev/assets/js/*.js')
        .on('error', console.error.bind(console))
        .pipe(gulp.dest('dev/server/assets/js'));
});

gulp.task('dist-replace-path-html', function(){
    gulp.src(['dev/templates/_main-content.html'])
    .pipe(replace('../assets/img/', '/documents/10179/669910/'))
    .pipe(replace('../assets/js/', '/documents/10179/669908/'))
    .pipe(gulp.dest('deploy'));
});

gulp.task('dist-replace-path-css', function(){  
  gulp.src(['dev/server/assets/css/*.css'])
    .pipe(replace('../img/', '/documents/10179/669910/'))
    .pipe(gulp.dest('deploy'));
});

gulp.task('dist-replace-path-js', function(){
  gulp.src(['dev/server/assets/js/*.js'])
    .pipe(replace('../img/', '/documents/10179/669910/'))
    .pipe(gulp.dest('deploy'));
});

gulp.task('dist-replace', ['dist-replace-path-html', 'dist-replace-path-js', 'dist-replace-path-css']);

gulp.task('serve', function () {
    
    browserSync.init({
        server: {
            baseDir: ["./dev", "./dev/server", "./../../global-assets"]
        }
    });
    
    gulp.watch('dev/assets/sass/**', ['sass']);
    gulp.watch('dev/assets/sass/**').on('change', reload);
    gulp.watch('../../global-assets/sass/**', ['sass']);
    gulp.watch('../../global-assets/sass/**').on('change', reload);
    gulp.watch('dev/assets/js/*.js', ['js']);
    gulp.watch('dev/assets/js/*.js').on('change', reload);
    gulp.watch('dev/templates/*.html', ['htmlSSI']);
    gulp.watch('dev/templates/*.html').on('change', reload);
    gulp.src('dev/assets/img/**')
    .pipe(gulp.dest('dev/server/assets/img'));
});

gulp.task('prepare-deploy', ['sass', 'js', 'htmlSSI'], function() {
    return gulp.src('dev/templates/_main-content.html')
    .pipe(gulp.dest('deploy'));
    gulp.src('dev/assets/img/**')
    .pipe(gulp.dest('deploy/img'));
    gulp.src('dev/server/assets/js/main.js')
    .pipe(gulp.dest('deploy'));
    gulp.src('dev/server/assets/css/style.css')
    .pipe(gulp.dest('deploy'));
});

gulp.task('deploy', function(callback) {
    runSequence('prepare-deploy',
                'dist-replace',
                 callback);
});

gulp.task('default', function(callback) {
    runSequence(['sass', 'js', 'htmlSSI'],
                'serve',
                 callback);
});