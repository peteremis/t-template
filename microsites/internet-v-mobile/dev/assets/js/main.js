$(function () {

    $('#neobmedzene-data .zavri').click(function(){
        $.fancybox.close();
        return false;
    });

    $(".lightbox").fancybox({
        // autoDimensions: true
        'width'  : 430,
        'height'  : 460,
        'autoSize': false
    });
    $(".fancybox").fancybox({
        parent: "#content"
    });

    $('#magenta1-select').on('change', function() {
        if (this.value==1) {
            $('#magenta1-1').show();
            $('#magenta1-0').hide();
        } else {
            $('#magenta1-0').show();
            $('#magenta1-1').hide();
        }

    });

    $('#zasady-tooltip').qtip({
        content: {
            text: 'Vďaka Zásadám férového používania (anglická skratka FUP) zabezpečujeme rovnakú kvalitu ' +
            'všetkým zákazníkom a predchádzame tomu, aby jeden zákazník čerpal nadmerne dáta na úkor ostatných. <br>' +
            'Preto sú naše neobmedzené dáta v objeme 100 GB. ' +
            'Po ich prečerpaní neplatíte nič navyše, ale prenos dát sa spomalí.'
        },
        position: {
            my: 'left center',
            at: 'right center'
        },
        style: {
            classes: 'tooltip tooltip-left'
        }
    });


    $("#nav-sticky").sticky({
        topSpacing: 0,
        responsiveWidth: false,
        widthFromWrapper: false
    });

    $(".scroll-to").click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top - ($("#nav-sticky").outerHeight())
                }, 1000);
                return false;
            }
        }
    });

    var contentSections = $("[id*='sec-']"),
        secondaryNav = $("#nav-sticky");

    function updateSecondaryNavigation() {
        contentSections.each(function () {
            var actual = $(this),
                actualHeight = actual.height() + parseInt(actual.css('paddingTop').replace('px', '')) + parseInt(actual.css('paddingBottom').replace('px', '')),
                actualAnchor = secondaryNav.find('a[href="#' + actual.attr('id') + '"]');

            if ((actual.offset().top - secondaryNav.outerHeight() <= $(window).scrollTop()) && (actual.offset().top + actualHeight - secondaryNav.outerHeight() > $(window).scrollTop())) {
                actualAnchor.addClass('active');
            } else {
                actualAnchor.removeClass('active');
            }

        });
    }

    updateSecondaryNavigation();

    $(window).scroll(function (event) {
        updateSecondaryNavigation();
    });


    $(".fancy").fancybox({});

});