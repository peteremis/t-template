'use strict';

angular.
module('deviceList').
component('deviceList', {
    templateUrl: '../assets/app/device-list.template.html',
    controller: ['PhoneService', 'deviceService', '$filter', '$resource', 'ModalService', '$location', '$timeout',
        function PhoneListController(PhoneService, deviceService, $filter, $resource, ModalService, $location, $timeout) {
            var self = this;

            //vybrate brandy
            self.deviceFilterBrand = [];
            self.deviceFilterScreen = [];
            self.zoznamBrandov = [{ name: "", maker: "", ticked: false }];
            self.screenSize = {};
            //sort Filters menu
            self.zakladnyFilter = [
                { name: "Abecedy: od A po Z", maker: "", ticked: false, value: "nameAZ" },
                { name: "Abecedy: od Z po A", maker: "", ticked: false, value: "nameZA" },
                { name: "Obľúbenosti", maker: "", ticked: false, value: "order" },
                { name: "Ceny: od najlacnejšieho", maker: "", ticked: false, value: "lowest" },
                { name: "Ceny: od najdrahšieho", maker: "", ticked: false, value: "highest" }
            ];
            //keep show all on first/zero position
            self.zoznamScreenov = [
                { name: "Všetky", maker: "", hideTag: true, ticked: true, min: 0, max: 20 },
                { name: "3,9\" a menej", maker: "", hideTag: false, ticked: false, min: 0, max: 3.9 },
                { name: "4\" až 4,4\"", maker: "", hideTag: false, ticked: false, min: 4, max: 4.4 },
                { name: "4,5\" až 4,9\"", maker: "", hideTag: false, ticked: false, min: 4.5, max: 4.9 },
                { name: "5\" až 5,4\"", maker: "", hideTag: false, ticked: false, min: 5, max: 5.4 },
                { name: "5,5\" a viac", maker: "", hideTag: false, ticked: false, min: 5.5, max: 10 }
            ];
            self.outputBrands = [];
            self.outputScreenSize = [];
            self.outputBaseFilter = [];
            //init filter setup
            self.orderProp = 'order';
            self.reverse = false;
            // helper for result count
            self.filterResult = [];
            self.tabletAll = [];
            self.devices = [];

            /******** LOAD DEVICES:start ********/
                
                /* Load Phones */
                self.loadPhones = function() {
                    self.devices = PhoneService.query();
                     console.log('phone', self.devices);
                };
                
                /* Load Tablets */
                // self.loadTablets = function() {
                   
                // };
                 deviceService.getAllTablets().then(function(device) {
                         self.tabletAll = device;
                         // self.devices = device;
                         console.log('tablety', self.tabletAll);
                         console.log('tablety', self.devices);
                         // $timeout(function() {
                         //     self.scrollTo();
                         // });
                     });

                //switch tabs load phone or tablet
                self.loadData = '';
                self.currentTab = [true, false];
                self.listVisible = [true,false];

                self.loadDevices = function(deviceType, tabId) {
                    //show active tab
                    switch (tabId) {
                        case 'tab1':
                            self.currentTab = [true, false];
                            self.listVisible = [true,false];
                            $timeout(function() {
                                 self.scrollToTag("#tab__row");
                             });
                            break;
                        case 'tab2':
                            self.currentTab = [false, true];
                            self.listVisible = [false,true]; 
                            $timeout(function() {
                                self.scrollToTag("#tab__row");
                             });
                            break
                            
                    };
                    //load device by type
                    switch (deviceType) {
                        case 'phone':
                            self.loadData = 'load phones';
                            break;
                        case 'tablet':
                            self.loadData = 'load tablets';
                            break;
                    }
                };

            /******** LOAD DEVICES:end ********/

            //scrollTo from url hash
                self.scrollTo = function() {
                    var $target = $('#' + $location.$$hash);
                    if ($target.length > 0) {
                        var scrollPos = $target.offset().top;
                        $("body,html").animate({ scrollTop: scrollPos }, "slow");
                    }
                };

            //scrol to tag section
                self.scrollToTag = function(tagId) {
                    var $target = $(tagId);
                    var scrollPos = $target.offset().top;
                    $("body,html").animate({ scrollTop: scrollPos }, "slow");
                };

            /******** FILTERS & SORTING:start ********/
                // set first filter
                self.selected = self.zakladnyFilter[0];
                self.defaultFilter = self.zakladnyFilter[0];
                self.filterUnavailable = function(device) {
                    // console.log(device);
                    return device.available == true;
                };

                //create list arr for dropdown
                self.createObjForList = function(list, listName) {
                    switch (list) {
                        case 'brand':
                            self.zoznamBrandov.push({
                                name: listName,
                                value: listName,
                                ticked: false
                            });
                            break;
                        case 'screenSize':
                            self.zoznamScreenov.push({
                                name: listName + "\"",
                                value: listName,
                                ticked: false
                            });
                            break;
                    }
                };

                //filter all mobile brands
                PhoneService.query().$promise.then(function(items) {
                    var output = [];
                    var keys = [];
                    //clean list
                    self.zoznamBrandov = [];

                    angular.forEach(items, function(item) {
                        var key = item["brand"];
                        if (keys.indexOf(key) === -1) {
                            keys.push(key);
                            output.push(item["brand"]);
                            self.createObjForList("brand", key);
                        }
                    });

                    //console.log('All brands list:', self.allBrands);
                    // sort arr by AZ (inputData, filterName)
                    self.changeSortOrder(self.zoznamBrandov, 'AZ');
                    //console.log('zoznam brands:', self.zoznamBrandov);
                });

                // filter for Brand DropDown & CheckBox
                self.checkBrandChange = function(item) {
                    var i = $.inArray(item.name, self.deviceFilterBrand);
                    if (i > -1) {
                        self.deviceFilterBrand.splice(i, 1);
                        item.ticked = false;
                    } else {
                        self.deviceFilterBrand.push(item.name);
                    }
                    //console.log('filtrovat', self.deviceFilterBrand);
                };
                // Filter parameter 
                self.brandFilter = function(device) {
                    if (self.deviceFilterBrand.length > 0) {
                        if ($.inArray(device.brand, self.deviceFilterBrand) < 0)
                            return;
                    }
                    return device;
                };

                // filter for ScreenSize DropDown & CheckBox
                self.checkScreenChange = function(item) {
                    self.screenSize = {
                        min: item.min,
                        max: item.max
                    };
                    //console.log('filtrovat', item);
                };

                // untick and reset screensize filter
                self.untick = function untick($index, item) {
                    //console.log('tick', item, "\n", $index);
                    self.resetScreeSize(0, 10);
                    item.ticked = false;
                };

                // change device list order basic filter
                self.changeOrder = function(item, mobileCheck) {

                    if (mobileCheck === 'mobile') { self.uncheckOther(item); }

                    switch (item.value) {
                        case 'nameAZ':
                            self.orderProp = 'name';
                            self.reverse = false;
                            break;
                        case 'nameZA':
                            self.orderProp = 'name';
                            self.reverse = true;
                            break;
                        case 'lowest':
                            self.orderProp = 'price.easy';
                            self.reverse = false;
                            break;
                        case 'highest':
                            self.orderProp = 'price.easy';
                            self.reverse = true;
                            break;
                        case 'order':
                            self.orderProp = 'age';
                            self.reverse = false;
                            break;
                    }
                };

                //sort menu item by Az etc...
                self.changeSortOrder = function(dataToSort, sortType) {
                    if (sortType == "AZ") {
                        dataToSort.sort(function(a, b) {
                            if (a.value < b.value) return -1;
                            if (a.value > b.value) return 1;
                            return 0;
                        });
                    }
                };

                // set screenSize lrange
                self.resetScreeSize = function(min, max) {
                    self.screenSize = {
                        min: parseInt(min),
                        max: parseInt(max)
                    };
                };

            /******** //FILTERS:end ********/
            
            /******** MOBILE-FILTER:start ********/
                
                //show hide mobile filter tab
                self.IsHiddenTab1 = true;
                self.IsHiddenTab2 = true;
                self.ShowHideTab1 = function(final) {
                    //If DIV is hidden it will be visible and vice versa.
                    self.IsHiddenTab1 = self.IsHiddenTab1 ? false : true;

                    if (!self.IsHiddenTab2) {
                        self.IsHiddenTab2 = true;
                    };
                    if (final) {
                        self.scrollToTag('#phone-list');
                    }
                };
                self.ShowHideTab2 = function(final) {
                    //If DIV is hidden it will be visible and vice versa.
                    self.IsHiddenTab2 = self.IsHiddenTab2 ? false : true;
                    if (!self.IsHiddenTab1) {
                        self.IsHiddenTab1 = true;
                    }
                    if (final) {
                        self.scrollToTag('#phone-list');
                    }
                };

                //divide checkboxes to columns
                self.getLimits = function(array) {
                    var arr = [
                        Math.floor(array.length / 2) + 1, -Math.floor(array.length / 2)
                    ];
                    return arr
                };
                self.getLimit2 = function(array) {
                    var arr = [
                        Math.floor(array.length / 2), -Math.floor(array.length / 2)
                    ];
                    return arr
                };

                //mobile filter checkbox like radio btn
                self.uncheckOther = function(item) {
                    //console.log('item', item);
                    var i;
                    // first, set everything to false
                    for (i = 0; i < self.zakladnyFilter.length; i++) {
                        self.zakladnyFilter[i].ticked = false;
                    }
                    // then set the clicked item to true
                    item.ticked = true;
                    //console.log('zakl filter', self.zakladnyFilter);
                };
                //uncheck other checkboxes
                self.updateSelection = function(screenSizeName, itemsArr) {
                    self.count = 0;
                    angular.forEach(itemsArr, function(screenSize, index) {
                        if (screenSizeName != screenSize.name) {
                            screenSize.ticked = false;
                        };
                        if (screenSize.ticked == false) {
                            self.count = self.count + 1;
                        }
                    });
                    //if all unchecked check showall
                    if (self.count == 6) {
                        // item[0] is show all
                        itemsArr[0].ticked = true;
                        self.checkScreenChange(itemsArr[0]);
                    }
                };

            /******** MOBILE-FILTER:end ********/

            /******** jQuery fix ********/
                //close modal
                self.pclose = function(data) {
                    $.fancybox.close();
                    //console.log(':::popup close:::');
                };
                //qTip
                self.qtip = function() {
                    $('.info').qtip({
                        content: 'Bezplatné volania získate na 3 čísla, ktoré si sami zvolíte. Čísla môžu byť v akejkoľvek mobilnej sieti v SR.'
                    });
                };

            //run at first-load
            self.init = function() {
                self.changeOrder(self.defaultFilter);
                self.resetScreeSize(0, 10);
                self.loadPhones();
                // self.loadTablets();
            };

            // runs once per controller instantiation
            self.init();

        }
    ]
});
