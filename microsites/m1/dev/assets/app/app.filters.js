'use strict';

// angular.
//   module('core').
//   filter('checkmark', function() {
//     return function(input) {
//       return input ? '\u2713' : '\u2718';
//     };
//   });

angular.
module('core')
    .filter('uniqueFilter', function() {
        // we will return a function which will take in a collection
        // and a keyname
        return function(collection, keyname) {
            // we define our output and keys array;
            // console.log('col',collection,'key', keyname);
            var output = [],
                keys = [];
            // we utilize angular's foreach function
            // this takes in our original collection and an iterator function
            angular.forEach(collection, function(item) {
                // we check to see whether our object exists
                var key = item[keyname];
                console.log('i', item);
                // if it's not already part of our keys array
                if (keys.indexOf(key) === -1) {
                    // add it to our keys array
                    keys.push(key);
                    // push this item to our final output array
                    output.push(item);
                }
            });
            // return our array which should be devoid of
            // any duplicates
            return output;
        };
    })
    .filter('capitalize', function() {
        return function(input) {
            return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
        }
    })
    .filter('deviceFilter', function() {
        return function(items, types) {
            var filtered = [];

            angular.forEach(items, function(item) {
                if (types.luxury == false && types.double_suite == false) {
                    filtered.push(item);
                } else if (types.luxury == true && types.double_suite == false && item.type == 'luxury') {
                    filtered.push(item);
                } else if (types.double_suite == true && types.luxury == false && item.type == 'double suite') {
                    filtered.push(item);
                }
            });

            return filtered;
        };
    })
    .filter('unsafe', function($sce) {
        return function(val) {
            return $sce.trustAsHtml(val);
        };
    })
    .filter('rangeFilter', function() {
        return function(items, attr, min, max) {
            var range = [],
                min = parseFloat(min),
                max = parseFloat(max);
            for (var i = 0, l = items.length; i < l; ++i) {
                var item = items[i];
                if (item.specs[attr] <= max && item.specs[attr] >= min) {
                    range.push(item);
                }
            }
            return range;
        };
    })
    .filter('decimal2comma', [
        function() { // should be altered to suit your needs
            return function(input) {
                var ret = (input) ? input.toString().replace(".", ",") : null;
                if (ret) {
                    var decArr = ret.split(",");
                    if (decArr.length > 1) {
                        var dec = decArr[1].length;
                        if (dec === 1) { ret += "0"; }
                    } //this is to show prices like 12,20 and not 12,2
                }
                return ret;
            };
        }
    ])
    .directive('a', function() {
        return {
            restrict: 'E',
            link: function(scope, elem, attrs) {
                if (attrs.href === '#pecka-info' || attrs.href === '#xsmini-info') {
                    elem.on('click', function(e) {
                        e.preventDefault();
                    });
                }
            }
        };
    });
