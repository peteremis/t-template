$(function() {
    $('.popup').fancybox({
        padding: 0,
        margin: 0,
        closeBtn: false,
        parent: "#content",
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.7)'
                }
            }
        }
    });

    $('.p-close').on('click', function(e) {
        e.preventDefault();
        $.fancybox.close();
    });

    // help menu

    $('.help-menu__item').hover(
        function() {
            var $this = $(this);
            expand($this);
        },
        function() {
            var $this = $(this);
            collapse($this);
        }
    );

    function expand($elem) {
        $elem.stop().animate({ width: '200px' }, 1000)
            .find('.help-menu__content').fadeIn(400, function() {
                $(this).find('p').stop(true, true).fadeIn(600);
            });
    }

    function collapse($elem) {
        $elem.stop().animate({ width: '42px' }, 1000)
            // .find('.help-menu__content').stop(true, true).fadeOut()
            .find('p').stop(true, true).fadeOut();
    }


    $(".programs").slick({

    autoplay: false,
    dots: false,
    infinite: false,
    initialSlide: 0,
    slidesToShow: 4,
    variableWidth: true,
    slidesToScroll: 1,
    nextArrow: '<button class="slick-next slick-arrow" aria-label="Next" type="button" style="display: block;" aria-disabled="false"></button>',
    prevArrow: '<button class="slick-prev slick-arrow" aria-label="Previous" type="button" aria-disabled="false" style="display: inline-block;"></button>',
    responsive: [
      {
        breakpoint: 700,
        settings: {
            dots: false,
            arrows: true,
            infinite: false,
            variableWidth: false,
            slidesToShow: 2
        }
      },
      {
        breakpoint: 450,
        settings: {
            dots: false,
            arrows: true,
            infinite: false,
            variableWidth: false,
            slidesToShow: 2
        }
      }
  ]
}).slick('slickFilter','.programItem');



});
