$(function () {

    $('.table-container').animate({
        scrollLeft: '+=165'
    }, 1);

    var actual = 3;
    var child = 3;

    $("table.happy").find('td:nth-child(' + child + ')').addClass("activeBox");

    $('#next-column').click(function (event) {

        event.preventDefault();
        $('.table-container').animate({
            scrollLeft: '+=165'
        }, 'slow');

        actual = actual + 1;

        if (actual > 7) {
            actual = 8;
            child = 8;
        } else if (actual == 7) {
            child = 7;
        } else if (actual == 6) {
            child = 6;
        } else if (actual == 5) {
            child = 5;
        } else if (actual == 4) {
            child = 4;
        } else if (actual == 3) {
            child = 3;
        }

        $("table.happy").find('td').removeClass("activeBox");
        $("table.happy").find('td:nth-child(' + child + ')').addClass("activeBox");

    });

    $('#previous-column').click(function (event) {

        event.preventDefault();
        $('.table-container').animate({
            scrollLeft: '-=165'
        }, 'slow');

        actual = actual - 1;

        if (actual < 2) {
            actual = 2;
            child = 2;
        } else if (actual == 2) {
            child = 2;
        } else if (actual == 3) {
            child = 3;
        } else if (actual == 4) {
            child = 4;
        } else if (actual == 5) {
            child = 5;
        } else if (actual == 6) {
            child = 6;
        } else if (actual == 7) {
            child = 7;
        }

        $("table.happy").find('td').removeClass("activeBox");
        $("table.happy").find('td:nth-child(' + child + ')').addClass("activeBox");
    });

    var scrollingSpeed = 1200;

    $('#btn-order-4g').click(function () {
        scrollTo('.c-row-4', 0);
        return false;
    });

    $('#order-top').click(function () {
        scrollTo('.c-row-table', 0);
        return false;
    });

    $('.c-row-2 .blue').click(function () {
        scrollTo('.c-row-7', 0);
        return false;
    });

    $('.c-row-2 .header-bottom img').click(function () {
        scrollTo('.c-row-6', 0);
        return false;
    });

});
