$(function () {

    $("#nav-sticky").sticky({
        topSpacing: 0,
        widthFromWrapper: true
    });

    $(".scroll-to").click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top - ($("#nav-sticky").outerHeight())
                }, 1000);
                return false;
            }
        }
    });

    var contentSections = $("[id*='sec-']"),
        secondaryNav = $("#nav-sticky");

    function updateSecondaryNavigation() {
        contentSections.each(function () {
            var actual = $(this),
                actualHeight = actual.height() + parseInt(actual.css('paddingTop').replace('px', '')) + parseInt(actual.css('paddingBottom').replace('px', '')),
                actualAnchor = secondaryNav.find('a[href="#' + actual.attr('id') + '"]');

            if ((actual.offset().top - secondaryNav.outerHeight() <= $(window).scrollTop()) && (actual.offset().top + actualHeight - secondaryNav.outerHeight() > $(window).scrollTop())) {
                actualAnchor.addClass('active');
            } else {
                actualAnchor.removeClass('active');
            }

        });
    }
    updateSecondaryNavigation();

    $(window).scroll(function (event) {
        updateSecondaryNavigation();
    });

    
    $("#info-1").qtip({
        content:    '<p><b>Informácia k prepočtu dátových jednotiek</b><br />' +
                    '1 prečítaný e-mail: 200 KB<br />' +
                    '1 web: 1 MB<br />' +
                    '1 uploadnutá fotka: 2,5 MB<br />' +
                    '1 stiahnutá hra/aplikácia: 25 MB<br />' +
                    '1 deň Facebooku v mobile: 10 MB<br />' +
                    '1 futbalový/hokejový zápas v Magio GO (cca 1,5 hod): 900 MB<br />' +
                    '1 stiahnutý album na Deezeri v najvyššej kvalite (cca 1 hod): 300 MB<br />' +
                    '<small>Prepočet má orientačný charakter.</small>' +
                    '</p>'
    });
    
    $(".info-2").qtip({
        content: '<p>Balík nie je dostupný pre <br />Happy XS mini</p>'
    });
    
    $(".info-3").qtip({
        content: '<p>Balík nie je dostupný pre <br />Happy XS mini</p>'
    });
    
    $(".info-4").qtip({
        content: '<p>Balík nie je dostupný pre<br />Happy XS mini, Happy XS a Happy S</p>'
    });
    
    $(".popups").fancybox();

});
