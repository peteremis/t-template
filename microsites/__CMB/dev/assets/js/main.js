function setCookie(cname, cvalue, exmin) {
  var d = new Date();
  d.setTime(d.getTime() + exmin * 60 * 1000);
  var expires = "expires=" + d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
  var name = cname + "=";
  var ca = document.cookie.split(";");
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == " ") {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

function checkCookie() {
  var user = getCookie("username");
  if (user != "") {
    alert("Welcome again " + user);
  } else {
    user = prompt("Please enter your name:", "");
    if (user != "" && user != null) {
      setCookie("username", user, 365);
    }
  }
}

function decrease() {
  $.ajax({
    url: "https://backvm.telekom.sk/www/cmb/public/decrease",
    cache: false,
    async: true,
    crossDomain: true,
    type: "GET",
    dataType: "json",
  });
}

var delayTime;
var timeCmbOpened = 15; //time in minutes
var timeCmbSent = 1440; //time in minutes (1day 24*60)
var cmbType = "day";

$(function () {
  var modalCmb = $("#myModal");
  var closeCmb = $(".cmb-close");
  closeCmb.click(function () {
    modalCmb.hide();
  });

  // When the user clicks anywhere outside of the modal, close it
  $(window).click(function (event) {
    if (event.target.id == "myModal") {
      modalCmb.css("display", "none");
    }
  });

  $("#cmb-form").on("submit", function (event) {
    event.preventDefault();

    var telnummber = $("#cmb-phone").val();
    if (telnummber.length == 10) {
      $("#cmb-phone").removeClass("error");
      sendCmb();
    } else {
      $("#cmb-phone").addClass("error");
    }

    setCookie("cmbSent", "sent", timeCmbSent);
  });

  $("#cmb-toggler").on("click", function () {
    showCmb(0);
  });

  $("#cmb__btn").on("click", function (event) {
    event.preventDefault();
    showCmb(0);
  });

  function showCmb(myTime) {
    setTimeout(function () {
      $("#cmbForm").show();
      $("#cmbThanks").hide();
      modalCmb.css("display", "flex");
      setCookie("cmbShow", "open", timeCmbOpened);
    }, myTime * 1000);
  }

  function sendCmb() {
    var telnummber = $("#cmb-phone").val();

    if (cmbType == "night") {
      var cmbCallTime = $("input[name='cmb-radio']:checked").val();
      var actualUrl = cmbCallTime + window.location.href;
    } else {
      var actualUrl = window.location.href;
    }
    var data = {
      msisdn: telnummber,
      url: actualUrl,
    };

    $.ajax({
      url: "https://backvm.telekom.sk/www/genesys/",
      method: "GET",
      crossDomain: true,
      data: data,
      dataType: "text",
      cache: false,
    }).done(function () {
      $("#cmbForm").hide();
      $("#cmbThanks").show();
      decrease();
    });
  }

  function autoStartCmb() {
    var pathUrl = window.location.pathname.substr(1);
    $.ajax({
      url: "https://backvm.telekom.sk/www/cmb/public/url/" + pathUrl,
      cache: false,
      async: true,
      crossDomain: true,
      type: "GET",
      dataType: "json",
    }).done(function (data) {
      if (data.popup) {
        showCmb(data.time);
      }
    });
  }

  function checkCmb() {
    $.ajax({
      url: "https://backvm.telekom.sk/www/cmb/public/calls",
      cache: false,
      async: true,
      crossDomain: true,
      type: "GET",
      dataType: "json",
    }).done(function (data) {
      cmbType = data[0].type;
      if (data[0].calls > 0) {
        $("#cmb-toggler").show();
        $("#cmb__block").show();
        if (cmbType == "night") {
          $(".cmb-radio1").show();
          $(".cmb-radio2").show();
          $("#cmbForm").removeClass("cmb-day");
          $("#cmbForm").addClass("cmb-night");
        } else {
          $(".cmb-radio1").hide();
          $(".cmb-radio2").hide();
          $("#cmbForm").removeClass("cmb-night");
          $("#cmbForm").addClass("cmb-day");
        }
        if (/^(.*;)?\s*cmbSent\s*=/.test(document.cookie) === false) {
          if (/^(.*;)?\s*cmbShow\s*=/.test(document.cookie) === false) {
            autoStartCmb();
          }
        }
      } else {
        $("#cmb-toggler").hide();
      }
    });
  }

  checkCmb();
});
