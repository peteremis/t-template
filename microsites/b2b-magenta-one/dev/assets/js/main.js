$(function() {

    /* TOOLTIP */
    $('.roaming-info').qtip({
        content: 'Susedné štáty: Česko, Maďarsko, Poľsko a Rakúsko.'
    });

   
    // video player
     $(".video-container").click(function() {
       $(".video-embed").css({ "opacity": "1", "display": "block" });
       $(".video-embed")[0].src += "&autoplay=1";
       $(".play_btn").css({ "opacity": "0", "display": "none" });
       $(this).unbind("click");
     });
     $(".play_btn").click(function() {
       $(".video-embed").css({ "opacity": "1", "display": "block" });
       $(".video-embed")[0].src += "&autoplay=1";
       $(".play_btn").css({ "opacity": "0", "display": "none" });
       $(this).unbind("click");
     });

    /* SLIDER */

    $('.pricing-tables').bxSlider({
        slideWidth: 300,
        minSlides: 1,
        maxSlides: 3,
        moveSlides: 1,
        slideMargin: 10,
        infiniteLoop: false,
        startSlide: 0,
        hideControlOnEnd: true,
        responsive: true
    });

    $('.prod-slider').bxSlider({
        slideWidth: 300,
        minSlides: 1,
        maxSlides: 3,
        moveSlides: 1,
        slideMargin: 0,
        infiniteLoop: false,
        startSlide: 0
    });

    $('.fancybox-media').fancybox({
        openEffect: 'none',
        closeEffect: 'none',
        helpers: {
            media: {}
        }
    });

    /* TABS */

    $(".tabs-menu a").click(function(event) {
        event.preventDefault();
        if (!$(this).parent().hasClass("current") && !$(this).parent().hasClass("isVariant")) {
            toggleTabs();
        }

        $(this).parent().addClass("current");
        $(this).parent().siblings().removeClass("current");
        var tab = $(this).attr("href"),
            parent = $(this).closest('ul').parent().parent();
        parent.find('.tab-content').not(tab).css("display", "none");
        $(tab).fadeIn();

    });

    var toggleTabs = function() {
        $('.tab__variant').toggleClass('selected');
    };

    /* STICKY NAV */

    $("#nav-sticky").sticky({
        topSpacing: 0,
        widthFromWrapper: true
    });

    $(".scroll-to").click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top - ($("#nav-sticky").outerHeight())
                }, 1000);
                return false;
            }
        }
    });

    var contentSections = $("[id*='sec-']"),
        secondaryNav = $("#nav-sticky");

    function updateSecondaryNavigation() {
        contentSections.each(function() {
            var actual = $(this),
                actualHeight = actual.height() + parseInt(actual.css('paddingTop').replace('px', '')) + parseInt(actual.css('paddingBottom').replace('px', '')),
                actualAnchor = secondaryNav.find('a[href="#' + actual.attr('id') + '"]');

            if ((actual.offset().top - secondaryNav.outerHeight() <= $(window).scrollTop()) && (actual.offset().top + actualHeight - secondaryNav.outerHeight() > $(window).scrollTop())) {
                actualAnchor.addClass('active');
            } else {
                actualAnchor.removeClass('active');
            }

        });
    }
    updateSecondaryNavigation();

    $(window).scroll(function(event) {
        updateSecondaryNavigation();
    });
});

$(window).load(function() {
    var form = $('.dynamicFormMain');

    form.appendTo('.form-holder');
});

$(document).ready(function() {

    function showPopup() {
        $('.form-popup').show();
        $('.fp__overlay').show();
    }

    function hidePopup() {
        $('.form-popup').hide();
        $('.fp__overlay').hide();
    }

    $('.fp__close').on('click', function() {
        $('.form-popup').hide();
        $('.fp__overlay').hide();
    });

    if ($('.portlet-msg-success').length > 0) {

        showPopup();
        setTimeout(hidePopup, 10000);

    }

    // var openPop = function() {
    //     showPopup();
    // };
    // setTimeout(openPop, 5000);

});
