$(function() {

$("#nav-sticky-new").sticky({
    topSpacing: 0,
    widthFromWrapper: true,
    className: 'is-sticky'
});

$(".scroll-to").click(function () {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html,body').animate({
                scrollTop: target.offset().top - ($("#nav-sticky-new").outerHeight() - 10)
            }, 1000);
            return false;
        }
    }
});    

$('.select-cont .select').click(function() {
    if (!$(this).hasClass('active')) {
        $('.select-cont .select.active').removeClass('active');
        $(this).addClass('active');
        
        var index = $(this).index();
        $('.actions-cont .items-cont').hide(200);

            $('.actions-cont .items-cont').eq(index).show(200);

    }
});

$('.actions-cont .item').click(function(){
    var comp = $(this).find('a.redirect');
    if (comp.length) {
        window.location.href = comp.attr('href');
    }
});

var mobileVersion = false;
checkVersion();

$(window).resize(function() {
    checkVersion();
});

function checkVersion() {
    if (!mobileVersion && $(window).width() <= 760) {
        console.log('width:', $(window).width());
        console.log('mobileVersion:', mobileVersion)
        getMobileVersion();
        mobileVersion = true;
    } else if (mobileVersion && $(window).width() > 760) {
        console.log('width:', $(window).width());
        console.log('mobileVersion:', mobileVersion)
        getDesktopVersion();
        mobileVersion = false;
    }
}

function getMobileVersion() {
    $('.items-cont.cont-1 .item-2').after($('.items-cont.cont-1 .item-5')); //5-ku za 2-ku
    $('.items-cont.cont-1 .item-7').after($('.items-cont.cont-1 .item-1'));
    $('.items-cont.cont-1 .item-1').after($('.items-cont.cont-1 .item-8'));
    $('.items-cont.cont-1 .right-cont .column').eq(1).hide();
}

function getDesktopVersion() {
    $('.items-cont.cont-1 .right-cont .column').eq(1).prepend($('.items-cont.cont-1 .item-8'));
    $('.items-cont.cont-1 .right-cont .column').eq(1).prepend($('.items-cont.cont-1 .item-1'));
    $('.items-cont.cont-1 .right-cont .column').eq(1).prepend($('.items-cont.cont-1 .item-5'));
    $('.items-cont.cont-1 .right-cont .column').eq(1).css('display','inline-block');
}
});
