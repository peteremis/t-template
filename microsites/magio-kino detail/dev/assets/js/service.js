$(document).ready(function () {
    var dataUrl;
    
    if (window.location.hostname == "localhost") {
        //only for dev purposes
        dataUrl = "/assets/js/movies.json";
    } else {
        dataUrl = "https://www.telekom.sk/documents/10179/16712671/movies.json";
    }
    
    var movieData = []; //store for all movies data
    var moviesHolder= $('#movies');
    var dataM = 'dataM';
    var dataL = 'dataL';

    function getDataOnInit(data, movieRatePlan) {
        moviesHolder.empty(); //reset
        movieData = data[movieRatePlan]; //default movie ratePlan to load
        showAllMovies(data[movieRatePlan]);
    }

    $.ajax({
        url: dataUrl,
        cache: false,
        //async: false,
        type: "get",
        crossDomain: true,
        success: function (movie) {
            getDataOnInit(movie, dataM); // get data on init page load

            var $ratePlanInput = $('.selectRatePlan');
                $ratePlanInput.on('change', function () {
                    var ratePlanInputVal = $(this).find('option:selected').val();
                 
                    if (ratePlanInputVal === dataM) {
                        getDataOnInit(movie, dataM);
                        $('p.tvHead span').text("MAGIO KINO M");
                        $('#kinoActivate').text("AKTIVOVAŤ MAGIO KINO M").attr('href', '#')
                    } else if (ratePlanInputVal === dataL) {
                        getDataOnInit(movie, dataL);
                        $('p.tvHead span').text("MAGIO KINO L");
                        $('#kinoActivate').text("AKTIVOVAŤ MAGIO KINO L").attr('href', '#')
                    }
                });
        },
        error: function () {
            alert("Get data error ... try again!");
        }
    });
    
    // generate single movie item
    function createMovie (id, img, movieName, desc, movieGenre, holder) {
        var singleMovie = '';
        singleMovie +=
        '<div id="movie-'+id+'" class="movie col-xs-6 col-sm-4 col-md-3">'+
            '<div class="imgWrapper">'+
                '<img src="'+img+'" alt="">'+
                '<div class="desc">'+(desc !== undefined ? desc : "Detail nie je k dispozícii")+'</div>'+
            '</div>'+
            '<div class="text">'+
                '<p class="movieTitle">'+movieName+'</p>'+
                '<p class="movieCat">'+movieGenre+'</p>'+
            '</div>'+
        '</div>';
        $(singleMovie).stop().fadeIn(500).css("display","inline-block").appendTo(holder);
    }

    // movie categories
    var actionMoviesHolder = 'actionMovies';
    var animatedMoviesHolder = 'animatedMovies';
    var horrorMoviesHolder = 'horrorMovies';

    //generate movie category <div> holder 
    function generateMovieHolder (movieCategory, text) {
        var GenerateCategory = '';
        GenerateCategory +=
        '<div class="movieHolder bggrey col-xs-12" id="'+movieCategory+'">'+
            '<p class="categoryHeading"><span  class="genre">'+text+'</span> zoradené od najnovších</p>'+
        '</div>';
        if ($('#'+movieCategory+'').length == 0) {
            $(GenerateCategory).appendTo(moviesHolder);
        }
    }

    //append movies to holder
    function showAllMovies(movie) {
        $.each( movie, function( i, movie ) {
            var category = movie.category[0];
            if (category.akcne == "ano") { //if product is in category, show
                generateMovieHolder (actionMoviesHolder, "Akčné, dobrodružné");
                createMovie (movie.id, movie.img, movie.movieName, movie.desc, movie.genre, "#"+actionMoviesHolder);
            } 
            if (category.animovane == "ano") {
                generateMovieHolder (animatedMoviesHolder, "Animované");
                createMovie (movie.id, movie.img, movie.movieName, movie.desc, movie.genre, "#"+animatedMoviesHolder)
            } 
            if (category.horror == "ano") {
                generateMovieHolder (horrorMoviesHolder, "Horory");
                createMovie (movie.id, movie.img, movie.movieName, movie.desc, movie.genre, "#"+horrorMoviesHolder)
            }
        });
    }


    var $searchQueryInput = $('#searchQuery');
    var $filterCategoryInput = $('.selectCategory');
    var filterCategoryInputVal = $('.selectCategory').find('option:selected').val(); //get category name from selected option
    
    // show movie genre by filtered category
    function filterMoviesByCategory (movieCategoryHolder, text, JSONcategoryName) {
        moviesHolder.empty(); //reset
       
        if ( $searchQueryInput.val().length > 0) {
            filterByUserInput($searchQueryInput);
        } else {
            generateMovieHolder (movieCategoryHolder, text);
            for (var a = 0; a < movieData.length; a++) {
                if (movieData[a].category[0][JSONcategoryName] == "ano") {
                    createMovie (movieData[a].id, movieData[a].img, movieData[a].movieName, movieData[a].desc, movieData[a].genre, "#"+movieCategoryHolder);
                }
            }
        }
    }

    // start search on user movie category change
    $filterCategoryInput.on('change', function(e) {
        filterCategoryInputVal = $(this).find('option:selected').val();

        switch(filterCategoryInputVal) {
            case "vsetky":
                moviesHolder.empty(); //reset
                showAllMovies(movieData);
                $searchQueryInput.val('') //reset search input field
                break;
            case "akcne":
                filterMoviesByCategory (actionMoviesHolder, "Akčné, dobrodružné", filterCategoryInputVal);
                break;
            case "animovane":
                filterMoviesByCategory (animatedMoviesHolder, "Animované", filterCategoryInputVal);
                break;
            case "horror":
                filterMoviesByCategory (horrorMoviesHolder, "Horory", filterCategoryInputVal);
            break;
            default:
                moviesHolder.empty(); //reset
                showAllMovies(movieData);
        }
    });

    var typingTimer;
    function filterByUserInput($thisInput) {
        var searchInputVal = $thisInput.val();
        clearTimeout(typingTimer);

        if (searchInputVal.length >= 0) {
            typingTimer = setTimeout( function() {
                userDoneTyping(searchInputVal, filterCategoryInputVal)
            }, 1000);
        } else {
            moviesHolder.empty(); //reset
            showAllMovies(movieData);
        }
    }

    function userDoneTyping(searchInputVal, JSONcategoryName) {
        moviesHolder.empty(); //reset
        generateMovieHolder ("queryHolder", "Výsledky hľadania:");
        var foundAnyResult;
        for (var a = 0; a < movieData.length; a++) {
            console.log( searchInputVal.toLowerCase(), JSONcategoryName)
            if (movieData[a].movieName.toLowerCase().indexOf(searchInputVal.toLowerCase()) !== -1 && movieData[a].category[0][JSONcategoryName] == "ano") {
                foundAnyResult = true;
                createMovie (movieData[a].id, movieData[a].img, movieData[a].movieName, movieData[a].desc, movieData[a].genre, "#queryHolder");
            }
        }

        if (foundAnyResult !== true) {
            $('#queryHolder').append('<div class="searchError"><p>Lutujeme, vami zvolený výraz sme nenašli. Skúste vyhľadávanie znova.</p></div>')
        }
    }

    // start search on user input (by name of movie)
    $searchQueryInput.keyup(function() {
        $thisInput = $(this);
        filterByUserInput($thisInput);
    })
    
});
    