$(function() {
    var photo = document.getElementById("head-motion-bg");

    var tl = new TimelineMax();

    tl.to(photo, 100, {scaleX: 1.5, scaleY: 1.5, repeatDelay:0, repeat:-1, yoyo:true})
    tl.play();
});
