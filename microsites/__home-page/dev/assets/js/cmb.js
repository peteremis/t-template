function setAd(adNum, adTitle, adLink, adUrl, showArrowLink) {
    var $adTitle = $('.ad-' + adNum + ' .box__title'),
    $adLink = $('.ad-' + adNum + '.box__link-main');
    $adLinkText = $('.ad-' + adNum + ' .box__link');

    $adTitle.text(adTitle);

    if (showArrowLink) {
        $adLinkText.html(adLink + ' <img class="link-arrow" src="/documents/10179/9664823/arrow-right.png" alt="">');
    } else {
        $adLinkText.text(adLink);
    }

    $adLink.attr('href', adUrl);
}

$(window).on('load', function() {

    var $livechatBtn = $('#livechat'),
        $cmbBoxes = $('.box__cmb'),
        $adBoxes = $('.box-ad');

    if ($livechatBtn.is(':visible')) {
        $cmbBoxes.show();
        $adBoxes.hide();
    }

    setAd(1, 'Zoberte si od nás paušál Happy a druhý získate so zľavou 50 %!', 'Prejsť k paušálom', '/volania/happy?pausal-za-polovicu#tab-6', true);
    setAd(2, 'Zoberte si od nás paušál Happy a druhý získate so zľavou 50 %!', 'Prejsť k paušálom', '/volania/happy?pausal-za-polovicu#tab-6', true);
    setAd(3, 'Aký rýchly internet môžete mať?', 'Zistiť rýchlosť', '/fix/objednavka/-/scenario/e-shop/chytry-balik', true);
    setAd(4, 'Nedostupné služby?', 'Pozrite si naše plánované pokrytie', '/dostupnost/nove-lokality ', true);
    setAd(5, 'Prihláste sa alebo si založte skupinu Magenta 1', 'Ďalej', '/magenta1-skupina/', true);
});
