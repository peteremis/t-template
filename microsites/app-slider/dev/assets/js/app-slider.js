$(function () {

    // GLOBALS AND VARS
    var $device = $('.device'),
        $device__screen = $('.device__screen'),
        $text__area = $('.text-area'),
        $controls__left = $('.controls__left'),
        $controls__right = $('.controls__right'),
        $stack = $('.stack'),
        $stack__item = $('.stack > .stack__item'),
        $copy__img = $(),
        $copy__text = $(),
        $current__images = $(),
        $current__texts = $(),
        direction = 1, // 0 - left, 1 - right
        item__count = $stack__item.length - 1,
        current__item = 0,
        animation = false,
        animation_infinite = true,
        first__load = true;

    // LOAD THE CONTENT
    function load_content() {

        $copy__img = $stack__item[current__item].children[0];
        $copy__text = $stack__item[current__item].children[1];

        $($copy__img).clone().appendTo($device__screen).css({
            '-webkit-animation': 'fadein 1s',
            '-moz-animation': 'fadein 1s',
            '-ms-animation': 'fadein 1s',
            '-o-animation': 'fadein 1s',
            'animation': 'fadein 1s'
        });
        $($copy__text).clone().appendTo($text__area);
    }

    // RELOAD CONTENT BASED ON CLICK DIRECTION
    function reload_content(dir) {

        if (dir == 1) {
            if (current__item < item__count) {
                current__item++;
                load_content();
            } else {
                current__item = 0;
                load_content();
            }
        } else if (dir == 0) {
            if (current__item == 0) {
                current__item = item__count;
                load_content();
            } else {
                current__item--;
                load_content();
            }
        }
    }

    // REMOVE THE ACTUAL CONTENT BEFORE NEW ONE BEING LOADED
    function replace_images() {
        $current__images = $('.device__screen');
        $current__texts = $('.text-area');
        $($current__images[0].children[0]).css({
            '-webkit-animation': 'scroll_down 1s',
            '-moz-animation': 'scroll_down 1s',
            '-ms-animation': 'scroll_down 1s',
            '-o-animation': 'scroll_down 1s',
            'animation': 'scroll_down 1s'
        });
        setTimeout(function () {
            $($current__images[0].children[0]).remove();
        }, 1000);

        $($current__texts[0].children[0]).remove();
    }

    // CLICK FUNCTIONS
    function click_functions(dir) {
        if (animation_infinite) {
            if (animation) {
                return false;
            }
        } else {
            if (animation || (dir == 0 && current__item == 0) || (dir == 1 && current__item == item__count)) {
                return false;
            }
        }

        animation = true;

        replace_images();
        reload_content(dir);

        setTimeout(function () {
            animation = false;
        }, 1000);
    }

    // SETUP THE CLICK FUNCTIONALITY
    $($controls__right).on('click', function (e) {
        e.preventDefault();
        direction = 1;
        click_functions(direction);
    });

    $($controls__right).dblclick(function (e) {
        e.preventDefault();
    });

    $($controls__left).on('click', function (e) {
        e.preventDefault();
        direction = 0;
        click_functions(direction);
    });

    $($controls__left).dblclick(function (e) {
        e.preventDefault();
    });

    // EXECUTE THE CODE
    load_content();

});
