$(function() {

    $(".scroll-to").click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });

    $('.tabs-menu a').click(function (event) {
        event.preventDefault();
        if (!$(this).parent().hasClass('current') && !$(this).parent().hasClass('isVariant')) {
            toggleTabs();
        }

        $(this).parent().addClass('current');
        $(this).parent().siblings().removeClass('current');
        var tab = $(this).attr('href'),
        parent = $(this).closest('ul').parent().parent();
        parent.find('.tab-content').not(tab).css('display', 'none');
        $(tab).fadeIn();
    });

    var toggleTabs = function () {
        $('.tab__variant').toggleClass('selected');
    };

    var $playBtn = $('.play-btn'),
    $closeBtn = $('.close-btn'),
    $videoHolder = $('.video-holder'),
    $video = $('#video'),
    videoURL = $('#video').prop('src');
    $hero = $('#hero'),
    $fixedContainer = $('#hero .container-fixed');

    $($playBtn).on('click', function(e){
        e.preventDefault();
        $($hero).addClass('no-padding');
        $($fixedContainer).addClass('hidden');
        $($videoHolder).removeClass('hidden');

        $($video)[0].src += "&autoplay=1";
    });

    $($closeBtn).on('click', function(e){
        e.preventDefault();
        $($hero).removeClass('no-padding');
        $($fixedContainer).removeClass('hidden');
        $($videoHolder).addClass('hidden');

        videoURL = videoURL.replace("&autoplay=1", "");
        $video.prop('src','');
        $video.prop('src',videoURL);
    });

    // /* FROM TABS TO ACCORDION */
    // var $accordion = $('.accordion'),
    // $heads = $('.tabs-menu a'),
    // $contents = $('.tab-content'),
    // itemCount = $($heads).length - 1,
    // accordionCode = '<div class="item open">';
    //
    // for (let i = 0; i <= itemCount; i++) {
    //     accordionCode += '<div class="heading">' + $($heads[i]).text() + '</div>';
    //     accordionCode += '<div class="content">' + $($contents[i]).html() + '</div></div>';
    //
    //     if (i !== itemCount) {
    //         accordionCode += '<div class="item open">';
    //     }
    // }
    //
    // $(accordionCode).appendTo($accordion);
    //
    // /* ACCORDION */
    //
    // $('.accordion .item .heading').click(function() {
    //
    //     var a = $(this).closest('.item');
    //     var b = $(a).hasClass('open');
    //     var c = $(a).closest('.accordion').find('.open');
    //
    //     if(b != true) {
    //         $(c).find('.content').slideUp(200);
    //         $(c).removeClass('open');
    //     }
    //
    //     //$(a).toggleClass('open');
    //     $(a).find('.content').slideToggle(200);
    //
    // });
    //
    // $('.accordion .item.open').find('.content').slideDown(200);

    /* TABS */
$(".tabs-menu a").click(function(event) {
    event.preventDefault();
    $(this).parent().addClass("current");
    $(this).parent().siblings().removeClass("current");
    var tab = $(this).attr("href"), // select clicked tab. In example "#tab-1" ..
        parent = $(this).closest('ul').parent().parent(); // <tabs-container>
    parent.find('.tab-content').not(tab).css("display", "none"); //hide all not clicked tabs
    $(tab).fadeIn(); //show clicked tab


});
/* TABS END */

/* FROM TABS TO ACCORDION */
var dataSegment = $("[data-segment]").each(function(){
  //console.log($(this).attr('data-segment')); //zobraz "data-segment" hodnotu pre kazdu <section>

  closestHead = $($(this).find('.tabs-menu a'));
  closestItemCount = closestHead.length - 1; //vytiahni pocet tabov pre kazdu <section> s atributom "data-segment"
  //console.log(closestItemCount + "heads");

  closestContent = $($(this).find('.tab-content')); //najdi tab-content v danej <section>
  //console.log(closestContent);

  closestAccordion = $(this).find('.accordion'); //najdi .accordion v danej <section>

  accordionItem = '<div class="item">';

  for (var i = 0; i <= closestItemCount; i++) {
      accordionItem += '<div class="heading">' + $(closestHead[i]).text()  + '</div>';
      accordionItem += '<div class="content">' + $(closestContent[i]).html() + '</div></div>';

      if (i !== closestItemCount) {
          accordionItem += '<div class="item">';
      }
  }

  //if data-segment and data-accordion value match, show accordion data
  if($(this).attr('data-segment') === closestAccordion.attr('data-accordion')) {
       $(accordionItem).appendTo(closestAccordion);
  }

  var $items = $('.accordion .item');
  //SET OPENED ITEM
  $($items[0]).addClass('open');

});
/* FROM TABS TO ACCORDION END */

/* ACCORDION */
$('.accordion .item .heading').click(function(e) {

    var clickedHead = e.target;
    var $item = $(clickedHead).closest('.item');
    var isOpen = $item.hasClass('open');
    var $content = $item.find('.content');
    var $acPrice = $(clickedHead).find('.ac-price');

    if (isOpen) {
        $content.slideUp(200);
        $item.removeClass('open');
        $acPrice.show();
    } else {
        $content.slideDown(200);
        $item.addClass('open');
        $acPrice.hide();
    }
});


$('.accordion .item.open').find('.content').slideDown(200);
/* ACCORDION END */

});
