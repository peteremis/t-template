$(function() {


  /* TABS */
  $(".tabs-menu a").click(function(event) {
      event.preventDefault();
      $(this).parent().addClass("current");
      $(this).parent().siblings().removeClass("current");
      var tab = $(this).attr("href"), // select clicked tab. In example "#tab-1" ..
          parent = $(this).closest('ul').parent().parent(); // <tabs-container>
      parent.find('.tab-content').not(tab).css("display", "none"); //hide all not clicked tabs
      $(tab).fadeIn(); //show clicked tab
  });
  /* TABS END */

  var dataSegment = $("[data-segment]").each(function(){
    //console.log($(this).attr('data-segment')); //zobraz "data-segment" hodnotu pre kazdu <section>

    closestHead = $($(this).find('.tabs-menu a'));
    closestItemCount = closestHead.length - 1; //vytiahni pocet tabov pre kazdu <section> s atributom "data-segment"
    closestContent = $($(this).find('.tab-content')); //najdi tab-content v danej <section>
    closestAccordion = $(this).find('.accordion'); //najdi .accordion v danej <section>
    accordionItem = '<div class="item">';
    for (var i = 0; i <= closestItemCount; i++) {
        accordionItem += '<div class="heading">' + $(closestHead[i]).text()  + '</div>';
        accordionItem += '<div class="content">' + $(closestContent[i]).html() + '</div></div>';

        if (i !== closestItemCount) {
            accordionItem += '<div class="item">';
        }
    }


    //if data-segment and data-accordion value match, show accordion data
    if($(this).attr('data-segment') === closestAccordion.attr('data-accordion')) {
         $(accordionItem).appendTo(closestAccordion);
    }

    var $items = $('.accordion .item');
    //SET OPENED ITEM
    $($items[0]).addClass('open');
  });

  /* ACCORDION */
  $('.accordion .item .heading').click(function(e) {
      var clickedHead = e.target;
      var $item = $(clickedHead).closest('.item');
      var isOpen = $item.hasClass('open');
      var $content = $item.find('.content');
      var $acPrice = $(clickedHead).find('.ac-price');

      if (isOpen) {
          $content.slideUp(200);
          $item.removeClass('open');
          $acPrice.show();
      } else {
          $content.slideDown(200);
          $item.addClass('open');
          $acPrice.hide();
      }
  });
  $('.accordion .item.open').find('.content').slideDown(200);
  /* ACCORDION END */

  /* SECONDARY RADIO BTNS */
  //default initial magenta color for radio box
  var defaultCheckboxColor = $('input[name=rb-pair]:checked', '#secondaryRadio').attr('id');
  $('.secondaryRadio').find('label[for='+  defaultCheckboxColor  +']').css({'background-color':'#e20074',
                'color':'#fff'});

  //initially show only first detailHolder
   $('.detailHolder').not(':first').hide();

  //on click set magenta color to selected radio box and make other radio boxes transparent
  $(".secondaryRadio input").click( function(){
    var getAttr = $(this).attr('id');
    var label = $('.secondaryRadio').find('label[for='+  getAttr  +']');
    var allCheckboxes = $(".secondaryRadio label").css({'background-color':'transparent',
  'color':'#000'})

     if( $(this).is(':checked')) {
       $(label).css({'background-color':'#e20074',
                     'color':'#fff'});
     }

     //If radiobtn and div IDs match, show div.
     var btnID = $(this).attr('id');
     //console.log(btnID);
     var checkedDiv = $('.detailHolder[id='+btnID+']');
     //console.log(checkedDiv);
     var divID = $('.detailHolder').attr('id');
     var objLength = $(this).length;
     //console.log(objLength);
     if (btnID = divID && objLength > 0) {
       //console.log("pravda");
       //console.log(checkedDiv);
       $('.detailHolder').hide();
       $(checkedDiv).show();
     }
  });
  /* SECONDARY RADIO BTNS END */

  /* TOGGLE ARROW */
  $('.toggle-arrow').click(function(event) {
      event.preventDefault();
    $(event.target).next('p.arrow-content').toggle(200);
    $(this).find('.arrow-right').toggleClass('arrow-rotate');
  });
  /* TOGGLE ARROW END */

  /* TOOLTIPS */
  $('#qtipInfo').qtip({
      content: 'Balík aktivujete na webe počas kúpy Happy paušálu.',
      position: {
          corner: {
              target: 'leftMiddle',
              tooltip: 'topMiddle'
          }
      }
      // position: {
      //     my: 'bottom center',
      //     at: 'top center'
      // }
  }).bind('click', function(event) {
      event.preventDefault();
      return false;
  });
  /* TOOLTIPS END */

  /* heapbox - zariadenia k magio tv */
  $('#magio-tv-tooltip').hide();





  // $('#heapTv').heapbox({
  // 	'onChange':function(value){
  //     console.log('Selected value: '+value);
  //     if (value === "4ks") {
  //       console.log("pravda");
  //       $('#price1').text("24,98 €");
  //       $('#monthly').text("8,77 €");
  //     } else if (value === "3ks") {
  //       $('#price1').text("24,98 €");
  //       $('#monthly').text("6,83 €");
  //     } else if (value === "2ks") {
  //       $('#price1').text("24,98 €");
  //       $('#monthly').text("4,98 €");
  //     } else if (value === "1ks") {
  //       $('#price1').text("0 €");
  //       $('#monthly').text("2,95 €");
  //       $('#magio-tv-tooltip').show();
  //     } else if (value === "1kssat") {
  //       $('#price1').text("29,99 €");
  //       $('#monthly').text("2,95 €");
  //     } else if (value === "2kssat") {
  //       $('#price1').text("54,97 €");
  //       $('#monthly').text("5,90 €");
  //     } else if (value === "3kssat") {
  //       $('#price1').text("54,97 €");
  //       $('#monthly').text("8,77 €");
  //     } else if (value === "4kssat") {
  //       $('#price1').text("54,97 €");
  //       $('#monthly').text("6,83 €");
  //     }
  //   }
  // });

  /* heapbox - zariadenia k magio tv */
  /* HEAPBOX END */


});
