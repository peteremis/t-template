//back to to previous url
//$('#a_referrer a').attr('href',document.referrer);

/* TABS */
$(".tabs-menu a").click(function(event) {
  event.preventDefault();
  $(this)
    .parent()
    .addClass("current");
  $(this)
    .parent()
    .siblings()
    .removeClass("current");
  var tab = $(this).attr("href"), // select clicked tab. In example "#tab-1" ..
    parent = $(this)
      .closest("ul")
      .parent()
      .parent(); // <tabs-container>
  parent
    .find(".tab-content")
    .not(tab)
    .css("display", "none"); //hide all not clicked tabs
  $(tab).fadeIn(); //show clicked tab
});
/* TABS END */

var dataSegment = $("[data-segment]").each(function() {
  closestHead = $($(this).find(".tabs-menu a"));
  closestItemCount = closestHead.length - 1; //vytiahni pocet tabov pre kazdu <section> s atributom "data-segment"
  closestContent = $($(this).find(".tab-content")); //najdi tab-content v danej <section>
  closestAccordion = $(this).find(".accordion"); //najdi .accordion v danej <section>
  accordionItem = '<div class="item">';
  for (var i = 0; i <= closestItemCount; i++) {
    accordionItem +=
      '<div class="heading">' + $(closestHead[i]).text() + "</div>";
    accordionItem +=
      '<div class="content">' + $(closestContent[i]).html() + "</div></div>";

    if (i !== closestItemCount) {
      accordionItem += '<div class="item">';
    }
  }

  //if data-segment and data-accordion value match, show accordion data
  if (
    $(this).attr("data-segment") === closestAccordion.attr("data-accordion")
  ) {
    $(accordionItem).appendTo(closestAccordion);
  }

  var $items = $(".accordion .item");
  //SET OPENED ITEM
  $($items[0]).addClass("open");
});

/* ACCORDION */
$(".accordion .item .heading").click(function(e) {
  var clickedHead = e.target;
  var $item = $(clickedHead).closest(".item");
  var isOpen = $item.hasClass("open");
  var $content = $item.find(".content");
  var $acPrice = $(clickedHead).find(".ac-price");

  if (isOpen) {
    $content.slideUp(200);
    $item.removeClass("open");
    $acPrice.show();
  } else {
    $content.slideDown(200);
    $item.addClass("open");
    $acPrice.hide();
  }
});
$(".accordion .item.open")
  .find(".content")
  .slideDown(200);
/* ACCORDION END */

/* SECONDARY RADIO BTNS */
//default initial magenta color for radio box
var defaultCheckboxColor = $(
  "input[name=rb-pair]:checked",
  "#secondaryRadio"
).attr("id");
$(".secondaryRadio")
  .find("label[for=" + defaultCheckboxColor + "]")
  .css({ "background-color": "#e20074", color: "#fff" });

//initially show only first detailHolder and hide all other detailHolders
$(".detailHolder")
  .not(":first")
  .hide();
$(".accordion")
  .find(".detailHolder")
  .eq(0)
  .show();

//on click set magenta color to selected radio box and make other radio boxes transparent
$(".secondaryRadio input").click(function() {
  var getAttr = $(this).attr("id");
  var label = $(".secondaryRadio").find("label[for=" + getAttr + "]");
  var allCheckboxes = $(".secondaryRadio label").css({
    "background-color": "transparent",
    color: "#000"
  });

  if ($(this).is(":checked")) {
    $(label).css({ "background-color": "#e20074", color: "#fff" });
  }

  //If radiobtn and div IDs match, show div.
  var btnID = $(this).attr("id");
  //console.log(btnID);
  var checkedDiv = $(".detailHolder[id=" + btnID + "]");
  //console.log(checkedDiv);
  var divID = $(".detailHolder").attr("id");
  var objLength = $(this).length;
  //console.log(objLength);
  if ((btnID = divID && objLength > 0)) {
    //console.log("pravda");
    //console.log(checkedDiv);
    $(".detailHolder").hide();
    $(checkedDiv).show();
  }
});
/* SECONDARY RADIO BTNS END */

/* TOGGLE ARROW */
$(".toggle-arrow").click(function(event) {
  event.preventDefault();
  $(event.target)
    .next("p.arrow-content")
    .toggle(200);
  $(this)
    .find(".arrow-right")
    .toggleClass("arrow-rotate");
});

if (matchMedia) {
  var mediaQueryMobil = window.matchMedia("(max-width: 768px)");
  mediaQueryMobil.addListener(WidthChange);
  WidthChange(mediaQueryMobil);
}

// media query change
function WidthChange(mediaQueryMobil) {
  if (mediaQueryMobil.matches) {
    // window width is less than 550px
    $(".arrow-content").hide();
    $(".toggle-arrow")
      .find(".arrow-right")
      .removeClass("arrow-rotate");
    $(".arrow-right.One").addClass("arrow-rotate");
    $(".arrow-content.One").show();
  } else {
    // window width is more than 550px
    $(".arrow-content").show();
    $(".toggle-arrow")
      .find(".arrow-right")
      .addClass("arrow-rotate");
  }
}
/* TOGGLE ARROW END */

/* TOGGLE TV programs */
$(".toggleProgram").change(function() {
  $(this)
    .children()
    .not(":first")
    .toggle();
  $(this).toggleClass("programOpen");
});
/* TOGGLE TV programs end */

/* HEAPBOX */
/* heapbox tab-3 fix */
//zobraz heapBox po kliknuti na tab-3
$("a[href=#tab-3]").on("click", function() {
  if (
    $("a[href=#tab-3]")
      .closest("li")
      .hasClass("current")
  ) {
    $("#heapbox_heapTv").show();
  }
});

var $heapBoxHolder = $(".heapBoxHolder");
var $accordionHeapBoxHolder = $(".accordion").find(".heapBoxHolder");

var heapbox = $(
  [
    '<select id="heapTv">' +
    '<option value="4kssat">4 kusy Magio Box SAT</option>' +
    '<option value="3kssat">3 kusy Magio Box SAT</option>' +
    '<option value="2kssat">2 kusy Magio Box SAT</option>' +
    '<option value="1kssat">1 kus Magio Box SAT</option>' +
    "</select>" +
    '<p class="heapTV-infoText">Štyri Magio Boxy umožňujú pripojiť štyri televízory. Na každom môžete sledovať iný program.</p>' +
    '<div class="heapTVtext">' +
    '<div class="heapTvtext-wrap">' +
    '<div class="left">' +
    "<p>Nahrávanie:</p>" +
    "</div>" +
    '<div class="right">' +
    '<a class="magioTooltip">áno</a>' + //append tooltip here
      "</div>" +
      "</div>" +
      '<div class="heapTvtext-wrap">' +
      '<div class="left">' +
      "<p>Cena jednorazovo pri kúpe:</p>" +
      "</div>" +
      '<div class="right" id="price1">' +
      "<p>30,00 €</p>" +
      "</div>" +
      "</div>" +
      '<div class="heapTvtext-wrap">' +
      '<div class="left">' +
      "<p>Cena mesačne:</p>" +
      "</div>" +
      '<div class="right" id="monthly">' +
      "<p>8,77 €</p>" +
      "</div>" +
      "</div>" +
      "</div>"
  ].join("\n")
);

if (window.matchMedia("(max-width: 768px)").matches) {
  //ak <550px zobraz heapbox v accordione
  $(heapbox).appendTo($accordionHeapBoxHolder);
  $(".item").on("click", function() {
    if ($(this).eq(2)) {
      //console.log("rozklikol si treti Tab");
      $("#heapbox_heapTv").show();
    }
  });
  //zmaz heapbox z tabu-3
  $("#tab-3")
    .find("#heapbox_heapTv")
    .remove();
  $("#tab-3")
    .find("#heapTv")
    .remove();
  $("#tab-3")
    .find(".heapTV-infoText")
    .remove();
  $("#tab-3")
    .find(".heapTVtext")
    .remove();
} else {
  //ak >550px zobraz heapbox v tab-3
  $(heapbox).appendTo($heapBoxHolder);
  // zmaz heapbox z accordionu
  $(".accordion")
    .find("#heapbox_heapTv")
    .remove();
  $(".accordion")
    .find("#heapTv")
    .remove();
  $(".accordion")
    .find(".heapTV-infoText")
    .remove();
  $(".accordion")
    .find(".heapTVtext")
    .remove();
}

//append tooltip to <a> element
var $magioTooltip = $(".magioTooltip");
var tooltipItem =
  '<img alt="" id="magio-tv-tooltip" src="/documents/10179/16408252/qm.png" style=" width: 15px; height: auto; margin-left: 3px;">';
$(tooltipItem).appendTo($magioTooltip);

$("#magio-tv-tooltip")
  .qtip({
    content: "Bez funkcie nahrávania zaplatíte iba 1,94€ mesačne",
    style: {
      classes: "qtip-tipsy"
    },
    // position: {
    //     corner: {
    //         target: 'leftMiddle',
    //         tooltip: 'topMiddle'
    //     }
    // }
    position: {
      my: "bottom right",
      at: "top center"
    }
  })
  .bind("click", function(event) {
    event.preventDefault();
    return false;
  });

/* heapbox - zariadenia k magio tv */
$("#magio-tv-tooltip").hide(); //hide tooltip on load

$("#heapTv").heapbox({
  onChange: function(value) {
    //console.log('Selected value: '+value);
    if (value === "1kssat") {
      $(".heapTV-infoText").text(
        "Jeden Magio Box umožňuje pripojiť jeden televízor."
      );
      $("#price1").text("0 €");
      $("#monthly").text("2,95 €");
      $("#magio-tv-tooltip").hide();
    } else if (value === "2kssat") {
      $(".heapTV-infoText").text(
        "Dva Magio Boxy umožňujú pripojiť dva televízory. Na každom môžete sledovať iný program."
      );
      $("#price1").text("10,00 €");
      $("#monthly").text("5,90 €");
      $("#magio-tv-tooltip").hide();
    } else if (value === "3kssat") {
      $(".heapTV-infoText").text(
        "Tri Magio Boxy umožňujú pripojiť tri televízory. Na každom môžete sledovať iný program."
      );
      $("#price1").text("20,00 €");
      $("#monthly").text("6,83 €");
      $("#magio-tv-tooltip").hide();
    } else if (value === "4kssat") {
      $(".heapTV-infoText").text(
        "Štyri Magio Boxy umožňujú pripojiť štyri televízory. Na každom môžete sledovať iný program."
      );
      $("#price1").text("30,00 €");
      $("#monthly").text("8,77 €");
      $("#magio-tv-tooltip").hide();
    }
  }
});
/* heapbox - zariadenia k magio tv */
/* HEAPBOX END */

/* did customer use TV checker? */
function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(";");
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == " ") {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}
//checker used
function TVcheckerUsed(url) {
  if (getCookie("CustomerChecked") == "true") {
    $("#btnNakup").attr("href", url);
    $("#btnNakup").text("PREJSŤ K NÁKUPU");
  } else {
    $("#btnNakup").attr("href", document.referrer);
    $("#btnNakup").text("OVERIŤ DOSTUPNOSŤ");
  }
}
/* did customer use TV checker? END */

/* podla toho z akej url pridem, zmen ceny magio TV */

var $magioBalikHolder = $(".programovyBalikHolder");

if (window.location.href.indexOf("/#magioM") > -1) {
  $("p.activation").text("pri 24 mes. viazanosti 10,00 €");
  TVcheckerUsed(
    "https://www.telekom.sk/fix/objednavka/-/scenario/e-shop/chytry-balik?televizia=m"
  );
  $(".tvheadline").text("MAGIO TV SAT M");

  $(".pricewrap .priceLeft .months").text("2. a 3. mesiac");
  $(".pricewrap .priceRight .activation").text("pri 24 mes. viazanosti 5,90 €");
  $(".pricewrap .priceRight .strikeThrough").text("v predajni 10,90 €");
  $(".pricewrap .priceRight .otherMonths").text("10,90 €");

  var magiosatMbalik = $(
    [
      '<p class="halbFett">Magio TV SAT M</p>' +
        '<p class="magenta">Základný balík programov</p>' +
        '<p class="normal">V našom pestrom balíčku programov je všetko, na čo si spomeniete. Filmy, šport, dokumenty, hudba alebo program pre vašich najmladších členov domácnosti.</p>' +
        '<div class="tvPackageCodebook" data-tvpackagecodebook="ZAKLADNY_M_SAT">&nbsp;</div>'
    ].join("\n")
  );
  $(magiosatMbalik).appendTo($magioBalikHolder);
} else if (window.location.href.indexOf("/#magioL") > -1) {
  $("p.activation").text("pri 24 mes. viazanosti 15,00 €");
  TVcheckerUsed(
    "https://www.telekom.sk/fix/objednavka/-/scenario/e-shop/chytry-balik?televizia=l"
  );
  $(".tvheadline").text("MAGIO TV SAT L");

  $(".pricewrap .priceLeft .months").text("2. a 3. mesiac");
  $(".pricewrap .priceRight .activation").text(
    "pri 24 mes. viazanosti 10,90 €"
  );
  $(".pricewrap .priceRight .strikeThrough").text("v predajni 15,90 €");
  $(".pricewrap .priceRight .otherMonths").text("15,90 €");

  var magiosatLbalik = $(
    [
      '<p class="halbFett">Magio TV SAT L</p>' +
        '<p class="magenta">Základný balík programov</p>' +
        '<p class="normal"><b>Pre ešte viac zábavy, športových zážitkov a filmových hrdinov rovno u vás doma</b> je tento balíček ako stvorený. Zabudnite na starosti a aspoň na chvíľu sa zrelaxujte pri tom najlepšom programe, ktorý sme vám v balíčku nachystali.</p>' +
        '<p class="normal">Magio Televízia L obsahuje aj programy Magio Televízie M.</p>' +
        '<div class="tvPackageCodebook" data-tvpackagecodebook="SAT_L_NEW">&nbsp;</div>'
    ].join("\n")
  );
  $(magiosatLbalik).appendTo($magioBalikHolder);
} else if (window.location.href.indexOf("/#magioXL") > -1) {
  $("p.activation").text("pri 24 mes. viazanosti 20,00 €");
  TVcheckerUsed(
    "https://www.telekom.sk/fix/objednavka/-/scenario/e-shop/chytry-balik?televizia=xl"
  );
  $(".tvheadline").text("MAGIO TV SAT XL");

  $(".pricewrap .priceLeft .months").text("2. a 3. mesiac");
  $(".pricewrap .priceRight .activation").text(
    "pri 24 mes. viazanosti 15,90 €"
  );
  $(".pricewrap .priceRight .strikeThrough").text("v predajni 20,90 €");
  $(".pricewrap .priceRight .otherMonths").text("20,90 €");

  var magiosatXLbalik = $(
    [
      '<p class="halbFett">Magio TV SAT XL</p>' +
        '<p class="magenta">Základný balík programov</p>' +
        '<p class="normal">Magio Televízia XL obsahuje aj programy Magio Televízie M a L.</p>' +
        '<div class="tvPackageCodebook" data-tvpackagecodebook="SAT_XL_NEW">&nbsp;</div>'
    ].join("\n")
  );
  $(magiosatXLbalik).appendTo($magioBalikHolder);
}

/* TOOLTIPS */
$("#triZadarmo, #tri")
  .qtip({
    content:
      "V cene Magio TV M si môžete vybrať 3 programové balíky podľa seba. Ďalšie si môžete dokúpiť za 1,97€/mes. Balíky, ktoré si vyberiete, aktivujete pridaním do košíka po tom, ako overíte dostupnosť a prejdete k nákupu.",
    style: {
      classes: "qtip-tipsy"
    },
    position: {
      my: "bottom right",
      at: "top right"
    }
  })
  .bind("click", function(event) {
    event.preventDefault();
    return false;
  });
//hotfix for accordion qtip
$(".accordion")
  .find("#triZadarmo")
  .attr("id", "tri");

$("#sestZadarmo, #sest")
  .qtip({
    content:
      "V cene Magio TV L si môžete vybrať 6 programových balíkov podľa seba. Ďalšie si môžete dokúpiť za 1,97€/mes. Balíky, ktoré si vyberiete, aktivujete pridaním do košíka po tom, ako overíte dostupnosť a prejdete k nákupu.",
    style: {
      classes: "qtip-tipsy"
    },
    position: {
      my: "bottom right",
      at: "top right"
    }
  })
  .bind("click", function(event) {
    event.preventDefault();
    return false;
  });
//hotfix for accordion qtip
$(".accordion")
  .find("#sestZadarmo")
  .attr("id", "sest");

$("#archivS, #archivSaccord")
  .qtip({
    content:
      "Magio archív aktivujete v nákupnom košíku počas kúpy Magio TV. Ak už Magio TV máte, aktivujte si Magio archív na bezplatnej zákazníckej linke 0800 123 456.",
    style: {
      classes: "qtip-tipsy"
    },
    position: {
      my: "bottom left",
      at: "top left"
    }
  })
  .bind("click", function(event) {
    event.preventDefault();
    return false;
  });
//hotfix for accordion qtip
$(".accordion")
  .find("#archivS")
  .attr("id", "archivSaccord");

$("#archivM, #archivMaccord")
  .qtip({
    content:
      "Magio archív aktivujete v nákupnom košíku počas kúpy Magio TV. Ak už Magio TV máte, aktivujte si Magio archív na bezplatnej zákazníckej linke 0800 123 456.",
    style: {
      classes: "qtip-tipsy"
    },
    position: {
      my: "bottom right",
      at: "top right"
    }
  })
  .bind("click", function(event) {
    event.preventDefault();
    return false;
  });
//hotfix for accordion qtip
$(".accordion")
  .find("#archivM")
  .attr("id", "archivMaccord");
/* TOOLTIPS END */

/* FANCYBOX TV ARCHIV S, M */
/* POPUP */
$(".archivy").fancybox({
  padding: 10,
  margin: 10,
  closeBtn: false,
  parent: "#content",
  helpers: {
    overlay: {
      css: {
        background: "rgba(0, 0, 0, 0.7)"
      }
    }
  }
});

$(".p-close, .link-close, .close-end, .text-close").on("click", function(e) {
  e.preventDefault();
  $.fancybox.close();
});
/* POPUP END */

/* TV STANICE QTIP */
$(document).ready(function() {
  var myTimer = setInterval(function() {
    //console.log("timer");
    if (
      $(".programovyBalikHolder img").length > 2 &&
      $(".programovyBalikStyle img").length
    ) {
      clearInterval(myTimer);
    }

    // img stanice
    var tvStanicaImg = $(
      ".programovyBalikHolder img, .programovyBalikStyle img"
    );

    // zisti "alt" pre kazdu TV stanicu
    $.each(tvStanicaImg, function() {
      var attrAlt = $(this).attr("alt");

      // text pre kazdu TV stanicu
      $(this).attr("data-text", getTextQtip(attrAlt));

      $(this).qtip({
        content: {
          attr: "data-text"
        },
        style: {
          classes: "qtip-tipsy"
        },
        position: {
          my: "bottom center",
          at: "top center"
        },
        newValue: {
          Markiza: "Toto je Markiza."
        }
      });
    });
  }, 300);

  function getTextQtip(attrAlt) {
    var result = "";
    switch (attrAlt) {
      case "jednotka":
      case "jendotkasat":
        result = "Toto je popisovy text pre TV stanicu Markiza.";
        break;
      case "dvojka":
      case "Dvojka-HD":
        result = "Text pre TV stanicu Dvojka.";
        break;
      case "markiza":
      case "markizasat":
      case "Markiza":
        result = "Toto je test text pre Markizu.";
        break;
    }
    return result;
  }
});
/* TV STANICE QTIP END */
