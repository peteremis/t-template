/* TABS */
$(".tabs-menu a").click(function(event) {
  event.preventDefault();
  $(this)
    .parent()
    .addClass("current");
  $(this)
    .parent()
    .siblings()
    .removeClass("current");
  var tab = $(this).attr("href"), // select clicked tab. In example "#tab-1" ..
    parent = $(this)
      .closest("ul")
      .parent()
      .parent(); // <tabs-container>
  parent
    .find(".tab-content")
    .not(tab)
    .css("display", "none"); //hide all not clicked tabs
  $(tab).fadeIn(); //show clicked tab
});
/* TABS END */

/* SCROLL TO */
$("div").on("click", ".scroll-to", function(e) {
  var parentNav = $(this)
    .closest(".main-navigation")
    .attr("id");
  var offset = 0;

  if (parentNav === "nav-sticky-custom") {
    offset = 68;
  }
  if (
    location.pathname.replace(/^\//, "") == this.pathname.replace(/^\//, "") &&
    location.hostname == this.hostname
  ) {
    var target = $(this.hash);
    target = target.length ? target : $("[name=" + this.hash.slice(1) + "]");
    if (target.length) {
      if ($(window).width() > 550) {
        $("html,body").animate(
          {
            scrollTop:
              target.offset().top - $("#nav-sticky").outerHeight() - offset
          },
          1000
        );
      } else {
        $("html,body").animate(
          {
            scrollTop: target.offset().top - offset
          },
          1000
        );
      }
      return false;
    }
  }
});
