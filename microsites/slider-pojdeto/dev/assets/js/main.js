$(function () {
    var $slider = $('.bxslider').bxSlider({
        nextSelector: '.slider-next',
        prevSelector: '.slider-prev',
        nextText: 'Prev',
        prevText: 'Next',
        useCSS: false,
        auto: true,
        autoHover: true
    });

});
