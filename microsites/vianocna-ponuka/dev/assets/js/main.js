$(function () {
    var generateTooltip = function (element, text) {
        $(element).qtip({
            content: {
                text: text
            },
            position: {
                my: 'bottom center',
                at: 'top center'
            }
        });
    }

    generateTooltip('.ec-info_1', 'Za nákup na webe vás odmeníme napr.<br/>extra web zľavou na nový mobil alebo<br/>Magio TV, internet či Pevnú linku.');
    generateTooltip('.ec-info_2', 'Ak nakúpite cez web, môžete tovar<br/>alebo službu vrátiť do 14 dní bez<br/>udania dôvodu.');
    generateTooltip('.ec-info_3', 'Pri kúpe cez web získate <b>doručenie<br/>kuriérom zadarmo</b> kdekoľvek<br/>v rámci Slovenska.');
    
    $(".fancy").fancybox();

    $(document).on('click', '.popup-modal-dismiss', function(e) {
        e.preventDefault();
        $.fancybox.close(); 
    });

    $(document).on('click', '.popup-modal-scroll', function(e) {
        e.preventDefault();
        $.fancybox.close(); 
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top - ($("#nav-sticky").outerHeight())
                }, 1000);
                return false;
            }
        }
    }); 
});