$(function () {

    /* FANCYBOX */

    $(".fancybox").fancybox({
        parent: "#content"
    });

    $('.tooltip').qtip({
        content: {
            text: 'PAUŠÁLY HAPPY M, L a XL:<br />- Neobmedzené SMS správy na Slovensko<br />PAUŠÁLY HAPPY XXL:<br />- Neobmedzené SMS správy aj do EÚ<br />PAUŠÁLY HAPPY PROFI:<br />- Neobmedzené SMS správy do EÚ + do zóny Profi<br />PAUŠÁLY HAPPY XS MINI, XS a S:<br />- Cena SMS v roaming v CZ, HU, AT a PL je 0,072 €.'
        },
        position: {
            my: 'left center',
            at: 'right center'
        }
    });

    /* STICKY NAV */

    $("#nav-sticky").sticky({
        topSpacing: 0,
        widthFromWrapper: true
    });

    $(".scroll-to").click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top - ($("#nav-sticky").outerHeight())
                }, 1000);
                return false;
            }
        }
    });

    var contentSections = $("[id*='sec-']"),
      secondaryNav = $("#nav-sticky");

    function updateSecondaryNavigation() {
        contentSections.each(function () {
            var actual = $(this),
              actualHeight = actual.height() + parseInt(actual.css('paddingTop').replace('px', '')) + parseInt(actual.css('paddingBottom').replace('px', '')),
              actualAnchor = secondaryNav.find('a[href="#' + actual.attr('id') + '"]');

            if ((actual.offset().top - secondaryNav.outerHeight() <= $(window).scrollTop()) && (actual.offset().top + actualHeight - secondaryNav.outerHeight() > $(window).scrollTop())) {
                actualAnchor.addClass('active');
            } else {
                actualAnchor.removeClass('active');
            }

        });
    }
    updateSecondaryNavigation();

    $(window).scroll(function (event) {
        updateSecondaryNavigation();
    });

    $('<div style="text-align: left; margin-left: 70px; margin-bottom: 15px;"><a href="/documents/10179/663129/Zoznam+RP+a+dostupnost+sluzieb+09_j%C3%BAl_2020.xlsx/e795634e-d2af-4c83-b03b-aaa39f331a0b">Kompletný zoznam roamingových partnerov</a></div>').appendTo('#roaming-price');
    $('<div style="text-align: left; margin-left: 70px; margin-bottom: 15px;"><a href="/wiki/ostatne/roaming-v-eu">Politika primeraného využívania roamingových služieb v rámci EÚ, Nórska, Islandu, Lichtenštajnska, Monaka a Andory</a></div>').appendTo('#roaming-price');
});