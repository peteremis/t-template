(function() {
    var app = angular.module('roamingZonesModule', []);

    app.controller('mainCtrl', function($scope, $http, $filter) {
        var jsonData = '';
        if (window.location.hostname == "localhost") {
            jsonData = '../assets/js/roaming-zones.json'
        } else {
            jsonData = '/documents/10179/2580336/roaming-zones.json?v=3'
        }

        var vm = this;
        vm.roamingData = [];
        vm.catSort = "happy";
        vm.zoneSort = "Zóna 0";
        vm.allZones = ["Zóna 0", "Zóna 1", "Zóna 2", "Zóna 3", "Zóna 4"];
        vm.test = [];
        //load json data
        $http.get(jsonData).success(function(response) {
            vm.roamingData = response.countries;
            vm.filterFinalObj('happy');
            vm.filterFinalObj('euro');
            vm.filterFinalObj('easy');
            vm.filterFinalObj('travelSurf');
        });

        vm.finalObj = {
            zona0: [{
                happy: "",
                euro: "",
                easy: "",
                travelSurf: ""
            }],
            zona1: [{
                happy: "",
                euro: "",
                easy: "",
                travelSurf: ""
            }],
            zona2: [{
                happy: "",
                euro: "",
                easy: "",
                travelSurf: ""
            }],
            zona3: [{
                happy: "",
                euro: "",
                easy: "",
                travelSurf: ""
            }],
            zona4: [{
                happy: "",
                euro: "",
                easy: "",
                travelSurf: ""
            }]
        };

        vm.filterTags = function(category, zone) {
            vm.filtered = [];
            vm.tempArr = [];
            vm.test = [];
            vm.zoneNameFilter = $filter('removeSLow')(zone);

            angular.forEach(vm.roamingData, function(country) {
                if (country[category] === zone) {
                    vm.filtered.push(country);
                    vm.tempArr.push(country.countryName);
                }
            });

            vm.test = vm.tempArr.join(', ');
            //vm.finalObj[vm.zoneNameFilter][category] = vm.test;
            //console.log('filtrovane', vm.zoneNameFilter);
            return vm.filtered;
        };



        vm.filterFinalObj = function(category) {
            //console.log('category: ', category);
            angular.forEach(vm.allZones, function(vsetkyZony) {
                vm.filteredCountries = [];
                vm.countryNamesArr = [];
                vm.countryNameString = [];
                //console.log('vsetky', vm.allZones, vsetkyZony);
                vm.zoneNameFilter = $filter('removeSLow')(vsetkyZony);
                //console.log(vm.zoneNameFilter);
                angular.forEach(vm.roamingData, function(country) {
                    //console.log(country);
                    //console.log(country[category], vsetkyZony);
                    if (country[category] === vsetkyZony) {
                        vm.filteredCountries.push(country);
                        vm.countryNamesArr.push(country.countryName);
                    }
                });

                vm.countryNameString = vm.countryNamesArr.join(', ');
                vm.finalObj[vm.zoneNameFilter][0][category] = vm.countryNameString;
            });
           // console.log('filtrovane', vm.finalObj);
        };
    });

    app.filter('removeSLow', function() {
        return function(text) {
            var str = text.replace(/\s+/g, '').replace(/ó/g, 'o');
            return str.toLowerCase();
        };
    })

}());
