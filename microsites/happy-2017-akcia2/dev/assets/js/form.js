var fromLSB = getParameterByName('fromLSB');
var isFinished = getParameterByName('isFinished');

if (isFinished=='true') {
	window.location = "/kuponakodarcek/dakujeme-za-objednavku";
}

function formChange(inputName, value) {

	$('.' + inputName)
		.find('input').val(value).blur().show().end()
		.find('label').show().end()
		.closest('li').show().end();
}


function getParameterByName(name, url) {
	if (!url) url = window.location.href;
	name = name.replace(/[\[\]]/g, "\\$&");
	var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
		results = regex.exec(url);
	if (!results) return null;
	if (!results[2]) return '';
	return decodeURIComponent(results[2].replace(/\+/g, " "));
}


$(function () {

	var formId = getParameterByName('formId');
	var formHash = getParameterByName('formHash');

	var loggedAgent = false;
	var lsbFormSave = false;

	if (formId !== null && formId !== undefined) {
		lsbFormSave = true;
	}

	if ($('div.desktop-agentBox').length) {
		loggedAgent = true;
	}

	if (loggedAgent || lsbFormSave) {
		var lsbForm = $('#happy-form');
		var form = $("#p_p_id_dynamicForm_WAR_eccadmin_").contents();

		lsbForm.append(form);
		lsbForm.show();

		$('.section_happy').hide();

		$('#sec-1-overlay').removeClass('overlay');
		$('#sec-2-overlay').removeClass('overlay');
	} else {
		$('#happy-form').hide();
	}

});s