$(function () {

	var sec1 = $("#sec-1");
	var sec2 = $("#sec-2");

	var tokenValue = '';
	var apiUrl = 'https://backvm.telekom.sk/www/happy/check.php';

	// var apiUrl = 'http://backvm.telekom.localhost/www/happy/check.php';

	localStorage.setItem('easy', 'Nie');

	$('#msisdn').on('keyup', function (e) {
		var inputValue = e.target.value,
			$siblingBtn = $("#msisdn-btn");

		if (inputValue.match(/^\d{10}$/)) {
			enableBtn($siblingBtn);
		} else {
			disableBtn($siblingBtn);
		}
	});

	$('#code').on('keyup', function (e) {
		var inputValue = e.target.value,
			$siblingBtn = $("#code-btn");

		if (inputValue.match(/^\d{4}$/)) {
			enableBtn($siblingBtn);
		} else {
			disableBtn($siblingBtn);
		}
	});

	$('#voucher1').on('keyup', function (e) {
		checkVoucher();
	});

	$('#voucher2').on('keyup', function (e) {
		checkVoucher();
	});

	function checkVoucher() {
		var v1 = $('#voucher1').val();
		var v2 = $('#voucher2').val();

		var $siblingBtn = $("#voucher-btn");

		if (v1.match(/^\d{6}$/) && v2.match(/^[a-zA-Z0-9]{6}$/)) {
			enableBtn($siblingBtn);
		} else {
			disableBtn($siblingBtn);
		}

	}

	function disableBtn(clickedBtn) {
		var $clickedBtn = $(clickedBtn);
		if (!$clickedBtn.hasClass('btn_disabled')) {
			$clickedBtn.addClass('btn_disabled');
		}
	}

	function enableBtn(clickedBtn) {
		var $clickedBtn = $(clickedBtn);
		if ($clickedBtn.hasClass('btn_disabled')) {
			$clickedBtn.removeClass('btn_disabled');
		}
	}

	$('#msisdn-btn').click(function (e) {
		e.preventDefault();

		if ($('#msisdn-btn').hasClass('btn_disabled')) {
			return false;
		}

		//$(".vykup-loader").show();
		$("#ajaxLoadingSpinnerOverlay").show();
		sec1.removeClass('success');

		$.ajax({
			type: "GET",
			async: true,
			crossDomain: true,
			dataType: "json",
			data: "msisdn=" + $("#msisdn").val(),
			url: apiUrl,

			success: function (d) {
				//$(".vykup-loader").hide();
				$("#ajaxLoadingSpinnerOverlay").hide();

				localStorage.setItem('msisdn', $("#msisdn").val());

				var msisdnValue = localStorage.getItem('msisdn');

				if (msisdnValue==='0901727051') {
					msisdnValue='0999111222';
				}

				$("#msisdn").val('');
				msisdnValue = msisdnValue.replace(/^(.{4})(.{3})(.*)$/, "$1 $2 $3");
				formChange('happy_cislo', msisdnValue);

				$('#current-phone-number').html(msisdnValue);

				if (d.response == 'ok') {
					tokenValue = d.token;
					localStorage.setItem('token', tokenValue);

					$("#phone-form").hide();
					$("#code-form").show();
					$("#code").focus();

				} else {
					// $('#msisdn-error').html(d.desc);
					$('#msisdn-error').text(d.description);
				}

			},
			error: function () {
				//$(".vykup-loader").hide();
				$("#ajaxLoadingSpinnerOverlay").hide();

				errorMsg = 'Momentálne nie je možné overiť mobilné číslo. Skúste to prosím neskôr.';
				$('#msisdn-error').html(errorMsg);
			}
		});

	});

	$('#code-btn').click(function (e) {
		e.preventDefault();

		if ($('#code-btn').hasClass('btn_disabled')) {
			return false;
		}

		//$(".vykup-loader").show();
		$("#ajaxLoadingSpinnerOverlay").show();
		sec1.removeClass('success');

		$.ajax({
			type: "GET",
			async: true,
			crossDomain: true,
			dataType: "json",
			data: "token=" + tokenValue + "&code=" + $("#code").val(),
			url: apiUrl,

			success: function (d) {
				//$(".vykup-loader").hide();
				$("#ajaxLoadingSpinnerOverlay").hide();

				if (d.response == 'ok') {

					$("#code-form").hide();
					$("#sec-1 .success-holder").show();
					$("#voucher1").focus();

					$('#sec-2-overlay').removeClass('overlay');

					sec1.addClass('success');

				} else {
					// $('#msisdn-error').html(d.desc);
					$('#code-error').text(d.description);
				}

			},
			error: function () {
				//$(".vykup-loader").hide();
				$("#ajaxLoadingSpinnerOverlay").hide();

				errorMsg = 'Momentálne nie je možné overiť mobilné číslo. Skúste to prosím neskôr.';
				$('#code-error').html(errorMsg);
			}
		});

	});

	$('#voucher-btn').click(function (e) {
		e.preventDefault();

		if ($('#voucher-btn').hasClass('btn_disabled')) {
			return false;
		}

		//$(".vykup-loader").show();
		$("#ajaxLoadingSpinnerOverlay").show();
		sec2.removeClass('success');

		var token = localStorage.getItem('token');
		var voucher1 = $("#voucher1").val();
		var voucher2 = $("#voucher2").val();

		$.ajax({
			type: "GET",
			async: true,
			crossDomain: true,
			dataType: "json",
			data: "token=" + token + "&v1=" + voucher1 + "&v2=" + voucher2,
			url: apiUrl,

			success: function (d) {
				//$(".vykup-loader").hide();
				$("#ajaxLoadingSpinnerOverlay").hide();

				localStorage.setItem('v1', voucher1);
				localStorage.setItem('v2', voucher2);

				$('#current-voucher').html('C17X50 - ' + voucher1 + ' - ' + voucher2);

				if (d.response == 'ok') {

					$(".form2-holder").hide();
					$("#sec-2 .success-holder").show();

					sec2.addClass('success');
					checkAllSteps();

				} else {
					// $('#msisdn-error').html(d.desc);
					$('#voucher-error').text(d.description);
				}

				formChange('happy_kupon', voucher1 + ' - ' + voucher2);

			},
			error: function () {
				//$(".vykup-loader").hide();
				$("#ajaxLoadingSpinnerOverlay").hide();

				errorMsg = 'Momentálne nie je možné overiť kód kupónu. Skúste to prosím neskôr.';
				$('#msisdn-error').html(errorMsg);
			}
		});

	});

	$('#half-order').click(function (e) {
		e.preventDefault();

		if ($('#half-order').hasClass('btn_disabled')) {
			return false;
		}
		//$(".vykup-loader").show();
		$("#ajaxLoadingSpinnerOverlay").show();

		var token = localStorage.getItem('token');
		var rplName = localStorage.getItem('rplName');
		var easy = localStorage.getItem('easy');

		$.ajax({
			type: "GET",
			async: true,
			crossDomain: true,
			dataType: "json",
			data: "token=" + token + "&rplName=" + rplName + "&easy=" + easy,
			url: apiUrl,

			success: function (d) {
				$("#ajaxLoadingSpinnerOverlay").hide();
				$(".finishFormButton").click();
				window.location = "/kuponakodarcek/dakujeme-za-objednavku";
			},
			error: function () {
				$("#ajaxLoadingSpinnerOverlay").hide();

				errorMsg = 'Momentálne nie je možné overiť kód kupónu. Skúste to prosím neskôr.';
				$('#msisdn-error').html(errorMsg);
			}
		});

	});

	$('.number-change').click(function (e) {
		e.preventDefault();

		$("#phone-form").show();
		$("#code-form").hide();
		$("#code").val('');
		$("#code-msisdn").hide();
		$("#code-error").hide();
		$("#sec-1 .success-holder").hide();

		localStorage.setItem('msisdn', '');
		localStorage.setItem('token', '');

		$('#sec-2-overlay').addClass('overlay');
	});

	$('.easy-info').click(function (e) {
		e.preventDefault();

	});

	$('#vop-agree').click(function (e) {
		// e.preventDefault();

		if ($(this).is(':checked')) {
			checkAllSteps();
		} else {
			$('#half-order').addClass('btn_disabled');
		}

	});

	$('#easy-agree').click(function (e) {
		var easy = localStorage.getItem('easy');

		if (easy === 'Áno') {
			localStorage.setItem('easy', 'Nie');
			formChange('happy_easy', 'Nie');
		} else {
			localStorage.setItem('easy', 'Áno');
			formChange('happy_easy', 'Áno');
		}
	});

	function checkAllSteps() {
		if (sec1.hasClass('success') && sec2.hasClass('success') && $('#vop-agree').is(':checked')) {
			$('#half-order').removeClass('btn_disabled');
		}
	}

});
