function getQuery(q) {
	return (window.location.search.match(new RegExp('[?&]' + q + '=([^&]+)')) || [, null])[1];
}


function getBannerArray() {

	if (window.location.href.indexOf("faktury") > -1) {

		var bannerArray = [
			{header: 'Plaťte svoje faktúry jednoducho cez aplikáciu', button: 'Inštalovat do mobilu'},
			{header: 'Vybavte si svoje platby moderne cez našu aplikáciu', button: 'Inštalovat aplikáciu'},
			{header: 'Povedzte Áno novému spôsobu platenia faktúr ', button: 'Telekom v mobile'}
		];
	} else {
		var bannerArray = [
			{header: 'Dokupujte si dáta cez aplikáciu Telekom ', button: 'Inštalovat app'},
			{header: 'Vyberte si dátový balík podľa potreby', button: 'Získať aplikáciu Telekom'},
			{header: 'Rovnaké informácie pohodlnejší prístup', button: 'Získať aplikáciu Telekom'}
		];
	}

	return bannerArray[Math.floor(Math.random() * bannerArray.length)];

}

function getBanner(invoiceError) {

	var page = 'spotreba';
	var invoice = 0;

	if (window.location.href.indexOf("faktury") > -1) {
		invoice = 1;

		page = 'faktury';
		if (invoiceError == 1) {
			page = 'faktury_error';
		}

	} 	else if (window.location.href.indexOf("faktury") > -1) {
		invoice = 1;
		page = 'faktury_detail';

		if (invoiceError == 1) {
			page = 'faktury_error';
		}
	}

	if (invoice == 1) {
		var banner = {};

		if (invoiceError == 1) {
			banner = {
				page: page,
				header: 'Potrebujete okamžitý prístup k faktúram za váš mobil? Nájdete ich v Telekom aplikácii.',
				button: 'Stiahnuť aplikáciu'
			};
		} else {
			banner = {
				page: page,
				header: 'Pohodlnejší prístup k faktúram za váš mobil? Áno!',
				button: 'Získať aplikáciu'
			};
		}
	} else {
		banner = {
			page: page,
			header: 'Šetrite čas a kontrolujte si spotrebu mobilu v aplikácii',
			button: 'Stiahnuť appku'
		};
	}

	return banner;

}

function saveStats(header, os, sourcePage) {

	var redirectUrl = 'https://play.google.com/store/apps/details?id=com.telekom.portal';

	if (os == 'iOS') {
		redirectUrl = 'https://itunes.apple.com/sl/app/telekom/id582997807?mt=8';
	}

	$.ajax({
		'url': 'https://m.telekom.sk/spotreba/banner/',
		'type': 'GET',
		'data': {
			'medium': sourcePage,
			'source': header,
			'action': 'click',
			'web': 1
		},
		'success': function (data) {
			window.location.href = redirectUrl;
		}
	});

}

$(document).ready(function () {

	// var isBanner = getQuery('banner');
	var isBanner = 1;

	var spotrebaBanner = $('#spotreba-banner');
	var fakturySplatnostInfo = $('#info-splatnost-banner');

	var textBanner = $('.text-banner');
	var buttonBanner = $('.button-green-small');

	var invoiceError = 0;

	if ($('.error').is(':visible')) {
		invoiceError = 1;
	}

	var banner = getBanner(invoiceError);

	var md = new MobileDetect(window.navigator.userAgent);

	var os = md.os();

	textBanner.html(banner.header);
	buttonBanner.html(banner.button);

	spotrebaBanner.on('click', function (e) {
		e.preventDefault();
		saveStats(banner.header, os, banner.page);
	});

	if (isBanner == 1 && (os == 'AndroidOS' || os == 'iOS')) {

		if (invoiceError == 1) {
			spotrebaBanner.insertAfter(".message-block.error");
			$('.app-banner').height(150);
		} else {
			spotrebaBanner.insertAfter(".page-heading");
		}

		spotrebaBanner.show();
	}

	if (window.location.href.indexOf("faktury") > -1) {
		fakturySplatnostInfo.show();
		fakturySplatnostInfo.insertAfter(".pagination-wrap");
	}

});
