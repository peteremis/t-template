$(function () {
  /* youtube video player */
  $(".youtube-video").jqueryVideoLightning({
    autoplay: 1,
    backdrop_color: "#000",
    backdrop_opacity: 0.6,
    glow: 0,
    glow_color: "#000",
  });
});
