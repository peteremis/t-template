$(function() {
  
  /* TOOLTIP EEC */
  $('.eecat').qtip({
    content: '<b>Nakupujte cez web bez obáv!</b><br>Pri kúpe cez web môžete vrátiť službu alebo tovar bez udania dôvodu až do do 14 dní!',
    style: {
        'text-align': 'center'
    },

     position: {
         my: 'bottom center',
         at: 'top center'
     }
    });
     /* TOOLTIP EEC */
  $('#info-balik-volania').qtip({
    content: 'S paušálom ÁNO M získate neobmedzené volania, SMS a MMS do všetkých sietí v SR, zo SR do EÚ+, z EÚ+ do SR a z EÚ+ do EÚ+. a 500 MB v SR a EÚ+.',
    style: {
        'text-align': 'center'
    },

     position: {
         my: 'bottom center',
         at: 'top center'
     }
    }); 
         /* TOOLTIP EEC */
  $('#volne-data').qtip({
    content: 'V skupine Magenta 1, v ktorej máte Magio internet L, získate vy aj ostatní členovia skupiny ešte viac dát a volaní v paušáloch ÁNO. Napríklad k paušálu M  získate 500 MB mesačne navyše.',
    style: {
        'text-align': 'center'
    },

     position: {
         my: 'bottom center',
         at: 'top center'
     }
    }); 

 
});
