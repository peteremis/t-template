(function() {

    //setup before functions
    var typingTimer; //timer identifier
    var doneTypingInterval = 1000; //time in ms
    var $input = $('#email');

    var name = "Friend";
    var tour1 = new Tour({
        debug: true,
        name: "tour1",
        onEnd: function(tour) {
            //giveAward();
        },
        basePath: location.pathname.slice(0, location.pathname.lastIndexOf('/')),
        steps: [{
            path: "/index.html",
            element: ".tour1-step-one",
            title: "Tour 1 : Mobilne tel cislo",
            content: "Vyberte si Mobilne telefonne cislo a kliknite <b>POKRACOVAT<b>",
            placement: "top",
            onShown: function(tour) {
                typeCheck('radio');
            }
        }, {
            path: "/index.html",
            element: ".tour1-step-two",
            title: "Tour 1 : Mobilne tel cislo",
            content: "Zadajte Mobilne telefonne cislo, ktore chcete spristupnit.",
            placement: "left",
            onShown: function(tour) {
                typeCheck('#phone');
            }
        }, {
            path: "/index.html",
            element: ".tour1-step-three",
            title: "Tour 1 : Mobilne tel cislo",
            content: "Vyberte sposob overenia cez ID kod a zadajte ID kod.",
            placement: "top",
            onShown: function(tour) {
                typeCheck('#idKod');
            }
        }, {
            path: "/index.html",
            element: ".tour1-step-four",
            title: "Title Tour 1",
            content: "Na zaver kliknite VYTVORIT ZIADOST",
            placement: "left",
            onShown: function(tour) {
                //typeCheck('#in4');
            }
        }],
        template: "<div class='popover'> <div class='arrow'></div> <h3 class='popover-title'></h3> <div class='popover-content'></div> <div class='popover-navigation'> <div class='btn-group'> <button class='btn btn-sm btn-default' data-role='prev'>&laquo; Prev</button> <button class='btn btn-sm btn-default' data-role='next'>Next &raquo;</button> <button class='btn btn-sm btn-default' data-role='pause-resume' data-pause-text='Pause' data-resume-text='Resume'>Pause</button> </div> <br><button class='btn btn-sm btn-default' data-role='end' id='endBtn1'>X</button> </div> </div>",
    });

    var tour2 = new Tour({
        debug: true,
        name: "tour2",
        onEnd: function(tour) {
            //giveAward();
        },
        basePath: location.pathname.slice(0, location.pathname.lastIndexOf('/')),
        steps: [{
            path: "/index.html",
            element: ".tour2-step-one",
            title: "Tour 2 : pevne sluzby s fakturou",
            content: "Vyberte si Mobilne telefonne cislo a kliknite <b>POKRACOVAT<b>",
            placement: "top",
            onShown: function(tour) {
                typeCheck('radio');
            }
        }, {
            path: "/index.html",
            element: ".tour2-step-two",
            title: "Tour 2 : pevne sluzby s fakturou",
            content: "Zadajte Mobilne telefonne cislo, ktore chcete spristupnit.",
            placement: "left",
            onShown: function(tour) {
                typeCheck('#phone');
            }
        }, {
            path: "/index.html",
            element: ".tour2-step-three",
            title: "Tour 2 : pevne sluzby s fakturou",
            content: "Vyberte sposob overenia cez ID kod a zadajte ID kod.",
            placement: "top",
            onShown: function(tour) {
                typeCheck('#idKod');
            }
        }, {
            path: "/index.html",
            element: ".tour2-step-four",
            title: "Tour 2 : pevne sluzby s fakturou",
            content: "Na zaver kliknite VYTVORIT ZIADOST",
            placement: "left",
            onShown: function(tour) {
                //typeCheck('#in4');
            }
        }],
        template: "<div class='popover'> <div class='arrow'></div> <h3 class='popover-title'></h3> <div class='popover-content'></div> <div class='popover-navigation'> <div class='btn-group'> <button class='btn btn-sm btn-default' data-role='prev'>&laquo; Prev</button> <button class='btn btn-sm btn-default' data-role='next'>Next &raquo;</button> <button class='btn btn-sm btn-default' data-role='pause-resume' data-pause-text='Pause' data-resume-text='Resume'>Pause</button> </div> <br><button class='btn btn-sm btn-default' data-role='end' id='endBtn2'>X</button> </div> </div>",
    });


    var tour3 = new Tour({
        debug: true,
        name: "tour3",
        onEnd: function(tour) {
            //giveAward();
        },
        basePath: location.pathname.slice(0, location.pathname.lastIndexOf('/')),
        steps: [{
            path: "/index.html",
            element: ".tour3-step-one",
            title: "Tour 3 : Mobilne tel cislo",
            content: "Vyberte si Mobilne telefonne cislo a kliknite <b>POKRACOVAT<b>",
            placement: "top",
            onShown: function(tour) {
                typeCheck('radio');
            }
        }, {
            path: "/index.html",
            element: ".tour3-step-two",
            title: "Tour 3 : Mobilne tel cislo",
            content: "Zadajte Mobilne telefonne cislo, ktore chcete spristupnit.",
            placement: "left",
            onShown: function(tour) {
                typeCheck('#phone');
            }
        }, {
            path: "/index.html",
            element: ".tour3-step-three",
            title: "Tour 3 : Mobilne tel cislo",
            content: "Vyberte sposob overenia cez ID kod a zadajte ID kod.",
            placement: "top",
            onShown: function(tour) {
                typeCheck('#idKod');
            }
        }, {
            path: "/index.html",
            element: ".tour3-step-four",
            title: "Title Tour 3",
            content: "Na zaver kliknite VYTVORIT ZIADOST",
            placement: "left",
            onShown: function(tour) {
                //typeCheck('#in4');
            }
        }],
        template: "<div class='popover'> <div class='arrow'></div> <h3 class='popover-title'></h3> <div class='popover-content'></div> <div class='popover-navigation'> <div class='btn-group'> <button class='btn btn-sm btn-default' data-role='prev'>&laquo; Prev</button> <button class='btn btn-sm btn-default' data-role='next'>Next &raquo;</button> <button class='btn btn-sm btn-default' data-role='pause-resume' data-pause-text='Pause' data-resume-text='Resume'>Pause</button> </div> <br><button class='btn btn-sm btn-default' data-role='end' id='endBtn3'>X</button> </div> </div>",
    });
    //check if user is typing
    function typeCheck(id) {
        //on keyup, start the countdown
        if (id == "radio") {
            //Check if it's a radio button
            // var radioButton = ;
            // if(radioButton.type == 'radio')
            // {
            //     //Check if it's checked
            //     if(radioButton.checked)
            //     {
            //         //current radio button is checked
            //         flagRadioButtonChecked=true;
            //         //breaking from here will keep reference of checked radio button to radioButton variable
            //         break;
            //     }
            // }
        } else {
            $(id).on('keyup', function() {
                clearTimeout(typingTimer);
                typingTimer = setTimeout(doneTyping, doneTypingInterval);
                console.log(":::::::KEY UP:::::::::");
            });
            //on keydown, clear the countdown 
            $input.on('keydown', function() {
                clearTimeout(typingTimer);
            });
        }
    }

    //user is "finished typing," do something
    function doneTyping(id) {
        // console.log(":::::::test::::::id");
        tour1.next();
    }

    //remove tour
    function removeTour() {
        localStorage.setItem('tour_end', 'yes');
        localStorage.setItem('tour_current_step', '');
        localStorage.setItem('tour_redirect_to', '');
        localStorage.clear();
    }

    //start tour
    function startTour1() {
        localStorage.clear();
        //tour1.init();
        tour1.restart();
        tour1.start();
        
    }

    function startTour2() {
        localStorage.clear();
        //tour1.init();
        tour2.restart();
        tour2.start();
        
    }

    function startTour3() {
        localStorage.clear();
        //tour1.init();
        tour3.restart();
        tour3.start();
        
    }

    // Next step in tour
    function skipToNextStep(tourNumber1) {
        //console.log(tourNumber1);
        switch (tourNumber1) {
            case "1":
                console.log("tour1");
                 tour1.next();
                break;
            case "2":
                console.log("tour2");
                 tour2.next();
                break;
            case "3":
                console.log("tour3");
                 tour3.next();
                break;
            default:
                console.log("Sorry, not defined" + tourNumber1 + ".");
        }
    }

    var tourNumber = "1";

    $('#tourNext').click(function(event) {
         //tour1.next();
         event.preventDefault();
        skipToNextStep(tourNumber);
        console.log(tourNumber);
    })

    //start tour on clik 
    $('#startTour1').click(function() {
        var tourNumber = "1";
        startTour1();
        console.log(tourNumber);
    });
    //start tour on clik 
    $('#startTour2').click(function() {
        var tourNumber = "2";
        startTour2();
    });

    //start tour on clik 
    $('#startTour3').click(function() {
        var tourNumber = 3;
        startTour3();
    });

    //Pokracovat btn

 
    //end/close tour btn
    $('#endBtn1, #createRequest').click(function() {
        removeTour();
        tour1.end();
    });
    $('#endBtn2, #createReques').click(function() {
        removeTour();
        tour2.end();
    });

    $('#endBtn3, #createReques').click(function() {
        removeTour();
        tour2.end();
    });

    //load tours steps
    tour1.init();
    tour2.init();
    tour3.init();



}());
