
  /* GET PARAMETERS FROM URL */
  function getUrlParameter(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)");
    var results = regex.exec(location.search);
    return results === null
      ? ""
      : decodeURIComponent(results[1].replace(/\+/g, " "));
  }
  /* END GET PARAMETERS FROM URL */


$(document).ready(function () {
    /* TABS */
    $(".tabs-menu a").click(function(event) {
      event.preventDefault();
      $(this)
        .parent()
        .addClass("current");
      $(this)
        .parent()
        .siblings()
        .removeClass("current");
      var tab = $(this).attr("href"), // select clicked tab. In example "#tab-1" ..
        parent = $(this)
          .closest("ul")
          .parent()
          .parent(); // <tabs-container>
      parent
        .find(".tab-content")
        .not(tab)
        .css("display", "none"); //hide all not clicked tabs
      $(tab).fadeIn(); //show clicked tab
    });

  /* TOOLTIPS */
  $("#info-discount").qtip({
    content:
      "Zľavu až do 30 € získate na nové zariadenie pri prechode a predĺžení viazanosti. Zľava sa uplatní po overení objednávky agentom, ktorý Vás bude kontaktovať po jej odoslaní.",
    style: {
      classes: "qtip-tipsy",
    },

    position: {
      my: "bottom center",
      at: "top center",
    },
  });

  $("#b2b-net-info-discount").qtip({
    content:
      "Zľavu až do 30 € získate na nové zariadenie pri prechode a predĺžení viazanosti s Biznis NETom, ako samostatnou službou alebo v kombinácii s TV alebo Biznis linkou. Zľava sa uplatní po overení objednávky agentom, ktorý Vás bude kontaktovať po jej odoslaní.",
    style: {
      classes: "qtip-tipsy",
    },

    position: {
      my: "bottom center",
      at: "top center",
    },
  });

  $("#b2b-tv-info-discount").qtip({
    content:
      "Zľavu až do 30 € získate na nové zariadenie pri prechode a predĺžení viazanosti s Biznis TV, ako samostatnou službou alebo v kombinácii s Biznis NETom alebo Biznis linkou. Zľava sa uplatní po overení objednávky agentom, ktorý Vás bude kontaktovať po jej odoslaní.",
    style: {
      classes: "qtip-tipsy",
    },

    position: {
      my: "bottom center",
      at: "top center",
    },
  });

  $("#info-zlava").qtip({
    content:
      "Ak si kúpite Magio internet a TV cez web, na každú službu vám dáme extra zľavu 10 €. Ak si vezmete aj pevnú linku, môžete získať zľavu až 30 €. Zľava sa rozdelí napoly a odráta 2. a 3. mesiac z mesačného poplatku.",
    style: {
      classes: "qtip-tipsy",
    },

    position: {
      my: "bottom center",
      at: "top center",
    },
  });


    $(".rpln-holder-slider").slick({
      dots: false,
      infinite: false,
      speed: 300,
      slidesToShow: 3,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 520,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
          },
        },
      ],
    });
  

  /* TABS */
  $(".rpln-tabs-menu a").click(function (event) {
    event.preventDefault();  
    $(this).parent().addClass("current");
    $(this).parent().siblings().removeClass("current");
    var tab = $(this).attr("href"); // select clicked tab. In example "#tab-1" ..
    parent = $(this).closest("ul").parent().parent(); // <tabs-container>
    parent.find(".tab-content").not(tab).css({"height":"0","opacity":"0"}); //hide all not clicked tabs
    $(".rpln-holder-slider").css({"height":"0","opacity":"0"});
    //$(tab).fadeIn(); //show clicked tab
    $(tab).css({"height":"auto","opacity":"1"});
    $(tab).find(".rpln-holder-slider").css({"height":"auto","opacity":"1"});
  });

  /* FETCH DATA FROM JSON */

function getPrice() {
 if (segment == "b2b") {
  //var myUrl = "assets/js/price-b2b.json"
  var myUrl = "https://backvm.telekom.sk/www/phones/export_vianoce.php?type=fix&b2b=2"
  } else {
  //var myUrl = "assets/js/price-b2c.json"
  var myUrl = "https://backvm.telekom.sk/www/phones/export_vianoce.php?type=fix"
  }
    $.ajax({
      url: myUrl,
      cache: false,
      async: true,
      crossDomain: true,
      type: "GET",
      dataType: "json",
      success: function (data) {
        // -------------------------------------------------------------------------------
        data.fix.forEach(function (itemData) {
          if (itemData.product_code === deviceId) {
            pricelist = itemData;
          }
        }); // !!! SET PROUCT ID !!!
        // -------------------------------------------------------------------------------

        if (pricelist.is_visible == 1){
        if (segment == "b2b") {

            if (productType == "net") {
              if (use_checker) {
                var adress = JSON.parse(localStorage.getItem("b2b_checker_address"));
                var checker = JSON.parse(localStorage.getItem("b2b_checker_technology"));
            
                if (adress != null) {
                  $(".rpln-label").text("Zvoľte si variant služby pre adresu "+adress.city+", "+adress.street+" "+adress.streetNum);
                }
                if (checker != null) {
                  if (checker.adsl.value == 1) {
                    $("#optik").hide();
                    $("#vdsl").hide();
                    selectedRp = selectedRp+"_adsl";
                    if (checker.adsl.rpln.M == undefined) {
                      $("#klasikNet_m_adsl").addClass("rpln-disabled");
                      $("#klasikNet_m_adsl").removeClass("my-rpln");
                    }
                    if (checker.adsl.rpln.mp == undefined) {
                      $("#klasikNet_mplus_adsl").addClass("rpln-disabled");
                      $("#klasikNet_mplus_adsl").removeClass("my-rpln");
                    }
                    if (checker.adsl.rpln.xl == undefined) {
                      $("#klasikNet_xl_adsl").addClass("rpln-disabled");
                      $("#klasikNet_xl_adsl").removeClass("my-rpln");
                    }
                  }
                  if (checker.vdsl.value == 1) {
                    $("#optik").hide();
                    $("#adsl").hide();

                    if (checker.vdsl.rpln.M == undefined) {
                      $("#klasikNet_m_adsl").addClass("rpln-disabled");
                      $("#klasikNet_m_adsl").removeClass("my-rpln");
                    }
                    if (checker.vdsl.rpln.MP == undefined) {
                      $("#klasikNet_mplus").addClass("rpln-disabled");
                      $("#klasikNet_mplus").removeClass("my-rpln");
                    }
                    if (checker.vdsl.rpln.LP == undefined) {
                      $("#klasikNet_lplus").addClass("rpln-disabled");
                      $("#klasikNet_lplus").removeClass("my-rpln");
                    }
                    if (checker.vdsl.rpln.XL == undefined) {
                      $("#klasikNet_xl").addClass("rpln-disabled");
                      $("#klasikNet_xl").removeClass("my-rpln");
                    }
                    if (checker.vdsl.rpln.XXL == undefined) {
                      $("#klasikNet_xxl").addClass("rpln-disabled");
                      $("#klasikNet_xxl").removeClass("my-rpln");
                    }
                  }
                  if (checker.optic.value == 1) {
                    $("#adsl").hide();
                    $("#vdsl").hide();
                  }
                } 
              }
              $("#b2b-net-price").css({"height":"auto","opacity":"1"});
              $("#b2b-tv-price").hide();
              $("#b2c-price").hide();
              $("#biznis-delivery").hide();
              var tech = $("#" + selectedRp).attr("category");
              $("#" + tech).click();           
            }
            if (productType == "tv") {
              $("#b2b-tv-price").css({"height":"auto","opacity":"1"});
              $("#b2c-price").hide();
              $("#b2b-net-price").hide();
              $("#biznis-delivery").hide();
            }
          } else {
            $("#b2c-price").css({"height":"auto","opacity":"1"});
            $("#b2b-net-price").hide();
            $("#b2b-tv-price").hide();
            $("#biznis-delivery").show();
          }



          $("#" + selectedRp).click().focus(); // SET DEFAULT RATEPLAN
          if (pricelist.is_orderable == 0){       
            $(".cta").attr("href", "#");
            $(".cta").addClass("btn_disabled");
            $(".cta").text("DOSTUPNÉ ČOSKORO");
          }
        } else {
          $("#soldOut").show();
        }
      },
    });
  }
function priceTab(tax){
if (segment == "b2c") {

  if ($("#" + segment +"-"+ productType +"-discount").enhancedSwitch('state')){
    $("#" + segment +"-"+ productType + "-rc").html(
      ((pricelist.prices[selectedRp].rc_sale_web/tax).toFixed(2)).replace(".", ",") + " € <span class='monthly'> mesačne po zľave</span>"
    );   
    $("#" + segment +"-"+ productType + "-oc").html(
      ((pricelist.prices[selectedRp].oc_sale_web/tax).toFixed(2)).replace(".", ",") + " € <span class='monthly'> akontácia po zľave</span>"
    );
    if (pricelist.prices[selectedRp].oc_sale_web < pricelist.prices[selectedRp].oc ){
      $("#" + segment +"-"+ productType + "-normal-oc").html(
        ((pricelist.prices[selectedRp].oc/tax).toFixed(2)).replace(".", ",") + " €"
      );
    } else {
      $("#" + segment +"-"+ productType + "-normal-oc").html("");
    }
    if (pricelist.prices[selectedRp].rc_sale_web < pricelist.prices[selectedRp].rc ){
      $("#" + segment +"-"+ productType + "-normal-rc").html(
        ((pricelist.prices[selectedRp].rc/tax).toFixed(2)).replace(".", ",")   + " €"
      );
    } else {
      $("#" + segment +"-"+ productType + "-normal-rc").html("");
    }
    $(".normal-price").fadeIn();
 

  } else {
    $(".normal-price").hide();
    $("#" + segment +"-"+ productType + "-rc").html(
      ((pricelist.prices[selectedRp].rc/tax).toFixed(2)).replace(".", ",") + " € <span class='monthly'> mesačne</span>"
    );
    $("#" + segment +"-"+ productType + "-oc").html(
      ((pricelist.prices[selectedRp].oc/tax).toFixed(2)).replace(".", ",") + " € <span class='monthly'> akontácia</span>"
    );
  }

}
if (segment == "b2b"){

  if ($("#" + segment +"-"+ productType +"-discount").enhancedSwitch('state')){
    $("#b2b"+"-"+ productType + "-rc").addClass("sale-price");
    $("#b2b"+"-"+ productType + "-oc").addClass("sale-price");
    $("#" + segment +"-"+ productType + "-rc").html(
      ((pricelist.prices[selectedRp].RC.rc_sale/tax).toFixed(2)).replace(".", ",") + " € <span class='monthly'> mesačne po zľave</span>"
    );
    
    $("#" + segment +"-"+ productType + "-oc").html(
      ((pricelist.prices[selectedRp].OC.oc_sale/tax).toFixed(2)).replace(".", ",") + " € <span class='monthly'> akontácia po zľave</span>"
    );
    if (pricelist.prices[selectedRp].OC.oc_sale < pricelist.prices[selectedRp].OC.oc ){
      $("#" + segment +"-"+ productType + "-normal-oc").html(
        ((pricelist.prices[selectedRp].OC.oc/tax).toFixed(2)).replace(".", ",") + " €"
      );
    }else {
      $("#" + segment +"-"+ productType + "-normal-oc").html("");
    }
    if (pricelist.prices[selectedRp].RC.rc_sale < pricelist.prices[selectedRp].RC.rc ){
      $("#" + segment +"-"+ productType + "-normal-rc").html(
        ((pricelist.prices[selectedRp].RC.rc/tax).toFixed(2)).replace(".", ",") + " €"
      );
    } else {
      $("#" + segment +"-"+ productType + "-normal-rc").html("");
    }
    $(".normal-price").fadeIn();


  } else {
    $("#b2b"+"-"+ productType + "-rc").removeClass("sale-price");
    $("#b2b"+"-"+ productType + "-oc").removeClass("sale-price");
    $(".normal-price").hide();
    $("#" + segment +"-"+ productType + "-rc").html(
      ((pricelist.prices[selectedRp].RC.rc/tax).toFixed(2)).replace(".", ",") + " € <span class='monthly'> mesačne</span>"
    );
    $("#" + segment +"-"+ productType + "-oc").html(
      ((pricelist.prices[selectedRp].OC.oc/tax).toFixed(2)).replace(".", ",") + " € <span class='monthly'> akontácia</span>"
    );
  }

}


}  

  /* SET NEW PRICES ON PAGE */
  function setNewPrices() {
 
  // B2C //
  if (segment == "b2c") {
    priceTab(1);
      $("#b2c-rpln-price-sale").text(
        ((rplnInfo.b2c[selectedRp].prices.tax_price - 5).toFixed(2)).replace(".", ",") + " € s online zľavou"        
      );
      $("#b2c-rpln-price").text(
        (rplnInfo.b2c[selectedRp].prices.tax_price.toFixed(2)).replace(".", ",") + " € zvyšné mesiace"
      );
    }
  // B2B //
  if (segment == "b2b"){
    if ($("#"+productType+"-dph").enhancedSwitch('state')){
      priceTab(1);
      $(".txt-dph").text(" s DPH");
      $("#b2b-"+productType+"-rpln-price").html(
        ((rplnInfo.b2b[selectedRp].prices.tax_price).toFixed(2)).replace(".", ",") + " € <span class='monthly'> mesačne s DPH</span>"        
      );
    } else {
      priceTab(1.2);
      $(".txt-dph").text(" bez DPH");
      $("#b2b-"+productType+"-rpln-price").html(
        ((rplnInfo.b2b[selectedRp].prices.taxless_price).toFixed(2)).replace(".", ",") + " € <span class='monthly'>mesačne bez DPH</span>"        
      );
    }
          
      $('#b2b-buyBtn').attr("href", "/biznis/vianoce/objednavka?zariadenie="+deviceId+"&rp="+selectedRp+"&produkt="+produkt); //SET URL TO BUY BUTTON  
      $('#b2b-tv-buyBtn').attr("href", "/biznis/vianoce/objednavka?zariadenie="+deviceId+"&rp="+selectedRp+"&produkt="+produkt);
  }
}

/*  TOGLE RPLN   */
  $(".my-rpln").click(function () {
    selectedRp = $(this).prop("id"); // GET NEW VALUE FROM BUTTON
    if (($(this).attr("category")) == "adsl") {
      selectedRp = $(this).attr("data-value"); 
    }
    $(".my-rpln").removeClass("active-rpln");
    $(this).addClass("active-rpln");
    setNewPrices();
  });

/*  NEW SWITCHER   */
  $(".switch").enhancedSwitch(); 
  $(".switch").click(function() {
    var selectedSwitch = $(this);
    selectedSwitch.enhancedSwitch('toggle');
    setNewPrices(); 
  });

/*  TO PREPARE GLOBAL VARIABLE */
  var selectedRp = "";
  var pricelist = [];
  var produkt = "";
  var productType ="";
  var technology ="";
  var availableRp = [];

/*  GET URL PARAMETERS */
  selectedRp = getUrlParameter("selectedrp"); // GET DATA FROM URL
  produkt = getUrlParameter("produkt");
  technology = getUrlParameter("technology");
  use_checker = true;

/*  START AND SET DEFAULT VALUES */
  if (segment == "b2b") {
    $(".link").attr("href","/biznis/vianoce"); 
    $(".eec-product").hide();
    $("#b2c-price").hide();
    if (produkt == "magio-tv") {
      productType = "tv";
      $("#b2b-tv-discount").enhancedSwitch('setTrue');
      $("#tv-dph").enhancedSwitch('setTrue');
      if (selectedRp == "") {
        selectedRp = "biznisTv_l";   
      } 
      getPrice();
    } else {
      productType = "net";
      produkt = "biznisNet";
      $("#b2b-net-discount").enhancedSwitch('setTrue');
      $("#net-dph").enhancedSwitch('setTrue');

      if (selectedRp == "") {
          use_checker = false;
          selectedRp = "klasikNet_lplus";  
        
      }
      getPrice();
    }
  }
  if (segment !== "b2b") {
       
    $("#b2b-net-price").hide();
    $("#b2b-tv-price").hide();
    productType = "all";
    $("#b2c-all-discount").enhancedSwitch('setTrue');
    segment = "b2c";
    if (selectedRp == "") {
      selectedRp = "L";
    } 
    getPrice();
  }

});
