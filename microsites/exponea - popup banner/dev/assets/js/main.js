function openBannerPopup(
  wantImage_Variant,
  wantWhite_Variant,
  mainText,
  subText,
  urlLink,
  mainImg,
  ecImg
) {
  var showImg = wantImage_Variant;
  var whiteVariant = wantWhite_Variant;
  var variant = null;
  // prettier-ignore
  variant =
    '<div id="exponea-popupContent" class="'+( whiteVariant ? "white" : '')+'">' +
        '<div class="p-head">' +
            '<div class="p-close">' +
                '<a href="#" class="close-btn '+( whiteVariant ? "white" : '')+'"><!-- close btn --></a>' +
            '</div>' +
        '</div>' +
        '<div class="p-content">' +
            '<div class="popup-lightbox clearfix">';
  // prettier-ignore
  if (showImg) {
    variant +=
                '<div class="img-wrapper">' +
                    '<img src="'+mainImg+'" alt="" class="mainImg">' +
                    '<img src="'+ecImg+'" alt="" class="ec">' +
                "</div>";
    }
  // prettier-ignore
  variant +=
                '<div class="content-wrapper '+( !showImg ? "width100" : '')+'">'+
                    (!showImg ? '<img src="'+ecImg+'" alt="" class="ec-only">' : '')+
                    '<h4 class="'+( !showImg ? "noimg" : '')+' '+( whiteVariant ? "white" : '')+'">'+mainText+'</h4>' +
                    '<p class="'+( !showImg ? "noimg" : '')+' '+( whiteVariant ? "white" : '')+'">'+subText+'</p>' +
                    '<a class="btn cta '+( !showImg ? "noimg" : '')+'" href="'+urlLink+'">ZISTIŤ VIAC</a>' +
                '</div>' +
            '</div>' +
        '</div>' +
    '</div>';

  setTimeout(function() {
    $.fancybox({
      content: variant,
      padding: 0,
      margin: 0,
      closeBtn: false,
      parent: "#content",
      wrapCSS: "exponeaPopupContent",
      helpers: {
        overlay: {
          closeClick: false,
          css: {
            background: "rgba(0, 0, 0, 0.7)",
            overflow: "visible"
          }
        }
      }
    });
  }, 1000);

  $(document).on("click", ".text-close, .close-btn", function(e) {
    e.preventDefault();
    $.fancybox.close();
  });
}

// invoke banner popup on page load
var imageInBanner = true;
var whiteVariant = false;
openBannerPopup(
  imageInBanner,
  whiteVariant,
  "Samsung<br>GALAXY FLIP Z",
  "teraz v telekome<br>za super cenu",
  "www.google.sk",
  "../assets/img/mobil_SamsungFlip 2.png",
  "../assets/img/EC.png"
);
