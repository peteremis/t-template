var $rateplanName = $('.rateplan-name');
var $rateplanButtonName = $('#button-rateplan-name');

var $rateplanPriceMonth = $('.rateplan-price-month span');
var $rateplanMonthBefore = $('.rateplan-price-month-before');
var $rateplanPriceMonthBefore = $('.rateplan-price-month-before span');

var $rateplanPriceB2B = $('.rateplan-price-month-b2b');
var $rateplanPriceMonthB2B = $('.rateplan-price-month-b2b span');
var $rateplanMonthBeforeB2B = $('.rateplan-price-month-before-b2b');
var $rateplanPriceMonthBeforeB2B = $('.rateplan-price-month-before-b2b span');

var $rateplanPriceDescB2B = $('.rateplan-price-month-desc-b2b');

var $rateplanData = $('.rateplan-desc-data');
var $rateplanVoice = $('.rateplan-desc-voice');
var $rateplanSms = $('.rateplan-desc-sms');
var $iconBackLp = $('.icon-back-lp');
var $billingEmail = $("#billing-email");
var $buttonSubmit = $("#tariff-change-submit");

var $emailPlaceholder = $("#email_placeholder_ok");
// var emailPlaceholderText = 'Čakajte prosím na načítanie informácií';
// var emailPlaceholderText = 'E-mail pre prijímanie elektronických faktúr <span class="required">*</span>';

var $rateplan = '';

var dataURL = 'https://m.telekom.sk/api/tariffchange?msisdn=';
var offerURL = 'https://m.telekom.sk/api/tariffchange/offer?id=';

var lpURL = 'https://www.telekom.sk/volania/pausal';
var lpURLBiznis = 'https://www.telekom.sk/biznis/pausal';
var tyURL = 'https://www.telekom.sk/volania/zmena-pausalu/dakujeme-za-objednavku';

console.log(window.location.hostname);

if (window.location.hostname === "prx01.dk2.itc.telekom.sk") {
  lpURL = 'https://prx01.dk2.itc.telekom.sk/volania/pausal-bd';
  tyURL = 'https://prx01.dk2.itc.telekom.sk/volania/pausal-zmena/dakujeme';
  dataURL = 'https://mb.telekom.sk/api/tariffchange?msisdn=';
  offerURL = 'https://mb.telekom.sk/api/tariffchange/offer?id=';
} else if (window.location.hostname === "wwt.telekom.sk" || window.location.hostname === "localhost") {
  lpURL = 'https://wwt.telekom.sk/volania/pausal';
  tyURL = 'https://wwt.telekom.sk/volania/pausal-zmena/dakujeme';
  dataURL = 'https://mb.telekom.sk/api/tariffchange?msisdn=';
  offerURL = 'https://mb.telekom.sk/api/tariffchange/offer?id=';
}

var offer = [];

var msisdn = '';
var redirect = 1;
var rpCodeParam = getParameterByName('rpCode');
var offerId = getParameterByName('id');

// var segment = 'b2c';
var segment = 'b2b';
var isRateplanValid = '';

$(document).ready(function () {

  nba(null, null).then(function (data) {
    // $.when(nba(null, null)).then(function (data, textStatus, jqXHR) {

    // console.log(textStatus);
    // console.log(data);
    // console.log(jqXHR);

    if (data.status == 'error') {
      window.location.href = lpURL;
    } else {
      msisdn = data['offer']['msisdn'];
      offer = data['offer']['productOffers'];

      isRateplanValid = checkIfRateplanIsValid(rpCodeParam, offer)
      if (isRateplanValid == '') {
        window.location.href = lpURL;
      }

      if (data['offer']['verifyStatus'] == 'BAD_REQUEST') {
        window.location.href = lpURL;
      }

      setEmail(msisdn);
      parseRpln(rpCodeParam, isRateplanValid);
    }
  });

});

function parseRpln(rpCodeParam, isRateplanValid) {

  if (rpCodeParam) {
    var rateplan = getPausal(rpCodeParam)

    if (rateplan != '') {

      var originalPrice = rateplan['cena'];
      var discountedPrice = originalPrice;

      if (isRateplanValid.hasOwnProperty('discountedPrice')) {
        discountedPrice = isRateplanValid.discountedPrice.toString();
      }

      if (isRateplanValid.hasOwnProperty('originalPrice')) {
        // originalPrice = isRateplanValid.originalPrice.toString();
      }

      if (discountedPrice == originalPrice || discountedPrice+',00' == originalPrice) {
        var discount = 0;
      } else {
        var discount = 1;
      }

      $rateplanName.html(rateplan['name']);
      $rateplanButtonName.html(rateplan['name']);
      $rateplanPriceMonth.html(discountedPrice);
      $rateplanData.html(rateplan['data'][0] + ' ' + rateplan['data'][1]);
      $rateplanVoice.html(rateplan['volania'][0] + ' ' + rateplan['volania'][1]);
      $rateplanSms.html(rateplan['sms'][0] + ' ' + rateplan['sms'][1]);

      segment = rateplan['segment'];
      // segment = 'b2b';

      if (discount == 1) {
        $rateplanMonthBefore.show();
        $rateplanPriceMonthBefore.html(originalPrice);
      }

      if (segment == 'b2b') {
        //vymenime URL v ikonke nalavo hore
        $iconBackLp.attr("href", lpURLBiznis);

        var priceWithVat = parseFloat(originalPrice.replace(",", "."));
        var priceWoVat = Number((priceWithVat / 1.20).toFixed(2));

        var priceWithVatDiscounted = parseFloat(discountedPrice.replace(",", "."));
        var priceWoVatDiscounted = Number((priceWithVatDiscounted / 1.20).toFixed(2));

        $rateplanPriceB2B.show();
        $rateplanPriceDescB2B.show();
        $rateplanPriceMonthB2B.html(priceWoVatDiscounted);

        if (discount == 1) {
          $rateplanMonthBeforeB2B.show();
          $rateplanPriceMonthBeforeB2B.html(priceWoVat);
        }
      } else {
        //vymenime URL v ikonke nalavo hore
        $iconBackLp.attr("href", lpURL);

        $rateplanPriceB2B.hide();
        $rateplanPriceDescB2B.hide();
      }
      redirect = 0;
    }
  }

  if (redirect == 1) {
    window.location.href = lpURL;
  }

}

function round(value, exp) {
  if (typeof exp === 'undefined' || +exp === 0)
    return Math.round(value);

  value = +value;
  exp = +exp;

  if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0))
    return NaN;

  // Shift
  value = value.toString().split('e');
  value = Math.round(+(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp)));

  // Shift back
  value = value.toString().split('e');
  return +(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp));
}

$iconBackLp.click(function () {
  event.preventDefault();
  window.location.href = lpURL;
});

function getParameterByName(name, url) {
  if (!url) {
    url = window.location.href;
  }
  name = name.replace(/[\[\]]/g, "\\$&");
  var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
    results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return '';
  return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function getPausal(rpCode) {

  var returnRateplan = '';

  for (var i = 0; i < dataRatePlanes.length; i++) {
    // console.log(dataRatePlanes[i]);

    if (dataRatePlanes[i]['rpCode'] == rpCode) {
      returnRateplan = dataRatePlanes[i];
    }
  }

  return returnRateplan;
}

function checkIfRateplanIsValid(rpCode, offer) {

  var returnRateplan = '';

  for (var i = 0; i < offer.length; i++) {
    if (offer[i]['rpCode'] == rpCode) {
      returnRateplan = offer[i];
    }
  }

  return returnRateplan;
}

function checkPausal(rpCode,) {

  var returnRateplan = '';

  for (var i = 0; i < dataRatePlanes.length; i++) {
    console.log(dataRatePlanes[i]);

    if (dataRatePlanes[i]['rpCode'] == rpCode) {
      returnRateplan = dataRatePlanes[i];
    }
  }

  return returnRateplan;
}

function setEmail(msisdn) {

  $.ajax({
    url: dataURL + msisdn,
    type: "GET",
    async: true,
    crossDomain: true,
    dataType: "json",
    success: function (data) {

      $billingEmail.prop("readonly", false).prop("disabled", false);

      $billingEmail.val(data.email);

      if (data.email != '') {
        $buttonSubmit.prop("disabled", false);
      }

    },
    error: function (error) {
      $billingEmail.prop("readonly", false).prop("disabled", false);

      if (error) {
        console.log('error');
      }
    }
  });
}

$billingEmail.keyup(function () {
  if ($(this).val().length === 0) {
    $buttonSubmit.prop("disabled", true);
  } else {
    $buttonSubmit.prop("disabled", false);
  }
});

function setOffer(id, email, msisdn) {

  console.log('offer');

  if (window.location.hostname === "localhost") {
    // window.location.href = tyURL;
    return;
  }

  $buttonSubmit.prop("disabled", true);

  $.ajax({
    url: offerURL + id + '&email=' + email + '&msisdn=' + msisdn,
    type: "GET",
    async: true,
    crossDomain: true,
    dataType: "json",
    success: function (data) {
      console.log(data);
      window.location.href = tyURL;
    },
    error: function (error) {
      $buttonSubmit.prop("disabled", false);
      if (error) {
        console.log('error');
      }
    }
  });
}

function nba2(phone, pin) {
  url = 'http://localhost:3000/assets/js/test.json';
  // url = 'https://backvm.telekom.sk/static/microsites/__breakDance2020/dev/assets/js/test.json';

  return $.ajax({
    url: url,
    dataType: 'json',
    type: 'GET',
  });
}