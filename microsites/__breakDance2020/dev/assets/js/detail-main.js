$(document).ready(function() {
  var selectboxes = $(".bd-select");
  var noCommitment = $("#bez-pausalu");
  var availabilityBtn = $("#hw-available");
  var mainRateplans = $(
    ".bd-scenario-detail .rateplans .rateplans_wrapper_main"
  );

  // generate carousels
  bd_generateCarousel = function(el, slidesToShow) {
    const settings = {
      slidesToShow: slidesToShow,
      slidesToScroll: 1,
      variableWidth: true,
      centerMode: false,
      arrows: true,
      infinite: false,
      touchThreshold: 10,
      swipeToSlide: true,
      swipe: true,
      touchMove: true,
      draggable: true,
      prevArrow:
        "<button type='button' class='slick-prev pull-left'><i class='fa fa-angle-left' aria-hidden='true'></i></button>",
      nextArrow:
        "<button type='button' class='slick-next pull-right'><i class='fa fa-angle-right' aria-hidden='true'></i></button>"
    };

    var slider = $(el).slick(settings);

    $(window).on("resize", function() {
      if ($(window).width() < 768 && !slider.hasClass("slick-initialized")) {
        console.log("slider initialised...");
        $(el).slick(settings);
      }
    });
  };

  $(".accordion").click(function() {
    $(".plus").text(function(i, v) {
      return v === "+" ? "-" : "+";
    });
  });

  // basic features toggle
  $(".accordion").on("click", function(e) {
    e.preventDefault();
    $("#show-features").toggle("slow");
  });

  // detailed features toggle
  $(".detailed-parameters a").on("click", function(e) {
    e.preventDefault();
    $("#detail-features").toggle("slow");
  });

  // legals toggle
  $(".legals-link.legals-1").on("click", function(e) {
    e.preventDefault();
    $("#legals-1").toggle("slow");
  });

  // init <select>
  selectboxes.select2({
    theme: "classic selectbox",
    minimumResultsForSearch: -1,
    width: "100%"
  });
  //init carousels
  bd_generateCarousel(mainRateplans, 2);

  var thx_availability =
    '<div class="p-content">' +
    '<div class="popup-lightbox">' +
    '<i class="t-icon--magenta s3 iconDone">V</i>' +
    '<h4 class="headline">Ďakujeme za záujem.<br>Keď bude zariadenie znova dostupné, pošleme Vám SMS správu.</h4>' +
    '<a href="#" class="btn btn_mag-outline popup-close">Zavrieť okno</a>' +
    "</div>" +
    "</div>";

  var order_availability =
    '<div class="p-content">' +
    '<div class="popup-lightbox">' +
    '<div class="postponedIcon"><i class="t-icon--magenta s3">m</i></div>' +
    '<h4 class="headline">Upozorníme Vás, keď bude zariadenie znova dostupné</h4>' +
    '<div class="postponedOrderFormWrapper"><form id="postponedOrder" action="#" name="postponedOrder">' +
    '<div class="input-wrapper postponed-phoneNum">' +
    '<input type="text" class="input_text" name="phoneNum" required />' +
    '<label class="placeholder" for="phoneNum">Vaše kontaktné telefónne číslo</label>' +
    "</div>" +
    "</form></div>" +
    '<p class="text"><b>Súhlas s kontaktovaním</b><br>Odoslaním formulára súhlasím, aby ma spoločnosť Slovak Telekom, a.s. kontaktovala s detailnou ponukou produktov a služieb, o ktoré som týmto prejavil(a) záujem, a to telefonicky, prípadne SMS alebo MMS na mnou uvedené telefónne číslo. Potvrdzujem, že som používateľom uvedeného telefónneho čísla a mnou poskytnuté údaje nie sú údajmi tretích osôb. Viac informácií o ochrane osobných údajov nájdete</p>' +
    '<a href="#" class="btn btn_mag-outline thx-availability">Potvrdiť</a>' +
    "</div>" +
    "</div>";

  // show variant "Baz Pausalu"
  noCommitment.on("click", function(e) {
    e.preventDefault();
    $(".rateplan-detail").toggle();
    $(".no-commit").toggle();
    $(".commit").toggle();
  });
  // show avalability order form in popup
  availabilityBtn.on("click", function(e) {
    e.preventDefault();
    functions.bd_openPopup(order_availability);
  });
  // show avalability thank you popup
  $(document).on("click", ".thx-availability", function(e) {
    e.preventDefault();
    functions.bd_openPopup(thx_availability);
  });

  // popup windows close
  $(document).on("click", ".popup-close", function(e) {
    e.preventDefault();
    $.fancybox.close();
  });

  var functions = {
    bd_generatePopup: function(ratioValue, content, responsive, width) {
      $.fancybox({
        content: content,
        padding: 0,
        margin: 0,
        width: width,
        height: "auto",
        autoSize: false,
        topRatio: ratioValue,
        closeBtn: false,
        wrapCSS: "bd-scenario-popup " + responsive,
        parent: "#content",
        helpers: {
          overlay: {
            closeClick: false,
            css: {
              background: "rgba(0, 0, 0, 0.7)"
            }
          }
        }
      });
    },
    // popup window init
    bd_openPopup: function(content) {
      if ($(document).innerWidth() <= 767) {
        functions.bd_generatePopup(1, content, "mobile", "100%");
      } else {
        functions.bd_generatePopup(0.5, content, "desktop", "50%");
      }
    }
  };
});
