let personalDataForm = $("#personalDataForm");

$(personalDataForm).validate({
  rules: {
    email: {
      required: true,
      myEmail: true,
      minlength: 5
    },
  },
  submitHandler: function(form) {
    var email = $billingEmail.val();
    setOffer(offerId, email, msisdn);
  },
  // Personal inputs messages
  messages: {
    email: {
      required: "Uveďte email v tvare vas@email.sk"
    },
  },
  success: function(input) {
    input.addClass("valid");
  },
  highlight: function(input, element, errorClass, validClass) {
    $(input)
      .removeClass("valid")
      .addClass("error"); //add error class to inputs
    $(input)
      .parent()
      .find("label")
      .addClass("error"); //add error class to labels

    $(input)
      .closest("span")
      .removeClass("valid")
      .addClass("error"); // Terms checkboxes
  },
  unhighlight: function(input, element, errorClass, validClass) {
    $(input)
      .removeClass("error")
      .addClass("valid");
    $(input)
      .parent()
      .find("label")
      .removeClass("error");

    $(input)
      .next()
      .removeClass("error"); // Delivery inputs error

    $(input)
      .closest("span")
      .removeClass("error")
      .addClass("valid"); // Terms checkboxes
  },
  // error fields
  errorElement: "div",
  errorClass: "error error_message personalInputError",
  errorPlacement: function(error, element) {
    if (element.attr("type") === "checkbox") {
      $(".termsAndConditionsForm").append(error);
    } else {
      error.insertAfter(
        $(element)
          .closest(".input-wrapper")
          .find("label")
      );
    }
  }
});

// custom email validation rule
jQuery.validator.addMethod(
  "myEmail",
  function(value, element) {
    console.log('xx');
    return (
      this.optional(element) ||
      /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
        value
      )
    );
  },
  "Email musí byť v tvare vas@email.sk"
);