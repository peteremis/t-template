var heroPausaly = "";
var vsetkyPausaly = [];
for (var i = 0; i < dataPausal.length; i++) {
  if (dataPausal[i].hero)
    heroPausaly += getPausalInfo(dataPausal[i], true, [], null);

  if (typeof vsetkyPausaly[dataPausal[i].category] === "undefined")
    vsetkyPausaly[dataPausal[i].category] = "";

  vsetkyPausaly[dataPausal[i].category] += getPausalInfo(
    dataPausal[i],
    false,
    [],
    null
  );
}

(function() {
  $("#hero-slider").html(heroPausaly);

  if ($(window).width() > 767) {
    initSlider("hero-slider");
  }

  addToolTip($(".pausaly"));
})();

$(document).ready(function() {
  nba().then(function(data) {
    console.log(data);
    if (data.status === "error") return;
    if (data.verifyStatus === "INVALID_OTP") {
      showPopup(getPopupContent("wrongPin").content);
      $(".input-pin .input_text").val("");
      return;
    }
    var popup = getPopupContent(data.verifyStatus);
    var icon = data.verifyStatus === "SHOW_OFFER" ? "V" : ">";

    $(".overenie .result")
      .find(".result-icon i")
      .text(icon)
      .end()
      .find(".result-content")
      .html(popup.result)
      .end()
      .show();

    $(".overenie .form")
      .find(".input-pin .input_text")
      .val("")
      .end()
      .find(".input-phone .input_text")
      .val("")
      .end()
      .find(".input-pin")
      .hide()
      .end()
      .find(".input-phone")
      .show()
      .end();

    if (data.verifyStatus === "SHOW_OFFER") {
      $(".overenie").addClass("show-offer");
      $(".overenie .form").hide();
      $(".user-offer").show();

      setUserOffer(data.productOffers);
    }
  });
});

$(document).on("click", ".cart .cart-heading, .cart .cart-content", function(
  event
) {
  if ($(event.target).hasClass("t-icon")) return;
  if ($(window).width() > 767) return;
  var parent = $(this).closest(".carts");
  var comp = $(this).closest(".cart");

  if (comp.hasClass("selected")) {
    comp
      .find(".cart-buttons")
      .hide()
      .end()
      .find(".cart-content-full")
      .hide(400)
      .end()
      .find(".cart-content")
      .show(400)
      .end()
      .removeClass("selected");
    parent.siblings(".carts-header").css("position", "sticky");
    return;
  }
  parent.siblings(".carts-header").css("position", "relative");

  parent
    .find(".cart-buttons")
    .hide()
    .end()
    .find(".cart.selected .cart-content-full")
    .hide(400)
    .end()
    .find(".cart-content")
    .show(400)
    .end()
    .find(".cart")
    .removeClass("selected");

  comp.addClass("selected");
  comp
    .find(".cart-buttons")
    .show()
    .end()
    .find(".cart-content")
    .hide(400)
    .end()
    .find(".cart-content-full")
    .show(400)
    .end();
});

$(".category").click(function() {
  if ($(this).hasClass("selected")) return;
  $(this)
    .parents()
    .find(".category.selected")
    .removeClass("selected");

  $(this).addClass("selected");
  var index = $(this).index();

  if ($(window).width() > 767) {
    $("#pausal-slider").slick("removeSlide", null, null, true);
    $("#pausal-slider").slick("slickAdd", vsetkyPausaly[index + 1]);
  } else {
    $("#pausal-slider")
      .hide(200)
      .html(vsetkyPausaly[index + 1])
      .show(200);
  }
  addToolTip(".categories");
});

$(".vsetky-pausaly .show-categories").click(function(e) {
  e.preventDefault();
  $(".categories").toggle(200);
  if (!$("#pausal-slider .cart").length) {
    if ($(window).width() > 767) {
      setTimeout(function() {
        initSlider("pausal-slider");
        $("#pausal-slider").slick("slickAdd", vsetkyPausaly[1]);
        addToolTip(".categories");
      }, 150);
    } else {
      $("#pausal-slider").html(vsetkyPausaly[1]);
    }
  }
});

$(".viac-info").click(function() {
  if (
    $(this)
      .parent()
      .hasClass("full-info")
  )
    $(this)
      .parent()
      .removeClass("full-info");
  else
    $(this)
      .parent()
      .addClass("full-info");
});

$("#checkPin").click(function() {
  var pin = "";
  $(".input-pin .input_text").each(function() {
    pin += $(this).val();
  });

  if (pin.length !== 4) return;

  showPopup(getPopupContent("inProgress").content);

  nba(null, pin).then(function(data) {
    if (data.verifyStatus === "INVALID_OTP") {
      showPopup(getPopupContent("wrongPin").content);
      $(".input-pin .input_text").val("");
      return;
    }
    var popup = getPopupContent(data.verifyStatus);
    var icon = data.verifyStatus === "SHOW_OFFER" ? "V" : ">";

    showPopup(popup.content);

    $(".overenie .result")
      .find(".result-icon i")
      .text(icon)
      .end()
      .find(".result-content")
      .html(popup.result)
      .end()
      .show();

    $(".overenie .form")
      .find(".input-pin .input_text")
      .val("")
      .end()
      .find(".input-phone .input_text")
      .val("")
      .end()
      .find(".input-pin")
      .hide()
      .end()
      .find(".input-phone")
      .show()
      .end();

    if (data.verifyStatus === "SHOW_OFFER") {
      $(".overenie").addClass("show-offer");
      $(".overenie .form").hide();
      $(".user-offer").show();

      setUserOffer(data.productOffers);
    }
  });
});

$(document).on("click", ".show-popup", function() {
  var type = $(this).attr("data-type");
  showPopup(getPopupContent(type).content);
});

$(document).on("click", "#resetForm", function() {
  $(".overenie").removeClass("show-offer");
  $(".overenie .form").show();
  $(".overenie .result").hide();
  $(".user-offer").hide();
});

$(document).on("click", ".popup-close-focus", function(e) {
  e.preventDefault();
  $.fancybox.close();
  $(".input-pin .input_text")
    .eq(0)
    .focus();
});

function setUserOffer(data) {
  var userOffer = "";
  for (var offer = 0; offer < data.length; offer++) {
    for (var all = 0; all < dataPausal.length; all++) {
      if (data[offer].rpCode === dataPausal[all].rpCode) {
        var addon = data[offer].addon ? data[offer].addon : [];
        userOffer += getPausalInfo(dataPausal[all], false, addon, data[offer]);
      }
    }
  }

  if ($(window).width() > 767) {
    if ($("#offer-slider").hasClass("slick-initialized")) {
      $("#offer-slider").slick("removeSlide", null, null, true);
    } else {
      initSlider("offer-slider");
    }
    $("#offer-slider").slick("slickAdd", userOffer);
  } else {
    $("#offer-slider").html(userOffer);
  }
  addToolTip(".user-offer");
}

function initSlider(slider) {
  $("#" + slider).slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    arrows: true,
    infinite: false,
    swipeToSlide: true,
    swipe: true,
    touchMove: true,
    draggable: true,
    dots: true,
    responsive: [
      {
        breakpoint: 1218,
        settings: {
          slidesToShow: 3
        }
      },
      {
        breakpoint: 1000,
        settings: {
          slidesToShow: 2
        }
      },
      {
        breakpoint: 767,
        settings: "unslick"
      }
    ]
  });
}

function addToolTip(parent) {
  $(parent)
    .find(".carts .t-icon.info")
    .each(function() {
      $(this).qtip({
        content: $(this).attr("alt"),
        style: {
          classes: "qtip-tipsy"
        },
        position: {
          corner: {
            target: "leftMiddle",
            tooltip: "topMiddle"
          }
        }
      });
    });
}

/* beautify preserve:start */
// prettier-ignore

function getPopupContent(type) {
    var content = '';
    var result = '';
    var phoneNum = $("#phoneNumber").val();
    switch (type) {
        case 'inProgress':
            content = '<div class="p-content">' +
                    '<div class="popup-lightbox">' +
                        '<div class="loader-spinning">' +
                            '<i class="t-icon--magenta d-block s3">o</i>' +
                        "</div>" +
                        '<h4 class="headline">Prebieha overovanie Vášho čísla. Ďakujeme za trpezlivosť.</h4>' +
                    "</div>" +
                "</div>";
            break;

        case 'noOffer':
            content = '<div class="p-content">' +
                    '<div class="popup-lightbox">' +
                        '<img src="../assets/img/verify_failed.png" />' +
                        '<h4 class="headline">Ľutujeme, momentálne pre Vás nemáme žiadnu ponuku na mieru.</h4>' +
                        '<p class="text">Pozrite našu štandardnú ponuku paušálov.</p>' +
                        '<a href="#" class="btn btn_mag-outline popup-close">Pokračovať</a>' +
                    "</div>" +
                "</div>";
            result = 'Pre číslo <b>' + phoneNum + '</b> momentálne nemáme žiadnu ponuku na mieru.Pozrite si našu štandardnú ponuku paušálov.';
            break;

        case 'SHOW_OFFER':
            content = '<div class="p-content">' +
                    '<div class="popup-lightbox">' +
                        '<img src="../assets/img/verify_success.png" />' +
                        '<h4 class="headline">Overenie prebehlo úspešne, pozrite si ponuku šitú na mieru pre Vás.</h4>' +
                        '<a href="#" class="btn btn_mag-outline popup-close">Pokračovať</a>' +
                    "</div>" +
                "</div>";
            result = 'Overenie čísla prebehlo úspešne. Pozrite si ponuku šitú na mieru pre toto číslo. <br / >' +
                '<p class="num">' + phoneNum + '</p>' +
                '<p>' +
                    '<a class="magenta cursor-p show-popup" data-type="otherNumber">' +
                        'Overiť iné číslo alebo zrušiť overenie ' +
                        '<i class="t-icon--magenta" >e</i>' +
                    '</a>' +
                '</p>';
            break;

        case "otherNumber":
            content = '<div class="p-content">' +
                    '<div class="popup-lightbox">' +
                        '<div class="iconDone"><i class="t-icon--magenta--border s3">ä</i></div>' +
                        '<h4 class="headline">Ste si istí, že chcete overiť iné číslo?</h4>' +
                        '<p class="text">Overením iného čísla sa váš nákupný košík vyprázdni a ponuka vynuluje.</p>' +
                        '<a href="#" class="btn cta_mag popup-close" id="resetForm">Overiť iné číslo</a>' +
                        '<a href="#" class="btn btn_mag-outline popup-close" >Zavrieť okno</a>' +
                     "</div>" +
                 "</div>";
            result = null;
             break;

        case 'wrongPin':
            content = '<div class="p-content">' +
                    '<div class="popup-lightbox">' +
                        '<img src="../assets/img/verify_failed.png" />' +
                        '<h4 class="headline">Ľutujeme, Vaš KÓD nie je správny.</h4>' +
                        '<p class="text">Zadajte KÓD z SMS správy znova</p>' +
                        '<a href="#" class="btn btn_mag-outline popup-close-focus">Pokračovať</a>' +
                    "</div>" +
                "</div>";
            result = 'Ľutujeme, Vaš KÓD nie je správny. Zadajte KÓD z SMS správy znova.'
            break;

        case 'EXPIRED_OTP':
            content = '<div class="p-content">' +
                    '<div class="popup-lightbox">' +
                        '<img src="../assets/img/verify_failed.png" />' +
                        '<h4 class="headline">Ľutujeme, Vaš KÓD nie je správny.</h4>' +
                        '<p class="text">Váš KÓD z SMS už nie je aktuálny. Zadajte telefónne číslo znova</p>' +
                        '<a href="#" class="btn btn_mag-outline popup-close-focus">Pokračovať</a>' +
                    "</div>" +
                "</div>";
            result = 'Ľutujeme, Váš KÓD z SMS už nie je aktuálny. Zadajte telefónne číslo znova.'
            break;

        default:
            content = '<div class="p-content">' +
                    '<div class="popup-lightbox">' +
                        '<img src="../assets/img/verify_failed.png" />' +
                        '<h4 class="headline">Ľutujeme, Vaše číslo nebolo možné overiť.</h4>' +
                        '<p class="text"><b>Skúste ho overiť neskôr</b> alebo si pozrite našu štandardnú ponuku.</p>' +
                        '<a href="#" class="btn btn_mag-outline popup-close">Pokračovať</a>' +
                    "</div>" +
                "</div>";
            result = 'Vaše číslo <b>' + phoneNum + '</b> nebolo možné z technických príčin overiť.Skúste to neskôr.'
            break;
    }

    return {
        content,
        result
    };
}

function getPausalInfo(data, isHero, addon, dataOffer) {
  var volania = "<div>Neobmedzené</div>";

  if (data.volania[0] !== "Neobmedzené" || data.sms[0] !== "Neobmedzené") {
    volania =
      "<div>" +
      data.volania[0] +
      '<span class="gray"> ' +
      data.volania[1] +
      "</span>" +
      "</div>" +
      "<div>" +
      data.sms[0] +
      '<span class="gray"> ' +
      data.sms[1] +
      "</span>" +
      "</div>";
  }

  var urlPausal = "/mob/objednavkabd/-/scenario/e-shop/pausaly?";
  if (dataOffer) {
    urlPausal += "uac=" + dataOffer.uacId;
  } else {
    urlPausal += "tariff=" + data.rpCode;
  }

  //tooltips
  var infoM1 = "Tooltip pre M1";

  var cart =
    '<div class="cart' +
    (!isHero && data.hero ? " hero" : "") +
    '" data-id="' +
    data.id +
    '">' +
    '<div class="cart-heading">' +
    '<div class="name">' +
    data.name +
    "</div>" +
    (!isHero && data.hero ? '<div class="hero">HERO</div>' : "") +
    '<div class="sh-d cena">' +
    data.cena +
    " € / mes" +
    "</div>" +
    '<div class="f-i">pri 24 mesačnej viazanosti</div>' +
    '<div class="web-zlava">' +
    "<span>web zľava</span>" +
    '<img class="img-mag" src="../assets/img/lp_percent.png" />' +
    '<img class="img-white" src="../assets/img/lp_percent_selected.png" />' +
    "</div>" +
    "</div>" +
    '<div class="cart-content">' +
    '<div class="cart-content-data w-1">' +
    '<div class="cart-content-data-1">' +
    '<div class="gray f-i">Dáta v SR a EÚ+</div>' +
    "<b>" +
    data.data[0] +
    "</b> " +
    '<span class="gray">' +
    data.data[1] +
    "</span>" +
    "</div>" +
    '<div class="cart-content-data-m1-f-i f-i magenta">' +
    '<div>Dáta s Magentou 1 <i class="t-icon info magenta" alt="' +
    infoM1 +
    '">></i></div>' +
    '<div class="with-m1"><b>' +
    data.dataM1[0] +
    "</b> " +
    data.dataM1[1] +
    "</div>" +
    "</div>" +
    '<div class="cart-content-data-m1 s-i">' +
    "<div>" +
    '<div class="inline"><b>' +
    data.dataM1[0] +
    "</b></div>" +
    ' <div class="inline gray"> ' +
    data.dataM1[1] +
    "</div>" +
    '<div class="with-m1 gray">s M1</div>' +
    "</div>" +
    "<div>" +
    '<i class="t-icon gray info" alt="' +
    infoM1 +
    '">></i>' +
    "</div>" +
    "</div>" +
    "</div>" +
    '<div class="cart-content-volania w-2">' +
    '<div class="s-i">' +
    volania +
    "</div>" +
    '<div class="f-i">' +
    "<div>" +
    "<p>Odchádzajúce volania</p>" +
    "<p>do všetkých sietí v SR, zo&nbsp;SR do EÚ+, z&nbsp;EÚ+ do SR, z&nbsp;EÚ+ do EÚ+</p>" +
    "<div>" +
    data.volania[0] +
    '<span class="gray"> ' +
    data.volania[1] +
    "</span></div>" +
    "</div>" +
    "<div>" +
    "<p>Prichádzajúce volania</p>" +
    "<p>v SR a EÚ+</p>" +
    "<div>" +
    data.volaniaIn[0] +
    '<span class="gray"> ' +
    data.volaniaIn[1] +
    "</span></div>" +
    "</div>" +
    "<div>" +
    "<p>SMS a MMS</p>" +
    "<p>do všetkých sietí v SR, zo&nbsp;SR do EÚ+, z&nbsp;EÚ+ do SR, z&nbsp;EÚ+ do EÚ+</p>" +
    "<div>" +
    data.sms[0] +
    '<span class="gray"> ' +
    data.sms[1] +
    "</span></div>" +
    "</div>" +
    "</div>" +
    "</div>" +
    '<div class="cart-content-darcek w-3">';

  if (data.darcek.length || addon.length) {
    var value = data.darcek;
    if (addon.length) value = addon;

    for (var j = 0; j < value.length; j++) {
      cart +=
        "<div>" +
        '<img src="../assets/img/lp_darcek.png" />' +
        "</div>" +
        '<div class="addon">' +
        value[j].name +
        (value[j].tooltip
          ? '<i class="t-icon info" alt="' + value[j].tooltip + '">></i>'
          : "") +
        "</div>";
    }
  }

  cart +=
    "</div>" +
    '<div class="cart-content-cena w-4 sh-m">' +
    "<b>" +
    data.cena +
    "</b>" +
    '<span class="eur">€</span>' +
    "</div>" +
    "</div>" +
    '<div class="cart-content-full">' +
    '<div class="cart-sec clearfix">' +
    '<div class="row">' +
    '<div class="col-xs-6">' +
    "Dáta v SR a EÚ+" +
    "</div>" +
    '<div class="col-xs-6 text-right">' +
    "<b>" +
    data.data[0] +
    "</b> " +
    '<span class="gray">' +
    data.data[1] +
    "</span>" +
    "</div>" +
    "</div>" +
    '<div class="row">' +
    '<div class="col-xs-6 magenta">' +
    "Dáta s Magentou 1 " +
    '<i class="t-icon info magenta" alt="' +
    infoM1 +
    '">></i>' +
    "</div>" +
    '<div class="col-xs-6 text-right magenta">' +
    "<b>" +
    data.dataM1[0] +
    "</b> " +
    '<span class="">' +
    data.dataM1[1] +
    "</span>" +
    "</div>" +
    "</div>" +
    "</div>" +
    '<div class="cart-sec clearfix">' +
    '<div class="row">' +
    '<div class="col-xs-6">' +
    "Odchádzajúce volania" +
    "<p>do všetkých sietí v SR, zo SR do EÚ+, z EÚ+ do SR, z EÚ+ do EÚ+</p>" +
    "</div>" +
    '<div class="col-xs-6 text-right">' +
    (data.volania === null
      ? "Neobmedzené"
      : "<b>" +
        data.volania[0] +
        "</b> " +
        '<span class="gray">' +
        data.volania[1] +
        "</span>") +
    "</div>" +
    "</div>" +
    "</div>" +
    '<div class="cart-sec clearfix">' +
    '<div class="row">' +
    '<div class="col-xs-6">' +
    "Prichádzajúce volania" +
    "<p>v SR a EÚ+</p>" +
    "</div>" +
    '<div class="col-xs-6 text-right">' +
    "Neobmedzené" +
    "</div>" +
    "</div>" +
    "</div>" +
    '<div class="cart-sec clearfix">' +
    '<div class="row">' +
    '<div class="col-xs-6">' +
    "SMS a MMS" +
    "<p>do všetkých sietí v SR, zo SR do EÚ+, z EÚ+ do SR, z EÚ+ do EÚ+</p>" +
    "</div>" +
    '<div class="col-xs-6 text-right">' +
    (data.sms === null
      ? "Neobmedzené"
      : "<b>" +
        data.sms[0] +
        "</b> " +
        '<span class="gray">' +
        data.sms[1] +
        "</span>") +
    "</div>" +
    "</div>" +
    "</div>";

  if (data.darcek.length) {
    cart += '<div class="cart-sec clearfix">';

    for (var j = 0; j < data.darcek.length; j++) {
      cart +=
        '<div class="row">' +
        '<div class="col-xs-6">' +
        data.darcek[j].text +
        (data.darcek[j].tooltip
          ? ' <i class="t-icon gray info" alt="' +
            data.darcek[j].tooltip +
            '">></i>'
          : "") +
        "</div>" +
        '<div class="col-xs-6 text-right">' +
        '<i class="t-icon">V</i>' +
        "</div>" +
        "</div>";
    }

    cart += "</div>";
  }

  if (addon !== null) {
    cart += '<div class="cart-sec clearfix">';

    for (var j = 0; j < addon.length; j++) {
      cart +=
        '<div class="row">' +
        '<div class="col-xs-6">' +
        addon[j].name +
        (addon[j].tooltip
          ? ' <i class="t-icon gray info" alt="' + addon[j].tooltip + '">></i>'
          : "") +
        "</div>" +
        '<div class="col-xs-6 text-right">' +
        '<i class="t-icon">V</i>' +
        "</div>" +
        "</div>";
    }

    cart += "</div>";
  }

  cart +=
    '<div class="cart-sec clearfix">' +
    '<div class="row">' +
    '<div class="col-xs-6">' +
    "Web zľava na nový mobil" +
    "</div>" +
    '<div class="col-xs-6 text-right">' +
    "- 20 €" +
    "</div>" +
    "</div>" +
    "</div>" +
    '<div class="cart-sec clearfix">' +
    '<div class="row">' +
    '<div class="col-xs-6">' +
    "Mesačne" +
    "<p>pri 24 mesačnej viazanosti</p>" +
    "</div>" +
    '<div class="col-xs-6 text-right">' +
    data.cena +
    " €" +
    "</div>" +
    "</div>" +
    "</div>" +
    "</div>" +
    '<div class="cart-buttons">' +
    "<div>" +
    '<div class="pausal">' +
    '<a href="' +
    urlPausal +
    '&nohw" ' +
    (dataOffer === null || dataOffer.transactionType !== "TARIFFCHANGE"
      ? 'class="magenta">Kúpiť len paušál'
      : 'class="btn cta_mag">Prejsť na tento paušál') +
    "</a>" +
    "</div>" +
    '<div class="mobil">' +
    (dataOffer === null || dataOffer.transactionType !== "TARIFFCHANGE"
      ? '<a href="' + urlPausal + '" class="btn cta_mag">Pridať mobil</a>'
      : "") +
    "</div>" +
    "</div>" +
    "</div>" +
    "</div>";
  /* beautify preserve:end */

  return cart;
}

async function nba(phone, pin) {
  var url = "";
  if (pin === null) {
    url = "https://api.myjson.com/bins/izobq";
  } else if (phone === null) {
    var phoneNum = $("#phoneNumber").val();
    if (phoneNum.startsWith("01")) url = "https://api.myjson.com/bins/zkz90";
    // PROLONGATION
    else if (phoneNum.startsWith("02"))
      url = "https://api.myjson.com/bins/lwfro";
    // PROLONGATION - addon
    else if (phoneNum.startsWith("03"))
      url = "https://api.myjson.com/bins/14yb4k";
    // TARIFFCHANGE
    else if (phoneNum.startsWith("04"))
      url = "https://api.myjson.com/bins/eacna";
    // EXPIRED_OTP
    else if (phoneNum.startsWith("11"))
      url = "https://api.myjson.com/bins/g9hfw";
    // noOffer
    else if (phoneNum.startsWith("22"))
      url = "https://api.myjson.com/bins/kukuruku"; // error
  }
  return new Promise((resolve, reject) => {
    $.ajax({
      url,
      dataType: "json",
      success: function(data) {
        resolve(data);
      },
      error: function() {
        resolve({
          status: "error"
        });
      }
    });
  });
}

// async function nba(msisdn, otp, handleSuccess, handleError) {

//     var data = {
//         'msisdn': msisdn
//     };
//     if (typeof otp != 'undefined' && otp != null) {
//         data = {
//             'otp': otp
//         };
//     }

//     var resourceurl = 'https://prx01.playground.itc.telekom.sk/p_p_auth=NftodmKC&amp;p_p_id=nba_WAR_portlets&amp;p_p_lifecycle=2&amp;p_p_state=normal&amp;p_p_mode=view&amp;p_p_cacheability=cacheLevelPage&amp;p_p_resource_id=getOffer';

//     return new Promise((resolve, reject) => {
//         $.ajax({
//             type: 'POST',
//             url: resourceurl,
//             data,
//             dataType: "json",
//             success: function (data) {
//                 resolve(data);
//             },
//             error: function () {
//                 resolve({
//                     status: 'error'
//                 });
//             }
//         });
//     });
// }
