$(window).load(function () {
    /* CACHING THE DOM */

    var $prodList = $('#prod-list'),
    $prodListPack = $('#product-list-pack'),
    $prodDetail = $('#prod-detail'),
    $orderPage = $('#product-form'),
    $pdTitle = $('.pd__prod-title'),
    $pdDesc = $('.pd__desc'),
    $pdImg = $('.pd__prod-img'),
    $keyFunc = $('.key-func'),
    $mainPrice = $('.main-price'),
    $mainPriceMonth = $('.main-price-monthly'),
    $pricingHolder = $('.pricing-holder'),
    $specs = $('.spec-table'),
    $package = $('.package-items'),
    $dlLink = $('.download-link'),
    $backBtn = $('.back'),
    $backBtn2 = $('.back2'),
    $loader = $("#ajaxLoadingSpinnerOverlay"),
    $loaderBoxes = $(".ajaxLoadingSpinnerOverlay-container > div"),
    $chooseBtn = $('#choose'),
    $productInputs = $('.product-row input'),
    $formProdInputs = $('.input-prod input'),
    $prodTitles = $('.prod-holder .prod-title'),
    products = [],
    $productsForFilter = $('.product-row'),
    $heapOptions = $('.heapOptions li a'),
    warningDisplayed = false,
    hueProdList = [6, 7, 9, 10],
    $centralProdRow = $('.prod-8'),
    prodJSONURL = '/documents/10179/6486285/products.json',
    testJSONURL = '../assets/js/products.json',
    JSONUrl = '',
    fancyObj = {
        padding: 11,
        margin: 0,
        minWidth: 250,
        closeBtn: false,
        helpers : {
            overlay : {
                css : {
                    'background' : 'rgba(0, 0, 0, 0.7)'
                }
            }
        }
    };

	var formId = getParameterByName('formId');
	var formHash = getParameterByName('urlHash');
	var fromLSBParam = getParameterByName('fromLSB');
	var fromLSB=false;

	if (fromLSBParam !== null) {
        if (fromLSBParam=='true') {
	        fromLSB=false;
        }
	}

	var lsbFormSave = false;

	if (formId !== null && formHash!==null) {
		lsbFormSave = true;

		// emptyOrderList();
		// inputClearer();
		// orderListBuilder();

        $('.order-list').hide();
        $('.title-1').hide();
		switchPage($prodList, $orderPage);
	}

    /* CHECK ENV */

    if (window.location.host.contains('localhost')) {
        JSONUrl = testJSONURL;
    } else {
        JSONUrl = prodJSONURL;
    }

    /* SCROLL-TO */

    $(".scroll-to").click(function (e) {
        var targetId = e.target.id;
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top - ($(".product__nav").outerHeight())
                }, 1000, function() {
                    switch (targetId) {
                        case 'cat-1':
                        $($heapOptions[1]).trigger('click');
                        break;
                        case 'cat-2':
                        $($heapOptions[4]).trigger('click');
                        break;
                        case 'cat-3':
                        $($heapOptions[2]).trigger('click');
                        break;
                    }
                });
                return false;
            }
        }
    });

    $('.product-filter select').change(function(){
        var test = $('.product-filter option:selected').index();

        $($productsForFilter).css('cssText', 'display: flex');



        switch (test) {
            case 1:
            $('#product-row-holder .product-row').not('.cat-1').css('cssText', 'display: none !important');
            break;
            case 2:
            $('#product-row-holder .product-row').not('.cat-2').css('cssText', 'display: none !important');
            break;
            case 3:
            $('#product-row-holder .product-row').not('.cat-3').css('cssText', 'display: none !important');
            break;
            case 4:
            $('#product-row-holder .product-row').not('.cat-4').css('cssText', 'display: none !important');
            break;
            case 5:
            $('#product-row-holder .product-row').not('.cat-5').css('cssText', 'display: none !important');
            break;
            default:
            $($productsForFilter).css('cssText', 'display: flex');
        }
    });

    var product = '<div class="col-md-4 item-holder prod-holder">' +
    '<div class="item-inner">' +
    '<img class="prod-img" src="/documents/10179/6486274/prod-list__p11.jpg" alt="">' +
    '<p class="prod-title"><b>HUE FAREBNÁ ŽIAROVKA</b>' +
    '<br/>PHILIPS HUE COLOR BULB E27</p>' +
    '</div>' +
    '</div>' +
    '<div class="col-md-3 item-holder price1-holder">' +
    '<div class="item-inner">' +
    '<p class="price1"><b>1,99 €</b>' +
    '<br/>cena mesačne</p>' +
    '</div>' +
    '</div>' +
    '<div class="clear-row"></div>' +
    '<div class="col-md-3 item-holder price2-holder">' +
    '<div class="item-inner">' +
    '<p class="price2"><b>59,99 €</b>' +
    '<br/>cena jednorázovo</p>' +
    '</div>' +
    '</div>' +
    '<div class="col-md-2 item-holder count-holder">' +
    '<span class="count">' +
    '<p>1ks</p>' +
    '</span>' +
    '</div>';

    function activatePrice(priceClassToActivate, priceClassToDeactivate) {
        $('.' + priceClassToActivate).addClass('active');
        if ($('.' + priceClassToDeactivate).hasClass('active')) {
            $('.' + priceClassToDeactivate).removeClass('active');
        }
    }

    function switchPage($pageToHide, $pageToShow, scrollTo) {
        loaderShow();

        setTimeout(function () {
            $pageToHide.hide();
            $pageToShow.show();

            if (scrollTo != null) {
                document.getElementById(scrollTo).scrollIntoView();
            }

            loaderHide();
        }, 800);

    }

    function loaderShow() {
        $loaderBoxes.addClass('animated');
        $loader.show();
    }

    function loaderHide() {
        $loader.hide();
    }

    $backBtn.on('click', function (e) {
        e.preventDefault();
        switchPage($prodDetail, $prodList, 'sec-4');
    });

    $backBtn2.on('click', function (e) {
        e.preventDefault();
        switchPage($orderPage, $prodList, 'sec-4');
    });

    $('.arrow-holder .arrow').on('click', function(e){
        e.preventDefault();

        if (isHueProdSelected(hueProdList)) {
            $centralProdRow.addClass('hue-selected');

            if (!warningDisplayed) {
                $.fancybox.open('#p-outer', fancyObj);
                warningDisplayed = true;
            }

        } else {
            $centralProdRow.removeClass('hue-selected');
        }
    });

    $.getJSON(JSONUrl, function (data) {
	    // console.log(data);

/*
	    $.each(data, function (key, val) {
            products.push(val);
        });
*/

        var products = data;

        function fillProdData(productNum) {

            if (!$('#choose').is(":visible")) {
                $('#choose').show();
            }

            console.log(productNum);
            console.log(products[productNum]);

            $pdImg.html(products[productNum].img);
            $pdTitle.html(products[productNum].title);
            $pdDesc.html(products[productNum].desc);
            $keyFunc.html(products[productNum].keyFunctions);
            $mainPrice.html(products[productNum].mainPrice);
            $mainPriceMonth.html(products[productNum].mainPriceMonth);
            $specs.html(products[productNum].specs);
            $package.html(products[productNum].package);
            $dlLink.html(products[productNum].downloadLink);
            $dlLink.attr('href', products[productNum].downloadLink);

            if (productNum == 0) {
                $('#choose').hide();
            }

            $pricingHolder.removeClass('hidden');

            if (products[productNum].hidePrice) {
                $pricingHolder.addClass('hidden');
            }
        }


        $('.prod-link').on('click', function (e) {
            e.preventDefault();
            var prodNum = $(this).data('prod');

            switchPage($prodList, $prodDetail);
            fillProdData(prodNum);
            $chooseBtn.data('prod', prodNum);

            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    $('html,body').animate({
                        scrollTop: target.offset().top
                    }, 1000);
                    return false;
                }
            }
        });

        $('#choose').on('click', function (e) {
            e.preventDefault();
            var prodNum = $chooseBtn.data('prod');
            var count = $('.prod-' + prodNum + ' .count-input').val();
            count++;
            $('.prod-' + prodNum + ' .count-input').val(count);

            switchPage($prodDetail, $prodList, 'sec-4');


        });

        function loadInitProdPage(productNum) {
            fillProdData(productNum);
            switchPage($prodList, $prodDetail, 'page-top');
        }

        function loadProductPage(productName) {

            switch (productName) {
                case "homebase":
                loadInitProdPage('0');
                break;
                case "doorsensor":
                loadInitProdPage('1');
                break;
                case "motionsensor":
                loadInitProdPage('2');
                break;
                case "smokedetector":
                loadInitProdPage('3');
                break;
                case "smartplug":
                loadInitProdPage('4');
                break;
                case "camera":
                loadInitProdPage('5');
                break;
                case "switch":
                loadInitProdPage('6');
                break;
                case "colorbulb":
                loadInitProdPage('7');
                break;
                case "bridge":
                loadInitProdPage('8');
                break;
                case "ledstrip":
                loadInitProdPage('9');
                break;
                case "huego":
                loadInitProdPage('10');
                break;
            }
        }

        var productName = getProductNameFromUrl();

        if (productName != null && productName != '') {
            loadProductPage(productName);
        }

    });

    function getProductNameFromUrl() {

        var hash = window.location.href;

        if (hash.indexOf("#") >= 0) {

            hash = window.location.href.slice(window.location.href.indexOf('#') + 1);

            return hash;
        }

    }

    function isHueProdSelected(prodArray) {
        var arrLength = prodArray.length;

        for (var i = 0; i < arrLength; i++) {
            var $prodInput = $('.prod-' + prodArray[i] + ' .count-input');
            var val = Number($prodInput.val());
            if (val > 0) {
                return true;
            }
        }

        return false;
    }


    function inputClearer() {
        $('.input-prod input, .payment-method input').val('');
    }

    function fillInputWithDefaultProdText(text, text2) {
        $('.default-products input').val(text);
        $('.default-products2 input').val(text2);
    }

    function fillProdData2Form(monthlyPrice, price) {
        for (var i = 0; i < $productInputs.length; i++) {
            $($formProdInputs[i]).val($($productInputs[i]).val());
        }

        switch (getSelectedPaymentMethod()) {
            case '1':
            case '2':
            fillInputWithDefaultProdText(
                'Objednávka SmartHome (základný balík obsahuje: SmartHome centrála 1 ks, dverový senzor 1 ks).',
                'Ďalšie objednané produkty:'
            );
            break;
            default:
            fillInputWithDefaultProdText('Objednávka Smart home', 'Produkty:');
        }

        $('.payment-method input').val($('.payment-option .checked').parent().parent().find('label').text());
        $('.price-monthly input').val(monthlyPrice);
        $('.one-time-payment input').val(price);
    }

    function getCentralPart() {
        return '<div class="row product-row">' +
        '<div class="col-md-4 item-holder prod-holder">' +
        '<div class="item-inner">' +
        '<img class="prod-img" src="/documents/10179/6486274/prod-list__p1.jpg" alt="">' +
        '<p class="prod-title"><b>SMARTHOME CENTRÁLA</b>' +
        '<br>QIVICON HOME BASE</p>' +
        '</div>' +
        '</div>' +
        '<div class="col-md-3 item-holder price1-holder">' +
        '<div class="item-inner">' +
        '<p class="price1"><b>0,00 €</b>' +
        '<br>cena mesačne</p>' +
        '</div>' +
        '</div>' +
        '<div class="clear-row"></div>' +
        '<div class="col-md-3 item-holder price2-holder">' +
        '<div class="item-inner">' +
        '<p class="price2"><b>0,00 €</b>' +
        '<br>cena jednorázovo</p>' +
        '</div>' +
        '</div>' +
        '<div class="col-md-2 item-holder count-holder">' +
        '<span class="count">' +
        '<p>1 ks</p>' +
        '</span>' +
        '</div>' +
        '</div>';
    }

    function getProd(src, title, monthlyPrice, price, count) {
        return '<div class="row product-row">' +
        '<div class="col-md-4 item-holder prod-holder">' +
        '<div class="item-inner">' +
        '<img class="prod-img" src="' + src + '" alt="">' +
        '<p class="prod-title">' + title + '</p>' +
        '</div>' +
        '</div>' +
        '<div class="col-md-3 item-holder price1-holder">' +
        '<div class="item-inner">' +
        '<p class="price1">' + monthlyPrice + '</p>' +
        '</div>' +
        '</div>' +
        '<div class="clear-row"></div>' +
        '<div class="col-md-3 item-holder price2-holder">' +
        '<div class="item-inner">' +
        '<p class="price2">' + price + '</p>' +
        '</div>' +
        '</div>' +
        '<div class="col-md-2 item-holder count-holder">' +
        '<span class="count">' +
        '<p>' + count + ' ks</p>' +
        '</span>' +
        '</div>' +
        '</div>';
    }

    function getFinalPrice(paymentMethod, monthlyPrice, oneTimePrice) {
        return '<div class="row">' +
            '<div class="product-row price-final">' +
        '<div class="col-md-4 item-holder pricing-method-final">' +
        '<div class="item-inner">' +
        '<p>Spôsob platby:' +
        '<br/><b>' + paymentMethod + '</b></p>' +
        '</div>' +
        '</div>' +
        '<div class="col-md-3 item-holder price1-holder">' +
        '<div class="item-inner">' +
        '<p class="price1"><b>' + monthlyPrice + ' €</b>' +
        '<br/>cena mesačne celkom</p>' +
        '</div>' +
        '</div>' +
        '<div class="col-md-3 item-holder price1-holder">' +
        '<div class="item-inner">' +
        '<p class="price2"><b>' + oneTimePrice + ' €</b>' +
        '<br/>cena jednorázovo celkom</p>' +
        '</div>' +
        '</div>' +
        '<div class="col-md-2"></div>' +
        '</div>' +
        '</div>';
    }

    function getStarterPack(pricingNum) {
        switch (pricingNum) {
            case '1':
            return getPack1();
            case '2':
            return getPack2();
            break;
        }
    }

    function getPackMonthlyPaymentTypeTextOnLanding() {
        return 'Je súčasťou základného balíka za mesačný poplatok';
    }

    function getPackOneTimePaymentTypeTextOnLanding() {
        return 'Je súčasťou základného balíka za mesačný poplatok';
    }

    function setPricingTextOnLanding(type) {
        if (type === 'monthly') {
            $('#product-list-pack .pricing-text').text(getPackMonthlyPaymentTypeTextOnLanding());
        } else if (type === 'onetime') {
            $('#product-list-pack .pricing-text').text(getPackOneTimePaymentTypeTextOnLanding());
        }
    }

    function getPackForProdLanding() {
        return '<h3 class="sec__sub-title">ZÁKLADNÝ BALÍČEK ZAHŔŇA</h3>' +
        '<div class="row product-row">' +
        '<div class="col-md-4 item-holder prod-holder">' +
        '<div class="item-inner">' +
        '<img class="prod-img" src="/documents/10179/6486274/prod-list__p1.png" alt="">' +
        '<a href="#page-top" class="prod-link scroll-to" data-prod="0">' +
        '<p class="prod-title"><b>SMARTHOME CENTRÁLA</b>' +
        '<br>QIVICON HOME BASE<br />' +
        '<span class="more-info">Detail produktu</span></p>' +
        '</a>' +
        '</div>' +
        '</div>' +
        '<div class="col-md-6 item-holder price1-holder">' +
        '<div class="item-inner">' +
        '<p class="pricing-text">Je súčasťou základného balíka za mesačný poplatok</p>' +
        '</div>' +
        '</div>' +
        '<div class="col-md-2 item-holder count-holder">' +
        '<span class="count">' +
        '<p>1 ks</p>' +
        '</span>' +
        '</div>' +
        '</div>' +
        '<div class="row product-row prod-1" prod="1">' +
        '<div class="col-md-4 item-holder prod-holder">' +
        '<div class="item-inner">' +
        '<img class="prod-img" src="/documents/10179/6486274/prod-list__p4.png" alt="">' +
        '<a href="#page-top" class="prod-link scroll-to" data-prod="1">' +
        '<p class="prod-title"><b>DVEROVÝ SENZOR</b>' +
        '<br/>CENTRALITE DOOR SENSOR<br />' +
        '<span class="more-info">Detail produktu</span></p>' +
        '</a>' +
        '</div>' +
        '</div>' +
        '<div class="col-md-6 item-holder price1-holder">' +
        '<div class="item-inner">' +
        '<p class="pricing-text">Je súčasťou základného balíka za mesačný poplatok</p>' +
        '</div>' +
        '</div>' +
        '<div class="col-md-2 item-holder count-holder">' +
        '<span class="count">' +
        '<p>1 ks</p>' +
        '</span>' +
        '</div>' +
        '</div>' +
        '<div class="row product-row product-row_last billing">' +
        '<div class="col-md-4 item-holder prod-holder">' +
        '<div class="item-inner">' +
        '<p class="prod-title"></p>' +
        '</div>' +
        '</div>' +
        '<div class="col-md-3 item-holder price1-holder">' +
        '<div class="item-inner">' +
        '<p class="price1"><b><span id="price1-main"></span></b>' +
        '<br/>cena mesačne</p>' +
        '</div>' +
        '</div>' +
        '<div class="clear-row"></div>' +
        '<div class="col-md-3 item-holder price2-holder">' +
        '<div class="item-inner">' +
        '<p class="price2"><b><span id="price2-main"></span></b>' +
        '<br/>cena jednorázovo</p>' +
        '</div>' +
        '</div>' +
        '<div class="col-md-2 item-holder count-holder">' +
        '<span class="count">' +
        '<p></p>' +
        '</span>' +
        '</div>' +
        '</div>' +
        '<div class="col-md-2 item-holder count-holder">' +
        '<span class="count">' +
        '<p></p>' +
        '</span>' +
        '</div>' +
        '</div>' +
        '<br/><br/>' +
        '<h3 class="sec__sub-title ssb-custom">VYBRAŤ PRODUKTY K ZÁKLADNÉMU BALÍČKU</h3>';
    }

    function getPack1() {
        return '<h3 class="sec__sub-title">ZÁKLADNÝ BALÍČEK ZAHŔŇA</h3>' +
        '<div class="row product-row">' +
        '<div class="col-md-4 item-holder prod-holder">' +
        '<div class="item-inner">' +
        '<img class="prod-img" src="/documents/10179/6486274/prod-list__p1.jpg" alt="">' +
        '<p class="prod-title"><b>SMARTHOME CENTRÁLA</b>' +
        '<br>QIVICON HOME BASE</p>' +
        '</div>' +
        '</div>' +
        '<div class="col-md-6 item-holder price1-holder">' +
        '<div class="item-inner">' +
        '<p class="pricing-text">Je súčasťou základného balíka za mesačný poplatok</p>' +
        '</div>' +
        '</div>' +
        '<div class="col-md-2 item-holder count-holder">' +
        '<span class="count">' +
        '<p>1 ks</p>' +
        '</span>' +
        '</div>' +
        '</div>' +
        '<div class="row product-row prod-1" prod="1">' +
        '<div class="col-md-4 item-holder prod-holder">' +
        '<div class="item-inner">' +
        '<img class="prod-img" src="/documents/10179/6486274/prod-list__p4.jpg" alt="">' +
        '<p class="prod-title"><b>DVEROVÝ SENZOR</b>' +
        '<br/>CENTRALITE DOOR SENSOR</p>' +
        '</div>' +
        '</div>' +
        '<div class="col-md-6 item-holder price1-holder">' +
        '<div class="item-inner">' +
        '<p class="pricing-text">Je súčasťou základného balíka za mesačný poplatok</p>' +
        '</div>' +
        '</div>' +
        '<div class="col-md-2 item-holder count-holder">' +
        '<span class="count">' +
        '<p>1 ks</p>' +
        '</span>' +
        '</div>' +
        '</div>' +
        '<div class="row product-row billing">' +
        '<div class="col-md-4 item-holder prod-holder">' +
        '<div class="item-inner">' +
        '<p class="prod-title">MESAČNÝ PAUŠÁL</p>' +
        '</div>' +
        '</div>' +
        '<div class="col-md-3 item-holder price1-holder">' +
        '<div class="item-inner">' +
        '<p class="price1 force-active"><b>8,99 €</b>' +
        '<br/>cena mesačne</p>' +
        '</div>' +
        '</div>' +
        '<div class="clear-row"></div>' +
        '<div class="col-md-3 item-holder price2-holder">' +
        '<div class="item-inner">' +
        '<p class="price2 force-disabled"><b>0,00 €</b>' +
        '<br/>cena jednorázovo</p>' +
        '</div>' +
        '</div>' +
        '<div class="col-md-2 item-holder count-holder">' +
        '<span class="count">' +
        '<p></p>' +
        '</span>' +
        '</div>' +
        '</div>' +
        '<div class="row product-row billing">' +
        '<div class="col-md-4 item-holder prod-holder">' +
        '<div class="item-inner">' +
        '<p class="prod-title">AKTIVAČNÝ POPLATOK</p>' +
        '</div>' +
        '</div>' +
        '<div class="col-md-3 item-holder price1-holder">' +
        '<div class="item-inner">' +
        '<p class="price1 force-disabled"><b>0,00 €</b>' +
        '<br/>cena mesačne</p>' +
        '</div>' +
        '</div>' +
        '<div class="clear-row"></div>' +
        '<div class="col-md-3 item-holder price2-holder">' +
        '<div class="item-inner">' +
        '<p class="price2 force-active"><b>0,00 €</b>' +
        '<br/>cena jednorázovo</p>' +
        '</div>' +
        '</div>' +
        '<div class="col-md-2 item-holder count-holder">' +
        '<span class="count">' +
        '<p></p>' +
        '</span>' +
        '</div>' +
        '</div>'
        '<br/><br/>';
    }

    function getPack2() {
        return '<h3 class="sec__sub-title">ZÁKLADNÝ BALÍČEK ZAHŔŇA</h3>' +
        '<div class="row product-row">' +
        '<div class="col-md-4 item-holder prod-holder">' +
        '<div class="item-inner">' +
        '<img class="prod-img" src="/documents/10179/6486274/prod-list__p1.jpg" alt="">' +
        '<p class="prod-title"><b>SMARTHOME CENTRÁLA</b>' +
        '<br>QIVICON HOME BASE</p>' +
        '</div>' +
        '</div>' +
        '<div class="col-md-6 item-holder price1-holder">' +
        '<div class="item-inner">' +
        '<p class="pricing-text">Je súčasťou základného balíka za mesačnú platbu</p>' +
        '</div>' +
        '</div>' +
        '<div class="col-md-2 item-holder count-holder">' +
        '<span class="count">' +
        '<p>1 ks</p>' +
        '</span>' +
        '</div>' +
        '</div>' +
        '<div class="row product-row prod-1" prod="1">' +
        '<div class="col-md-4 item-holder prod-holder">' +
        '<div class="item-inner">' +
        '<img class="prod-img" src="/documents/10179/6486274/prod-list__p4.jpg" alt="">' +
        '<p class="prod-title"><b>DVEROVÝ SENZOR</b>' +
        '<br/>CENTRALITE DOOR SENSOR</p>' +
        '</div>' +
        '</div>' +
        '<div class="col-md-6 item-holder price1-holder">' +
        '<div class="item-inner">' +
        '<p class="pricing-text">Je súčasťou základného balíka za mesačnú platbu</p>' +
        '</div>' +
        '</div>' +
        '<div class="col-md-2 item-holder count-holder">' +
        '<span class="count">' +
        '<p>1 ks</p>' +
        '</span>' +
        '</div>' +
        '</div>' +
        '<div class="row product-row billing">' +
        '<div class="col-md-4 item-holder prod-holder">' +
        '<div class="item-inner">' +
        '<p class="prod-title">MESAČNÝ PAUŠÁL</p>' +
        '</div>' +
        '</div>' +
        '<div class="col-md-3 item-holder price1-holder">' +
        '<div class="item-inner">' +
        '<p class="price1 force-active"><b>10,99 €</b>' +
        '<br/>cena mesačne</p>' +
        '</div>' +
        '</div>' +
        '<div class="clear-row"></div>' +
        '<div class="col-md-3 item-holder price2-holder">' +
        '<div class="item-inner">' +
        '<p class="price2 force-disabled"><b>0,00 €</b>' +
        '<br/>cena jednorázovo</p>' +
        '</div>' +
        '</div>' +
        '<div class="col-md-2 item-holder count-holder">' +
        '<span class="count">' +
        '<p></p>' +
        '</span>' +
        '</div>' +
        '</div>' +
        '<div class="row product-row billing">' +
        '<div class="col-md-4 item-holder prod-holder">' +
        '<div class="item-inner">' +
        '<p class="prod-title">AKTIVAČNÝ POPLATOK</p>' +
        '</div>' +
        '</div>' +
        '<div class="col-md-3 item-holder price1-holder">' +
        '<div class="item-inner">' +
        '<p class="price1 force-disabled"><b>0,00 €</b>' +
        '<br/>cena mesačne</p>' +
        '</div>' +
        '</div>' +
        '<div class="clear-row"></div>' +
        '<div class="col-md-3 item-holder price2-holder">' +
        '<div class="item-inner">' +
        '<p class="price2 force-active"><b>179,98 €</b>' +
        '<br/>cena jednorázovo</p>' +
        '</div>' +
        '</div>' +
        '<div class="col-md-2 item-holder count-holder">' +
        '<span class="count">' +
        '<p></p>' +
        '</span>' +
        '</div>' +
        '</div>' +
        '<br/><br/>';
    }

    function emptyOrderList() {
        $('.order-list').html('');
    }

    function getSelectedPaymentMethod() {
        return $('.payment-option .checked').find('input').attr('value');
    }

    function orderListBuilder() {

        var monthlyPrice2Calc = 0;
        var oneTimePrice2Calc = 0;

        var tempCentralPart = '';

        switch (getSelectedPaymentMethod()) {
            case '1':
            tempCentralPart = getStarterPack('1');
            break;
            case '2':
            tempCentralPart = getStarterPack('2');
            break;
        }

        $(tempCentralPart).appendTo('.order-list');

        var canAssignTitle = true;

        for (var i = 0; i < $productInputs.length; i++) {


            if ($($productInputs[i]).val() > 0) {
                var tempImg = $($productInputs[i]).parent().parent().parent().find('.prod-holder').find('img').attr('src');
                var tempTitle = $($productInputs[i]).parent().parent().parent().find('.prod-holder').find('.prod-title').html();
                var tempMonthlyPrice = $($productInputs[i]).parent().parent().parent().find('.price1').html();
                var tempMonthlyPrice2Calc = tempMonthlyPrice.slice((tempMonthlyPrice.indexOf('>') + 1), tempMonthlyPrice.indexOf(' '));
                tempMonthlyPrice2Calc = parseFloat(tempMonthlyPrice2Calc.replace(",", "."));

                var tempPrice = $($productInputs[i]).parent().parent().parent().find('.price2').html();

                var tempOneTimePrice2Calc = tempPrice.slice((tempPrice.indexOf('>') + 1), tempPrice.indexOf(' '));
                tempOneTimePrice2Calc = parseFloat(tempOneTimePrice2Calc.replace(",", "."));

                var tempCount = $($productInputs[i]).val();

                switch ($('.payment-option .checked').find('input').attr('value')) {
                    case '1':
	                case '2':
		                tempPrice = '<b>0,00 €</b><br>cena jednorázovo';
                    break;
                    case '3':
                        tempMonthlyPrice = '<b>0,00 €</b><br>cena mesačne';
                    break;
                }

                var tempProd = '';

                if (canAssignTitle) {
                    if ($('.payment-option .checked').find('input').attr('value') != 3) {
                        tempProd = '<h3 class="sec__sub-title sec-ma">NAVYŠE STE SI ZVOLILI</h3>';
                        canAssignTitle = false;
                    }
                }

                tempProd += getProd(tempImg, tempTitle, tempMonthlyPrice, tempPrice, tempCount);

                $(tempProd).appendTo('.order-list');


                monthlyPrice2Calc += tempMonthlyPrice2Calc * parseInt(tempCount);
                oneTimePrice2Calc += tempOneTimePrice2Calc * parseInt(tempCount);

            }



        }

        switch ($('.payment-option .checked').find('input').attr('value')) {
            case '1':
                monthlyPrice2Calc = monthlyPrice2Calc + 8.99;
                oneTimePrice2Calc = 0.00;
                break;
            case '2':
	            monthlyPrice2Calc = monthlyPrice2Calc + 10.99;
	            oneTimePrice2Calc = 179.98;
	            break;
            case '3':
                monthlyPrice2Calc = 0;
            break;
        }

        monthlyPrice2Calc = monthlyPrice2Calc.toFixed(2).replace(".", ",");
        oneTimePrice2Calc = oneTimePrice2Calc.toFixed(2).replace(".", ",");

        var tempPaymentMethod = $('.payment-option .checked').parent().parent().find('label').text();

        var tempFinalPrice = getFinalPrice(tempPaymentMethod, monthlyPrice2Calc, oneTimePrice2Calc);

        $(tempFinalPrice).appendTo('.order-list');

        switch ($('.payment-option .checked').find('input').attr('value')) {
            case '1':
                activatePrice('price1', 'price2');
                $('.price-final .price2').addClass('active');
	            break;
            case '2':
	            activatePrice('price1', 'price2');
	            $('.price-final .price2').addClass('active');
	            break;
            case '3':
            activatePrice('price2', 'price1');
            break;
        }

        fillProdData2Form(monthlyPrice2Calc, oneTimePrice2Calc);

    }

    function setMainprice() {
        var checked = $('.payment-option .checked').find('input').attr('value');

	    switch (checked) {
		    case '1':

			    $('#price1-main').text('8,99');
			    $('#price2-main').text('0,00');

			    break;
		    case '2':

			    $('#price1-main').text('10,99');
			    $('#price2-main').text('179,98');

			    break;
	    }
    }

    function isBasketEmpty() {
        for (var i = 0; i < $productInputs.length; i++) {
            if ($($productInputs[i]).val() > 0) {
                return false;
            }
        }
        return true;
    }


    $('#order').on('click', function (e) {
        e.preventDefault();
        if (!$(this).hasClass('btn_disabled')) {
            if ($('.payment-option .checked').find('input').attr('value') == 3 && isBasketEmpty()) {
                $('.error-msg').show().delay(1400).fadeOut();
            } else {
                emptyOrderList();
                inputClearer();
                orderListBuilder();
                switchPage($prodList, $orderPage);

                if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                    var target = $(this.hash);
                    target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                    if (target.length) {
                        $('html,body').animate({
                            scrollTop: target.offset().top
                        }, 1000);
                        return false;
                    }
                }
            }
        }
    });

    $('#check1').on('click', function () {
        activatePrice('price1', 'price2');
        setPricingTextOnLanding('monthly');

        if ($('#prod-list .prod-0').hasClass('disabled-prod')) {
            $('#prod-list .prod-0').removeClass('disabled-prod');
        }

        if ($prodListPack.hasClass('hidden')) {
            $prodListPack.removeClass('hidden');
        }

	    setMainprice();
    });

    $('#check2').on('click', function () {
        activatePrice('price1', 'price2');
        setPricingTextOnLanding('monthly');

        if ($('#prod-list .prod-0').hasClass('disabled-prod')) {
            $('#prod-list .prod-0').removeClass('disabled-prod');
        }

        if ($prodListPack.hasClass('hidden')) {
            $prodListPack.removeClass('hidden');
        }

	    setMainprice();
    });

    $('#check3').on('click', function () {
        activatePrice('price2', 'price1');
        $prodListPack.addClass('hidden');
    });



    /* INIT */

    $prodListPack.html(getPackForProdLanding());
	setMainprice();

    $('.input-prod, .payment-method input, .default-products, .default-products2').closest('.fieldTypeINPUT').addClass('outside');
    $('input.submitField').addClass('btn cta_mag');
    $('#prod-list .prod-0').addClass('hidden');

    activatePrice('price1', 'price2');

});

function getParameterByName(name, url) {
	if (!url) url = window.location.href;
	name = name.replace(/[\[\]]/g, "\\$&");
	var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
		results = regex.exec(url);
	if (!results) return null;
	if (!results[2]) return '';
	return decodeURIComponent(results[2].replace(/\+/g, " "));
}
