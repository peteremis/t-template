$(function () {
    $('#cmb__block').appendTo('#cmb__holder');

/*
    var controller = new ScrollMagic.Controller();

    var $hand = $('.hand');
    var handIntro = TweenMax.from($hand, 2.5, {
        yPercent: 0,
        xPercent: 40,
    });
    var screenIntro = TweenMax.to('.screen', 1, {
        alpha: 1,
        ease: Power1.easeIn,
    });

    var introTl = new TimelineMax()
    .add(handIntro)
    .add(screenIntro);

    var containerScene = new ScrollMagic.Scene({
        triggerElement: '.sec-5',
        reverse: true
    })
    .setTween(introTl)
    //.addIndicators()
    .addTo(controller);
*/

    /* SIDEBAR SLIDER */
/*
    var $ss__links = $('.ss__link'),
    $ss__slides = $('.ss__slide');

    $ss__links.on('click', function (e) {
        e.preventDefault();

        if (!$(e.target).hasClass('active')) {
            $ss__links.removeClass('active');
            $(e.target).addClass('active');
        }

        $ss__slides.removeClass('active');
        $('.ss__slide-' + e.target.getAttribute('data')).addClass('active');
    });
*/
    /* CAROUSEL */
/*
    $("#big-slider").owlCarousel({
        singleItem: true,
        navigation: true,
        navigationText: ["<img src='/documents/10179/6486274/ico-prev.png'>", "<img src='/documents/10179/6486274/ico-next.png'>"]
    });
*/
    /* COUNTER */

    $(".count .arrow").on("click", function () {

        var $button = $(this);
        var oldValue = $button.parent().parent().children().val();

        if ($button.hasClass("inc")) {
            var newVal = parseFloat(oldValue) + 1;
        } else {
            if (oldValue > 0) {
                var newVal = parseFloat(oldValue) - 1;
            } else {
                newVal = 0;
            }
        }

        $button.parent().parent().children().val(newVal);

    });

    /* GALLERY */

    $(".fancy-gal").fancybox();

    /* FORM */

    $('.dynamicFormMain').appendTo('.form-holder');

    $(".scroll-to2").click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });

    /* POPUP FORM */

    function showPopup() {
        $('.form-popup').show();
        $('.fp__overlay').show();
    }

    function hidePopup() {
        $('.form-popup').hide();
        $('.fp__overlay').hide();
    }

    $('.fp__close').on('click', function () {
        $('.form-popup').hide();
        $('.fp__overlay').hide();
    });

    if ($('.portlet-msg-success').length > 0) {

        showPopup();
        setTimeout(hidePopup, 10000);

    }

    /* TABS */

    $(".tabs-menu a").click(function (event) {
        event.preventDefault();
        if (!$(this).parent().hasClass("current") && !$(this).parent().hasClass("isVariant")) {
            toggleTabs();
        }

        $(this).parent().addClass("current");
        $(this).parent().siblings().removeClass("current");
        var tab = $(this).attr("href"),
        parent = $(this).closest('ul').parent().parent();
        parent.find('.tab-content').not(tab).css("display", "none");
        $(tab).fadeIn();

    });

    var toggleTabs = function () {
        $('.tab__variant').toggleClass('selected');
    };

    $('.app-slider').slick({
      infinite: true,
      slidesToShow: 3,
      slidesToScroll: 3,
      responsive: [{
          breakpoint: 620,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: true
          }
        }]
    });

    $('.p-close, .c-close').on('click', function(e) {
        e.preventDefault();
        $.fancybox.close();
    });

});
