$(function() {
  //load YTplayer
  jQuery(function() {
    jQuery("#bgndVideo2").YTPlayer();
  });

  //initial overlay hide
  $(".videoOverlay").hide();

  jQuery("#bgndVideo2").on("YTPTime", function(e) {
    var currentTime = e.time;
    console.log(currentTime);
    if (currentTime == 32) {
      //if the player is at the 50 second
      $(".videoOverlay").show();
      $("#bgndVideo2").css("background-color", "black");
    } else if (currentTime <= 0) {
      $(".videoOverlay").hide();
      $("#bgndVideo2").css("background-color", "transparent");
    }
  });

  $(".scroll-to").click(function() {
    if (
      location.pathname.replace(/^\//, "") ==
        this.pathname.replace(/^\//, "") &&
      location.hostname == this.hostname
    ) {
      var target = $(this.hash);
      target = target.length ? target : $("[name=" + this.hash.slice(1) + "]");
      if (target.length) {
        if ($("#nav-sticky").is(":visible")) {
          $("html,body").animate(
            {
              scrollTop: target.offset().top - $("#nav-sticky").outerHeight()
            },
            1000
          );
        } else {
          $("html,body").animate(
            {
              scrollTop: target.offset().top - 0
            },
            1000
          );
        }
        return false;
      }
    }
  });

  // $(".video-link").jqueryVideoLightning({
  //     autoplay: 1,
  //     backdrop_color: "#000",
  //     backdrop_opacity: 0.6,
  //     glow: 0,
  //     glow_color: "#000"
  // });
});
