$(function () {

    $(".product__nav").sticky({
        topSpacing: 0,
        widthFromWrapper: true
    });


    $(window).resize(function () {
        if (window.innerWidth <= 500) {
            $(".hero .img__holder").append($(".hero .img__swap"));
        }
        if (window.innerWidth <= 870) {
            $(".product__nav").addClass("product__nav_mobile");
        }
        if (window.innerWidth > 500) {
            $(".hero .img__holder__origin").append($(".hero .img__swap"));
        }
        if (window.innerWidth > 870) {
            $(".product__nav").removeClass("product__nav_mobile");
        }
    });
    $('.prod__carousel_2').bxSlider({
        useCSS: false,
        slideMargin: 5,
        autoReload: true,
        breaks: [{
            screen: 0,
            slides: 1,
            pager: true
        }, {
            screen: 470,
            slides: 2,
            pager: false,
            controls: true
        }, {
            screen: 768,
            slides: 2,
            pager: false,
            controls: false
        }]
    });
    $('.prod__carousel_3').bxSlider({
        useCSS: false,
        slideMargin: 5,
        autoReload: true,
        breaks: [{
            screen: 0,
            slides: 1,
            pager: true
        }, {
            screen: 500,
            slides: 2,
            pager: true,
            controls: true
        }, {
            screen: 768,
            slides: 3,
            pager: false,
            controls: false
        }]
    });
    $('.prod__carousel_4').bxSlider({
        useCSS: false,
        slideMargin: 5,
        autoReload: true,
        infiniteLoop: false,
        breaks: [{
            screen: 0,
            slides: 1,
            pager: true
        }, {
            screen: 500,
            slides: 2,
            pager: true,
            controls: true
        }, {
            screen: 768,
            slides: 3,
            pager: true,
            controls: true
        }, {
            screen: 900,
            slides: 4,
            pager: false,
            controls: false
        }]
    });
    var contentSections = $("[id*='sec-']"),
        secondaryNav = $(".product__nav"),
        menuInnerHTML = 'MENU',
        currentID = null;

    function updateSecondaryNavigation() {
        contentSections.each(function () {
            var actual = $(this),
                actualHeight = actual.height() + parseInt(actual.css('paddingTop').replace('px', '')) + parseInt(actual.css('paddingBottom').replace('px', '')),
                actualAnchor = secondaryNav.find('a[href="#' + actual.attr('id') + '"]');

            if ((actual.offset().top - secondaryNav.outerHeight() <= $(window).scrollTop()) && (actual.offset().top + actualHeight - secondaryNav.outerHeight() > $(window).scrollTop())) {
                actualAnchor.addClass('active');

                if (actual.attr('id') == currentID) {}
                else {
                    $('.product__nav_link').html('');
                    $('.content__' + actual.attr('id')).clone().appendTo($('.product__nav_link'));
                    currentID = actual.attr('id');
                }

            } else {
                actualAnchor.removeClass('active');
            }
        });
    }

    $(".menuIcon").click(function (e) {
        e.preventDefault();
        $(".menu").toggleClass("menuOpen");
        $("#nav-icon").toggleClass('open');
        if (!($(".menu").hasClass("menuOpen")) && ($('.scroll-to.active').children().length > 0)) {
            $('.product__nav_link').html('');
            $('.scroll-to.active').children().clone().appendTo($('.product__nav_link'));
        } else {
            $('.product__nav_link').html('');
            $('.product__nav_link').html(menuInnerHTML);
        }
    });
    $(".scroll-to").click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            console.log(target)
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top - 83 + 'px'
                }, 1000);
                return false;
            }
        }
    });

    $(window).scroll(function () {
        updateSecondaryNavigation();
    });

    function toggleNavClass() {
        if (window.innerWidth <= 870) {
            $(".product__nav").addClass("product__nav_mobile");
        }
        if (window.innerWidth > 870) {
            $(".product__nav").removeClass("product__nav_mobile");
        }
    }
    updateSecondaryNavigation();
    toggleNavClass();

    /* YOUTUBE */

    var $allVideos = $("iframe[src^='//www.youtube.com']"),
        $fluidEl = $("body");
    $allVideos.each(function () {
        $(this)
            .data('aspectRatio', this.height / this.width)
        .removeAttr('height')
            .removeAttr('width');
    });
    $(window).resize(function () {
        var newWidth = $fluidEl.width();
        $allVideos.each(function () {
            var $el = $(this);
            $el
                .width(newWidth)
                .height(newWidth * $el.data('aspectRatio'));
        });
    }).resize();

});


	//SCROLL TOP BTN
    // When scroll down 320px from the top of the document, show the button
    window.onscroll = function() {
        scrollFunction()
    };

    function scrollFunction() {
        if (document.body.scrollTop > 350 || document.documentElement.scrollTop > 350) {
            document.getElementById("scrollTopBtn").style.display = "block";
        } else {
            document.getElementById("scrollTopBtn").style.display = "none";
        }
    }

    // When click, scroll to the top of the document
    function scrollTopFunction() {
        $('html,body').animate({
            scrollTop: 0 + 'px'
        }, 1000);
    }
