$(function () {
    var $control = $('.device-controls > a');

    $($control).on('click', function (e) {
        e.preventDefault();
        $(this).parent().prev('.device_img').children('img').attr('src', $(this).children('img').attr('src'));
        $(this).children('img');
    });

    $(".scroll-to").click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });

    $('.info-10').qtip({
        content: 'Web zľava 10 € je jednorazová a získate ju, ak si službu aktivujete cez web.',
        position: {
            my: 'left center',
            at: 'right center'
        }
    });
    $('.info-20').qtip({
        content: 'Web zľava 20 € je jednorazová a získate ju, ak si službu aktivujete cez web.',
        position: {
            my: 'left center',
            at: 'right center'
        }
    });

});