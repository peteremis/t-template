angular.module("nehnutelnostiApp").factory("nehnutelnostiService", http);

http.$inject = ["$http"];
function http($http) {
  return $http.get("assets/js/data.json?v=2").then(function(data) {
    return data;
  });
}
