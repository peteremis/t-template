angular
  .module('nehnutelnostiApp')
  .component('nehnutelnostiIndex', {
    templateUrl: '../views/home.html',
    controller: MainController,
    controllerAs: '$ctrl'
  });

MainController.$inject = ['$scope', '$routeParams', 'nehnutelnostiService', '$timeout'];

function MainController($scope, $routeParams, nehnutelnostiService, $timeout) {

  /* GET DATA FOR INDEX PAGE - START */
  nehnutelnostiService.then(function(homeData) {
    $scope.indexData = homeData.data;
    //console.log($scope.nehnutelnostiIndex.data);


    /* MAGNIFIC POPUP - START */
    //watch timeout for jQuery plugin to be loaded properly
    $scope.$watch("homeData", function(newValue, oldValue) {
      $timeout(function() {
        $('.zoom-gallery').each(function() {
          $(this).magnificPopup({
            delegate: 'a',
            type: 'image',
            closeOnContentClick: false,
            closeBtnInside: false,
            mainClass: 'mfp-with-zoom mfp-img-mobile',
            image: {
              verticalFit: true,
              // titleSrc: function(item) {
              // 	return item.el.attr('title') + ' &middot; <a class="image-source-link" href="'+item.el.attr('data-source')+'" target="_blank">image source</a>';
              // }
            },
            gallery: {
              enabled: true,
              arrowMarkup: '<button title="%title%" type="button" class="mfp-arrow mfp-arrow-%dir%"></button>', // markup of an arrow button
              tPrev: 'Previous (Left arrow key)',
              tNext: 'Next (Right arrow key)',
            },
            zoom: {
              enabled: true,
              duration: 300, // don't foget to change the duration also in CSS
              opener: function(element) {
                return element.find('img');
              }
            }
          });
        });
      });
    });
    /* MAGNIFIC POPUP - END */
  });
  /* GET DATA FOR INDEX PAGE - END */

};
