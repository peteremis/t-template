angular
    .module('nehnutelnostiApp')
    .component('mapIndex', {
        templateUrl: '../views/home.html',
        controller: MapCtrl,
        controllerAs: '$ctrl'
    });

    MapCtrl.$inject = ['$scope', '$routeParams', 'nehnutelnostiService'];
    function MapCtrl($scope, $routeParams, nehnutelnostiService) {


//app.controller("MapCtrl", ['$scope', '$routeParams', 'nehnutelnostiService', function($scope, $routeParams, nehnutelnostiService){

  nehnutelnostiService.then(function(mapData) {
    $scope.mapData = mapData.data; //get data from data.json


    //Map Styling
    var mapOptions = {
      zoom: 7,
      center: new google.maps.LatLng(48.7556882, 19.8113612),
      mapTypeId: google.maps.MapTypeId.ROADMAP,
    };
    //var geocoder = new google.maps.Geocoder();

    //create new map
    var map = new google.maps.Map(document.getElementById("nehnutelnosti-mapa"), mapOptions);

    var marker, i;
    var mapMarkerImage = '../assets/img/t-marker.gif';

    for (i = 0; i < $scope.mapData.length; i++) {
      //get tooltip text
      var MapTooltip = $scope.mapData[i].info.nazov;

      //create new marker
      var marker = new google.maps.Marker({
                  name: $scope.mapData[i].info.nazov,
                  position: new google.maps.LatLng($scope.mapData[i].info.lat, $scope.mapData[i].info.long),
                  map: map,
                  icon: mapMarkerImage,
                  title: MapTooltip,
                });

      //create infowindow without content
      var infowindow = new google.maps.InfoWindow({
        content: " "
      });

      //redirect z mapy na URL adresu danej nehnutelnosti
      //set map infowindow content
      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {

        infowindow.setContent(
          '<div id="mapCont">' +
          '<div class="mapTitle">Nehnutelnost v lokalite:</div>' +
          '<p>' + this.title + '</p>' +
          '<a onclick="zobrazNehnutelnost(800);" class="btn cta_mag">PREJSŤ NA DETAIL</a>' +
          '</div>'
        );

        infowindow.open(map, this);

        //redirect na danu nehnutelnost
        window.location.href = $scope.mapData[i].info.MapUrl;
      }
    })(marker, i));

    }

  });

// //MAP DATA
// var locations = [
//       ["Horná 77, Banská Bystrica", 48.739263, 19.157646, 0, "#/nehnutelnost/0"],
//       ["Bojnická 16, Bratislava", 48.196078, 17.157683, 1, "#/nehnutelnost/1"],
//       ["Kukučínova 52, Bratislava", 48.17617, 17.131588, 2, "#/nehnutelnost/2"],
//       ["Švermova 5, Brezno", 48.802588, 19.639998, 3, "#/nehnutelnost/3"],
//       ["Ružová 135, Dunajská Streda", 47.995709, 17.619045, 4, "#/nehnutelnost/4"],
//       ["Pekárenská 217, Jesenské", 48.302245, 20.073781, 5, "#/nehnutelnost/5"],
//       ["Poštová 18, Košice", 48.721851, 21.251094, 6, "#/nehnutelnost/6"],
//       ["A. Sládkoviča 24, Levice", 48.213292, 18.605733, 7, "#/nehnutelnost/7"],
//       ["Nám. republiky 4, Lučenec", 48.331037, 19.662657, 8, "#/nehnutelnost/8"],
//       ["Erbová 2, Mýtne Ludany", 48.164219, 18.663897, 9, "#/nehnutelnost/9"],
//       ["Sládkovičova 2, Nitra", 48.313288, 18.090169, 10, "#/nehnutelnost/10"],
//       ["Andovská 114, Nové Zámky", 47.983193, 18.142768, 11, "#/nehnutelnost/11"],
//       ["Jesenského 135, Pohorelá", 48.858623, 20.012734, 12, "#/nehnutelnost/12"],
//       ["Železničná 2, Poltár", 48.432212, 19.794059, 13, "#/nehnutelnost/13"],
//       ["Obchodná 4, Pribeta", 47.901238, 18.312297, 14, "#/nehnutelnost/14"],
//       ["L. Svobodu 2674/1, Poprad", 49.046321, 20.293593, 15, "#/nehnutelnost/15"],
//       ["Prístupová cesta 2087/190, Považská Bystrica", 49.111485, 18.448159, 16, "#/nehnutelnost/16"],
//       ["Marka Čulena 55, Prešov", 49.000749, 21.219261, 17, "#/nehnutelnost/17"],
//       ["Potočná 334, Skalica", 48.84629, 17.225897, 18, "#/nehnutelnost/18"],
//       ["Obchodná 7/39, Somotor", 48.404205, 21.808958, 19, "#/nehnutelnost/19"],
//       ["Ing.Straku 4, Spišská Nová Ves", 48.94692, 20.56267, 20, "#/nehnutelnost/20"],
//       ["Štúrova 559, Šaštín-Stráže", 48.638368, 17.145152, 21, "#/nehnutelnost/21"],
//       ["Telgárt 501, Telgárt", 48.850601, 20.186359, 22, "#/nehnutelnost/22"],
//       ["Partizánska 439/22, Turčianske Teplice", 48.8591, 18.862715, 23, "#/nehnutelnost/23"],
//       ["Komárňanská 209/5, Veľký Meder", 47.856345, 17.771539, 24, "#/nehnutelnost/24"],
//       ["Námestie slobody 284/11, Vrbové", 48.621337, 17.72404, 25, "#/nehnutelnost/25"],
//       ["Sládkovičova 4, Zvolen", 48.574896, 19.127444, 26, "#/nehnutelnost/26"]
//   ];
//
//   //Map Styling
//   var mapOptions = {
//     zoom: 7,
//     center: new google.maps.LatLng(48.7556882, 19.8113612),
//     mapTypeId: google.maps.MapTypeId.ROADMAP,
//
//       // styles: [
//       // {
//       //   stylers: [{hue: "#fff"},{saturation: -100}]
//       // },
//       // {
//       //   featureType: "road",
//       //   elementType: "geometry",
//       //   stylers: [{lightness: 100},{visibility: "simplified"}]
//       // },
//       // {
//       //   featureType: "road",
//       //   elementType: "labels",
//       //   stylers: [{visibility: "off"}]
//       // },
//       // {
//       //   featureType: "transit.line",
//       //   stylers: [{visibility: "off"}]
//       // },
//       // {
//       //   featureType: "poi",
//       //   stylers: [{ visibility: "off"}]
//       // },
//       // {
//       //   featureType: "water",
//       //   elementType: "labels",
//       //   stylers: [{ visibility: "off"}]
//       // },
//       // {
//       //   featureType: "road",
//       //   stylers: [{ visibility: "on"}]
//       // }
//       // ]
//   };
//   //var geocoder = new google.maps.Geocoder();
//
//   //create new map
//   var map = new google.maps.Map(document.getElementById("nehnutelnosti-mapa"), mapOptions);
//
//   var marker, i;
//   var mapMarkerImage = '../assets/img/t-marker.gif';
//
//   for (i = 0; i < locations.length; i++) {
//
//     //get tooltip text from var = locations
//     var MapTooltip = locations[i][0];
//
//     //create new marker
//     var marker = new google.maps.Marker({
//                 name: locations[i][0],
//                 position: new google.maps.LatLng(locations[i][1], locations[i][2]),
//                 map: map,
//                 icon: mapMarkerImage,
//                 title: MapTooltip,
//               });
//
//     //create infowindow without content
//     var infowindow = new google.maps.InfoWindow({
//       content: " "
//     });
//
//     //redirect z mapy na URL adresu danej nehnutelnosti
//     //tooltip on hover
//     //set infowindow content
//     google.maps.event.addListener(marker, 'click', (function(marker, i) {
//       return function() {
//
//       infowindow.setContent(
//         '<div id="mapCont">' +
//         '<div class="mapTitle">Nehnutelnost v lokalite:</div>' +
//         '<p>' + this.title + '</p>' +
//         '<a onclick="zobrazNehnutelnost(800);" class="btn cta_mag">PREJSŤ NA DETAIL</a>' +
//         '</div>'
//       );
//
//       infowindow.open(map, this);
//
//       window.location.href = locations[i][4];
//     }
//   })(marker, i));
//
//   }

}; //end of MapCtrl
