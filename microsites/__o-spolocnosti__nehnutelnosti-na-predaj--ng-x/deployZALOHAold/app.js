var app = angular.module("nehnutelnostiApp", ['ngRoute']);

function zobrazNehnutelnost(time){
 $('html, body').animate({
    scrollTop: $("#sec-1, #sec-1a").offset().top + 'px'
  }, time);
  return;
};





//APP CONFIG - ROUTING
app.config(function ($routeProvider, $locationProvider) {



  $routeProvider
    .when('/', { // na prode zmenit na /nehnutelnosti-na-predaj
      controller: 'MainController',
      templateUrl: '/documents/10179/16464398/home.html'
    })
    .when('/nehnutelnost/:id', {
        controller: 'NehnutelnostController',
        templateUrl: '/documents/10179/16464398/nehnutelnost.html'
      })
    .otherwise({
      redirectTo: '/'
    });

    $locationProvider
    .hashPrefix('');

    // //check browser support
    // if(window.history && window.history.pushState){
    //  // don't want to set base URL in <head> tag
    //  $locationProvider.html5Mode({
    //          enabled: true,
    //          requireBase: false
    //   });
    // }
});
