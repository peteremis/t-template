$(document).ready(function () {
	var id = getParameterByName('id')
	load(id);

	$("#show-pecka-info, #show-xsmini-info").fancybox({
		hideOnContentClick: true,
		maxWidth: 580,
		minWidth: 200
	});

	//$.fancybox("#xsmini-info");

});

$(window).load(function () {
	$(".detail").fadeIn("slow");
});

function getParameterByName(name) {
	var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
	return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
}

function load(id) {

	$.ajax({
		type: "get",
		async: false,
		crossDomain: true,
		dataType: "json",
		data: "",
		//url: 'https://www.telekom.sk/documents/10179/453831/detail.json',
		url: 'https://m.telekom.sk/wb/vypredaj3/detail.php',
		success: function (phone) {
			showDetail(phone, id);
		},
		error: function () {
			//alert("Chyba pri posielani ... skus este raz!");
		}
	});
}

function showDetail(phones, id) {
	var html = "";

	for (var a = 0; a < phones.length; a++) {
		if (phones[a].id == id) {
			var show = 1;
			var phone = phones[a];

			var img = '<img src="' + phone.image + '">';
			$(img).appendTo(".detail-img");

			var detail = phone.description;

			var details = '';
			for (var b = 0; b < detail.length; b++) {
				details +=
					'<tr>' +
					'<td><div class="odrazka"></div></td>' +
					'<td>' + detail[b].desc[0] + '</td>' +
					'<td class="row-2">' + detail[b].desc[1] + '</td>' +
					'</tr>';
			}

			$(details).appendTo(".detail-table tbody");

			if (typeof phone.tablet !== 'undefined') {
				$("#tel-desc").html("Tablet");
			}

			var phoneName = phone.name;
			$("#phoneName").text(phoneName);

			$("#peckaPrice").text(phone.pecka + ',00 €');
			$("#xsminiOne").text(phone.xsminiOne + ',00 €');
			$("#xsminiMonth").text(phone.xsminiMonth + ',00 €');

			$("#xsminiUrl").attr("href", phone.xsminiOrder);
			$("#peckaUrl").attr("href", phone.peckaOrder);

		}
	}

	if (show != 1) {
		window.location.replace("/vypredaj-telefonov");
	}
}