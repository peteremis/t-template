$(document).ready(function() {
    //$("#activate-vp1, #activate-vp2, #activate-vp3, #activate-vp4, #activate-vp5, #activate-vp6, #activate-vp7, #activate-vp8, #activate-vp9, #activate-vp10, #activate-vp11, #activate-vp12, #activate-vp13, #activate-vp14, #activate-vp15, #activate-vp16, #activate-vp17, #activate-vp18, #activate-vp19, #activate-vp20, #activate-vp21, #activate-vp22, #activate-vp22, #activate-vp23, #activate-vp24, #activate-vp25, #activate-vp26, #activate-vp27, #activate-vp28, #activate-vp29, #activate-vp30, #activate-vp31, #activate-vp32, #activate-vp33, #activate-vp34, #activate-vp35, #activate-vp36, #activate-vp37, #activate-vp38, #activate-vp39, #activate-vp40").fancybox({
    $("#activate-vp1, #activate-vp2, #activate-vp3, #activate-vp4, #activate-vp5, #activate-vp6, #activate-vp7, #activate-vp8, " +
        "#activate-vp9, #activate-vp10, #activate-vp11, #activate-vp12, #activate-vp13, #activate-vp14, #activate-vp15, #activate-vp16, #activate-vp17, " +
        "#activate-vp18, #activate-vp19, #activate-vp20, #activate-vp21, #activate-vp22, #activate-vp23, #activate-vp24, #activate-vp25, #activate-vp26, " +
        "#activate-vp27, #activate-vp28, #activate-vp29, #activate-vp30, #activate-vp31, #activate-vp32, #activate-vp33, #activate-vp34, #activate-vp35, " +
        "#activate-vp36, #activate-vp37, #activate-vp38, #activate-vp39, #activate-vp40, #activate-vp41, #activate-vp42, #activate-vp43, #activate-vp44, " +
        "#activate-vp45, #activate-vp46, #activate-vp47, #activate-vp48, #activate-vp49, #activate-vp50, #activate-vp51, #activate-vp52, #activate-vp53, " +
        "#activate-vp54, #activate-vp55, #activate-vp56, #activate-vp57, #activate-vp58, #activate-vp59, #activate-vp60, " +
        "#activate-vp61, #activate-vp62, #activate-vp63, #activate-vp64, #activate-vp65, #activate-vp66, #activate-vp67, " +
        "#activate-vp68, #activate-vp69, #activate-vp70, #activate-vp71, #activate-vp72, #activate-vp73, #activate-vp74, " +
        "#activate-vp75, #activate-vp76, #activate-vp77, #activate-vp78, #activate-vp79, #activate-vp80, #activate-vp81, " +
        "#activate-tb1, #activate-tb2, #activate-tb3, #activate-tb3").fancybox({
        'hideOnContentClick': true
    });
});

function loadPhones() {

    $("#phones").empty();

    var detail = {};
    var html = '';

    $.ajax({
        type: "get",
        async: false,
        crossDomain: true,
        dataType: "json",
        data: "",
        //url: 'https://www.telekom.sk/documents/10179/453831/detail.json',
        // url: 'https://m.telekom.sk/wb/vypredaj3/detail.php',
        url: '/assets/data/detail.json',
        success: function(phone) {
            detail = phone;
        },
        error: function() {
            //alert("Chyba pri posielani ... skus este raz!");
        }
    });

    $.ajax({
        type: "get",
        async: false,
        crossDomain: true,
        dataType: "json",
        data: "",
        //url: 'https://www.telekom.sk/documents/10179/462443/phones.json',
        // url: 'https://m.telekom.sk/wb/vypredaj3/phones.php',
        url: '/assets/data/phones.json',
        success: function(phones) {
            html = generatePhones(phones, detail);
            $(html).appendTo("#phones");

        },
        error: function() {
            //alert("Chyba pri posielani ... skus este raz!");
        }
    });

    html = '';

    $.ajax({
        type: "get",
        async: false,
        crossDomain: true,
        dataType: "json",
        data: "",
        //url: 'https://www.telekom.sk/documents/10179/462443/tablets.json',
        url: 'https://m.telekom.sk/wb/vypredaj3/tablets.php',
        // url: '../assets/data/tablets.json',
        success: function(phones) {

            //console.log(detail);
            html = generatePhones(phones, detail);
            $(html).appendTo("#tablet");
        },
        error: function() {
            //alert("Chyba pri posielani ... skus este raz!");
        }
    });
}

function generatePhones(phones, detail) {
    var html = '';
    var button_order = '';
    var button_vypr = '';
    var button_xsmini = '';
    var xsmini = 0;

    phones.sort(function(a, b) {
        return parseFloat(b.xsmini) - parseFloat(a.xsmini);
    });

    phones.sort(function(a, b) {
        return parseFloat(b.available) - parseFloat(a.available);
    });

    phones.sort(function(a, b) {
        return parseFloat(a.order) - parseFloat(b.order);
    });

    for (var a = 0; a < phones.length; a++) {

        xsmini = 0;

        button_order = '<div class="btn-order">' + '<a class="button" href="' + phones[a].url + '"><span>KÚPIŤ</span></a>' + '</div>';
        button_vypr = '<div class="btn-order">' + '<div class="vypredane">VYPREDANÉ</div>' + '</div>';

        var xsminiDetailUrl = '/vypredaj-telefonov/detail?id=' + phones[a].id;
        button_xsmini = '<div class="btn-order">' + '<a class="button" href="' + xsminiDetailUrl + '"><span>VIAC INFO</span></a>' + '</div>';

        if (phones[a].xsmini == 1) {
            button_order = button_xsmini;
            xsmini = 1;
        }

        if (phones[a].available != 1) {
            button_order = button_vypr;
        }

        if (phones[a].visible == 1) {
            html +=
                '<div class="phone">' +
                '<div class="images">';

            if (xsmini == 1) {
                html += '<img class="img-ec" src="https://www.telekom.sk/documents/10179/462918/pausal_happy_mini_71x71.png">' +
                '<img class="img-ec2" src="https://www.telekom.sk/documents/10179/462918/easy_karta_71x71.png">';
            } else {
                html += '<img class="img-ec" src="https://www.telekom.sk/documents/10179/462918/easy_karta_71x71.png">';
            }

            var detailPrice = $.grep(detail, function(e) {
                return e.id == phones[a].id;
            });
            // console.log("phpne", phones[a]);
            // console.log("detail",detailPrice);
            // console.log(detailPrice[0].image);

            html += '<img class="img-phone" src="' + detailPrice[0].image + '">';
                if (xsmini == 1) {
                    html += '</div>';
                } else {
                    html += '<div class="img-info"><a href="#' + phones[a].id + '" id="activate-' + phones[a].id + '"><img src="https://www.telekom.sk/documents/10179/462918/vypredaj_info.png"></a></div>' +
                        '</div>';
                }
            html += '<h3>' + phones[a].name + '</h3>' +
            	'<div class="desc_wrap">'+
                '<div class="desc">' + phones[a].description['desc1'] + '</div>' +
                '<div class="desc">' + phones[a].description['desc2'] + '</div>' +
                '<div class="desc">' + phones[a].description['desc3'] + '</div></div>';

            if (xsmini == 1) {
                html += '<div class="price-xs">';
                html += '<div class="left"><div class="price">' + detailPrice[0].pecka + ',00 €</div><div class="desc">k Easy karte</div></div>';
                html += '<div class="right"><div class="price">' + detailPrice[0].xsminiMonth + ',00 €/mes.</div><div class="desc">k Happy XS mini</div></div>';
                html += '</div>';
            } else {
                html += '<div class="price">' +
                    '<div class="price1">' + phones[a].price1 + '</div>' +
                    '<div class="price2">' + phones[a].price2 + '</div>' +
                    '<div class="price-eur">€</div>' +
                    '</div>';
            }

            html = html + button_order +
                '</div>';

            button_order = '';
        }

    }

    return html;
}

function loadDetail() {

    $("#detail").empty();

    $.ajax({
        type: "get",
        async: false,
        crossDomain: true,
        dataType: "json",
        data: "",
        //url: 'https://www.telekom.sk/documents/10179/462443/detail.json',
        url: 'https://m.telekom.sk/wb/vypredaj3/detail.php',
        success: function(phones) {
            var html = "";
            for (var a = 0; a < phones.length; a++) {

                xsmini = 0;

                if (typeof phones[a].xsminiOne !== 'undefined') {
                    xsmini = 1;
                }

                html +=
                    '<div class="detail-lightbox" id="' + phones[a].id + '">' +
                    '<div class="detail-header">' +
                    '<h2>' + phones[a].name + '</h2>' +
                    '</div>' +
                    '<div class="detail-desc">' +
                    '<div class="images-detail">';

                if (xsmini == 1) {
                    html += '<img class="detail-ec" src="https://www.telekom.sk/documents/10179/462918/vypredaj_ec_xsimini.png">';
                } else {
                    html += '<img class="detail-ec" src="https://www.telekom.sk/documents/10179/462918/vypredaj_ec.png">';
                }

                html += '<div class="detail-img"><img src="' + phones[a].image + '"></div>' +
                    '</div>' +
                    '<div class="detail-text">' +
                    '<table class="detail-table">';

                var detail = phones[a].description;

                for (var b = 0; b < detail.length; b++) {
                    html +=
                        '<tr>' +
                        '<td><div class="odrazka"></div></td>' +
                        '<td>' + detail[b].desc[0] + '</td>' +
                        '<td class="row-2">' + detail[b].desc[1] + '</td>' +
                        '</tr>';
                }
                html +=
                    '</table>' +
                    '</div>' +
                    '</div>' +
                    '</div>';
            }

            $(html).appendTo("#detail");
        },
        error: function() {
            //alert("Chyba pri posielani ... skus este raz!");
        }
    });
}
