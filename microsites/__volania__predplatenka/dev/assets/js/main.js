function getUrlParam(key) {
  var result = new RegExp(key + "=([^&]*)", "i").exec(window.location.search);
  return result && result[1] || "";
}

$(document).ready(function () {

  $(".fancy").fancybox();

  $('#sec1-tip1').qtip({
    content: 'Po kúpe svojej Predplatenky si môžete aktivovať balík Nekonečné volania alebo balík s 1 GB dát na 30 dní bezplatne. Viac info o balíkoch nájdete nižšie na stránke v časti DOPLNKOVÉ BALÍKY K PREDPLATENKE.',
    position: {
      my: 'bottom center',
      at: 'right top'
    }
  });

  $('#sec1-tip2').qtip({
    content: 'Za minútu hovoru do všetkých sietí v SR (Telekom, Orange, O2 alebo 4ka) zaplatíte 0,10€. Ak dosiahnete hranicu 0,50€, v daný deň už viac za hovory nezaplatíte. <br>Mesačne môžete prevolať až 2000 minút. Po prekročení tohto limitu vám budú ďalšie volania do konca mesiaca spoplatnené sumou 0,10€ za minútu. Viac info v <a href=" https://www.telekom.sk/osobne/pomoc-a-podpora/dokumenty-a-pravne-informacie/dokumenty-na-stiahnutie/cenniky-sluzieb-mobilnej-siete/" target="_blank">Cenníku pre poskytovanie služieb prostredníctvom mobilnej siete v časti A</a>.',
    position: {
      my: 'bottom center',
      at: 'right top',
    },
    hide: {fixed: true, delay: 1000},
    style: {width: {min: 300}}
  });


  $('#sec1-tip3').qtip({
    content: 'Za SMS zaplatíte 0,10€ a za MMS 0,39€ do všetkých sietí (Telekom, Orange, O2 alebo 4ka). Ak pri odosielaní SMS dosiahnete hranicu 0,50€, v daný deň už viac za SMS nezaplatíte. Mesačne môžete odoslať až 2000 SMS správ.',
    position: {
      my: 'bottom center',
      at: 'right top'
    }
  });


  $('#sec1-tip4').qtip({
    content: 'Po pripojení na internet v mobile sa automaticky aktivuje denný balík 300 MB za 0,50€ (kým dosiahnete hranicu 0,50€, platíte za 1 MB 0,10€). Balík je platný v daný deň do 23:59. V ten deň, keď nesurfujete, neplatíte nič. Po vyčerpaní dát z balíka, sa prístup na internet zastaví. Pre obnovenie dát si môžete aktivovať jeden z dátových balíkov k Predplatenke. Rýchlosť internetu: 150 Mb/s.',
    position: {
      my: 'bottom center',
      at: 'right top'
    }
  });

  $('.sec3-tip-30dni').qtip({
    content: 'Po 30 dňoch sa balík pri dostatočnej výške kreditu automaticky obnoví.',
    position: {
      my: 'bottom center',
      at: 'right top'
    }
  });

  $('.sec3-tip-30dni-xmas').qtip({
    content: 'Po 30 dňoch sa balík pri dostatočnej<br> výške kreditu automaticky obnoví.<br><b>1 GB dát zadarmo získate každý<br> mesiac aj po skončení Vianoc</b><br> dovtedy, kým budete mať balík s <br>automatickou obnovou aktívny',
    position: {
      my: 'bottom center',
      at: 'right top'
    }
  });

  $('.sec3-tip-1gb10-fup').qtip({
    content: 'S balíkom 1 GB na 10 dní môžete na Slovensku a v celej EÚ presurfovať 1 GB za 2,00€ / 10 dní. Po minutí 1 GB dát v EÚ mimo SR bude vaša dátová prevádzka pozastavená. Pre ďalšie surfovanie je potrebné dokúpiť si niektorý z dátových balíčkov.',
    position: {
      my: 'bottom center',
      at: 'right top'
    }
  });

  $('.sec3-tip-2gb-fup').qtip({
    content: 'S balíkom 2 GB na deň môžete v EÚ mimo Slovenska presurfovať 731,43 MB za 1,50€ / 24 hodín. Po minutí 731,43 MB dát v EÚ mimo SR v rámci dňa sa vám začne započítavať príplatok 0,0042 € za 1 MB prenesených dát až do výšky 2GB. Po spotrebovaní 2GB bude dátová prevádzka pozastavená. Pre ďalšie surfovanie je potrebné dokúpiť si niektorý z dátových balíčkov.',
    position: {
      my: 'bottom center',
      at: 'right top'
    }
  });

  $('.sec3-tip-1gb30-fup').qtip({
    content: 'S balíkom 1 GB na 30 dní môžete na Slovensku a v celej EÚ presurfovať 1 GB za 3,00€ / 30 dní. Po minutí 1 GB dát v EÚ mimo SR bude vaša dátová prevádzka pozastavená. Pre ďalšie surfovanie je potrebné dokúpiť si niektorý z dátových balíčkov.',
    position: {
      my: 'bottom center',
      at: 'right top'
    }
  });

  $('.sec3-tip-3gb-fup').qtip({
    content: 'S balíkom 3 GB na 30 dní môžete na Slovensku a v celej EÚ presurfovať 3 GB za 6,00€ / 30 dní. Po minutí 3 GB dát v EÚ mimo SR bude vaša dátová prevádzka pozastavená. Pre ďalšie surfovanie je potrebné dokúpiť si niektorý z dátových balíčkov.',
    position: {
      my: 'bottom center',
      at: 'right top'
    }
  });

  $('.sec3-tip-5gb-fup').qtip({
    content: 'S balíkom 5 GB na 30 dní môžete v EÚ mimo Slovenska presurfovať 3,81 GB za 8,00€ / 30 dní. Po minutí 3,81 GB dát v EÚ mimo SR v rámci dňa sa vám začne započítavať príplatok 0,0042 € za 1 MB prenesených dát až do výšky 5GB. Po spotrebovaní 5GB bude dátová prevádzka pozastavená. Pre ďalšie surfovanie je potrebné dokúpiť si niektorý z dátových balíčkov.',
    position: {
      my: 'bottom center',
      at: 'right top'
    }
  });

  $('.sec3-tip-300mb-fup').qtip({
    content: 'S balíkom 300 MB môžete na Slovensku a v celej EÚ presurfovať 300 MB za 0,50€ / deň. Po minutí 300 MB dát v EÚ mimo SR v rámci dňa bude vaša dátová prevádzka pozastavená. Pre ďalšie surfovanie je potrebné dokúpiť si niektorý z dátových balíčkov.',
    position: {
      my: 'bottom center',
      at: 'right top'
    }
  });

  $('.sec3-tip-unl-fup').qtip({
    content: 'S balíkom Nekonečné dáta na deň môžete v EÚ mimo Slovenska presurfovať 975,24 MB za 2,00€&nbsp;/&nbsp;deň. Po minutí 975,24 MB dát v EÚ mimo SR v rámci dňa sa vám začne započítavať príplatok 0,0042 € za 1 MB prenesených dát.',
    position: {
      my: 'bottom center',
      at: 'right top'
    }
  });

  $('.sec3-tip-unl10-fup').qtip({
    content: 'S balíkom Nekonečné pripojenie na 10 dní môžete v EÚ mimo Slovenska presurfovať 1,9 GB za 4,00€ / 10dní. Po minutí 1,9 GB dát v EÚ mimo SR v rámci dňa sa vám začne započítavať príplatok 0,0042 € za 1 MB prenesených dát.',
    position: {
      my: 'bottom center',
      at: 'right top'
    }
  });

  $("#nav-sticky").sticky({
    topSpacing: 0,
    widthFromWrapper: true
  });

  $(".scroll-to").click(function () {
    var comp = $(this);
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $('.' + this.hash.substr(1));
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        if (comp.hasClass('action-data')) {
          $('.a-action-data').click();
        }
        $('html,body').animate({
          scrollTop: target.offset().top - ($("#nav-sticky").outerHeight())
        }, 1000);
        return false;
      }
    }
  });

  var contentSections = $("[id*='sec-']"),
    secondaryNav = $("#nav-sticky");

  function updateSecondaryNavigation() {
    contentSections.each(function () {
      var actual = $(this),
        actualHeight = actual.height() + parseInt(actual.css('paddingTop').replace('px', '')) + parseInt(actual.css('paddingBottom').replace('px', '')),
        actualAnchor = secondaryNav.find('a[href="#' + actual.attr('id') + '"]');

      if ((actual.offset().top - secondaryNav.outerHeight() <= $(window).scrollTop()) && (actual.offset().top + actualHeight - secondaryNav.outerHeight() > $(window).scrollTop())) {
        actualAnchor.addClass('active');
      } else {
        actualAnchor.removeClass('active');
      }

    });
  }

  updateSecondaryNavigation();

  $(window).scroll(function (event) {
    updateSecondaryNavigation();
  });


  $('.popup').fancybox({
    padding: 11,
    margin: 0,
    closeBtn: false,
    parent: "#content",
    helpers: {
      overlay: {
        css: {
          'background': 'rgba(0, 0, 0, 0.7)'
        }
      }
    }
  });

  $('.p-close, .link-close').on('click', function (e) {
    e.preventDefault();
    $.fancybox.close();
  });


  var $sliders = $('.app-slider');

  $(".tabs-content").each(function () {

    var $this = $(this);
    var slick = $this.find($sliders).slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      centerMode: false,
      infinite: false,
      swipe: true,
      arrows: true,
      dots: true,
      autoplay: false,
      initialSlide: 0,
      touchMove: false,
      draggable: false,

      responsive: [{
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          arrows: true,
          dots: true,
        }
      }, {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          arrows: true,
          dots: true,
        }
      }]
    });

  });

  /* ::: DataDevice ::: */
  var $dataDeviceSlider = $('.data-device-slider');
  var $dataDevicePagin = $('.data-device-pagin');

  $(document).on('click', '.tabs > li > a', function (e) {

    console.log('click');

    var $parent = $(this).parent();
    if ($parent.is('.selected')) {
      e.preventDefault();
      // mobile support
      $(this).closest('ul').addClass('active');
    } else {
      var target = $(this).attr('href');
      e.preventDefault();
      $(target).siblings().removeClass('selected');
      $(target).addClass('selected');

      $parent.siblings().removeClass('selected');
      $parent.addClass('selected');
      // mobile support
      $(this).closest('ul').removeClass('active');
    }

    $('.as1').slick('setPosition');
    // $('.as2').slick('setPosition');
    $('.as3').slick('setPosition');

  });

  /* ::: GET CURRENT SLIDE NUM ::: */
  $dataDeviceSlider.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
    //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
    var i = (currentSlide ? currentSlide : 0) + 1;
    $dataDevicePagin.text('(' + i + '/' + slick.slideCount + ')');
  });


  /* ::: PAGINATION INDICATOR ::: */
  if ($(window).width() < 769) {
    $dataDeviceSlider.on('init', function (event, slick) {
      if ($(window).width() < 769) {
        $('.data-device-slider .slick-dots li').css("width", 100 / $('.dataDevice__item').length + '%');
      }
    });
  };

  /* TOGGLE ARROW */
  $('.toggle-arrow, .arrow-right').click(function (event) {
    event.preventDefault();
    if ($(this).hasClass('arrow-right')) {
      return;
    }
    $(event.target).closest('.togleArrow').find('.arrow-content').toggle(200);
    $(this).find('.arrow-right').toggleClass('arrow-rotate');
  });
  /* TOGGLE ARROW END */

  function checkMobileOS() {
    var MobileUserAgent = navigator.userAgent || navigator.vendor || window.opera;
    if (MobileUserAgent.match(/iPad/i) || MobileUserAgent.match(/iPhone/i) || MobileUserAgent.match(/iPod/i)) {
      return 'iOS';
    } else if (MobileUserAgent.match(/Android/i)) {
      return 'Android';
    } else {
      return 'unknown';
    }
  }

  $(".sms_link").each(function (index) {
    $(this).on("click", function () {
      // event.preventDefault();
      var href = '';
      var message_text = this.dataset.smsText;
      var message_number = this.dataset.smsNumber;
      if (checkMobileOS() == 'iOS') {
        href = "sms:" + message_number + "&body=" + encodeURI(message_text);
      }
      if (checkMobileOS() == 'Android') {
        href = "sms:" + message_number + "?body=" + encodeURI(message_text);
      }
      $(this).attr('href', href);
    });
  });

  if (window.location.hash) {
    var elem = window.location.hash.substr(1);

    console.log(elem);

    if ($('.' + elem).length) {

      if (elem === 'nekonecne-pripojenia') {
        changeTabPripojenia();
      }

      setTimeout(function () {
        $('html,body').animate({
          scrollTop: $("." + elem).offset().top - 60 + 'px'
        }, 500);
      }, 500);
    }

  }

  /* FROM TABS TO ACCORDION */
  var dataSegment = $("[data-segment]").each(function () {
    //console.log($(this).attr('data-segment')); //zobraz "data-segment" hodnotu pre kazdu <section>

    closestHead = $($(this).find('.tabs-menu a'));
    closestItemCount = closestHead.length - 1; //vytiahni pocet tabov pre kazdu <section> s atributom "data-segment"
    //console.log(closestItemCount + "heads");

    closestContent = $($(this).find('.tab-content')); //najdi tab-content v danej <section>
    //console.log(closestContent);

    closestAccordion = $(this).find('.accordion'); //najdi .accordion v danej <section>

    accordionItem = '<div class="item">';

    for (var i = 0; i <= closestItemCount; i++) {
      accordionItem += '<div class="heading">' + $(closestHead[i]).text() + '</div>';
      accordionItem += '<div class="content">' + $(closestContent[i]).html() + '</div></div>';

      if (i !== closestItemCount) {
        accordionItem += '<div class="item">';
      }
    }

    //if data-segment and data-accordion value match, show accordion data
    if ($(this).attr('data-segment') === closestAccordion.attr('data-accordion')) {
      $(accordionItem).appendTo(closestAccordion);
    }

    var $items = $('.accordion .item');
    //SET OPENED ITEM
    $($items[0]).addClass('open');

  });
  /* FROM TABS TO ACCORDION END */

  /* ACCORDION */
  $('.accordion .item .heading').click(function (e) {

    var clickedHead = e.target;
    var $item = $(clickedHead).closest('.item');
    var isOpen = $item.hasClass('open');
    var $content = $item.find('.content');
    var $acPrice = $(clickedHead).find('.ac-price');

    if (isOpen) {
      $content.slideUp(200);
      $item.removeClass('open');
      $acPrice.show();
    } else {
      $content.slideDown(200);
      $item.addClass('open');
      $acPrice.hide();
    }
  });


  $('.accordion .item.open').find('.content').slideDown(200);
  /* ACCORDION END */
});


function changeTabPripojenia() {
  $('#sec-5 .tab__holder .tabs-menu .variant.current').removeClass('current');
  $('#nekonecne-pripojenia-tab').addClass('current');
  $('#sec-5 .tab #tab-1').hide();
  $('#sec-5 .tab #tab-2').show();
}