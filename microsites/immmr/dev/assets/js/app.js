$(function () {

    // CACHE THE DOM
    var $pagePhoneNumberVerification = $('#page-verif'),
        $pageSmsVerification = $('#page-verif-sms'),
        $pageActivation = $('#page-activation'),
        $pageThankYou = $('#page-thank-you'),
        $pageThankYouDeactivate = $('#deactivation-thank-you'),
        $phoneNumber = $('.phone-number'),
        $legalContent = $('.legal-content'),
        $formNumberValidation = $('#msisdnForm'),
        $submitBtnFormNumberValidation = $('#msisdn-btn'),
        $formSmsValidation = $('#codeForm'),
        $submitBtnFormSmsValidation = $('#code-btn'),
        $activationAllowed = $('#activation-allowed'),
        $errorNotAvailable = $('#error-non-available'),
        $infoAllreadyActivated = $('#info-allready-activated'),
        $errorNotAvailableAtAll = $('#error-non-available-at-all'),
	    $errorNotAvailableVolte = $('#error-non-available-volte'),

	    $deactivationAllowed = $('#deactivation-allowed'),
	    $deactivationError = $('#deactivation-error'),
	    $deactivateBtn = $('#deactivate'),
	    $deactivate = $('#deactivate-step1'),

        $responseHolders = $('.app-response'),
        $inputPhoneNum = $('#msisdn'),
        $inputSms = $('#code'),
        $editNum = $('.edit-num'),
        $loader = $("#ajaxLoadingSpinnerOverlay"),
        $loaderBoxes = $(".ajaxLoadingSpinnerOverlay-container > div"),
        $checkboxCondition = $('#conditions'),
        $fakeCheckBoxPipe = $('#uniform-conditions > span'),
        $activateBtn = $('#activate'),
        $smsVerifError = $('#verif-error-msg'),
        $errorResponse = $('.error-response'),
        $customErrorHolder = $('.error-msg_custom'),
        phoneNum = '',
        token = '';

	var apiURL = 'https://m2.telekom.sk';
	if (window.location.hostname == 'localhost') {
		apiURL = 'http://mr.telekom.localhost';
	}

    // CONFIG

    var legalContentText = 'Služba Magenta Talk je doplnkovou komunikačnou službou k mesačným programom služieb Happy spoločnosti ' +
        'Slovak Telekom, a.s., ktorá prostredníctvom svojich funkcionalít umožňuje jej užívateľom širší rozmer komunikácie. ' +
        'O bezplatnú aktiváciu a deaktiváciu služby Magenta Talk je možné požiadať prostredníctvom akéhokoľvek predajného kanálu spoločnosti ' +
        'Slovak Telekom, a.s., pokiaľ Osobitné podmienky pre poskytovanie doplnkovej služby Magenta Talk účastníkom spoločnosti Slovak Telekom, a.s. ' +
        '(ďalej len ako „OP“) neustanovujú inak. Okamihom aktivácie služby Magenta Talk sa OP stávajú neoddeliteľnou súčasťou Zmluvy. ' +
        'Poskytovanie služby Magenta Talk je bezplatné, pokiaľ v OP nie je uvedené inak. Účastník berie na vedomie a súhlasí, ' +
        'že služba Magenta Talk umožňuje prístup k službám tiesňového volania prostredníctvom mobilnej telefónnej siete, ' +
        'ale len z koncového zariadenia, ktoré je zapojené do mobilnej telefónnej siete spoločnosti Slovak Telekom, a.s. a v ' +
        'ktorom je aktivovaná SIM karta účastníka spoločnosti Slovak Telekom, a.s. Účastník využivajúci službu Magenta Talk ' +
        'berie na vedomie a súhlasí, že v prípade chýbajúceho dátového pripojenia a za účelom uskutočňovania alebo prijímania ' +
        'hlasových hovorov, služba Magenta Talk môže prekonať túto stratu dátového pripojenia prostredníctvom štandardného prístupu ' +
        'prepájania okruhov k mobilnej telefónnej sieti spoločnosti Slovak Telekom, a.s. Ďalšie podmienky poskytovania, obmedzenia a ' +
        'bližší popis funkcionalít služby Magenta Talk sú uvedené v <a href="https://www.telekom.sk/Documents/OP%20Mob%20105" class="link">OP</a> a ' +
        'na webovej stránke <a href="/magenta-talk/aktivacia" class="link">Magenta Talk</a>.',
        errorResponseText = 'Momentálne nie je možné overiť mobilné číslo. Skúste to prosím neskôr.',
        $mainPage = $pagePhoneNumberVerification,
        phoneNumLength = 10,
        smsLength = 4,
        test = false;


    /* ANALYTICS */

    function thankYouPageLoadAnalytics() {
        dataLayer.push({
            event: 'VirtualPageview',
            virtualPageURL: '/immr/mam-zaujem?auth',
            virtualPageTitle: 'TY immr aktivacia',
            eventCategory: 'immr lead',
            eventAction: 'show'
        });
    }

    function activationBtnAnalytics() {
        dataLayer.push({
            event: 'immr lead',
            eventCategory: 'Lead',
            eventAction: 'click',
            eventLabel: 'TY immr aktivacia'
        });
    }

    /* ANALYTICS END */

    function loadInitialScreen($page) {
        $page.show();
    }

    function loaderShow() {
        $loaderBoxes.addClass('animated');
        $loader.show();
    }

    function loaderHide() {
        $loader.hide();
    }

    function toggleBtnState($btn) {
        if ($btn.is(':disabled')) {
            $btn.prop('disabled', false);
            if ($btn.hasClass('btn_disabled')) {
                $btn.removeClass('btn_disabled');
            }
        } else {
            $btn.prop('disabled', true);
            $btn.addClass('btn_disabled');
        }
    }

    function switchPage($currentPage, $nextPage) {
        $currentPage.hide();
        $nextPage.show();
    }

    function checkIfPlaceHolder($input) {
        var placeholder = $input.eq(0).attr('placeholder');
        var inputVal = $input.eq(0).val();

        if (placeholder == inputVal) {
            if (!$input.hasClass('placeholder')) {
                $input.addClass('placeholder');
            }
            return true;
        } else {
            if ($input.hasClass('placeholder')) {
                $input.removeClass('placeholder');
            }
            return false;
        }
    }

    function checkButton($input, $btn, length) {

        if (test) {
            $btn.removeClass('btn_disabled');
            $btn.prop("disabled", false);
        } else {
            if (!checkIfPlaceHolder($input)) {
                if ($input.eq(0).val().length >= length) {
                    $btn.removeClass('btn_disabled');
                    $btn.prop("disabled", false);
                } else {
                    if (!$input.hasClass('btn_disabled')) {
                        $btn.addClass('btn_disabled');
                        $btn.prop("disabled", true);
                    }
                }
            }
        }

    }

    function focusInput($input) {
        $input.focus();
    }

    function showAppResponse($msgHolder) {
        $responseHolders.hide();
        $msgHolder.show();
    }

    function showErrorResponse() {
        $errorResponse.text(errorResponseText);
    }

    function showErrorMsg($holder, msg) {
        $holder.text(msg);
    }

    function removeShowErrorMsg($holder) {
        if (!($holder).text() == '') {
            $holder.text('');
        }
    }

    function removeErrorResponse() {
        if (!($errorResponse).text() == '') {
            $errorResponse.text('');
        }
    }

    function showResponseError(responseErrorText) {
        $customErrorHolder.text(responseErrorText);
    }

    function removeResponseError() {
        if (!($customErrorHolder).text() == '') {
            $customErrorHolder.text('');
        }
    }

    function loadLegalContent($holder) {
        $holder.html(legalContentText);
    }

    function currentPage() {
        return $('.app__page:visible');
    }

    function editPhoneNumber() {
        switchPage(currentPage(), $pagePhoneNumberVerification);
        setConditionCheckboxToDefaultState();
        removeResponseError();
        phoneNum = '';
        focusInput($inputSms);
    }

    function fillPhoneNumber($fields, phoneNum) {
        $fields.text(phoneNum);
    }

    function emptyInputField($input) {
        $input.val('');
    }

    function toggleConditionsCheckBox() {
        $checkboxCondition.change(function () {
            toggleBtnState($activateBtn);
        });
    }

    function setConditionCheckboxToDefaultState() {
        if ($checkboxCondition.is(':checked')) {
            $checkboxCondition.prop('checked', false);
            toggleBtnState($activateBtn);
            if ($fakeCheckBoxPipe.hasClass('checked')) {
                $fakeCheckBoxPipe.removeClass('checked');
            }
        }
    }

    function phoneVerifSuccess(response) {

        switch (response.response) {
        case 'ok':
            token = response.token;
            fillPhoneNumber($phoneNumber, phoneNum);
            switchPage($pagePhoneNumberVerification, $pageSmsVerification);
            loaderHide();
            focusInput($inputSms);
            break;
        }
    }

    function smsVerifSucces(response) {
        switch (response.response) {
        case 'ok':

            if 	($("#sec-1").hasClass('deactivation')) {
	            if (response.immr === 1 || response.immr == 1) {
		            switchPage($pageSmsVerification, $pageActivation);
		            showAppResponse($deactivationAllowed);
		            loaderHide();
	            } else {
		            switchPage($pageSmsVerification, $pageActivation);
		            showAppResponse($deactivationError);
		            loaderHide();
	            }
            } else {
	            if (response.immr === 1 || response.immr == 1) {
		            switchPage($pageSmsVerification, $pageActivation);
		            showAppResponse($infoAllreadyActivated);
		            loaderHide();
	            } else {
		            switchPage($pageSmsVerification, $pageActivation);
		            showAppResponse($activationAllowed);
		            loaderHide();
	            }
            }

            break;
        case 'error':
            if (response.code === 'wrong_segment') {
	            switchPage($pageSmsVerification, $pageActivation);
	            showAppResponse($errorNotAvailableAtAll);
	            loaderHide();
            } else if (response.code === 'wrong_volte') {
                switchPage($pageSmsVerification, $pageActivation);
                showAppResponse($errorNotAvailableVolte);
                loaderHide();
            } else if (response.code === 'wrong_code') {
                showErrorMsg($smsVerifError, 'Zadali ste nesprávny overovací kód.');
                loaderHide();
            } else {
                switchPage($pageSmsVerification, $pageActivation);
                showAppResponse($errorNotAvailable);
                loaderHide();
            }
            break;
        }
    }

    function activationSuccess(response) {
        switch (response.response) {
        case 'ok':
            console.log($("#sec-1").hasClass('deactivation'));
	        if 	($("#sec-1").hasClass('deactivation')) {
		        switchPage($pageActivation, $pageThankYouDeactivate);
	        } else{
		        switchPage($pageActivation, $pageThankYou);
            }
            loaderHide();

            break;
        case 'error':
            showResponseError(response.desc);
            loaderHide();
            break;
        }
    }

    function ajaxError() {
        showErrorResponse();
        loaderHide();
    }

    function ajaxCall(data, url, success, error) {
        removeErrorResponse();
        removeShowErrorMsg($smsVerifError);

        $.ajax({
            type: "GET",
            async: true,
            crossDomain: true,
            dataType: "json",
            data: data,
            url: url,

            success: function (response) {
                success(response);
            },
            error: function () {
                error();
            }
        });
    }

    // Init script

    // loadLegalContent($legalContent);

    loadInitialScreen($pagePhoneNumberVerification);

    focusInput($inputPhoneNum);

    $inputPhoneNum.keyup(function (event) {

        checkButton($inputPhoneNum, $submitBtnFormNumberValidation, phoneNumLength);

        if (event.keyCode == 13) {
            $submitBtnFormNumberValidation.click();
        }
    });

    $inputSms.keyup(function (event) {

        checkButton($inputSms, $submitBtnFormSmsValidation, smsLength);

        if (event.keyCode == 13) {
            $submitBtnFormSmsValidation.click();
        }
    });

    $inputPhoneNum.keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });

    $inputSms.keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });

    $formNumberValidation.submit(function (e) {
        e.preventDefault();
        phoneNum = $inputPhoneNum.val();
        loaderShow();
        ajaxCall('msisdn=' + phoneNum, apiURL + '/api/immmr/check-mobile', phoneVerifSuccess, ajaxError);
    });

    $editNum.on('click', function () {
        emptyInputField($inputPhoneNum);
        emptyInputField($inputSms);
        editPhoneNumber();
        focusInput($inputPhoneNum);
        toggleBtnState($submitBtnFormNumberValidation);
    });

	$deactivate.on('click', function (e) {
/*	    $("#deactivate-text_step1, #deactivate-text_step2, #deactivate-text_step3").text('Deaktivácia Magenta Talk');
	    $(".deactivate-text").hide();
	    $("#sec-1").addClass('deactivation');
		$deactivate.hide();
		e.preventDefault();*/

        e.preventDefault();
		window.location = '/magenta-talk/deaktivacia';

	});

    $formSmsValidation.submit(function (e) {
        e.preventDefault();
        loaderShow();
        ajaxCall("&token=" + token + "&deactivate=" + checkDeactivation() + "&code=" + $inputSms.val() + "&msisdn=" + phoneNum, apiURL + '/api/immmr/check-code', smsVerifSucces, ajaxError);
    });

    $activateBtn.on('click', function () {
        loaderShow();
        activationBtnAnalytics();
        ajaxCall("msisdn=" + phoneNum + "&token=" + token, apiURL + '/api/immmr/activate', activationSuccess, ajaxError);
    });

	$deactivateBtn.on('click', function () {
        loaderShow();
        ajaxCall("deactivate=1&msisdn=" + phoneNum + "&token=" + token, apiURL + '/api/immmr/activate', activationSuccess, ajaxError);
    });

    toggleConditionsCheckBox();

	function checkDeactivation () {
		var deactivation = 0;
		if ($("#sec-1").hasClass('deactivation')) {
			deactivation = 1;
		}

		return deactivation;
	}

    if(window.location.href.indexOf("deaktivacia") > -1) {
	    $("#deactivate-text_step1, #deactivate-text_step2, #deactivate-text_step3").text('Deaktivácia Magenta Talk');
	    $(".deactivate-text").hide();
	    $("#sec-1").addClass('deactivation');
	    $deactivate.hide();
    }
});
