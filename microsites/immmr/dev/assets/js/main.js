$(function() {
   $('.tooltip-1').qtip({
       content: {
           text: 'Pre overenie vášho telefónneho čísla<br/>vám pošleme overovací SMS kód.'
       },
       position: {
           my: 'bottom center',
           at: 'top center'
       }
   });
});