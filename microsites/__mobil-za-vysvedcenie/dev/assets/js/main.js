$(function() {
  
    /* HEAPBOX CALC - START */
    var heapVal = $('#heapbox_heap .holder').prop('rel'); //init value

    $( document ).ready(function() {
        $('ul.heapOptions').find("li").eq(2).find('a').click(); //set default pausal to "ANO L"
    });

    $('.heapOption a').on('click', function(){
        heapVal = $('#heapbox_heap .holder').prop('rel'); //get new value

        function setPrices(L3MonthlyFeeService, L3MonthlyFeeWithMob, L3Monthlyregular, L3webPrice, L3regularPrice, G7MonthlyFeeService, G7MonthlyFeeWithMob, G7Monthlyregular, G7webPrice, G7regularPrice) {
            $('#L3MonthlyFeeService').text(L3MonthlyFeeService + ",00 €");
            $('#L3MonthylFeeWithMob').text(L3MonthlyFeeWithMob + ",00 €");
            $('#L3Monthlyregular').text(L3Monthlyregular + ",00 €");
            $('#L3webPrice').text(L3webPrice + ",00 €");
            $('#L3regularPrice').text(L3regularPrice + ",00 €");

            $('#G7MonthlyFeeService').text(G7MonthlyFeeService + ",00 €");
            $('#G7MonthylFeeWithMob').text(G7MonthlyFeeWithMob + ",00 €");
            $('#G7Monthlyregular').text(G7Monthlyregular + ",00 €");
            $('#G7webPrice').text(G7webPrice + ",00 €");
            $('#G7regularPrice').text(G7regularPrice + ",00 €");       
        }

        switch (heapVal) {
            case "XXL":
                setPrices(70, 0, 1, 1, 1, 70, 0, 1, 1, 1);
                break;
            case "XL":
                setPrices(40, 0, 1, 1, 1, 40, 0, 1, 1, 1);
                break;
            case "L":
                setPrices(30, 0, 1, 1, 1, 30, 0, 1, 1, 1);
                break;
            case "M_DATA":
                setPrices(20, 1, 3, 1, 19, 20, 0, 2, 1, 9);
                break;
            case "M":
                setPrices(20, 1, 3, 1, 19, 20, 0, 2, 1, 9);
                break;                
            default:
                break;
        }
    });
    /* HEAPBOX CALC - END */

 
});
