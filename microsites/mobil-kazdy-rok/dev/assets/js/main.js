$(function () {
    $('.info-startpage').qtip({
        content: {
            text: 'Pre overenie vášho telefónneho čísla vám pošleme overovací SMS kód.'
        },
        position: {
            my: 'bottom center',
            at: 'top center'
        }
    });
    $('.info-standard-prolong-step1').qtip({
        content: {
            text: 'Krok 1:<br/>Vyberte si nový mobil'
        },
        position: {
            my: 'left center',
            at: 'right center'
        }
    });
    $('.info-standard-prolong-step2').qtip({
        content: {
            text: 'Krok 2:<br/>Potrebujete poradiť s ponukou? Zavoláme Vám.'
        },
        position: {
            my: 'left center',
            at: 'right center'
        }
    });
    $('.info-standard-prolong-step3, .info-number-transfer-step3').qtip({
        content: {
            text: 'Objednávku vám zadarmo doručíme do 1 dňa.'
        },
        position: {
            my: 'left center',
            at: 'right center'
        }
    });
        $('.info-number-transfer-step1').qtip({
        content: {
            text: 'Krok 1:<br/>Stačí vyplniť základné údaje.'
        },
        position: {
            my: 'left center',
            at: 'right center'
        }
    });
    $('.info-number-transfer-step2').qtip({
        content: {
            text: 'Krok 2:<br/>Zabudnite na starosti. Všetko vybavíme za vás!'
        },
        position: {
            my: 'left center',
            at: 'right center'
        }
    });

    $('.tooltip-battery').qtip({
        content: {
            text: 'Darujeme vám externú batériu, ktorá slúži ako nabíjačka mobilu či tabletu aj tam, kde nie je elektrická zásuvka.<br/>'+
                  'Ak sa vám vybije, môžete si ju nabiť sami alebo vám ju vymeníme za nabitú v niektorom z našich Telekom centier až do konca roku 2017.'
        },
        position: {
            my: 'right center',
            at: 'left center'
        }
    });

    var $lightboxActivator = $('.more-info-lightbox'),
        $lightboxElem = $('.board__lightbox'),
        $lightboxClose = $('.lightbox-close');

    $lightboxActivator.on('click', function(e) {
        e.preventDefault();
        $lightboxElem.css('display', 'block');
    });

    $lightboxClose.on('click', function(e) {
        e.preventDefault();
        $lightboxElem.css('display', 'none');
    });

    $(".aly").fancybox({
         padding: 11,
         margin: 0,
         closeBtn: false,
         parent: "#content",
         helpers: {
             overlay: {
                 css: {
                     'background': 'rgba(0, 0, 0, 0.7)'
                 }
             }
         }
    });

    $('.happy_young-info').qtip({
        content: 'Web zľavu až 20 € na mobil získate iba pri kúpe paušálu na našom webe. Konkrétna výška zľavy bude vypočítaná pri nákupe.',
        position: {
            corner: {
                target: 'leftMiddle',
                tooltip: 'topMiddle'
            }
        }
    }).bind('click', function(event) {
        event.preventDefault();
        return false;
    });

    $('.roaming-info').qtip({
        content: 'Všetky členské štáty EÚ a Island, Nórsko, Lichtenštajnsko',
        position: {
            corner: {
                target: 'leftMiddle',
                tooltip: 'topMiddle'
            }
        }
    }).bind('click', function(event) {
        event.preventDefault();
        return false;
    });

    $('.p-close').on('click', function(e) {
        e.preventDefault();
        $.fancybox.close();
    });

});
