var $starHeaderSelector = $("#survey-header");
var $starHeaderDescSelector = $(".header-desc");
var $starDescSelector = $("#star-desc");

var $noteSelector = $("#note");
var starThanks;

var $feedbackFooter = $('.feedback-footer')
var $starAnswer1 = $("#star-answer-1");
var $starAnswer2 = $("#star-answer-2");
var $starAnswer3 = $("#star-answer-3");
var $starAnswer4 = $("#star-answer-4");
var $starAnswer5 = $("#star-answer-5");

var elementId = '';
var surveyAnswerType = '';
var surveyPageType = '';
var surveyAnswerId = '';
var surveyPageId = '';
var surveyQuestionId = '';

var $starNote = $('.star-note');
var $ratingStars = $('.rating-stars');
var $ratingNps = $('.rating-nps');

var pathname = window.location.pathname;
var urlParams = window.location.search;

var dataURL = "https://backvm.telekom.sk/adastra/api/";
if (window.location.hostname === "localhost") {
  dataURL = "http://survey.telekom.localhost/api/";
}

var $insertDiv = $('.portlet-content-container');
if (window.location.hostname === "localhost") {
  $insertDiv = $('.portlet-content-container');
}

var dataURLSave = dataURL + 'save';
var dataURLNote = dataURL + 'note';

var token = create_UUID();

// loadCss();

$(window).bind("load", function () {

  if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
    // return;
  }

  var sourceSwap = function () {
    var $this = $(this);
    var newSource = $this.data('alt-src');
    $this.data('alt-src', $this.attr('src'));
    $this.attr('src', newSource);
  };

  var starShow = getCookieValue('rating-star-showed');

  if (starShow === '') {
    loadSurvey();
  }

  $(function () {
    $('img[data-alt-src]').each(function () {
      new Image().src = $(this).data('alt-src');
    }).hover(sourceSwap, sourceSwap);
  });

  function loadSurvey() {

    $.ajax({
      type: "get",
      async: false,
      crossDomain: true,
      dataType: "json",
      data: "",
      url: dataURL + '?page=' + pathname + '&params=' + escape(urlParams),

      success: function (res) {

        var survey = res;

        if (res['question'] !== undefined) {
          console.log('show survey');

          starThanks = survey['thank_you'];
          elementId = survey['element'];

          $starDescSelector.html(res['question']);
          surveyPageId = survey['page_id'];
          surveyQuestionId = survey['question_id'];
          surveyAnswerId = survey['answer_id'];
          surveyAnswerType = survey['answer_type'];
          surveyPageType = survey['page_type'];

          $starHeaderSelector.html(survey['header']);
          $starHeaderDescSelector.html(survey['description']);

          if (surveyAnswerType == 'nps') {
            $ratingStars.hide();
            $ratingNps.show();

          } else {
            $starAnswer1.html(survey['answers']["1"]);
            $starAnswer2.html(survey['answers']["2"]);
            $starAnswer3.html(survey['answers']["3"]);
            $starAnswer4.html(survey['answers']["4"]);
            $starAnswer5.html(survey['answers']["5"]);
          }

          $feedbackFooter.show();

          if (elementId != '') {
            var $elementId = $(elementId);
            $feedbackFooter.insertAfter($elementId);
          }

        } else {
          console.log('no survey');
        }

      },
      error: function () {
        //alert("Chyba pri posielani ... skus este raz!");
      }
    });
  }

  /* 1. Visualizing things on Hover - See next part for action on click */
  $('#stars li, #nps li').on('mouseover', function () {
    var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on

    // Now highlight all the stars that's not after the current hovered star
    $(this).parent().children('li.star, li.nps').each(function (e) {
      if (e < onStar) {
        $(this).addClass('hover');
      } else {
        $(this).removeClass('hover');
      }
    });
  });

  $('#nps, #stars').on('mouseleave', function () {
    $('li.star, li.nps').removeClass('hover');
  });

  /* 2. Action to perform on click */
  $('#stars li, #nps li').on('click', function () {
    var onStar = parseInt($(this).data('value'), 10); // The star currently selected

    if (surveyAnswerType == 'nps') {
      var stars = $(this).parent().children('li.star, li.nps');
    } else {
      var stars = $(this).parent().children('li.star, li.nps');
    }

    for (i = 0; i < stars.length; i++) {
      $(stars[i]).removeClass('selected');
    }

    for (i = 0; i < onStar; i++) {
      $(stars[i]).addClass('selected');
    }

    $starNote.show();
    $ratingStars.hide();
    $ratingNps.hide();
    $starDescSelector.hide();

    if (surveyPageType == 'thank_you') {
      $starHeaderSelector.hide();
      $starHeaderDescSelector.hide();
    }

    if (window.location.hostname !== "localhost") {
      setCookie();
    }

    // JUST RESPONSE (Not needed)
    if (surveyAnswerType == 'nps') {
      var ratingValue = parseInt($('#nps li.selected').last().data('value'), 10);
    } else {
      var ratingValue = parseInt($('#stars li.selected').last().data('value'), 10);
    }
    responseMessage(starThanks);

    var sendData = {
      question_id: surveyQuestionId,
      page_id: surveyPageId,
      answer: ratingValue,
      token: token,
    };

    $.ajax({
      type: "get",
      async: true,
      crossDomain: true,
      dataType: "json",
      data: sendData,
      url: dataURLSave,

      success: function (res) {
      },
      error: function () {
      }
    });

  });

  function responseMessage(msg) {
    $('.success-box').fadeIn(200);
    $('.success-box div.text-message').html("<span>" + msg + "</span>");
  }

  /* 2. Action to perform on click */
  $('#submit').on('click', function (e) {

    var noteValue = $noteSelector.val();
    $starNote.hide();
    e.preventDefault();

    var sendData = {
      note: noteValue,
      token: token,
    };

    $.ajax({
      type: "get",
      async: true,
      crossDomain: true,
      dataType: "json",
      data: sendData,
      url: dataURLNote,

      success: function (res) {
      },
      error: function () {
      }
    });

  });

  function setCookie() {
    var now = new Date();
    var cookievalue = now;
    var minutes = 20160;

    now.setTime(now.getTime() + (minutes * 60 * 1000));

    document.cookie = "rating-star-showed=" + cookievalue + ";path=/;expires=" + now.toUTCString();

  }

  function getCookieValue(a) {
    var b = document.cookie.match('(^|[^;]+)\\s*' + a + '\\s*=\\s*([^;]+)');
    return b ? b.pop() : '';
  }

});

function create_UUID() {
  var dt = new Date().getTime();
  var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
    var r = (dt + Math.random() * 16) % 16 | 0;
    dt = Math.floor(dt / 16);
    return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
  });
  return uuid;
}

function loadCss(page_type) {
  var link = document.createElement('link');

// set properties of link tag
  if (page_type == 'thank_you') {
    link.href = 'https://backvm.telekom.sk/static/microsites/exponea-survey/deploy/style.css';
  } else {
    link.href = 'https://backvm.telekom.sk/static/microsites/exponea-survey/deploy/style.css';
  }

  link.rel = 'stylesheet';
  link.type = 'text/css';

// Loaded successfully
  link.onload = function () {
    console.log('success');
  };

// Loading failed
  link.onerror = function () {
    console.log('error');
  };

// append link element to html
  document.body.appendChild(link);
}