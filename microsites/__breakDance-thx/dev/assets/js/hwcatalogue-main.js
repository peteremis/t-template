$(document).ready(function() {
  var selectboxes = $(".bd-select");
  var mainRateplans = $(
    ".bd-scenario-hwcatalogue .rateplans .rateplans_wrapper_main"
  );
  var mainCategories = $(
    ".bd-scenario-hwcatalogue .categories .categories_wrapper_main"
  );

  // generate carousels
  bd_generateCarousel = function(el, slidesToShow) {
    const settings = {
      slidesToShow: slidesToShow,
      slidesToScroll: 1,
      variableWidth: true,
      centerMode: false,
      arrows: true,
      infinite: false,
      touchThreshold: 10,
      swipeToSlide: true,
      swipe: true,
      touchMove: true,
      draggable: true,
      prevArrow:
        "<button type='button' class='slick-prev pull-left'><i class='fa fa-angle-left' aria-hidden='true'></i></button>",
      nextArrow:
        "<button type='button' class='slick-next pull-right'><i class='fa fa-angle-right' aria-hidden='true'></i></button>",
      responsive: [
        {
          breakpoint: 768,
          settings: {
            slidesToShow: slidesToShow,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 2560,
          settings: "unslick"
        }
      ]
    };

    var slider = $(el).slick(settings);

    $(window).on("resize", function() {
      if ($(window).width() < 768 && !slider.hasClass("slick-initialized")) {
        console.log("slider initialised...");
        $(el).slick(settings);
      }
    });
  };

  // generate range sliders in filters
  bd_generateSlider = function(
    sliderID,
    filterName,
    min,
    max,
    startVal,
    endVal,
    unit
  ) {
    var filtersSliders_wrapper = $(".filters-allFilters .filters-sliders");
    var filterHTML =
      '<div class="filter clearfix">' +
      '<p class="filterName">' +
      filterName +
      "</p>" +
      '<div class="clearfix filterData">' +
      '<input class="ui-slider-from" type="text" readonly>' +
      '<input class="ui-slider-to" type="text" readonly>' +
      '<div id="' +
      sliderID.replace(/^#/, "") +
      '" class="sliderWrapper">' +
      "</div>" +
      "</div>" +
      "</div>";
    filtersSliders_wrapper.append(filterHTML);

    $(sliderID).slider({
      range: true,
      min: min,
      max: max,
      values: [startVal, endVal],
      slide: function(event, ui) {
        $(sliderID)
          .parent(".filterData")
          .find(".ui-slider-from")
          .val(ui.values[0] + " " + unit);
        $(sliderID)
          .parent(".filterData")
          .find(".ui-slider-to")
          .val(ui.values[1] + " " + unit);
      }
    });

    $(sliderID)
      .parent(".filterData")
      .find(".ui-slider-from")
      .val(startVal + " " + unit);
    $(sliderID)
      .parent(".filterData")
      .find(".ui-slider-to")
      .val(endVal + " " + unit);
  };

  // user checker
  $("#checkPin").click(function() {
    var pin = "";
    $(".input-pin .input_text").each(function() {
      pin += $(this).val();
    });

    if (pin.length !== 4) return;

    showPopup(getPopupContent("inProgress").content);

    checkPhone(null, pin).then(function(data) {
      var popup = getPopupContent(data.status);
      var icon = data.status === "SHOW_OFFER" ? "V" : ">";

      showPopup(popup.content);
      $(".overenie .result")
        .find(".result-icon i")
        .text(icon)
        .end()
        .find(".result-content")
        .html(popup.result)
        .end()
        .show();

      $(".overenie .form")
        .find(".input-pin .input_text")
        .val("")
        .end()
        .find(".input-phone .input_text")
        .val("")
        .end()
        .find(".input-pin")
        .hide()
        .end()
        .find(".input-phone")
        .show()
        .end();

      if (data.status === "SHOW_OFFER") {
        $(".overenie .form").hide();
        $(".user-offer").show();
      }
    });
  });

  getPopupContent = function(type) {
    var content = "";
    var result = "";
    var phoneNum = $("#phoneNumber").val();
    switch (type) {
      case "inProgress":
        content =
          '<div class="p-content">' +
          '<div class="popup-lightbox">' +
          '<div class="loader-spinning">' +
          '<i class="t-icon--magenta d-block s3">o</i>' +
          "</div>" +
          '<h4 class="headline">Prebieha overovanie Vášho čísla. Ďakujeme za trpezlivosť.</h4>' +
          "</div>" +
          "</div>";
        break;

      case "noOffer":
        content =
          '<div class="p-content">' +
          '<div class="popup-lightbox">' +
          '<img src="../assets/img/verify_failed.png" />' +
          '<h4 class="headline">Ľutujeme, momentálne pre Vás nemáme žiadnu ponuku na mieru.</h4>' +
          '<p class="text">Pozrite našu štandardnú ponuku paušálov.</p>' +
          '<a href="#" class="btn btn_mag-outline popup-close">Pokračovať</a>' +
          "</div>" +
          "</div>";
        result =
          "Pre číslo <b>" +
          phoneNum +
          "</b> momentálne nemáme žiadnu ponuku na mieru.Pozrite si našu štandardnú ponuku paušálov.";
        break;

      case "SHOW_OFFER":
        content =
          '<div class="p-content">' +
          '<div class="popup-lightbox">' +
          '<img src="../assets/img/verify_success.png" />' +
          '<h4 class="headline">Overenie prebehlo úspešne, pozrite si ponuku šitú na mieru pre Vás.</h4>' +
          '<a href="#" class="btn btn_mag-outline popup-close">Pokračovať</a>' +
          "</div>" +
          "</div>";
        result =
          "Overenie čísla prebehlo úspešne. Pozrite si ponuku šitú na mieru pre toto číslo. <br / >" +
          '<p class="num">' +
          phoneNum +
          "</p>" +
          "<p>" +
          '<a href="#" class="magenta">' +
          "Overiť iné číslo alebo zrušiť overenie " +
          '<i class="t-icon--magenta" >e</i>' +
          "</a>" +
          "</p>";
        break;

      case "error":
        content =
          '<div class="p-content">' +
          '<div class="popup-lightbox">' +
          '<img src="../assets/img/verify_failed.png" />' +
          '<h4 class="headline">Ľutujeme, Vaše číslo nebolo možné overiť.</h4>' +
          '<p class="text"><b>Skúste ho overiť neskôr</b> alebo si pozrite našu štandardnú ponuku.</p>' +
          '<a href="#" class="btn btn_mag-outline popup-close">Pokračovať</a>' +
          "</div>" +
          "</div>";
        result =
          "Vaše číslo <b>" +
          phoneNum +
          "</b> nebolo možné z technických príčin overiť.Skúste to neskôr.";
        break;
    }

    return {
      content,
      result
    };
  };

  checkPhone = async function(phone, pin) {
    var url = "";
    if (pin === null) {
      url = "https://api.myjson.com/bins/zkz90";
    } else if (phone === null) {
      var phoneNum = $("#phoneNumber").val();
      if (phoneNum.startsWith("01")) url = "https://api.myjson.com/bins/zkz90";
      // PROLONGATION
      else if (phoneNum.startsWith("02"))
        url = "https://api.myjson.com/bins/lwfro";
      // PROLONGATION - addon
      else if (phoneNum.startsWith("03"))
        url = "https://api.myjson.com/bins/14yb4k";
      // TARIFFCHANGE
      else if (phoneNum.startsWith("11"))
        url = "https://api.myjson.com/bins/g9hfw";
      // noOffer
      else if (phoneNum.startsWith("22"))
        url = "https://api.myjson.com/bins/kukuruku"; // error
    }
    return new Promise((resolve, reject) => {
      $.ajax({
        url,
        dataType: "json",
        success: function(data) {
          resolve(data);
        },
        error: function() {
          resolve({
            status: "error"
          });
        }
      });
    });
  };

  // toggle "all filters"
  $(".filters-toggle-button").on("click", function(e) {
    e.preventDefault();

    $(this)
      .find("i")
      .hasClass("fa-sliders")
      ? $(this)
          .find("i")
          .removeClass("fa-sliders")
          .addClass("fa-times")
      : $(this)
          .find("i")
          .removeClass("fa-times")
          .addClass("fa-sliders");

    $(".filters-allFilters").slideToggle("fast");
  });

  // click on "show more filters"
  $(".showmore").on("click", function(e) {
    e.preventDefault();
    var inputs = $(this)
      .parent()
      .find(".input-wrapper");

    if (
      $(this)
        .find("a span")
        .text()
        .trim() == "Viac"
    ) {
      console.log(inputs);
      for (let index = 0; index < inputs.length; index++) {
        const input = inputs[index];
        if ($(input).hasClass("hidden")) {
          $(input).removeClass("hidden");
        }
      }
      $(this)
        .find("a span")
        .text("Menej");
    } else {
      for (let index = 0; index < inputs.length; index++) {
        const input = inputs[index];
        if (index >= 7) {
          $(input).addClass("hidden");
        }
      }
      $(this)
        .find("a span")
        .text("Viac");
    }
  });

  // legals toggle
  $(".legals-link.legals-1").on("click", function(e) {
    e.preventDefault();
    $("#legals-1").toggle("slow");
  });

  // init <select>
  selectboxes.select2({
    theme: "classic selectbox",
    minimumResultsForSearch: -1,
    width: "100%"
  });

  //init carousels
  bd_generateCarousel(mainRateplans, 2);
  bd_generateCarousel(mainCategories, 2);

  // init filters
  bd_generateSlider("#slider", "Cena", 1, 50, 10, 40, "€");
  bd_generateSlider("#slider2", "Displej", 1, 100, 30, 55, '"');
  bd_generateSlider("#slider3", "Fotoaparát", 1, 20, 12, 16, "Mpx");
});
