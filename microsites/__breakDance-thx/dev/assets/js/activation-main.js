$(document).ready(function() {
  // generate popup windows
  getPopupContent = function(type) {
    var content = "";
    var result = "";
    var phoneNum = $("#phoneNumber").val();
    switch (type) {
      case "inProgress":
        content =
          '<div class="p-content">' +
          '<div class="popup-lightbox">' +
          '<div class="loader-spinning">' +
          '<i class="t-icon--magenta d-block s3">o</i>' +
          "</div>" +
          '<h4 class="headline">Prebieha overovanie Vášho čísla. Ďakujeme za trpezlivosť.</h4>' +
          "</div>" +
          "</div>";
        break;

      case "noOffer":
        content =
          '<div class="p-content">' +
          '<div class="popup-lightbox">' +
          '<img src="../assets/img/verify_failed.png" />' +
          '<h4 class="headline">Ľutujeme, Váš paušál ešte nie je možné predĺžiť.</h4>' +
          '<p class="text">Môžete si však vybrať paušál s novým telefónnym číslom.</p>' +
          '<a href="#" class="btn btn_mag-outline popup-close">Zavrieť okno</a>' +
          "</div>" +
          "</div>";
        result =
          "Pre číslo <b>" +
          phoneNum +
          "</b> momentálne nemáme žiadnu ponuku na mieru.Pozrite si našu štandardnú ponuku paušálov.";
        break;

      case "SHOW_OFFER":
        content =
          '<div class="p-content">' +
          '<div class="popup-lightbox">' +
          '<i class="t-icon--magenta s3 iconDone">V</i>' +
          '<h4 class="headline">Overenie údajov prebehlo úspešne, môžete pokračovať v objednávke.</h4>' +
          '<a href="#" class="btn btn_mag-outline popup-close">Pokračovať</a>' +
          "</div>" +
          "</div>";
        $("#verifiedNum").show();
        $("#verifiedNum .num").text(phoneNum);
        $(".activation-type-text.infoText").show();
        result = null;
        break;

      case "otherNumber":
        content =
          '<div class="p-content">' +
          '<div class="popup-lightbox">' +
          '<div class="iconDone"><i class="t-icon--magenta--border s3">ä</i></div>' +
          '<h4 class="headline">Ste si istí, že chcete overiť iné číslo?</h4>' +
          '<p class="text">Overením iného čísla sa váš nákupný košík vyprázdni a ponuka vynuluje.</p>' +
          '<a href="#" class="btn cta_mag popup-close" id="resetForm">Overiť iné číslo</a>' +
          '<a href="#" class="btn btn_mag-outline popup-close" >Zavrieť okno</a>' +
          "</div>" +
          "</div>";
        result = null;
        break;

      case "PENALTY":
        content =
          '<div class="p-content">' +
          '<div class="popup-lightbox">' +
          '<i class="t-icon--magenta s3 iconDone">V</i>' +
          '<h4 class="headline">Overenie údajov prebehlo úspešne, môžete pokračovať v objednávke.</h4>' +
          '<a href="#" class="btn btn_mag-outline popup-close">Pokračovať</a>' +
          "</div>" +
          "</div>";
        result =
          "<span>Vážený zákazník, svoju zmluvu si môžete predĺžiť a vybrať si paušál a nové zariadenie už teraz.</span>" +
          "<span>Vo Vašej prvej faktúre po predĺžení <b>nájdete jednorazový doplatok za predčasné ukončenie aktuálnej viazanosti</b>. Výška jednorazového doplatku zodpovedá neuhradenej doplnkovej časti mesačného poplatku za čas predčasnej zmeny Vášho aktuálneho paušálu.</span>" +
          "<span>Výšku Vášho jednorazového doplatku si môžete overiť v tabuľke č. 2 Dodatku uzavretého na základe tejto objednávky, a to ešte pred jeho uzavretím. Dodatok Vám prinesie kuriér.</span>" +
          "<span>Ak máte záujem prolongovať bez jednorazového doplatku za predčasnú prolongáciu, počkajte prosím do termínu ....., kedy končí Vašaviazanosť.</span>";
        $("#verifiedNum .num").text(phoneNum);
        $("#verifiedNum").show();
        break;

      case "error":
        content =
          '<div class="p-content">' +
          '<div class="popup-lightbox">' +
          '<img src="../assets/img/verify_failed.png" />' +
          '<h4 class="headline">Ľutujeme, Vaše číslo nebolo možné z technických príčin overiť. Skúste to neskôr.</h4>' +
          '<a href="#" class="btn btn_mag-outline popup-close">Zavrieť okno</a>' +
          "</div>" +
          "</div>";
        result =
          "Vaše číslo <b>" +
          phoneNum +
          "</b> nebolo možné z technických príčin overiť. Skúste to neskôr.";
        break;
    }

    return {
      content,
      result
    };
  };

  //check phone number
  checkPhone = async function(phone, pin) {
    removeErrors();
    $(".result").hide();

    var url = "";
    if (pin === null) {
      //** check PHONE num **//
      url = "https://api.myjson.com/bins/zkz90";

      //** Check PIN num **//
    } else if (phone === null) {
      var phoneNum = $("#phoneNumber").val();

      // SHOW OFFER
      if (phoneNum.startsWith("01")) url = "https://api.myjson.com/bins/zkz90";
      // ERROR - noOffer
      else if (phoneNum.startsWith("11"))
        url = "https://api.myjson.com/bins/g9hfw";
      // PENALTY
      else if (phoneNum.startsWith("15")) return { status: "PENALTY" };
      // intergation error
      else if (phoneNum.startsWith("22"))
        url = "https://api.myjson.com/bins/kukuruku";
    }

    return new Promise((resolve, reject) => {
      $.ajax({
        url,
        dataType: "json",
        success: function(data) {
          console.log(data);
          resolve(data);
          $(".activation-type-text.numberText").hide();
        },
        error: function() {
          resolve({
            status: "error"
          });
        }
      });
    });
  };

  // prevent double event firing for <labels> and <inputs>
  $(".activation-type")
    .find("label")
    .on("click", function(e) {
      e.preventDefault();
    });

  // change activation type on click
  $(".activation-type").on("click", function() {
    var self = $(this);
    var radio = self.find('input[type="radio"]').parent();
    var selectedRadioVal = self.find('input[type="radio"]').val();

    radio.addClass("checked");
    self
      .addClass("selected")
      .siblings(".activation-type")
      .removeClass("selected")
      .end()
      .siblings(".activation-type")
      .find(".checked")
      .removeClass("checked");

    // scenario - prolong
    if (selectedRadioVal == "prolong") {
      setTimeout(function() {
        if (self.find("#phoneNumber").is(":visible") || $(".input-pin")) {
          self.find("#phoneNumber").focus();
        }
      }, 500);

      if ($(".overenie .input-pin").is(":visible")) {
        $(".activation-type-text.numberText").hide();
      }

      if ($(".overenie .input-phone").is(":visible")) {
        $(".activation-type-text.numberText").show();
      }

      if (!$("#verifiedNum").is(":visible")) {
        $(".overenie .result").hide();
        $(".activation-type-text.infoText").hide();
        //$(".activation-type-text.numberText").show();
      }
      // scenario - new number
    } else if (selectedRadioVal == "newNum") {
      $(".activation-type-text.infoText").show();
      $(".activation-type-text.numberText").hide();
      $(".overenie .result, #verifiedNum").hide();
      $(".overenie .form").show();
    }

    //DELETE LOG
    console.log(selectedRadioVal);
  });

  // user wants to use other number - SHOW POPUP
  $("#checkNewNum").on("click", function(e) {
    showPopup(getPopupContent("otherNumber").content);
  });

  // user wants to use other number - RESET FORM
  $(document).on("click", "#resetForm", function(e) {
    e.preventDefault();

    $(".submitformbtn")
      .addClass("disabled")
      .attr("disabled");

    $(".overenie .result, #verifiedNum").hide();
    $(".overenie .form").show();

    $(".activation-type-text.infoText").hide();
    $(".activation-type-text.numberText").show();
  });

  function throwErrorMsg(msg) {
    removeErrors();
    var err = '<p class="overenie error">' + msg + "</p>";
    var wrapper = $(".input-pin .input-wrapper");
    wrapper.append(err);
  }

  function removeErrors() {
    if ($(".overenie.error").length) $(".overenie.error").remove();
  }

  // check PIN
  $("#checkPin").click(function() {
    removeErrors();
    var pin = "";
    $(".input-pin .input_text").each(function() {
      pin += $(this).val();
    });

    if (pin.length !== 4)
      return throwErrorMsg(
        "Overovací kód je príliš krátky. Skúste to prosím znova."
      );

    //check if PIN is valid
    if (parseInt(pin) !== 1111)
      return throwErrorMsg(
        "Overovací kód nie je správny. Skontrolujte kód a skúste to prosím znova."
      );

    showPopup(getPopupContent("inProgress").content);

    checkPhone(null, pin).then(function(data) {
      $(".activation-type-text.numberText").hide();
      var popup = getPopupContent(data.status);
      var icon = data.status === "SHOW_OFFER" ? "V" : ">";

      showPopup(popup.content);

      if (popup.result != null) {
        $(".overenie .result")
          .find(".result-icon i")
          .text(icon)
          .end()
          .find(".result-content")
          .html(popup.result)
          .end()
          .show();
      }

      $(".overenie .form")
        .find(".input-pin .input_text")
        .val("")
        .end()
        .find(".input-phone .input_text")
        .val("")
        .end()
        .find(".input-pin")
        .hide()
        .end()
        .find(".input-phone")
        .show()
        .end();

      $("html,body").animate({
        scrollTop: $(".activation-type.selected").offset().top - 30
      });

      if (data.status === "SHOW_OFFER" || data.status === "PENALTY") {
        $(".overenie .form").hide();
        $(".user-offer").show();

        //enable btn
        $(".submitformbtn")
          .removeClass("disabled")
          .removeAttr("disabled");
      }
    });
  });
});
