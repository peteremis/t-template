$(document).ready(function() {
  // !!! ONLY FOR DEV PURPOSES - DELETE THIS
  var dataURL = "/documents/10179/18062247/data.json";
  if (window.location.hostname === "localhost") {
    dataURL = "http://localhost:3000/assets/js/data.json";
  }
  // ONLY FOR DEV PURPOSES - DELETE THIS !!!

  var selectboxes = $(".bd-select");
  var courierSelectbox = $(".delivery-store .bd-select");
  var personalFormWrapper = $(".personalFormWrapper");
  var deliveryFormWrapper = $(".deliveryFormWrapper");
  var termsFormWrapper = $(".termsFormWrapper");
  var personalDataform_shortDesc = $(".forms-validated");
  var editPersonalInfo_btn = $(".editbtn");
  var revalidationInfoBox = $(".revalidation");
  var submitbtn = $(".submitbtn");
  var validated = false;
  var deliveryByPostValid = false;
  var deliveryAddressSwitcher = document.querySelector(
    ".delivery-address-switch"
  );
  var postponedOrderBtn = $(".postponedOrder");

  var verify_inProgress =
    '<div class="p-content">' +
    '<div class="popup-lightbox">' +
    '<div class="loader-spinning">' +
    '<i class="t-icon--magenta d-block s3">o</i>' +
    "</div>" +
    '<h4 class="headline">Prebieha overovanie údajov</h4>' +
    '<p class="text">Milý zákazník, overujeme údaje objednávky. Bude to trvať najviac 10 sekúnd.</p>' +
    "</div>" +
    "</div>";
  var verify_done =
    '<div class="p-content">' +
    '<div class="popup-lightbox">' +
    '<i class="t-icon--magenta s3 iconDone">V</i>' +
    '<h4 class="headline">Overenie údajov prebehlo úspešne, môžete pokračovať v objednávke.</h4>' +
    '<a href="#" class="btn btn_mag-outline popup-close">Pokračovať</a>' +
    "</div>" +
    "</div>";
  var verify_withGuarantee =
    '<div class="p-content">' +
    '<div class="popup-lightbox">' +
    '<div class="iconDone"><i class="t-icon--magenta--border s3">></i></div>' +
    '<p class="text"><b>Pre uskutočnenie objednávky bude potrebné uhradiť zábezpeku vo výške 100 €.</b><br> Ak so zábezpekou súhlasíte, dokončite objednávku. <b>Ozveme sa vám telefonicky a prejdeme si s Vami detaily.</b> Bližšie informácie o zábezpeke a jej podmienkach nájdete vo <a href="#" class="magenta">Všeobecných obchodných podmienkach.</a><br> V prípade, že zábezpeku platiť nechcete, môžete si vziať paušál bez zariadenia.</p>' +
    '<a href="#" class="btn cta_mag guaranteebtn">Súhlasím so zálohou</a>' +
    '<a href="#" class="btn btn_mag-outline guaranteebtn">Chcem paušál bez zariadenia</a>' +
    "</div>" +
    "</div>";
  var verify_badData =
    '<div class="p-content">' +
    '<div class="popup-lightbox">' +
    '<img src="../assets/img/verify_failed.png" />' +
    '<h4 class="headline small">Mrzí nás to, na základe poskytnutých údajov nie je možné dokončiť túto objednávku.</h4>' +
    '<p class="text mb0">Skontrolujte si:</p>' +
    '<ul class="text"><li>zadané číslo občianskeho preukazu,</li><li>či máte uhradené všetky faktúry v Telekome.</li></ul>' +
    '<p class="text">Alebo sa zastavte osobne v jednom z našich <a href="#" class="magenta">Telekom centier.</a></p>' +
    '<a href="#" class="btn btn_mag-outline popup-close">Zavrieť okno</a>' +
    "</div>" +
    "</div>";
  var verify_failed =
    '<div class="p-content">' +
    '<div class="popup-lightbox">' +
    '<img src="../assets/img/verify_failed.png" />' +
    '<h4 class="headline">Overenie údajov nebolo možné dokončiť.</h4>' +
    '<p class="text">Pokračujte v objednávke a odošlite ju. <b>Bude vás kontaktovať náš agent<b>, ktorý s Vami preverí a doplní potrebné údaje.</p>' +
    '<a href="#" class="btn btn_mag-outline popup-close">Pokračovať</a>' +
    "</div>" +
    "</div>";
  var order_postponed =
    '<div class="p-content">' +
    '<div class="popup-lightbox">' +
    '<div class="postponedIcon"><i class="t-icon--magenta s3">m</i></div>' +
    '<h4 class="headline">Uložte si túto variantu objednávky.</h4>' +
    '<p class="text">Odkaz na rozpracovanú objednávku Vám odošleme na Vami uvedený kontakt. <b>V objednávke môžete kedykoľvek pokračovať.</b></p>' +
    '<div class="postponedOrderFormWrapper"><form id="postponedOrder" action="#" name="postponedOrder">' +
    '<div class="input-wrapper postponed-email">' +
    '<input type="text" class="input_text" name="postponedEmail" required />' +
    '<label class="placeholder" for="postponedEmail">E-mail</label>' +
    "</div>" +
    '<div class="input-wrapper postponed-phoneNum">' +
    '<input type="text" class="input_text" name="postponedPhoneNum" required />' +
    '<label class="placeholder" for="postponedPhoneNum">SMS na číslo</label>' +
    "</div>" +
    "</form></div>" +
    '<a href="#" class="btn cta_mag ">Uložiť objednávku na neskôr</a>' +
    '<a href="#" class="btn btn_mag-outline popup-close">Zavrieť okno</a>' +
    "</div>" +
    "</div>";

  var functions = {
    bd_generatePopup: function(ratioValue, content, responsive, width) {
      $.fancybox({
        content: content,
        padding: 0,
        margin: 0,
        width: width,
        height: "auto",
        autoSize: false,
        topRatio: ratioValue,
        closeBtn: false,
        wrapCSS: "bd-scenario-popup " + responsive,
        parent: "#content",
        helpers: {
          overlay: {
            closeClick: false,
            css: {
              background: "rgba(0, 0, 0, 0.7)"
            }
          }
        }
      });
    },
    // popup window init
    bd_openPopup: function(content) {
      if ($(document).innerWidth() <= 767) {
        functions.bd_generatePopup(1, content, "mobile", "100%");
      } else {
        functions.bd_generatePopup(0.5, content, "desktop", "50%");
      }
    },
    // scroll to target element
    bd_scrollToEl: function(el) {
      $("html, body").animate(
        {
          scrollTop: el.offset().top
        },
        800
      );
    },
    // DELIVERY OPTIONS <select>
    // user have to choose between "courier" or "store"
    bd_changeDeliveryOptions: function(selectedVal) {
      if (selectedVal == "kurierom") {
        $(".instore-img, .delivery-store").hide();
        $(".courier-img, .delivery-courier").show();
        $(".feeinfo").html(
          "Poplatok za doručenie kuriérom<br><b>zadarmo</b><br>"
        );
      } else if (selectedVal == "vpredajni") {
        $(".courier-img, .delivery-courier").hide();
        $(".instore-img, .delivery-store").show();
        $(".feeinfo").html(
          "Poplatok za vyzdvihnutie v predajni<br><b>zadarmo</b><br>"
        );
      }
    },
    // generate short summary for "personal data form"
    bd_generateShortSummary: function() {
      var fullName =
        $("input[name='firstName']").val() +
        " " +
        $("input[name='lastName']").val();
      var phoneNum = $("input[name='phoneNum']").val();
      var email = $("input[name='email']").val();
      var address =
        $("input[name='address']").val() +
        " " +
        $("input[name='streetNum']").val() +
        " " +
        $("input[name='zipcode']").val();
      var birthNum = $("input[name='birthNum']").val();
      var idNum = $("input[name='userIdNum']").val();
      var billingOption = $("select[name='billing']").val();

      $(".short-fullName .value").text(fullName);
      $(".short-phoneNum .value").text(phoneNum);
      $(".short-email .value").text(email);
      $(".short-address .value").text(address);
      $(".short-birthNum .value").text(birthNum);
      $(".short-userIdNum .value").text(idNum);
      $(".short-billing .value").text(billingOption);
    }
  };

  // init ALL <select>
  selectboxes
    .select2({
      theme: "classic selectbox",
      minimumResultsForSearch: -1,
      width: "100%"
    })
    .on("change", function(e) {
      var selectedOption = $(this).val();
      functions.bd_changeDeliveryOptions(selectedOption);

      if (
        // if delivery store form is visible, validate <select>
        // handle error classes for "region" and "store" <select>
        $(deliveryForm)
          .find(".delivery-store")
          .is(":visible")
      ) {
        if ($(this).val() != 0) {
          $(this)
            .parent()
            .find(".selectbox")
            .removeClass("error");

          var deliveryStoreSelectboxes = $(deliveryForm).find(
            ".delivery-store .selectbox"
          );

          deliveryByPostValid = true;
          for (
            var index = 0;
            index < deliveryStoreSelectboxes.length;
            index++
          ) {
            var storeSelectbox = deliveryStoreSelectboxes[index];
            $(storeSelectbox).hasClass("error")
              ? (deliveryByPostValid = false)
              : "";
          }
        } else {
          // user did not selected "region" and "store"
          $(this)
            .parent()
            .find(".selectbox")
            .addClass("error");
          deliveryByPostValid = false;
        }
      }
    });

  // init DELIVERY ADDRESS <switcher>
  new Switchery(deliveryAddressSwitcher, {
    color: "#e20074",
    secondaryColor: "#d0d0d0"
  });

  // on init add error classes for "store delivery" <select>
  for (var index = 0; index < courierSelectbox.length; index++) {
    const element = courierSelectbox[index];
    $(element)
      .data("select2")
      .$container.addClass("error");
  }

  // user wants delivery to other address
  deliveryAddressSwitcher.onchange = function() {
    deliveryAddressSwitcher.checked
      ? $(".delivery-courier .delivery-inputs").show()
      : $(".delivery-courier .delivery-inputs").hide();
  };

  // popup windows close
  $(document).on("click", ".popup-close", function(e) {
    e.preventDefault();
    $.fancybox.close();
  });

  // legals toggle
  $(".legals-link.legals-1").on("click", function(e) {
    e.preventDefault();
    $("#legals-1").toggle("slow");
  });

  // show revalidation info box when user type to "personal data form" inputs
  personalDataForm.find("input, select").on("keyup change", function() {
    validated && personalFormWrapper.is(":visible")
      ? revalidationInfoBox.removeClass("hidden")
      : "";
  });

  // edit "personal data" btn
  editPersonalInfo_btn.on("click", function(e) {
    e.preventDefault();
    personalDataform_shortDesc.hide();
    personalFormWrapper.show();
    deliveryFormWrapper.hide();
    termsFormWrapper.hide();
    submitbtn.text("Overiť údaje");
  });

  // show postponed order form in popup
  postponedOrderBtn.on("click", function(e) {
    e.preventDefault();
    functions.bd_openPopup(order_postponed);
  });

  // ** VALIDATE FORM AND SUBMIT ** //
  $(submitbtn).on("click", function(e) {
    e.preventDefault();
    if ($(personalDataForm).valid()) {
      if ($(".delivery-store").is(":visible") && deliveryByPostValid == false) {
        // if delivery to store is selected and user did not selected "region" & "store", form is not valid
        return;
      }

      validated = true;
      revalidationInfoBox.addClass("hidden");
      functions.bd_openPopup(verify_inProgress);

      $.ajax({
        url: dataURL,
        type: "GET",
        async: true,
        crossDomain: true,
        dataType: "json",
        success: function(data) {
          // !!! DELETE ALL TIMEOUTS !!! TIMEOUTS ARE ONLY FOR DEV PURPOSES !!!
          setTimeout(function() {
            if (data.response == true) {
              functions.bd_openPopup(verify_done);
              personalDataform_shortDesc.show();
              functions.bd_generateShortSummary();
              personalFormWrapper.hide();
              deliveryFormWrapper.show();
              termsFormWrapper.show();

              functions.bd_scrollToEl(personalDataform_shortDesc);

              deliveryFormWrapper.is(":visible")
                ? submitbtn.text("Objednať")
                : submitbtn.text("Overiť údaje");

              // setTimeout(function() {
              //   bd_openPopup(verify_withGuarantee);
              // }, 6000);

              // setTimeout(function() {
              //   bd_openPopup(verify_badData);
              // }, 9000);
            }
          }, 2000);
        },
        error: function(error) {
          if (error) {
            functions.bd_openPopup(verify_failed);
          }
        }
      });
    }
  });
});
