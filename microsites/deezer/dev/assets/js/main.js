$(function() {

    $(".scroll-to").click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });

    $('.tabs-menu a').click(function (event) {
        event.preventDefault();
        if (!$(this).parent().hasClass('current') && !$(this).parent().hasClass('isVariant')) {
            toggleTabs();
        }

        $(this).parent().addClass('current');
        $(this).parent().siblings().removeClass('current');
        var tab = $(this).attr('href'),
        parent = $(this).closest('ul').parent().parent();
        parent.find('.tab-content').not(tab).css('display', 'none');
        $(tab).fadeIn();
    });

    var toggleTabs = function () {
        $('.tab__variant').toggleClass('selected');
    };

    $('.popup').fancybox({
        padding: 11,
        margin: 0,
        closeBtn: false,
        helpers : {
            overlay : {
                css : {
                    'background' : 'rgba(0, 0, 0, 0.7)'
                }
            }
        }
    });

    $('.p-close, .link-close').on('click', function(e) {
        e.preventDefault();
        $.fancybox.close();
    });

    $('.t-1').qtip({
        content: {
            text: 'Každý mesiac počas celej doby viazanosti<br />(12 mesiacov) ušetríte 3 €.<br />Ušetrená suma spolu je tak 36 €.'
        },
        position: {
            my: 'bottom center',
            at: 'top center'
        }
    });

    /* PRICING BOX */

    var $select = $('#deezer-select'),
    $order = $('#order');

    $order.on('click', function(e) {
        e.preventDefault();
    });

    function disableOrder() {
        $($order).addClass('btn_disabled');
        $($order).removeClass('popup');
    }

    function enableOrder() {
        $($order).removeClass('btn_disabled');
        $($order).addClass('popup');
    }

    function setHref(val) {
        $($order).attr('href', '#deezer' + val);
    }

    $($select).change(function(){
        var id = $(this).val();

        if (id == 0) {
            disableOrder();
        } else {
            enableOrder();
            setHref(id);
        }
    });
});
