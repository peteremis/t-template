//on tarif select change price
$(document).on('change', '#filter-tariff-input', function () {
    //set timer
    var bf_timer = setInterval(function () {
        if ($('#ajaxLoadingSpinnerOverlay').is(":hidden")) {
            clearInterval(bf_timer);
            generateText();
        }
    }, 200);
});

function generateText() {
    var selectedTarif = $('#filter-tariff-input').val();
    switch (selectedTarif) {
        case 'ANO_M_DATA':
            appendBF_rdr();
            break;
        case 'ANO_S':
            appendBF_rdr();
            break;
        case 'ANO_M':
            appendBF_rdr();
            break;
        default:
            return;
    }
}

//BF text
function appendBF_rdr() {
    if ($('.blackFriday_rdr').length === 0) {
        $('.catalogue-items-container.pageHolder').append('<div class="blackFriday_rdr text-center"><h3>Testujem či tu može byť nejaka <a onClick="">vyjebana veta.</a></h3></div>');
    }
}


