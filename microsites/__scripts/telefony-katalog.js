var bf_list = {
    "Samsung Galaxy Note9": {},
    "Samsung Galaxy J5 (2017)": {}
};
  
changePrice();

$(document).on("click", ".filter-top .filter-toggle", checkLoad);
$(document).on("click", ".filter-top .filter-close", checkLoad);
$(document).on("change", ".filter select", checkLoad);
$(document).on("click", ".filter-body input", checkLoad);
$(document).on("slidechange", ".filter-body .ui-slider", checkLoad);
$(document).on("click", ".paging li a", checkLoad);
$(document).on("change", ".pagination #pages", checkLoad);

function checkLoad() {
    if (window.location.href.indexOf("?black-friday-2018") == -1) {
        return;
    }

    var bf_timer = setInterval(function () {
        if ($("#ajaxLoadingSpinnerOverlay").is(":hidden")) {
            clearInterval(bf_timer);
            changePrice();
        }
    }, 200);
}

function changePrice() {
    if (window.location.href.indexOf("?black-friday-2018") == -1) {
        return;
    }

    $(".filter-top .filter-category li a").on("click", checkLoad);
    $(".products li").each(function () {
        var comp = $(this);
        var name = comp
            .find("h3 a.scenarioNavigation span")
            .text()
            .trim();

        $.each(bf_list, function (key, value) {
            if (name == key) {
                comp
                    .find(".prices span")
                    .eq(0)
                    .text("---");
            }
        });
    });
}