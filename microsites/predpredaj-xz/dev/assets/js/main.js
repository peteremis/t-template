$(document).ready(function() {

      $('.gallery__holder').slick({
          infinite: false,
          slidesToShow: 3,
          slidesToScroll: 1,
          infinite: false,
          dots: false,
          responsive: [{
              breakpoint: 700,
              settings: {
                  slidesToShow: 1,
                  slidesToScroll: 1,
                  infinite: true
              }
          }]
      });
});
