$( document ).ready(function() {

    /* TABS */
    $(".tabs-menu a").click(function(event) {
        event.preventDefault();
        $(this).parent().addClass("current");
        $(this).parent().siblings().removeClass("current");
        var tab = $(this).attr("href"), // select clicked tab. In example "#tab-1" ..
            parent = $(this).closest('ul').parent().parent(); // <tabs-container>
        parent.find('.tab-content').not(tab).css("display", "none"); //hide all not clicked tabs
        $(tab).fadeIn(); //show clicked tab


    });
    /* TABS END */

    var dataSegment = $("[data-segment]").each(function(){

      closestHead = $($(this).find('.tabs-menu a'));
      closestItemCount = closestHead.length - 1; //vytiahni pocet tabov pre kazdu <section> s atributom "data-segment"

      closestContent = $($(this).find('.tab-content')); //najdi tab-content v danej <section>

      closestAccordion = $(this).find('.accordion'); //najdi .accordion v danej <section>

      accordionItem = '<div class="item">';

      for (var i = 0; i <= closestItemCount; i++) {
          accordionItem += '<div class="heading">' + $(closestHead[i]).text()  + '</div>';
          accordionItem += '<div class="content">' + $(closestContent[i]).html() + '</div></div>';

          if (i !== closestItemCount) {
              accordionItem += '<div class="item">';
          }
      }

      //if data-segment and data-accordion value match, show accordion data
      if($(this).attr('data-segment') === closestAccordion.attr('data-accordion')) {
           $(accordionItem).appendTo(closestAccordion);
      }

      var $items = $('.accordion .item');
      //SET OPENED ITEM
      $($items[0]).addClass('open');

    });

    /* ACCORDION */
    $('.accordion .item .heading').click(function(e) {

        var clickedHead = e.target;
        var $item = $(clickedHead).closest('.item');
        var isOpen = $item.hasClass('open');
        var $content = $item.find('.content');
        var $acPrice = $(clickedHead).find('.ac-price');

        if (isOpen) {
            $content.slideUp(200);
            $item.removeClass('open');
            $acPrice.show();
        } else {
            $content.slideDown(200);
            $item.addClass('open');
            $acPrice.hide();
        }
    });


    $('.accordion .item.open').find('.content').slideDown(200);
    /* ACCORDION END */

    $(".scroll-to").click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top - ($("#nav-sticky").outerHeight() - 10)
                }, 1000);
                return false;
            }
        }
    });

    // $('.info-qm').qtip({
    //     content: {
    //         text: 'Ponuku môžete využiť aj k Magio televízii či k Pevnej linke M, L alebo XL. Prejdite k nákupu, kde si vyberiete ľubovoľnú službu.'
    //     }
    // });

    // *** Calculate prices
    var pricesAll = {
        l: ['15,90','- 5,00','10,00','69,00'],
        xl: ['20,90','- 5,00','10,00','1,00']
    }


    $('#select_program').change(function(){
        addPrices($(this).val());
    });

    addPrices('l');

    function addPrices(program) {
        var prices = pricesAll[program];
    
        $('.pricing-table .feePrice').each(function(key){
            $(this).text(prices[key] + ' €');
        });
    }

    // *** Calculate prices - END
});
