var christmasItem = $(".element-item");
var $grid = $(".grid");

var sortingsWrapper = $(".sortingWrapper");
var heapBoxElement = $("#select_program_rp"); // heapbox div id
var currentFilter = $("#filters .button.is-checked")
  .attr("data-filter")
  .substr(1);

var data = [];

var dataURL = "";
if (window.location.hostname === "localhost") {
  dataURL = "../assets/js/data.json";

  /*====================
   SET THIS INIT VALUES
  ====================*/
  var rateplanValues =
    '[{"value":"anoS","text":"paušálom ÁNO S"},{"value":"anoM","text":"paušálom ÁNO M"},{"value":"anoMdata","text":"paušálom ÁNO M DÁTA"},{"value":"anoL","text":"paušálom ÁNO L"},{"value":"anoXL","text":"paušálom ÁNO XL"},{"value":"anoXXL","text":"paušálom ÁNO XXL", "selected": true}]';
  var magioValues =
    '[{"value":"magioM","text":"Magio internetom alebo TV M"},{"value":"magioL","text":"Magio internetom alebo TV L"},{"value":"magioXL","text":"Magio internetom alebo TV XL", "selected": true}]';

  var initFilter = "magio"; // SET default init filter category in LIFERAY javascript field
  var initRateplanValues = magioValues; // SET default ratenplan values in LIFERAY javascript field
} else {
  dataURL = "/documents/10179/16511938/data.json?v=20";
}

$(document).ready(function() {
  // init Isotope
  $grid.isotope({
    filter: "." + initFilter,
    layoutMode: "masonry",
    masonry: {
      columnWidth: ".grid-sizer",
      gutter: ".gutter-sizer"
    },
    itemSelector: ".element-item",
    getSortData: {
      name: ".name",
      availability: "[data-availability]",
      price: ".pricenum parseInt", //set prices manually for each <span class="pricenum">
      category: "[data-category]"
    }
  });

  // filter functions
  var filterFns = {
    // // show if name ends with -tv
    // ium: function() {
    //   var name = $(this)
    //     .find(".name")
    //     .text();
    //   return name.match(/tv$/);
    // }
  };

  // filter button click
  $("#filters").on("click", "a", function() {
    var $clickedBtn = $(this);
    highlightClickedBtn("#filters.button-group", $clickedBtn);

    var filterValue = $(this).attr("data-filter");
    // use filterFn if matches value
    filterValue = filterFns[filterValue] || filterValue;
    $grid.isotope({ filter: filterValue });

    changeHeapBoxOptions(
      heapBoxElement,
      filterValue === ".rateplan" ? rateplanValues : magioValues,
      filterValue === ".easy"
    );

    // change width dynamically by data-width attribute,
    if (filterValue === ".rateplan") {
      changeItemWidth(false);
    } else {
      changeItemWidth(false);
    }

    // scroll back to top
    $("html,body").animate(
      {
        scrollTop: $("#sortingOptionsSection-sticky-wrapper").offset().top - 102
      },
      "slow"
    );
  });

  // bind sort button click
  $("#sorts").on("click", "a", function() {
    var $clickedBtn = $(this);
    highlightClickedBtn("#sorts.button-group", $clickedBtn);
    var sortByValue = $(this).attr("data-sort-by");
    $grid.isotope({ sortBy: sortByValue });
  });

  //Stick navigations filters
  $("#sortingOptionsSection").sticky({
    topSpacing: 0,
    widthFromWrapper: true,
    bottomSpacing: endpointPosition()
  });

  //init function
  init();
});

function init() {
  changeItemWidth(false); //change item width on init
  fetchData(); // initial data fetch
  showMainHeadline(initFilter);
  redirectToDetail(); //redirect to product detail page or to scenario
}

// data fetch
function fetchData() {
  $.ajax({
    url: dataURL,
    type: "GET",
    async: true,
    crossDomain: true,
    dataType: "json"
  })
    .done(function(itemsData) {
      data = itemsData;
      changeHeapBoxOptions(heapBoxElement, initRateplanValues, false);
      filterByURLparam();
      $("#ajaxLoadingSpinnerOverlay").hide();
    })
    .fail(function(error) {
      console.log(error);
      console.log("error fetching data");
    });
}

// headline for FIX or for PAUSALY
function showMainHeadline(filter) {
  if (filter === "magio") {
    $(".akcieHeadline-sub.fix").removeClass("hidden");
  } else {
    $(".akcieHeadline-sub.pausaly").removeClass("hidden");
  }
}

// get url param by param name
function getUrlParameter(sParam) {
  var sPageURL = window.location.search.substring(1),
    sURLVariables = sPageURL.split("&"),
    sParameterName,
    i;
  for (i = 0; i < sURLVariables.length; i++) {
    sParameterName = sURLVariables[i].split("=");
    if (sParameterName[0] === sParam) {
      return sParameterName[1] === undefined
        ? true
        : decodeURIComponent(sParameterName[1]);
    }
  }
}

// handle .is-checked class on buttons
function highlightClickedBtn(btnGroupID, clickedBtn) {
  $(btnGroupID).each(function(i, buttonGroup) {
    var $buttonGroup = $(buttonGroup);
    $buttonGroup.on("click", "a", function() {});
    $buttonGroup.find(".is-checked").removeClass("is-checked");
    clickedBtn.addClass("is-checked");
  });
}

/* filter items based on url param
 * example: ?category=rateplan&selectedrp=anoS
 * example: ?category=magio&selectedrp=magioXL
 */
function filterByURLparam() {
  if (getUrlParameter("category") !== undefined) {
    var category = getUrlParameter("category");
    var selectedRatePlan = getUrlParameter("selectedrp");

    var $highlightBtn = $("#filters a[data-filter*=" + category + "]");
    highlightClickedBtn("#filters.button-group", $highlightBtn);

    changeHeapBoxOptions(
      heapBoxElement,
      category === "rateplan" ? rateplanValues : magioValues,
      category === "easy"
    );

    switch (category) {
      case "rateplan":
        $grid.isotope({ filter: "." + category });
        $('.heapOption [rel="' + selectedRatePlan + '"]').click();
        break;
      case "magio":
        $grid.isotope({ filter: "." + category });
        $('.heapOption [rel="' + selectedRatePlan + '"]').click();
        break;
      case "magenta1":
        $grid.isotope({ filter: "." + category });
        $('.heapOption [rel="' + selectedRatePlan + '"]').click();
        break;
      case "easy":
        $grid.isotope({ filter: "." + category });
        $('.heapOption [rel="' + selectedRatePlan + '"]').click();
        break;
      default:
        $grid.isotope({ filter: ".rateplan" });
        break;
    }
  } else {
    //else use default init filter
    var $highlightBtn = $("#filters a[data-filter*=" + initFilter + "]");
    highlightClickedBtn("#filters.button-group", $highlightBtn);
  }
}

// change item width dynamically
function changeItemWidth(change) {
  for (i = 0; i < christmasItem.length; i++) {
    var elementitemWidth = christmasItem.eq(i).attr("data-changeWidth");
    if (change === true) {
      christmasItem.eq(i).addClass(elementitemWidth);
      $grid.isotope("layout"); //recalculate layout
    } else {
      christmasItem.eq(i).removeClass(elementitemWidth);
      $grid.isotope("layout"); //recalculate layout
    }
  }
}

// reinvoke sorting update
function updateSort() {
  $grid
    .isotope("updateSortData")
    .isotope({ sortBy: $("#sorts a.is-checked").attr("data-sort-by") }); //update data and resort
}

// on item click, redirect user to device-detail page
// on button "KUPIT" click, redirect user to scenario
function redirectToDetail() {
  $(".element-item, .element-item .link, .element-item .btn").on(
    "click",
    function(e) {
      e.stopPropagation();
      e.preventDefault();
      var selectedRatePlan = $(
        "#changePlan .heapBox .heapOption a[class='selected']"
      ).attr("rel");

      var selectedFilter = $("#filters .button.is-checked")
        .attr("data-filter")
        .substr(1);

      function generateParametersForCatalogue(item) {
        //add catalogue parameters for - ANO scenario
        if (
          selectedRatePlan !== undefined &&
          selectedRatePlan.indexOf("ano") != -1
        ) {
          var rtplForCatalogue = selectedRatePlan.replace("ano", "");
          if (rtplForCatalogue === "Mdata") {
            rtplForCatalogue = "M_data";
          }
          var pausalCatalogueParams =
            "&vyberrp=" +
            rtplForCatalogue +
            "&zariadenie=" +
            item.mobileCatalogueParam;
          var addParams = true;
        } else if (
          //add catalogue parameters for - FIX scenario
          selectedRatePlan !== undefined &&
          selectedRatePlan.indexOf("magio") != -1
        ) {
          var rtplForCatalogue = selectedRatePlan.replace("magio", "");
          var fixCatalogueParams =
            "&internet=" +
            rtplForCatalogue +
            "&televizia=" +
            rtplForCatalogue +
            "&zariadenie=" +
            item.fixCatalogueParam;
          var addParams = true;
        } else {
          var addParams = false;
        }

        if (addParams !== false) {
          return selectedFilter == "rateplan"
            ? pausalCatalogueParams
            : fixCatalogueParams;
        }
      }

      var buyBTN = $(this).hasClass("btn"); // check if btn "KUPIT" was clicked
      var isSoldOut = $(this)
        .closest(".element-item")
        .hasClass("soldOut");
      var isEasy = $(this)
        .closest(".element-item")
        .hasClass("easy");
      var clickedElementItem = $(this);
      var clickedItem = $(this)
        .closest(".element-item")
        .attr("id");

      data.forEach(function(item) {
        if (clickedItem === item.id && isEasy && !isSoldOut) {
          var setRedirectURL = buyBTN ? item.buyBtnURL : item.detailPageURL;
          if (clickedElementItem.hasClass("chri_popup")) {
            return;
          } else {
            if (setRedirectURL) {
              window.open(setRedirectURL, "_self");
            }
          }
        }

        if (clickedItem === item.id && !isEasy && !isSoldOut) {
          var setRedirectURL = buyBTN ? item.buyBtnURL : item.detailPageURL;
          if (clickedElementItem.hasClass("chri_popup")) {
            return;
          } else {
            if (setRedirectURL) {
              window.open(
                setRedirectURL +
                  "?category=" +
                  selectedFilter +
                  "&selectedrp=" +
                  selectedRatePlan +
                  generateParametersForCatalogue(item),
                "_self"
              );
            }
          }
        }
      });
    }
  );
}

/*
 * recalc prices after rateplan change
 * + change eyecatcher on rateplan change
 * + hide products if rpAvailability = false
 */
function recalculatePrices(selectedRatePlan, selectedRatePlanName) {
  for (let index = 0; index < christmasItem.length; index++) {
    let Item = christmasItem[index];
    let ItemID = $(Item).attr("id");

    data.forEach(function(itemData) {
      if (ItemID === itemData.id && itemData.prices[selectedRatePlan]) {
        if (itemData.prices[selectedRatePlan].rpAvailability) {
          //check if item is in current selected filter category
          var isItemInCurrentFilter = $(Item).hasClass(currentFilter);
          if (isItemInCurrentFilter) {
            $(Item).show();
          }

          if (
            itemData.prices[selectedRatePlan].monthlyWeb !== null &&
            itemData.prices[selectedRatePlan].monthlyWeb !== undefined
          ) {
            $(Item)
              .find(".monthlyWeb span")
              .text(itemData.prices[selectedRatePlan].monthlyWeb.toFixed(2));
          }

          if (
            itemData.prices[selectedRatePlan].monthlyStore !== null &&
            itemData.prices[selectedRatePlan].monthlyStore !== undefined
          ) {
            $(Item)
              .find(".monthlyStore span")
              .text(itemData.prices[selectedRatePlan].monthlyStore.toFixed(2));
            $(Item)
              .find(".monthlyStore")
              .css("visibility", "visible");
          } else {
            $(Item)
              .find(".monthlyStore")
              .css("visibility", "hidden");
          }

          if (
            itemData.prices[selectedRatePlan].oneTimeWeb !== null &&
            itemData.prices[selectedRatePlan].oneTimeWeb !== undefined
          ) {
            $(Item)
              .find(".oneTimeWeb span")
              .text(itemData.prices[selectedRatePlan].oneTimeWeb.toFixed(2));
          }

          if (
            itemData.prices[selectedRatePlan].oneTimeStore !== null &&
            itemData.prices[selectedRatePlan].oneTimeStore !== undefined
          ) {
            $(Item)
              .find(".oneTimeStore span")
              .text(itemData.prices[selectedRatePlan].oneTimeStore.toFixed(2));
            $(Item)
              .find(".oneTimeStore")
              .css("visibility", "visible");
          } else {
            $(Item)
              .find(".oneTimeStore")
              .css("visibility", "hidden");
          }

          $(Item)
            .find(".ec")
            .attr("src", itemData.prices[selectedRatePlan].eyecatcherURL);
          $(Item)
            .find(".prices-rtpl")
            .text(selectedRatePlanName);
        } else {
          $(Item).hide();
        }
      }
    });
  }
}

// get .grid endpoint position
function endpointPosition() {
  return $(window).width() <= 768
    ? $(document).height() - $(".fakeEndPoint").offset().top + 1910
    : $(document).height() - $(".fakeEndPoint").offset().top + 1020;
}

/*
 * heapBox functions
 */
function changeHeapBoxOptions(heapBoxElement, valuesToLoad, isEasy) {
  heapBoxElement.heapbox({ insert: "inside" });
  heapBoxElement.heapbox("set", valuesToLoad);

  //reset prices on category change
  var selectedRatePlan = $(
    "#changePlan .heapBox .heapOption a[class='selected']"
  );
  selectedRatePlan.click();

  //hide heapbox for easy
  if (isEasy) {
    sortingsWrapper.hide();
  } else {
    sortingsWrapper.show();
  }
}

//recalculate and resort on heapBox rateplan change
heapBoxElement.heapbox({
  onChange: function(val, elm) {
    var selectedRatePlan = val;
    var selectedRatePlanName = $(
      "#heapbox_select_program_rp.heapBox .heapOption a[class='selected']"
    ).text();
    recalculatePrices(selectedRatePlan, selectedRatePlanName);
    updateSort();
  }
});

//popup
$(".chri_popup").on("click", function() {
  var clickedBtn = $(this).attr("href");
  $.fancybox([
    {
      href: clickedBtn,
      padding: 0,
      margin: 30,
      closeBtn: false,
      width: 550,
      height: "auto",
      // autoDimensions: false,
      autoSize: false,
      parent: "#content",
      helpers: {
        overlay: {
          css: {
            background: "rgba(0, 0, 0, 0.7)"
          }
        }
      }
    }
  ]);
});

$(".p-close, .link-close, .text-close, .close-btn").on("click", function(e) {
  e.preventDefault();
  $.fancybox.close();
});

/* TOGGLE ARROW */
$(".toggle-arrow, .arrow-right").click(function(event) {
  event.preventDefault();
  if ($(this).hasClass("arrow-right")) {
    return;
  }

  if (
    $(event.target).hasClass("toggle-arrow main") ||
    $(event.target).hasClass("arrow-right main")
  ) {
    $(this)
      .closest(".togleArrow.main")
      .find(".arrow-content.main")
      .toggle(200);
    $(this)
      .find(".arrow-right.main")
      .toggleClass("arrow-rotate");
  } else {
    $(this)
      .closest(".togleArrow")
      .find(".arrow-content")
      .toggle(200);
    $(this)
      .find(".arrow-right")
      .toggleClass("arrow-rotate");
  }
});
/* TOGGLE ARROW END */
