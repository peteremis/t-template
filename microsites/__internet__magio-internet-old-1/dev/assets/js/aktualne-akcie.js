// *** Calculate prices - START
var pricesAll = {
  m: [
    ["10,90", "- 5,00", "8,00", "159,00"],
    ["10,90", "- 5,00", "8,00", "199,00"],
    ["10,90", "- 5,00", "1,00", "39,00"]
  ],
  l: [
    ["15,90", "- 5,00", "8,00", "99,00"],
    ["15,90", "- 5,00", "8,00", "139,00"],
    ["15,90", "- 5,00", "1,00", "19,00"]
  ],
  xl: [
    ["20,89", "- 5,00", "8,00", "39,00"],
    ["20,89", "- 5,00", "8,00", "79,00"],
    ["20,89", "- 5,00", "1,00", "1,00"]
  ]
};

addPrices("xl");

function addPrices(program) {
  var prices = pricesAll[program];
  $("#tab-1 .tab-acc .tab-acc-sub").each(function(key) {
    $(this)
      .find(".tab-ex-label .tab-ex-label-program")
      .text(program.toUpperCase());
    $(this)
      .find(".tab-ex-price")
      .each(function(key2) {
        $(this).text(prices[key][key2] + " €");
      });
  });

  $(".accordion .tab-acc .tab-acc-sub").each(function(key) {
    $(this)
      .find(".tab-ex-label .tab-ex-label-program")
      .text(program.toUpperCase());
    if (prices[key] !== undefined) {
      $(this)
        .find(".tab-ex-price")
        .each(function(key2) {
          $(this).text(prices[key][key2] + " €");
        });
    }
  });
}
// *** Calculate prices - END

/* TABS */
$(".tabs-menu a").click(function(event) {
  event.preventDefault();
  $(this)
    .parent()
    .addClass("current");
  $(this)
    .parent()
    .siblings()
    .removeClass("current");
  var tab = $(this).attr("href"), // select clicked tab. In example "#tab-1" ..
    parent = $(this)
      .closest("ul")
      .parent()
      .parent(); // <tabs-container>
  parent
    .find(".tab-content")
    .not(tab)
    .css("display", "none"); //hide all not clicked tabs
  $(tab).fadeIn(); //show clicked tab
});
$("#tab-1 .tab-help-desc a").click(function(event) {
  event.preventDefault();
  var menu = $(".tabs-menu");
  menu.find(".variant.current").removeClass("current");
  menu.find(".variant:eq(1)").addClass("current");
  $("#tab-1").hide();
  $("#tab-3").fadeIn();
});
/* TABS END */

/* FROM TABS TO ACCORDION */
var dataSegment = $("[data-segment]").each(function() {
  //console.log($(this).attr('data-segment')); //zobraz "data-segment" hodnotu pre kazdu <section>

  closestHead = $($(this).find(".tabs-menu a"));
  closestItemCount = closestHead.length - 1; //vytiahni pocet tabov pre kazdu <section> s atributom "data-segment"
  //console.log(closestItemCount + "heads");

  closestContent = $($(this).find(".tab-content")); //najdi tab-content v danej <section>
  //console.log(closestContent);

  closestAccordion = $(this).find(".accordion"); //najdi .accordion v danej <section>

  accordionItem = '<div class="item">';

  for (var i = 0; i <= closestItemCount; i++) {
    accordionItem +=
      '<div class="heading">' + $(closestHead[i]).text() + "</div>";
    accordionItem +=
      '<div class="content">' + $(closestContent[i]).html() + "</div></div>";

    if (i !== closestItemCount) {
      accordionItem += '<div class="item">';
    }
  }

  //if data-segment and data-accordion value match, show accordion data
  if (
    $(this).attr("data-segment") === closestAccordion.attr("data-accordion")
  ) {
    $(accordionItem).appendTo(closestAccordion);
  }

  var $items = $(".accordion .item");
  //SET OPENED ITEM
  $($items[0]).addClass("open");
});
$(".accordion #select_program").attr("id", "select_program2");
$(".select_program").heapbox({
  onChange: function(val, elm) {
    addPrices($(elm).val());
  }
});

/* FROM TABS TO ACCORDION END */

/* ACCORDION */

$(".accordion .item .heading").click(function(e) {
  var clickedHead = e.target;
  var $item = $(clickedHead).closest(".item");
  var isOpen = $item.hasClass("open");
  var $content = $item.find(".content");
  var $acPrice = $(clickedHead).find(".ac-price");

  if (isOpen) {
    $content.slideUp(200);
    $item.removeClass("open");
    $acPrice.show();
  } else {
    $content.slideDown(200);
    $item.addClass("open");
    $acPrice.hide();
  }
});

$(".accordion .item.open")
  .find(".content")
  .slideDown(200);
/* ACCORDION END */

/* DIV DATA-HREF */
$(document).on("click", "#content .tab-acc-sub", function() {
  var href = $(this).attr("data-href");

  if (typeof href != "undefined") {
    window.location.href = "/" + href;
  }
});
