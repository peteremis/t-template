$(function () {
    $("#nav-sticky").sticky({
        topSpacing: 0,
        widthFromWrapper: true
    });

    $(".scroll-to").click(function () {
        if (
            location.pathname.replace(/^\//, "") ==
            this.pathname.replace(/^\//, "") &&
            location.hostname == this.hostname
        ) {
            var target = $(this.hash);
            target = target.length ? target : $("[name=" + this.hash.slice(1) + "]");
            if (target.length) {
                $("html,body").animate({
                        scrollTop: target.offset().top - ($("#nav-sticky").outerHeight() - 10)
                    },
                    1000
                );
                return false;
            }
        }
    });

    var contentSections = $("[id*='sec-']"),
        secondaryNav = $("#nav-sticky");

    function updateSecondaryNavigation() {
        contentSections.each(function () {
            var actual = $(this),
                actualHeight =
                actual.height() +
                parseInt(actual.css("paddingTop").replace("px", "")) +
                parseInt(actual.css("paddingBottom").replace("px", "")),
                actualAnchor = secondaryNav.find(
                    'a[href="#' + actual.attr("id") + '"]'
                );

            if (
                actual.offset().top - secondaryNav.outerHeight() <=
                $(window).scrollTop() &&
                actual.offset().top + actualHeight - secondaryNav.outerHeight() >
                $(window).scrollTop()
            ) {
                actualAnchor.addClass("active");
            } else {
                actualAnchor.removeClass("active");
            }
        });
    }

    updateSecondaryNavigation();

    $(window).scroll(function (event) {
        updateSecondaryNavigation();
    });

    $(".fancy").fancybox({});

    $(".tooltip").click(function (event) {
        event.preventDefault();
    });

    var targets = $("[rel~=tooltip]"),
        target = false,
        tooltip = false,
        title = false;

    targets.bind("mouseenter", function () {
        target = $(this);
        tip = target.attr("title");
        tooltip = $('<div id="tooltip"></div>');

        if (!tip || tip == "") return false;

        target.removeAttr("title");
        tooltip
            .css("opacity", 0)
            .html(tip)
            .appendTo("body");

        var init_tooltip = function () {
            if ($(window).width() < tooltip.outerWidth() * 1.5)
                tooltip.css("max-width", $(window).width() / 2);
            else tooltip.css("max-width", 340);

            var pos_left =
                target.offset().left +
                target.outerWidth() / 2 -
                tooltip.outerWidth() / 2,
                pos_top = target.offset().top - tooltip.outerHeight() - 20;

            if (pos_left < 0) {
                pos_left = target.offset().left + target.outerWidth() / 2 - 20;
                tooltip.addClass("left");
            } else tooltip.removeClass("left");

            if (pos_left + tooltip.outerWidth() > $(window).width()) {
                pos_left =
                    target.offset().left -
                    tooltip.outerWidth() +
                    target.outerWidth() / 2 +
                    20;
                tooltip.addClass("right");
            } else tooltip.removeClass("right");

            if (pos_top < 0) {
                var pos_top = target.offset().top + target.outerHeight();
                tooltip.addClass("top");
            } else tooltip.removeClass("top");

            tooltip
                .css({
                    left: pos_left,
                    top: pos_top
                })
                .animate({
                        top: "+=10",
                        opacity: 1
                    },
                    50
                );
        };

        init_tooltip();
        $(window).resize(init_tooltip);

        var remove_tooltip = function () {
            tooltip.animate({
                    top: "-=10",
                    opacity: 0
                },
                50,
                function () {
                    $(this).remove();
                }
            );

            target.attr("title", tip);
        };

        target.bind("mouseleave", remove_tooltip);
        tooltip.bind("click", remove_tooltip);
    });

    $(".popup").fancybox({
        padding: 10,
        margin: 0,
        parent: "#content",
        closeBtn: false,
        helpers: {
            overlay: {
                css: {
                    background: "rgba(0, 0, 0, 0.7)"
                }
            }
        }
    });

    $(".close-end a").click(function () {
        $.fancybox.close();
    });

    /* SHOW LEGAL */
    $("#show_legal").click(function () {
        $(this).hide();
        $("#legal_more").show(200);
    });

    /* SEC-AKCIE LOAD CONTENT */

    // $(".sec-akcie").load("templates/_aktualne-akcie.html", function () {});

    /* TOOLTIPS */
    $(".tooltip0").qtip({
        content: "Internet cez optickú sieť je závislý od dostupnosti vo vašej lokalite. Inak je Magio internet dostupný cez technológiu VDSL, prípadne ADSL.",
        style: {
            "font-size": 12
        },
        position: {
            corner: {
                target: "leftMiddle",
                tooltip: "topMiddle"
            }
        }
    });

    $(".tooltip1").qtip({
        content: "Rýchlosť internetu je závislá od technológie, ktorá je dostupná na adrese zriadenia služby.<br>Technológie, ktoré poskytujeme:<br>Optika: 30/3 Mb/s<br>Optika k domu - VDSL: 10/1 Mb/s<br>ADSL: 4/0,5 Mb/s",
        style: {
            "font-size": 12
        },
        position: {
            corner: {
                target: "leftMiddle",
                tooltip: "topMiddle"
            }
        }
    });

    $(".tooltip3").qtip({
        content: "Rýchlosť internetu je závislá od technológie, ktorá je dostupná na adrese zriadenia služby.<br>Technológie, ktoré poskytujeme:<br>Optika: 120/12 Mb/s<br>Optika k domu - VDSL: 40/4 Mb/s<br>ADSL: 8/1 Mb/s",
        style: {
            "font-size": 12
        },
        position: {
            corner: {
                target: "leftMiddle",
                tooltip: "topMiddle"
            }
        }
    });

    $(".tooltip4").qtip({
        content: "Rýchlosť internetu je závislá od technológie, ktorá je dostupná na adrese zriadenia služby.<br>Technológie, ktoré poskytujeme:<br>Optika: 600/60 Mb/s<br>Optika k domu - VDSL: 90/9 Mb/s<br>ADSL: 15/1 Mb/s",
        style: {
            "font-size": 12
        },
        position: {
            corner: {
                target: "leftMiddle",
                tooltip: "topMiddle"
            }
        }
    });
    $(".tooltip2").qtip({
        content: "Rýchlosť internetu je závislá od technológie, ktorá je dostupná na adrese zriadenia služby. <br>Technológie, ktoré poskytujeme:<br>Optika GPON: 30/3 Mbit/s<br>Optika Active Ethernet: 15/1 Mbit/s<br>Optika k domu - VDSL: 10/1 Mbit/s<br>ADSL: 4/0,5 Mbit/s",
        style: {
            "font-size": 12
        },
        position: {
            corner: {
                target: "leftMiddle",
                tooltip: "topMiddle"
            }
        }
    });
    $(".tooltip-l").qtip({
        content: "Rýchlosť internetu je závislá od technológie, ktorá je dostupná na adrese zriadenia služby. <br>Technológie, ktoré poskytujeme:<br>Optika GPON: 250/25 Mbit/s<br>Optika Active Ethernet: 60/6 Mbit/s<br>Optika k domu - VDSL: 40/4 Mbit/s<br>ADSL: 8/1 Mbit/s",
        style: {
            "font-size": 12
        },
        position: {
            corner: {
                target: "leftMiddle",
                tooltip: "topMiddle"
            }
        }
    });
    $(".tooltip-xl").qtip({
        content: "Rýchlosť internetu je závislá od technológie, ktorá je dostupná na adrese zriadenia služby. <br>Technológie, ktoré poskytujeme:<br>Optika GPON: 600/60 Mbit/s<br>Optika Active Ethernet: 100/10 Mbit/s<br>Optika k domu - VDSL: 90/9 Mbit/s<br>ADSL: 15/1 Mbit/s",
        style: {
            "font-size": 12
        },
        position: {
            corner: {
                target: "leftMiddle",
                tooltip: "topMiddle"
            }
        }
    });
    $("#naj_znacka_m").qtip({
        content: "Vaša dôvera je u nás na prvom mieste.<br>Magio internet a televízia sú víťazmi Ankety o najdôveryhodnejšiu značku v oblasti poskytovateľov internetových a televíznych služieb za rok 2018, ktorú uskutočnila nezávislá agentúra Nielsen.",
        style: {
            "text-align": "center"
        },
        //position: {
        //    corner: {
        //        target: 'rightMiddle',
        //        tooltip: 'topMiddle'
        //   }
        //}
        position: {
            my: "left top",
            at: "right center"
        }
    });
});

$(document).ready(function () {
    /* ::: DataDevice ::: */
    var $dataDeviceSlider = $('.data-device-slider');
    var $dataDevicePagin = $('.data-device-pagin');

    /* ::: TAB CONFIG ::: */
    $('ul.tabs li a').on('click', function (e) {
        var currentAttrValue = $(this).attr('href');
        $('.tab ' + currentAttrValue).show().siblings().hide();
        $(this).parent('li').addClass('current').siblings().removeClass('current');
        e.preventDefault();
    });

    /* ::: GET CURRENT SLIDE NUM ::: */
    $dataDeviceSlider.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
        //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
        var i = (currentSlide ? currentSlide : 0) + 1;
        $dataDevicePagin.text('(' + i + '/' + slick.slideCount + ')');
    });


    /* ::: PAGINATION INDICATOR ::: */
    if ($(window).width() < 769) {
        $dataDeviceSlider.on('init', function (event, slick) {
            if ($(window).width() < 769) {
                $('.data-device-slider .slick-dots li').css("width", 100 / $('.dataDevice__item').length + '%');
            }
        });
    };

    $("#slider_internet").slick({
        slidesToShow: 3,
        slidesToScroll: 3,
        centerMode: false,
        infinite: false,
        swipe: true,
        arrows: true,
        dots: true,
        autoplay: false,
        initialSlide: 0,
        autoplaySpeed: 5000,
        touchMove: false,
        draggable: false,

        responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
                arrows: true,
                dots: true,
            }
        }, {
            breakpoint: 768,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: true,
                dots: true,
            }
        }]
    });

});

//popup
$(".chri_popup").on("click", function() {
    var clickedBtn = $(this).attr("href");
    $.fancybox([
        {
            href: clickedBtn,
            padding: 0,
            margin: 30,
            closeBtn: false,
            width: 550,
            height: "auto",
            // autoDimensions: false,
            autoSize: false,
            parent: "#content",
            helpers: {
                overlay: {
                    css: {
                        background: "rgba(0, 0, 0, 0.7)"
                    }
                }
            }
        }
    ]);
});

$(".p-close, .link-close, .text-close, .close-btn").on("click", function(e) {
    e.preventDefault();
    $.fancybox.close();
});