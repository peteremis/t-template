/* bundles */
function getBundle($id) {
	for (var i = 0; i < data.bundles.length; i++) {
		if (data.bundles[i].id == $id) {
			return data.bundles[i];
		}
	}
	return null;
}

/* update */
function updateCalcSum() {
	// standard fee
	$('#calc-sum-standard').html(formatValue(Number($('#mobile-products-standard-sum').html()) + Number($('#solo-price-sum').html()), '', '', '0', 2));
	// discount
	$('#calc-discount').html(formatValue(Number($('#mobile-products-discount').html()) + Number($('#bundle-discount').html()), '', '', '0', 2));
	// discounted price
	$('#calc-sum').html(formatValue(Number($('#mobile-products-sum').html()) + Number($('#bundle-price-sum').html()), '', '', '0', 2));
}

function updateFixedIndicator() {
	var currentValue;
	var bundleValue;

	currentValue = Number($('input[name="current-value"]').val());
	bundleValue = Number($('#bundle-price-sum').html());
	currentValue5pct = Number(currentValue * 0.05);

	$('.calc-fixed-indicator').hide();
	if (bundleValue > currentValue + currentValue5pct) {
	    // up-sell
	    $('#calc-fixed-indicator-green').show();
	} else if (bundleValue > currentValue - currentValue5pct) {
	   // neutral
	   $('#calc-fixed-indicator-yellow').show();
	} else {
	   // budget
	   $('#calc-fixed-indicator-red').show();
	}
}

function updateFixedCalc() {
	var $id = generateValueString();
	var $bundle = getBundle($id);
	if ($bundle) {
		// up-sell
		$('#tv-1-upsell').html(formatUpsellValue($bundle["tv-1-upsell"]));
		$('#tv-2-upsell').html(formatUpsellValue($bundle["tv-2-upsell"]));
		$('#tv-3-upsell').html(formatUpsellValue($bundle["tv-3-upsell"]));
		$('#net-1-upsell').html(formatUpsellValue($bundle["net-1-upsell"]));
		$('#net-2-upsell').html(formatUpsellValue($bundle["net-2-upsell"]));
		$('#net-3-upsell').html(formatUpsellValue($bundle["net-3-upsell"]));
		$('#phone-1-upsell').html(formatUpsellValue($bundle["phone-1-upsell"]));
		$('#phone-2-upsell').html(formatUpsellValue($bundle["phone-2-upsell"]));
		$('#phone-3-upsell').html(formatUpsellValue($bundle["phone-3-upsell"]));
		// sums
		$('#solo-price-sum').html($bundle["solo-price-discounted"]);
		$('#bundle-discount').html(formatValue($bundle["bundle-discount-sum"], '', '', '0'));
		$('#bundle-price-sum').html($bundle["bundle-price-sum"]);
		// debug
		$('#bundle-id').html($id);
	}
	else
		null;
	updateCalcSum();
	updateFixedIndicator();
}

function getBundleValue($id, $value) {
	var $bundle = getBundle($id);
	if ($bundle[$value])
		return $bundle[$value];
	else
		return null;
}

function getPrice($kind, $value) {
	var $id = generateValueString($kind, $value);
	var $bundle = getBundle($id);
	if ($bundle)
		return $bundle[$kind + '-solo-price'] - $bundle[$kind + '-bundle-discount'];
	else
		return null;
}

/* formatting */
function formatValue($value, $prefix, $suffix, $default, $round) {
	if(typeof($default)==='undefined') $default = '';
	if(typeof($prefix)==='undefined') $prefix = '';
	if(typeof($suffix)==='undefined') $suffix = '';
	if(typeof($round)==='undefined') $round = 0;
	if ($value == null || $value == '' || $value == 'free' || $value == '0')
		return $default;
	else {
		return $prefix + Number($value).toFixed($round) + $suffix;
	}
}

function formatUpsellValue($value) {
	return formatValue($value, '+', '');
}

function formatPrice($value) {
	return $value;
}

function formatMobileRateplanValue($value) {
	return $value.toFixed(2);
}

/* helpers */
function generateValueString($kind, $value) {
	var $id = getProductValue('net') + getProductValue('tv') + getProductValue('phone');
	if ($kind) $id = updateValueString($id, $kind, $value);
	return $id;
}

function updateValueString($string, $kind, $value) {
	switch($kind) {
	case 'net':
		return $value + $string.charAt(1) + $string.charAt(2);
	  break;
	case 'tv':
		return $string.charAt(0) + $value + $string.charAt(2);
	  break;
	case 'phone':
		return $string.charAt(0) + $string.charAt(1) + $value;
	  break;
	default:
	  return $string;
	}
}

function getProductValue($product) {
	var $value = $('.' + $product + ':checked').attr('value');
	if (isNaN($value))
		return '4';
	else
		return $value;
}

/* calculator - mobile */
function updateMobileRateplan($id) {
	// rateplan fees
	$count = getMobileRateplanCount($id);
	$rateplan = getMobileRateplan($id);
	// standard fee
	if ($count && $rateplan) {
		$('#mobile-product-price-std-' + $id).html(formatValue($count * $rateplan["fee-standard"], '', '', '', 2));
	}
	else if ($rateplan) {
		$('#mobile-product-price-std-' + $id).html(formatValue($rateplan["fee-standard"], '', '', '', 2));
	}
	else {
		$('#mobile-product-price-std-' + $id).html('');
	}
	// discount and price
	if ($count) {
		if ($rateplan) {
			$multiplier = getBundleMultiplier();
			$('#mobile-product-price-' + $id).html(formatValue($count * ($rateplan["fee-standard"] - ($rateplan["discount"] * $multiplier)), '', '', '', 2));
			$('#mobile-product-discount-' + $id).html(formatValue($rateplan["discount"] * $multiplier * $count, '', '', '0', 2));
		}
	}
	else {
		$('#mobile-product-price-' + $id).html('');
		$('#mobile-product-discount-' + $id).html('');
	}
}

function getBundleMultiplier() {
	return $('input[name="calc-mobile-bundle"]:checked').val();
}

function updateMobileCalc() {
	for (var i = 0; i < data.rateplans.length; i++) {
		updateMobileRateplan(data.rateplans[i].id);
	}
	// sum standard fees
  $('#mobile-products-standard-sum').html(formatValue(getRateplanStandardFee(), '', '', '0', 2));
	// sum fees
  $('#mobile-products-sum').html(formatValue(getRateplanSum('price'), '', '', '0', 2));
  // sum discounts
  $('#mobile-products-discount').html(formatValue(getRateplanSum('discount'), '', '', '0', 2));
  updateCalcSum();
}

function getMobileRateplan($id) {
	for (var i = 0; i < data.rateplans.length; i++) {
		if (data.rateplans[i].id == $id) {
			return data.rateplans[i];
		}
	}
	return null;
}

function getMobileRateplanCount($id) {
	return $('#mobile-product-' + $id).val();
}

function getRateplanSum($kind) {
	var sum = 0;
	var amount;
	$('.calc-mobile-product-' + $kind).each(function() {
		amount = $(this).html();
		if (amount) {
  		sum += Number(amount);
  	}
  });
  return sum;
}

function getRateplanStandardFee() {
	var sum = 0;
	var amount;
	var count = 0;
	$('.calc-mobile-product-price-std').each(function() {
		count = getMobileRateplanCount($(this).attr('product-id'));
		amount = $(this).html();
		if (count > 0 && amount) {
  		sum += Number(amount);
  	}
  });
  return sum;
}