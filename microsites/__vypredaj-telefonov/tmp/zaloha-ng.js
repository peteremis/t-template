  self.showDetailFilter = function() {

                ModalService.showModal({
                    templateUrl: "../assets/app/modal.detail-filter.html",
                    controller: "modalDetailCtrl",
                    inputs: {
                        zoznamBrandov: self.zoznamBrandov 
                    }
                }).then(function(modal) {
                    modal.close.then(function(result) {
                        console.log( 'result deatail-filter',result);
                    });
                });
            };

            self.showSortFilter = function() {

                ModalService.showModal({
                    templateUrl: "../assets/app/modal.filter.html",
                    controller: "modalFilterCtrl",
                    inputs: {
                        zakladnyFilter: self.zakladnyFilter
                    }
                }).then(function(modal) {
                    modal.close.then(function(result) {
                        self.changeOrder(result.selected);
                    });
                });
            };
app.controller('modalFilterCtrl', ['$scope', 'close', 'zakladnyFilter', function($scope, close, zakladnyFilter) {
    $scope.selected = null;
    $scope.zakladnyFilter = zakladnyFilter;
    console.log('filter', $scope.zakladnyFilter);
    $scope.changeOrder = function(item) {
        $scope.selected = item.value;
        // console.log(item);
        $scope.close();
    };

    $scope.close = function() {
        close({
            selected: $scope.selected
        }, 100); // close, but give 500ms for bootstrap to animate
    };

}]);

app.controller('modalDetailCtrl', ['$scope', 'close', 'zoznamBrandov', function($scope, close, zoznamBrandov) {
    $scope.selected = null;
    $scope.zoznamBrandov = zoznamBrandov;
    console.log('modal zoznamBrandov', $scope.zoznamBrandov);
    $scope.changeOrder = function(itemValue) {
        $scope.selected = itemValue;
        $scope.close();
    };

    $scope.close = function() {
        close({
            selected: $scope.zoznamBrandov
        }, 100); // close, but give 500ms for bootstrap to animate
    };

}]);
