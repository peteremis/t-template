'use strict';

angular.
module('deviceList').
component('deviceList', {
    templateUrl: '../assets/app/device-list.template.html',
    controller: ['PhoneService', 'deviceService', '$filter', '$resource', 'ModalService', '$location', '$timeout',
        function PhoneListController(PhoneService, deviceService, $filter, $resource, ModalService, $location, $timeout) {
            var self = this;

            //vybrate brandy
            self.deviceFilterBrand = [];
            self.deviceFilterScreen = [];
            self.zoznamBrandov = [{ name: "", maker: "", ticked: false }];
            self.screenSize = {};
            self.zakladnyFilter = [
                { name: "Abecedy: od A po Z", maker: "", ticked: false, value: "nameAZ" },
                { name: "Abecedy: od Z po A", maker: "", ticked: false, value: "nameZA" },
                { name: "Obľúbenosti", maker: "", ticked: false, value: "order" },
                { name: "Ceny: od najlacnejšieho", maker: "", ticked: false, value: "lowest" },
                { name: "Ceny: od najdrahšieho", maker: "", ticked: false, value: "highest" }
            ];
            //keep show all on first/zero position
            self.zoznamScreenov = [
                { name: "Všetky", maker: "", hideTag: true, ticked: true, min: 0, max: 20 },
                { name: "3,9\" a menej", maker: "", hideTag: false, ticked: false, min: 0, max: 3.9 },
                { name: "4\" až 4,4\"", maker: "", hideTag: false, ticked: false, min: 4, max: 4.4 },
                { name: "4,5\" až 4,9\"", maker: "", hideTag: false, ticked: false, min: 4.5, max: 4.9 },
                { name: "5\" až 5,4\"", maker: "", hideTag: false, ticked: false, min: 5, max: 5.4 },
                { name: "5,5\" a viac", maker: "", hideTag: false, ticked: false, min: 5.5, max: 10 }
            ];
            self.outputBrands = [];
            self.outputScreenSize = [];
            self.outputBaseFilter = [];
            self.orderProp = 'order';
            self.reverse = false;
            // helper for result count
            self.filterResult = [];

            self.devices = PhoneService.query();
            // console.log('phone', self.devices);

            self.tabletAll = [];
            deviceService.getAllTablets().then(function(device) {
                self.tabletAll = device;
                // console.log('tablety', self.tabletAll);
                $timeout(function() {
                    self.scrollTo();
                });
            });

            // set first filter
            self.selected = self.zakladnyFilter[0];
            self.defaultFilter = self.zakladnyFilter[0];
            //scrollTo from url hash
            self.scrollTo = function() {
                var $target = $('#' + $location.$$hash);
                //console.log($target.length);
                if ($target.length > 0) {
                    var scrollPos = $target.offset().top;
                    $("body,html").animate({ scrollTop: scrollPos }, "slow");
                }
            };
            //scrol to phone-list section
            self.scrollToTag = function(tagId) {
                var $target = $(tagId);
                var scrollPos = $target.offset().top;
                $("body,html").animate({ scrollTop: scrollPos }, "slow");
            };

            self.urlBrand = '';

            self.init = function() {
                self.changeOrder(self.defaultFilter);
                self.resetScreeSize(0, 10);
                
                self.urlBrand = $location.absUrl().split("/").pop().split('#')[1];
                
                if (!angular.isUndefined(self.urlBrand)) {
                    //self.urlBrand = self.urlBrand.toUpperCase();
                    self.deviceFilterBrand.push(self.urlBrand);
                }
            };

            self.filterUnavailable = function(device) {
                // console.log(device);
                return device.available == true;
            };

            // change device list order basic filter
            self.changeOrder = function(item, mobileCheck) {

                if (mobileCheck === 'mobile') { self.uncheckOther(item); }

                switch (item.value) {
                    case 'nameAZ':
                        self.orderProp = 'name';
                        self.reverse = false;
                        break;
                    case 'nameZA':
                        self.orderProp = 'name';
                        self.reverse = true;
                        break;
                    case 'lowest':
                        self.orderProp = 'price.easy';
                        self.reverse = false;
                        break;
                    case 'highest':
                        self.orderProp = 'price.easy';
                        self.reverse = true;
                        break;
                    case 'order':
                        self.orderProp = 'age';
                        self.reverse = false;
                        break;
                }
            };

            self.uncheckOther = function(item) {
                //console.log('item', item);
                var i;
                // first, set everything to false
                for (i = 0; i < self.zakladnyFilter.length; i++) {
                    self.zakladnyFilter[i].ticked = false;
                }
                // then set the clicked item to true
                item.ticked = true;
                //console.log('zak filter', self.zakladnyFilter);
            };

            //create list arr for dropdown
            self.createObjForList = function(list, listName) {
                switch (list) {
                    case 'brand':
                        self.zoznamBrandov.push({
                            name: listName,
                            value: listName,
                            ticked: false
                        });
                        break;
                    case 'screenSize':
                        self.zoznamScreenov.push({
                            name: listName + "\"",
                            value: listName,
                            ticked: false
                        });
                        break;
                }
            };

            //filter all mobile brands/screens
            PhoneService.query().$promise.then(function(items) {
                var output = [];
                var keys = [];
                //clean list
                self.zoznamBrandov = [];
                
                angular.forEach(items, function(item) {
                    var key = item["brand"];
                    if (keys.indexOf(key) === -1) {
                        keys.push(key);
                        output.push(item["brand"]);
                        
                        if (!angular.isUndefined(self.urlBrand) && self.urlBrand == key) {
                            self.zoznamBrandov.push({
                                name: self.urlBrand,
                                value: self.urlBrand,
                                ticked: true
                            });                
                        } else {
                            self.createObjForList("brand", key);
                        }
                    }
                });
                //self.allBrands = output;
                //console.log('all brands list:', self.allBrands);
                // sort arr by AZ
                self.changeSortOrder(self.zoznamBrandov, 'AZ');
                //console.log('zoznam brands:', self.zoznamBrandov);
            });

            // filter for Brand DropDown & CheckBox
            self.checkBrandChange = function(item) {
                var i = $.inArray(item.name, self.deviceFilterBrand);
                if (i > -1) {
                    self.deviceFilterBrand.splice(i, 1);
                    item.ticked = false;
                } else {
                    self.deviceFilterBrand.push(item.name);
                }
                //console.log('filtrovat', self.deviceFilterBrand);
            };
            // Filter parameter 
            self.brandFilter = function(device) {

                if (self.deviceFilterBrand.length > 0) {
                    if ($.inArray(device.brand, self.deviceFilterBrand) < 0)
                        return;
                }
                return device;
            }

            // filter for ScreenSize DropDown & CheckBox
            self.checkScreenChange = function(item) {
                    self.screenSize = {
                        min: item.min,
                        max: item.max
                    };
                    //console.log('filtrovat', item);
                }
                // untick and reset screensize filter
            self.untick = function untick($index, item) {
                    //console.log('tick', item, "\n", $index);
                    self.resetScreeSize(0, 10);
                    item.ticked = false;
                }
                // set screenSize lrange
            self.resetScreeSize = function(min, max) {
                self.screenSize = {
                    min: parseInt(min),
                    max: parseInt(max)
                };
            }

            self.changeSortOrder = function(dataToSort, sortType) {
                if (sortType == "AZ") {
                    dataToSort.sort(function(a, b) {
                        if (a.value < b.value) return -1;
                        if (a.value > b.value) return 1;
                        return 0;
                    });
                }
            }

            //show hide mobile filter
            self.IsHiddenTab1 = true;
            self.IsHiddenTab2 = true;
            self.ShowHideTab1 = function(final) {
                //If DIV is hidden it will be visible and vice versa.
                self.IsHiddenTab1 = self.IsHiddenTab1 ? false : true;
                
                if (!self.IsHiddenTab2) {
                    self.IsHiddenTab2 = true;
                };
                if (final) {
                    self.scrollToTag('#phone-list');
                }
            };
            self.ShowHideTab2 = function(final) {
                //If DIV is hidden it will be visible and vice versa.
                self.IsHiddenTab2 = self.IsHiddenTab2 ? false : true;
                if (!self.IsHiddenTab1) {
                    self.IsHiddenTab1 = true;
                }
                if (final) {
                    self.scrollToTag('#phone-list');
                }
            };

            //divide checkboxes to columns
            self.getLimits = function(array) {
                var arr = [
                    Math.floor(array.length / 2) + 1, -Math.floor(array.length / 2)
                ];
                return arr
            };
            self.getLimit2 = function(array) {
                var arr = [
                    Math.floor(array.length / 2), -Math.floor(array.length / 2)
                ];
                return arr
            };

            //uncheck other checkboxes
            self.updateSelection = function(screenSizeName, itemsArr) {
                self.count = 0;
                angular.forEach(itemsArr, function(screenSize, index) {
                    if (screenSizeName != screenSize.name) {
                        screenSize.ticked = false;
                    };
                    if (screenSize.ticked == false) {
                        self.count = self.count + 1;
                    }
                });
                //if all unchecked check showall
                if (self.count == 6) {
                    // item[0] is show all
                    itemsArr[0].ticked = true;
                    self.checkScreenChange(itemsArr[0]);
                }
            };

            //jQuery fix
            //close modal
            self.pclose = function(data) {
                $.fancybox.close();
                //console.log(':::popup close:::');
            };
            //qTip
            self.qtip = function() {
                $('.info').qtip({
                    content: 'Bezplatné volania získate na 3 čísla, ktoré si sami zvolíte. Čísla môžu byť v akejkoľvek mobilnej sieti v SR.'
                });
            };

            // runs once per controller instantiation
            self.init();

        }
    ]
});
