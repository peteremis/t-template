$(function() {
    $('.popup').fancybox({
        padding: 11,
        margin: 0,
        closeBtn: false,
        parent: "#content",
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.7)'
                }
            }
        }
    });

  
    
    $('.p-close').on('click', function(e) {
        e.preventDefault();
        $.fancybox.close();
    });
});
 