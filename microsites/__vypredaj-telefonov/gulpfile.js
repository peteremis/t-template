var gulp = require('gulp'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    uglify = require('gulp-uglify'),
    browserSync = require('browser-sync').create(),
    reload = browserSync.reload,
    includer = require('gulp-html-ssi'),
    runSequence = require('run-sequence'),
    es = require('event-stream'),
    replace = require('gulp-replace'),
    config = require('./config.json'),
    globalTemplates = '../../global-assets/templates/*.html',
    localTemplates = ['dev/templates/_main-content.html', 'dev/templates/index.html'],
    tabletTemplates = ['dev/templates/_main-content-tablety.html', 'dev/templates/index-tablety.html'],
    jasmineBrowser = require('gulp-jasmine-browser'),
    watch = require('gulp-watch'),
    jasmine = require('gulp-jasmine-livereload-task');
var print = require('gulp-print');
var gutil = require('gulp-util');
var rename = require('gulp-rename');
var cleanCSS = require('gulp-clean-css');
//var git = require('gulp-git');
var argv = require('yargs').argv;
var runSequence = require('run-sequence');
var prompt = require('gulp-prompt');
var myVar = 'update';

//Base tasks html + css + js
gulp.task('htmlSSI', function() {
    es.merge(gulp.src(globalTemplates), gulp.src(localTemplates))
        .pipe(includer())
        .pipe(gulp.dest('dev/server/'));
});
gulp.task('htmlSSI-tab', function() {
    es.merge(gulp.src(globalTemplates), gulp.src(tabletTemplates))
        .pipe(includer())
        .pipe(rename("index.html"))
        .pipe(gulp.dest('dev/server-tablety/'));
});


/* ::: SASS ::: */
gulp.task('sass', function() {
    return gulp.src('dev/assets/sass/style.scss')
        .pipe(sass({ outputStyle: 'compact' }))
        .pipe(sourcemaps.init())
        .pipe(sass.sync().on('error', sass.logError))
        .pipe(sourcemaps.write('/maps'))
        .pipe(gulp.dest('dev/server/assets/css'))
        .pipe(browserSync.stream());
});
gulp.task('sass-tab', function() {
    return gulp.src('dev/assets/sass/style-tablety.scss')
        .pipe(sass({ outputStyle: 'compact' }))
        .pipe(sourcemaps.init())
        .pipe(sass.sync().on('error', sass.logError))
        .pipe(sourcemaps.write('/maps'))
        .pipe(rename("style.css"))
        .pipe(gulp.dest('dev/server-tablety/assets/css'))
        .pipe(browserSync.stream());
});


/* ::: SASS DEPLOY ::: */
gulp.task('sass-deploy', function() {
    return gulp.src('dev/assets/sass/content.scss')
        .pipe(sass({ outputStyle: 'compact' }))
        .pipe(sass.sync().on('error', sass.logError))
        .pipe(gulp.dest('dev/server/assets/css'))
        .pipe(browserSync.stream());
});


/* ::: JS ::: */
gulp.task('js', function() {
    return gulp.src('dev/assets/js/*.js')
        .on('error', console.error.bind(console))
        .pipe(gulp.dest('dev/server/assets/js'));
});



//Deploy helpers
/* ::: DIST-REPLACE-PATH-HTML ::: */
gulp.task('dist-replace-path-html', function() {
    gulp.src(['dev/templates/_main-content.html'])
        .pipe(replace('../assets/img/', config.imgFolder))
        .pipe(replace('../assets/js/', config.codeFolder))
        .pipe(replace('../assets/app/', config.appFolder))
        .pipe(replace('../assets/data/', config.dataFolder))
        .pipe(gulp.dest('deploy'));
});


/* ::: DIST-REPLACE-PATH-CSS ::: */
gulp.task('dist-replace-path-css', function() {
    gulp.src(['dev/server/assets/css/*.css'])
        .pipe(replace('../img/', config.imgFolder))
        .pipe(gulp.dest('deploy'));
});


/* ::: DIST-REPLACE-PATH-CSS-DEPLOY ::: */
gulp.task('dist-replace-path-css-deploy', function() {
    gulp.src(['dev/server/assets/css/content.css'])
        .pipe(replace('../img/', config.imgFolder))
        .pipe(gulp.dest('deploy'));
});



/* ::: JS REPLACE PATH ::: */
gulp.task('dist-replace-path-js', function() {
    gulp.src(['dev/server/assets/js/*.js'])
        .pipe(replace('../img/', config.imgFolder))
        .pipe(gulp.dest('deploy'));
});


/* ::: APP REPLACE PATH ::: */
gulp.task('dist-replace-path-data', function() {
    gulp.src(['dev/server/assets/data/*.js'])
        .pipe(replace('../img/', config.imgFolder))
        .pipe(replace('../assets/data/', config.dataFolder))
        .pipe(gulp.dest('deploy-app/data'));
});
gulp.task('dist-replace-path-app', function() {
    gulp.src(['dev/assets/app/*.js'])
        .pipe(replace('../img/', config.imgFolder))
        .pipe(replace('../assets/data/', config.dataFolder))
        .pipe(replace('../assets/app/', config.appFolder))
        .pipe(gulp.dest('deploy-app/'));
    gulp.src(['dev/assets/app/*.html'])
        .pipe(replace('../assets/img/', config.imgFolder))
        .pipe(replace('../assets/js/', config.codeFolder))
        .pipe(replace('../assets/app/', config.appFolder))
        .pipe(replace('../assets/data/', config.dataFolder))
        .pipe(gulp.dest('deploy-app/'));
});

/* ::: PREPARE DEPLOY ::: */
gulp.task('prepare-deploy', ['sass', 'js', 'htmlSSI'], function() {
    return
    gulp.src('dev/templates/_main-content.html')
        .pipe(gulp.dest('deploy'));
    gulp.src('dev/assets/img/**')
        .pipe(gulp.dest('deploy/img'));
    gulp.src('dev/server/assets/js/main.js')
        .pipe(gulp.dest('deploy'));
    gulp.src('dev/server/assets/css/content.css')
        .pipe(gulp.dest('deploy'));
});

/* ::: DIST REPLACE ::: */
gulp.task('dist-replace', ['dist-replace-path-html', 'dist-replace-path-js', 'dist-replace-path-css', 'dist-replace-path-app', 'dist-replace-path-data']);
gulp.task('dist-replace-deploy', ['dist-replace-path-html', 'dist-replace-path-js', 'dist-replace-path-css', 'dist-replace-path-app', 'dist-replace-path-data']);


/* :::DEPLOY  TASKY::: */
//Deploy
gulp.task('deploy', function(callback) {
    runSequence(
        'sass-deploy',
        'prepare-deploy',
        'dist-replace-deploy',
        'print',
        callback);
});

/* :::SERVER  TASKY::: */
//Serve
gulp.task('serve', function() {
    browserSync.init({
        server: {
            baseDir: ["./dev", "./dev/server", "./../../global-assets"]
        }
    });

    gulp.watch('dev/assets/sass/*.scss', ['sass']);
    gulp.watch('../../global-assets/sass/*.scss', ['sass']);
    gulp.watch('dev/assets/js/*.js', ['js']);
    gulp.watch('dev/assets/js/*.js').on('change', reload);
    gulp.watch('dev/templates/*.html', ['htmlSSI']);
    gulp.watch('dev/server/*.html').on('change', reload);
    gulp.src('dev/assets/img/**')
        .pipe(gulp.dest('dev/server/assets/img'));
});

// DEFAUL Task
gulp.task('default', function(callback) {
    runSequence(['sass', 'js', 'htmlSSI'],
        'serve',
        callback);
});

/* ::: HELPERS ::: */

//Print CSS path
gulp.task('print', function() {
    gulp.src('dev/server/assets/css/style.css')
        .pipe(print(function(filepath) {
            return "\n@import url(" + config.globalCSS + "global-core.css);\n@import url(" + config.codeFolder + "content.css?v=1);";
        }))
});

//minify Css
gulp.task('minify:css', function() {
    return gulp.src('deploy/content.css')
        .pipe(cleanCSS())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest('deploy'));
});

//deploy to dist convert and back
//convert deploy 2 dev html
gulp.task('conv', function() {
    gulp.src(['convert/_input.html'])
        .pipe(replace(config.imgFolder, '../assets/img/'))
        .pipe(replace(config.codeFolder, '../assets/js/'))
        .pipe(gulp.dest('convert/dev/'));
});
//convert dev 2 deploy html
gulp.task('conv-back', function() {
    gulp.src(['convert/dev-src/_input.html'])
        .pipe(replace('../assets/img/', config.imgFolder))
        .pipe(replace('../assets/js/', config.codeFolder))
        .pipe(gulp.dest('convert/deploy/'));
});