$(function() {
  //append dynamic form inside section
  $("#form-holder").append($("#p_p_id_dynamicForm_WAR_eccadmin_"));

  // video player
  $(".video-container").click(function() {
    $(".video-embed").css({ "opacity": "1", "display": "block" });
    $(".video-embed")[0].src += "&autoplay=1";
    $(".play_btn").css({ "opacity": "0", "display": "none" });
    $(this).unbind("click");
  });
  $(".play_btn").click(function() {
    $(".video-embed").css({ "opacity": "1", "display": "block" });
    $(".video-embed")[0].src += "&autoplay=1";
    $(".play_btn").css({ "opacity": "0", "display": "none" });
    $(this).unbind("click");
  });

  /* STICKY NAV */

  $("#nav-sticky").sticky({
    topSpacing: 0,
    widthFromWrapper: true
  });

  $(".scroll-to").click(function() {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top - ($("#nav-sticky").outerHeight() - 2)
        }, 1000);
        return false;
      }
    }
  });

  var contentSections = $("[id*='sec-']"),
    secondaryNav = $("#nav-sticky");

  function updateSecondaryNavigation() {
    contentSections.each(function() {
      var actual = $(this),
        actualHeight = actual.height() + parseInt(actual.css('paddingTop').replace('px', '')) + parseInt(actual.css('paddingBottom').replace('px', '')),
        actualAnchor = secondaryNav.find('a[href="#' + actual.attr('id') + '"]');

      if ((actual.offset().top - secondaryNav.outerHeight() <= $(window).scrollTop()) && (actual.offset().top + actualHeight - secondaryNav.outerHeight() > $(window).scrollTop())) {
        actualAnchor.addClass('active');
      } else {
        actualAnchor.removeClass('active');
      }

    });
  }
  updateSecondaryNavigation();

  $(window).scroll(function(event) {
    updateSecondaryNavigation();
  });

  /* ::: TAB CONFIG ::: */
  $('ul.tabs li a').on('click', function(e) {
    var currentAttrValue = $(this).attr('href');
    $('.tab ' + currentAttrValue).show().siblings().hide();
    $(this).parent('li').addClass('current').siblings().removeClass('current');
    e.preventDefault();
  });
  /* ::: Poznamka Show/Hide ::: */
  $('.show-hide').click(function(){var id = $(this).attr('data-container');$(this).toggleClass('open-down');$('#'+id).slideToggle(500);});

});

