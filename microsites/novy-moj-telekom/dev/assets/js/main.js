 $(function() {
     //zoom feature
     var zoomImage1 = $('#zoom-01'),
         zoomImage2 = $('#zoom-02'),
         zoomImage3 = $('#zoom-03'),
         zoomImage4 = $('#zoom-04'),
         zoomImage5 = $('#zoom-05'),
         zoomImage6 = $('#zoom-06'),
         zoomConfig = {
             responsive: true,
             borderSize: 1,
             borderColour: '#2F2F2F',
             tint: true,
             tintColour: '#FFF',
             tintOpacity: 0.6,
             zoomWindowWidth: 300,
             zoomWindowHeight: 300,
             zoomWindowFadeIn: 350,
            zoomWindowFadeOut: 350
         },
         zoomConfigLeft = {
             responsive: true,
             borderSize: 1,
             borderColour: '#2F2F2F',
             tint: true,
             tintColour: '#FFF',
             tintOpacity: 0.6,
             zoomWindowWidth: 300,
             zoomWindowHeight: 300,
             zoomWindowPosition: 10,
             zoomWindowFadeIn: 350,
            zoomWindowFadeOut: 350
         };

     function adjustZoom(attr, data) {
         if (window.innerWidth <= 860) {
             $('.zoomContainer').remove();
             zoomImage1.removeData('elevateZoom');
             zoomImage2.removeData('elevateZoom');
             zoomImage3.removeData('elevateZoom');
             zoomImage4.removeData('elevateZoom');
             zoomImage5.removeData('elevateZoom');
             zoomImage6.removeData('elevateZoom');
         } else {
             zoomImage1.elevateZoom(zoomConfig);
             zoomImage2.elevateZoom(zoomConfigLeft);
             zoomImage3.elevateZoom(zoomConfig);
             zoomImage4.elevateZoom(zoomConfigLeft);
             zoomImage5.elevateZoom(zoomConfig);
             zoomImage6.elevateZoom(zoomConfigLeft);
         }
     }

     adjustZoom();
     $(window).resize(function(e) {
         adjustZoom();
     });


     //slide DownUp Configuration
     // toggle section row
     $(".toggle-info").click(function(e) {
         e.preventDefault();
         var target = this.getAttribute("href");
         //toggle upDown icon
         $(this).find('.more-less').toggleClass('glyphicon-plus glyphicon-minus');
         if ($(target).hasClass("height-transition-hidden"))
             $(target).slideDownTransition();
         else
             $(target).slideUpTransition();

     });
 });

 // slideDown-Up plugin
 (function($) {

     $.fn.slideUpTransition = function() {
         return this.each(function() {
             var $el = $(this);
             $el.css("max-height", "0");
             $el.addClass("height-transition-hidden");

         });
     };

     $.fn.slideDownTransition = function() {
         return this.each(function() {
             var $el = $(this);
             $el.removeClass("height-transition-hidden");

             // temporarily make visible to get the size
             $el.css("max-height", "none");
             var height = $el.outerHeight();

             // reset to 0 then animate with small delay
             $el.css("max-height", "0");

             setTimeout(function() {
                 $el.css({
                     "max-height": height
                 });
             }, 1);
         });
     };
 })(jQuery);
