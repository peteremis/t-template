$(document).ready(function () {

	var modal = $('#termsModal');
	if (modal.length > 0) {

		modal.modal({
			escapeClose: false,
			clickClose: false,
			showClose: false
		});

		var accordion = $('.terms-accordion');

		$('#terms-header').on('click', function (e) {
			console.log('click');
			e.preventDefault();
			$('#ta-0').slideToggle();
		});


		$('.chwrap').on('click', function () {
			$(this).find('input').click()
		});

		accordion.find('input[type=checkbox]').on('click', function (e) {
			e.stopPropagation()
		});

		$('#termsDisagree').on('click', function () {
			window.location = '/'
		});

		$('#termsAccept').on('click', function (e) {
			e.preventDefault();
			var isOk = true;
			accordion.find('input').each(function () {
				var self = $(this);
				var wrap = $(this).parent();
				if (!self.prop('checked')) {
					wrap.addClass('error');
					isOk = false
				} else {
					wrap.removeClass('error')
				}
			});

			if (isOk) {
				$.ajax({
					url: 'http://backvm.telekom.sk/static/microsites/moj-telekom-popup-terms/dev/server/popup.php',
					type: 'GET',
					dataType: 'json',
					success: function () {
						$.modal.close();
					},
					error: function (xhr) {
						console.log(xhr);
					}
				});
			} else {
				modal.find('.terms-modal-wrapper').scrollTop(0);
			}
		})

	}

});
