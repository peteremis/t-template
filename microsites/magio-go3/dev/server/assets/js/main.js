$(function() {

    $(".lightbox").fancybox();

    $('.tooltip-1').qtip({
        content: {
            text: 'Magio GO Archív zadarmo<br/>získajú klienti, ktorí majú<br/>Magio televíziu a k nej<br/>aktivovaný archív.'
        },
        position: {
            my: 'bottom center',
            at: 'top center'
        }
    });

    $('.tooltip-2').qtip({
        content: {
            text: 'V Archíve Magio GO sú tieto<br/>TV stanice: Jednotka, Dvojka, <br/>Markíza, TV JOJ, Doma, Plus,<br/>Dajto, TA3, ČT1, ČT2,'
        },
        position: {
            my: 'bottom center',
            at: 'top center'
        }
    });

    $('.tooltip-3').qtip({
        content: {
            text: 'Šesť stupňov kvality:<br/>supernízka, nízka, stredná,<br/>vysoká, super vysoká a auto.'
        },
        position: {
            my: 'bottom center',
            at: 'top center'
        }
    });

    $('.tooltip-4').qtip({
        content: {
            text: 'Šesť stupňov kvality:<br/>supernízka, nízka, stredná,<br/>vysoká, super vysoká a auto.'
        },
        position: {
            my: 'bottom center',
            at: 'top center'
        }
    });

    $(".tabs-menu a").click(function(event) {
        event.preventDefault();
        if (!$(this).parent().hasClass("current") && !$(this).parent().hasClass("isVariant")) {
            toggleTabs();
        }

        $(this).parent().addClass("current");
        $(this).parent().siblings().removeClass("current");
        var tab = $(this).attr("href"),
            parent = $(this).closest('ul').parent().parent();
        parent.find('.tab-content').not(tab).css("display", "none");
        $(tab).fadeIn();

    });

    var toggleTabs = function() {
        $('.tab__variant').toggleClass('selected');
    };

    var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));

    elems.forEach(function(html) {
        var switchery = new Switchery(html, { color: '#6bb324', secondaryColor: '#ededed' });
    });

    $('.switchery').parent().closest('span').addClass('hide-span');


    $("#nav-sticky").sticky({
        topSpacing: 0,
        widthFromWrapper: true
    });

    $(".scroll-to").click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top - ($("#nav-sticky").outerHeight())
                }, 1000);
                return false;
            }
        }
    });

    var contentSections = $("[id*='sec-']"),
        secondaryNav = $("#nav-sticky");

    function updateSecondaryNavigation() {
        contentSections.each(function() {
            var actual = $(this),
                actualHeight = actual.height() + parseInt(actual.css('paddingTop').replace('px', '')) + parseInt(actual.css('paddingBottom').replace('px', '')),
                actualAnchor = secondaryNav.find('a[href="#' + actual.attr('id') + '"]');

            if ((actual.offset().top - secondaryNav.outerHeight() <= $(window).scrollTop()) && (actual.offset().top + actualHeight - secondaryNav.outerHeight() > $(window).scrollTop())) {
                actualAnchor.addClass('active');
            } else {
                actualAnchor.removeClass('active');
            }

        });
    }
    updateSecondaryNavigation();

    $(window).scroll(function(event) {
        updateSecondaryNavigation();
    });


});
