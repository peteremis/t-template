$(function() {
  $(document).on("click", 'a[href^="#"]', function(event) {
    event.preventDefault();

    $("html, body").animate(
      {
        scrollTop: $($.attr(this, "href")).offset().top
      },
      1000
    );
  });

  // PERSONALIZED VIDEO - START
  function getQueryParams(qs) {
    qs = qs.split("+").join(" ");
    var params = {},
      tokens,
      re = /[?&]?([^=]+)=([^&]*)/g;
    while ((tokens = re.exec(qs))) {
      params[decodeURIComponent(tokens[1])] = decodeURIComponent(tokens[2]);
    }
    return params;
  }

  var queryParam = getQueryParams(document.location.search);
  var videoHolder = $("#personalizedPlayer");

  if (queryParam.q !== undefined) {
    var splitted = queryParam.q.split("-");
    var video_url =
      "https://lazarus.blob.core.windows.net/vid/" +
      splitted[0] +
      "/" +
      splitted[1] +
      ".mp4";

    videoHolder.attr(
      "poster",
      "https://lazarus.blob.core.windows.net/vid" +
        splitted[0] +
        "/" +
        splitted[1] +
        ".jpeg"
    );

    let source = $(
      '<video id="vid" class="personalised-video" width="100%" height="auto" controls autoplay muted><source type="video/mp4"></source></video>'
    );

    source.attr("src", video_url);
    videoHolder.children("source").remove();
    videoHolder.append(source);
  }
  function runVideo() {
    setTimeout(function() {
      document.getElementById("vid").play();
    }, 2000);
  }
  runVideo();
  // PERSONALIZED VIDEO - END
});
