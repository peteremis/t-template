$('.popup').fancybox({
    padding: 11,
    margin: 0,
    closeBtn: false,
    helpers : {
        overlay : {
            css : {
                'background' : 'rgba(0, 0, 0, 0.7)'
            }
        }
    }
});

$('.p-close').on('click', function(e) {
    e.preventDefault();
    $.fancybox.close();
});


$(".scroll-to").click(function () {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html,body').animate({
                scrollTop: target.offset().top - ($("#nav-sticky").outerHeight() - 10)
            }, 1000);
            return false;
        }
    }
});
