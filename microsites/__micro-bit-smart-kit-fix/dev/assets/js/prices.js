var pricelist = {
  is_orderable: 1,
  is_visible: 1,
  prices: {
    M: {
      oc: 49.0,
      oc_sale_web: 19.0,
      rc: 2.0,
      rc_sale_web: 2.0,
      rpln: {
        taxless_price: 9.08,
        tax_price: 10.9,
      },
    },
    L: {
      oc: 29.0,
      oc_sale_web: 1.16,
      rc: 2.0,
      rc_sale_web: 1.91,
      rpln: {
        taxless_price: 13.25,
        tax_price: 15.9,
      },
    },
    XL: {
      oc: 1.0,
      oc_sale_web: 19.0,
      rc: 2.0,
      rc_sale_web: 0,
      rpln: {
        taxless_price: 17.41,
        tax_price: 20.9,
      },
    },
  },
};
