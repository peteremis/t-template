$(function() {

  /* HEAPBOX */
  $('#heap').heapbox();

  /* HEAPBOX NO ARROW */
  $('#heap-noArrow').heapbox();

/* FANCYBOX*/
  $(".fancy").fancybox();

  $('.fancy-kred').fancybox({
      maxWidth: 320
  });

  $('.fancybox-media').fancybox({
      openEffect: 'none',
      closeEffect: 'none',
      helpers: {
          media: {}
      }
  });
/* FANCYBOX END*/



/* scroll to + sticky nav */
$("#nav-sticky").sticky({
    topSpacing: 0,
    widthFromWrapper: true
});

$(".scroll-to").click(function () {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html,body').animate({
                scrollTop: target.offset().top - ($("#nav-sticky").outerHeight())
            }, 1000);
            return false;
        }
    }
});

var contentSections = $("[id*='sec-']"),
    secondaryNav = $("#nav-sticky");

function updateSecondaryNavigation() {
    contentSections.each(function () {
        var actual = $(this),
            actualHeight = actual.height() + parseInt(actual.css('paddingTop').replace('px', '')) + parseInt(actual.css('paddingBottom').replace('px', '')),
            actualAnchor = secondaryNav.find('a[href="#' + actual.attr('id') + '"]');

        if ((actual.offset().top - secondaryNav.outerHeight() <= $(window).scrollTop()) && (actual.offset().top + actualHeight - secondaryNav.outerHeight() > $(window).scrollTop())) {
            actualAnchor.addClass('active');
        } else {
            actualAnchor.removeClass('active');
        }

    });
}
updateSecondaryNavigation();

  $(window).scroll(function (event) {
      updateSecondaryNavigation();
  });
/* scroll to + sticky nav END */




/* POPUP */
    $('.popup').fancybox({
        padding: 0,
        margin: 0,
        closeBtn: false,
        parent: "#content",
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.7)'
                }
            }
        }
    });

    $('.p-close, .link-close, .text-close, .close-btn').on('click', function(e) {
        e.preventDefault();
        $.fancybox.close();
    });
/* POPUP END */





/* TABS */
$(".tabs-menu a").click(function(event) {
    event.preventDefault();
    $(this).parent().addClass("current");
    $(this).parent().siblings().removeClass("current");
    var tab = $(this).attr("href"), // select clicked tab. In example "#tab-1" ..
        parent = $(this).closest('ul').parent().parent(); // <tabs-container>
    parent.find('.tab-content').not(tab).css("display", "none"); //hide all not clicked tabs
    $(tab).fadeIn(); //show clicked tab


});
/* TABS END */

var dataSegment = $("[data-segment]").each(function(){
  console.log($(this).attr('data-segment')); //zobraz "data-segment" hodnotu pre kazdu <section>

  closestHead = $($(this).find('.tabs-menu a'));
  closestItemCount = closestHead.length - 1; //vytiahni pocet tabov pre kazdu <section> s atributom "data-segment"
  //console.log(closestItemCount + "heads");

  closestContent = $($(this).find('.tab-content')); //najdi tab-content v danej <section>
  //console.log(closestContent);

  closestAccordion = $(this).find('.accordion'); //najdi .accordion v danej <section>

  accordionItem = '<div class="item">';

  for (var i = 0; i <= closestItemCount; i++) {
      accordionItem += '<div class="heading">' + $(closestHead[i]).text()  + '</div>';
      accordionItem += '<div class="content">' + $(closestContent[i]).html() + '</div></div>';

      if (i !== closestItemCount) {
          accordionItem += '<div class="item">';
      }
  }

  //if data-segment and data-accordion value match, show accordion data
  if($(this).attr('data-segment') === closestAccordion.attr('data-accordion')) {
       $(accordionItem).appendTo(closestAccordion);
  }

  var $items = $('.accordion .item');
  //SET OPENED ITEM
  $($items[0]).addClass('open');

});



/* FROM TABS TO ACCORDION - OLD */
  //  var $accordion = $(".accordion"),
  //  $heads = $('.tabs-menu.default a'),
  //  $contents = $('.tab-content.default'),
  //  itemCount = $($heads).length - 1,
  //  accordionCode = '<div class="item">';
  //
  // loop for defaultAccordion
  //  for (var i = 0; i <= itemCount; i++) {
  //      accordionCode += '<div class="heading">' + $($heads[i]).text()  + '</div>';
  //      accordionCode += '<div class="content">' + $($contents[i]).html() + '</div></div>';
  //
  //      if (i !== itemCount) {
  //          accordionCode += '<div class="item">';
  //      }
  //  }
  //
  //  $(accordionCode).appendTo($accordion);
  //
  //  var $items = $('.accordion .item');
  //  //SET OPENED ITEM
  //  $($items[0]).addClass('open');
/* FROM TABS TO ACCORDION END - OLD */


/* ACCORDION */
$('.accordion .item .heading').click(function(e) {

    var clickedHead = e.target;
    var $item = $(clickedHead).closest('.item');
    var isOpen = $item.hasClass('open');
    var $content = $item.find('.content');
    var $acPrice = $(clickedHead).find('.ac-price');

    if (isOpen) {
        $content.slideUp(200);
        $item.removeClass('open');
        $acPrice.show();
    } else {
        $content.slideDown(200);
        $item.addClass('open');
        $acPrice.hide();
    }
});


$('.accordion .item.open').find('.content').slideDown(200);
/* ACCORDION END */




/* TOOLTIPS */
$('#info-balik-volania, #info-balik-minuty').qtip({
    content: 'Balík aktivujete na webe počas kúpy Happy paušálu.',
    position: {
        corner: {
            target: 'leftMiddle',
            tooltip: 'topMiddle'
        }
    }
    // position: {
    //     my: 'bottom center',
    //     at: 'top center'
    // }
}).bind('click', function(event) {
    event.preventDefault();
    return false;
});
/* TOOLTIPS END */




/* SLIDER SLICK.js */
$("#program_slider").slick({
    slidesToShow: 2,
    slidesToScroll: 1,
    centerMode: false,
    infinite: false,
    swipeToSlide: true,
    swipe: true,
    arrows: true,
    dots: true,
    autoplay: false,
    initialSlide: 0,
    autoplaySpeed: 5000,
    touchMove: false,
    draggable: false,
    // customPaging: function(slider, i) {
    //     return '<span class="slide-dot">&nbsp;</span>';
    // },
    responsive: [{
        breakpoint: 1024,
        settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            arrows: true,
            dots: true
        }
    }, {
        breakpoint: 768,
        settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
            infinite: false,
            dots: true
        }
    }]
});
/* SLIDER SLICK.js END */


/* TOGGLE ARROW */
$('.toggle-arrow, .arrow-right').click(function(event) {
  event.preventDefault();
  if ($(this).hasClass('arrow-right')) {
    return;
  }
  $(event.target).closest('.togleArrow').find('.arrow-content').toggle(200);
  $(this).find('.arrow-right').toggleClass('arrow-rotate');
});
/* TOGGLE ARROW END */


/* RANGE INPUT SLIDER */
  //live update number on page
$('.range-slider').each(function() {
    var compInputRange = $('.range-slider .input-range');
    var range = $(this).find('.input-range'); // range input
    var value = $(this).find('.range-value'); // range display
	// set / display initial starting range value
	var startingVal = range.val();
	value.html(startingVal + '&euro;');
	// live update range value
	range.on('input change', function(){
		roundAmount();
    });
    
    // check browser on initial load
	//remove BG if EDGE or FIREFOX
    checkBrowsers();
    
    compInputRange.on('input', function () {
        var val = ($(this).val() - $(this).attr('min')) / ($(this).attr('max') - $(this).attr('min'));

        $(this).css('background-image', '-webkit-gradient(linear, left top, right top, '
                + 'color-stop(' + val + ', #94A14E), '
                + 'color-stop(' + val + ', #C5C5C5)'
                + ')'
        );
	    // check browser on slider change
	    //remove BG if EDGE or FIREFOX
	    //checkBrowsers();
    });    
 
    // round number based on range amount
	function roundAmount() {
	    value.html(range.val() + '&euro;');
	};
    
    function checkBrowsers () {
        //check if browser = Edge
	    if (/Edge\/\d./i.test(navigator.userAgent)) {
            compInputRange.css('background-image', 'none')
	    }
        //check if browser = MSIE
        if (/MSIE 9/i.test(navigator.userAgent) || /rv:11.0/i.test(navigator.userAgent) || /rv:10.0/i.test(navigator.userAgent)) {
		     // This is internet explorer 9,10 or 11 fix
             compInputRange.css('height', '20px')
	    }
        
        //check if browser = Firefox
        if (/Firefox\/\d./i.test(navigator.userAgent)){
            compInputRange.css({
                'background-image':'transparent',
                'height':'1px'
            });
        }
    }
});
/* RANGE INPUT SLIDER END */

/* RANGE SLIDER 2 options START */
$(document).ready(function () {
    var sk_localization = 'sk-SK';
    function createRangeSlider(appendSliderToEl, headline, sliderID, minValue, maxValue, value1, value2, quotationMarks, localization) {

        if (quotationMarks === undefined || quotationMarks === null) {
            quotationMarks = '';
        }

        var sliderHTML = '<div class="slider_range">'+
            '<div class="'+sliderID+'">'+
                ''+headline+':'+
            '</div>'+
            '<div id="'+sliderID+'" class="slider_range-content '+sliderID+'"></div>'+
            '<div class="slider-priceWrapper clearfix">'+
                '<div class="left-price">'+
                    '0'+
                '</div>'+
                '<div class="right-price">'+
                    '0'+
                '</div>'+
            '</div>'+
        '</div>';

        var appendToElement = $(appendSliderToEl);
        var slider_name = sliderID;
        $(sliderHTML).appendTo(appendToElement);

        function updateSliderValues($slider, ui) {
            var left = $slider.find('span.ui-slider-handle')[0];
            var right = $slider.find('span.ui-slider-handle')[1];
            $slider.next().find($('.left-price')).text(ui.values[0].toLocaleString(localization) + quotationMarks); //update price on left side
            $slider.next().find($('.right-price')).text(ui.values[1].toLocaleString(localization) + quotationMarks) //update price on right side
        };

        $('#' + slider_name).slider({
            range: true,
            min: minValue,
            max: maxValue,
            step: 0.01,
            values: [value1, value2],
            slide: function (event, ui) {
                updateSliderValues($(this), ui);
            },
            create: function (event, ui) {
                $(this).next().find($('.left-price')).text($(this).slider("values", 0).toLocaleString(localization) + quotationMarks);
                $(this).next().find($('.right-price')).text($(this).slider("values", 1).toLocaleString(localization) + quotationMarks);
            }
        });
    }

    createRangeSlider('.sliders_range', 'Cena', 'test-slider' , 0, 50, 3, 27, "", sk_localization);
    createRangeSlider('.sliders_range', 'Uhlopriečka', 'randomSlider', 0, 50, 16, 21, "''", sk_localization); //setLocalization to ['$locale.language']
});
/* RANGE SLIDER 2 options END */

/* SEARCH BAR - X click */
  //delete value on X button click
$('.close-bttn').click(function(event){
        event.preventDefault();
        $('input[id="search-bar"]').val('');
    });
  // delete value on focus and hide magnifying-glass
$('input[id="search-bar"]').focus(function(){
        $(this).val('');
        $('#magnifying-glass').hide();
        $('#search-bar').css('padding-left','10px')
    });
// on focusout, get magnifying-glass back
    $('input[id="search-bar"]').focusout(function(){
            $('#magnifying-glass').show();
            $('#search-bar').css('padding-left','35px')
        });
/* SEARCH BAR - X click END */

/* SWITCHERY */
var elem = document.querySelector('.js-switch');
var init = new Switchery(elem, { color: '#6bb324', secondaryColor: '#ededed' });
$('.switchery').parent().closest('span').addClass('hide-span');
/* SWITCHERY END */

/* SECONDARY RADIO BTNS */
//default magenta color for radio box
var defaultCheckboxColor = $('#secondaryRadio input[name=rb-pair]:checked').attr('id');
$('.secondaryRadio').find('label[for='+  defaultCheckboxColor  +']').css({'background-color':'#e20074',
              'color':'#fff'});

//on click set magenta color to selected radio box and make other radio boxes transparent
$(".secondaryRadio input").click( function(){
    var getAttr = $(this).attr('id');
    
    $(".secondaryRadio label").css({'background-color':'transparent','color':'#000'})
    $('.secondaryRadio').find('label[for='+  getAttr  +']').css({'background-color':'#e20074','color':'#fff'});
   
    //If radiobtn and div IDs match, show div.
    var checkedDiv = $('.detailHolder[id='+getAttr+']');
    //console.log(checkedDiv);
    var divID = $('.detailHolder').attr('id');
    var objLength = $(this).length;
    //console.log(objLength);
    if (getAttr = divID && objLength > 0) {
        //console.log("pravda");
        //console.log(checkedDiv);
        $('.detailHolder').hide();
        $(checkedDiv).show();
    }
});
/* SECONDARY RADIO BTNS END */


/* FILTERED SEARCH select2 plugin */
$("#filter1").select2({
	closeOnSelect:true
  });

//fix for remove automatically created heapBox
$('.select2-container').prev().remove();

$("#filter1").change(function(){
       //alert($(this).val());
       var val = $('#filter1').val();

        $('div#multiselect').each(function(){
          var divAttr = $(this).attr('class');
          //console.log("div:" + divAttr);
          var getValue = val;

         	if (getValue == null) {
              $('div#multiselect').hide();
             } else {
              for (i = 0; i <= getValue.length; i++) {
                //console.log(getValue[i]);
              if (getValue[i] === divAttr) {
                $('div[class='+getValue[i]+']').show();
                //console.log("true");
              } else {
               var show = $('div[class='+getValue[i]+']').show();
               $(show).siblings().hide();
              }
             }
            }
        });
});

$("#filter1")
        .val('M') //je potrebne nastavit pociatocnu hodnotu filtra, napr. pre pausal M, value="M"
        .trigger('change');
/* FILTERED SEARCH select2 plugin END */



/* ANIMATED PROGRESSBAR */

function progress(percent, $element, time) {
    //zisti sirku progressBaru ktoru budeme animovat
    var progressBarWidth = Math.ceil(percent * $element.width() / 100);

    //animuj od 0% az po %hodnotu, ktoru sme si zadali v "data-progress" atribúte
    $({countNum: 0}).animate({countNum: percent}, {
        duration: time, //duration must be equal to progressBar load time
        easing:'linear',
        step: function () {
        // What todo on every count
            var pct = '';
            if(percent == 0){
                pct = Math.floor(this.countNum) + '%';
            }else{
                pct = Math.floor(this.countNum+1) + '%';
            }
            //zobraz text s percentami v progressBare
            $element.find('div').html(pct);
        }
    });
    //animuj pozadie progressBaru
    $element.find('div').stop(true, false).animate({ width: progressBarWidth }, time);
}

//progressBar function
function progressBar (progressBarClass, time) {
   $(progressBarClass).each(function() {
    var bar = $(this);
    var getPercent = $(this).attr('data-progress');

    progress(getPercent, bar, time);
  });
}

//progressBar reset function
function progressBarReset (progressBarClass, time) {
   $(progressBarClass).each(function() {
    var bar = $(this);
    var getPercent = 0; //pri scrollovani nahor, resetni progressbar na 0%

    progress(getPercent, bar, time);
  });
}

//ProgressBar waypoint if scroll down
$('.AnimatedProgressBar').waypoint(function(direction) {
  if (direction === 'down') {
    progressBar ($('.AnimatedProgressBar'), 1500);
 }
}, {
  offset: '90%'
});

//ProgressBar waypoint if scroll back to top
$('.AnimatedProgressBar').waypoint(function(direction) {
 if (direction === 'up') {
   progressBarReset ($('.AnimatedProgressBar'), 0);
 }
}, {
  offset: '100%'
});
/* ANIMATED PROGRESSBAR END */


});
