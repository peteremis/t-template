var slider1,
    slider2,
    slider3;
$(function () {
    
    /* SLIDER */

    slider1 = $('.pricing-table-1').bxSlider({
        slideWidth: 300,
        minSlides: 1,
        maxSlides: 3,
        moveSlides: 1,
        slideMargin: 10,
        infiniteLoop: false,
        startSlide: 1
    });
    slider2 = $('.pricing-table-2').bxSlider({
        slideWidth: 300,
        minSlides: 1,
        maxSlides: 3,
        moveSlides: 1,
        slideMargin: 10,
        infiniteLoop: false,
        startSlide: 1
    });
    slider3 = $('.pricing-table-3').bxSlider({
        slideWidth: 300,
        minSlides: 1,
        maxSlides: 3,
        moveSlides: 1,
        slideMargin: 10,
        infiniteLoop: false,
        startSlide: 1
    });
    
    $(".tabs-menu a").click(function() {   // button that sets the DIV visible
                // Reloads the slideshow (bxSlider API function)
    });
    
    /* TABS */
    
    $(".tabs-menu a").click(function(event) {
        event.preventDefault();
        $(this).parent().addClass("current");
        $(this).parent().siblings().removeClass("current");
        var tab = $(this).attr("href");
        $(".tab-content").not(tab).css("display", "none");
        $(tab).fadeIn();
        
        slider1.reloadSlider();
        slider2.reloadSlider();
        slider3.reloadSlider();
    });
    
    /* STICKY NAV */

    $("#nav-sticky").sticky({
        topSpacing: 0,
        widthFromWrapper: true
    });

    $(".scroll-to").click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top - ($("#nav-sticky").outerHeight())
                }, 1000);
                return false;
            }
        }
    });

    var contentSections = $("[id*='sec-']"),
        secondaryNav = $("#nav-sticky");

    function updateSecondaryNavigation() {
        contentSections.each(function () {
            var actual = $(this),
                actualHeight = actual.height() + parseInt(actual.css('paddingTop').replace('px', '')) + parseInt(actual.css('paddingBottom').replace('px', '')),
                actualAnchor = secondaryNav.find('a[href="#' + actual.attr('id') + '"]');

            if ((actual.offset().top - secondaryNav.outerHeight() <= $(window).scrollTop()) && (actual.offset().top + actualHeight - secondaryNav.outerHeight() > $(window).scrollTop())) {
                actualAnchor.addClass('active');
            } else {
                actualAnchor.removeClass('active');
            }

        });
    }
    updateSecondaryNavigation();

    $(window).scroll(function (event) {
        updateSecondaryNavigation();
    });
});
