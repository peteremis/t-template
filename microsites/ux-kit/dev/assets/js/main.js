$(function () {
    $('.tooltip').qtip({
        content: {
            text: 'This is a tooltip.'
        },
        position: {
            my: 'left center',
            at: 'right center'
        }
    });

    /* TABS */

    $(".tabs-menu a").click(function (event) {
        event.preventDefault();
        if (!$(this).parent().hasClass("current") && !$(this).parent().hasClass("isVariant")) {
            toggleTabs();
        }

        $(this).parent().addClass("current");
        $(this).parent().siblings().removeClass("current");
        var tab = $(this).attr("href"),
            parent = $(this).closest('ul').parent().parent();
        parent.find('.tab-content').not(tab).css("display", "none");
        $(tab).fadeIn();

    });

    var toggleTabs = function () {
        $('.tab__variant').toggleClass('selected');
    };

    var elem = document.querySelector('.js-switch');
    var init = new Switchery(elem, { color: '#6bb324', secondaryColor: '#ededed' });
    $('.switchery').parent().closest('span').addClass('hide-span');

    /* POPUP */

    $('.popup').fancybox({
        padding: 11,
        margin: 0,
        closeBtn: false,
        helpers : {
            overlay : {
                css : {
                    'background' : 'rgba(0, 0, 0, 0.7)'
                }
            }
    }
    });

    $('.p-close').on('click', function(e) {
        e.preventDefault();
        $.fancybox.close();
    });
});
