$(function() {

    $('#a_referrer a').attr('href',document.referrer);

    $(".tabs-menu a").click(function (event) {
        event.preventDefault();
        if (!$(this).parent().hasClass("current") && !$(this).parent().hasClass("isVariant")) {
        	toggleTabs();
        }

        $(this).parent().addClass("current");
        $(this).parent().siblings().removeClass("current");
        var tab = $(this).attr("href"),
        	parent = $(this).closest('ul').parent().parent();
        parent.find('.tab-content').not(tab).css("display", "none");
        $(tab).fadeIn();

    });

    var toggleTabs = function () {
    	$('.tab__variant').toggleClass('selected');
    };

    $(".scroll-to").click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top - ($("#nav-sticky").outerHeight() - 10)
                }, 1000);
                return false;
            }
        }
    });

    $('.info-magio').qtip({
        content: {
            text: 'Ponuku môžete využiť aj k Magio televízii či k Pevnej linke M, L alebo XL. Prejdite k nákupu, kde si vyberiete ľubovoľnú službu.'
        }
    });

    $('.nas-tip').qtip({
        content: {
            text: 'Cenu za internet si môžete znížiť až o 35 % pridaním Magio televízie a/alebo Pevnej linky. Kliknite na PREJSŤ K NÁKUPU a skúste kalkulačku služieb.'
        }
    });
});
