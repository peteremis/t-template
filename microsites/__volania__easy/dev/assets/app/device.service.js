'use strict';

angular.module('core.device').service('dataService', ['$http',
    function($http) {
        var deviceJson = '../assets/data/phone-list.json';
        this.allTablets = function() {
            return $http.get(deviceJson, {
                cache: true
            }).then(function(res) {
                return res.data;
            });
        };
    }
]);
