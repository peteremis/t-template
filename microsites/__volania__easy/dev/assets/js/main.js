$(function () {

    $(".fancy").fancybox();

    $('.fancy-kred').fancybox({
        maxWidth: 320
    });

    $('.fancybox-media').fancybox({
        openEffect: 'none',
        closeEffect: 'none',
        helpers: {
            media: {}
        }
    });

    $('#balik-vzdyGiga').qtip({
		content:  '<b>BALÍK VŽDY GIGA</b><br>Pri aktivácii novej Easy karty získaš <br> <b>balík dát VŽDY GIGA s 1,5GB dát zadarmo.</b> Balík platí 30 dní a automaticky sa obnovuje. Môžeš si ho kedykoľvek deaktivovať poslaním SMS v tvare <br> GIGA VYP na 12330.',
		position: {
			my: 'bottom center',
			at: 'right top'
		}
    });

    $('#max-price').qtip({
		content:  'Za všetky hovory do Telekomu, Orange, O2 alebo 4ky zaplatíš spolu maximálne 0,50 EUR za deň',
		position: {
			my: 'bottom center',
			at: 'right top'
		}
    });    

    $('#balik-dataZadarmo').qtip({
		content:  '<b>+2,5 GB DÁT ZADARMO</b><br>Za prvé dobitie kreditu na novej Easy karte získaš<br> <b>balík dát VŽDY 2 GIGA s 2,5 GB dát zadarmo.</b> Balík platí 30 dní a automaticky sa obnovuje. Môžeš si ho kedykoľvek deaktivovať polaním sms v tvare <br> 2GIGA VYP na 12330.',
		position: {
			my: 'bottom center',
			at: 'right top'
		}
    });

    $('#balik-vzdyGiga, #balik-dataZadarmo').on('click', function(e){
        e.preventDefault();
    });

    $('#iphonesutaz').on('click', function(e){
        // e.preventDefault();
        changeTab();
    });


    /*$('.info-1').on('click', function(e){
        e.preventDefault;
    });

    $('.info-2').on('click', function(e){
        e.preventDefault;
    });*/

    // $('.info-1').qtip({
    //     content: {
    //         text: 'AKO TO FUNGUJE?<br/>Za minútu hovoru platíš 9 centov. Za SMS zaplatíš 6 centov. Ak dosiahneš hranicu 50 centov  za volania a SMS do jednej zo sietí v SR (Telekom/Orange/O2/4ka), v daný deň už v tejto sieti viac nezaplatíš.'
    //     },
    //     position: {
    //         my: 'left center',
    //         at: 'right center'
    //     },
    //     show: {
    //         event: 'click'
    //     },
    //     hide: {
    //         event: 'click mouseleave'
    //     }
    // }).bind('click', function (event) {
    //     event.preventDefault();
    //     return false;
    // });

    // $('.info-2').qtip({
    //     content: {
    //         text: 'Za surfovanie zaplatíš 0,10 €/MB, max. však do 0,50 €. Následne v danom dni surfuješ 200 MB zadarmo. Ak nesurfuješ, neplatíš samozrejme nič. Kedykoľvek si môžeš dokúpiť ďalších 200 MB poslaním SMS v tvare ESTE200 na 12330 alebo využiť <a href="#sec-5">Dátové balíčky.</a>'
    //     },
    //     position: {
    //         my: 'left center',
    //         at: 'right center'
    //     },
    //     show: {
    //         event: 'click'
    //     },
    //     hide: {
    //         event: 'click unfocus'
    //     }
    // }).bind('click', function (event) {
    //     event.preventDefault();
    //     return false;
    // });


    //  $('.automaticka-obnova').qtip({
    //     content: {
    //         text: 'Balík sa ti vždy automaticky obnoví na ďalšie obdobie. Obnovovanie balíka si môžeš kedykoľvek deaktivovať poslaním SMS v tvare GIGA VYP na 12330. Po vypršaní balíčka sa ti už ďalší neaktivuje.'
    //     },
    //     position: {
    //         my: 'left center',
    //         at: 'right center'
    //     }
    // });
    //  $('.automaticka-obnova-2').qtip({
    //     content: {
    //         text: 'Balík sa ti vždy automaticky obnoví na ďalšie obdobie. Obnovovanie balíka si môžeš kedykoľvek deaktivovať poslaním SMS v tvare 2GIGA VYP na 12330. Po vypršaní balíčka sa ti už ďalší neaktivuje.'
    //     },
    //     position: {
    //         my: 'left center',
    //         at: 'right center'
    //     }
    // });

    //  $('.dalsie-data').qtip({
    //     content: {
    //         text: 'Po vyčerpaní dát z balíčka si môžeš dokúpiť ďalších 500 MB dát len za 1 €. A to až 4 x. Pošli SMS v tvare ESTE500 na 12330.'
    //     },
    //     position: {
    //         my: 'left center',
    //         at: 'right center'
    //     }
    // });

    /* STICKY NAV */

    $("#nav-sticky").sticky({
        topSpacing: 0,
        widthFromWrapper: true
    });

    $(".scroll-to").click(function () {
        var comp = $(this);
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $('.' + this.hash.substr(1));
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                if (comp.hasClass('action-data')) {
                    $('.a-action-data').click();
                }
                $('html,body').animate({
                    scrollTop: target.offset().top - ($("#nav-sticky").outerHeight())
                }, 1000);
                return false;
            }
        }
    });

    var contentSections = $("[id*='sec-']"),
        secondaryNav = $("#nav-sticky");

    function updateSecondaryNavigation() {
        contentSections.each(function () {
            var actual = $(this),
                actualHeight = actual.height() + parseInt(actual.css('paddingTop').replace('px', '')) + parseInt(actual.css('paddingBottom').replace('px', '')),
                actualAnchor = secondaryNav.find('a[href="#' + actual.attr('id') + '"]');

            if ((actual.offset().top - secondaryNav.outerHeight() <= $(window).scrollTop()) && (actual.offset().top + actualHeight - secondaryNav.outerHeight() > $(window).scrollTop())) {
                actualAnchor.addClass('active');
            } else {
                actualAnchor.removeClass('active');
            }

        });
    }
    updateSecondaryNavigation();

    $(window).scroll(function (event) {
        updateSecondaryNavigation();
    });

});

 /* popup */
 $(document).ready(function() {
     $('.popup').fancybox({
         padding: 11,
         margin: 0,
         closeBtn: false,
         parent: "#content",
         helpers: {
             overlay: {
                 css: {
                     'background': 'rgba(0, 0, 0, 0.7)'
                 }
             }
         }
     });

     $('.p-close, .link-close').on('click', function(e) {
         e.preventDefault();
         $.fancybox.close();
     });
 });

 /* tabs & sliders */
 $(document).ready(function() {
     /* ::: DataDevice ::: */
     var $dataDeviceSlider = $('.data-device-slider');
     var $dataDevicePagin = $('.data-device-pagin');
     var $dataDevicePaginPrev = $('.data-device-pagin-prev');
     var $dataDevicePaginNext = $('.data-device-pagin-next');

     /* ::: TAB CONFIG ::: */
     $('ul.tabs li a').on('click', function(e) {
         $('.as1').css("visibility", "hidden");
         $('.as2').css("visibility", "hidden");
         var currentAttrValue = $(this).attr('href');
         $('.tab ' + currentAttrValue).show().siblings().hide();
         $(this).parent('li').addClass('current').siblings().removeClass('current');
         $('.as1').slick('setPosition', 0);
         $('.as2').slick('setPosition', 0);
         setTimeout(function() {
             $('.as1').css("visibility", "visible");
             $('.as2').css("visibility", "visible");
         }, 200);
         e.preventDefault();
     });

     /* ::: GET CURRENT SLIDE NUM ::: */
     $dataDeviceSlider.on('init reInit afterChange', function(event, slick, currentSlide, nextSlide) {
         //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
         var i = (currentSlide ? currentSlide : 0) + 1;
         $dataDevicePagin.text('(' + i + '/' + slick.slideCount + ')');
     });


     /* ::: PAGINATION INDICATOR ::: */
     if ($(window).width() < 769) {
         $dataDeviceSlider.on('init', function(event, slick) {
             if ($(window).width() < 769) {
                 $('.data-device-slider .slick-dots li').css("width", 100 / $('.dataDevice__item').length + '%');
             }
         });
     };
 });

function changeTab() {
    $('#sec-4 .tab__holder .tabs-menu .variant.current').removeClass('current');
    $('#iphone11-tab').addClass('current');
    $('#sec-4 .tab #tab-4').hide();
    $('#sec-4 .tab #tab-5').show();
}

 $(document).ready(function() {
    function checkMobileOS() { var MobileUserAgent = navigator.userAgent || navigator.vendor || window.opera; if (MobileUserAgent.match(/iPad/i) || MobileUserAgent.match(/iPhone/i) || MobileUserAgent.match(/iPod/i)) { return 'iOS'; } else if (MobileUserAgent.match(/Android/i)) { return 'Android'; } else { return 'unknown'; } }
    $(".sms_link").each(function(index) {
        $(this).on("click", function() {
            // event.preventDefault();
            var href = '';
            var message_text = this.dataset.smsText;
            var message_number = this.dataset.smsNumber;
            if (checkMobileOS() == 'iOS') {href = "sms:" + message_number + "&body=" + encodeURI(message_text); }
            if (checkMobileOS() == 'Android') { href = "sms:" + message_number + "?body=" + encodeURI(message_text); }
            $(this).attr('href', href);
        });
    });

    $('#content .banner-5 .easy-data').click(function(){
        $('#content .banner-5 .easy-data-cont').toggle(200);
    });

    
    if (window.location.hash) {
        var elem = window.location.hash.substr(1);

        if ($('.' + elem).length) {

            if (elem === 'iphone11') {
                changeTab();
            }

            setTimeout(function () {
                $('html,body').animate({
                    scrollTop: $("." + elem).offset().top - 60 + 'px'
                }, 500);
            }, 500);
        }

    }

    $("#slider_internet").slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        centerMode: false,
        infinite: false,
        swipeToSlide: true,
        swipe: true,
        arrows: true,
        dots: true,
        autoplay: false,
        initialSlide: 4,
        autoplaySpeed: 5000,
        touchMove: false,
        draggable: false,
        // pouzivalo sa so starsou verziou, kedy sa pre pagination nezobrazovali kruzky ale obdlzniky
        // customPaging: function(slider, i) {
        //     return '<span class="slide-dot">&nbsp;</span>';
        // },
        responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
                arrows: true,
                dots: true,
                initialSlide: 5
            }
        }, {
            breakpoint: 768,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: true,
                dots: true,
                initialSlide: 5
            }
        }]
    });    

    /* FROM TABS TO ACCORDION */
    var dataSegment = $("[data-segment]").each(function () {
        //console.log($(this).attr('data-segment')); //zobraz "data-segment" hodnotu pre kazdu <section>

        closestHead = $($(this).find('.tabs-menu a'));
        closestItemCount = closestHead.length - 1; //vytiahni pocet tabov pre kazdu <section> s atributom "data-segment"
        //console.log(closestItemCount + "heads");

        closestContent = $($(this).find('.tab-content')); //najdi tab-content v danej <section>
        //console.log(closestContent);

        closestAccordion = $(this).find('.accordion'); //najdi .accordion v danej <section>

        accordionItem = '<div class="item">';

        for (var i = 0; i <= closestItemCount; i++) {
            accordionItem += '<div class="heading">' + $(closestHead[i]).text() + '</div>';
            accordionItem += '<div class="content">' + $(closestContent[i]).html() + '</div></div>';

            if (i !== closestItemCount) {
                accordionItem += '<div class="item">';
            }
        }

        //if data-segment and data-accordion value match, show accordion data
        if ($(this).attr('data-segment') === closestAccordion.attr('data-accordion')) {
            $(accordionItem).appendTo(closestAccordion);
        }

        var $items = $('.accordion .item');
        //SET OPENED ITEM
        $($items[0]).addClass('open');

    });
    /* FROM TABS TO ACCORDION END */

    /* ACCORDION */
    $('.accordion .item .heading').click(function (e) {

        var clickedHead = e.target;
        var $item = $(clickedHead).closest('.item');
        var isOpen = $item.hasClass('open');
        var $content = $item.find('.content');
        var $acPrice = $(clickedHead).find('.ac-price');

        if (isOpen) {
            $content.slideUp(200);
            $item.removeClass('open');
            $acPrice.show();
        } else {
            $content.slideDown(200);
            $item.addClass('open');
            $acPrice.hide();
        }
    });


    $('.accordion .item.open').find('.content').slideDown(200);
    /* ACCORDION END */    
});
