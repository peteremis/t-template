$(document).ready(function () {
    /* scroll to + sticky nav */
    $("#nav-sticky").sticky({
        topSpacing: 0,
        widthFromWrapper: true
    });

    $("#vyhody_slider").slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: true,
        infinite: false,
        swipeToSlide: true,
        swipe: true,
        touchMove: true,
        draggable: true,
        dots: true,
        responsive: [{
                breakpoint: 800,
                settings: {
                    slidesToShow: 2,
                },
            },
            {
                breakpoint: 500,
                settings: "unslick",
            },
        ],
    });

    $(".info").each(function () {
        $(this).qtip({
            content: $(this).attr("alt"),
            style: {
                classes: "qtip-tipsy",
            },
            position: {
                corner: {
                    target: "leftMiddle",
                    tooltip: "topMiddle",
                },
                viewport: $(window),
            },
        });
    });

    if (window.location.hash) {
        var target = window.location.hash;

        if (target.length > 1) {
            target = target.substr(1);

            setTimeout(function () {
                scrollToTarget($("." + target));
            }, 750);
        }
    }

    $("#sec-3 .all-benefits").click(function (e) {
        e.preventDefault();
        var comps = $("#sec-3 .hide-mobil");
        if (comps.is(":visible")) {
            comps.hide(200);
            $(this).text("Ďaľšie výhody");
        } else {
            comps.show(200);
            $(this).text("Menej výhod");
        }
    });

    $(".hero__link").click(function (e) {
        e.preventDefault();
        scrollToTarget($("#sec-2"));
    });

    $("#internet-porucha").qtip({
        content: "V prípade výpadku Magio internetu nám zavolajte na bezplatnú non-stop linku 0800 123 777 a ak sa nám nepodarí poruchu okamžite odstrániť, na 48 hodín vám zadarmo poskytneme neobmedzený internet v mobile.",
        style: {
            classes: "qtip-tipsy",
        },
        position: {
            corner: {
                target: "leftMiddle",
                tooltip: "topMiddle",
            },
        },
        // position: {
        //     my: 'bottom center',
        //     at: 'top center'
        // }
    });

    // scroll
    $('.scroll-to').click(function (e) {
        e.preventDefault();
        var target = $('.' + $(this).attr('href').substr(1));
        scrollToTarget(target);
    });

    function scrollToTarget(target) {
        $("html,body").animate({
                scrollTop: target.offset().top - $("#nav-sticky").outerHeight()
            },
            1000
        );
    }

    $('.show-cmb').click(function (e) {
        e.preventDefault();

        $("#cmbForm").show();
        $("#cmbThanks").hide();
        $("#myModal").css("display", "flex");
        setCookie("cmbShow", "open", 15);
    });
});