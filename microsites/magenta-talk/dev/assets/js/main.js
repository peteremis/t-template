$(document).ready(function(){
    $(".tabs-menu a").click(function(event) {
        event.preventDefault();
        if (!$(this).parent().hasClass("current") && !$(this).parent().hasClass("isVariant")) {
            toggleTabs();
        }

        $(this).parent().addClass("current");
        $(this).parent().siblings().removeClass("current");
        var tab = $(this).attr("href"),
            parent = $(this).closest('ul').parent().parent();
        parent.find('.tab-content').not(tab).css("display", "none");
        $(tab).fadeIn();

    });

    var toggleTabs = function() {
        $('.tab__variant').toggleClass('selected');
    };

    $('.as1').slick({
        infinite: false,
        slidesToShow: 3,
        slidesToScroll: 1,
        dots: true,
        responsive: [{
            breakpoint: 700,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: true
            }
        }]
    });

    console.log($('div[class^="part"]'));

    $(".scroll-to").click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top - ($("#nav-sticky").outerHeight())
                }, 1000);
                return false;
            }
        }
    });

    // SEC-5 RADIO BUTTONS
    $('input[name="happy"]').click(function(){
        var val = $('input[name="happy"]:checked').val();
        $('#sec-5 div[class^="sec-cont-"]').not('#sec-5 div.sec-cont-' + val).hide();
        $('#sec-5 div.sec-cont-' + val).show();
    });

    // SEC-1 HR LINE show if <320px else hide
    $(window).on('resize', function() {
       if ($(window).width() <= 320) {
         if (!$('.line').length) {
           var hrline =  $('<hr class="line">');
           hrline.insertAfter($('section#hero'));
         }
       }
      else {
         $('.line').remove();
      }
 });


});
