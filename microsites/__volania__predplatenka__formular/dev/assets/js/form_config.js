const config = {
  companyName: "field__company-name",
  companyIco: "field__company-ico",
  companyPerson: "field__company-person-name",

  //select box
    // simcard: "sim-count",

  //select box
  //B2C alebo B2B
  customerType: "customer-type",

  phoneNumMigration: "prenos-msisdn",

  subjectCustomer: "subject-customer",
  subjectRobot: "subject-robot",

  firstName: "field__person-firstname",
  lastName: "field__person-lastname",
  birthNum: "field__id-number",
  userIdNum: "field__op-number",

  mesto: "cityInvoiceAutocompleteClass",
  ulica: "streetInvoiceAutocompleteClass",

  streetNum: "filed__adress-number",
  zipcode: "zipInvoiceAutocompleteClass",

  phoneNum: "field__mob-number",
  email: "field__email",

  //checkbox
  // deliveryAddressSwitcher: "cityDeliveryCheckboxClass",

  deliveryPhoneNum: "kontaktneCisloPreKuriera",
  deliveryCity: "cityDeliveryAutocompleteClass",
  deliveryAddress: "streetDeliveryAutocompleteClass",
  deliveryStreetNum: "streeNumbertDelivery",
  deliveryZipcode: "zipDeliveryAutocompleteClass",

  //checkboxy
  personalData: "fyzicka-osoba-op",
  digitalSign: "esign_suhlas",
  courierSign: "esign_osobne",
  marketing: "checkboxMarketing",
  phoneZoznam: "checkboxZverejnenie",
};

const configCheckBox = {
  digitalSign: "suhlas-esign",
  courierSign: "suhlas-osobne",
  marketing: "suhlas-marketing",
  phoneZoznam: "suhlas-zverejenie",
  suhlasZmluva: "suhlas-zmluva",
};

const configAutoComplete = {
  mesto: "cityInvoiceAutocompleteClass",
  ulica: "streetInvoiceAutocompleteClass",
};

const watchCity = 'mesto';
const watchStreet = 'ulica';

const mainUrl = {
  apiUrlProd: "https://backvm.telekom.sk/shop/ins/easy.php",
  apiUrlDev: "http://shop.telekom.localhost/easy.php",
  logUrlProd: "https://backvm.telekom.sk/shop/ins/easy_log.php",
  logUrlDev: "http://shop.telekom.localhost/easy_log.php",
};

