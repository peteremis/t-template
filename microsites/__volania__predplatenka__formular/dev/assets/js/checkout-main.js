$(document).ready(function () {

  var url = '';
  var lf_p_auth = '';
  var uniqId = Math.random().toString(36).substr(2, 9);

  if (typeof dynamicFormsFrontend != "undefined") {
    url = new URL(dynamicFormsFrontend.fileInputUrl);
    lf_p_auth = url.searchParams.get("p_auth");
  }

  var logUrl = mainUrl.logUrlProd;

  if (window.location.hostname == "localhost") {
    logUrl = mainUrl.logUrlDev;
  }

  saveLog('', 'browser', '', '', uniqId, lf_p_auth);
  saveLog('', 'version', 'version', 1, uniqId, lf_p_auth);

  var $mainPortlet = $('#p_p_id_dynamicForm_WAR_eccadmin_');
  var $lfFormName = $('#p_p_id_dynamicForm_WAR_eccadmin_ .dynamicFormMainForm');

  if (window.location.href.indexOf("test") > -1) {
  } else {
    $mainPortlet.css({'position': 'absolute', 'top': '-5000px', 'height': '0'});
  }

  var personalFormWrapper = $(".personalFormWrapper");
  var deliveryFormWrapper = $(".deliveryFormWrapper");
  var termsFormWrapper = $(".termsFormWrapper");
  var personalDataform_shortDesc = $(".forms-validated");
  var editPersonalInfo_btn = $(".editbtn");
  var revalidationInfoBox = $(".revalidation");
  var $submitbtn = $(".submitbtn");

  var validated = false;
  var deliveryByPostValid = false;
  var deliveryAddressSwitcher = document.querySelector(
    ".delivery-address-switch"
  );
  var postponedOrderBtn = $(".postponedOrder");

  var $orderNewBox = $(".order-new-box");
  var $simCard = $(".simcard");
  var $simCardOptions = $(".simcard-options");
  var $orderMigrationBox = $(".order-migration-box");
  var $migration = $(".migration");

  var $currentOperator = $("#current-operator");
  var $customerType = $("#customer-type");

  var $customerB2C = $(".customerB2C");
  var $customerB2B = $(".customerB2B");

  var $checkPersonalData = $("#personalData");
  var $checkDigitalSign = $("#digitalSign");
  var $checkCourierSign = $("#courierSign");

  // var $formInput = $('.personalFormWrapper input, .deliveryFormWrapper input');
  var $formInput = $('.personalFormWrapper input[type="text"], .deliveryFormWrapper input[type="text"], .personalFormWrapper input[type="number"], .deliveryFormWrapper input[type="number"]');
  var $formCheckbox = $('.personalFormWrapper input[type="checkbox"], .termsFormWrapper input[type="checkbox"], .deliveryFormWrapper input[type="checkbox"]');

  var
    b2cPersonLF = ['field__person-firstname', 'field__person-lastname'],
    b2cPerson = ["firstName", "lastName"],
    b2bPersonLF = ['field__company-name', 'field__company-ico', 'field__company-person-name'],
    b2bPerson = ["companyName", "companyIco", "companyPerson"];

  var functions = {
    bd_generatePopup: function (ratioValue, content, responsive, width) {
      $.fancybox({
        content: content,
        padding: 0,
        margin: 0,
        width: width,
        height: "auto",
        autoSize: false,
        topRatio: ratioValue,
        closeBtn: false,
        wrapCSS: "bd-scenario-popup " + responsive,
        parent: "#content",
        helpers: {
          overlay: {
            closeClick: false,
            css: {
              background: "rgba(0, 0, 0, 0.7)"
            }
          }
        }
      });
    },

    switchOrderType: function (orderType) {
      console.log(orderType);

      if (orderType == 'new') {
        $orderNewBox.addClass('selected');
        $orderMigrationBox.removeClass('selected');
        $simCard.show();
        $migration.hide();

      } else {
        $orderMigrationBox.addClass('selected');
        $orderNewBox.removeClass('selected');
        $simCard.hide();
        $migration.show();
        formChangeSelectbox('sim-count', 1);
        $simCardOptions.val('1').change();

        console.log('Change SIM Card to 1');
      }

      formChangeInput('order-type', orderType)
      changeSubject(orderType);

    },

    // scroll to target element
    bd_scrollToEl: function (el) {
      $("html, body").animate(
        {
          scrollTop: el.offset().top
        },
        800
      );
    },
    // DELIVERY OPTIONS <select>
    // user have to choose between "courier" or "store"
    bd_changeDeliveryOptions: function (selectedVal) {
      if (selectedVal == "kurierom") {
        $(".instore-img, .delivery-store").hide();
        $(".courier-img, .delivery-courier").show();
        $(".feeinfo").html(
          "Poplatok za doručenie kuriérom<br><b>zadarmo</b><br>"
        );
      } else if (selectedVal == "vpredajni") {
        $(".courier-img, .delivery-courier").hide();
        $(".instore-img, .delivery-store").show();
        $(".feeinfo").html(
          "Poplatok za vyzdvihnutie v predajni<br><b>zadarmo</b><br>"
        );
      }
    },
  };

  $customerType
    .select2({
      theme: "classic selectbox",
      minimumResultsForSearch: -1,
      width: "100%"
    })
    .on("change", function (e) {
      var selectedOption = $(this).val();

      if (selectedOption == 'b2c') {
        $customerB2C.show();
        $customerB2B.hide();
        changeToB2C();
      } else {
        $customerB2C.hide();
        $customerB2B.show();
        changeToB2B();
      }

    });

  $currentOperator
    .select2({
      theme: "classic selectbox",
      minimumResultsForSearch: -1,
      width: "100%"
    })
    .on("change", function (e) {
      var selectedOption = $(this).val();
      formChangeInput('prenos-operator', selectedOption)
    });

  $('.delivery-billing-options')
    .select2({
      theme: "classic selectbox",
      minimumResultsForSearch: -1,
      width: "100%"
    })
    .on("change", function (e) {
      var selectedOption = $(this).val();

      console.log('Delivery: ' + selectedOption);

    });

  $simCardOptions
    .select2({
      theme: "classic selectbox",
      minimumResultsForSearch: -1,
      width: "100%"
    })
    .on("change", function (e) {
      var selectedOption = $(this).val();
      formChangeSelectbox('sim-count', selectedOption);
    });

  // init DELIVERY ADDRESS <switcher>
  new Switchery(deliveryAddressSwitcher, {
    color: "#e20074",
    secondaryColor: "#d0d0d0"
  });

  // user wants delivery to other address
  deliveryAddressSwitcher.onchange = function () {
    if (deliveryAddressSwitcher.checked) {
      console.log('delivery other');
      $(".delivery-courier .delivery-inputs").show()
    } else {
      console.log('delivery same');
      $(".delivery-courier .delivery-inputs").hide();
    }
  };

  console.log(watchCity);
  $(watchCity).onchange = function () {
    console.log('change' + $(watchCity).val());
  };

  // popup windows close
  $(document).on("click", ".popup-close", function (e) {
    e.preventDefault();
    $.fancybox.close();
  });

  // legals toggle
  $(".legals-link.legals-1").on("click", function (e) {
    e.preventDefault();
    $("#legals-1").toggle("slow");
  });

  // legals toggle
  // $(".order-radio").on("change", function (e) {
  //   console.log('order-radio: ' + this.value);
  //   e.preventDefault();
  //   functions.switchOrderType(this.value);
  // });

  $("#order-new").on("click", function (e) {
    e.preventDefault();
    functions.switchOrderType('new');
  });

  $("#order-migration").on("click", function (e) {
    e.preventDefault();
    functions.switchOrderType('migration');
  });

  // show revalidation info box when user type to "personal data form" inputs
  personalDataForm.find("input, select").on("keyup change", function () {
    validated && personalFormWrapper.is(":visible")
      ? revalidationInfoBox.removeClass("hidden")
      : "";
  });

  $formInput.on("change", function (e) {
    e.preventDefault();
    var inputValue = this.value;
    // var inputName = this.attr('name');
    var inputName = $(this).attr('name');

    if (typeof config[inputName] != "undefined") {
      formChangeInput(config[inputName], inputValue)
      console.log(inputName + ": " + inputValue);
    } else {
      console.log('Input NOT FOUND');
      console.log('name: ' + inputName);
      console.log('value: ' + inputValue);
    }

  });

  $formCheckbox.on("change", function (e) {
    e.preventDefault();

    var inputValue = false;

    if (this.checked) {
      inputValue = true;
    }

    var inputName = $(this).attr('name');

    if (typeof configCheckBox[inputName] != "undefined") {
      formChangeCheckbox(configCheckBox[inputName], inputValue)
    } else {
      console.log('Checkbox NOT FOUND');
    }

    console.log('checkbox name: ' + inputName + ', value: ' + inputValue);

  });

  $checkPersonalData.change(function () {
    if (this.checked) {
      $checkDigitalSign.css("display", "flex");
      $checkCourierSign.css("display", "flex");

      formChangeCheckbox(configCheckBox['digitalSign'], true);
      formChangeCheckbox(configCheckBox['courierSign'], true);
    } else {
      $checkDigitalSign.css("display", "none");
      $checkCourierSign.css("display", "none");

      formChangeCheckbox(configCheckBox['digitalSign'], false);
      formChangeCheckbox(configCheckBox['courierSign'], false);
    }
  });

  function formChangeInput(inputName, value) {
    // console.log('change input box: ' + inputName + ', value: ' + value);
    saveLog('', 'value', inputName, value, uniqId, lf_p_auth);

    $('.' + inputName).find('input').val(value).blur().end();
  }

  function changeCustomerType(value) {
    console.log('change customer: ' + value);
    saveLog('', 'value', 'formFieldId153403', value, uniqId, lf_p_auth);

    $('#formFieldId153403').find('input').val(value).blur().end();
  }

  function formChangeCheckbox(inputName, value) {
    formChangeInput(inputName, value)
  }

  function formChangeSelectbox(inputName, value) {
    // console.log('change select: ' + inputName + ', value: ' + value);

    saveLog('', 'value', inputName, value, uniqId, lf_p_auth);
    $('.' + inputName).find('select').val(value).change().blur().end();
  }

  function changeSubject(orderType) {
    if (orderType == 'new') {
      formChangeInput('subject-robot', 'Objednávka novej Predplatenky s bonusovým kreditom')
      formChangeInput('subject-customer', 'Objednávka novej Predplatenky')
    } else {
      formChangeInput('subject-robot', 'Objednávka novej Predplatenky')
      formChangeInput('subject-customer', 'Objednávka novej Predplatenky - prenos čísla')
    }
  }

  function changeToB2C() {
    formChangeInput('customer-type', 'fyzicka-osoba')
    changeCustomerType('B2C');

    $.each(b2bPersonLF, function (index, inputName) {
      formChangeInput(inputName, '');
    });

    $.each(b2bPerson, function (index, inputName) {
      $('input[name=' + inputName + ']').val('');
    });

  }

  function changeToB2B() {
    formChangeInput('customerType', 'pravnicka-osoba')
    changeCustomerType('B2B');

    $.each(b2cPersonLF, function (index, inputName) {
      formChangeInput(inputName, '');
    });

    $.each(b2cPerson, function (index, inputName) {
      $('input[name=' + inputName + ']').val('');
    });

  }

  function getCookieValue(a) {
    var b = document.cookie.match('(^|[^;]+)\\s*' + a + '\\s*=\\s*([^;]+)');
    return b ? b.pop() : '';
  }

  function defaultForm() {
    formChangeCheckbox(configCheckBox['digitalSign'], false)
    formChangeCheckbox(configCheckBox['courierSign'], false);
    formChangeCheckbox(configCheckBox['marketing'], false);
    formChangeCheckbox(configCheckBox['phoneZoznam'], false);
    formChangeCheckbox(configCheckBox['suhlasZmluva'], false);

    formChangeInput('kontaktneCisloPreKuriera', '')
    formChangeSelectbox('sim-count', 1);

    //produkcia
    // $("#formFieldId153412 .form__submit").appendTo(".submitformbtn");
    // $(".submitbtn").hide();

    //test
    // $("#formFieldId153614 .form__submit").appendTo(".submitformbtn");
    // $(".submitbtn").hide();
  }

  changeToB2C();

  functions.switchOrderType('new');

  defaultForm();

  // ** VALIDATE FORM AND SUBMIT ** //
  $submitbtn.on("click", function (e) {
    // $('.form__submit').on("click", function (e) {
    // $('.tlacidlo_odoslat').on("click", function (e) {
    e.preventDefault();
    if ($(personalDataForm).valid()) {

      console.log('Form send');

      // $('.dynamicFormMainForm').submit();
      // wasFormSubmitted = true;

      // $('.tlacidlo_odoslat').trigger('click');
      // $('.tlacidlo_odoslat').click();

      var formData = personalDataForm.serializeArray();
      saveLog(formData, 'form', '', '', uniqId, lf_p_auth);

      formData = $lfFormName.serializeArray();
      saveLog(formData, 'lf_form', '', '', uniqId, lf_p_auth);

      if (window.location.hostname == "localhost") {
      } else {
        if (window.location.href.indexOf("test") > -1) {
          window.location.href = "/test/predplatenka/mam-zaujem?p_auth=" + lf_p_auth + "&p_p_id=dynamicForm_WAR_eccadmin&p_p_lifecycle=1&p_p_state=normal&p_p_mode=view&p_p_col_id=column-1&p_p_col_count=2&_dynamicForm_WAR_eccadmin_action=dynamicFormFinish&isFinished=true&fromLSB=false";
        } else {
          window.location.href = "/volania/predplatenka/mam-zaujem?p_auth=" + lf_p_auth + "&p_p_id=dynamicForm_WAR_eccadmin&p_p_lifecycle=1&p_p_state=normal&p_p_mode=view&p_p_col_id=column-1&p_p_col_count=2&_dynamicForm_WAR_eccadmin_action=dynamicFormFinish&isFinished=true&fromLSB=false";
        }
      }
    }
  });

  function saveLog(formData, logType, inputName, inputValue, uniqId, lf_p_auth) {
    var log = {
      formData: formData,
      logType: logType,
      inputName: inputName,
      inputValue: inputValue,
      uniqId: uniqId,
      lf_p_auth: lf_p_auth,
    }

    $.ajax({
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      url: logUrl,
      type: 'POST',
      // data: log,
      data: JSON.stringify(log),
      success: function (data) {
        console.log('log saved');
      },
      error: function () {
        console.log("log error");
      },
    });
  }

  /* Sample function that returns boolean in case the browser is Internet Explorer*/
  function isIEUserAgent() {
    ua = navigator.userAgent;
    /* MSIE used to detect old browsers and Trident used to newer ones*/
    var is_ie = ua.indexOf("MSIE ") > -1 || ua.indexOf("Trident/") > -1;

    return is_ie;
  }

  /* Create an alert to show if the browser is IE or not */
  if (isIEUserAgent()){
    $('#wrapper').hide();
    $('#scenario-bd').hide();
    $('.portlet-boundary_dynamicForm_WAR_eccadmin_').hide();

    $('.ie-not-supported').appendTo('body').show();

    window.setTimeout(function(){
      window.location.href = "/volania/predplatenka";
    }, 10000);
  }

  loggedAgent = false;
  if ($('ENABLE_div.desktop-agentBox').length) {
    loggedAgent = true;
  }

  /* Create an alert to show if the browser is IE or not */
  if (loggedAgent){
    $('.submitbtn').hide();
    $('.personalFormWrapper').hide();
    $('.deliveryFormWrapper').hide();
    $('.termsFormWrapper').hide();
    // $('#wrapper').hide();
    // $('#scenario-bd').hide();
    // $('.portlet-boundary_dynamicForm_WAR_eccadmin_').hide();

    $('.lsb-not-supported').appendTo('.submitformbtn').show();

/*
    window.setTimeout(function(){
      window.location.href = "/volania/predplatenka";
    }, 10000);
*/
  }

});