$(document).ready(function () {
  var apiUrl = mainUrl.apiUrlProd;

  if (window.location.hostname == "localhost") {
    apiUrl = mainUrl.apiUrlDev;
  }

  var
    $cityInput = $(".city-input"),
    $cityResultList = $(".city-form__result_list"),
    $streetInput = $(".street-input"),
    $streetInputLabel = $(".street-input-label"),
    $streetResultList = $(".street-form__result_list"),
    $numberInput = $(".number-input"),
    $numberInputLabel = $(".number-input-label"),
    $zipInput = $(".zipcode-input"),
    $zipInputLabel = $(".zipcode-input-label"),
    $birthNum = $(".birthNum-input"),
    $noStreet = $(".no-street"),

    $results = $(".results"),
    $btn = $(".submitbtn"),
    // $btn = $(".submitformbtn .submitField"),
    $errorMsg = $(".error-msg"),
    $infoMsg = $(".info-msg"),
    hasStreet = true,
    cityId = "",
    streetId = "",
    isInfoVisible = false,
    isErrorVisible = false,
    URL_CITY = apiUrl + "?searchTermKey=citySubstring",
    URL_STREET = apiUrl + "?searchTermKey=streetSubstring",
    URL_FINAL = apiUrl + "?searchTermKey=streetNoSubstring",
    MIN_KEY_DOWN = 2,
    MSG_CANT_CONNECT =
      "Nepodarilo sa načítať informácie, skúste to prosím ešte raz.";

  // $cityInput.val('Nitra');
  // $streetInput.val('Jabloňová');
  // $numberInput.val('32');

  // submitAddress();
  // switchBtnState($btn, false);

  // $cityInput.removeClass('valid');
  // $cityInput.addClass('error');

  function resultsList_Behavior(results, input) {
    $.each($(results).find("li"), function () {
      if ($(this).text().toLowerCase() === $cityInput.val().toLowerCase()) {
        $(this).click();
        if ($streetInput.is(":disabled")) {
          $numberInput.focus();
        } else {
          $streetInput.focus();
        }
      } else if (
        $(this).text().toLowerCase() === $streetInput.val().toLowerCase()
      ) {
        $(this).click();
        changeDataAttribute($numberInput, 'ok');
        $numberInput.focus();
      } else if ($(this).text() === $numberInput.val()) {
        $(this).click();
      }

      $(this).on("click", function () {
        if ($(input).hasClass("city-input")) {
          switchInputState($streetInput, false);
          $streetInput.focus();
          formChangeInput(configAutoComplete.mesto, $(this).text());
        } else if ($(input).hasClass("street-input")) {
          switchInputState($numberInput, false);
          changeDataAttribute($numberInput, 'ok');
          formChangeInput(configAutoComplete.ulica, $(this).text());
          $numberInput.focus();
        }
      });
    });
  }

  function formChangeInput(inputName, value) {
    console.log('change input box: ' + inputName + ', value: ' + value);
    $('.' + inputName).find('input').val(value).blur().end();
  }

  function hideList($input) {
    var $tempParent = $input.next(".results");
    $tempParent.hide();
  }

  function showList($input) {
    var $tempParent = $input.next(".results");
    resultsList_Behavior($tempParent, $input);
    $tempParent.show();
  }

  function emptyElem($elem) {
    $($elem).html("");
    $results.hide();
  }

  function changeDataAttribute($input, text) {
    $input.attr('data-status', text);
  }

  function addTextToInput($input, text) {
    $input.val(text);
  }

  function clearInput($input) {
    $input.val("");
  }

  function showErrorMsg(msg) {
    $errorMsg.show();
    $errorMsg.html(msg);
    isErrorVisible = true;
  }

  function hideErrorMsg() {
    $errorMsg.hide();
    $errorMsg.html("");
    isErrorVisible = false;
  }

  function showInfoMsg(msg) {
    $infoMsg.show();
    $infoMsg.html(msg);
    isInfoVisible = true;
  }

  function hideInfoMsg() {
    $infoMsg.hide();
    $infoMsg.html("");
    isInfoVisible = false;
  }

  function preventSubmitOnEnterPress($input) {
    $input.keypress(function (e) {
      var code = e.keyCode ? e.keyCode : e.which;
      if (code == 13) {
        return false;
      }
    });
  }

  function clearResultOnArrowKey($input, $result) {
    $input.keydown(function (e) {
      var code = e.keyCode ? e.keyCode : e.which;
      if (code == 38 || code == 40) {
        emptyElem($result);
      }
    });
  }

  function resetOnBackSpace($input, resetCityInput, resetStreetInput) {
    $input.on("keydown", function (e) {
      if (e.which == 8 || e.which == 46) {
        if (resetCityInput) {
          resetFields();
        } else if (resetStreetInput) {
          clearInput($numberInput);

          emptyElem($streetResultList);

          switchInputState($numberInput, true);
          switchInputState($numberInputLabel, true);

          changeDataAttribute($streetInput, 'error');
          changeDataAttribute($numberInput, 'error');

          $noStreet.hide();
        }
      }
    });
  }

  function resetFields() {
    clearInput($cityInput);
    clearInput($streetInput);
    clearInput($numberInput);

    emptyElem($cityResultList);
    emptyElem($streetResultList);

    switchInputState($streetInput, true);
    switchInputState($streetInputLabel, true);

    switchInputState($numberInput, true);
    switchInputState($numberInputLabel, true);

    changeDataAttribute($streetInput, 'error');
    changeDataAttribute($numberInput, 'error');

    $results.hide();
    $noStreet.hide();

  }

  var generateCityList = function (results) {
    var htmlToAppend = "";
    for (var i = 0; i < results.length; i++) {
      if (results[i].numOfStreets == 0) {
        hasStreet = false;
      } else {
        hasStreet = true;
      }
      htmlToAppend +=
        '<li class="' +
        (hasStreet ? "hasStreet" : "") +
        '" data-id="' +
        results[i].id +
        '">' +
        results[i].name +
        "</li>";
    }

    return htmlToAppend;
  };

  var generateStreetList = function (results) {
    var htmlToAppend = "";
    if (results !== null) {
      for (var i = 0; i < results.length; i++) {
        htmlToAppend +=
          '<li data-id="' +
          results[i].id +
          '">' +
          results[i].street_name +
          "</li>";
      }
    }
    return htmlToAppend;
  };

  function bindClickEvent($result, $input) {
    $result.on("click", function (e) {
      e.preventDefault();

      var choosenElem = $(e.target).text();
      $input.val(choosenElem);

      emptyElem($result);
      hideList($input);

      if ($input.hasClass("city-input")) {
        // console.log($result);
        // console.log($input);

        if ($(e.target).hasClass("hasStreet")) {
          switchInputState($streetInput);
          switchInputState($streetInputLabel);
          $streetInput.val("");
          cityId = $(e.target).data("id");
        } else {
          changeDataAttribute($streetInput, 'ok');

          $noStreet.show();
          switchInputState($streetInput, true);
          switchInputState($numberInput);
          $numberInput.focus();
          switchInputState($numberInputLabel);
        }
      } else if ($input.hasClass("street-input")) {
        changeDataAttribute($streetInput, 'ok');

        streetId = $(e.target).data("id");
        switchInputState($numberInput);
        switchInputState($numberInputLabel);
      }
    });
  }

  function bindKeyup($input, $result) {
    $input.on("keyup", function () {
      if (isErrorVisible) {
        hideErrorMsg();
      }
      if (isInfoVisible) {
        hideInfoMsg();
      }

      if ($(this).val() == "") {
        emptyElem($result);

        if ($input.hasClass("city-input")) {
          clearInput($streetInput);
          clearInput($numberInput);

          switchInputState($streetInput, true);
          switchInputState($streetInputLabel, true);

          switchInputState($numberInput, true);
          switchInputState($numberInputLabel, true);

          switchBtnState($btn, true);
        } else if ($input.hasClass("street-input")) {
          clearInput($numberInput);

          switchInputState($numberInput, true);
          switchInputState($numberInputLabel, true);

          switchBtnState($btn, true);
        }
      } else {
      }
    });
  }

  function bindNumKeyUp() {
    $numberInput.on("keyup", function () {
      if ($(this).val() != "") {
        switchBtnState($btn, false);
      } else {
        switchBtnState($btn, true);
      }
    });
  }

  function switchInputState($input, disable) {
    if (disable) {
      if (!$input.attr("disabled")) {
        $input.attr("disabled", true);
      }
      if (!$input.hasClass("disabled")) {
        $input.addClass("disabled");
      }
    } else {
      if ($input.attr("disabled")) {
        $input.attr("disabled", false);
      }
      if ($input.hasClass("disabled")) {
        $input.removeClass("disabled");
      }
    }
  }

  function switchBtnState($btn, disable) {
    if (disable) {
      if (!$btn.attr("disabled")) {
        $btn.attr("disabled", true);
      }
      if (!$btn.hasClass("disabled")) {
        $btn.addClass("disabled");
      }
    } else {
      if ($btn.attr("disabled")) {
        $btn.attr("disabled", false);
      }

      if ($btn.hasClass("disabled")) {
        $btn.removeClass("disabled");
      }
    }
  }

  function hideDropdownIfClickedElsewhere() {
    $(document).mouseup(function (e) {
      var container = $(".results"),
        elemToHide = $(".results ul");

      if (!container.is(e.target) && container.has(e.target).length === 0) {
        container.hide();
        elemToHide.html("");
        $results.hide();
      }
    });
  }

  function bindAutocomplete(
    $input,
    $result,
    addCityId,
    generateFunction,
    url,
    minLength
  ) {
    var $tempParent = $input.next(".results");
    $input.autocomplete({
      source: function (request, response) {
        $input.addClass("loading");
        emptyElem($result);

        var cityData = {
            searchTermValue: request.term,
          },
          streetData = {
            searchTermValue: request.term,
            cityId: cityId,
          },
          tempData = {};

        addCityId
          ? $.extend(true, tempData, streetData)
          : $.extend(true, tempData, cityData);

        $.ajax({
          url: url,
          data: tempData,
          dataType: "json",
          success: function (data) {
            if (data !== null && jQuery.isEmptyObject(data) === false) {
              response(
                $.map(data, function (item) {
                  if (item.name) {
                    return {
                      label: item["name"],
                      value: item["name"],
                    };
                  } else if (item.streetNoCount) {
                    return {
                      label: item["street_name"],
                      value: item["street_name"],
                    };
                  }
                })
              );
            } else {
              $results.hide();
            }

            var temphtmlToAppend = "";
            temphtmlToAppend = generateFunction(data);
            $(temphtmlToAppend).appendTo($result);

            showList($input);

            $input.removeClass("loading");
          },
          error: function () {
            console.log("error");
            showErrorMsg(MSG_CANT_CONNECT);
            $input.removeClass("loading");
          },
        });
      },
      select: function (event, ui) {
        $input.val(ui.item.value);
        resultsList_Behavior($tempParent, $input);
      },
      focus: function (event, ui) {
        $.each($tempParent.find("li"), function () {
          if ($(this).text() === ui.item.value) {
            $(this).css({"background-color": "#f5f5f5"});
          } else {
            $(this).css({"background-color": "inherit"});
          }
        });
      },

      minLength: minLength,
    });
  }

  function submitAddress() {
    $numberInput.addClass("loading");
    var tempVal = $numberInput.val();

    var street = $streetInput.val();
    if (street == 'Ulica' || street == 'Obec nemá ulice') {
      street = '';
    }
    searchAddress = $cityInput.val() + " " + street + " " + tempVal;

    $.ajax({
      type: "GET",
      async: true,
      crossDomain: true,
      dataType: "json",
      data: {
        searchTermValue: tempVal,
        cityName: $cityInput.val(),
        streetName: street,
        cityId: cityId,
        streetId: streetId,
      },
      url: URL_FINAL,
      success: function (response) {
        hideInfoMsg();
        hideErrorMsg();

        $numberInput.removeClass("loading");

        if (response != null && typeof response.status !== 'undefined') {
          if (response.status == "CORRECTED") {

            if (response.address.zip != '') {
              $zipInput.val(response.address.zip);
            }
            changeDataAttribute($numberInput, 'ok');

            // $birthNum.focus();
          } else {
            console.log('Error');
            resetFields();
            $cityInput.focus();
          }
        } else {
          changeDataAttribute($numberInput, 'ok');
        }

      },
      error: function (response) {
        $("#ajaxLoadingSpinnerOverlay").hide();
        hideInfoMsg();
        hideErrorMsg();
        showErrorMsg(response.responseJSON["error"]);
      },
    });
  }

  $(".scroll-to").click(function () {
    if (
      location.pathname.replace(/^\//, "") ==
      this.pathname.replace(/^\//, "") &&
      location.hostname == this.hostname
    ) {
      var target = $(this.hash);
      target = target.length ? target : $("[name=" + this.hash.slice(1) + "]");
      if (target.length) {
        $("html,body").animate(
          {
            scrollTop: target.offset().top - $("#nav-sticky").outerHeight(),
          },
          1000
        );
        return false;
      }
    }
  });

  $cityInput.keydown(function (e) {
    if (e.which === 9) {
      $results.hide();
    }
  });

  $streetInput.keydown(function (e) {
    if (e.which === 9) {
      $results.hide();
    }
  });

  switchInputState($streetInput, true);
  switchInputState($numberInput, true);
  switchBtnState($btn, true);

  bindAutocomplete(
    $cityInput,
    $cityResultList,
    false,
    generateCityList,
    URL_CITY,
    MIN_KEY_DOWN
  );

  bindAutocomplete(
    $streetInput,
    $streetResultList,
    true,
    generateStreetList,
    URL_STREET,
    MIN_KEY_DOWN
  );

  preventSubmitOnEnterPress($cityInput);
  preventSubmitOnEnterPress($streetInput);
  preventSubmitOnEnterPress($numberInput);

  resetOnBackSpace($cityInput, true, false);
  resetOnBackSpace($streetInput, false, true);

  bindClickEvent($cityResultList, $cityInput);
  bindKeyup($cityInput, $cityResultList);

  bindClickEvent($streetResultList, $streetInput);
  bindKeyup($streetInput, $streetResultList);

  bindNumKeyUp();

  hideDropdownIfClickedElsewhere();

  $numberInput.blur(function () {
    if ($numberInput.val() != '') {
      submitAddress();
    }
  });

  $(".not-found-form").show();
  $(".not-found-success").hide();

  $(".p-close").on("click", function (e) {
    e.preventDefault();
    $.fancybox.close();
  });

  /* TOGGLE ARROW */
  $(document).on("click", ".toggle-arrow, .arrow-right", function (event) {
    event.preventDefault();
    if ($(this).hasClass("arrow-right")) {
      return;
    }
    $(event.target).closest(".togleArrow").find(".arrow-content").toggle(200);
    $(this).find(".arrow-right").toggleClass("arrow-rotate");
  });
  /* TOGGLE ARROW END */

  $(".ajaxLoadingSpinnerOverlay-container div").addClass("animated");
});
