angular.module("nehnutelnostiApp", ['ngRoute'])
  //APP CONFIG - ROUTING
  .config(function($routeProvider, $locationProvider) {

    $routeProvider
      .when('/', {
        template: '<nehnutelnosti-index></nehnutelnosti-index>'
      })
      .when('/nehnutelnost' + '/:param', {
        template: '<nehnutelnost-detail></nehnutelnost-detail>'
      })
      .otherwise({
        redirectTo: '/'
      });

    $locationProvider
      .hashPrefix('');

    // //check browser support
    // if(window.history && window.history.pushState){
    //  // don't want to set base URL in <head> tag
    //  $locationProvider.html5Mode({
    //          enabled: true,
    //          requireBase: false
    //   });
    // }
  });

function zobrazNehnutelnost(time) {
  setTimeout(function() {
    $('html, body').animate({
      scrollTop: $("#sec-1").offset().top + 'px'
    }, time);
    return;
  }, 0);

};
