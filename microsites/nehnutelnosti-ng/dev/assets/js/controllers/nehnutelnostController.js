angular
  .module('nehnutelnostiApp')
  .component('nehnutelnostDetail', {
    templateUrl: '../views/nehnutelnost.html',
    controller: NehnutelnostController,
    controllerAs: '$ctrl'
  });

NehnutelnostController.$inject = ['$scope', '$routeParams', 'nehnutelnostiService', '$timeout'];
function NehnutelnostController($scope, $routeParams, nehnutelnostiService, $timeout) {

  nehnutelnostiService.then(function(detail) {


    //$scope.id = [$routeParams.id]; //get index number
    //$scope.detail = detail.data[$scope.id]; //get data from data.json by index number
    //console.log($scope.detail);
    //console.log($scope.detail.gallery);

    if (typeof $routeParams.param != 'undefined') {
        var param = $routeParams.param;

        angular.forEach(detail.data, function(val){
          if (val.info.routeName == param) {
            $scope.detail = val;
          }
        });
    }

    /* click printpage */
    $("#printpage").click(function(e) {
      window.print();
      setTimeout(window.close, 0);
    });
    /* click printpage END */

    /* poslat ponuku na email */
    $('#mailto').click(function(e) {
      $(this).attr("href", "mailto:komu?subject=Nehnutelnost&body=" + window.location.href)
    });
    /* poslat ponuku na email END */

    /* MAGNIFIC POPUP - START */
    //watch timeout for jQuery plugin to be loaded properly
    $scope.$watch("detail", function(newValue, oldValue) {
      $timeout(function() {
        $('.zoom-gallery').each(function() {
          $(this).magnificPopup({
            delegate: 'a',
            type: 'image',
            closeOnContentClick: false,
            closeBtnInside: false,
            mainClass: 'mfp-with-zoom mfp-img-mobile',
            image: {
              verticalFit: true,
              // titleSrc: function(item) {
              // 	return item.el.attr('title') + ' &middot; <a class="image-source-link" href="'+item.el.attr('data-source')+'" target="_blank">image source</a>';
              // }
            },
            gallery: {
              enabled: true,
              arrowMarkup: '<button title="%title%" type="button" class="mfp-arrow mfp-arrow-%dir%"></button>', // markup of an arrow button
              tPrev: 'Previous (Left arrow key)',
              tNext: 'Next (Right arrow key)',
            },
            zoom: {
              enabled: true,
              duration: 300, // don't foget to change the duration also in CSS
              opener: function(element) {
                return element.find('img');
              }
            }
          });
        });
      });
    });
    /* MAGNIFIC POPUP - END */

  });

};
