$(function () {

    $(".scroll-to").click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });

    $(".tabs-menu a").click(function (event) {
        event.preventDefault();
        if (!$(this).parent().hasClass("current") && !$(this).parent().hasClass("isVariant")) {
            toggleTabs();
        }

        $(this).parent().addClass("current");
        $(this).parent().siblings().removeClass("current");
        var tab = $(this).attr("href"),
            parent = $(this).closest('ul').parent().parent();
        parent.find('.tab-content').not(tab).css("display", "none");
        $(tab).fadeIn();

    });

    $(".lightbox").fancybox();

    var toggleTabs = function () {
        $('.tab__variant').toggleClass('selected');
    };

    function tooltipBuilder(elem, text) {
        $(elem).qtip({
            content: {
                text: text
            },
            position: {
                my: 'bottom center',
                at: 'top center'
            }
        });
    }

    tooltipBuilder(
        '.tooltip-1',
        'Množstvo TV staníc závisí od predplateného balíčka Magio GO. Zákazníci Magio Televízie majú zadarmo dostupné tie kanály, ktoré majú aktivované v rámci programových a prémiových balíčkov v Magio Televízii.');

    tooltipBuilder(
        '.tooltip-2',
        'Ďalšie funkcie:<br/>' +
        '■  pretáčanie a zastavenie obrazu počas živého vysielania aj archívu,<br/>' +
        '■  nastavenie poradia obľúbených TV staníc,<br/>' +
        '■  rodičovská kontrola programu,<br/>' +
        '■  nastavenie slovenského alebo anglického jazyka.');

    tooltipBuilder(
        '.tooltip-3',
        'Množstvo TV staníc závisí od predplateného balíčka Magio GO. Zákazníci Magio Televízie majú zadarmo dostupné tie kanály, ktoré majú aktivované v rámci programových a prémiových balíčkov v Magio Televízii.'
    );

    var zoomImage = $('.full'),
        thumbnail = $(".preview a"),
        galArray = $('.thumbnails a'),
        galSize = galArray.length,
        leftControll = $('.control-left'),
        rightControll = $('.control-right'),
        currentImg = $('.current-img'),
        allImg = $('.all-img'),
        indicatorContent = $('.in__content'),
        galImgArray = [],
        zoomConfig = {
            responsive: true,
            borderSize: 1,
            borderColour: '#2F2F2F',
            tint: true,
            tintColour: '#FFF',
            tintOpacity: 0.6,
            zoomWindowWidth: 300,
            zoomWindowHeight: 350
        };

    for (var i = 0; i < galArray.length; i++) {
        galImgArray.push($(galArray[i]).data('full'));
    }



    function getCurrentImg() {
        var currentImg = $('.full').attr('src');

        for (var i = 0; i < galSize; i++) {
            if (currentImg == galImgArray[i]) {
                return i + 1;
            }
        }
    }

    function adjustZoom(attr, data) {
        if (window.innerWidth <= 860) {
            $('.zoomContainer').remove();
            zoomImage.removeData('elevateZoom');
        } else {
            zoomImage.attr('src', attr);
            zoomImage.data('zoom-image', data);
            zoomImage.elevateZoom(zoomConfig);
        }
    }

    function calculateIndicator() {
        var tempCurrentImgPos = getCurrentImg(),
            percent;

        percent = (tempCurrentImgPos / parseFloat(galSize)) * 100;

        return percent;
    }


    function checkPositionBoundary() {
        var tempCurrImg = getCurrentImg();

        if (tempCurrImg == 1) {
            return -1;
        } else if (tempCurrImg == galSize) {
            return 0
        }
        return 1;
    }

    function fillIndicator() {
        $(indicatorContent).css('width', calculateIndicator() + '%');
    }

    function getNextImg() {
        return galImgArray[getCurrentImg()];
    }

    function getPrevImg() {
        return galImgArray[(getCurrentImg()) - 2];
    }

    function replaceImg(src) {
        zoomImage.attr('src', src);
        zoomImage.data('zoom-image', src);
    }

    function adjustCurrImgCounter() {
        currentImg.text(getCurrentImg());
    }

    rightControll.on('click', function (e) {
        e.preventDefault();
        var tempCurrImg = checkPositionBoundary();
        if (tempCurrImg == -1 || tempCurrImg > 0) {
            replaceImg(getNextImg());
            fillIndicator();
            adjustCurrImgCounter();
        }

    });

    leftControll.on('click', function (e) {
        e.preventDefault();
        var tempCurrImg = checkPositionBoundary();
        if (tempCurrImg >= 0) {
            replaceImg(getPrevImg());
            fillIndicator();
            adjustCurrImgCounter();
        }

    });

    fillIndicator();
    calculateIndicator();

    adjustCurrImgCounter();
    allImg.text(galSize);

    adjustZoom();

    thumbnail.on("click", function (e) {
        e.preventDefault();
        var attr,
            data;

        $(".selected").removeClass("selected");
        $(this).addClass("selected");
        var picture = $(this).data();

        attr = $(this).data('image');
        data = $(this).data('zoom-image');

        adjustZoom(attr, data);

        zoomImage.fadeOut(100, function () {
            zoomImage.attr("src", picture.full);
        }).fadeIn();

    });

    $(window).resize(function (e) {
        adjustZoom();
    });


});
