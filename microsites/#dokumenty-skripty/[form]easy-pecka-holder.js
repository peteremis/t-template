$(document).ready(function () {

    /*
     var form = $("#p_p_id_dynamicForm_WAR_eccadmin_").contents();
     $("#c-form").append(form);
     */


    $('.formCompositionRenderDisplay h2').hide();
    //$('.formCompositionRenderDisplay h4').hide();

    //$('.labelField').width(250);

    $('.prevPageButtonSpan').hide();
    $('.pageMiddle').hide();


    $('.fyzicka-osoba').find('.radio > span').addClass('checked');
    $('.pravnicka-osoba').find('.radio > span').removeClass('checked');





});

$(window).load(function () {

    var b2cperson = ['field__person-title', 'field__person-firstname', 'field__person-lastname'],
        b2bperson = ['field__company-name', 'field__company-ico', 'field__company-person-name'];
    //legalFo = ['legal-fo'];

    var customerType = $('.checked input:radio[name=typ-zakaznika]').val();
    enableCustomer(customerType);

    var customUrl = '/mam-zaujem/volania/easy/objednavka/prenos',
        currentUrl = window.location.pathname,
        $parentDiv = $('#prenos-zaujem').closest('li'),
        $checker = $parentDiv.find('.checker > span'),
        $checkBox = $parentDiv.find('input:checkbox');

    $checkBox.click(function () {

        if ($checker.hasClass('checked')) {
            window.history.replaceState({}, 'currentUrl', customUrl);
        } else {
            window.history.replaceState({}, 'currentUrl', currentUrl);
        }


    });

    $("input:radio[name=typ-zakaznika]").click(function () {
        var value = $(this).val(),
            customerType = value;

        enableCustomer(value);

    });

    var operator = $('#formFieldId110760 select').val();
    changeJuro(operator);

    $('#formFieldId110760 select').on('change', function () {
        changeJuro($(this).val());
    });

    function changeJuro(operator) {
        if (operator == 'Juro') {
            $('#formFieldId110761 span:nth-child(1)').html('Cena SIM pri prenose je 0,04 €‚¬ bez úvodného kreditu.<br>Akcia Dvojnásobný kredit za prenos čísla sa nevzťahuje na prechod zo služby Juro.');
        } else {
            $('#formFieldId110761 span:nth-child(1)').html('Cena SIM pri prenose je 0,04 €‚¬ bez úvodného kreditu.');

        }
    }

    function toggleField(classnameArr, state) {

        for (i = 0; i < classnameArr.length; i++) {

            var $form = $('.' + classnameArr[i]).closest('li');
            if (state == 'hide') {
                $form.hide();
            } else if (state == 'show') {
                $form.show();
            }
        }
    }

    function changeTootlipText(tooltipClassname, newText) {
        $('.' + tooltipClassname).qtip('option', 'content.text', newText);
    }

    function checkTooltip(customerType) {
        if (customerType == 'radioVal0') {
            changeTootlipText('info-adress', 'Adresa trvalého bydliska');
        } else {
            changeTootlipText('info-adress', 'Sídlo spoločnosti');
        }
    }

    function enableCustomer(value) {
        if (value === 'radioVal0') {
            checkTooltip(value);
            toggleField(b2cperson, 'show');
            toggleField(b2bperson, 'hide');
            //toggleField(legalFo, 'show');

        } else {
            //firemny zakaznik
            checkTooltip(value);
            toggleField(b2bperson, 'show');
            toggleField(b2cperson, 'hide');
            //toggleField(legalFo, 'hide');
        }
    }

    function addTooltip(targetClassName, tooltipClassName) {
        var $target = $('.' + targetClassName),
            tootlipContent = '<img class="' + tooltipClassName + ' info-tooltip' + '" src="/documents/10179/2652709/ico-info.png" alt="" />';

        $target.addClass('relative');

        $(tootlipContent).appendTo($target);
    }

    function createTooltip(tooltipClass, tooltipText) {
        $('.' + tooltipClass).qtip({
            content: {
                text: tooltipText
            },
            position: {
                my: 'left center',
                at: 'right center'
            }
        });
    }

    addTooltip('fyzicka-osoba', 'info-fo');
    addTooltip('pravnicka-osoba', 'info-po');
    addTooltip('cityInvoiceAutocompleteClass', 'info-adress');
    addTooltip('field__company-person-name', 'info-cpn');


    createTooltip('info-fo', 'spotrebiteľ');
    createTooltip('info-po', 'obchodná spoločnosť, fyzická osoba – podnikateľ, iná právnická osoba');
    createTooltip('info-cpn', 'označenie štatutárneho orgánu alebo oprávneného zástupcu právnickej osoby, ktorý je oprávnený zaväzovať právnickú osobu');
    createTooltip('info-adress', 'Adresa trvalého bydliska');

    checkTooltip(customerType);


});