/* **************
    s obrazkom
    ********** */
$(document).ready(function() {
    var btnLink = '/mob/objednavka/-/scenario/e-shop/hw-happy/vyber-hw/honor_8';
    var btnText = 'Pozrieť v katalógu';
    // ak nieje eyeCatcher tak: var ecUrl = '';
    var ecUrl = '/mob/objednavka/-/scenario/e-shop/stickers?p_p_lifecycle=2&amp;p_p_cacheability=cacheLevelPage&amp;_shoppingScenario_WAR_portlets_implicitModel=true&amp;stickerId=188';
    
    // ////////////////////////////////////////
    var dropZone = document.getElementById("column-2").getElementsByClassName("portlet-dropzone")[0].getElementsByClassName("waterbear-theme")[0];
    var ecIconHtml = '<div class="big-icon" style="position:absolute;top:10px;left:10px;"><img src="'+ ecUrl +'" class="sticker-picture"></div>';
    if (ecUrl.length == 0) {var ecIconHtml = ''; } 
    // obrazok uz existuje
    dropZone.innerHTML = '<div class="related-portlet-wrapper" style="position:relative;">' + dropZone.innerHTML + ecIconHtml +'<p class="buttons"> <a class="button" style="width:100%" href="' + btnLink + '" >'+ btnText +'</a> </p></div>';
});


    /* **************
    bez obrazka
    ********** */
$(document).ready(function() {
    var btnLink = "https://www.telekom.sk/mob/objednavka/-/scenario/e-shop/hw-happy/vyber-hw/samsung_galaxy_s8_orchid_gray";
    var btnText = 'Pozrieť v katalógu';
    var ecUrl = '/mob/objednavka/-/scenario/e-shop/stickers?p_p_lifecycle=2&amp;p_p_cacheability=cacheLevelPage&amp;_shoppingScenario_WAR_portlets_implicitModel=true&amp;stickerId=188';
    
    // ///////////////////////////
	var last = btnLink.substring(btnLink.lastIndexOf("/") + 1, btnLink.length);
    var dropZone = document.getElementById("column-2").getElementsByClassName("related-portlet-wrapper")[0];
    var imgHtml = '<img alt="" src="https://www.telekom.sk/mob/objednavka/-/scenario/e-shop/images?p_p_lifecycle=2&p_p_cacheability=cacheLevelPage&pictureType=DETAIL&productCode='+ last.toUpperCase() +'&pictureOrder=0" style="width: 100%; height: 100%;">';
    var ecIconHtml = '<div class="big-icon" style="position:absolute;top:10px;left:10px;"><img src="'+ ecUrl +'" class="sticker-picture"></div>';
    if (ecUrl.length == 0) {var ecIconHtml = ''; } 
    var toolBoxHtml = '<div class="related-portlet-wrapper" style="position:relative; margin-bottom:20px;">' + imgHtml + ecIconHtml +'<p class="buttons"> <a class="button" style="width:100%" href="' + btnLink + '" >'+ btnText +'</a> </p></div>';
    $(".related-portlet-wrapper").before(toolBoxHtml);
});
