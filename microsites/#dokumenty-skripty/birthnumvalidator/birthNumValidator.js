(function ($) {

    $.fn.birthNumValidator = function (options) {

        var opts = $.extend({}, $.fn.birthNumValidator.defaults, options),
            validateAge = opts.validateAge,
            $birthNumInput = this,
            $errorMsgHolder = $(this).closest('li').find('.error'),
            $vaidatedIconHolder = $(this).closest('li').find('.validated'),
            $submitBtn = $('.form__submit input'),
            birthNumber = '',
            numLength = 0,
            substring = "/";

        function validate() {

            birthNumber = $birthNumInput.val();
            numLength = birthNumber.length;

            if (containsWrongChar()) {
                validationError('Zadajte rodné číslo bez lomky.');
            } else if (numLength == '9') {
                validationSuccess();
            } else if (numLength == '10') {
                if (!isDivisible()) {
                    validationError('Zadajte platné rodné číslo.');
                } else {

                    validationSuccess();

                    if (validateAge) {
                        if (!isAdult()) {
                            validationError('Objednávky môžu vytvárať len osoby, ktoré dovŕšili 18 rokov.');
                        } else {
                            validationSuccess();
                        }
                    }

                }

            } else {
                validationError('Zadajte platné rodné číslo.');
            }

        }

        function containsWrongChar() {
            return birthNumber.indexOf(substring) !== -1
        }

        function calculateAge(birthday) {
            var ageDifMs = Date.now() - birthday.getTime();
            var ageDate = new Date(ageDifMs);
            return Math.abs(ageDate.getUTCFullYear() - 1970);
        }

        function getBirthDay() {
            var year = birthNumber.substring(0, 2),
                month = birthNumber.substring(2, 4) - 1,
                firstMonthLetter = birthNumber.substring(2, 3),
                day = birthNumber.substring(4, 6),
                birthday = null;

            if (firstMonthLetter == '5' || firstMonthLetter == '6') {
                month = Number(month);
                month = month - 50;

                if (month < 10) {
                    month = '0' + month;
                }

                month = month.toString();
            }


            if (Number(year) >= 0 && Number(year) <= 53) {
                year = '20' + year;
            } else {
                year = '19' + year;
            }


            birthday = new Date(year, month, day);

            return birthday;
        }

        function isAdult() {
            if (calculateAge(getBirthDay()) >= 18) {
                return true;
            } else {
                return false;
            }
        }

        function isDivisible() {
            return sumBirthNum() % 11 == 0;
        }

        function sumBirthNum() {
            var firstSixNum = birthNumber.substr(0,6);
            var secondFourNum = birthNumber.substr(6,10);
            
            return firstSixNum + secondFourNum;
        }

        function validationError(msg) {
            errorMsg(msg);
            hideValidationIcon();
            showErrorMsg();
            disableBtn();
        }

        function validationSuccess() {
            errorMsg('');
            showValidationIcon();
            hideErrorMsg();
            enableBtn();
        }

        function errorMsg(msg) {
            $errorMsgHolder.text(msg);
        }

        function hideValidationIcon() {
            $vaidatedIconHolder.css('display', 'none');
        }

        function showValidationIcon() {
            $vaidatedIconHolder.css('display', 'inline-block');
        }

        function hideErrorMsg() {
            $errorMsgHolder.css('display', 'none');
        }

        function showErrorMsg() {
            $errorMsgHolder.css('display', 'inline-block');
        }

        function disableBtn() {
            $errorMsgHolder.prop("disabled", true);
        }

        function enableBtn() {
            $submitBtn.prop("disabled", false);
        }


        $birthNumInput.unbind('blur');

        $birthNumInput.blur(function () {
            validate();
        });
    };

    $.fn.birthNumValidator.defaults = {
        validateAge: false
    };

}(jQuery));