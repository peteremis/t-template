$(function() {

    var cmbPar = getParameterByName('cmb');

    setTimeout(function() {

        $('.fixed-link__table').hide();
        $('.cmb__area').hide();

        if (cmbPar === '1') {
            $('.hero-cta').click();
            // console.log('par 1');
        }
        if (cmbPar === '2') {
            $('#chat').click();
            // console.log('XXX');
        }
    }, 500);
    
    $(".cmb_btn1").click(function() {
        event.preventDefault();
        $('.hero-cta').click();
    });

    function getParameterByName(name, url) {
        console.log(name, url);
        if (!url) {
            url = window.location.href;
        }
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

});
