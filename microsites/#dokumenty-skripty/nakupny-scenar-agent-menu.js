$(document).ready(function() {

    $("#sms-verification-verification-code-button").click(function() {
        setTimeout(agentMenu, 1000);
    });

    function agentMenu() {
        if ($(".agentbox").length > 0 && $(".message-yellow").length > 0) {
            //10
            if ($('.message-yellow p:eq(0):contains("zľavu 10%")').length > 0 || $('.message-yellow p:eq(1):contains("zľavu 10%")').length > 0) {
            	$(".message-yellow p, .info-button-wrapper").hide();
                $(".message-yellow").prepend('<div class="agent-menu-box"> <p><b>LSB:</b> <br> Pre toto číslo máme špecialnu <b>zľavu 10%</b> na mesačný paušál. <br> Použi tento formulár, zľavu uveď do poznámky LSB formulára <br><br> <a class="button" href="/mam-zaujem/volania/happy/formular?pausal=Happy" style="margin-right: 10px;"> <span class="info-button-container"> <span class="info-button-text">LSB Formulár</span> </span> </a> </p>  </div>');
            }
            //20
            if ($('.message-yellow p:eq(0):contains("zľavu 20%")').length > 0 || $('.message-yellow p:eq(1):contains("zľavu 20%")').length > 0) {
            	$(".message-yellow p, .info-button-wrapper").hide();
                $(".message-yellow").prepend('<div class="agent-menu-box"> <p><b>LSB:</b> <br> Pre toto číslo máme špecialnu <b>zľavu 20%</b> na mesačný paušál. <br> Použi tento formulár, zľavu uveď do poznámky LSB formulára <br><br> <a class="button" href="/mam-zaujem/volania/happy/formular?pausal=Happy" style="margin-right: 10px;"> <span class="info-button-container"> <span class="info-button-text">LSB Formulár</span> </span> </a> </p>  </div>');
            }
            //30
            if ($('.message-yellow p:eq(0):contains("zľavu 30%")').length > 0 || $('.message-yellow p:eq(1):contains("zľavu 30%")').length > 0) {
            	$(".message-yellow p, .info-button-wrapper").hide();
                $(".message-yellow").prepend('<div class="agent-menu-box"> <p><b>LSB:</b> <br> Pre toto číslo máme špecialnu <b>zľavu 30%</b> na mesačný paušál. <br> Použi tento formulár, zľavu uveď do poznámky LSB formulára <br><br> <a class="button" href="/mam-zaujem/volania/happy/formular?pausal=Happy" style="margin-right: 10px;"> <span class="info-button-container"> <span class="info-button-text">LSB Formulár</span> </span> </a> </p>  </div>');
            }
            //2
            if ($('.message-yellow p:eq(0):contains("zľavu 2 €")').length > 0 || $('.message-yellow p:eq(1):contains("zľavu 2 €")').length > 0) {
            	$(".message-yellow p, .info-button-wrapper").hide();
                $(".message-yellow").prepend('<div class="agent-menu-box"> <p><b>LSB:</b> <br> Pre toto číslo máme špecialnu <b>zľavu 2€</b> na mesačný paušál. <br> Použi tento formulár, zľavu uveď do poznámky LSB formulára <br><br> <a class="button" href="/mam-zaujem/volania/happy/formular?pausal=Happy" style="margin-right: 10px;"> <span class="info-button-container"> <span class="info-button-text">LSB Formulár</span> </span> </a> </p>  </div>');
            }
            //nonHw
            if ($('.message-yellow p:eq(0):contains("nový mobilný telefón")').length > 0 || $('.message-yellow p:eq(1):contains("nový mobilný telefón")').length > 0) {
            	$(".message-yellow p, .info-button-wrapper").hide();
                $(".message-yellow").prepend('<div class="agent-menu-box"> <p><b>LSB:</b> <br> Zákazník má narok na akciu <b>NON HW 2 HW</b> a môže si dokúpiť <b>nový mobilný telefón</b>. <br> Použi tento formulár, zľavu uveď do poznámky LSB formulára <br><br> <a class="button" href="/mam-zaujem/novy-mobilny-telefon" style="margin-right: 10px;"> <span class="info-button-container"> <span class="info-button-text">LSB Formulár</span> </span> </a> </p>  </div>');
            }
            //console.log("agent box");
        } else {
            //console.log("not agent");
        }
    };
});
