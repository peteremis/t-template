$(document).ready(function() {
    function checkMobileOS() { var MobileUserAgent = navigator.userAgent || navigator.vendor || window.opera; if (MobileUserAgent.match(/iPad/i) || MobileUserAgent.match(/iPhone/i) || MobileUserAgent.match(/iPod/i)) { return 'iOS'; } else if (MobileUserAgent.match(/Android/i)) { return 'Android'; } else { return 'unknown'; } }
    $(".sms_link").each(function(index) {
        $(this).on("click", function() {
            // event.preventDefault();
            var href = '';
            var message_text = this.dataset.smsText;
            var message_number = this.dataset.smsNumber;
            if (checkMobileOS() == 'iOS') {href = "sms:" + message_number + "&body=" + encodeURI(message_text); } 
            if (checkMobileOS() == 'Android') { href = "sms:" + message_number + "?body=" + encodeURI(message_text); }
            $(this).attr('href', href);
        });
    });
});



//https://www.telekom.sk/documents/10179/13169475/sms-prefill.js