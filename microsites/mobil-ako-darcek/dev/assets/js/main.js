$(function() {

    $('.external_battery').qtip({
        content: 'Darujeme vám externú batériu, ktorá slúži ako nabíjačka mobilu či tabletu aj tam, kde nie je elektrická zásuvka.<br/>'+
                 'Ak sa vám vybije, môžete si ju nabiť sami alebo vám ju vymeníme za nabitú v niektorom z našich Telekom centier až do konca roku 2017.',
        position: {
            my: 'bottom center',
            at: 'top center'
        }
    });
    $('.happy-l-tip').qtip({
        content: 'PAUŠÁL HAPPY L<br>■ neobmedzené volania do mobilnej a pevnej siete Telekom<br> ■ volania do ostatných sietí v SR a do EÚ: 250 minút<br> ■ V Česku, Maďarsku, Poľsku a Rakúsku voláte a SMS-kujete v rámci voľných minút a SMS. Po ich minutí máte ceny ako doma!<br> ■ neobmedzené sms a mms do všetkých sietí  v SR a z CZ, HU, AT, PL do SR<br> ■ 4G Internet v mobile - 2 000 MB v SR',
        position: {
            my: 'bottom center',
            at: 'top center'
        }
    });
    $('.happy-xl-tip').qtip({
        content: 'PAUŠÁL HAPPY XL<br>■ neobmedzené volania do všetkých sietí v SR<br> ■ neobmedzené prichádzajúce roamingové volania v susedných štátoch (CZ, HU, AT, PL)<br> ■ 1000 minút odchádzajúce volania zo SR a zo susedných štátov (CZ, HU, AT, PL) do EÚ<br> ■ 1000 minút prichádzajúce roamingové volania vo zvysku EÚ<br> ■ neobmedzené sms a mms do vsetkých sietí v SR a z CZ, HU, AT, PL do SR<br> ■ 4G Internet v mobile – 4 000 MB v SR',
        position: {
            my: 'bottom center',
            at: 'top center'
        }
    });
    $('.huaweip9-tip').qtip({
        content: 'HUAWEI P9 LITE<br>■ 5,2“ FullHD IPS displej <br> ■ snímač odtlačku prsta<br> ■ tenké telo s kovovým rámom<br> ■ 3000mAh batéria pre dlhú výdrž<br> ■ skvelý pomer ceny a výkonu',
        position: {
            my: 'bottom center',
            at: 'top center'
        }
    });
    $('.huaweiy5-tip').qtip({
        content: 'HUAWEI Y5 II<br>■ 5“ HD IPS displej<br> ■ 8 MP fotoaparát s duálnym bleskom<br> ■ Univerzálne Easy Key tlačidlo<br> ■ Podpora 4G LTE<br> ■ Elegantný a kompaktný dizajn',
        position: {
            my: 'bottom center',
            at: 'top center'
        }
    });
      $('.lenovo100s-tip').qtip({
        content: 'LENOVO IDEAPAD 110S<br>■ 11,6“ HD displej<br> ■ 4 jadrový procesor Intel Celeron N3160<br> ■ Pamäť: 2 GB, 32 GB storage (úložisko)<br> ■ WiFi 802.11 B/G/N<br> ■ VGA kamera 0.3 Mpx<br> ■ Možnosť rozšírenia o MicroSD kartu do 64 GB<br> ■ 2 x USB 2.0, 1 x HDMI<br> ■ Windows 10 Home SK', position: {
            my: 'bottom center',
            at: 'top center'
        }
    });
        $('.lenovoc2-tip').qtip({
        content: 'LENOVO C2<br>■ 5“ HD IPS displej<br> ■ 4 jadrový procesor<br> ■ 4G LTE rýchlosť<br> ■ Veľká batéria 2750 mAh<br> ■ Operačný systém Android 6.0<br> ■ Fotoaparát 8 MP so snímačom SONY a 5MP selfie fotoaparát (kamera)<br> ■ 1W reproduktor s Waves MaxxAudio', position: {
            my: 'bottom center',
            at: 'top center'
        }
    });
    $('.happy-xxl-tip').qtip({
        content: 'Paušál Happy XXL<br>■ neobmedzené volania do všetkých sietí v SR <br> ■ neobmedzené prichádzajúce roamingové volania v susedných štátoch (CZ, HU, AT, PL)<br> ■ 1000 minút odchádzajúce volania zo SR a zo susedných štátov (CZ, HU, AT, PL) do EÚ<br> ■ 1000 minút prichádzajúce roamingové volania vo zvyšku EÚ<br> ■ 100 minút odchádzajúce roamingové volania vo zvyšku EÚ<br> ■ SMS a MMS neobmedzené zo SR a zo susedných státov do SR a EÚ<br> ■ 4G Internet v mobile – 6 000 MB v SR + 100 MB v EÚ',
        position: {
            my: 'bottom center',
            at: 'top center'
        }
    });
    $('.xbox-tip').qtip({
        content: 'XBOX ONE S MINECRAFT FAVORITES<br>■ Herná konzola Microsoft Xbox One S s 500 GB pevným diskom<br> ■ 8 jadrový procesor<br> ■ 8 GB pamäť RAM<br> ■ WiFi pripojenie<br> ■ Blu-Ray mechanika', 
        position: {
            my: 'bottom center',
            at: 'top center'
        }
    });
    $('.coolpad-tip').qtip({
        content: 'COOLPAD PORTO S<br>■ 5“ HD displej (294 ppi)<br> ■ Tenký kompaktný dizajn<br> ■ Rýchle 4G pripojenie<br> ■ 8 Mpx fotoaparát<br> ■ Podpora Micro SD karty až do 32 GB', 
        position: {my: 'bottom center',
            at: 'top center'
        }
    });

    $(".lightbox").fancybox();

    $('.fancybox-media').fancybox({
        openEffect: 'none',
        closeEffect: 'none',
        helpers: {
            media: {}
        }
    });

    $(document).on('click', '.popup-modal-dismiss', function(e) {
        e.preventDefault();
        $.fancybox.close();
    });
    $("#nav-sticky").sticky({
        topSpacing: 0,
        widthFromWrapper: true
    });

    $(".scroll-to").click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top - ($("#nav-sticky").outerHeight())
                }, 1000);
                return false;
            }
        }
    });

    var contentSections = $("[id*='sec-']"),
        secondaryNav = $("#nav-sticky");

    function updateSecondaryNavigation() {
        contentSections.each(function() {
            var actual = $(this),
                actualHeight = actual.height() + parseInt(actual.css('paddingTop').replace('px', '')) + parseInt(actual.css('paddingBottom').replace('px', '')),
                actualAnchor = secondaryNav.find('a[href="#' + actual.attr('id') + '"]');

            if ((actual.offset().top - secondaryNav.outerHeight() <= $(window).scrollTop()) && (actual.offset().top + actualHeight - secondaryNav.outerHeight() > $(window).scrollTop())) {
                actualAnchor.addClass('active');
            } else {
                actualAnchor.removeClass('active');
            }

        });
    }
    updateSecondaryNavigation();

    $(window).scroll(function(event) {
        updateSecondaryNavigation();
    });

});
