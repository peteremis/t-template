var qtipdata = [
	//cesky balicek
  {
    "stanica": ["CT-1-HD", "CT-1", "CT1-SD-HD", "CT1"],
    "text": "Česká televízna stanica ponúka bohatý program zameraný na rodinu. Televízia vysiela české ale aj zahraničné filmy a seriály, spravodajstvo ako aj zábavné a súťažné programy"
  },
  {
    "stanica": ["CT-2-HD", "CT-2", "CT2-SD-HD", "CT2"],
    "text": "Česká televízia orientovaná na kultúru, vzdelanie, dokumenty a na programy so zameraním na prírodu, históriu a vedu."
  },
	//zakladny balicek
	{
		"stanica": ["CNN-intl-europe"],
		"text": "Spravodajský kanál, prinášajúci top aktuality zo sveta."
	},
	{
		"stanica": ["CT-24-SDHD", "CT-24"],
		"text": "Česká spravodajská televízia prinášajúca najaktuálnejšie informácie z domova aj zo zahraničia. "
	},
	{
		"stanica": ["Dajto-SDHD", "Dajto-HD", "Dajto", "dajtohd"],
		"text": "Televízia so zameraním na mužské publikum vysielajúca prevažne seriály a celovečerné filmy a šport."
	},
	{
		"stanica": ["Doma-SDHD", "Doma-HD", "Doma2"],
		"text": "Slovenská televízna stanica s bohatým programom plná známych domácich ale aj zahraničných seriálov ako Chlapi neplačú, Ženy z rodu Gilmorovcov, Velvet, Nekonečná láska a iné pútavé relácie Na streche, Adela show, Smotánka a mnohé iné zábavné programy, ktoré si nemôžete nechať ujsť. "
	},
	{
		"stanica": ["Dvojka-SDHD", "Dvojka-HD", "Dvojka", "dvojkahd"],
		"text": "Slovenská televízia vysielajúca rôzne športové prenosy, či z domácich, ale aj medzinárodných športových utkaní. Všetko doplňujú aj publicistické programy, rada dokumentov, alebo niekoľko seriálov, predovšetkým tých starších. Nechýbajú samozrejme ani pravidelné spravodajské relácie a to buď celoštátne, alebo relácie zamerané na konkrétne regióny."
	},
	{
		"stanica": ["Fashion-tv"],
		"text": "TV kanál ponúkajúci domáce a zahraničné módne prehliadky, hudbu, životný štýl a rôzne nové trendy zo sveta."
	},
	{
		"stanica": ["Jednotka-SDHD", "Jednotka-HD", "Jednotka", "jednotkahd"],
		"text": "Slovenská televízia pre celú rodinu, ktorá ponúka divákom všetko, od spravodajstva, cez radu seriálov, až po filmy, vzdelávacie, či úzkoprofilové programy. O celodennú zábavu sa postarajú známe relácie ako Milujem Slovensko, 5 proti 5, Dámsky klub a mnohé iné."
	},
	{
		"stanica": ["JOJ-SDHD", "JOJ-HD", "JOJ"],
		"text": "Slovenská televízia vysielajúca nepretržite 24 hodín denne, počas ktorých si prídu na svoje všetky generácie. Svojim divákom ponúka nielen radu spravodajských, ale hlavne zábavných programov, rôznych seriálov a relácií. Medzi tie najsledovanejšie určite patrí Moja mama varí lepšie ako tvoja, Nákupné maniačky, Nikto nie je dokonalý, Uhádni môj vek a mnohé ďalšie."
	},
	{
		"stanica": ["LEO-gold-HD", "leogoldhd"],
		"text": "TV program pre dospelých s erotickým zameraním v HD kvalite."
	},
	{
		"stanica": ["Magio-tv-kanal"],
		"text": "Kanál, ktorý prináša praktické informácie o programovej ponuke, TV kanáloch a Videopožičovni."
	},
	{
		"stanica": ["Markiza-SDHD", "Markiza-HD", "Markiza", "markizahd"],
		"text": "Slovenská najsledovanejšia televízia pre celú rodinu vysielajúca seriály a programy nielen zahraničnej, ale aj vlastnej tvorby. Počas celého dňa ponúka viacero úspešných seriálov ako Búrlivé víno, Horná Dolná a taktiež relácií Dobre vedieť, Chart show, Farma, Kredenc, Tvoja tvár znie povedome a mnohé iné, ktoré sa o Vašu zábavu určite postarajú. "
	},
	{
		"stanica": ["Nova-international-SDHD", "Nova-international"],
		"text": "Česká televízia určená pre celú rodinu plná seriálov a zaujímavých relácií."
	},
	{
		"stanica": ["Plus-SDHD", "Plus-HD", "Plus"],
		"text": "Slovenská televízia s pomerne jasným konceptom, v ktorom sa striedajú ako správy, zábavné programy, tak samozrejme aj príjemné filmy, staršie aj nové. Všetko dopĺňajú seriály. Medzi tie najobľúbenejšie určite patria Simpsonovci, ale aj rada kriminálnych seriálov, ktoré majú na Joj Plus svoje nezastupiteľné miesto. Okrem toho sa táto stanica venuje aj celej rade dokumentov. Je tak komplexným kanálom, na ktorom nielen mladší divák nájde všetko od poznania, cez informácie, až po zábavu."
	},
	{
		"stanica": ["Prima-plus-HD", "Prima-plus"],
		"text": "Televízia ponúkajúca predovšetkým programy vlastnej tvorby, či už ide o obľúbené seriály, spravodajské programy, zábavné šou, kulinárske či hobby programy."
	},
	{
		"stanica": ["RTL", "rtl"],
		"text": "Plnoformátová nemecká televízia pre celú rodinu  vysielajúca žánrový mix a spravodajstvo."
	},
	{
		"stanica": ["Senzi-tv"],
		"text": "TV kanál Senzi predstavuje najsledovanejšiu šlágrovú televíziu na Slovensku, ktorá vysiela 24 hodín denne prúd najlepších piesní, ľudoviek a dychoviek."
	},
	{
		"stanica": ["TA3-SDHD", "TA3", "ta3"],
		"text": "Slovenská spravodajská televízia prinášajúca najaktuálnejšie informácie z domova aj zo zahraničia. Okrem spravodajstva ako takého samozrejme nechýbajú ani rôzne individuálne pohľady, debaty, alebo diskusie."
	},
	{
		"stanica": ["TV9", "tv9"],
		"text": "Spravodajská televízia zameraná sa mapovanie diania v krajských mestách Slovenska, plná živých vstupov, spravodajských blokov a relácií."
	},
	{
		"stanica": ["TV-lux"],
		"text": "Slovenská televízia zameraná najmä na  tematiku duchovných a ľudských hodnôt."
	},
	{
		"stanica": ["WAU-SDHD", "WAU-HD", "Wau"],
		"text": "Televízia so zameraním na ženské témy. Programy sú tvorené nielen seriálmi, alebo zábavnými súťažami, či talkshow, ale hlavne programami zahraničnej produkcie, ktoré na ženy priamo smerujú. Medzi tie najzaujímavejšie rozhodne patria napríklad dokonalé premeny žien v ich svadobný deň, dokumenty o tých najšpinavších domácnostiach, alebo napríklad nahliadnutie do životov tých najbohatších rodín, ktoré žijú v USA."
	},
	//extra balicek L
	{
		"stanica": ["Digi-sport-1-SDHD", "Digi-sport-2-SDHD", "Digi-sport-3-SDHD", "Digi-sport-4-SDHD", "Digi-sport-5-SDHD"],
		"text": "Športový kanál s najlepšou futbalovou ponukou vo vysokom rozlíšení."
	},
	{
		"stanica": ["Tuki-tv-HD", "Tuki-tv", "Tuki-TV"],
		"text": "TV kanál určený pre tých najmenších plný rozprávok a dobrodružstiev vo vysokom rozlíšení."
	},
	//balicek XL
	{
		"stanica": ["360-tune-box-HD"],
		"text": "Hudobný kanál, ktorý sa zameriava na mladé publikum, ktorého hudobné záujmy presahujú zostavu populárnych interprétov vo vysielaní doterajších staníc."
	},
	{
		"stanica": ["Auto-motor-sport"],
		"text": "Televízny kanál, ktorý vysiela relácie s tematikou pre motoristov, kde im prináša novinky zo sveta automobilov, motocyklov a zaujímavosti z oblasti motoristického športu"
	},
	{
		"stanica": ["Cartoon-network-", "Cartoon-network"],
		"text": "Televízny kanál, ktorý vysiela prevažne animovaný obsah pre deti a tínedžerov."
	},
	{
		"stanica": ["Crime-and-investigation", "Crime-and-investigation-SD"],
		"text": "Televízia zameraná na vyšetrovanie a zločiny z celého sveta."
	},
	{
		"stanica": ["Deutsche-welle"],
		"text": "Nemecká medzinárodná spravodajská TV stanica."
	},
	{
		"stanica": ["Discovery-science-HD", "Discovery-science"],
		"text": "Populárno-vzdelávací televízny program, ktorý prináša zaujímavé dokumenty o vede, vynálezoch, technike, modernom inžinierstve, ale aj dejinách a prírode vo vysokom rozlišení"
	},
	{
		"stanica": ["Discovery-turbo-xtra"],
		"text": "Dokumentárny kanál vysielajúci širokú škálu dokumentov a rozmanitých príbehov, ktoré prezentujú svet a veci okolo neho zaujímavám a netradičným spôsobom."
	},
	{
		"stanica": ["DocuBox-HD", "DocuBox"],
		"text": "TV kanál venovaný  prírode, voľne žijúcim živočíchom a histórií."
	},
	{
		"stanica": ["DoQ"],
		"text": "Televízia plná životopisných filmov a zaujímavých osudov z medziľudských vzťahov. TV kanál ponúka rôzne zábery zo sveta bohatých ako aj zo života svätých."
	},
	{
		"stanica": ["Duck-tv"],
		"text": "Vzdelávací, zábavný a relaxačný program pre najmenších divákov, pomocou jednoduchých animácií pomáha rozvíjať vizuálne a sluchové schopnosti detí."
	},
	{
		"stanica": ["Dusk"],
		"text": "Zmyselný kanál určený predovšetkým pre ženy plný romantiky a ľahkej erotiky."
	},
	{
		"stanica": ["English-club"],
		"text": "Jedinečný vzdelávací TV kanál pre všetkých, ktorý sa chcú vzdelávať a zdokonaľovať v angličtine bez ohľadu na vek."
	},
	{
		"stanica": ["Euronews"],
		"text": "Spravodajská mnohojazyčná TV stanica, ktorá prináša správy z celého sveta z európskej perspektívy."
	},
	{
		"stanica": ["FashionBox-HD"],
		"text": "TV kanál ponúkajúci domáce a zahraničné módne prehliadky, hudbu, životný štýl a rôzne nové trendy zo sveta."
	},
	{
		"stanica": ["Fast&FunBox-HD"],
		"text": "Predstavuje nový TV kanál plný adrenalínu, extrémnych športov, automobilových pretekov, prehliadok a súťaží z celého sveta."
	},
	{
		"stanica": ["FightBox", "Fight-box", "FightBox-HD"],
		"text": "Športový kanál zameraný na bojové športy ako Box, Taekwondo, Sumo a ďalšie."
	},
	{
		"stanica": ["Film+-HD", "FILM+", "Film-plus-HD", "Film-plus"],
		"text": "Filmový kanál, vysielajúci množstvo filmov rôznych žánrov americkej a európskej kinematografie pre celú rodinu spolu s televíznymi premiérami úspešných kinohitov."
	},
	{
		"stanica": ["FilmBox-arthouse"],
		"text": "Významný a celosvetový TV kanál, kde sa celé spektrum svetovej kinematografie odohráva práve pred vašimi očami."
	},
	{
		"stanica": ["FilmBox-extra-HD"],
		"text": "Filmový kanál so širokou ponukou kvalitných filmov ocenených mnohými cenami, hollywoodskymi hitmi a dielami uznávanej európskej kinematografie je ideálny pre všetkých filmových nadšencov."
	},
	{
		"stanica": ["FilmBox-family"],
		"text": "Kanál, ktorý ponúka zážitok pre celú rodinu a vysiela tituly, ktoré oslovia nielen deti, ale aj dospelých."
	},
	{
		"stanica": ["FilmBox-plus"],
		"text": "Filmový kanál s bohatou ponukou mnohonásobne ocenených filmov, uznávanej európskej kinematografie a známych hollywoodskych hitov."
	},
	{
		"stanica": ["FilmBox-premium-HD"],
		"text": "Filmový kanál s bohatou ponukou mnohonásobne ocenených filmov, uznávanej európskej kinematografie a známych hollywoodskych hitov vo vysokom rozlíšení."
	},
	{
		"stanica": ["Food-network"],
		"text": "TV kanál plný relácií o potravinách a varení z kultúr po celom svete."
	},
	{
		"stanica": ["Ginx", "ginx"],
		"text": "Jediný medzinárodný kanál zameraný na počítačové hry."
	},
	{
		"stanica": ["Golf-channel"],
		"text": "Športový kanál, ktorý ponúka široký pohľad do sveta golfu vrátane priamych prenosov, dokumentov a spravodajstva."
	},
	{
		"stanica": ["Horor-film"],
		"text": "TV kanál zameraný výhradne na hororové filmové scénky. Vysiela každý deň od polnoci do 6-hod. rannej."
	},
	{
		"stanica": ["Kinosvet", "kinosvet"],
		"text": "Český dokumentárny televízny kanál."
	},
	{
		"stanica": ["LEO-tv"],
		"text": "TV program pre dospelých s erotickým zameraním."
	},
	{
		"stanica": ["Marc-Dorcel", "Marc-dorcel", "markdorcel"],
		"text": "TV program pre dospelých s erotickým zameraním."
	},
	{
		"stanica": ["Megamax", "megamax"],
		"text": "TV kanál zameraný na detské publikum od veku 7 do 14 rokov."
	},
	{
		"stanica": ["Mnam-tv"],
		"text": "TV stanica, ktorá divákom ponúka široké spektrum kulinárskych programov pre fanúšikov varenia"
	},
	{
		"stanica": ["Mnau-tv"],
		"text": "Zábavný program obsahujúci zábavné videá domácich miláčikov."
	},
	{
		"stanica": ["Mooz-dance", "Mooz-dance-HD"],
		"text": "Hudobný program, na ktorom si môžete užit rôzne populárne dance videoklipy bez reklám."
	},
	{
		"stanica": ["MTV-dance"],
		"text": "Stanica ponúkajúca najväčšie tanečné hity 24 hodín denne."
	},
	{
		"stanica": ["MTV-hits"],
		"text": "Stanica ponúkajúca najväčšie tanečné hity 24 hodín denne."
	},
	{
		"stanica": ["MTV-rocks"],
		"text": "TV kanál plný rockovej hudby."
	},
	{
		"stanica": ["Nick-jr.", "Nick-jr"],
		"text": "Detský televízny kanál plný dobrodružstiev."
	},
	{
		"stanica": ["Ocko-gold"],
		"text": "Česká hudobná stanica vysielajúca hudobné klipy rôznych žánrov, zábavné relácie a iné."
	},
	{
		"stanica": ["RTG-channel"],
		"text": "Dokumentárna stanica, ktorá predstavuje krásy a rôznorodosť ruskej kultúry, histórie, prírody a kuchyne."
	},
	{
		"stanica": ["Shorts"],
		"text": "Unikátny zábavný kanál plne venovaný krátkym filmom"
	},
	{
		"stanica": ["Stingray-brava-HD"],
		"text": "Hudobný program, ktorý je zameraný na klasickú hudbu, operu a a balet."
	},
	{
		"stanica": ["Stingray-jazz-HD"],
		"text": "Hudobný program, ktorý je určený pre miľovníkov hudobných štýlov ako sú jazz, soul alebo blues."
	},
	{
		"stanica": ["Sundance", "sundance"],
		"text": "TV kanál venovaný dokumentom, nezávislým hraným filmov, krátkym filmov a svetovej kinematografie."
	},
	{
		"stanica": ["Travelxp-HD"],
		"text": "Dokumentárny program, ktorý prináša tie najzaujímavejšie cestopisné a lifestylové programy z bezmála päťdesiatich krajín sveta, a to bez výnimky vlastnej produkcie."
	},
	{
		"stanica": ["TV-1000-russkoe-kino"],
		"text": "Filmový kanál, ktorý ponúka rôzne filmové žánre z rôznych kútov Európy."
	},
	{
		"stanica": ["Ukraine-tv", "Ukraine-tv"],
		"text": ""
	},
	{
		"stanica": ["Up-network"],
		"text": "Fascinujúci TV kanál ponúkajúci všetko podstatné pre priaznivcov letectva a lietadiel."
	},
	{
		"stanica": ["WAR-svet-valek"],
		"text": "TV kanál venovaný dokumentom hlavne z obdobia druhej svetovej vojny a priaznivcom vojenských stratégií."
	},
	{
		"stanica": ["XMO", "xmo"],
		"text": "Erotický kanál zameraný na vysielanie filmov a programov s gay tematikou."
	},
	//premiove baliky
	//HBO + Cinemax + HBO GO
	{
		"stanica": ["Cinemax", "cinemax"],
		"text": "Filmový kanál vysielajúci filmy nezávislej produkcie, zaujímavé art-ové novinky a diela z prestížnych festivalov."
	},
	{
		"stanica": ["Cinemax-2"],
		"text": "Doplnkový filmový kanál vysielajúci filmy nezávislej produkcie, zaujímavé art-ové novinky a diela z prestížnych festivalov."
	},
	{
		"stanica": ["HBO-2-SDHD", "HBO-2"],
		"text": "Doplnkový filmový kanál pre celú rodinu, ktorý umožňuje svoji divákom si vychutnať tie najväčšie filmové hity v pohodlí domova filmový, výber žánrov a času."
	},
	{
		"stanica": ["HBO-3-SDHD", "HBO-3"],
		"text": "Doplnkový filmový kanál pre celú rodinu, ktorý umožňuje svoji divákom si vychutnať tie najväčšie filmové hity v pohodlí domova filmový, výber žánrov a času  vo vysokom rozlíšení."
	},
	{
		"stanica": ["HBO-go"],
		"text": "Filmový kanál pre celú rodinu, ktorý umožňuje svoji divákom si vychutnať tie najväčšie filmové hity v pohodlí domova v tej najvyššej obrazovej kvalite."
	},
	{
		"stanica": ["HBO-kino"],
		"text": ""
	},
	{
		"stanica": ["HBO-SDHD", "HBO-HD"],
		"text": "Filmový kanál pre celú rodinu, ktorý umožňuje svoji divákom si vychutnať tie najväčšie filmové hity v pohodlí domova v tej najvyššej obrazovej kvalite."
	},
	{
		"stanica": ["HBO"],
		"text": "Filmový kanál pre celú rodinu, ktorý umožňuje svoji divákom si vychutnať tie najväčšie filmové hity v pohodlí domova."
	},
	//HD rodinny
	{
		"stanica": ["Animal-planet-HD"],
		"text": "Poznávací kanál, ktorý ponúka svojim divákom netradičný a zaujímavý pohľad na fascinujúci svet zvierat a rastlín na našej planéte."
	},
	{
		"stanica": ["Discovery-id-Xtra-HD"],
		"text": "TV kanál ponúkajúci tajomné a vzrušujúce príbehy zamerané predovšetkým na ženské publikum vo vysokom rozlíšení."
	},
	{
		"stanica": ["Discovery-science-HD"],
		"text": "Populárno-vzdelávací televízny program, ktorý prináša zaujímavé dokumenty o vede, vynálezoch, technike, modernom inžinierstve, ale aj dejinách a prírode vo vysokom rozlišení."
	},
	{
		"stanica": ["Discovery-showcase-HD"],
		"text": "Náučný kanál, ktorý prináša pútavé dokumenty zamerané na rôzne vynálezy, techniku a vedu a zaujímavé fakty z ľudského života."
	},
	{
		"stanica": ["Film-europe-HD"],
		"text": "Filmový kanál so zameraním na  všetky žánre filmov vo vysokom rozlíšení."
	},
	{
		"stanica": ["History-channel-HD"],
		"text": "Náučný kanál, ktorý netradičným a zaujímavým spôsobom približuje a objasňuje fakty z rôznych historických období sveta, ľudí a prírody."
	},
	{
		"stanica": ["Luxe-HD"],
		"text": "TV stanica, ktorá sa zameriava na luxus a všetko, čo s ním súvisí a v krátkych dokumentárnych šotoch rozoberá rôzne oblasti nadpriemerného materiálneho života."
	},
	{
		"stanica": ["MTV-HD"],
		"text": "Hudobná stanica, ktorá ponúka nielen to najlepšie zo súčasnej zahraničnej i domácej hudobnej scény, ale aj množstvo koncertov a reality show zo života, nielen, celebrít."
	},
	{
		"stanica": ["Shorts-HD", "shortshd"],
		"text": "Unikátny zábavný kanál plne venovaný krátkym filmom."
	},
	// HD sportovy
	{
		"stanica": ["Deutsche-welle"],
		"text": "Nemecká medzinárodná spravodajská TV stanica."
	},
	{
		"stanica": ["Digi-sport-1-HD"],
		"text": "Športový kanál s najlepšou futbalovou ponukou vo vysokom rozlíšení."
	},
	{
		"stanica": ["Digi-sport-2-HD"],
		"text": "Športový kanál s najlepšou futbalovou ponukou vo vysokom rozlíšení."
	},
	{
		"stanica": ["Digi-sport-3-HD"],
		"text": "Športový kanál s najlepšou futbalovou ponukou vo vysokom rozlíšení."
	},
	{
		"stanica": ["Digi-sport-4-HD"],
		"text": "Športový kanál s najlepšou futbalovou ponukou vo vysokom rozlíšení."
	},
	{
		"stanica": ["Digi-sport-5-HD"],
		"text": "Športový kanál s najlepšou futbalovou ponukou vo vysokom rozlíšení."
	},
	{
		"stanica": ["Eurosport-1-HD"],
		"text": "Obľúbený športový program, ktorý prináša priame prenosy a záznamy z mnohých športových odvetví -  futbal, atletika, tenis, cyklistika, plávanie, snooker a ďalšie vo vysokom rozlíšení."
	},
	{
		"stanica": ["Eurosport-2-HD"],
		"text": "Sesterský kanál kanálu Eurosport, ktorý vysiela športové prenosy, magazíny, športové  spravodajstvo z rôznych druhov športu vo vysokom rozlíšení."
	},
	{
		"stanica": ["Fuel-HD", "fuellhd"],
		"text": "Športový TV kanál zameraný na extrémne športy ako skateboarding, bmx, snowboarding, surfing, motocross a iné."
	},
	{
		"stanica": ["Golf-channel"],
		"text": "Športový kanál, ktorý ponúka široký pohľad do sveta golfu vrátane priamych prenosov, dokumentov a spravodajstva."
	},
	{
		"stanica": ["iConcerts-HD", "iconcertshd"],
		"text": "Hudobný kanál, ktorý prináša koncertné vystúpenia interpretov rôznych žánrov hudby a je to prvý a zatiaľ jediný program, ktorý sa špecializuje výhradne na živé koncertné vystúpenia."
	},
	{
		"stanica": ["MyZen-HD", "myzenhd"],
		"text": "Jedinečný program ponúkajúci rôzne relaxačné metódy obohatené príjemnou hudbou zen."
	},
	{
		"stanica": ["N-24"],
		"text": "Nemecký spravodajský TV kanál plný správ a a informačno-zábavného programu."
	},
	{
		"stanica": ["Nova-sport-1-HD"],
		"text": "Populárny športový televízny program, ktorý divákom ponúka nielen množstvo športov ako hokej, futbal, tenis, ale aj aktuálne športové spravodajstvo a športové magazíny."
	},
	{
		"stanica": ["Nova-sport-2-HD"],
		"text": "Populárny športový televízny program, ktorý divákom ponúka nielen množstvo športov ako hokej, futbal, tenis, ale aj aktuálne športové spravodajstvo a športové magazíny."
	},
	{
		"stanica": ["Outdoor-HD", "outdoorhd"],
		"text": "TV kanál určený pre miľovníkov outdoorových aktivít medzi ktoré patria poľovníctvo rybolov, jazdy v teréne alebo dobrodružné výpravy do divočiny."
	},
	{
		"stanica": ["Sport-1-HD"],
		"text": "Športový kanál s najširšou ponukou futbalu - exkluzívne prenosy z talianskej, španielskej, francúzskej, portugalskej, holandskej a škótskej ligy, ale aj hokeja, tenisu a golfu."
	},
	{
		"stanica": ["Sport-2-HD"],
		"text": "Športový kanál s najširšou ponukou futbalu - exkluzívne prenosy z talianskej, španielskej, francúzskej, portugalskej, holandskej a škótskej ligy, ale aj hokeja, tenisu a golfu."
	},
	{
		"stanica": ["Trace-sport-HD"],
		"text": "Športový program, v ktorom sa dozviete všetko o vašich obľúbených športových hviezdach, ich vášňach, ich záväzkoch, tajomstvách ich motivácie a úspechu."
	},
	{
		"stanica": ["TVE-international"],
		"text": "Španielsky spravodajský TV kanál obsahujúci zmes správ, diskusií a aktuálnych informácií."
	},
	//madarsky EXTRA
	{
		"stanica": ["Comedy-central", "Comedy-central-family"],
		"text": "Tv stanica vysielajúca tie najlepsie seriály a komediálne filmy."
	},
	{
		"stanica": ["Comedy-central-extra-HU", "Comedy-central-extra-hu"],
		"text": "Tv stanica vysielajúca tie najlepsie seriály a komediálne filmy."
	},
	{
		"stanica": ["Cool-tv"],
		"text": "Plnoformátová maďarská rodinná televízia s prevzatou a netradičnou vlastnou programovou tvorbou a seriálmi."
	},
	{
		"stanica": ["Duna", "duna"],
		"text": "Maďarská televízia so širokou ponukou filmov, magazínov, kultúry, spravodajstva a náučných dokumentov."
	},
	{
		"stanica": ["Duna-world"],
		"text": "Medzinárodná maďarská televízia, ktorá vysiela spravodajstvo zamerané na krásu maďarskej kultúry ako aj kultúru iných krajín."
	},
	{
		"stanica": ["Echo", "ECHO"],
		"text": "Maďarský televízny kanál so zameraním na vysielanie obchodných správ a verejných záležitostí."
	},
	{
		"stanica": ["FEM-3"],
		"text": "Maďarský televízny kanál poskytujúci novinky zo sveta módy a celebrít zameraný predovšetkým na ženy."
	},
	{
		"stanica": ["Film+-hu", "Film-plus-hu"],
		"text": "Filmový kanál, vysielajúci množstvo filmov rôznych žánrov americkej a európskej kinematografie pre celú rodinu spolu s televíznymi premiérami úspešných kinohitov."
	},
	{
		"stanica": ["M1", "m1"],
		"text": "Najsledovanejší kanál národnej televízie, ktorý vysiela spravodajstvo, regionálne spravodajstvo, filmy a seriály pre celú rodinu."
	},
	{
		"stanica": ["M2", "m2"],
		"text": "Druhý kanál maďarskej národnej televízie pre celú rodinu so zameraním na  spravodajstvo, zábavu, hudbu, film a šport."
	},
	{
		"stanica": ["M4-sport-HD", "M4-Sport"],
		"text": "Športový kanál s najlepšou futbalovou ponukou."
	},
	{
		"stanica": ["MTV-hu", "MTV-HU"],
		"text": "Filmový kanál, vysielajúci množstvo filmov rôznych žánrov americkej a európskej kinematografie pre celú rodinu spolu s televíznymi premiérami úspešných kinohitov."
	},
	{
		"stanica": ["Muzsika-tv","Muzsika-TV"],
		"text": "Hudobná Tv stanica."
	},
	{
		"stanica": ["Nickelodeon-hu","Nickelodeon-HU"],
		"text": "Detský televízny kanál vysielajúci množstvo animovaných a hraných rozprávok a seriálov pre deti."
	},
	{
		"stanica": ["PRO-4"],
		"text": ""
	},
	{
		"stanica": ["RTL+", "RTL-plus"],
		"text": "Plnoformátová maďarská rodinná televízia s prevzatou a netradičnou vlastnou programovou tvorbou a seriálmi."
	},
	{
		"stanica": ["RTL-2"],
		"text": "TV kanál zameraný na reality shows."
	},
	{
		"stanica": ["RTL-klub"],
		"text": "Plnoformátová maďarská televízna stanica pre celú rodinu so zameraním na spravodajstvo, zábavné relácie a seriály aj vlastnej výroby."
	},
	{
		"stanica": ["Slager-tv", "Slager-TV"],
		"text": "Maďarský hudobný kanál zameraný na hudbu 80. a 90. Rokov maďarských interpretov."
	},
	{
		"stanica": ["Sorozat+", "Sorozat-plus"],
		"text": "TV stanica vysielajúca telenovely, Tv shows po celý deň a noc."
	},
	{
		"stanica": ["Story-4", "story4"],
		"text": "Plnoformátova maďarská rodinná televízia."
	},
	{
		"stanica": ["Story-5", "story5"],
		"text": "Lifestylový kanál plný maďarských a zahraničných hviezd a klebiet zameraný na ženy."
	},
	{
		"stanica": ["Super-tv-2", "Super-TV-2"],
		"text": "Komerčný maďarský televízny kanál plný vzrušujúcich filmov a seriálov."
	},
	{
		"stanica": ["TV2","TV-2"],
		"text": "Plnoformátová maďarská rodinná televízia vysielajúca nielen zahraničnú, ale aj vlastnú tvorbu, filmy a seriály."
	},
	// spravodajsky
	{
		"stanica": ["Al-alam"],
		"text": "Arabský spravodajský kanál, ktorý sa venuje politike, podnikaniu, športu a iným domácim či zahraničným správam."
	},
	{
		"stanica": ["Al-jazeera-english"],
		"text": "Medzinárodný spravodajský TV kanál vysielajúci správy, dokumenty, živé debaty, aktuálne udalosti 24 hodín denne."
	},
	{
		"stanica": ["BBC-world-news"],
		"text": "Medzinárodný spravodajský TV kanál, ktorý poskytuje bohatú ponuku zo svet a správ , dokumentárnych filmov, programov z oblasti životného štýlu a udalostí zo sveta."
	},
	{
		"stanica": ["Bloomberg-eu"],
		"text": "Spravodajský TV kanál vysielajúci správy a aktuálne informácie zo sveta Spojených štátov amerických."
	},
	{
		"stanica": ["Canal-24-horas"],
		"text": "Španielsky spravodajský TV kanál vysielajúci správy po Španielsku a vo svete a to nepretržite 24 hodín denne."
	},
	{
		"stanica": ["France-24-en"],
		"text": "Francúzsky spravodajský kanál vysielajúci najaktuálnejšie spravodajstvo z celého sveta, ale aj rôzne publicistické relácie a diskusie."
	},
	{
		"stanica": ["France-24-fr"],
		"text": "Francúzsky spravodajský TV kanál vysielajúci medzinárodné správy 24 hodín denne."
	},
	{
		"stanica": ["Rai-news"],
		"text": "Taliansky spravodajský TV kanál, ponúkajúci nepretržitý obsah správ."
	},
	{
		"stanica": ["Russia-today"],
		"text": "TV kanál poskytujúci hodinové prehľady správ, dokumentárne programy, športové správy, diskusie a talk show vo vzťahu k Rusku."
	},
	{
		"stanica": ["TVP-info"],
		"text": "Poľský spravodajský TV kanál, ktorého ponuka obsahuje celoštátny prehľad správ a aktuálnych udalostí."
	},
	// dokumentarny
	{
		"stanica": ["Fishing-&-hunting", "Fishing-and-hunting"],
		"text": "Dokumentárny televízny kanál s množstvom zaujímavosti zo sveta rybolovu a poľovníctva."
	},
	{
		"stanica": ["National-Geographic-SDHD", "National-Geographic-HD"],
		"text": ""
	},
	{
		"stanica": ["National-Geographic-wild-SDHD", "National-geographic-wild"],
		"text": "Dokumentárno-náučný kanál s programom, ktorý prináša nové pohľady na vedu, históriu, prírodou a učí, ako žiť so záujmom."
	},
	{
		"stanica": ["Spektrum", "Spektrum-HD", "spektrumhd", "spektrum"],
		"text": "Dokumentárny kanál umožňujúci lepšie poznanie sveta vďaka bohatej programovej štruktúre s dôrazom na populárnu vedu, históriu, ale aj každodenný život."
	},
	{
		"stanica": ["Travel-channel-HD", "Travel-channel"],
		"text": "Inšpirujúci, informatívny a zábavný kanál, ktorý sprevádza divákov rôznymi kútmi planéty a ponúka jedinečný pohľad na cestovanie."
	},
	{
		"stanica": ["Viasat-explore", "Viasat-Explore"],
		"text": "Dokumentárny kanál, prostredníctvom ktorého môžu diváci objavovať a skúmať mnohé neznáme fakty z rôznych prostredí; zo zákulisia futbalu, rýchlych automobilov."
	},
	{
		"stanica": ["Viasat-history"],
		"text": "Dokumentárny kanál, na ktorom si diváci môžu vychutnať nielen množstvo kvalitných dokumentov z rôznych historických dôb, ale tiež biografie, dobové dokumenty a iné."
	},
	{
		"stanica": ["Viasat-nature"],
		"text": "Kanál, ktorý svojim divákom prináša zaujímavé a pútavé programy o zvieratách, prírode a živote v divočine v rôznych častiach sveta."
	},
	// filmovy
	{
		"stanica": ["AMC-HD", "AMC", "amc"],
		"text": "AMC predstavuje jednu z najpopulárnejších televíznych staníc, ktorá prináša divákom exkluzívne premiéry nových seriálov."
	},
	{
		"stanica": ["AXN", "axn"],
		"text": "Filmový kanál so zameraním predovšetkým na akčné filmy a seriály. ENG. Filmový kanál so zameraním predovšetkým na akčné filmy a seriály."
	},
	{
		"stanica": ["AXN-black"],
		"text": "Filmový kanál vysielajúci seriály a filmy sci-fi žánru."
	},
	{
		"stanica": ["AXN-white"],
		"text": "Filmový kanál vysielajúci seriály a filmy najmä krimi, akčného a dramatické žánru."
	},
	{
		"stanica": ["CS-FilmMini", "CS-Film"],
		"text": "TV kanál ponúkajúci české a slovenské filmy rovnako ako snímky zo zahraničnej produkcie."
	},
	{
		"stanica": ["FilmBox", "FIlmBox-HD", "filmbox"],
		"text": "Rodinný filmový kanál, ktorý ponúka množstvo zahraničných a českých filmov všetkých žánrov."
	},
	{
		"stanica": ["Film-europe"],
		"text": "Filmový kanál so zameraním na  všetky žánre filmov najmä európskej produkcie, ktoré sú vysielané v pôvodnom znení s titulkami."
	},
	{
		"stanica": ["JOJ-cinema-HD", "JOJ-Cinema"],
		"text": "TV kanál plný filmov nerušených reklamou a kinohitov 24 hodín denne."
	},
	//hudobny + detsky
	{
		"stanica": ["Deluxe-music-Channel-HD", "deluxe"],
		"text": "Nemecký televízny kanál ponúkajúci širokú škálu klipov a hudobných videí."
	},
	{
		"stanica": ["Disney-channel"],
		"text": "Detský kanál so širokou ponukou rozprávok, seriálov a  programov pre deti a mládež."
	},
	{
		"stanica": ["Disney-junior"],
		"text": "Detský kanál so širokou ponukou rozprávok, seriálov a  programov pre deti a mládež."
	},
	{
		"stanica": ["JimJam"],
		"text": "Detský kanál so zameraním predovšetkým na deti predškolského veku, ktorý podporuje ich fantáziu a tvorivosť."
	},
	{
		"stanica": ["Minimax", "minimax"],
		"text": "Televízny kanál určený pre deti predškolského a školského veku, ktorý vysiela množstvo animovaných rozprávok a seriálov."
	},
	{
		"stanica": ["MTV", "mtv"],
		"text": "Hudobná stanica, ktorá ponúka nielen to najlepšie zo súčasnej zahraničnej i domácej hudobnej scény, ale aj množstvo koncertov a reality show zo života, nielen, celebrít."
	},
	{
		"stanica": ["Nickelodeon", "nickleodeon"],
		"text": "Detský televízny kanál vysielajúci množstvo animovaných a hraných rozprávok a seriálov pre deti."
	},
	{
		"stanica": ["Ocko-tv"],
		"text": "Česká hudobná stanica vysielajúca hudobné klipy rôznych žánrov, zábavné relácie a iné."
	},
	{
		"stanica": ["VH1-classic"],
		"text": "To najlepšie z hitparády minulých rokov. Hity 50., 60., 70., a 80., rokov."
	},
	{
		"stanica": ["VH1-europe"],
		"text": "Americká hudobná stanica vysielajúca klipy rôznych hudobný žánrov pre všetky generácie."
	},
	// sportovy
	{
		"stanica": ["Eurosport-1-SDHD"],
		"text": "Obľúbený športový program, ktorý prináša priame prenosy a záznamy z mnohých športových odvetví -  futbal, atletika, tenis, cyklistika, plávanie, snooker a ďalšie vo vysokom rozlíšení."
	},
	{
		"stanica": ["Eurosport-2-SDHD"],
		"text": "Sesterský kanál kanálu Eurosport, ktorý vysiela športové prenosy, magazíny, športové  spravodajstvo z rôznych druhov športu vo vysokom rozlíšení."
	},
	{
		"stanica": ["Nova-sport-1-SDHD"],
		"text": "Populárny športový televízny program, ktorý divákom ponúka nielen množstvo športov ako hokej, futbal, tenis, ale aj aktuálne športové spravodajstvo a športové magazíny."
	},
	{
		"stanica": ["Nova-sport-2-SDHD"],
		"text": "Populárny športový televízny program, ktorý divákom ponúka nielen množstvo športov ako hokej, futbal, tenis, ale aj aktuálne športové spravodajstvo a športové magazíny."
	},
	{
		"stanica": ["Sport-1-SDHD"],
		"text": "Športový kanál s najširšou ponukou futbalu - exkluzívne prenosy z talianskej, španielskej, francúzskej, portugalskej, holandskej a škótskej ligy, ale aj hokeja, tenisu a golfu."
	},
	{
		"stanica": ["Sport-2-SDHD"],
		"text": "Športový kanál s najširšou ponukou futbalu - exkluzívne prenosy z talianskej, španielskej, francúzskej, portugalskej, holandskej a škótskej ligy, ale aj hokeja, tenisu a golfu."
	},
	// styl a fakty
	{
		"stanica": ["Animal-planet", "Animal-Planet"],
		"text": "Poznávací kanál, ktorý ponúka svojim divákom netradičný a zaujímavý pohľad na fascinujúci svet zvierat a rastlín na našej planéte."
	},
	{
		"stanica": ["CBS-reality"],
		"text": "TV kanál inšpirovaný skutočnými životmi plný špeciálnych, reálnych a nepredvídateľných programov. Kanál ponúka jedinečný náhľad do životov cez výbornú kombináciu kvalitných dokumentárnych filmov a seriálov."
	},
	{
		"stanica": ["Discovery-channel-HD", "Discovery-channel", "Discovery"],
		"text": "Náučný kanál, ktorý prináša pútavé dokumenty zamerané na rôzne vynálezy, techniku a vedu a zaujímavé fakty z ľudského života."
	},
	{
		"stanica": ["Discovery-ID"],
		"text": "TV kanál ponúkajúci tajomné a vzrušujúce príbehy zamerané predovšetkým na ženské publikum."
	},
	{
		"stanica": ["TLC", "tlc"],
		"text": "Televízia s programom o reálnych ľudoch. Uvidíte mnohé životné situácie a prekvapenia, ktoré musíte vidieť!"
	},
	{
		"stanica": ["Eroxxx-HD", "Eroxxx"],
		"text": "TV program pre dospelých s erotickým zameraním vo vysokom rozlíšení."
	},
	{
		"stanica": ["Eurosport-1"],
		"text": "Obľúbený športový program, ktorý prináša priame prenosy a záznamy z mnohých športových odvetví -  futbal, atletika, tenis, cyklistika, plávanie, snooker a ďalšie."
	},
	{
		"stanica": ["History-channel-HD", "History-channel"],
		"text": "Náučný kanál, ktorý netradičným a zaujímavým spôsobom približuje a objasňuje fakty z rôznych historických období sveta, ľudí a prírody vo vysokom rozlíšení."
	},
	{
		"stanica": ["Spektrum-home"],
		"text": "Televízny kanál určený nielen pre domácich kutilov, ktorý ponúka množstvo užitočných rád pre starostlivosť o byt, dom, záhradu."
	},
	{
		"stanica": ["TV-paprika", "paprika"],
		"text": "Kulinársky televízny kanál ponúkajúci množstvo receptov, kuchárskych tipov a trikov a zaujímavosti z gastronómie z celého sveta."
	},
	//volny cas
	{
		"stanica": ["Arirang"],
		"text": "Kórejská televízna stanica so zameraním na rodinu."
	},
	{
		"stanica": ["Ceskoslovensko-festival"],
		"text": "Kanál pre všetkých filmových nadšencov, ktorý vysiela tie najlepšie československé filmy zo 60.-80. rokov."
	},
	{
		"stanica": ["Fine-living"],
		"text": "Kanál so zameraním na životný štýl, Zameriava sa na témy ako domácnosť, varenie, cestovanie, životný štýl a zdravie."
	},
	{
		"stanica": ["France-24"],
		"text": "Francúzsky spravodajský kanál vysielajúci najaktuálnejšie spravodajstvo z celého sveta, ale aj rôzne publicistické relácie a diskusie."
	},
	{
		"stanica": ["KBS-world"],
		"text": "Kórejská televízna stanica so zameraním na rodinu."
	},
	{
		"stanica": ["Slagr-tv"],
		"text": "Československá ľudová televízia ponúkajúca pestrý a rôznorodý program zameraný hlavne na dychovky a ľudovky."
	},
	{
		"stanica": ["TV-8"],
		"text": "Monotypová televízia so zameraním na audiotextové relácie, pričom dominujú rôzne interaktívne hry, veštiarne a teleshopping."
	},
	{
		"stanica": ["Viasat-epic-drama"],
		"text": "Filmový a seriálový kanál, vysielajúci množstvo historických dobových drám vo vysokom rozlíšení."
	},
	{
		"stanica": ["Viasat-naturehistory-HD", "Viasat-naturehistoryHD"],
		"text": "Dokumentárny kanál, na ktorom si diváci môžu vychutnať nielen množstvo kvalitných dokumentov z rôznych historických dôb, ale tiež biografie, dobové dokumenty a iné."
	},
	// Magio TV SAT
	// magio SAT L
	{
		"stanica": ["CCTV-9-documentary-", "CCTV-9-documentary"],
		"text": ""
	},
	{
		"stanica": ["CCTV-news-", "CCTV-news"],
		"text": ""
	},
	{
		"stanica": ["CNN-intl.-europe"],
		"text": "Spravodajský kanál, prinášajúci top aktuality zo sveta."
	},
	{
		"stanica": ["Credo-tv-", "Credo-tv"],
		"text": "Rumunský televízny kanál ponúkajúci širokú škálu klipov a hudobných videí."
	},
	{
		"stanica": ["CS-filmcs-minics-horror", "CS-filmcs-minics-horror"],
		"text": ""
	},
	{
		"stanica": ["Gospel-channel"],
		"text": "Televízia zameraná najmä na tematiku duchovných a ľudských hodnôt."
	},
	{
		"stanica": ["Publica-tv-", "Publica-tv"],
		"text": ""
	},
	{
		"stanica": ["RTV-", "rtv"],
		"text": "Spravodajský kanál, prinášajúci top aktuality zo sveta."
	},
	{
		"stanica": ["SKY-news-", "SKY-news"],
		"text": "Spravodajský kanál, prinášajúci top aktuality zo sveta."
	},
	{
		"stanica": ["Super-one"],
		"text": "TV program pre dospelých s erotickým zameraním vo vysokom rozlíšení."
	},
	{
		"stanica": ["TV-sonce"],
		"text": ""
	},
	{
		"stanica": ["uTV-", "uTV"],
		"text": ""
	},
	{
		"stanica": ["VH1", "vh1"],
		"text": "Americká hudobná stanica vysielajúca klipy rôznych hudobný žánrov pre všetky generácie."
	}
];

generateQtip(qtipdata);

function generateQtip(qtipdata) {

  var myTimer = setInterval(function() {

    if ($('.programovyBalikHolder img').length > 2 && $('.programovyBalikStyle img').length || $('.local img').length) {
      clearInterval(myTimer);
    }

    for (var a = 0; a < qtipdata.length; a++) {
      // img stanice
      var tvStanicaImg = $('.programovyBalikHolder img, .programovyBalikStyle img, .local img');
      //console.log(tvStanicaImg[a])

      // zisti "alt" pre kazdu TV stanicu
      $.each(tvStanicaImg, function() {
        var attrAlt = $(this).attr('alt');
        //console.log(attrAlt)

        var qtipText = $(this);

        //ak alt = qtipdata[a].stanica vytvor qtip
        if (attrAlt == qtipdata[a].stanica[0] || attrAlt == qtipdata[a].stanica[1] || attrAlt == qtipdata[a].stanica[2] || attrAlt == qtipdata[a].stanica[3] || attrAlt == qtipdata[a].stanica[4] || attrAlt == qtipdata[a].stanica[5]) {
					// vytvor text pre kazdu TV stanicu
					$(qtipText).attr('data-text', qtipdata[a].text);
          //create new qtip
          $(this).qtip({
            content: {
              attr: 'data-text' //display tooltip text from data-text attribute
            },
            style: {
              classes: 'qtip-tipsy'
            },
            position: {
              my: 'bottom center',
              at: 'top center'
            }
          })
        }
      });
    }

    //console.log(qtipdata[a].stanica);
  }, 300)
}
