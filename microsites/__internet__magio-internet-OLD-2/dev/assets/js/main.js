$(function() {
  $("#nav-sticky").sticky({
    topSpacing: 0,
    widthFromWrapper: true
  });

  $(".scroll-to").click(function() {
    if (
      location.pathname.replace(/^\//, "") ==
        this.pathname.replace(/^\//, "") &&
      location.hostname == this.hostname
    ) {
      var target = $(this.hash);
      target = target.length ? target : $("[name=" + this.hash.slice(1) + "]");
      if (target.length) {
        $("html,body").animate(
          {
            scrollTop:
              target.offset().top - ($("#nav-sticky").outerHeight() - 10)
          },
          1000
        );
        return false;
      }
    }
  });

  var contentSections = $("[id*='sec-']"),
    secondaryNav = $("#nav-sticky");

  function updateSecondaryNavigation() {
    contentSections.each(function() {
      var actual = $(this),
        actualHeight =
          actual.height() +
          parseInt(actual.css("paddingTop").replace("px", "")) +
          parseInt(actual.css("paddingBottom").replace("px", "")),
        actualAnchor = secondaryNav.find(
          'a[href="#' + actual.attr("id") + '"]'
        );

      if (
        actual.offset().top - secondaryNav.outerHeight() <=
          $(window).scrollTop() &&
        actual.offset().top + actualHeight - secondaryNav.outerHeight() >
          $(window).scrollTop()
      ) {
        actualAnchor.addClass("active");
      } else {
        actualAnchor.removeClass("active");
      }
    });
  }

  updateSecondaryNavigation();

  $(window).scroll(function(event) {
    updateSecondaryNavigation();
  });

  $(".fancy").fancybox({});

  $(".tooltip").click(function(event) {
    event.preventDefault();
  });

  var targets = $("[rel~=tooltip]"),
    target = false,
    tooltip = false,
    title = false;

  targets.bind("mouseenter", function() {
    target = $(this);
    tip = target.attr("title");
    tooltip = $('<div id="tooltip"></div>');

    if (!tip || tip == "") return false;

    target.removeAttr("title");
    tooltip
      .css("opacity", 0)
      .html(tip)
      .appendTo("body");

    var init_tooltip = function() {
      if ($(window).width() < tooltip.outerWidth() * 1.5)
        tooltip.css("max-width", $(window).width() / 2);
      else tooltip.css("max-width", 340);

      var pos_left =
          target.offset().left +
          target.outerWidth() / 2 -
          tooltip.outerWidth() / 2,
        pos_top = target.offset().top - tooltip.outerHeight() - 20;

      if (pos_left < 0) {
        pos_left = target.offset().left + target.outerWidth() / 2 - 20;
        tooltip.addClass("left");
      } else tooltip.removeClass("left");

      if (pos_left + tooltip.outerWidth() > $(window).width()) {
        pos_left =
          target.offset().left -
          tooltip.outerWidth() +
          target.outerWidth() / 2 +
          20;
        tooltip.addClass("right");
      } else tooltip.removeClass("right");

      if (pos_top < 0) {
        var pos_top = target.offset().top + target.outerHeight();
        tooltip.addClass("top");
      } else tooltip.removeClass("top");

      tooltip
        .css({
          left: pos_left,
          top: pos_top
        })
        .animate(
          {
            top: "+=10",
            opacity: 1
          },
          50
        );
    };

    init_tooltip();
    $(window).resize(init_tooltip);

    var remove_tooltip = function() {
      tooltip.animate(
        {
          top: "-=10",
          opacity: 0
        },
        50,
        function() {
          $(this).remove();
        }
      );

      target.attr("title", tip);
    };

    target.bind("mouseleave", remove_tooltip);
    tooltip.bind("click", remove_tooltip);
  });

  $(".popup").fancybox({
    padding: 10,
    margin: 0,
    parent: "#content",
    closeBtn: false,
    helpers: {
      overlay: {
        css: {
          background: "rgba(0, 0, 0, 0.7)"
        }
      }
    }
  });

  $(".close-end a").click(function() {
    $.fancybox.close();
  });

  /* SHOW LEGAL */
  $("#show_legal").click(function() {
    $(this).hide();
    $("#legal_more").show(200);
  });

  /* SEC-AKCIE LOAD CONTENT */
  $(".sec-akcie").load("templates/_aktualne-akcie.html?v=2", function() {});

  /* TOOLTIPS */
  $(".tooltip0").qtip({
    content:
      "Internet cez optickú sieť je závislý od dostupnosti vo vašej lokalite. Inak je Magio internet dostupný cez technológiu VDSL, prípadne ADSL.",
    style: {
      "font-size": 12
    },
    position: {
      corner: {
        target: "leftMiddle",
        tooltip: "topMiddle"
      }
    }
  });

  $(".tooltip1").qtip({
    content:
      "Rýchlosť internetu je závislá od technológie, ktorá je dostupná na adrese zriadenia služby.<br>Technológie, ktoré poskytujeme:<br>Optika: do 30/3 Mb/s<br>Optika k domu - VDSL: do 10/1 Mb/s<br>ADSL: do 4/0,5 Mb/s",
    style: {
      "font-size": 12
    },
    position: {
      corner: {
        target: "leftMiddle",
        tooltip: "topMiddle"
      }
    }
  });

  $(".tooltip3").qtip({
    content:
      "Rýchlosť internetu je závislá od technológie, ktorá je dostupná na adrese zriadenia služby.<br>Technológie, ktoré poskytujeme:<br>Optika: do 120/12 Mb/s<br>Optika k domu - VDSL: do 40/4 Mb/s<br>ADSL: do 8/1 Mb/s",
    style: {
      "font-size": 12
    },
    position: {
      corner: {
        target: "leftMiddle",
        tooltip: "topMiddle"
      }
    }
  });

  $(".tooltip4").qtip({
    content:
      "Rýchlosť internetu je závislá od technológie, ktorá je dostupná na adrese zriadenia služby.<br>Technológie, ktoré poskytujeme:<br>Optika: do 600/60 Mb/s<br>Optika k domu - VDSL: do 90/9 Mb/s<br>ADSL: do 15/1 Mb/s",
    style: {
      "font-size": 12
    },
    position: {
      corner: {
        target: "leftMiddle",
        tooltip: "topMiddle"
      }
    }
  });
  $(".tooltip2").qtip({
    content:
      "Vezmite si k Magio internetu aj Magio televíziu a/alebo Pevnú linku a získate Chytrý balík služieb za výhodnejšu cenu.<br><br>Pokračujte zeleným tlačidlom VYBRAŤ a navoľte si balík podľa svojich predstáv.",
    style: {
      "font-size": 12
    },
    position: {
      corner: {
        target: "leftMiddle",
        tooltip: "topMiddle"
      }
    }
  });
  $("#naj_znacka_m").qtip({
    content:
      "Vaša dôvera je u nás na prvom mieste.<br>Magio internet a televízia sú víťazmi Ankety o najdôveryhodnejšiu značku v oblasti poskytovateľov internetových a televíznych služieb za rok 2018, ktorú uskutočnila nezávislá agentúra Nielsen.",
    style: {
      "text-align": "center"
    },
    //position: {
    //    corner: {
    //        target: 'rightMiddle',
    //        tooltip: 'topMiddle'
    //   }
    //}
    position: {
      my: "left top",
      at: "right center"
    }
  });
});
