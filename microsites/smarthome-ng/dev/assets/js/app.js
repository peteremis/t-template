angular.module("smartHome", ['ngRoute'])
  //APP CONFIG - ROUTING
  .config(function($routeProvider, $locationProvider) {

    $routeProvider
      .when('/', {
        template: '<smarthome-index></smarthome-index>'
      })
      .when('/zakladny-balik', {
        template: '<zakladny-balik></zakladny-balik>'
      })
      .when('/instalacia', {
        template: '<instalacia></instalacia>'
      })
      .otherwise({
        redirectTo: '/'
      });

    $locationProvider
      .hashPrefix('');
  });
