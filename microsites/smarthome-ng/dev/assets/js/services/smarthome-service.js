angular
  .module('smartHome')
  .factory('smarthomeService', http);

  http.$inject = ['$http'];
  function http($http) {

    return $http.get('assets/js/data.json')
      .then(function(data) {
        return data;
      })

  };
