$(function() {


  /* STICKY NAV START */
   $(window).scroll(function () {
        var $this = $(this),
            $NavSticky = $('.navsticky');
        if ($this.scrollTop() > 482) {
           $NavSticky.css({'position':'fixed','top':'0'});
        } else {
           $NavSticky.css({'position':'relative'});
        }
    });
    /* STICKY NAV END */

  $(document).on('click', '.scroll-to', function () {

      var id_comp = $(this).attr('data-id');
      var target = $('#' + id_comp); //vrati #sec- po kliknuti na NAV item

          target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
          if (target.length) {
              $('html,body').animate({
                  scrollTop: target.offset().top - ($("#nav-sticky").height()) //zoscrolluj na sekciu a odrataj vysku NAV
              }, 1000);
              return false;
          }

  });

  /* TOGGLE ARROW */
$(document).on('click', '.toggle-arrow, .arrow-right', function(event) {
  event.preventDefault();
  if ($(this).hasClass('arrow-right')) {
    return;
  }
  $(event.target).closest('.togleArrow').find('.arrow-content').toggle(200);
  $(this).find('.arrow-right').toggleClass('arrow-rotate');
});
/* TOGGLE ARROW END */

});
