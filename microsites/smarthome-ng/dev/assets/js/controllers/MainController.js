angular
  .module('smartHome')
  .component('smarthomeIndex', {
    templateUrl: '../views/home.html',
    controller: MainController,
    controllerAs: '$ctrl'
  });

MainController.$inject = ['$scope', '$routeParams', 'smarthomeService', '$timeout'];

function MainController($scope, $routeParams, smarthomeService, $timeout) {


  /* GET DATA FOR INDEX PAGE - START */
  smarthomeService.then(function(indexData) {
    $scope.indexData = indexData.data;

  });
  /* GET DATA FOR INDEX PAGE - END */

};
