$(window).load(function () {
  const queryString = window.location.search;
  const urlParams = new URLSearchParams(queryString);
  const kioskMode = urlParams.get("kiosk");
  console.log(kioskMode);

  if (kioskMode == 1) {
    $(".videowrapper").hide();
    $("video").removeAttr("autoplay");
    $("video").removeAttr("loop");

    $(".sec-6").remove();
  }

  $("#nav-sticky").sticky({
    topSpacing: 0,
    widthFromWrapper: true,
  });

  $(".scroll-to").click(function () {
    if (
      location.pathname.replace(/^\//, "") ==
        this.pathname.replace(/^\//, "") &&
      location.hostname == this.hostname
    ) {
      var target = $(this.hash);
      target = target.length ? target : $("[name=" + this.hash.slice(1) + "]");
      if (target.length) {
        $("html,body").animate(
          {
            scrollTop:
              target.offset().top - ($("#nav-sticky").outerHeight() - 10),
          },
          1000
        );
        return false;
      }
    }
  });

  var contentSections = $("[id*='sec-']"),
    secondaryNav = $("#nav-sticky");

  function updateSecondaryNavigation() {
    contentSections.each(function () {
      var actual = $(this),
        actualHeight =
          actual.height() +
          parseInt(actual.css("paddingTop").replace("px", "")) +
          parseInt(actual.css("paddingBottom").replace("px", "")),
        actualAnchor = secondaryNav.find(
          'a[href="#' + actual.attr("id") + '"]'
        );

      if (
        actual.offset().top - secondaryNav.outerHeight() <=
          $(window).scrollTop() &&
        actual.offset().top + actualHeight - secondaryNav.outerHeight() >
          $(window).scrollTop()
      ) {
        actualAnchor.addClass("active");
      } else {
        actualAnchor.removeClass("active");
      }
    });
  }

  updateSecondaryNavigation();

  $(window).scroll(function (event) {
    updateSecondaryNavigation();
  });

  /* CAROUSEL START */
  //ukaz obsah na zaklade nastavenia slick initalSlide
  $("#program_slider").on("init reInit", function (event, slick) {
    //console.log(parseInt(slick.currentSlide + 1) + ' / ' + slick.slideCount);
    $(".content").hide();
    $(".content[data-id=" + (slick.currentSlide + 1) + "]").show(); //zobraz .content prisluchajuci pre initialSlide: 1
  });

  $("#app_slider").slick({
    centerMode: true,
    centerPadding: "0%",
    arrows: true,
    slidesToShow: 1,
    dots: true,
    infinite: false,
    speed: 400,
    cssEase: "linear",
    adaptiveHeight: true,
    variableWidth: false,
    swipeToSlide: true,
    swipe: true,
    autoplay: false,
    initialSlide: 0,
    touchMove: true,
    draggable: true,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          arrows: true,
          centerMode: true,
          //centerPadding: '40px',
          slidesToShow: 1,
        },
      },
      {
        breakpoint: 543,
        settings: {
          arrows: false,
          centerMode: true,
          //centerPadding: '40px',
          slidesToShow: 1,
        },
      },
    ],
  });

  // po kliknuti na sipky slidera ukaz .content patriaci k currentSlide
  $("#program_slider").on("afterChange", function (event, slick, currentSlide) {
    $(".content").hide();
    $(".content[data-id=" + (currentSlide + 1) + "]").show();
  });
  /* CAROUSEL END */

  /* TOGGLE ARROW */
  $(".toggle-arrow").click(function (event) {
    event.preventDefault();
    $(event.target).next("div.arrow-content").toggle(200);
    $(this).find(".arrow-right").toggleClass("arrow-rotate");
  });
  /* TOGGLE ARROW END */

  /* TABS */
  $(".tabs-menu a").click(function (event) {
    event.preventDefault();
    $(this).parent().addClass("current");
    $(this).parent().siblings().removeClass("current");
    var tab = $(this).attr("href"), // select clicked tab. In example "#tab-1" ..
      parent = $(this).closest("ul").parent().parent().parent(); // <tabs-container>
    parent.find(".tab-content").not(tab).css("display", "none"); //hide all not clicked tabs
    $(tab).fadeIn(); //show clicked tab
  });
  /* TABS END */

  /* FROM TABS TO ACCORDION */
  // var dataSegment = $("[data-segment]").each(function(){
  //     var closestHead = $($(this).find('.tabs-menu a'));
  //         closestItemCount = closestHead.length - 1; //number of tabs in <section> with attr "data-segment"
  //         closestContent = $($(this).find('.tab-content'));
  //         closestAccordion = $(this).find('.accordion');
  //         accordionItem = '';

  //     for (var i = 0; i <= closestItemCount; i++) {
  //         var tabID = $(closestContent[i]).attr('id');
  //         accordionItem += ('<div class="item '+tabID+'">' +
  //                             '<div class="heading">' + $(closestHead[i]).text()  + '</div>');
  //         accordionItem += ('<div class="content">' + $(closestContent[i]).html() + '</div>' +
  //                             '</div>');

  //         // if (i !== closestItemCount) {
  //         //     accordionItem += '<div class="item">';
  //         // }
  //     }

  //     //if data-segment and data-accordion value match, show accordion data
  //     if($(this).attr('data-segment') === closestAccordion.attr('data-accordion')) {
  //         $(accordionItem).appendTo(closestAccordion);
  //     }

  //     var $items = $('.accordion .item');
  //     //SET OPENED ITEM
  //     $($items[0]).addClass('open');

  // });

  //   $(".accordion .item .heading").click(function(e) {
  //     var clickedHead = e.target;
  //     var $item = $(clickedHead).closest(".item");
  //     var isOpen = $item.hasClass("open");
  //     var $content = $item.find(".content");
  //     var $acPrice = $(clickedHead).find(".ac-price");

  //     if (isOpen) {
  //       $content.slideUp(200);
  //       $item.removeClass("open");
  //       $acPrice.show();
  //     } else {
  //       $content.slideDown(200);
  //       $item.addClass("open");
  //       $acPrice.hide();
  //     }
  //   });

  /* scroll to selected tab - akcie a novinky */
  function scrollTo_NewsAndActions(actionID) {
    if (
      window.location.hash.substring(1).split("?")[0] === actionID &&
      actionID !== ""
    ) {
      var parentSection = $("#" + actionID).closest("section");
      var mobileItemToOpen = $(".item." + actionID).find(".heading");

      var mobileItem = $(".item");
      mobileItem.each(function (index) {
        $(this).removeClass("open");
      });

      if (actionID === "sutaz") {
        var offset = 0;
      } else {
        var offset = $("#nav-sticky").outerHeight() - 5;
      }

      $("html,body")
        .animate(
          {
            scrollTop: parentSection.offset().top - offset,
          },
          1000
        )
        .promise()
        .done(function () {
          if ($(window).width() >= 0) {
            //old value = 600
            parentSection.find("a[href=" + "#" + actionID + "]").click(); //open tab on desktop
          } else {
            $(mobileItemToOpen).click(); // open tab on mobile
          }
        });
    }
  }
  scrollTo_NewsAndActions(window.location.hash.substring(1).split("?")[0]);

  /* youtube video player */
  $(".youtube-video").jqueryVideoLightning({
    autoplay: 1,
    backdrop_color: "#000",
    backdrop_opacity: 0.6,
    glow: 0,
    glow_color: "#000",
  });

  // generate heapBoxex -> generate both versions, for mob and desktop
  var jsonValues =
    '[{"value":"ano","text":"s paušálmi ÁNO"},{"value":"anoBiznis","text":"s paušálmi ÁNO Biznis"},{"value":"happy","text":"s paušálmi Happy"}, {"value":"bezZavazkov","text":"k programu Bez záväzkov"}, {"value":"sEasyKartou","text":"s Easy Kartou"}]';
  var mobileHeapbox = $(".accordion .plandatainfo-selectbox-mobile");
  var desktopHeapbox = $(".plandatainfo-selectbox");

  function generateHeapbox(heapboxWrapperClass) {
    heapboxWrapperClass.heapbox({
      onChange: function (val, elm) {
        var items = $(".plandatainfo-details");
        if (val) {
          items.hide();
          $(".plandatainfo-details." + val).show();
        }
      },
    });
    heapboxWrapperClass.heapbox("set", jsonValues);
  }

  if ($(document).width() <= 543) {
    generateHeapbox(mobileHeapbox);
    mobileHeapbox.prev(".heapBox").show();
  } else {
    generateHeapbox(desktopHeapbox);
  }

  /* start <video> on page load */
  var video = $("video");
  for (var i = 0; i < video.length; i++) {
    video[i].muted = true;
    video[i].loop = true;
    video[i].play();
  }

  var jsonDataNavyse =
    '[{"value":"S","text":"S paušálom ÁNO S"},{"value":"M","text":"S paušálom ÁNO M"},{"value":"M_DATA","text":"S paušálom ÁNO M Dáta"},{"value":"L","text":"S paušálom ÁNO L"},{"value":"XL","text":"S paušálom ÁNO XL"},{"value":"XXL","text":"S paušálom ÁNO XXL"}]';
  $("#heapbox_giveaway").show();
  $("#giveaway").change(function () {
    var val = $(this).val();
    var giveAwayTextHolder = $(".giveawayText span");
    switch (val) {
      case "S":
        giveAwayTextHolder.text("250 MB");
        break;
      case "M":
        giveAwayTextHolder.text("500 MB");
        break;
      case "M_DATA":
        giveAwayTextHolder.text("4 GB");
        break;
      case "L":
        giveAwayTextHolder.text("5 GB");
        break;
      case "XL":
        giveAwayTextHolder.text("10 GB");
        break;
      case "XXL":
        giveAwayTextHolder.text("15 GB");
        break;
      default:
        giveAwayTextHolder.text("250 MB");
        break;
    }
  });
  $("#giveaway").heapbox("set", jsonDataNavyse);
});
