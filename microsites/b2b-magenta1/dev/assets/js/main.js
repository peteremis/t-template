$(document).ready(function () {

    if (getParameterByName('vianocne-akcie')) {
        scrollTo('.c-row-3', 0);
    }

    function getParameterByName(name) {
        var found = false;
        if (window.location.href.indexOf(name) > -1) {
            found = true;
        }

        return found;
    }

    if (getParameterByName('biznis')) {
        $('#rodina1').text('VO FIRME');
        $('#rodina2').text('CHCEM PAUĹ ĂL DO FIRMY').attr("href", "https://www.telekom.sk/objednavka/-/scenario/e-shop/chytry-balik-biznis");
        $('#rodina3').text('PAUĹ ĂLY DO FIRMY').attr("href", "https://www.telekom.sk/objednavka/-/scenario/e-shop/chytry-balik-biznis");

        $('#rodina4').text('v rĂˇmci firmy pre vĂˇs aj vaĹˇich zamestnancov a to aj s Easy Pecka');
        $('#rodina5').text('zamestnancami');
        $('#rodina6').text('vaĹˇich zamestnancov');
        $('#rodina7').text('firmy');

        $('#magenta1-sluzby').attr("href", "https://www.telekom.sk/objednavka/-/scenario/e-shop/chytry-balik-biznis");
        $('#osobne').css('color', '#000');
        $('#biznisoffice').show();
        $("#row-2-ec1").attr("src", "/documents/10179/538197/eyecatcher_biznis_100.png");
        $("#row-2-ec2").attr("src", "/documents/10179/538197/eyecatcher_biznis_50.png");
    } else {
        $('#rodina1').text('V RODINE');
        $('#business').css('color', '#E20074');
    }

    $('#left-happy-xs').qtip({
        content: $('#left-happy-xs-content')
    });

    $('#left-happy-m').qtip({
        content: $('#left-happy-m-content')
    });

    $('#left-happy-l').qtip({
        content: $('#left-happy-l-content')
    });

    $('#left-happy-xl2').qtip({
        content: $('#left-happy-xl2-content')
    });

    $('#left-happy-xl').qtip({
        content: $('#left-happy-xl-content')
    });

    $('#info-eset').qtip({
        content: $('#info-eset-content')
    });

    $('#right-happy-xldata').qtip({
        content: $('#right-happy-xldata-content')
    });

    $('#right-happy-xl2').qtip({
        content: $('#right-happy-xl2-content')
    });

    $('#right-happy-l').qtip({
        content: $('#right-happy-l-content')
    });

    $('#right-happy-xl').qtip({
        content: $('#right-happy-xl-content')
    });

    $('#row-1-order').click(function () {
        scrollTo('.c-row-2', 0);
        return false;
    });

    var scrollingSpeed = 1200;

    function scrollTo(id, height) {
        height = height ? height : 0;
        $('html,body').animate({
                scrollTop: $(id).offset().top - height
            },
            scrollingSpeed,
            function () {
                disabled = false;
            }
        );
    }

    $("#nav-sticky").sticky({
        topSpacing: 0,
        widthFromWrapper: true
    });

    $(".scroll-to").click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top - ($("#nav-sticky").outerHeight())
                }, 1000);
                return false;
            }
        }
    });

    var contentSections = $("[id*='sec-']"),
        secondaryNav = $("#nav-sticky");

    function updateSecondaryNavigation() {
        contentSections.each(function () {
            var actual = $(this),
                actualHeight = actual.height() + parseInt(actual.css('paddingTop').replace('px', '')) + parseInt(actual.css('paddingBottom').replace('px', '')),
                actualAnchor = secondaryNav.find('a[href="#' + actual.attr('id') + '"]');

            if ((actual.offset().top - secondaryNav.outerHeight() <= $(window).scrollTop()) && (actual.offset().top + actualHeight - secondaryNav.outerHeight() > $(window).scrollTop())) {
                actualAnchor.addClass('active');
            } else {
                actualAnchor.removeClass('active');
            }

        });
    }

    updateSecondaryNavigation();

    $(window).scroll(function (event) {
        updateSecondaryNavigation();
    });


    $('.benefit1').qtip({
        content: 'Bonus na paušál vám uplatníme formou zľavy z mesačného poplatku vo výške 7,50 € počas 20 mesiacov. Bonus na telefón vo výške 50 € dostanete okamžite pri prenose čísla formou zľavy z kúpnej ceny telefónu.',
        position: {
            corner: {
                target: 'leftMiddle',
                tooltip: 'topMiddle'
            }
        }
    })

    $('.benefit2').qtip({
        content: 'Bonus na paušál vám uplatníme formou zľavy z mesačného poplatku vo výške 5 € počas 20 mesiacov. Bonus na telefón vo výške 50 € dostanete okamžite pri prenose čísla formou zľavy z kúpnej ceny telefónu.',
        position: {
            corner: {
                target: 'leftMiddle',
                tooltip: 'topMiddle'
            }
        }
    })
});
