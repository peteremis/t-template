$(function() {

  /* scroll to + sticky nav */
  $("#nav-sticky").sticky({
      topSpacing: 0,
      widthFromWrapper: true
  });

  $(".scroll-to").click(function () {
      if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
          var target = $(this.hash); //vrati #sec- po kliknuti na NAV item
          target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
          if (target.length) {
              $('html,body').animate({
                  scrollTop: target.offset().top - ($("#nav-sticky").height()) //zoscrolluj na sekciu a odrataj vysku NAV
              }, 1000);
              return false;
          }
      }
  });

  var contentSections = $("[id*='sec-']"),
      secondaryNav = $("#nav-sticky");

  function updateSecondaryNavigation() {
      contentSections.each(function () {
          var actual = $(this),
              actualHeight = actual.height() + parseInt(actual.css('paddingTop').replace('px', '')) + parseInt(actual.css('paddingBottom').replace('px', '')),
              actualAnchor = secondaryNav.find('a[href="#' + actual.attr('id') + '"]');

          if ((actual.offset().top - secondaryNav.outerHeight() <= $(window).scrollTop()) && (actual.offset().top + actualHeight - secondaryNav.outerHeight() > $(window).scrollTop())) {
              actualAnchor.addClass('active');
          } else {
              actualAnchor.removeClass('active');
          }

      });
  }
  updateSecondaryNavigation();

    $(window).scroll(function (event) {
        updateSecondaryNavigation();
    });
  /* scroll to + sticky nav END */


  /* SEC-5 TOGGLE ARROW */

  // default toggle arrow behavior
  $('.toggle-arrow').click(function(event) {
      event.preventDefault();
    $(event.target).next('p.arrow-content').toggle(200);
    $(this).find('.arrow-right').toggleClass('arrow-rotate');
  });

  // media query event handler
  if (matchMedia) {
    var mediaQueryMobil = window.matchMedia("(max-width: 500px)");
    mediaQueryMobil.addListener(WidthChange);
    WidthChange(mediaQueryMobil);
  }

  // media query change
  function WidthChange(mediaQueryMobil) {
    if (mediaQueryMobil.matches) {
      // window width is less than 500px
      $('#numberOne').hide();
      $('.arrow-content').hide();
      $('.toggle-arrow').find('.arrow-right').removeClass('arrow-rotate');
    } else {
      // window width is more than 500px
      $('#numberOne').show();
      $('.arrow-content').show();
      $('.toggle-arrow').find('.arrow-right').addClass('arrow-rotate');
    }
  }
/* SEC-5 TOGGLE ARROW END */

});
