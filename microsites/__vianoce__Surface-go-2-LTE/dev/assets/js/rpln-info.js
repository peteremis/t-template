var rplnInfo = { 
    "b2c":{
        "M": {
            "name": "M",
            "prices": {
                "taxless_price": 9.08,
                "tax_price": 10.90
            },
            "speed" : "30/3 Mb/s",
            "tv_channels": 60
        },
        "L": {
            "name": "L",
            "prices": {
                "taxless_price": 13.25,
                "tax_price": 15.90
            },
            "speed" : "250/25 Mb/s",
            "tv_channels": 72
        },
        "XL": {
            "name": "XL",
            "prices": {
                "taxless_price": 17.41,
                "tax_price": 20.90
            },
            "speed" : "600/60 Mb/s",
            "tv_channels": 121
        }
    },
    "b2b":{
        "biznisTv_m":{
            "name": "Biznis TV M",
            "prices":{
                "taxless_price": 9.08,
                "tax_price": 10.90
                },
            "tv_channels": 60
            },
        "biznisTv_l":{
            "name": "Biznis TV L",
            "prices":{
                "taxless_price": 13.25,
                "tax_price": 15.90
                },
            "tv_channels": 72
            },
        "biznisTv_xl":{
            "name": "Biznis TV XL",
            "prices":{
                "taxless_price": 17.41,
                "tax_price": 20.90
                },
            "tv_channels": 121
            },
        "optikNet_m": {
            "name": "Biznis OptikNet M",
            "prices":{
                "taxless_price": 11.58,
                "tax_price": 13.90
                },
            "speed": {
                "download": "60 Mb/s",
                "upload": "12 Mb/s"
                }
            },
        "optikNet_l": {
            "name": "Biznis OptikNet L", 
            "prices":{
                "taxless_price": 16.58,
                "tax_price": 19.90
                },
            "speed":{
                "download": "300 Mb/s",
                "upload": "60 Mb/s"
                }
            },
        "optikNet_xl": {
            "name": "Biznis OptikNet XL", 
            "prices":{
                "taxless_price": 19.92,
                "tax_price": 23.90
                },
            "speed":{
                "download": "450 Mb/s",
                "upload": "90 Mb/s"
                } 
            },
        "optikNet_xxl": {
            "name": "Biznis OptikNet XXL", 
            "prices":{
                "taxless_price": 21.58,
                "tax_price": 25.90
                },
            "speed":{
                "download": "600 Mb/s",
                "upload": "120 Mb/s"
                }   
            },
        "optikNet_max": {
            "name": "Biznis OptikNet MAX",  
            "prices":{
                "taxless_price": 25.75,
                "tax_price": 30.90
                },
            "speed":{
                "download": "1000 Mb/s",
                "upload": "200 Mb/s" 
                }
            },
        "klasikNet_m": {
            "name": "Biznis KlasikNET M",
            "prices":{
                "taxless_price": 11.58,
                "tax_price": 13.90
                },
            "speed":{
                "download": "10 Mb/s",
                "upload": "2 Mb/s",
                "adsl": {
                    "download": "4 Mb/s",
                    "upload": "0.5 Mb/s"
                    }
                }
            },
        "klasikNet_mplus": {
            "name": "Biznis KlasikNET M+",
            "prices":{    
                "taxless_price": 14.92,
                "tax_price": 17.90
            },
            "speed":{
                "download": "25 Mb/s",
                "upload": "5 Mb/s",
                "adsl": {
                    "download": "8 Mb/s",
                    "upload": "1 Mb/s"
                    }
                }
            },
        "klasikNet_lplus": {
            "name": "Biznis KlasikNET L+",
            "prices":{
                "taxless_price": 17.42,
                "tax_price": 20.90
                },
            "speed":{
                "download": "40 Mb/s",
                "upload": "8 Mb/s" 
                }
            },
        "klasikNet_xl": {
            "name": "Biznis KlasikNET XL",
            "prices":{
                "taxless_price": 19.92,
                "tax_price": 23.90
                },
            "speed":{  
                "download": "60 Mb/s",
                "upload": "9 Mb/s",
                "adsl": {
                    "download": "15+ Mb/s",
                    "upload": "1 Mb/s"
                    }
                }    
            },
        "klasikNet_xxl": {
            "name": "Biznis KlasikNET XXL",
            "prices":{
                "taxless_price": 21.58,
                "tax_price": 25.90
                },
            "speed":{    
                "download": "90 Mb/s",
                "upload": "10 Mb/s"
                }      
            }                      
        }    
    }