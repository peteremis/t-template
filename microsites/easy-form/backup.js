$(document).ready(function () {

	/*
	 var form = $("#p_p_id_dynamicForm_WAR_eccadmin_").contents();
	 $("#c-form").append(form);
	 */


	$('.formCompositionRenderDisplay h2').hide();
	//$('.formCompositionRenderDisplay h4').hide();

	//$('.labelField').width(250);

	$('.prevPageButtonSpan').hide();
	$('.pageMiddle').hide();


});

$(window).load(function () {

	var customerType = $('input:radio[name=typ-zakaznika]').val();
	enableCustomer(customerType);

	$("input:radio[name=typ-zakaznika]").click(function () {
		var value = $(this).val();
		enableCustomer(value);

	});

	var operator = $('#formFieldId13154 select').val();
	changeJuro (operator);

	$('#formFieldId13154 select').on('change', function () {
		changeJuro ($(this).val());
	});

	function changeJuro (operator) {
		if (operator=='Juro') {
			$('#formFieldId13460 span:nth-child(1)').html('Cena SIM pri prenose je 0,04 â‚¬ bez úvodného kreditu.<br>Akcia Dvojnásobný kredit za prenos čísla sa nevzťahuje na prechod zo služby Juro.');
		} else {
			$('#formFieldId13460 span:nth-child(1)').html('Cena SIM pri prenose je 0,04 â‚¬ bez úvodného kreditu.');

		}
	}

	function enableCustomer(value) {
		if (value === 'radioVal0') {
			$('#formFieldId12975').show();
			$('#formFieldId13634').hide();
			$('#formFieldId13635').hide();
			$('#formFieldId13636').hide();
		} else {
			//firemny zakaznik
			$('#formFieldId12975').hide();
			$('#formFieldId13634').show();
			$('#formFieldId13635').show();
			$('#formFieldId13636').show();
		}
	}
});