$(document).ready(function () {

	/*
	 var form = $("#p_p_id_dynamicForm_WAR_eccadmin_").contents();
	 $("#c-form").append(form);
	 */

	$(".hide-prenos").hide();

	$('.formCompositionRenderDisplay h2').hide();
	//$('.formCompositionRenderDisplay h4').hide();

	//$('.labelField').width(250);

	$('.prevPageButtonSpan').hide();
	$('.pageMiddle').hide();


	$('.fyzicka-osoba').find('.radio > span').addClass('checked');
	$('.pravnicka-osoba').find('.radio > span').removeClass('checked');

	$('.form__submit label').hide();
	$('.finishFormButton').hide();

});

$(window).load(function () {

	var $esignOsobne = $('.esign_osobne');

	var $esignSuhlas = $('.esign_suhlas');
	var $esignSuhlaChecker = $('.esign_suhlas .checker span');

	// $esignSuhlaChecker.hide();

	var esignSuhlasNotice = '<div style="background-color: #fff7da; padding: 20px; width: 95%;margin-left: 4px; margin-top: 10px; margin-right: 20px;">Elektronický podpis urýchli proces preberania objednávky od kuriéra (<a href="https://www.telekom.sk/wiki/ostatne/elektronicke-podpisy-dokumentacia">zistiť Viac o elektronickom podpise</a>)</div>';
	var esignOsobneNotice = '<div style="background-color: #fff7da; padding: 20px; width: 95%;margin-left: 4px; margin-top: 10px; margin-right: 20px;">V prípade potvrdenia tohto súhlasu môže objednávku prebrať len osoba, ktorej meno je uvedené v zmluvných dokumentoch.</div>';

	$esignSuhlas.append(esignSuhlasNotice);
	$esignOsobne.attr("style", "padding-left: 20px").append(esignOsobneNotice);

	$esignSuhlaChecker.change(function(){
		checkEsign();
	});

	checkEsign();

	console.log($esignSuhlaChecker);

	var b2cperson = ['field__person-title', 'field__person-firstname', 'field__person-lastname'],
		b2bperson = ['field__company-name', 'field__company-ico', 'field__company-person-name'],
		fyzickaOsobaOP = 'Zároveň súhlasím s <a href="https://www.telekom.sk/osobne/m-site/obchodne-podmienky-e-shop/">obchodnými podmienkami pre uzatváranie zmlúv s využitím prostriedkov komunikácie na diaľku</a> a so začatím poskytovania služieb za podmienok uvedených v tejto objednávke aj pred uplynutím lehoty na odstúpenie od zmluvy uzatvorenej na diaľku v dĺžke 14 dní odo dňa doručenia Registračného formulára a Easy karty a bol som poučený o tom, že udelením súhlasu so začatím poskytovania služieb pred uplynutím lehoty na odstúpenie od zmluvy na diaľku som povinný uhradiť sumu úmernú rozsahu poskytnutého plnenia do momentu odstúpenia.';
	//legalFo = ['legal-fo'];

	var customerType = $('.checked input:radio[name=typ-zakaznika]').val();
	enableCustomer(customerType);

	var prenosURL = '/mam-zaujem/prepaid/volania/easy/objednavka/prenos',
		easyURL = '/mam-zaujem/prepaid/volania/easy/objednavka/nova-easy',
		currentURL = window.location.pathname,
		$parentDiv = $('#prenos-zaujem').closest('li'),
		$checker = $parentDiv.find('.checker > span'),
		$checkBox = $parentDiv.find('input:checkbox'),
		$pageTitle = $('.pageNameField'),
		$fyzickaOsobaHolder = $('#fyzicka-osoba-op').closest('.formFieldRender').find('.formFieldValidationResult'),
		$zverejnenieUdajovHolder = $('#zerejnenie-udajov').closest('.formFieldRender').find('.formFieldValidationResult'),
		$udelenieSuhlasuHolder = $('#udelenie-suhlasu').closest('.formFieldRender').find('.formFieldValidationResult'),
		$rozhodcovskaZmluvaHolder = $('#rozhodcovska-zmluva').closest('.formFieldRender').find('.formFieldValidationResult'),
		mandatoryFieldText = '<span>Povinné</span>',
		optionalFieldText = '<span></span>',
		$pocetSIM = $('#pocet-sim').closest('li'),
		$prenosOnlyHolder = $('.prenos-only').closest('li');

	$(mandatoryFieldText).appendTo($fyzickaOsobaHolder);
	$(optionalFieldText).appendTo($zverejnenieUdajovHolder);
	$(optionalFieldText).appendTo($udelenieSuhlasuHolder);
	$(optionalFieldText).appendTo($rozhodcovskaZmluvaHolder);

	$pocetSIM.hide();

	$("input:radio[name=typ-zakaznika]").click(function () {
		var value = $(this).val(),
			customerType = value;

		console.log('change');
		enableCustomer(value);

	});

	var operator = $('#formFieldId111518 select').val();
	changeJuro(operator);

	$('#formFieldId111518 select').on('change', function () {
		changeJuro($(this).val());
		console.log($(this).val());
	});

	function checkEsign() {
		if ($esignSuhlaChecker.hasClass('checked')) {
			$esignOsobne.show();
		} else {
			$esignOsobne.hide();
		}
	}

	function changeJuro(operator) {
		if (operator == 'Juro') {
			$('#formFieldId111519 span:nth-child(1)').html('Cena SIM pri prenose je 0,04 €‚ bez úvodného kreditu.<br>Akcia Dvojnásobný kredit za prenos čísla sa nevzťahuje na prechod zo služby Juro.');
		} else {
			$('#formFieldId111519 span:nth-child(1)').html('Cena SIM pri prenose je 0,04 €‚ bez úvodného kreditu.');

		}
	}

	function toggleField(classnameArr, state) {

		for (i = 0; i < classnameArr.length; i++) {

			var $form = $('.' + classnameArr[i]).closest('li');
			if (state == 'hide') {
				$form.hide();
			} else if (state == 'show') {
				$form.show();
			}
		}
	}

	function removeValue(classnameArr) {

		var $form;
		for (i = 0; i < classnameArr.length; i++) {
			$form = $('.' + classnameArr[i] + ' input');
			$form.val('').blur();
		}
	}

	function changeTootlipText(tooltipClassname, newText) {
		$('.' + tooltipClassname).qtip('option', 'content.text', newText);
	}

	function checkTooltip(customerType) {
		if (customerType == 'radioVal0') {
			changeTootlipText('info-adress', 'Adresa trvalého bydliska');
			$('#fyzicka-osoba-op').html(fyzickaOsobaOP);
		} else {
			changeTootlipText('info-adress', 'Sídlo spoločnosti');
			$('#fyzicka-osoba-op').html('');
		}
	}

	function enableCustomer(value) {
		if (value === 'radioVal0') {
			checkTooltip(value);
			toggleField(b2cperson, 'show');
			toggleField(b2bperson, 'hide');
			//toggleField(legalFo, 'show');

			removeValue(b2bperson);

		} else {
			//firemny zakaznik
			checkTooltip(value);
			toggleField(b2bperson, 'show');
			toggleField(b2cperson, 'hide');
			//toggleField(legalFo, 'hide');

			removeValue(b2cperson);
		}
	}

	function addTooltip(targetClassName, tooltipClassName) {
		var $target = $('.' + targetClassName),
			tootlipContent = '<img class="' + tooltipClassName + ' info-tooltip' + '" src="/documents/10179/2652709/ico-info.png" alt="" />';

		$target.addClass('relative');

		$(tootlipContent).appendTo($target);
	}

	function createTooltip(tooltipClass, tooltipText) {
		$('.' + tooltipClass).qtip({
			content: {
				text: tooltipText
			},
			position: {
				my: 'left center',
				at: 'right center'
			}
		});
	}

	addTooltip('fyzicka-osoba', 'info-fo');
	addTooltip('pravnicka-osoba', 'info-po');
	//addTooltip('cityInvoiceAutocompleteClass', 'info-adress');
	addTooltip('field__company-person-name', 'info-cpn');


	createTooltip('info-fo', 'spotrebiteľ');
	createTooltip('info-po', 'obchodná spoločnosť, fyzická osoba – podnikateľ, iná právnická osoba');
	createTooltip('info-cpn', 'označenie štatutárneho orgánu alebo oprávneného zástupcu právnickej osoby, ktorý je oprávnený zaväzovať právnickú osobu');
	//createTooltip('info-adress', 'Adresa trvalého bydliska');

	$('.info-adress').qtip({
		content: {
			text: 'Adresa trvalého bydliska'
		},
		position: {
			my: 'left center',
			at: 'right center'
		}
	});

	checkTooltip(customerType);


});