$(window).load(function () {

  console.log(window.location.hostname);

  var apiUrl = 'https://backvm.telekom.sk/shop/ins/index3.php';

  if (window.location.hostname == 'localhost') {
    apiUrl = 'http://backvm.telekom.localhost/shop/ins/planned.php';
  }

  var address = '',
    addressType = '',

    $cityInput = $('.cityInvoiceAutocompleteClass input'),
    cityInput = '.cityInvoiceAutocompleteClass input',

    cityResultList = '.city-form__result_list',
    cityResults = '.results-city',

    $streetInput = $('.streetInvoiceAutocompleteClass input'),
    streetInput = '.streetInvoiceAutocompleteClass input',

    streetResultList = '.street-form__result_list',
    streetResults = '.results-street',

    $numberInput = $('.number-input'),
    $btn = $('.sendReq'),
    $errorMsg = $('.error-msg'),
    $infoMsg = $('.info-msg'),

    $resetBtn = $('.reset'),
    hasStreet = true,
    cityId = '',
    streetId = '',
    isInfoVisible = false,
    isErrorVisible = false,
    boxElem = '',
    /*
      URL_CITY = 'https://backvm.telekom.sk/shop/ins/?searchTermKey=citySubstring',
      URL_STREET = 'https://backvm.telekom.sk/shop/ins/?searchTermKey=streetSubstring',
      URL_FINAL = 'https://backvm.telekom.sk/shop/ins/?searchTermKey=streetNoSubstring',
    */
    URL_CITY = apiUrl + '?searchTermKey=citySubstring',
    URL_STREET = apiUrl + '?searchTermKey=streetSubstring',
    URL_FINAL = apiUrl + '?searchTermKey=streetNoSubstring',
    MIN_KEY_DOWN = 3,
    MSG_CANT_CONNECT = 'Nepodarilo sa načítať informácie, skúste to prosím ešte raz.',
    MSG_CANT_FIND_ADDRESS = 'Na zadanej adrese sme nenašli plánované pripojenia. Pre ďalšie overenie pokračujte na túto <a href="/chytry-balik">stránku</a>.';

  $cityInput.addClass('city-input');
  $streetInput.addClass('street-input');

  var cityList = '<div class="results-city" style="display: none;"> <ul class="city-form__result_list"></ul> </div>';
  $cityInput.after(cityList);

  var streetList = '<div class="results-street" style="display: none;"> <ul class="street-form__result_list"></ul> </div>';
  $streetInput.after(streetList);

  function hideList(resultDiv) {
    resultDiv.hide();
  }

  function showList(resultDiv) {
    console.log(resultDiv);
    resultDiv.show();
    // $('.results-city').show();
  }

  function emptyElem(elem) {
    $(elem).html('');
  }

  function addTextToInput($input, text) {
    $input.val(text);
  }

  function clearInput($input) {
    $input.val('');
  }

  function showErrorMsg(msg) {
    $errorMsg.text(msg);
    isErrorVisible = true;
  }

  function hideErrorMsg() {
    $errorMsg.text('');
    isErrorVisible = false;
  }

  function showInfoMsg(msg) {
    $infoMsg.html(msg);
    isInfoVisible = true;
  }

  function hideInfoMsg() {
    $infoMsg.html('');
    isInfoVisible = false;
  }

  function preventSubmitOnEnterPress($input) {
    $input.keypress(function (e) {
      var code = (e.keyCode ? e.keyCode : e.which);
      if (code == 13) {
        return false;
      }
    });
  }

  function clearResultOnArrowKey($input, $result) {
    $input.keydown(function (e) {
      var code = (e.keyCode ? e.keyCode : e.which);
      if (code == 38 || code == 40) {
        emptyElem($result);
      }
    });
  }

  function resetOnBackSpace($input, resetCityInput, resetStreetInput) {
    $input.on('keydown', function (e) {
      if (e.which == 8 || e.which == 46) {
        if (resetCityInput) {
          // resetFields();
        } else if (resetStreetInput) {
          clearInput($numberInput);

          emptyElem(streetResultList);

          switchInputState($numberInput, true);

          emptyElem('.box');
        }
      }
    });
  }

  function resetFields() {
    clearInput($cityInput);
    clearInput($streetInput);
    clearInput($numberInput);

    emptyElem(cityResultList);
    emptyElem(streetResultList);

    switchInputState($streetInput, true);
    switchInputState($numberInput, true);

    emptyElem('.box');
  }

  function bindReset($elem) {
    $elem.on('click', function (e) {
      e.preventDefault();
      resetFields();
    });
  }

  var generateCityList = function (results) {
    // console.log(results);

    var htmlToAppend = '';
    for (var i = 0; i < results.length; i++) {


      if (results[i].numOfStreets == 0) {
        hasStreet = false;
      } else {
        hasStreet = true;
      }

      htmlToAppend += '<li class="' + (hasStreet ? 'hasStreet' : '') + '" data-id="' + results[i].id + '">' + results[i].name + '</li>';
    }

    // console.log(htmlToAppend);
    return htmlToAppend;
  }

  var generateStreetList = function (results) {
    var htmlToAppend = '';
    for (var i = 0; i < results.length; i++) {
      htmlToAppend += '<li data-id="' + results[i].id + '">' + results[i].street_name + '</li>';
    }

    return htmlToAppend;
  }

  function bindClickEvent(result, resultDiv, $input) {

    var $result = $(result);
    var $resultDiv = $(resultDiv);

    $result.on('click', function (e) {
      e.preventDefault();

      var choosenElem = $(e.target).text();
      $input.val(choosenElem);

      emptyElem(result);
      hideList($resultDiv);

      if ($input.hasClass('city-input')) {

        if ($(e.target).hasClass('hasStreet')) {
          switchInputState($streetInput);
          cityId = $(e.target).data('id');
        } else {
          addTextToInput($streetInput, 'Obec nemá ulice');
        }
        switchInputState($numberInput);
      } else if ($input.hasClass('street-input')) {
        streetId = $(e.target).data('id');
        switchInputState($numberInput);
      }

    });
  }

  function bindKeyup($input, result) {

    $input.on('keyup', function () {
      if (isErrorVisible) {
        hideErrorMsg();
      }
      if (isInfoVisible) {
        hideInfoMsg();
      }

      if ($(this).val() == '') {
        emptyElem(result);

        if ($input.hasClass('city-input')) {
          clearInput($streetInput);
          clearInput($numberInput);
          switchInputState($streetInput, true);
          switchInputState($numberInput, true);
          switchBtnState($btn, true);
        } else if ($input.hasClass('street-input')) {
          clearInput($numberInput);
          switchInputState($numberInput, true);
          switchBtnState($btn, true);
        }
      }
    });
  }

  function bindNumKeyUp() {
    $numberInput.on('keyup', function () {
      if ($(this).val() != '') {
        switchBtnState($btn, false);
      } else {
        switchBtnState($btn, true);
      }
    });
  }

  function switchInputState($input, disable) {
    if (disable) {
      if (!$input.attr('disabled')) {
        $input.attr('disabled', true);
      }
      if (!$input.hasClass('disabled')) {
        $input.addClass('disabled');
      }
    } else {
      if ($input.attr('disabled')) {
        $input.attr('disabled', false);
      }
      if ($input.hasClass('disabled')) {
        $input.removeClass('disabled');
      }
    }
  }

  function switchBtnState($btn, disable) {
    if (disable) {
      if (!$btn.attr('disabled')) {
        $btn.attr('disabled', true);
      }
      if (!$btn.hasClass('disabled')) {
        $btn.addClass('disabled');
      }
    } else {
      if ($btn.attr('disabled')) {
        $btn.attr('disabled', false);
      }

      if ($btn.hasClass('disabled')) {
        $btn.removeClass('disabled');
      }
    }
  }

  function hideDropdownIfClickedElsewhere() {
    $(document).mouseup(function (e) {
      var container = $('.results'),
        elemToHide = $('.results ul');

      if (!container.is(e.target) && container.has(e.target).length === 0) {
        elemToHide.html('');
      }
    });
  }

  function bindAutocomplete($input, result, resultDiv, addCityId, generateFunction, url, minLength) {

    var $result = $(result)
    var $resultDiv = $(resultDiv)

    $input.autocomplete({
      source: function (request, response) {
        $input.addClass('loading');
        emptyElem(result);

        var cityData = {
            searchTermValue: request.term
          },
          streetData = {
            searchTermValue: request.term,
            cityId: cityId
          },
          tempData = {};

        addCityId ? $.extend(true, tempData, streetData) : $.extend(true, tempData, cityData)

        $.ajax({
          url: url,
          data: tempData,
          dataType: "json",
          success: function (data) {

            if (data.length == 0) {
              showInfoMsg(MSG_CANT_FIND_ADDRESS);
            }

            var temphtmlToAppend = '';

            temphtmlToAppend = generateFunction(data);

            // console.log(temphtmlToAppend);
            // console.log($result);

            $(temphtmlToAppend).appendTo($result);
            $result.show();
            showList($resultDiv);

            $input.removeClass('loading');
          },
          error: function () {
            showErrorMsg(MSG_CANT_CONNECT);
            $input.removeClass('loading');
          }
        });
      },
      minLength: minLength
    });
  }

  function submitAddress() {

    var tempVal = $numberInput.val();

    $.ajax({
      type: "GET",
      async: true,
      crossDomain: true,
      dataType: "json",
      data: {
        searchTermValue: tempVal,
        cityName: $cityInput.val(),
        streetName: $streetInput.val(),
        cityId: cityId,
        streetId: streetId
      },
      url: URL_FINAL,

      success: function (response) {

        if (typeof (response.address) === "undefined" || response.address == null) {
          // if (response.length == 0) {
          showInfoMsg(MSG_CANT_FIND_ADDRESS);

          $.fancybox({
            href: '#not-found',
            modal: true
          });
          return false;

        } else {

          address = response.address;
          addressType = response.addressType;

          $('.box').empty();

          $(boxElem).appendTo('.box');

        }
      },
      error: function () {
        showErrorMsg(MSG_CANT_CONNECT);
      }
    });
  }

  $btn.on('click', function () {
    submitAddress();
  });

  switchInputState($streetInput, true);
  switchInputState($numberInput, true);
  switchBtnState($btn, true);

  bindAutocomplete($cityInput, cityResultList, cityResults, false, generateCityList, URL_CITY, MIN_KEY_DOWN);
  bindAutocomplete($streetInput, streetResultList, streetResults, true, generateStreetList, URL_STREET, MIN_KEY_DOWN);

  preventSubmitOnEnterPress($cityInput);
  preventSubmitOnEnterPress($streetInput);
  preventSubmitOnEnterPress($numberInput);

  resetOnBackSpace($cityInput, true, false);
  resetOnBackSpace($streetInput, false, true);

  bindClickEvent(cityResultList, cityResults, $cityInput);
  bindKeyup($cityInput, cityResultList);

  bindClickEvent(streetResultList, streetResults, $streetInput);
  bindKeyup($streetInput, streetResultList);

  bindReset($resetBtn);

  bindNumKeyUp();

  hideDropdownIfClickedElsewhere();

  $('.not-found-form').show();
  $('.not-found-success').hide();

  $('.popup').fancybox({
    padding: 11,
    margin: 0,
    closeBtn: true,
    hideOnOverlayClick: true,
    helpers: {
      overlay: {
        css: {
          'background': 'rgba(0, 0, 0, 0.7)'
        }
      }
    }
  });

  $('.p-close').on('click', function (e) {
    e.preventDefault();
    $.fancybox.close();
  });

});

$(document).ready(function () {
});