/***
 * Predpoklady:
 * 1-Doplnit tento skript ako hlavny skript stranky s formularom
 * 2-Doplnit css classy pre jednotlive inputy, ktore, ktore ideme robit autocomplete a validaciu. V nasom pripade ide o
 *  -cityInvoiceAutocompleteClass - pre adresu mesto v sekcii fakturacna adresa
 *  -streetInvoiceAutocompleteClass - pre ulicu v sekcii fakturacna adresa
 *  -zipInvoiceAutocompleteClass - pre psc v sekcii fakturacna adresa
 *  -cityDeliveryAutocompleteClass - pre mesto v sekcii adresa pre dorucenie
 *  -streetDeliveryAutocompleteClass - pre ulicu v sekcii adresa pre dorucenie
 *  -zipDeliveryAutocompleteClass - pre psc v sekcii adresa pre dorucenie
 *  -cityDeliveryCheckboxClass - pre checkbox na zasktnutie inej adresy dorucenia
 * 3-Dolezitym predpokladom na spravne fungovanie je aby button pre submitnutie formulara mal classu finishFormButton, inak je potrebenv kode rucne zmenit na prisluchajucu
 * 4-Nastavenie spravnej domeny pre dotahovanie autocomplete a validation v premennej DOMAIN;
 */

var DOMAIN = window.location.origin;
var invoiceCityId = "0";
var deliveryCityId = "0";
var invoiceValidationDoneAndOk = false;
var deliveryValidationDoneAndOk = false;
var cityInvoiceAutocompleteClass = "cityInvoiceAutocompleteClass";
var streetInvoiceAutocompleteClass = "streetInvoiceAutocompleteClass";
var zipInvoiceAutocompleteClass = "zipInvoiceAutocompleteClass";
var cityDeliveryAutocompleteClass = "cityDeliveryAutocompleteClass";
var streetDeliveryAutocompleteClass = "streetDeliveryAutocompleteClass";
var zipDeliveryAutocompleteClass = "zipDeliveryAutocompleteClass";
var cityDeliveryCheckboxClass = "cityDeliveryCheckboxClass";
$(function() {

  //autocomplete city
  autocompleteCity(cityInvoiceAutocompleteClass, streetInvoiceAutocompleteClass, zipInvoiceAutocompleteClass, "INVOICE");

  //street autocomplete
  autocompleteStreet(streetInvoiceAutocompleteClass, "INVOICE");

  //after blur street get and fill zip
  autocompleteZip(streetInvoiceAutocompleteClass,zipInvoiceAutocompleteClass, cityInvoiceAutocompleteClass);

  //after type city, delete street and zip
  //after type street, delete zip
  deleteAfterStartType(cityInvoiceAutocompleteClass, streetInvoiceAutocompleteClass, zipInvoiceAutocompleteClass);

  //city autocomplete
  autocompleteCity(cityDeliveryAutocompleteClass, streetDeliveryAutocompleteClass, zipDeliveryAutocompleteClass, "DELIVERY");

  //street autocomplete
  autocompleteStreet(streetDeliveryAutocompleteClass, "DELIVERY");

  //after blur street get and fill zip
  autocompleteZip(streetDeliveryAutocompleteClass,zipDeliveryAutocompleteClass, cityDeliveryAutocompleteClass);

  //after type city, delete street and zip
  //after type street, delete zip
  deleteAfterStartType(cityDeliveryAutocompleteClass, streetDeliveryAutocompleteClass, zipDeliveryAutocompleteClass);

  //save click event to temp var to diable after click and after success validate give back and calll
  var clickHandler = $('.finishFormButton').data("events")['click'].splice(0, 1);
  $(".finishFormButton").click(function(event) {
    validateAddress(cityInvoiceAutocompleteClass, streetInvoiceAutocompleteClass, zipInvoiceAutocompleteClass, clickHandler, "INVOICE");
    if ($("." + cityDeliveryCheckboxClass + ">div>span").hasClass("checked")){
      validateAddress(cityDeliveryAutocompleteClass, streetDeliveryAutocompleteClass, zipDeliveryAutocompleteClass, clickHandler, "DELIVERY");
    }
  });
});

function validateAddress(cityClass, streetClass, zipClass, clickHandler, type){
  $.ajax({
    url: DOMAIN + "/delegate/addressValidation?",
    dataType: "json",
    data: {
      street: $("." + streetClass + ">span>input").val(),
      city: $("." + cityClass + ">span>input").val(),
      zip: $("." + zipClass + ">span>input").val(),
    },
    success: function(data) {
      if (data != null) {
        if (data.status === "INVALID") {
          if(type === "DELIVERY"){
            deliveryValidationDoneAndOk = false;
          } else {
            invoiceValidationDoneAndOk = false;
          }
          $("." + streetClass + " .error").show();
          $("." + streetClass + " .error").text("Túto adresu sa nepodarilo overiť");
          $("." + streetClass + " .validated").hide();
          $("." + cityClass + " .error").show();
          $("." + cityClass + " .error").text("Túto adresu sa nepodarilo overiť");
          $("." + cityClass + " .validated").hide();
          $("." + zipClass + " .error").show();
          $("." + zipClass + " .error").text("Túto adresu sa nepodarilo overiť");
          $("." + zipClass + " .validated").hide();
        } else if (data.status === "CORRECTED") {

          if(type === "DELIVERY"){
            deliveryValidationDoneAndOk = true;
          } else {
            invoiceValidationDoneAndOk = true;
          }
          if (($("." + cityDeliveryCheckboxClass + ">div>span").hasClass("checked") && deliveryValidationDoneAndOk && invoiceValidationDoneAndOk) ||
            (!$("." + cityDeliveryCheckboxClass + ">div>span").hasClass("checked") && invoiceValidationDoneAndOk)) {
            console.log("OK");
            submitFunction = clickHandler[0].handler;
            $('.finishFormButton').bind("click", clickHandler[0])
            submitFunction();
          }
        }
      }
    }
  });
}

function autocompleteZip(streetClass, zipClass, cityClass){
  $("." + streetClass + ">span>input").blur(function() {
    $.ajax({
      url: DOMAIN + "/delegate/addressValidation?",
      dataType: "json",
      data: {
        street: $("." + streetClass + ">span>input").val(),
        city: $("." + cityClass + ">span>input").val(),
      },
      success: function(data) {
        if (data != null) {
          if (data.status === "INVALID") {

          } else if (data.status === "CORRECTED") {
            $("." + zipClass + ">span>input").val(data.address.zip);
            $("." + zipClass + " .error").hide();
            $("." + zipClass + " .validated").show();
            $("." + cityClass + " .error").hide();
            $("." + cityClass + " .validated").show();
          }
        }
      }
    });
  });
}

function autocompleteCity(cityClass, streetClass, zipClass, type){
  $("." + cityClass + ">span>input").autocomplete({
    source: function(request, response) {
      $.ajax({
        url: DOMAIN + "/delegate/addressSuggestion?searchTermKey=cityPrefix",
        dataType: "json",
        data: {
          searchTermValue: request.term
        },
        success: function(data) {
          if (data.length < 1) {
            if(type === "DELIVERY"){
              deliveryCityId = "0";
            } else {
              invoiceCityId = "0";
            }
          } else {
            response(data.slice(0, 10));
          }
        }
      });
    },
    minLength: 2,
    focus: function(event, ui) {
      $("." + cityClass + ">span>input").val(ui.item.name);
      return false;
    },
    select: function(event, ui) {
      $("." + cityClass + ">span>input").val(ui.item.name);
      if(type === "DELIVERY"){
        deliveryCityId = ui.item.id;
      } else {
        invoiceCityId = ui.item.id;
      }
      if (ui.item.numOfStreets === 0) {
        $("." + streetClass + ">span>input").prop("disabled", true);
      } else {
        $("." + streetClass + ">span>input").prop("disabled", false);
      }
      return false;
    }
  }).data("ui-autocomplete")._renderItem = function(ul, item) {
    ul.css("background", "white").css("color", "#6c6c6c").css("border", "1px solid #d0d0d0");
    var re = new RegExp("^" + this.term);
    var t = item.name.replace(re, "<span class='address-suggestions-highlighted'>" +
      this.term +
      "</span>");
    return $("<li></li>")
      .data("item.autocomplete", item)
      .append(t)
      .appendTo(ul);
  };
}

function autocompleteStreet(streetClass, type){
  $("." + streetClass + ">span>input").autocomplete({
    source: function(request, response) {
      $.ajax({
        url: DOMAIN + "/delegate/addressSuggestion?searchTermKey=streetPrefix",
        dataType: "json",
        data: {
          searchTermValue: request.term,
          cityId: ((type === "DELIVERY") ? deliveryCityId : invoiceCityId),
        },
        success: function(data) {
          if (data != null) {
            response(data.slice(0, 10));
          }
        }
      });
    },
    minLength: 1,
  }).data("ui-autocomplete")._renderItem = function(ul, item) {
    ul.css("background", "white").css("color", "#6c6c6c").css("border", "1px solid #d0d0d0");
    var re = new RegExp("^" + this.term);
    var t = item.label.replace(re, "<span class='address-suggestions-highlighted'>" +
      this.term +
      "</span>");
    return $("<li></li>")
      .data("item.autocomplete", item)
      .append(t)
      .appendTo(ul);
  };
}


function deleteAfterStartType(cityClass, streetClass, zipClass){
  $("." + cityClass + ">span>input").keydown(function() {
    $("." + streetClass + ">span>input").val("");
    $("." + zipClass + ">span>input").val("");
  });

  $("." + streetClass + ">span>input").keydown(function() {
    $("." + zipClass + ">span>input").val("");
  });
}