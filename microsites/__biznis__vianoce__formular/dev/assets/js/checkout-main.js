    /* GET PARAMETERS FROM URL */
  function getUrlParameter(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)");
    var results = regex.exec(location.search);
    return results === null
      ? ""
      : decodeURIComponent(results[1].replace(/\+/g, " "));
  }


$(document).ready(function () {
  selectedRp = getUrlParameter("rp"); // GET DATA FROM URL
  produkt = getUrlParameter("produkt");
  device = getUrlParameter("zariadenie");
  if (device == "SAMSUNG_UE50TU8072UXXH_PLUS_SAMSUNG_GALAXY_A20E"){
    device = "TV Samsung + Samsung Galaxy A20e"
  }
  if (device == "SAMSUNG_UE50TU7172_PLUS_SAMSUNG_GALAXY_A20E"){
    device = "TV Samsung + Samsung Galaxy A20e"
  }
  if (device == "TV_PHILIPS_65PUS8505_12_FIX"){
    device = "TV Philips 65PUS8505"
  }
  if (device == "HUAWEI_FREEBUDS_3I_FIX"){
    device = "Huawei FreeBuds 3i"
  }
  if (device == "ASUS_VIVOBOOK_14_PLUS_MICRO_BIT_STARTER_KIT_FIX"){
    device = "Asus Vivobook 14 + Micro:bit Starter kit"
  }
  if (device == "ASUS_VIVOBOOK_14_FIX"){
    device = "Asus Vivobook 14"
  }
  if (device == "HUAWEI_MATEBOOK_13_AMD_2020_FIX"){
    device = "Huawei Matebook 13 AMD"
  }
  if (device == "MSSURFACE_GO_2_LTE_M_PLUS_MS_SURFACE_GO_TYPE_COVER_FIX"){
    device = "Microsoft Surface Go 2-LTE"
  }
  if (device == "MS_SURFACE_GO_2P_PLUS_MS_SURFACE_GO_TYPE_COVER_FIX"){
    device = "Microsoft Surface Go 2 P"
  }
  if (device == "MICRO_BIT_STARTER_KIT_FIX"){
    device = "Micro:bit Smart Kit"
  }
  if (device == "PLAYSTATION_4_PRO_1TB_FIFA21_FIX"){
    device = "PlayStation 4 Pro 1TB"
  }
  if (selectedRp == "biznisTv_l"){
    selectedRp = "Magio TV L"
  }
  if (selectedRp == "biznisTv_m"){
    selectedRp = "Magio TV M"
  }
  if (selectedRp == "biznisTv_xl"){
    selectedRp = "Magio TV XL"
  }
  
  if (selectedRp == "optikNet_m"){
    selectedRp = "OptikNet M"
  }

  if (selectedRp == "optikNet_l"){
    selectedRp = "OptikNet L"
  }

  if (selectedRp == "optikNet_xl"){
    selectedRp = "OptikNet XL"
  }

  if (selectedRp == "optikNet_xxl"){
    selectedRp = "OptikNet XXL"
  }

  if (selectedRp == "optikNet_max"){
    selectedRp = "OptikNet MAX"
  }

  if (selectedRp == "klasikNet_m"){
    selectedRp = "KlasikNet M"
  }
  if (selectedRp == "klasikNet_mplus"){
    selectedRp = "KlasikNet M+"
  }
  if (selectedRp == "klasikNet_lplus"){
    selectedRp = "OptikNet L+"
  }
  if (selectedRp == "klasikNet_xl"){
    selectedRp = "OptikNet XL"
  }
  if (selectedRp == "klasikNet_xxl"){
    selectedRp = "OptikNet XXL"
  }


  if (selectedRp != "") {
    $("#badge-rpln").text(selectedRp);
  }  else {
    $("#badge-rpln").hide()
  }
  if (device != "") {
    $("#badge-device").text(device);
  } else {
    $("#badge-device").hide()
  }
  if (produkt =="magio-tv") {
    $("#biznisTvHolder").hide();
  }
  if (produkt =="biznisNet") {
    $("#biznisNetHolder").hide();
  }
  var $form = $("#personalDataForm");
  
  // SUBMIT FORM
  
  $(".submitbtn").on("click", function(e) {
    e.preventDefault();
      if ($("#personalDataForm").valid()) {
  
      $("#formFieldId153716 input").val(selectedRp).blur();
      $("#formFieldId153718 input").val(device).blur();
      $("#formFieldId153734 input").val(produkt).blur();

      var companyIco = $form.find("#companyIco").val();
      $("#formFieldId153719 input").val(companyIco).blur();
      var userName = $form.find("#userName").val();
      $("#formFieldId153720 input").val(userName).blur();
      var useremail = $form.find("#useremail").val();
      $("#formFieldId153721 input").val(useremail).blur();
      var phoneNum = $form.find("#phoneNum").val();
      $("#formFieldId153722 input").val(phoneNum).blur();
      
      var otherServices = "";

      if($("#biznisNet").is(":checked")){
        otherServices = otherServices + "Biznis NET ";
      }

      if($("#biznisTv").is(":checked")){
        otherServices = otherServices + "Biznis TV ";
      }

      if($("#biznisTp").is(":checked")){
        otherServices = otherServices + "Biznis linka ";
      }

      $("#formFieldId153735 input").val(otherServices).blur();

      if($("#ebill").is(":checked")){
        $('#formFieldId153723 input').click();
      }

      setTimeout(function() {
        $(".tlacidlo_odoslat")
        .eq(0)
        .click();
      }, 500);
    } 
  });

});