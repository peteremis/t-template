let personalDataForm = $("#personalDataForm");

$(personalDataForm).validate({
  rules: {
    // Personal inputs
    name: {
      required: true,
      minlength: 2,
    },
    email: {
      required: true,
      minlength: 5
    },
    phoneNum: {
      required: true,
      minlength: 2,
    },
    companyIco: {
      required: true,
      minlength: 2,
    },
  },
  // Personal inputs messages
  messages: {
    name: {
      required: "Uveďte vaše meno",
      minlength: "Vložte minimálne 2 znaky"
    },
    companyIco: {
      required: "Uveďte vaše IČO",
      minlength: "Vložte minimálne 2 znaky"
    },
    email: {
      required: "Uveďte email v tvare vas@email.sk"
    },
    phoneNum: {
      required: "Uveďte tel.č. v tvare 0944111222",
      minlength: "Uveďte tel.č. v tvare 0944111222"
    },
  },
  success: function(input) {
    input.addClass("valid");
  },
  highlight: function(input, element, errorClass, validClass) {
    $(input)
      .removeClass("valid")
      .addClass("error"); //add error class to inputs
    $(input)
      .parent()
      .find("label")
      .addClass("error"); //add error class to labels

    $(input)
      .closest("span")
      .removeClass("valid")
      .addClass("error"); // Terms checkboxes
  },
  unhighlight: function(input, element, errorClass, validClass) {
    $(input)
      .removeClass("error")
      .addClass("valid");
    $(input)
      .parent()
      .find("label")
      .removeClass("error");

    $(input)
      .next()
      .removeClass("error"); // Delivery inputs error

    $(input)
      .closest("span")
      .removeClass("error")
      .addClass("valid"); // Terms checkboxes
  },
  // error fields
  errorElement: "div",
  errorClass: "error error_message personalInputError",
  errorPlacement: function(error, element) {
    if (element.attr("type") === "checkbox") {
      $(".termsAndConditionsForm").append(error);
    } else {
      error.insertAfter(
        $(element)
          .closest(".input-wrapper")
          .find("label")
      );
    }
  }
});

// custom email validation rule
jQuery.validator.addMethod(
  "myEmail",
  function(value, element) {
    return (
      this.optional(element) ||
      /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
        value
      )
    );
  },
  "Email musí byť v tvare vas@email.sk"
);

jQuery.validator.addMethod(
  "selectboxSelected",
  function(value, element) {
    return value !== "0";
  },
  "Nevybrali ste..."
);

//custom phone num validation rule
jQuery.validator.addMethod(
  "mobPhone",
  function(value, element) {
    console.log('phone ');
    return this.optional(element) || /^0(9)\d{8}$/.test(value);
  },
  "Telefónne číslo musí byť v tvare 0944111222"
);

//custom phone num validation rule
jQuery.validator.addMethod(
  "userIdNum",
  function(value, element) {
    return this.optional(element) || /^([a-zA-Z]{2}\d{6})$/.test(value);
  },
  "Zadajte číslo občianskeho preukazu v správnom formáte"
);

//custom phone num validation rule
jQuery.validator.addMethod(
  "zipCode",
  function(value, element) {
    return this.optional(element) || /^[0-9]{5}$/.test(value);
  },
  "Zadajte PSČ v správnom formáte"
);

//custom phone num validation rule
jQuery.validator.addMethod(
  "personName",
  function(value, element) {
    return this.optional(element) || /^[a-zA-ZàáâäãåąčćďęèéêëėįìíîïłĺľńňòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĎĖĘÈÉÊËÌÍÎÏĮŁĹĽŇŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]+$/.test(value);
  },
  "Zadali ste neplatné znaky"
);

//custom birthday num validation rule
jQuery.validator.addMethod(
  "birthdayNumValid",
  function(value, element) {
    return birthNumberValidation(value);
  },
  "Rodné číslo nie je uvedené v správnom tvare"
);

function birthNumberValidation(value) {
  var rcRegex = "^[0-9]{6}/?[0-9]{3,4}$";
  var rcOverride = "0000";
  if (!value.match(rcRegex) && !value.match(rcOverride)) {
    return false;
  }
  var month = getMonthFromBirthNumber(value);
  var day = getDayFromBirthNumber(value);
  if (day < 1 || day > 31 || month < 1 || month > 12) {
    return false;
  }
  if (value.length == 10 && value.substring(6, 10) == rcOverride) {
    /* foreigner, don't check modulo 11 */
    return true;
  }
  /* It is OK if:
   * 	- modulo is 0 or
   * 	- if modulo is 10 and last digit is 0
   */
  if (value.length == 10) {
    var lastDigit = value.substring(9, 10);
    var fist9Digits = value.substring(0, 9);
    var controlNumber = fist9Digits % 11;
    if (controlNumber == 10) {
      controlNumber = 0;
    }
    if (controlNumber != lastDigit) {
      return false;
    }
  } else if (value.length == 9 && value.substring(0, 2) > 53) {
    return false;
  }
  return true;
}

function getMonthFromBirthNumber(value) {
  var month = value.substring(2, 4);
  var year = getYearFromBirthNumber(value);
  if (month > 70 && month < 83 && year > 2003) {
    /* A woman after 2004. */
    month = month - 70;
  }
  if (month > 50 && month < 63) {
    /* A woman. */
    month = month - 50;
  }
  if (month > 20 && month < 33 && year > 2003) {
    /* A man after 2004. */
    month = month - 20;
  }
  return month;
}

function getYearFromBirthNumber(value) {
  var year = value.substring(0, 2);
  if (value.length == 8 || year >= 54) {
    year += 1900;
  } else if (year < 54) {
    year += 2000;
  }
  return year;
}

function getDayFromBirthNumber(value) {
  return value.substring(4, 6);
}
