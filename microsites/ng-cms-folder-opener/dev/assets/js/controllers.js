(function() {
	/**
    * Cms Folder Opener
    * @author  Martin Gomola
 	* @version 1.0, 21/06/2016
    */

    var app = angular.module('urlApp', []);

    app.controller('mainCtrl', function( $scope, $location, $http, $window ) {

        var vm = this;
        vm.parentFolder = 10179;
        vm.childFolder = '';
        vm.openUrl = function openUrl(parentFolder,childFolder) {
        	vm.baseUrl = "https://www.telekom.sk/group/control_panel/manage?p_p_id=20";
        	vm.parentFolderPar = "&doAsGroupId=" + parentFolder;
        	vm.childFolderPar = "&_20_folderId=" + childFolder;
        	vm.parameters = "&p_p_lifecycle=0&p_p_state=maximized&p_p_mode=view&_20_displayStyle=icon&_20_viewEntries=1&_20_viewFolders=1&_20_entryEnd=50&_20_entryStart=0&_20_folderEnd=50&_20_folderStart=0&_20_struts_action=%2Fdocument_library%2Fview&_20_viewEntriesPage=1&_20_action=browseFolder&_20_expandFolder=0";
            vm.finalUrl = vm.baseUrl + vm.parentFolderPar + vm.childFolderPar + vm.parameters;
            $window.open(vm.finalUrl, '_blank');
            console.log(vm.childFolderPar);
        }
    });

}());
