$(function() {
    var $label = $('.activation-type-container-inner > label')[0],
        $labelClass = $($label).find('.label'),
        $smsVerifInput = $('#sms-verification-input-number'),
        $smsVerifButton = $('#sms-verification-input-number-button');

    function getParameterByName(name, url) {
        if (!url) {
            url = window.location.href;
        }
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    var fromProlongChecker = getParameterByName('prolong-checker');

    if (fromProlongChecker) {
        var prolongMsisdn = getParameterByName('msisdn');

        $($labelClass).trigger('click');
        $($smsVerifButton).removeClass('disabled');
        $($smsVerifInput).val(prolongMsisdn);
    }

});
