$(document).ready(function () {

    if (window.matchMedia('(max-width: 480px)').matches) {
        $("#info-data-navyse").attr("src", "../assets/img/icon_info_grey.png");
    } else {}

    if (getParameterByName('data-navyse')) {
        scrollTo('#line-navyse', 50);
    }

    function getParameterByName(name) {
        var found = false;
        if (window.location.href.indexOf(name) > -1) {
            found = true;
        }

        return found;
    }

    $(document).on('click', '.popup-modal-dismiss', function (e) {
        e.preventDefault();
        $.fancybox.close();
    });

    $(document).on('click', '.popup-modal-scroll', function (e) {
        e.preventDefault();
        $.fancybox.close();
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top - ($("#nav-sticky").outerHeight())
                }, 1000);
                return false;
            }
        }
    });

    $('.ec-beta').qtip({
        content: {
            text: 'Služba immmr je zatiaľ v pilotnej prevádzke. Pomôžte nám ju zlepšiť. Svoje podnety posielajte na <a href="mailto:data@telekom.sk">data@telekom.sk</a>'
        },
        hide: {
            event: 'unfocus'
        },
        position: {
            my: 'left center',
            at: 'right center'
        }
    });

    $('.roaming-info').qtip({
        content: 'Susedné štáty: Česko, Maďarsko, Poľsko a Rakúsko.',
        position: {
            corner: {
                target: 'leftMiddle',
                tooltip: 'topMiddle'
            }
        }
    }).bind('click', function (event) {
        event.preventDefault();
        return false;
    });

    $('.tooltip').qtip({
        content: {
            text: 'Vybrané paušály sú:<br> Happy M, L, XL volania, XL, XXL a Profi'
        },
        position: {
            my: 'bottom center',
            at: 'top center'
        }
    });

    $('.happy_xs-info').qtip({
        content: 'Počas pracovných dní v čase od 19.00 hod. do 7.00 hod. Soboty, nedele a štátne sviatky počas celých 24 hodín.',
        position: {
            corner: {
                target: 'leftMiddle',
                tooltip: 'topMiddle'
            }
        }
    }).bind('click', function (event) {
        event.preventDefault();
        return false;
    });

    $('#row-top-more, #order-row-top, #show-happy').click(function () {
        scrollTo('#sec-2', 0);
        return false;
    });

    $('.c-row-1 .c-tile-head img').qtip({
        content: 'Vybraný smartfón ako darček získate k Happy paušálu so Sony Xperia M2.',
        style: {
            'font-size': 12
        },
        position: {
            corner: {
                target: 'leftMiddle',
                tooltip: 'topMiddle'
            }
        }
    });

    $('#info-data100').qtip({
        content: 'Balíček nie je dostupný pre<br>Happy XS mini.',
        style: {
            'font-size': 12
        },
        position: {
            corner: {
                target: 'leftMiddle',
                tooltip: 'topMiddle'
            }
        }
    });

    $('#info-data500').qtip({
        content: 'Balíček nie je dostupný pre<br>Happy XS mini.',
        style: {
            'font-size': 12
        },
        position: {
            corner: {
                target: 'leftMiddle',
                tooltip: 'topMiddle'
            }
        }
    });

    $('#info-data1000').qtip({
        content: 'Balíček nie je dostupný pre<br>Happy XS mini, XS a S.',
        style: {
            'font-size': 12
        },
        position: {
            corner: {
                target: 'leftMiddle',
                tooltip: 'topMiddle'
            }
        }
    });

    $('#info-data-navyse').qtip({
        content: 'Platí pre paušál Happy M a vyšší.',
        position: {
            corner: {
                target: 'leftMiddle',
                tooltip: 'topMiddle'
            }
        }
    });

    $('#info-zdielanie').qtip({
        content: 'Službu Zdieľanie dát z paušálu na tablete či druhom telefóne dostanete ku každému paušálu Happy L a vyššiemu na 24 mesiacov zadarmo.',
        position: {
            corner: {
                target: 'leftMiddle',
                tooltip: 'topMiddle'
            }
        }
    });

    $('.happy-desc .row-5-head img').qtip({
        content: 'S následnými neobmedzenými<br>e-mailmi a surfovaním',
        position: {
            corner: {
                target: 'leftMiddle',
                tooltip: 'topMiddle'
            }
        }
    });


    $('.c-row-5 .box-header img').qtip({
        content: 'Platí pre odchádzajúce roamingové hovory z EÚ do SR za mesačný poplatok 2 €.',
        position: {
            corner: {
                target: 'leftMiddle',
                tooltip: 'topMiddle'
            }
        }
    });


    $('.c-row-6 .c-tile-1 .c-tile-head img').qtip({
        content: 'Zľava sa uplatňuje 20 mesiacov v náležite rozrátanej sume.<br>Platí len pre paušály Happy.',
        position: {
            corner: {
                target: 'leftMiddle',
                tooltip: 'topMiddle'
            }
        }
    });

    $('.happy-desc .row-8-head img').qtip({
        content: 'Jednorazová zľava, ktorú získate iba pri kúpe paušálu na našom webe.<br>Konkrétna výška zľavy bude vypočítaná pri nákupe.',
        position: {
            corner: {
                target: 'leftMiddle',
                tooltip: 'topMiddle'
            }
        }
    });

    $('#info-powerbank').qtip({
        content: 'Externá batéria v kľúčenke:<br>– zabudovaný nabíjací a dátový kábel<br>– kapacita batérie: 1000 mAh<br>– kompaktné rozmery: 55 x 30 x 20 mm<br>– hmotnosť: len 38 gramov',
        position: {
            corner: {
                target: 'leftMiddle',
                tooltip: 'topMiddle'
            }
        }
    }).bind('click', function (event) {
        event.preventDefault();
        return false;
    });

    $('#info-balik-volania, #info-balik-minuty, #info-balik-sms').qtip({
        content: 'Balík aktivujete na webe počas kúpy Happy paušálu.',
        position: {
            corner: {
                target: 'leftMiddle',
                tooltip: 'topMiddle'
            }
        }
    }).bind('click', function (event) {
        event.preventDefault();
        return false;
    });

    $('#zona-svet').qtip({
        //show: 'click',
        hide: 'unfocus',
        content: '<span class="bold">Zóna svet pre program služieb Profi:</span><br>Aljaška, Rakúsko, Belgicko, Bulharsko, Kanada, Chorvátsko, Cyprus, Česká republika, Dánsko, Estónsko, Fínsko, Francúzsko, Nemecko, Grécko, Maďarsko, Island, Írsko, Taliansko, Lotyšsko, Lichtenštajnsko, Litva, Luxembursko, Malta, Holandsko, Nórsko , Poľsko, Portugalsko, Portoriko, Rumunsko, Slovinsko, Španielsko, Švédsko, Veľká Británia, Americké Panenské ostrovy, USA, Vatikán, Austrália, Kostarika, Guam, Hong Kong, Čína, India, Indonézia, Izrael, Japonsko, Malajzia, Mexiko, Monako, Peru, Kórejská republika, Ruská federácia, Singapur, Južná Afrika, Švajčiarsko, Thajsko, Ukrajina, Uzbekistan, Venezuela, Albánska, Angola, Argentína, Bahamy, Bahrajn, Bangladéš, Bielorusko, Bosna a Hercegovina, Brunej, Burundi, Kambodža, Kolumbia,  Egypt, Francúzska Guyana, Gabon, Ghana, Guadeloupe, Guatemala, Chile , Irán, Etiópska federatívna demokratická republika, Kuvajt, Laos, Libanon, Macao, Macedónsko, Martinik, Maurícius, Moldavsko, Mongolsko, Čierna Hora, Namíbia, Holandské Antily, Niger, Nigéria, Severné Mariány, Pakistan, Palestínske územia, Panama, Filipíny , Réunion, San Maríno, Saudská Arábia, Srbsko, Srí Lanka, Sýria, Taiwan, Tadžikistan, Turecko, Spojené arabské emiráty, Vietnam, Jemen, Zambia',
        position: {
            my: 'middle right',
            at: 'bottom left'
        }
    });

    function showContent(id) {
        $('.content-desc').hide();
        $('#' + id).show();

        scrollTo('#' + id, 0);
    }

    $('#sluzby .c-tile-1 .c-tile-more span').click(function (event) {

        event.preventDefault();
        showContent('balicky');

        if (window.matchMedia('(max-width: 600px)').matches) {} else {
            $(".c-row-4 .c-tile").css({
                "border-bottom": "1px solid #d4d4d4"
            });
            $(".c-row-4 .c-tile-1").css({
                "border-bottom": "none"
            });
        }
    });

    $('#sluzby .c-tile-2 .c-tile-more span').click(function (event) {

        event.preventDefault();
        showContent('volania');

        if (window.matchMedia('(max-width: 600px)').matches) {} else {
            $(".c-row-4 .c-tile").css({
                "border-bottom": "1px solid #d4d4d4"
            });
            $(".c-row-4 .c-tile-2").css({
                "border-bottom": "none"
            });

        }

    });

    $('#sluzby .c-tile-3 .c-tile-more span').click(function (event) {

        event.preventDefault();
        showContent('siet100');

        if (window.matchMedia('(max-width: 600px)').matches) {} else {
            $(".c-row-4 .c-tile").css({
                "border-bottom": "1px solid #d4d4d4"
            });
            $(".c-row-4 .c-tile-3").css({
                "border-bottom": "none"
            });

        }

    });

    $('#sluzby .c-tile-4 .c-tile-more span').click(function (event) {

        event.preventDefault();
        showContent('sms');

        if (window.matchMedia('(max-width: 600px)').matches) {} else {
            $(".c-row-4 .c-tile").css({
                "border-bottom": "1px solid #d4d4d4"
            });
            $(".c-row-4 .c-tile-4").css({
                "border-bottom": "none"
            });

        }
    });

    $('#orderphone').click(function () {
        happyLink('/objednavka/-/scenario/e-shop/hw-happy/vyber-hw');
        return false;
    });
    $('#orderprenos').click(function () {
        happyLink('/volania/prenos-cisla');
        return false;
    });
    $('#happyroaming').click(function () {
        happyLink('/volania/roaming#happyroaming ');
        return false;
    });

    $('.c-row-7 .box-1').click(function () {
        happyLink('/objednavka/-/scenario/e-shop/happy');
        return false;
    });
    $('.c-row-7 .box-2').click(function () {
        happyLink('/objednavka/-/scenario/e-shop/happy');
        return false;
    });
    $('.c-row-7 .box-3-a').click(function () {
        happyLink('/objednavka/-/scenario/e-shop/happy');
        return false;
    });
    $('.c-row-7 .box-3-b').click(function () {
        happyLink('/objednavka/-/scenario/e-shop/happy');
        return false;
    });

    function happyLink(url) {
        window.location.href = url;
    }

    $('.table-container').animate({
        scrollLeft: '+=395'
    }, 1);

    //790

    var actual = 2;
    var child = 2;

    $('#next-column').click(function (event) {

        $('#previous-img').attr('src', '../assets/img/left.png');

        event.preventDefault();
        $('.table-container').animate({
            scrollLeft: '+=395'
        }, 'slow');

        actual = actual + 1;

        //console.log(actual);

        if (actual >= 4) {
            $('#next-img').attr('src', '../assets/img/right-cb.png');

            actual = 4;
            child = 4;

        } else if (actual == 3) {

            child = 3;
        }

    });

    $('#previous-column').click(function (event) {

        event.preventDefault();
        $('.table-container').animate({
            scrollLeft: '-=395'
        }, 'slow');

        $('#next-img').attr('src', '../assets/img/right.png');

        actual = actual - 1;

        //console.log(actual);

        if (actual < 2) {
            $('#previous-img').attr('src', '../assets/img/left-cb.png');

            actual = 1;
            child = 1;

        } else if (actual == 2) {
            child = 2;
        } else if (actual == 3) {
            child = 3;
        } else if (actual == 4) {
            child = 4;
        }

    });

    var scrollingSpeed = 1200;

    $('#btn-order-4g').click(function () {
        scrollTo('.c-row-4', 0);
        return false;
    });

    $('#order-top').click(function () {
        scrollTo('.c-row-table', 0);
        return false;
    });

    $('.c-row-2 .blue').click(function () {
        scrollTo('.c-row-7', 0);
        return false;
    });

    $('.c-row-2 .header-bottom img').click(function () {
        scrollTo('.c-row-6', 0);
        return false;
    });

    function scrollTo(id, height) {

        if (id === '#balicky' || id === '#volania' || id === '#siet100' || id === '#sms') {
            return false;
        }

        height = height ? height : 0;
        $('html,body').animate({
                scrollTop: $(id).offset().top - height
            },
            scrollingSpeed,
            function () {
                disabled = false;
            }
        );
    }

    $("a#activate-order100").fancybox({
        'hideOnContentClick': true
    });

    $("#activate-order500").fancybox({
        'hideOnContentClick': true
    });

    $("#activate-order1000").fancybox({
        'hideOnContentClick': true
    });


    $("a#info-order1000a").fancybox({
        'hideOnContentClick': true
    });

    $("#info-order100").fancybox({
        'hideOnContentClick': true
    });

    $("#info-order500").fancybox({
        'hideOnContentClick': true
    });

    $("#info-order1000").fancybox({
        'hideOnContentClick': true
    });

    $(".lightbox").fancybox();

    $("#nav-sticky").sticky({
        topSpacing: 0,
        widthFromWrapper: true
    });

    $(".scroll-to").click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top - ($("#nav-sticky").outerHeight())
                }, 1000);
                return false;
            }
        }
    });

    var contentSections = $("[id*='sec-']"),
        secondaryNav = $("#nav-sticky");

    function updateSecondaryNavigation() {
        contentSections.each(function () {
            var actual = $(this),
                actualHeight = actual.height() + parseInt(actual.css('paddingTop').replace('px', '')) + parseInt(actual.css('paddingBottom').replace('px', '')),
                actualAnchor = secondaryNav.find('a[href="#' + actual.attr('id') + '"]');

            if ((actual.offset().top - secondaryNav.outerHeight() <= $(window).scrollTop()) && (actual.offset().top + actualHeight - secondaryNav.outerHeight() > $(window).scrollTop())) {
                actualAnchor.addClass('active');
            } else {
                actualAnchor.removeClass('active');
            }

        });
    }

    updateSecondaryNavigation();

    $(window).scroll(function (event) {
        updateSecondaryNavigation();
    });

    var orderUrl = '/mam-zaujem/volania/happy/formular?pausal=';

    $('#order-happy-s').click(function (event) {

        //event.preventDefault();

        if (window.matchMedia('(max-width: 600px)').matches) {
            $(this).attr("href", orderUrl + 'Happy S');
        } else {}

    });

    $('#order-happy-m').click(function (event) {

        //event.preventDefault();

        if (window.matchMedia('(max-width: 600px)').matches) {
            $(this).attr("href", orderUrl + 'Happy M');
        } else {}

    });

    $('#order-happy-l').click(function (event) {

        //event.preventDefault();

        if (window.matchMedia('(max-width: 600px)').matches) {
            $(this).attr("href", orderUrl + 'Happy L');
        } else {}

    });

    $('#order-happy-xl-call').click(function (event) {

        //event.preventDefault();

        if (window.matchMedia('(max-width: 600px)').matches) {
            $(this).attr("href", orderUrl + 'Happy XL Volania');
        } else {}

    });

    $('#order-happy-xl').click(function (event) {

        //event.preventDefault();

        if (window.matchMedia('(max-width: 600px)').matches) {
            $(this).attr("href", orderUrl + 'Happy XL');
        } else {}

    });

    $('#order-happy-xs-mini').click(function (event) {

        //event.preventDefault();

        if (window.matchMedia('(max-width: 600px)').matches) {
            $(this).attr("href", orderUrl + 'Happy XS mini');
        } else {}

    });

    $('#order-happy-xs').click(function (event) {

        //event.preventDefault();

        if (window.matchMedia('(max-width: 600px)').matches) {
            $(this).attr("href", orderUrl + 'Happy XS');
        } else {}

    });

    $('#order-happy-xxl').click(function (event) {

        //event.preventDefault();

        if (window.matchMedia('(max-width: 600px)').matches) {
            $(this).attr("href", orderUrl + 'Happy XXL');
        } else {}

    });

    $('#order-happy-profi').click(function (event) {

        //event.preventDefault();

        if (window.matchMedia('(max-width: 600px)').matches) {
            $(this).attr("href", orderUrl + 'Happy Profi');
        } else {}

    });

});
