$(document).ready(function () {
  // document.body.scrollTop = 0;
  // document.documentElement.scrollTop = 0;

  initSlider("#vyhody_slider");

  initSlider("#tab-1 .slider");

  $(".info").each(function () {
    $(this).qtip({
      content: {
        button: "Close",
        text: $(this).attr("alt"),
      },
      style: {
        classes: "qtip-new",
      },
      position: {
        corner: {
          target: "leftMiddle",
          tooltip: "topMiddle",
        },
        viewport: $(window),
      },
    });
  });

  $("#nav-sticky").sticky({
    topSpacing: 0,
    widthFromWrapper: true,
  });

  $(".scroll-to").click(function (e) {
    var target = $(this.hash);
    target = target.length ? target : $("." + this.hash.slice(1));
    if (target.length) {
      $("html,body").animate(
        {
          scrollTop: target.offset().top - $("#nav-sticky").outerHeight(),
        },
        1000
      );
      return false;
    }
  });

  if (window.location.hash) {
    var target = window.location.hash;

    if (target.length > 1) {
      setTimeout(function () {
        $("html,body").animate(
          {
            scrollTop:
              $("." + target.substr(1)).offset().top -
              $("#nav-sticky").outerHeight(),
          },
          500
        );
      }, 750);
    }
  }

  var contentSections = $("[id*='sec-']"),
    secondaryNav = $("#nav-sticky");

  function updateSecondaryNavigation() {
    contentSections.each(function () {
      var actual = $(this),
        actualHeight =
          actual.height() +
          parseInt(actual.css("paddingTop").replace("px", "")) +
          parseInt(actual.css("paddingBottom").replace("px", "")),
        actualAnchor = secondaryNav.find(
          'a[href="#' + actual.attr("id") + '"]'
        );

      if (
        actual.offset().top - secondaryNav.outerHeight() <=
          $(window).scrollTop() &&
        actual.offset().top + actualHeight - secondaryNav.outerHeight() >
          $(window).scrollTop()
      ) {
        actualAnchor.addClass("active");
      } else {
        actualAnchor.removeClass("active");
      }
    });
  }
  updateSecondaryNavigation();

  $(window).scroll(function (event) {
    updateSecondaryNavigation();
  });

  /* TABS */
  $(".tabs-menu a").click(function (event) {
    event.preventDefault();
    var li = $(this).parent();
    var ul = $(this).closest("ul");
    li.addClass("current");
    li.siblings().removeClass("current");
    var tab = $(this).attr("href"), // select clicked tab. In example "#tab-1" ..
      parent = ul.parent().parent().parent(); // <tabs-container>
    parent.find(".tab-content").not(tab).css("display", "none"); //hide all not clicked tabs
    $(tab).fadeIn(); //show clicked tab

    if (ul.find(".step-ico-ok").length) {
      if (li.index() === 2) {
        ul.find("li:eq(1)")
          .find(".step-ico")
          .hide()
          .end()
          .find(".step-ico-ok")
          .show();
        console.log(2);
      } else if (ul.find("li:eq(1) .step-ico-ok").is(":visible")) {
        ul.find("li:eq(1)")
          .find(".step-ico-ok")
          .hide()
          .end()
          .find(".step-ico")
          .show();
      }
    }
    setTimeout(function () {
      initSlider(tab + " .slider");
    }, 50);
  });
  /* TABS END */

  $("#m1_data").change(function () {
    $("#sec-7 .data span").text($(this).val());
  });

  $("#show_legal").click(function () {
    $(this).hide();
    $("#legal_more").show(200);
  });

  /* TOGGLE ARROW */
  $(".toggle-arrow, .arrow-right").click(function (event) {
    event.preventDefault();
    if ($(this).hasClass("arrow-right")) {
      return;
    }
    $(event.target).closest(".togleArrow").find(".arrow-content").toggle(200);
    $(this).find(".arrow-right").toggleClass("arrow-rotate");
  });
  /* TOGGLE ARROW END */
});

$(".tab-acc-sub").click(function () {
  var href = $(this).find("a").attr("href");
  window.location = href;
});

$("#sec-5 .all-benefits").click(function (e) {
  e.preventDefault();
  var comps = $("#sec-5 .hide-mobil");
  if (comps.is(":visible")) {
    comps.hide(200);
    $(this).text("Ďaľšie výhody");
  } else {
    comps.show(200);
    $(this).text("Menej výhod");
  }
});

function initSlider(comp) {
  if (!$(comp).hasClass("slick-initialized")) {
    $(comp).slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      arrows: true,
      infinite: false,
      swipeToSlide: true,
      swipe: true,
      touchMove: true,
      draggable: true,
      dots: true,
      responsive: [
        {
          breakpoint: 800,
          settings: {
            slidesToShow: 2,
          },
        },
        {
          breakpoint: 503,
          settings:
            comp === "#vyhody_slider"
              ? "unslick"
              : {
                  slidesToShow: 1,
                },
        },
      ],
    });
  }
}


//popup
$(".chri_popup").on("click", function() {
  var clickedBtn = $(this).attr("href");
  $.fancybox([
    {
      href: clickedBtn,
      padding: 0,
      margin: 30,
      closeBtn: false,
      width: 550,
      height: "auto",
      // autoDimensions: false,
      autoSize: false,
      parent: "#content",
      helpers: {
        overlay: {
          css: {
            background: "rgba(0, 0, 0, 0.7)"
          }
        }
      }
    }
  ]);
});

$(".p-close, .link-close, .text-close, .close-btn").on("click", function(e) {
  e.preventDefault();
  $.fancybox.close();
});