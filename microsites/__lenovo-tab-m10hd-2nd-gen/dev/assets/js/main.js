  /* GET PARAMETERS FROM URL */
  function getUrlParameter(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)");
    var results = regex.exec(location.search);
    return results === null
      ? ""
      : decodeURIComponent(results[1].replace(/\+/g, " "));
  }
  /* END GET PARAMETERS FROM URL */


$(document).ready(function () {
  /* TABS */
  $(".tabs-menu a").click(function(event) {
    event.preventDefault();
    $(this)
      .parent()
      .addClass("current");
    $(this)
      .parent()
      .siblings()
      .removeClass("current");
    var tab = $(this).attr("href"), // select clicked tab. In example "#tab-1" ..
      parent = $(this)
        .closest("ul")
        .parent()
        .parent(); // <tabs-container>
    parent
      .find(".tab-content")
      .not(tab)
      .css("display", "none"); //hide all not clicked tabs
    $(tab).fadeIn(); //show clicked tab
  });

  /* TOOLTIPS */
  $("#info-discount").qtip({
    content:
      "Zľavu až do 30 € získate na nové zariadenie pri prechode a predĺžení viazanosti. Zľava sa uplatní po overení objednávky agentom, ktorý Vás bude kontaktovať po jej odoslaní.",
    style: {
      classes: "qtip-tipsy",
    },

    position: {
      my: "bottom center",
      at: "top center",
    },
  });

  $("#info-zlava").qtip({
    content:
      "Ak si kúpite Magio internet a TV cez web, na každú službu vám dáme extra zľavu 10 €. Ak si vezmete aj pevnú linku, môžete získať zľavu až 30 €. Zľava sa rozdelí napoly a odráta 2. a 3. mesiac z mesačného poplatku.",
    style: {
      classes: "qtip-tipsy",
    },

    position: {
      my: "bottom center",
      at: "top center",
    },
  });


function getPrice() {
  if (pricelist.is_visible == 0){
    $("#soldOut").show();
  } else {
    $("#" + selectedRp).click().focus(); // SET DEFAULT RATEPLAN
  }
}
function priceTab(){

  if ($("#b2c-all-discount").enhancedSwitch('state')){
    $("#b2c-all-rc").html(
      ((pricelist.prices[selectedRp].rc_sale_web).toFixed(2)).replace(".", ",") + " € <span class='monthly'> mesačne po zľave</span>"
    );   
    $("#b2c-all-oc").html(
      ((pricelist.prices[selectedRp].oc_sale_web).toFixed(2)).replace(".", ",") + " € <span class='monthly'> akontácia po zľave</span>"
    );
    if (pricelist.prices[selectedRp].oc_sale_web < pricelist.prices[selectedRp].oc ){
      $("#b2c-all-normal-oc").html(
        ((pricelist.prices[selectedRp].oc).toFixed(2)).replace(".", ",") + " €"
      );
    }
    if (pricelist.prices[selectedRp].rc_sale_web < pricelist.prices[selectedRp].rc ){
      
      $("#b2c-all-normal-rc").html(
        ((pricelist.prices[selectedRp].rc).toFixed(2)).replace(".", ",")   + " €"
      );
    }
    $(".normal-price").fadeIn();
 
  } else {
    $(".normal-price").hide();
    $("#b2c-all-rc").html(
      ((pricelist.prices[selectedRp].rc).toFixed(2)).replace(".", ",") + " € <span class='monthly'> mesačne</span>"
    );
    $("#b2c-all-oc").html(
      ((pricelist.prices[selectedRp].oc).toFixed(2)).replace(".", ",") + " € <span class='monthly'> akontácia</span>"
    );
  }

}  

/* SET NEW PRICES ON PAGE */
function setNewPrices() {
  priceTab();
    $("#b2c-rpln-price-sale").text(
      ((pricelist.prices[selectedRp].rpln.tax_price - 5).toFixed(2)).replace(".", ",") + " € s online zľavou"        
    );
    $("#b2c-rpln-price").text(
      (pricelist.prices[selectedRp].rpln.tax_price.toFixed(2)).replace(".", ",") + " € zvyšné mesiace"
    );    
}

/*  TOGLE RPLN   */
$(".my-rpln").click(function () {
  selectedRp = $(this).prop("id"); // GET NEW VALUE FROM BUTTON
  if (($(this).attr("category")) == "adsl") {
    selectedRp = $(this).attr("data-value"); 
  }
  $(".my-rpln").removeClass("active-rpln");
  $(this).addClass("active-rpln");
  setNewPrices();
});

/*  NEW SWITCHER   */
$(".switch").enhancedSwitch(); 
$(".switch").click(function() {
  var selectedSwitch = $(this);
  selectedSwitch.enhancedSwitch('toggle');
  setNewPrices(); 
});

/*  TO PREPARE GLOBAL VARIABLE */
  var selectedRp = "";
/*  GET URL PARAMETERS */
  selectedRp = getUrlParameter("selectedrp"); // GET DATA FROM URL
/*  START AND SET DEFAULT VALUES */  
  $("#b2c-all-discount").enhancedSwitch('setTrue');
  if (selectedRp == "") {
    selectedRp = "L";
  } 
  getPrice();

});
