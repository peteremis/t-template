var pricelist = {
  is_orderable: 1,
  is_visible: 1,
  prices: {
    M: {
      oc: 79.0,
      oc_sale_web: 49.0,
      rc: 4,
      rc_sale_web: 4,
      rpln: {
        taxless_price: 9.08,
        tax_price: 10.9,
      },
    },
    L: {
      oc: 59.0,
      oc_sale_web: 29.0,
      rc: 4,
      rc_sale_web: 4,
      rpln: {
        taxless_price: 13.25,
        tax_price: 15.9,
      },
    },
    XL: {
      oc: 19.0,
      oc_sale_web: 1.0,
      rc: 4,
      rc_sale_web: 3.5,
      rpln: {
        taxless_price: 17.41,
        tax_price: 20.9,
      },
    },
  },
};
