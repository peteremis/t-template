var mousetimeout;
var screensaver_active = false;
var idletime = 10000;

var global = new Object();

global.url = "https://m.telekom.sk/selfcare/";
global.staffino = "telekom-centrum-aupark-bratislava-0001";

jQuery(document).ready(function($) {
    $(".login-link").eq(0).hide();
    $('#slf-mobil').hide();
    $('#slf-spotreba').hide();

    id = getParameterByName('id');
    loadShop(id);

    //$(document).idle({
    //  onIdle: function(){
    //      $('#selfcare').fadeOut();
    //      $('#screensaver').fadeIn();
    //      screensaver_animation();
    //  },
    //  onActive: function(){
    //      $('#screensaver').fadeOut();
    //      $('#selfcare').fadeIn();
    //  },
    //  idle: idletime
    //});

});

function getParameterByName(name) {
    var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
    return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
}


function loadShop(id) {
    $.ajax({
        type: "GET",
        async: true,
        crossDomain: true,
        dataType: "json",
        data: "",
        url: global.url + 'shopConfig.php',

        success: function(data) {

            if (typeof data[id] !== 'undefined') {
                global.staffino = data[id]['staffinoUri'];

                if (id=='07.BA.TMSK') {
                    $('#slf-faktury').hide();
                }

            } else {
                $('#slf-staffino').hide();
            }

        },
        error: function() {
            alert("Nepodarilo sa načítať informácie, skúste to prosím ešte raz.");
        }
    });
}

function screensaver_animation() {
    $('#ajaxLoadingSpinnerOverlay').hide();

    $('.pics').cycle({
        fx: 'fade',
        width: null,
        height: 'auto',
        timeout: 10000
    });
}

$(function() {
    FastClick.attach(document.body);
});

function updateClock() {
    var currentTime = new Date();
    //var options = {month: 'long', day: 'numeric', hour: 'numeric', minute: 'numeric'};
    var options = { month: 'long', day: 'numeric', hour: 'numeric', minute: 'numeric', second: 'numeric' };

    $("#slf-datum").html(currentTime.toLocaleString('sk-SK', options));

}

function showApp(app, type) {

    if (app == 'aplikacia') {
        $("#slf-iframe-content").attr("src", "https://www.telekom.sk/telekom-aplikacia/predajna?zdroj=selfcarezona&button=aplikacia");

    } else if (app == 'mobil') {
        $("#slf-iframe-content").attr("src", "https://www.telekom.sk/mobilkazdyrok/predajna");

    } else if (app == 'spotreba') {
        $("#slf-iframe-content").attr("src", "https://m.telekom.sk/prihlasenie/selfcare");

    } else if (app == 'faktury') {
        $("#slf-iframe-content").attr("src", "https://m.telekom.sk/prihlasenie/selfcare?url=https://m.telekom.sk/faktury/selfcare?zdroj=selfcarezona&button=faktura");

    } else if (app == 'staffino') {
        $("#slf-iframe-content").attr("src", "https://staffino.com/sk/" + global.staffino + "?kiosk=1");

    } else if (app == 'disclaimer') {
        $("#slf-iframe-content").attr("src", "https://m.telekom.sk/selfcare/disclaimer.html");

    } else if (app == 'lsb') {
        $("#slf-iframe-content").attr("src", "https://www.telekom.sk/samoobsluzna-zona?zdroj=selfcarezona&button=objednavka");
        //$("#slf-lsb img").attr("src","/documents/10179/620924/dokoncenie2.png");
    } else if (app == 'agent') {
        $("#slf-iframe-content").attr("src", "https://www.telekom.sk/agent?zdroj=selfcarezona&button=lsb");

    } else if (app == 'www') {
        $("#slf-iframe-content").attr("src", "https://www.telekom.sk/?zdroj=selfcarezona&button=telekom");

    } else if (app == 'predpredaj') {
        $("#slf-iframe-content").attr("src", "https://predpredaj.telekom.sk/telekom");
    }

    $("#slf-slogan").hide();
    $('#selfcare').css("background-image", "none");
    $("#slf-iframe-box").show();

}

$(document).ready(function() {
    setInterval('updateClock()', 1000);

    $('#slf-iframe-content').on('load', function() {
        $('#ajaxLoadingSpinnerOverlay').hide();

        // $("#slf-iframe-content").contents().find("#header").hide();
        $("#slf-iframe-content").contents().find("#footer").remove();
    });

    // $("#slf-iframe-content").contents().find(".toe").click(function() {
    //     $("#slf-iframe-content").contents().find("#header").show();
    // });



    $("#slf-spotreba").click(function() {
        $('#ajaxLoadingSpinnerOverlay').show();
        showApp('spotreba', '');
    });

    $("#slf-aplikacia").click(function() {
        $('#ajaxLoadingSpinnerOverlay').show();
        showApp('aplikacia', '');
    });

    $("#slf-mobil").click(function() {
        $('#ajaxLoadingSpinnerOverlay').show();
        showApp('mobil', '');
    });

    $("#slf-staffino").click(function() {
        $('#ajaxLoadingSpinnerOverlay').show();
        showApp('staffino', '');
    });

    $("#slf-faktury").click(function() {

        $('#ajaxLoadingSpinnerOverlay').show();
        showApp('faktury', '');
    });

    $("#slf-lsb").click(function() {
        $('#ajaxLoadingSpinnerOverlay').show();
        showApp('lsb', '');
    });

    $("#suhlas").click(function(event) {
        event.preventDefault();
        $('#ajaxLoadingSpinnerOverlay').show();
        showApp('disclaimer', '');
    });

    $("#slf-datum").click(function() {
        $('#ajaxLoadingSpinnerOverlay').show();
        showApp('agent', '');
    });

    $("#slf-www").click(function() {
        $('#ajaxLoadingSpinnerOverlay').show();
        showApp('www', '');
    });

    $("#slf-predpredaj").click(function() {
        $('#ajaxLoadingSpinnerOverlay').show();
        showApp('predpredaj', '');
    });



    $("#slf-domov").click(function() {
        $('#ajaxLoadingSpinnerOverlay').show();
        //$("#slf-lsb img").attr("src","/documents/10179/620924/dokoncenie.png");
        $('#selfcare').css("background-image", "url(https://www.telekom.sk/documents/10179/620924/img10.png)");
        $("#slf-slogan").show();
        $("#slf-iframe-box").hide();
        $('#ajaxLoadingSpinnerOverlay').hide();
    });

    $("#slf-back").click(function() {
        document.getElementById('iframe').history.back(-1);
    });

    //showApp('mobil', '');

});
