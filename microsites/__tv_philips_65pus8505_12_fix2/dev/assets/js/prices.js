var pricelist = {
  is_orderable: 1,
  is_visible: 1,
  prices: {
    M: {
      oc: 369.0,
      oc_sale_web: 339.0,
      rc: 18,
      rc_sale_web: 18,
      rpln: {
        taxless_price: 9.08,
        tax_price: 10.9,
      },
    },
    L: {
      oc: 269.0,
      oc_sale_web: 239.0,
      rc: 17,
      rc_sale_web: 17,
      rpln: {
        taxless_price: 13.25,
        tax_price: 15.9,
      },
    },
    XL: {
      oc: 189.0,
      oc_sale_web: 159.0,
      rc: 17,
      rc_sale_web: 17,
      rpln: {
        taxless_price: 17.41,
        tax_price: 20.9,
      },
    },
  },
};
