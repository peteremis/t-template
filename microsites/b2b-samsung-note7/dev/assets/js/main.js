$(function () {
    var $control = $('.device-controls > a'),
        $content = $('.device-controls').children(),
        $control__holder_up = $('.device-controls_upper');


    $($control).on('click', function (e) {
        e.preventDefault();
        $('.device_img img').attr('src', $(this).children('img').attr('src'));
    });



    if (($(window).innerWidth()) < 600) {

        $content.appendTo($control__holder_up);

        var $control_up = $('.device-controls_upper > a');

        $($control_up).on('click', function (e) {
            e.preventDefault();
            $('.device_img img').attr('src', $(this).children('img').attr('src'));
            console.log(this);
        });

    }

    $(".scroll-to").click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top - ($(".product__nav").outerHeight())
                }, 1000);
                return false;
            }
        }
    });

    $('.fancy').fancybox();

});