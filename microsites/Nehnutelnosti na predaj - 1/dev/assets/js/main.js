$(function() {

/* MAGNIFIC POPUP - START */
	$('.zoom-gallery').magnificPopup({
		delegate: 'a',
		type: 'image',
		closeOnContentClick: false,
		closeBtnInside: false,
		mainClass: 'mfp-with-zoom mfp-img-mobile',
		image: {
			verticalFit: true,
			// titleSrc: function(item) {
			// 	return item.el.attr('title') + ' &middot; <a class="image-source-link" href="'+item.el.attr('data-source')+'" target="_blank">image source</a>';
			// }
		},
		gallery: {
			enabled: true,
      arrowMarkup: '<button title="%title%" type="button" class="mfp-arrow mfp-arrow-%dir%"></button>', // markup of an arrow button
      tPrev: 'Previous (Left arrow key)',
      tNext: 'Next (Right arrow key)',
		},
		zoom: {
			enabled: true,
			duration: 300, // don't foget to change the duration also in CSS
			opener: function(element) {
				return element.find('img');
			}
		}

	});
/* MAGNIFIC POPUP - END */



/* NEHNUTELNOST - DATA JSON START */


/* click printpage */
$("#printpage").click(function(e){
	if (nehnutelnosti.status) {
		e.preventDefault();
		swal('Ejha...', 'Táto nehnuteľnosť je už predaná!', 'error')
		return;
	} else {
		window.print();
	}
});
/* click printpage END */

/* poslat ponuku na email */
$('#mailto').click( function(e){
	if (nehnutelnosti.status) {
		e.preventDefault();
		swal('Ejha...', 'Táto nehnuteľnosť je už predaná!', 'error')
		return;
	} else {
		$(this).attr("href","mailto:komu?subject=Nehnutelnost&body=" + window.location.href)
	}
});
/* poslat ponuku na email END */


var nehnutelnosti;

				$.getJSON('assets/js/data.json', function(data) {
					//console.log(data.nehnutelnost);
							nehnutelnosti = {
								"status" : data.nehnutelnost.predane,
								"propertyName" : data.nehnutelnost.nazov
							};

							/* hide buttons if predane */
							if (nehnutelnosti.status) {
								$(".table-options").hide();
							}

							/* predane / nepredane status */
							var getNazov = nehnutelnosti.propertyName;
								if (nehnutelnosti.status) {
									$('.sold').show();
									$('#badgePredane').show();
									var $showNazov = $('#nazov').find('#nazovNehnutelnosti').text(getNazov + "\xa0");
									$showNazov.css('text-decoration','line-through');
								} else {
									$('.sold').hide();
									$('#badgePredane').hide();
									var $showNazov = $('#nazov').find('#nazovNehnutelnosti').text(getNazov + "\xa0");
								}


							// gallery images from json
							var galleryLength = data.gallery.length;
							for(var i = 0; i < galleryLength; i++) {
							    html = $([
										'<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">',
							      '<a id="img1" href="../assets/img/' + data.gallery[i].img + '" data-source="" title="">',
							      '<img id="img1thumb" style="max-width: 100%;" src="../assets/img/' + data.gallery[i].imgthumb + '">',
							      '</a>',
										'</div>'
							    ].join("\n"));
							    $(html).appendTo(".zoom-gallery");
							};

							//kontakt data from json
							var kontaktLength = data.kontakty.length;
							for(var i = 0; i < kontaktLength; i++) {
							    kontakt = $([
							        '<tr>',
							          '<td>' + data.kontakty[i].kontakt + '</td>',
							          '<td><a href="' + data.kontakty[i].kontaktMailto + '">' + data.kontakty[i].kontaktMail + '</a></td>',
							        '</tr>'
							    ].join("\n"));
							    $(kontakt).appendTo("#table-contact");
							}

							//hlavne info
							//document.getElementById("nazov").innerHTML = data.nehnutelnost.nazov; //nahradene skriptom predane / nepredane status
							document.getElementById("info").innerHTML = data.nehnutelnost.info;
							document.getElementById("mapLink").href = data.nehnutelnost.map;

							//katastralne udaje
							document.getElementById("okres").innerHTML = data.nehnutelnost.okres;
							document.getElementById("obec").innerHTML = data.nehnutelnost.obec;
							document.getElementById("uzemie").innerHTML = data.nehnutelnost.uzemie;
							document.getElementById("lvlastnictva").innerHTML = data.nehnutelnost.lvlastnictva;
							document.getElementById("vpodiel").innerHTML = data.nehnutelnost.vpodiel;

							//main img a podrobnosti
							document.getElementById("mainImg").src = '../assets/img/' + data.nehnutelnost.mainImg + '';
							document.getElementById("vlastnik").innerHTML = data.nehnutelnost.vlastnik;
							document.getElementById("ulica").innerHTML = data.nehnutelnost.ulica;
							document.getElementById("rok").innerHTML = data.nehnutelnost.rok;
							document.getElementById("rozloha").innerHTML = data.nehnutelnost.rozloha;
							document.getElementById("plocha").innerHTML = data.nehnutelnost.plocha;
							document.getElementById("budovy").innerHTML = data.nehnutelnost.budovy;
							document.getElementById("elPripojka").innerHTML = data.nehnutelnost.elPripojka;
							document.getElementById("vodovPripoj").innerHTML = data.nehnutelnost.vodovPripoj;
							document.getElementById("kanal").innerHTML = data.nehnutelnost.kanal;
							document.getElementById("plyn").innerHTML = data.nehnutelnost.plyn;
							document.getElementById("bremeno").innerHTML = data.nehnutelnost.bremeno;
							document.getElementById("najom").innerHTML = data.nehnutelnost.najom;
				});

});
