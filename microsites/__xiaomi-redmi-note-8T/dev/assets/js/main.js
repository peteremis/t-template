$(document).ready(function() {
  //back to to previous url
  $("#a_referrer a").attr("href", document.referrer);

  /* TABS */
  $(".tabs-menu a").click(function(event) {
    event.preventDefault();
    $(this)
      .parent()
      .addClass("current");
    $(this)
      .parent()
      .siblings()
      .removeClass("current");
    var tab = $(this).attr("href"), // select clicked tab. In example "#tab-1" ..
      parent = $(this)
        .closest("ul")
        .parent()
        .parent(); // <tabs-container>
    parent
      .find(".tab-content")
      .not(tab)
      .css("display", "none"); //hide all not clicked tabs
    $(tab).fadeIn(); //show clicked tab
  });

  /* TOOLTIP */
  $("#info-akontacia").qtip({
    content:
      "Akontácia je úvodná suma, ktorú zaplatíte pri kúpe v predajni alebo cez web.",
    style: {
      classes: "qtip-tipsy"
    },

    position: {
      my: "bottom center",
      at: "top center"
    }
  });
  $("#info-akontacia-b2b").qtip({
    content:
      "Akontácia je úvodná suma, ktorú zaplatíte pri kúpe v predajni alebo cez web.",
    style: {
      classes: "qtip-tipsy"
    },

    position: {
      my: "bottom center",
      at: "top center"
    }
  });

  /* TOGGLE ARROW */
  $(".toggle-arrow, .arrow-right").click(function(event) {
    event.preventDefault();
    if ($(this).hasClass("arrow-right")) {
      return;
    }
    $(event.target)
      .closest(".togleArrow")
      .find(".arrow-content")
      .toggle(200);
    $(this)
      .find(".arrow-right")
      .toggleClass("arrow-rotate");
  });

  $(".toggle-arrow-main, .arrow-right-main").click(function(event) {
    event.preventDefault();
    if ($(this).hasClass("arrow-right-main")) {
      return;
    }
    $(event.target)
      .closest(".togleArrow-main")
      .find(".arrow-content-main")
      .toggle(200);
    $(this)
      .find(".arrow-right-main")
      .toggleClass("arrow-rotate");
  });
  /* TOGGLE ARROW END */

  /* SCROLL */
  $(".scroll-to").click(function() {
    if (
      location.pathname.replace(/^\//, "") ==
        this.pathname.replace(/^\//, "") &&
      location.hostname == this.hostname
    ) {
      var target = $(this.hash);
      target = target.length ? target : $("[name=" + this.hash.slice(1) + "]");
      if (target.length) {
        $("html,body").animate(
          {
            scrollTop:
              target.offset().top - ($("#nav-sticky").outerHeight() - 10)
          },
          1000
        );
        return false;
      }
    }
  });

  /* GET PARAMETERS FROM URL */
  function getUrlParameter(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)");
    var results = regex.exec(location.search);
    return results === null
      ? ""
      : decodeURIComponent(results[1].replace(/\+/g, " "));
  }

  /* FETCH DATA FROM JSON */

  var dataURL = "";
  if (window.location.hostname === "localhost") {
    dataURL = "../assets/js/data.json";
  } else {
    dataURL = "https://www.telekom.sk/documents/10179/16511938/data.json"; // SET JSON URL
  }

  function setPrice2(myrpln) {
    $("#" + myrpln)
      .click()
      .focus(); // SET DEFAULT RATEPLAN (CLICK ON THE BUTTON)
  }

  /* SET NEW PRICES ON PAGE */
  function setNewPrices(newRpln) {
    // EEC //

    $("#eec").attr("src", pricelist.prices[newRpln].eyecatcherURL);
    if (newRpln == "anoS") {
      var my_rp = "S";
    }
    if (newRpln == "anoM") {
      var my_rp = "M";
    }
    if (newRpln == "anoMdata") {
      var my_rp = "M_DATA";
    }
    if (newRpln == "anoL") {
      var my_rp = "L";
    }
    if (newRpln == "anoXL") {
      var my_rp = "XL";
    }
    if (newRpln == "anoXXL") {
      var my_rp = "XXL";
    }
    $("#buyBtn").attr("href", pricelist.buyBtnURL + "?vyberrp=" + my_rp); //SET URL TO BUY BUTTON
    if (pricelist.prices[newRpln].oneTimeStore == null) {
      $("#oneTimeWebPrice").text(
        pricelist.prices[newRpln].oneTimeWeb.toFixed(2) + " €"
      );
      $("#oneTimeRegularPrice").hide();
    } else {
      $("#oneTimeWebPrice").text(
        pricelist.prices[newRpln].oneTimeWeb.toFixed(2) + " € s web zľavou"
      );
      $("#oneTimeRegularPrice").text(
        pricelist.prices[newRpln].oneTimeStore.toFixed(2) + " €"
      );
      $("#oneTimeRegularPrice").show();
    }

    if (pricelist.prices[newRpln].monthlyStore == null) {
      $("#MonthylFeeMobile").text(
        pricelist.prices[newRpln].monthlyWeb.toFixed(2) + " €"
      );
      $("#MonthlyRegularPrice").hide();
    } else {
      $("#MonthylFeeMobile").text(
        pricelist.prices[newRpln].monthlyWeb.toFixed(2) + " € s web zľavou"
      );
      $("#MonthlyRegularPrice").text(
        pricelist.prices[newRpln].monthlyStore.toFixed(2) + " €"
      );
      $("#MonthlyRegularPrice").show();
    }
    $("#MonthlyFeeService").text(
      pricelist.prices[newRpln].rplnPrice.toFixed(2) + " € mesačne"
    );
  }

  var pricelist = {
    buyBtnURL:
      "https://www.telekom.sk/mob/objednavka/-/scenario/e-shop/hw-pausal/vyber-hw/xiaomi_redmit_note_8t_64gb",
    prices: {
      anoS: {
        rpAvailability: true,
        monthlyWeb: 2,
        monthlyStore: null,
        oneTimeWeb: 139,
        oneTimeStore: 149,
        rplnPrice: 10
      },
      anoM: {
        rpAvailability: true,
        monthlyWeb: 2,
        monthlyStore: null,
        oneTimeWeb: 99,
        oneTimeStore: 119,
        eyecatcherURL: "/documents/10179/17001051/ec-webzlava.png",
        rplnPrice: 20
      },
      anoMdata: {
        rpAvailability: true,
        monthlyWeb: 2,
        monthlyStore: null,
        oneTimeWeb: 99,
        oneTimeStore: 119,
        eyecatcherURL: "/documents/10179/17001051/ec-webzlava.png",
        rplnPrice: 20
      },
      anoL: {
        rpAvailability: true,
        monthlyWeb: 1.91,
        monthlyStore: 2,
        oneTimeWeb: 1.16,
        oneTimeStore: null,
        eyecatcherURL: "/documents/10179/17001051/ec-webzlava.png",
        rplnPrice: 30
      },
      anoXL: {
        rpAvailability: true,
        monthlyWeb: 0,
        monthlyStore: 1,
        oneTimeWeb: 5,
        oneTimeStore: null,
        eyecatcherURL: "/documents/10179/17001051/ec-webzlava.png",
        rplnPrice: 40
      },
      anoXXL: {
        rpAvailability: true,
        monthlyWeb: 0,
        monthlyStore: 1.0,
        oneTimeWeb: 5,
        oneTimeStore: null,
        eyecatcherURL: "/documents/10179/17001051/ec-webzlava.png",
        rplnPrice: 70
      }
    }
  };

  $(".rpln").click(function() {
    var rateplan = $(this).prop("id"); // GET NEW VALUE FROM BUTTON
    $(".rpln").removeClass("active-plan");
    $(this).addClass("active-plan");
    setNewPrices(rateplan);
  });

  var selectedrp = "anoL"; // GET DATA FROM URL
  setPrice2(selectedrp);
});
