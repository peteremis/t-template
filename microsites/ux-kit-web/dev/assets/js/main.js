$(function () {

    hljs.initHighlightingOnLoad();

    $('.tooltip').qtip({
        content: {
            text: 'This is a tooltip.'
        },
        position: {
            my: 'bottom center',
            at: 'top center'
        }
    });

    /* TABS */

    $(".tabs-menu a").click(function (event) {
        event.preventDefault();
        if (!$(this).parent().hasClass("current") && !$(this).parent().hasClass("isVariant")) {
            toggleTabs();
        }

        $(this).parent().addClass("current");
        $(this).parent().siblings().removeClass("current");
        var tab = $(this).attr("href"),
        parent = $(this).closest('ul').parent().parent();
        parent.find('.tab-content').not(tab).css("display", "none");
        $(tab).fadeIn();

    });

    var toggleTabs = function () {
        $('.tab__variant').toggleClass('selected');
    };

    $('.btn, .cta, .link, .play').on('click', function(e){
        e.preventDefault();
    });

    var elem = document.querySelector('.js-switch');
    var init = new Switchery(elem, { color: '#6bb324', secondaryColor: '#ededed' });
    $('.switchery').parent().closest('span').addClass('hide-span');

    /* POPUP */

    $('.popup').fancybox({
        padding: 11,
        margin: 0,
        closeBtn: false,
        helpers : {
            overlay : {
                css : {
                    'background' : 'rgba(0, 0, 0, 0.7)'
                }
            }
        }
    });

    $('.p-close').on('click', function(e) {
        e.preventDefault();
        $.fancybox.close();
    });

    /* ::: Slider ::: */
    $("#program_slider").slick({
        slidesToShow: 2,
        slidesToScroll: 1,
        centerMode: false,
        infinite: false,
        swipeToSlide: true,
        swipe: true,
        arrows: true,
        dots: false,
        autoplay: false,
        initialSlide: 2,
        autoplaySpeed: 5000,
        touchMove: false,
        draggable: false,
        customPaging: function(slider, i) {
            return '<span class="slide-dot">&nbsp;</span>';
        },
        responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
                arrows: true,
                dots: true
            }
        }, {
            breakpoint: 768,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: true,
                infinite: false,
                dots: true
            }
        }]
    });
});
