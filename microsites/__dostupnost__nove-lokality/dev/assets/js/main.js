$(function() {

	var apiUrl = 'https://backvm.telekom.sk/shop/ins/dostupnost.php';

	if (window.location.hostname == 'localhost') {
		apiUrl = 'http://backvm.telekom.localhost/shop/ins/dostupnost.php';
	}

	var address = '',
		addressType = '',
		lat = '',
		long = '',
		internetSpeed = '',
		tvChannels = '',
		date = '',
		marker = '',
		availability = '',
		isAvailable = '',

		internetType = '',
		tvType = '',

		$cityForm = $('#city-form'),
		$cityInput = $('.city-input'),
		$cityResultList = $('.city-form__result_list'),
		$streetForm = $('#street-form'),
		$streetInput = $('.street-input'),
		$streetInputLabel = $('.street-input-label'),
		$streetResultList = $('.street-form__result_list'),
		$numberInput = $('.number-input'),
		$numberInputLabel = $('.number-input-label'),
		$btn = $('.sendReq'),
		$errorMsg = $('.error-msg'),
		$infoMsg = $('.info-msg'),
		$mapHolder = $('.map-holder'),
		$resetBtn = $('.reset'),
		isMapInitialised = false,
		hasStreet = true,
		cityId = '',
		streetId = '',
		isInfoVisible = false,
		isErrorVisible = false,
		insText = '',
		boxElem = '',
		availableServicesHolder = $('.av-services'),
		/*
			URL_CITY = 'https://backvm.telekom.sk/shop/ins/?searchTermKey=citySubstring',
			URL_STREET = 'https://backvm.telekom.sk/shop/ins/?searchTermKey=streetSubstring',
			URL_FINAL = 'https://backvm.telekom.sk/shop/ins/?searchTermKey=streetNoSubstring',
		*/
		URL_CITY = apiUrl + '?searchTermKey=citySubstring',
		URL_STREET = apiUrl + '?searchTermKey=streetSubstring',
		URL_FINAL = apiUrl + '?searchTermKey=streetNoSubstring',
		MIN_KEY_DOWN = 3,
		MSG_CANT_CONNECT = 'Nepodarilo sa načítať informácie, skúste to prosím ešte raz.',
		MSG_CANT_FIND_ADDRESS = '<b>Na zadanej adrese sme nenašli žiadne dostupné služby. Zadajte inú adresu alebo skontrolujte zadané údaje a skúste to znovu.</b>',
		API_KEY = 'AIzaSyAxxGXewHnIIPPZRLBhiUQqyxKXGyGeeus';

	function resultsList_Behavior(results, input) {
		$.each($(results).find('li'), function() {
			if ($(this).text().toLowerCase() === $cityInput.val().toLowerCase()) {
				$(this).click();
				if ($streetInput.is(':disabled')) {
					$numberInput.focus()
				} else {
					$streetInput.focus()
				}
			} else if ($(this).text().toLowerCase() === $streetInput.val().toLowerCase()) {
				$(this).click();
				$numberInput.focus()
			} else if ($(this).text() === $numberInput.val()) {
				$(this).click();
			}

			$(this).on('click', function() {
				if($(input).hasClass('city-input')) {
					switchInputState($streetInput, false);
					$streetInput.focus()
				} else if ($(input).hasClass('street-input')) {
					switchInputState($numberInput, false);
					$numberInput.focus()
				}
			})
		})
	}

	function hideList($input) {
		var $tempParent = $input.next('.results');
		$tempParent.hide();
	}

	function showList($input) {
		var $tempParent = $input.next('.results');
		resultsList_Behavior($tempParent, $input)
		$tempParent.show();
	}

	function emptyElem($elem) {
		$($elem).html('');
	}

	function addTextToInput($input, text) {
		$input.val(text);
	}

	function clearInput($input) {
		$input.val('');
	}

	function showErrorMsg(msg) {
		$errorMsg.show();
		$errorMsg.text(msg);
		isErrorVisible = true;
	}

	function hideErrorMsg() {
		$errorMsg.hide();
		$errorMsg.text('');
		isErrorVisible = false;
	}

	function showInfoMsg(msg) {
		$infoMsg.show();
		$infoMsg.html(msg);
		isInfoVisible = true;
	}

	function hideInfoMsg() {
		$infoMsg.hide();
		$infoMsg.html('');
		isInfoVisible = false;
	}

	function hideMap() {
		$mapHolder.hide();
	}

	function showMap() {
		$mapHolder.show();
	}

	function preventSubmitOnEnterPress($input) {
		$input.keypress(function(e) {
			var code = (e.keyCode ? e.keyCode : e.which);
			if (code == 13) {
				return false;
			}
		});
	}

	function clearResultOnArrowKey($input, $result) {
		$input.keydown(function(e) {
			var code = (e.keyCode ? e.keyCode : e.which);
			if (code == 38 || code == 40) {
				emptyElem($result);
			}
		});
	}

	function resetOnBackSpace($input, resetCityInput, resetStreetInput) {
		$input.on('keydown', function(e) {
			if (e.which == 8 || e.which == 46) {
				if (resetCityInput) {
					// resetFields();
				} else if (resetStreetInput) {
					clearInput($numberInput);

					emptyElem($streetResultList);

					switchInputState($numberInput, true);
					switchInputState($numberInputLabel, true);

					hideMap();
					emptyElem('.box');
				}
			}
		});
	}

	function resetFields() {
		insText = '';
		clearInput($cityInput);
		clearInput($streetInput);
		clearInput($numberInput);

		emptyElem($cityResultList);
		emptyElem($streetResultList);

		switchInputState($streetInput, true);
		switchInputState($streetInputLabel, true);

		switchInputState($numberInput, true);
		switchInputState($numberInputLabel, true);

		hideMap();
		emptyElem('.box');
	}

	function bindReset($elem) {
		$elem.on('click', function(e) {
			e.preventDefault();
			resetFields();
		});
	}

	var generateCityList = function(results) {
		var htmlToAppend = '';
		for (var i = 0; i < results.length; i++) {
			if (results[i].numOfStreets == 0) {
				hasStreet = false;
			} else {
				hasStreet = true;
			}
			htmlToAppend += '<li class="' + (hasStreet ? 'hasStreet' : '') + '" data-id="' + results[i].id + '">' + results[i].name + '</li>';
		}

		return htmlToAppend;
	}

	var generateStreetList = function(results) {
		var htmlToAppend = '';
		if (results !== null) {
			for (var i = 0; i < results.length; i++) {
				htmlToAppend += '<li data-id="' + results[i].id + '">' + results[i].street_name + '</li>';
			}
		}
		return htmlToAppend;
	}

	/**
	 * 2019 update: new behavior added
	**/
	function generateAvailableServices() {
		availableServicesHolder = $('.av-services');

		console.log(internetType);
		console.log(tvType);

		var tvDesc = '\'<p><b>Televízia v mobile, tablete aj počítači</b></p><p>K Magio TV získate zadarmo online televíziu Magio GO s archívom, vďaka ktorej môžete sledovať program aj na notebooku, tablete či smartfóne.</p><p><b>Magio archív</b></p><p>Už odvysielaný program môžete sledovať z archívu až 7 dní dozadu. Teraz prvé 3 mesiace zadarmo a potom už od 1,99 mesačne.</p><p><b>Praktické funkcie</b></p><p>Nahrajte si svoje obľúbené filmy alebo iné programy s Magio Boxom s funkciou nahrávania. Práve vysielaný program môžete tiež zastaviť alebo pretočiť dozadu a späť. Vo Videopožičovni si môžete kadykoľvek vybrať z ponuky až 700 rôznych titulov.</p>\'';

		if (tvType=='SAT') {
			tvDesc = '24 TV kanálov<br />7 dňový Magio archív<br />Zastav, pretoč, pusti znova';
		}

		function generateHeading(availabilityClass, text) {
			var heading =
			'<div class="av-services_heading '+availabilityClass+' text-center">'+
				'<p>Overenie prebehlo úspešne.<br><b>Na adrese <span class="av-services_address">'+address+'</span>'+text+'</b></p>'+
			'</div>';
			$(heading).prependTo(availableServicesHolder);
		}

		function unavailableService(UnavailableText) {
			var UnavailableService =
			'<div class="row av-services_content unavailable">'+
				'<div class="col-sm-12 col-md-1">'+
					'<img src="../assets/img/failure.png" alt="done">'+
				'</div>'+
				'<div class="col-sm-12 col-md-11">'+
					'<p class="service-name"><b>'+UnavailableText+'</b></p>'+
				'</div>'+
			'</div>';
			$(UnavailableService).appendTo(availableServicesHolder);
		}

		function serviceIsAvailable(service, serviceName, toggleElementID, blueToggleLink, price, infoText, insText) {

			var content =
			'<div class="row av-services_content">' +
			'	<div class="col-sm-12 col-md-1">' +
					'<img src="../assets/img/done.png" alt="done">' +
				'</div>' +
				'<div class="col-sm-12 col-md-9">' +
					'<a href="https://www.telekom.sk/fix/objednavka/-/scenario/e-shop/chytry-balik?invoiceNr=&city=' + $cityInput.val() + '&zipCode=&street=' + $streetInput.val() + '&streetNum=' + $numberInput.val() + '&usePlannedTechnologies=false" target="_blank"><p class="service-name" id="service-'+service+'"><b>'+serviceName+'</b></p></a>' +
				insText +
					'<div class="togleArrow">' +
						'<a onClick="$(\'#'+toggleElementID+'\').toggleClass(\'hidden\')" class="toggle-arrow" href="#">'+blueToggleLink+'<span class="arrow-right"></span></a>'+
					'</div>'+
				'</div>'+
				'<div class="col-sm-12 col-md-2 text-right">'+
					'<p><b>od '+price+' €</b></p>'+
					'<p class="monthly">'+
						'mesačne'+
					'</p>'+
				'</div>'+
				'<div id="'+toggleElementID+'" class="av-services_content__hidden hidden clearfix row no-padd">'+
					'<div class="col-sm-12 col-md-1">'+
						'&nbsp;'+
					'</div>'+
					'<div class="col-sm-12 col-md-9">'+
							infoText+
					'</div>'+
					'<div class="col-sm-12 col-md-2 text-right">'+
						'<a href="https://www.telekom.sk/fix/objednavka/-/scenario/e-shop/chytry-balik?invoiceNr=&city=' + $cityInput.val() + '&zipCode=&street=' + $streetInput.val() + '&streetNum=' + $numberInput.val() + '&usePlannedTechnologies=false" target="_blank" class="link">Vybrať variant</a>'+
					'</div>'+
				'</div>'+
			'</div>';
			$(content).appendTo(availableServicesHolder);
		}

		if (isAvailable.tv.M === 'yes' || isAvailable.voice.M === 'yes' || isAvailable.internet.M === 'yes' ) {
			generateHeading('available',' sú dostupné tieto služby:');
		} else {
			generateHeading('unavailable',' nie sú dostupné žiadne služby.');
		}

		function generateServiceContent(service, serviceName, toggleElementID, blueToggleLink, price, infoText, insText) {
			console.log(service);

			$.each(isAvailable[service], function(key, value) {
					// service is available in this location
				if (key === 'M' && value === 'yes') {
					$('.sec-'+service+'').show();
					serviceIsAvailable(
						service,
						serviceName,
						toggleElementID,
						blueToggleLink,
						price,
						infoText,
						insText
					);
				} else if (key === 'M' && value === 'no') {
					// service is not available in this location
					$('.sec-'+service+'').hide();
					var unavailableText = '';
					if (service === 'tv') {
						unavailableText = 'Magio televízia nie je dostupná na uvedenej adrese';
					} else if (service === 'internet') {
						unavailableText = 'Magio internet nie je dostupný na uvedenej adrese';
					} else {
						unavailableText = 'Pevná linka nie je dostupná na uvedenej adrese';
					}
					unavailableService(unavailableText);
				}
			});
		};

		generateServiceContent(
			'tv',
			'Magio televízia M, L alebo XL',
			'toggleContentTV',
			'Až '+$(tvChannels).text()+' aj v HD a internetová TV zadarmo',
			'10,90',
			tvDesc,
			''
		);
		generateServiceContent(
			'internet',
			'Magio internet M, L alebo XL',
			'toggleContentInternet',
			'Stabilný internet s rýchlosťou '+internetSpeed+'',
			'10,90',
			'<p><b>Spoľahlivosť a bezpečnosť</b></p><p>Surfujte doma bez výpadkov naraz na počítači, notebooku, telefóne alebo tablete cez bezpečnú bezdrôtovú Wi-Fi sieť alebo kábel.</p><p><b>Neobmedzený internet</b></p><p>Magio internet na doma nemá žiadne časové ani dátové obmedzenia a je ideálny pre časté a intenzívne využívanie.</p><p><b>Doplnková služba Giga</b></p><p>Získajte rýchlosť pripojenia až do 1 Gbit/s s Doplnkovou službou Giga. Len za 5 eur mesačne bez viazanosti k Magio internetu XL.</p><p><b>Magio internet security</b></p><p>Nedovoľte nikomu prekĺznuť do vášho počítača a ukradnúť vaše súkromné súbory, údaje, fotografie alebo heslá.</p>',
			insText
		);
		generateServiceContent(
			'voice',
			'Pevná linka M, L alebo XL',
			'toggleContentVoice',
			'Neobmedzené volania s vysoko kvalitným zvukom',
			'10,90',
			'<p><b>Neobmedzené volania</b></p><p>Užívajte si neobmedzené volania na mobily, do pevných sietí a Európskej únie z pohodlia svojho domova.</p><p><b>Spoľahlivosť a kvalita</b></p><p>S rokmi overenou Pevnou linkou od Telekomu sa môžete spoľahnúť na stabilné pripojenie s vysoko kvalitným zvukom.</p><p><b>CLIP a CLIR sú samozrejmosťou a zadarmo</b></p><p>Aj na Pevnej linke môžete bezplatne využívať službu CLIP, s ktorou vidíte, aké číslo vám volá alebo CLIR, ktorá zabráni zobrazovaniu vášho čísla ak si to neželáte.</p>',
			''
		);

		//handle submit btn behavior
		$btn.unbind('click');
		$btn.text('Overiť inú adresu');
		switchInputState($streetInput, true);
		switchInputState($numberInput, true);
		switchInputState($cityInput, true);
		$btn.on('click', function(e) {
			if ($btn.text() === 'Overiť inú adresu') {
				$('.sec-tv, .sec-internet, .sec-voice').show();
				$(availableServicesHolder).empty();
				$('.av-services-btns').empty();
				resetFields();
				switchInputState($cityInput, false);
				switchInputState($streetInput, true);
				switchInputState($numberInput, true);
			}

			var bindSubmit = function() {
				submitAddress();
			}
			$btn.text('Overiť');
			$btn.bind('click', bindSubmit);
		});

		var chytryBalikBtn = '<div class="av-eshopBtn text-center">'+
			'<p class="pricelegal text-right">Uvedené ceny sú s DPH.</p>'+
				'<a href="/fix/objednavka/-/scenario/e-shop/chytry-balik" class="btn cta_mag">PREJSŤ K NÁKUPU</a>'+
			'</div>';

		if ($('.av-services-btns').children().length === 0) {
			// $(chytryBalikBtn).appendTo('.av-services-btns'); //DISABLED
		} else {
			return;
		}
	}

	function generateBox() {
		var url = '<a id="zaujem" class="button scroll-to" href="#sec-2" style="display: inline-block;">Mám záujem</a>';
		if (addressType == 'real') {
			url = '<a class="button" href="https://www.telekom.sk/fix/objednavka/-/scenario/e-shop/chytry-balik?invoiceNr=&city=' + $cityInput.val() + '&zipCode=&street=' + $streetInput.val() + '&streetNum=' + $numberInput.val() + '&usePlannedTechnologies=false" style="display: inline-block;">Mám záujem</a>';
		}

		var  availabilityTag = '';
		if (availability!=='') {
			availabilityTag = '<span class="ins_date">'+ availability +'</span>';
		}

		boxElem = '<div id="ulicebox" class="ulicebox"><div class="row">' + address + '</div></div>' +
			'<div class="sluzby" style="display: block;"><div id="sluzby-header">DOSTUPNÉ SLUŽBY</div>' +
			'<div id="internet" class="internet"><img src="https://m.telekom.sk/wb/novepokrytie/img/icon_internet.png">' +
			'<span class="black">Magio Internet</span> ' + internetSpeed + '</div>' + availabilityTag +
			'<div id="tv" class="tv"><img src="https://m.telekom.sk/wb/novepokrytie/img/icon_tv.png"><span class="black">Magio televízia</span> ' + tvChannels + '</div><p></p></div>' +
			'<div class="zaujem" style="display: block;">' +
			url +
			'</div>';
	}

	function bindClickEvent($result, $input) {
		$result.on('click', function(e) {
			e.preventDefault();

			var choosenElem = $(e.target).text();
			$input.val(choosenElem);

			emptyElem($result);
			hideList($input);

			if ($input.hasClass('city-input')) {
				// console.log($result);
				// console.log($input);

				if ($(e.target).hasClass('hasStreet')) {
					switchInputState($streetInput);
					switchInputState($streetInputLabel);
					$streetInput.val('')
					cityId = $(e.target).data('id');
				} else {
					addTextToInput($streetInput, 'Obec nemá ulice');
					switchInputState($streetInput, true);
					switchInputState($numberInput);
					$numberInput.focus()
					switchInputState($numberInputLabel);
				}
			} else if ($input.hasClass('street-input')) {
				streetId = $(e.target).data('id');
				switchInputState($numberInput);
				switchInputState($numberInputLabel);

			}
		});
	}

	function bindKeyup($input, $result) {
		$input.on('keyup', function() {
			if (isErrorVisible) {
				hideErrorMsg();
			}
			if (isInfoVisible) {
				hideInfoMsg();
			}

			if ($(this).val() == '') {
				emptyElem($result);

				if ($input.hasClass('city-input')) {
					clearInput($streetInput);
					clearInput($numberInput);

					switchInputState($streetInput, true);
					switchInputState($streetInputLabel, true);

					switchInputState($numberInput, true);
					switchInputState($numberInputLabel, true);

					switchBtnState($btn, true);
				} else if ($input.hasClass('street-input')) {
					clearInput($numberInput);

					switchInputState($numberInput, true);
					switchInputState($numberInputLabel, true);

					switchBtnState($btn, true);
				}
			} else {
			}
		});
	}

	function bindNumKeyUp() {
		$numberInput.on('keyup', function() {
			if ($(this).val() != '') {
				switchBtnState($btn, false);
				// 	// Filter non-digits from input value.
				// if (/\D/g.test(this.value)){
				// 	this.value = this.value.replace(/\D/g, '');
				// }
			} else {
				switchBtnState($btn, true);
			}
		});
	}

	function switchInputState($input, disable) {
		if (disable) {

			if (!$input.attr('disabled')) {
				$input.attr('disabled', true);
			}
			if (!$input.hasClass('disabled')) {
				$input.addClass('disabled');
			}

		} else {

			if ($input.attr('disabled')) {
				$input.attr('disabled', false);
			}
			if ($input.hasClass('disabled')) {
				$input.removeClass('disabled');
			}

		}
	}

	function switchBtnState($btn, disable) {
		if (disable) {
			if (!$btn.attr('disabled')) {
				$btn.attr('disabled', true);
			}
			if (!$btn.hasClass('disabled')) {
				$btn.addClass('disabled');
			}
		} else {
			if ($btn.attr('disabled')) {
				$btn.attr('disabled', false);
			}

			if ($btn.hasClass('disabled')) {
				$btn.removeClass('disabled');
			}
		}
	}

	function hideDropdownIfClickedElsewhere() {
		$(document).mouseup(function(e) {
			var container = $('.results'),
				elemToHide = $('.results ul');

			if (!container.is(e.target) && container.has(e.target).length === 0) {
				elemToHide.html('');
			}
		});
	}

	function bindAutocomplete($input, $result, addCityId, generateFunction, url, minLength) {
		var $tempParent = $input.next('.results');
			$input.autocomplete({
				source: function(request, response) {
					$input.addClass('loading');
					emptyElem($result);

					var cityData = {
							searchTermValue: request.term
						},
						streetData = {
							searchTermValue: request.term,
							cityId: cityId
						},
						tempData = {};

					addCityId ? $.extend(true, tempData, streetData) : $.extend(true, tempData, cityData)

					$.ajax({
						url: url,
						data: tempData,
						dataType: "json",
						success: function(data) {
							if (data !== null && jQuery.isEmptyObject(data) === false) {
								response($.map(data, function(item) {
									if (item.name) {
										return {
											label: item['name'],
											value: item['name'],
										  }
									} else if (item.streetNoCount) {
										return {
											label: item['street_name'],
											value: item['street_name']
										  }
									}
								}));
							} else {
								showInfoMsg(MSG_CANT_FIND_ADDRESS);
							}

							var temphtmlToAppend = '';
								temphtmlToAppend = generateFunction(data);
							$(temphtmlToAppend).appendTo($result);

							showList($input);

							$input.removeClass('loading');
						},
						error: function() {
							console.log("error")
							showErrorMsg(MSG_CANT_CONNECT);
							$input.removeClass('loading');
						}
					});

				},
				select: function(event , ui) {
					$input.val(ui.item.value);
					resultsList_Behavior($tempParent, $input)
				},
				focus: function(event, ui) {
					$.each($tempParent.find('li'), function() {
						if ($(this).text() === ui.item.value) {
							$(this).css({'background-color':'#f5f5f5'})
						} else {
							$(this).css({'background-color':'inherit'})
						}
					})
				},

				minLength: minLength
			});
	}

	function submitAddress() {
		$('#ajaxLoadingSpinnerOverlay').show();
		var tempVal = $numberInput.val();

		$.ajax({
			type: "GET",
			async: true,
			crossDomain: true,
			dataType: "json",
			data: {
				searchTermValue: tempVal,
				cityName: $cityInput.val(),
				streetName: $streetInput.val(),
				cityId: cityId,
				streetId: streetId
			},
			url: URL_FINAL,
			success: function (response) {
				if (typeof(response.address) === "undefined" || response.address == null) {
					// if (response.length == 0) {
					$('#ajaxLoadingSpinnerOverlay').hide();
					showInfoMsg(MSG_CANT_FIND_ADDRESS);
					$.fancybox({
						href: '#not-found',
						modal: true
					});
					return false;
				} else {

					console.log(cityId);
					console.log($streetInput.val());
					console.log(tempVal);

					if (response.planned['internet']['date'] != '') {
						if (response.planned['internet']['type'] == 'FIBRE') {
							insText = '<div class="ins">Od <span class="bold">' + response.planned['internet']['date'] + '</span> dostupné optické pripojenie s rýchlosťou až do <span class="bold">300 Mb/s</span></div>';
						} else {
							insText = '<div class="ins">Od <span class="bold">' + response.planned['internet']['date'] + '</span> dostupnéVDSL  pripojenie s rýchlosťou až do <span class="bold">80 Mb/s</span></div>';
						}
					}

					console.log(response);

					$('#ajaxLoadingSpinnerOverlay').hide();
					address = response.address;
					addressType = response.addressType;
					internetSpeed = response.internet;
					tvChannels = response.tv;
					isAvailable = response.availability;

					internetType = response.internetType;
					tvType = response.tvType;
					//availability = response.date;

					$(availableServicesHolder).empty();
					generateAvailableServices();

					$('.sec-internet-insert').text(internetSpeed);
					$('.sec-tv-insert').text($(tvChannels).text());

					// generateBox();
					// $('.box').empty();
					// $(boxElem).appendTo('.box');
					// showMap();
					// getLongLat(address);
					// initMap();
				}
			},
			error: function() {
				showErrorMsg(MSG_CANT_CONNECT);
			}
		});
	}

	$btn.on('click', function() {
		submitAddress();
	});

	function initMap() {
		var map = new google.maps.Map(document.getElementById('map'), {
			zoom: 17,
			center: {
				lat: 48.891132,
				lng: 18.042297
			},
			disableDefaultUI: true
		});

		var geocoder = new google.maps.Geocoder();

		geocodeAddress(geocoder, map);

	}

	function geocodeAddress(geocoder, resultsMap) {
		geocoder.geocode({
			'address': address
		}, function(results, status) {
			if (status === 'OK') {
				resultsMap.setCenter(results[0].geometry.location);
				marker = new google.maps.Marker({
					map: resultsMap,
					position: results[0].geometry.location
				});
			} else {
				alert('Geocode was not successful for the following reason: ' + status);
			}
		})

	};

	function getLongLat(address) {
		$.ajax({
			type: "GET",
			async: true,
			crossDomain: true,
			dataType: "json",
			data: {
				address: address,
				key: API_KEY
			},
			url: 'https://maps.googleapis.com/maps/api/geocode/json',

			success: function(response) {
				lat = response.results[0].geometry.location.lat;
				long = response.results[0].geometry.location.lng;
			},
			error: function() {
			}
		});
	}

	$(".scroll-to").click(function () {
		if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
			if (target.length) {
				$('html,body').animate({
					scrollTop: target.offset().top - ($("#nav-sticky").outerHeight())
				}, 1000);
				return false;
			}
		}
	});

	switchInputState($streetInput, true);
	switchInputState($numberInput, true);
	switchBtnState($btn, true);

	bindAutocomplete($cityInput, $cityResultList, false, generateCityList, URL_CITY, MIN_KEY_DOWN);
	bindAutocomplete($streetInput, $streetResultList, true, generateStreetList, URL_STREET, MIN_KEY_DOWN);

	preventSubmitOnEnterPress($cityInput);
	preventSubmitOnEnterPress($streetInput);
	preventSubmitOnEnterPress($numberInput);

	resetOnBackSpace($cityInput, true, false);
	resetOnBackSpace($streetInput, false, true);

	bindClickEvent($cityResultList, $cityInput);
	bindKeyup($cityInput, $cityResultList);

	bindClickEvent($streetResultList, $streetInput);
	bindKeyup($streetInput, $streetResultList);


	bindReset($resetBtn);

	bindNumKeyUp();

	hideDropdownIfClickedElsewhere();

	$('.not-found-form').show();
	$('.not-found-success').hide();

	$('.popup').fancybox({
		padding: 11,
		margin: 0,
		closeBtn: true,
		hideOnOverlayClick: true,
		helpers: {
			overlay: {
				css: {
					'background': 'rgba(0, 0, 0, 0.7)'
				}
			}
		}
	});

	$('.p-close').on('click', function (e) {
		e.preventDefault();
		$.fancybox.close();
	});

	$('#not-found-send').on('click', function (e) {

		var citiVal = $cityInput.val();
		var streetVal = $streetInput.val();
		var numberVal = $numberInput.val();

		var internetVal = $('#nf-internet').attr('checked');
		var tvVal = $('#nf-tv').attr('checked');
		var pevnaVal = $('#nf-pevna').attr('checked');
		var phoneVal = $('#nf-phone').val();

		$.ajax({
			type: "GET",
			async: true,
			crossDomain: true,
			dataType: "json",
			data: {
				phone: phoneVal,
				mesto: citiVal,
				ulica: streetVal,
				cislo: numberVal,
				tv: tvVal,
				internet: internetVal,
				pevna: pevnaVal
			},
			url: "https://backvm.telekom.sk/www/checker/email.php",

			success: function (response) {
				$('.not-found-form').hide();
				$('.not-found-success').show();
			},
			error: function () {
				showErrorMsg(MSG_CANT_CONNECT);
			}
		});

		e.preventDefault();
		// $.fancybox.close();
	});

	/* TOGGLE ARROW */
	$(document).on('click', '.toggle-arrow, .arrow-right', function(event) {
		event.preventDefault();
		if ($(this).hasClass('arrow-right')) {
		return;
		}
		$(event.target).closest('.togleArrow').find('.arrow-content').toggle(200);
		$(this).find('.arrow-right').toggleClass('arrow-rotate');
	});
	/* TOGGLE ARROW END */

});
