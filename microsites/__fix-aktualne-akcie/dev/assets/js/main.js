$(function() {
  /* TABS */
  $(".tabs-menu a").click(function(event) {
    event.preventDefault();
    $(this)
      .parent()
      .addClass("current");
    $(this)
      .parent()
      .siblings()
      .removeClass("current");
    var tab = $(this).attr("href"), // select clicked tab. In example "#tab-1" ..
      parent = $(this)
        .closest("ul")
        .parent()
        .parent(); // <tabs-container>
    parent
      .find(".tab-content")
      .not(tab)
      .css("display", "none"); //hide all not clicked tabs
    $(tab).fadeIn(); //show clicked tab
  });
  /* TABS END */
});
