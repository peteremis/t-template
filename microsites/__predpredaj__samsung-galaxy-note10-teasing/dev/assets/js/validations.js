$(function() {
  var $form = $("#contact-form");
  var termsCheckBox = $("#termscheck");
  var sendFormBtn = $(".sendfrm");

  // initialize the validation plugin
  var formValidator = $form.validate({
    rules: {
      phoneNum: {
        required: true,
        customNumberValidation: true,
        maxlength: 13
      },
      termscheck: {
        required: true
      }
    },
    messages: {
      phoneNum: {
        required: "Napíšte svoje mobilné telefónne číslo.",
        customNumberValidation:
          "Napíšte tel. číslo v tvare 0911222333 alebo +421911222333.",
        maxlength: "Telefónne číslo môže obsahovať maximálne 10 číslic."
      },
      termscheck: {
        required: "Zaškrtnite prosím súhlas s podmienkami."
      }
    },
    errorClass: "error",
    validClass: "valid",
    onclick: function(element) {
      //for checkboxes only
      $form.valid();
      if (
        formValidator.numberOfInvalids() === 0 &&
        $("#termscheck").is(":checked")
      ) {
        //all fields valid
      } else {
        //invalid field found
      }
    },
    onfocusout: function(element) {
      this.element(element);
      if (
        formValidator.numberOfInvalids() === 0 &&
        $("#termscheck").is(":checked")
      ) {
        //all fields valid
      } else {
        //invalid field found
      }
    },
    onkeyup: function(element) {
      this.element(element);
      if (
        formValidator.numberOfInvalids() === 0 &&
        $("#termscheck").is(":checked")
      ) {
        //all fields valid
      } else {
        //invalid field found
      }
    },
    invalidHandler: function(errors) {
      // console.log(formValidator.numberOfInvalids() + " field(s) are invalid")
    },
    highlight: function(element, errorClass, validClass) {
      $(element)
        .parents(".form-group")
        .addClass(errorClass)
        .removeClass(validClass);
    },
    unhighlight: function(element, errorClass, validClass) {
      $(element)
        .parents(".form-group")
        .removeClass(errorClass)
        .addClass(validClass);
    },
    // error form field
    errorPlacement: function(error, element) {
      var placement = $(element).data("error");
      if (placement) {
        //$(placement).append(error)
      } else {
        //error.insertAfter(element);
        for (let index = 0; index < element.length; index++) {
          const el = element[index];
          if ($(el).attr("id") === termsCheckBox.attr("id")) {
            error.insertAfter(
              $(el)
                .closest(".form-group")
                .find("label")
            );
          } else {
            error.insertAfter(
              $(el)
                .closest(".form-group")
                .find("input")
            );
          }
        }
      }
    }
  });

  // custom phone number validation rule
  $.validator.addMethod("customNumberValidation", function(value, element) {
    if ($(element).val() === "") {
      return true;
    } else {
      return /^(((\+4219)|(09))[0-9]{2}\s*[0-9]{3}\s*[0-9]{3})$/.test(value);
    }
  });

  // custom email validation rule
  $.validator.addMethod("customEmailValidation", function(value, element) {
    return (
      /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
        value
      ) && /^(?=.{1,64}@.{4,64}$)(?=.{6,100}$).*/.test(value)
    );
  });

  //after formular sent, redirect user to thank you page
  // if(window.location.search.indexOf('dynamicFormFinish') !== -1) {
  //     window.location.href = '/test_miso/magio-tv-dirctomlp-thankyoupage';
  // }

  // SUBMIT FORM
  sendFormBtn.on("click", function(e) {
    e.preventDefault();
    //check if form is valid
    if ($form.valid()) {
      // SEND FORM ...
      var phoneNum = $form.find("#phoneNum").val();
      $(".lrformPhoneNum")
        .find("input")
        .val(phoneNum);

      setTimeout(function() {
        $(".tlacidlo_odoslat").click();
      }, 500);
    } else {
      e.preventDefault();
    }
  });

  $("a").click(function(e) {
    e.preventDefault();
  });
});
