var nbaVars = {
	config: null,
	heroPausaly: '',
	utm_source: 'web',
	prices: null,
	gaRateplanes: [],
	gaMagio: [],
	nbaOffers: {
		tariffChange: [],
		nonHw: [],
	},
};
var typeSegment = 'b2c';
var dataLayer = [];

var $grid = $('.grid');

// var christmasItem = $('.element-item');
// var sortingsWrapper = $('.sortingWrapper');
// var heapBoxElement = $('#select_program'); // heapbox div id

var initRatePlaneId = 12;

$(document).ready(function () {
	dataRatePlanes.sort(function (a, b) {
		if (a.id < b.id) {
			return 1;
		} else {
			return -1;
		}
	});

	for (var i = 0; i < dataRatePlanes.length; i++) {
		if (dataRatePlanes[i].segment === typeSegment) {
			if (dataRatePlanes[i].hero) {
				nbaVars.heroPausaly += getPausalInfo(dataRatePlanes[i], [], null);
			}
		}
	}

	$.when(nba(null, null, nbaVars.utm_source))
		.done(function (data, textStatus, jqXHR) {
			if (data.configuration) {
				config = data.configuration;
				//setScenarioPath();
			}

			$('#phoneNumber').val(data.offer.msisdn);
			nba_action(data.offer, true);
		})
		.fail(function (error, textStatus, errorThrown) {
			if (error.responseJSON.configuration) {
				config = error.responseJSON.configuration;
				//setScenarioPath();
			}
		});

	$('#offers').html(nbaVars.heroPausaly);

	$('#productsHeader .product-header.active').show();

	/* ::: Sticky navigations filters ::: */
	$('#sortingOptionsSection').sticky({
		topSpacing: 0,
		widthFromWrapper: true,
	});

	initIsotope();

	new Switchery(document.querySelector('.js-switch'), {
		color: '#e20074',
		secondaryColor: '#ededed',
	});
	$('.switchery').parent().closest('span').addClass('hide-span');

	//init function
	init();
});

// filter button click
$('#filters a').click(function () {
	var clickedBtn = $(this);

	if (!clickedBtn.hasClass('is-checked')) {
		var filterValue = $(this).attr('data-filter');

		$('#filters a.is-checked').removeClass('is-checked');
		$(this).addClass('is-checked');

		if (filterValue === '.predplatenka') {
			$('#wrapper .predplatenka-statut').show();
		} else {
			$('#wrapper .predplatenka-statut').hide();
		}

		$('#productsHeader .product-header.active').removeClass('active');
		$('#productsHeader .product-header' + filterValue + '-cont').addClass('active');

		dataLayer.push({
			event: 'VirtualPageview',
			serviceType: filterValue !== '.rateplan' ? filterValue.substr(1) : 'pausal',
		});

		if (filterValue.substr(1) !== 'predplatenka') {
			dataLayer.push({
				event: 'servicesShow',
				services: filterValue.substr(1) === 'rateplan' ? nbaVars.gaRateplanes : nbaVars.gaMagio,
			});

			var defaultGa = filterValue.substr(1) === 'rateplan' ? nbaVars.gaRateplanes : nbaVars.gaMagio;

			for (var i = 0; i < defaultGa.length; i++) {
				if (defaultGa[i].preselected) {
					dataLayer.push({
						event: 'serviceChoose',
						service: defaultGa[i],
					});
				}
			}

			gaOffersShow();
		}

		$grid.isotope({
			filter: filterValue + '.visible',
		});

		// scroll back to top
		$('html,body').animate(
			{
				scrollTop: $('#categoryTitle').offset().top + $('#categoryTitle').height() + 75,
			},
			'slow'
		);
	}
});

$(document).on('click', '.reset-form', function () {
	$.when(nba(null, null, null, true)).then(function (data) {
		localStorage.removeItem('nbaOfferNonHwType');
		setGaHeroPausal(false);
		dataLayer.push({
			event: 'servicesShow',
			services: nbaVars.gaRateplanes,
		});
		nbaVars.nbaOffers.tariffChange = [];
		nbaVars.nbaOffers.nonHw = [];
		$('#phoneChecker .results .active').removeClass('active');
		$('#phoneChecker .checker').show();
		$('#phoneChecker #phoneNumber').val('');
		$('#productsSection .rateplan-items').removeClass('zlava-pre-vernych');
		$('#offers-cont h3').html('Zvoľte si paušál');
		$('#offers').slick('removeSlide', null, null, true).slick('slickAdd', nbaVars.heroPausaly);
		$('#offers .cart:first').removeClass('active').click();
		$('.nba-hide').show(200);
	});
});

function init() {
	//changeItemWidth(false); //change item width on init
	setGaHeroPausal(true);
	setGaMagio();
	initSlider('offers', 4);
	initSlider('magioProducts', 3);
	fetchData();
}

function initScrollTo() {
	var result = null;

	if (window.location.hash) {
		var element = window.location.hash.substr(1);

		if ($('.products-section #' + element.toUpperCase()).length) {
			element = element.toUpperCase();
			var product = $('.products-section #' + element);
			var parent = product.closest('.products-section').attr('data-type');

			if (parent === 'magio' || parent === 'predplatenka') {
				setCategory(parent);
			}

			result = $('#' + element);
		} else {
			if (element === 'predplatenka' || element === 'magio') {
				setCategory(element);
				result = $('.products-header');
				element = 'products-header';
			} else {
				result = $('.' + element);
			}
		}
	}
	return result;
}

function setCategory(element) {
	$('#filters a.is-checked').removeClass('is-checked');
	$('#filters a[data-filter=".' + element + '"]').addClass('is-checked');
	$('#productsHeader .product-header.active').removeClass('active');
	$('#productsHeader .product-header.' + element + '-cont').addClass('active');
}

function setGaHeroPausal(initPage) {
	nbaVars.gaRateplanes = [];
	var indexHero = 0;
	for (var i = 0; i < dataRatePlanes.length; i++) {
		if (dataRatePlanes[i].segment === typeSegment) {
			if (dataRatePlanes[i].hero) {
				if (initPage) nbaVars.heroPausaly += getPausalInfo(dataRatePlanes[i], [], null);

				indexHero++;

				var serviceFamily = 'T Základ';
				if (dataRatePlanes[i].category === 2) serviceFamily = 'T Dáta';
				else if (dataRatePlanes[i].category === 3) serviceFamily = 'T Ideál';
				else if (dataRatePlanes[i].category === 4) serviceFamily = 'T Nekonečno';

				nbaVars.gaRateplanes.push({
					serviceFamily: serviceFamily,
					service: dataRatePlanes[i].name,
					id: dataRatePlanes[i].rpCode,
					price: parseFloat(dataRatePlanes[i].cena.replace(',', '.')),
					position: indexHero,
					preselected: dataRatePlanes[i].id === initRatePlaneId,
				});
			}
		}
	}
}

function setGaMagio() {
	nbaVars.gaMagio.push({
		serviceFamily: 'Magio',
		service: 'Magio M',
		id: '',
		price: 10.9,
		position: 1,
		preselected: true,
	});
	nbaVars.gaMagio.push({
		serviceFamily: 'Magio',
		service: 'Magio L',
		id: '',
		price: 15.9,
		position: 2,
		preselected: false,
	});
	nbaVars.gaMagio.push({
		serviceFamily: 'Magio',
		service: 'Magio XL',
		id: '',
		price: 20.9,
		position: 3,
		preselected: false,
	});
}

function fetchData() {
	$.ajax({
		url: 'https://backvm.telekom.sk/www/phones/export_vianoce.php',
		type: 'GET',
		timeout: 2000,
		dataType: 'json',
		success: function (data) {
			nbaVars.prices = data;
			setInitValues();
		},
		error: function (error) {
			$.getScript('../assets/js/prices_backup.js', function () {
				nbaVars.prices = christPricesBackup;
				setInitValues();
			});
			console.log(error);
		},
	});
}

function setInitValues() {
	var elScroll = initScrollTo();
	var selectedProgram = $('#filters .button.is-checked').attr('data-filter').substr(1);

	dataLayer.push({
		event: 'pageData',
		serviceType: selectedProgram === 'rateplan' ? 'pausal' : selectedProgram,
	});

	if (selectedProgram !== 'predplatenka') {
		dataLayer.push({
			event: 'servicesShow',
			services: selectedProgram === 'rateplan' ? nbaVars.gaRateplanes : nbaVars.gaMagio,
		});

		var defaultGa = selectedProgram === 'rateplan' ? nbaVars.gaRateplanes : nbaVars.gaMagio;

		for (var i = 0; i < defaultGa.length; i++) {
			if (defaultGa[i].preselected) {
				dataLayer.push({
					event: 'serviceChoose',
					service: defaultGa[i],
				});
			}
		}
	}

	var selectedData = {
		parent: 'rateplan',
		uacId: null,
		dataProgram: getDataRateplane(initRatePlaneId),
	};

	changeItemsValues(selectedData);

	selectedData = {
		parent: 'magio',
		uacId: null,
		dataProgram: { rpCode: 'M', name: 'M' },
	};
	changeItemsValues(selectedData);

	if (elScroll) {
		setTimeout(() => {
			scrollTo(elScroll);
		}, 500);
	}

	$('#ajaxLoadingSpinnerOverlay').hide();
}

function initIsotope() {
	// init Isotope
	$grid.isotope({
		filter: $('#filters .button.is-checked').attr('data-filter'),
		layoutMode: 'masonry',
		masonry: {
			columnWidth: '.grid-sizer',
			gutter: '.gutter-sizer',
		},
		itemSelector: '.element-item',
		getSortData: {
			soldout: '[data-soldout]',
		},
		sortBy: 'soldout',
	});
}

$(document).on('click', '#productsHeader .carts .cart', function () {
	if (!$(this).hasClass('active')) {
		var parent = $(this).closest('.product-header').attr('data-type');

		$('#productsHeader .' + parent + '-cont .cart.active').removeClass('active');
		$(this).addClass('active');

		var id = parseInt($(this).attr('data-id'));

		var selectedData = { parent };

		if (parent === 'rateplan') {
			var id = parseInt($(this).attr('data-id'));
			var uacId = $(this).attr('data-uac');
			selectedData.uacId = uacId ? uacId : null;
			selectedData.dataProgram = getDataRateplane(id);

			for (var i = 0; i < nbaVars.gaRateplanes.length; i++) {
				if (nbaVars.gaRateplanes[i].service === $(this).find('.title .name').text().trim()) {
					dataLayer.push({
						event: 'serviceChoose',
						service: nbaVars.gaRateplanes[i],
					});
				}
			}
		} else {
			var rpCode = $(this).find('.title .name').text().trim();
			selectedData.dataProgram = {
				rpCode: rpCode,
				name: rpCode,
			};
			if (parent === 'magio') {
				for (var i = 0; i < nbaVars.gaMagio.length; i++) {
					if (nbaVars.gaMagio[i].service === 'Magio ' + rpCode) {
						dataLayer.push({
							event: 'serviceChoose',
							service: nbaVars.gaMagio[i],
						});
					}
				}
			}
		}

		changeItemsValues(selectedData);
	}
});

/* TOGGLE ARROW */
$('.toggle-arrow, .arrow-right').click(function (event) {
	event.preventDefault();
	if ($(this).hasClass('arrow-right')) {
		return;
	}

	if ($(event.target).hasClass('toggle-arrow main') || $(event.target).hasClass('arrow-right main')) {
		$(this).closest('.toggleArrow.main').find('.arrow-content.main').toggle(200);
		$(this).find('.arrow-right.main').toggleClass('arrow-rotate');
	} else {
		$(this).closest('.toggleArrow').find('.arrow-content').toggle(200);
		$(this).find('.arrow-right').toggleClass('arrow-rotate');
	}
});
/* TOGGLE ARROW END */

function getDataRateplane(id) {
	var result = $.grep(dataRatePlanes, function (rp) {
		return rp.id === id;
	})[0];

	return result ? result : null;
}

function changeItemsValues(data) {
	var selectedRpCode = data.dataProgram.rpCode.toUpperCase();

	var webZlava = 20;
	if (data.parent === 'rateplan') {
		var webZlavy = {
			30: ['RP1140', 'RP1141', 'RP1142', 'RP1143'],
			50: ['RP1144', 'RP1145'],
		};

		Object.keys(webZlavy).forEach(function (key) {
			var index = webZlavy[key].indexOf(data.dataProgram.rpCode);
			if (webZlava === 20 && index > -1) webZlava = key;
		});
	}

	$('#productsSection .' + data.parent + '-items .element-item.ch-p').each(function () {
		var element = $(this);

		var urlDetail = '';
		var urlShop = '';

		if (data.parent === 'rateplan') {
			urlDetail = '/mob/objednavka/-/scenario/e-shop/pre-vas-pausaly/zariadenia?hw=' + element.attr('id');

			if (data.uacId) {
				urlDetail += '&uac=' + data.uacId;
			} else {
				urlDetail += '&tariff=' + data.dataProgram.rpCode;
			}
			urlShop = urlDetail + '&vyberkrok=PLAN_BD_ACTIVATION';
			element.find('.ec.online-zlava .sum span').text(webZlava).end();
		} else {
			urlShop = '/fix/objednavka/-/scenario/e-shop/chytry-balik';
		}

		element.removeClass('visible').find('.prices-rtpl').html(data.dataProgram.name);

		var idProduct = $(this).attr('id');
		var productPrices = data.parent === 'rateplan' ? nbaVars.prices.mobile : nbaVars.prices.fix;
		var product = $.grep(productPrices, function (p) {
			return p.product_code === idProduct;
		});

		if (product.length) {
			product = product[0];

			if (data.parent === 'magio') {
				urlDetail = product.link + '?selectedrp=' + data.dataProgram.rpCode;
			}

			element
				.find('.btn-action .link')
				.attr('href', urlDetail)
				.end()
				.find('.btn-action .btn')
				.attr('href', product.is_visible && product.is_orderable ? urlShop : '#');

			$.map(product.prices, function (prices, index) {
				if (index === selectedRpCode) {
					var price_oc = prices.oc;
					var price_rc = prices.rc;
					var price_oc_web = prices.oc_sale_web;
					var price_rc_web = prices.rc_sale_web;
					if (data.parent === 'magio') {
						price_oc_web = price_oc;
						price_rc_web = price_rc;
					}
					if ($('#productsSection .' + data.parent + '-items').hasClass('zlava-pre-vernych')) {
						price_oc_web = prices.oc_sale_total;
						price_rc_web = prices.rc_sale_total;
					}

					element
						.addClass('visible')
						.find('.monthlyWeb .sum')
						.html(getPriceFormat(price_rc_web))
						.end()
						.find('.monthlyStore .sum')
						.html(price_rc > price_rc_web ? getPriceFormat(price_rc) : '')
						.end()
						.find('.oneTimeWeb .sum')
						.html(getPriceFormat(price_oc_web))
						.end()
						.find('.oneTimeStore .sum')
						.html(price_oc > price_oc_web ? getPriceFormat(price_oc) : '')
						.end();
					if (price_rc_web >= price_rc) element.find('.monthly').addClass('no-discont');
					else element.find('.monthly').removeClass('no-discont');
					if (price_oc_web >= price_oc) element.find('.one-time').addClass('no-discont');
					else element.find('.one-time').removeClass('no-discont');
				}
			});

			if (!product.is_orderable) {
				element.find('.btn-action .btn').addClass('btn_disabled').text('DOSTUPNÉ ČOSKORO');
			}

			if (!product.is_visible) {
				element.addClass('soldOut').attr('data-soldout', 1);
			} else {
				element.attr('data-soldout', 0);
			}
		}
	});

	$grid.isotope('updateSortData').isotope({
		filter: $('#filters a.is-checked').attr('data-filter') + '.visible',
	});

	if ($('#filters a.is-checked').attr('data-filter').substr(1) === data.parent) {
		gaOffersShow();
	}
}

function gaOffersShow() {
	var parent = $('#filters a.is-checked').attr('data-filter').substr(1);

	var gaData = [];
	var gaPosition = 0;
	var onlineZlava = 0;
	$('#productsSection .' + parent + '-items .element-item.ch-p').each(function () {
		var el = $(this);
		onlineZlava = el.find('.ec-circle.online-zlava .sum span').text().trim() + ' €';
		gaPosition++;
		gaData.push({
			offer: el.find('p.name b').text().trim(),
			id: el.attr('id'),
			price: el.find('.oneTimeWeb .sum').text().trim(),
			stored: !el.hasClass('soldOut'),
			position: gaPosition,
		});
	});

	if (gaData.length) {
		dataLayer.push({
			event: 'offersShow',
			discountByTariff: parent === 'rateplan' ? onlineZlava : '30 €',
			discountByCustomer: $('#productsSection .' + parent + '-items').hasClass('zlava-pre-vernych')
				? '30 €'
				: '0 €',
			offers: gaData,
		});
	}
}

function getPriceFormat(val) {
	return val.toFixed(2).replace('.', ',') + ' €';
}

//assign rateplan values to heapbox
function setRPvalues(filterValue) {
	if (filterValue === '.rateplan') {
		return rateplanValues;
	} else if (filterValue === '.magio') {
		return magioValues;
	} else if (filterValue === '.biznis') {
		return biznisRateplanValues;
	} else if (filterValue === '.easy') {
		return true;
	} else {
		return true;
	}
}

// get url param by param name
function getUrlParameter(sParam) {
	var sPageURL = window.location.search.substring(1),
		sURLVariables = sPageURL.split('&'),
		sParameterName,
		i;
	for (i = 0; i < sURLVariables.length; i++) {
		sParameterName = sURLVariables[i].split('=');
		if (sParameterName[0] === sParam) {
			return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
		}
	}
}

// handle .is-checked class on buttons
function highlightClickedBtn(btnGroupID, clickedBtn) {
	$(btnGroupID).each(function (i, buttonGroup) {
		var $buttonGroup = $(buttonGroup);
		$buttonGroup.on('click', 'a', function () {});
		$buttonGroup.find('.is-checked').removeClass('is-checked');
		clickedBtn.addClass('is-checked');
	});
}

function initSlider(slider, countItem) {
	$('#' + slider).slick({
		slidesToShow: countItem,
		slidesToScroll: 1,
		arrows: true,
		infinite: false,
		swipeToSlide: true,
		swipe: true,
		touchMove: true,
		draggable: true,
		dots: true,
		responsive: [
			{
				breakpoint: 900,
				settings: {
					slidesToShow: 3,
				},
			},
			{
				breakpoint: 700,
				settings: {
					slidesToShow: 2,
				},
			},
			{
				breakpoint: 520,
				settings: {
					slidesToShow: 1,
				},
			},
		],
	});

	if (slider === 'offers' && $(window).width() < 520) {
		$('#offers .slick-next').click();
	}
}

$('#phoneNumber')
	.keypress(function (e) {
		if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) return false;
	})
	.keyup(function (e) {
		if ($('div.phoneNumber').hasClass('valid')) {
			var phone = $('#phoneNumber').val();
			checkPhoneFormat(phone);
		}
	});

$('#checkPhone').click(function () {
	var phone = $('#phoneNumber').val();

	$('div.phoneNumber').addClass('valid');

	if (!checkPhoneFormat(phone)) {
		dataLayer.push({
			event: 'verifyPhoneNumber',
			status: 'invalid number',
		});
		return;
	}

	showPopup(getPopupContent('inProgress').content);

	$.when(nba(phone, null, nbaVars.utm_source)).then(function (data, textStatus, jqXHR) {
		if (nbaVars.config === null && data.configuration) {
			nbaVars.config = data.configuration;
			//setScenarioPath();
		}
		if (data.offer.verifyStatus === 'OTP_SENT') {
			$('.input-phone').hide();
			$('.input-pin').show(200);
			$('.input-pin .input_text').eq(0).focus();
			$.fancybox.close();
		} else {
			showPopup(getPopupContent('error').content);
		}
		dataLayer.push({
			event: 'verifyPhoneNumber',
			status: 'valid number',
		});
	});
});

$('.input-pin .input_text')
	.focus(function () {
		$('.input-pin .placeholder').css('top', '-20px');

		if ($(this).val().length) {
			$(this).next('.input_text').focus();
		} else {
			var count = (focus = $(this).index());
			for (var i = count - 1; i >= 0; i--) {
				var comp = $('.input-pin .input_text').eq(i);
				if (!comp.val().length) focus = i;
			}

			if (count !== focus) $('.input-pin .input_text').eq(focus).focus();
		}
	})
	.focusout(function () {
		var hasVal = false;
		$('.input-pin .input_text').each(function () {
			if ($(this).val().length) hasVal = true;
		});
		if (!hasVal) $('.input-pin .placeholder').css('top', '0');
	})
	.keypress(function (e) {
		if (e.keyCode < 48 || e.keyCode > 57 || $(this).val().length > 0) return false;
	})
	.keyup(function (e) {
		if (e.keyCode === 8) {
			var attr = $(this).attr('hasVal');
			if (typeof attr === 'undefined') {
				$(this).prev('.input_text').removeAttr('hasVal').val('').focus();
			} else {
				$(this).val('').removeAttr('hasVal');
			}
		} else {
			$(this).attr('hasVal', '').next('.input_text').focus();
		}
	})
	.bind('paste', function (e) {
		e.preventDefault();
		var pin = e.originalEvent.clipboardData.getData('Text');
		//window.clipboardData.getData("Text");

		if (pin.length === 4 && pin.match(/[0-9]{4}/g)) {
			$('.input-pin .input_text').each(function (key) {
				$(this).val(pin.substr(key, 1));
			});
		}
	});

$('#checkPin').click(function () {
	var pin = '';
	$('.input-pin .input_text').each(function () {
		pin += $(this).val();
	});

	if (pin.length !== 4) return;

	showPopup(getPopupContent('inProgress').content);
	$('.phoneNumber').removeClass('valid');

	$.when(nba(null, pin, nbaVars.utm_source)).then(function (data, textStatus, jqXHR) {
		nba_action(data.offer, false);
	});
});

$('.element-item').click(function (e) {
	if ($(this).hasClass('soldOut')) {
		e.preventDefault();
		return false;
	}
	if ($(this).hasClass('ch-p')) {
		var url = $(this).find('.btn-action .link').attr('href');
		gaOfferChoose($(this), 'Viac Info');
		window.location.href = url;
	} else {
		if (!$(e.target).hasClass('btn')) {
			openPopup($(this).find('.chri_popup').attr('href'));
		}
	}
});

$(document).on('click', '.show-popup', function (e) {
	e.preventDefault();
	var type = $(this).attr('data-type');
	showPopup(getPopupContent(type).content);
});

//open popup
$('.chri_popup').on('click', function (e) {
	e.preventDefault();
	openPopup($(this).attr('href'));
});

function openPopup(element) {
	$.fancybox([
		{
			href: element,
			padding: 0,
			margin: 30,
			closeBtn: false,
			width: 550,
			height: 'auto',
			// autoDimensions: false,
			autoSize: false,
			parent: '#content',
			helpers: {
				overlay: {
					css: {
						background: 'rgba(0, 0, 0, 0.7)',
					},
				},
			},
		},
	]);
}

$('.p-close, .link-close, .text-close, .close-btn').on('click', function (e) {
	e.preventDefault();
	$.fancybox.close();
});

function checkPhoneFormat(phone) {
	if (phone.length === 10 && phone.match(/09[0-9]{8}/g)) {
		$('div.phoneNumber').removeClass('has-error').find('.error').remove();
		return true;
	} else {
		if (!$('div.phoneNumber').hasClass('has-error')) {
			var error = '<span class="error">Mobilné číslo musí byť v tvare 0903123456</span>';
			$('div.phoneNumber').addClass('has-error').append(error);
		}
		return false;
	}
}

function showPopup(content) {
	var ratioValue = 0.5;
	var width = '50%';
	var responsive = 'desktop';

	if ($(document).innerWidth() <= 767) {
		ratioValue = 1;
		width = '100%';
		responsive = 'mobile';
	}

	$.fancybox({
		content: content,
		padding: 0,
		margin: 0,
		width: width,
		height: 'auto',
		autoSize: false,
		topRatio: ratioValue,
		closeBtn: false,
		wrapCSS: 'bd-scenario-popup ' + responsive,
		parent: '#content',
		helpers: {
			overlay: {
				closeClick: false,
				css: {
					background: 'rgba(0, 0, 0, 0.7)',
				},
			},
		},
	});
}

function getParamUrl(paramName) {
	var result = null;
	var url = window.location.href;

	var start = url.indexOf('?');
	if (start > -1) {
		var params = url.substr(start + 1).split('&');
		for (var i = 0; i < params.length; i++) {
			if (params[i].startsWith(paramName + '=')) result = params[i].substr(paramName.length + 1);
		}
	}

	// ak nie je tento param tak ide z webu
	if (paramName === 'utm_source' && result === null) result = 'web';

	return result;
}

function setScenarioPath() {
	if (typeSegment === null) return;
	var oldPath = getPath(typeSegment, true);
	var newPath = getPath(typeSegment, false);
	oldPath = oldPath.substr(0, oldPath.length - 1);
	newPath = newPath.substr(0, newPath.length - 1);

	$('.scenario-path').each(function () {
		var path = $(this).attr('href');
		var replacePath = path.replace(oldPath, newPath);
		$(this).attr('href', replacePath);
	});
}

function getPath(segment, getDefaultPath) {
	var path = '';

	if (nbaVars.utm_source === 'app') {
		path = '/oneapp/predlzenie/sc';
	} else {
		if (segment === 'b2c') {
			path = '/mob';
		} else {
			path = '/biznis';
		}
		path += '/objednavka';
	}

	path += '/-/scenario/e-shop/';

	if (!getDefaultPath && config !== null) {
		path += config[segment + 'ScenarioPath'] + '?';
	} else {
		if (segment === 'b2c') {
			path += '/pre-vas-pausaly?';
		} else {
			path += '/biznis-pausaly?';
		}
	}

	return path;
}

function nba_action(data, afterLoadPage) {
	var status = typeof data !== 'undefined' && typeof data.verifyStatus !== 'undefined' ? data.verifyStatus : 'error';
	if (status === 'INVALID_OTP') {
		showPopup(getPopupContent('wrongPin').content);
		$('.input-pin .input_text').val('');
		dataLayer.push({
			event: 'verifySmsCode',
			status: 'invalid code',
			customerDiscount: '0 €',
		});
		return;
	} else if (status === 'EXPIRED_OTP') {
		showPopup(getPopupContent('EXPIRED_OTP').content);
		$('.input-pin .input_text').val('');
		$('.input-pin').hide();
		$('.input-phone').show();
		return;
	}

	if (typeof data !== 'undefined' && typeof data.productOffers !== 'undefined' && data.productOffers.length) {
		for (var offer = 0; offer < data.productOffers.length; offer++) {
			if (data.productOffers[offer].transactionType.toUpperCase() === 'TARIFFCHANGE') {
				nbaVars.nbaOffers.tariffChange.push(data.productOffers[offer]);
			} else if (typeof data.productOffers[offer].has_hardware !== 'undefined') {
				nbaVars.nbaOffers.nonHw.push(data.productOffers[offer]);
			}
		}

		if (nbaVars.nbaOffers.nonHw.length) {
			status = 'SHOW_OFFER_NON_HW';
		} else {
			nbaVars.nbaOffers.tariffChange = [];
		}

		if (status !== 'SHOW_OFFER_NON_HW' && data.productOffers[0].transactionType.toUpperCase() === 'TARIFFCHANGE') {
			showPopup(getPopupContent('TARIFF-CHANGE').content);
			$('.input-pin .input_text').val('');
			$('.input-pin').hide();
			$('.input-phone .input_text').val('');
			$('.input-phone').show();

			dataLayer.push({
				event: 'verifySmsCode',
				status: 'valid code',
				customerDiscount: '0 €',
			});
			return;
		}
	}

	if (status === 'SHOW_OFFER' || status === 'SHOW_OFFER_NON_HW') {
		if (!data.productOffers.length) {
			status = 'NO-OFFER';
			dataLayer.push({
				event: 'verifySmsCode',
				status: 'valid code',
				customerDiscount: '0 €',
			});
		} else if (typeSegment !== null && data.productOffers[0].segment.toLowerCase() !== typeSegment) {
			status = 'OTHER-SEGMENT';
		}
	}

	if (status !== 'SHOW_OFFER_NON_HW') {
		localStorage.removeItem('nbaOfferNonHwType');
	}

	var popup = getPopupContent(status);

	if (!afterLoadPage) showPopup(popup.content);

	$('#phoneChecker').find('.show-phone-number').text($('#phoneNumber').val());
	if (status === 'NO-OFFER') {
		$('#phoneChecker .results .active').removeClass('active');
		$('#phoneChecker .results .no-offer').addClass('active');
	}

	if (status === 'SHOW_OFFER' || status === 'SHOW_OFFER_NON_HW') {
		$('#phoneChecker .results .active').removeClass('active');

		if (status === 'SHOW_OFFER') {
			$('#phoneChecker .results .show-offer').addClass('active');
			localStorage.removeItem('nbaOfferNonHwType');
		} else {
			$('#phoneChecker .results .show-offer-non-hw').addClass('active');
		}

		$('#phoneChecker .checker').hide();

		//$('.user-offer').show();

		$('#offers-cont h3').html('Zvoľte si paušál z ponuky na mieru pre vás');
		$('#productsSection .rateplan-items').addClass('zlava-pre-vernych');

		setUserOffer(status === 'SHOW_OFFER' ? data.productOffers : nbaVars.nbaOffers.nonHw);

		$('.nba-hide').hide(200);
	}

	$('.checker')
		.find('.input-pin .input_text')
		.val('')
		.end()
		.find('.input-phone .input_text')
		.val('')
		.end()
		.find('.input-pin')
		.hide()
		.end()
		.find('.input-phone')
		.show()
		.end();
}

function setUserOffer(data) {
	var userOffer = '';
	var hasDiscount = false;
	nbaVars.gaRateplanes = [];
	var indexOffer = 0;

	for (var offer = 0; offer < data.length; offer++) {
		if (offer < 3) {
			if (!hasDiscount && typeof data[offer].discountedPrice !== 'undefined') hasDiscount = true;
			for (var all = 0; all < dataRatePlanes.length; all++) {
				if (data[offer].rpCode === dataRatePlanes[all].rpCode) {
					var addon = data[offer].addon ? data[offer].addon : [];
					userOffer += getPausalInfo(dataRatePlanes[all], addon, data[offer]);

					indexOffer++;
					var serviceFamily = 'T Základ';
					if (dataRatePlanes[all].category === 2) serviceFamily = 'T Dáta';
					else if (dataRatePlanes[all].category === 3) serviceFamily = 'T Ideál';
					else if (dataRatePlanes[all].category === 4) serviceFamily = 'T Nekonečno';

					nbaVars.gaRateplanes.push({
						serviceFamily: serviceFamily,
						service: dataRatePlanes[all].name,
						id: dataRatePlanes[all].rpCode,
						price:
							typeof data[offer].discountedPrice !== 'undefined'
								? data[offer].discountedPrice
								: parseFloat(dataRatePlanes[all].cena.replace(',', '.')),
						position: indexOffer,
						preselected: indexOffer === 1,
					});
				}
			}
		}
	}

	dataLayer.push({
		event: 'verifySmsCode',
		status: 'valid code',
		customerDiscount: '30 €',
	});

	dataLayer.push({
		event: 'servicesShow',
		services: nbaVars.gaRateplanes,
	});

	if ($('#offers').hasClass('slick-initialized')) {
		$('#offers').slick('removeSlide', null, null, true);
	} else {
		initSlider('offers', 4);
	}

	if (hasDiscount && !$('#offers').hasClass('discont')) $('#offers').addClass('discont');
	else if (!hasDiscount && $('#offers').hasClass('discont')) $('#offers').removeClass('discont');

	$('#offers').slick('slickAdd', userOffer);

	$('#offers-cont .cart:first').click();

	//addToolTip('.user-offer');
}

$(document).on('click', '.non-hw', function () {
	var type = parseInt($(this).attr('data-type'));

	$('.overenie .form').hide();

	//$('.user-offer').show();

	localStorage.setItem('nbaOfferNonHwType', type);
	if (type === 1) {
		window.location.href = '/volania/pausal';
	} else {
		setUserOffer(nbaVars.nbaOffers.nonHw);
	}
});

$(document).on('click', '.popup-close', function (e) {
	e.preventDefault();
	$.fancybox.close();
	var scroll = $(this).attr('data-scroll');
	if (typeof scroll !== 'undefined') {
		scrollTo($('.' + scroll), 500);
	}
});

$('#productsSection .element-item.ch-p .btn-action a').click(function (e) {
	e.preventDefault();
	var parent = $(this).closest('.element-item');
	if ($(this).hasClass('btn_disabled') || parent.hasClass('soldOut')) return false;

	var type = $(this).hasClass('btn') ? 'Nakup' : 'Viac Info';
	gaOfferChoose(parent, type);

	var url = $(this).attr('href');
	window.location.href = url;
	return false;
});

function gaOfferChoose(el, type) {
	var elId = el.attr('id');
	var parent = el.closest('.products-section').hasClass('rateplan-items') ? 'rateplan' : 'magio';
	var position = 0;

	$('#productsSection .' + parent + '-items .element-item.ch-p').each(function (key) {
		if ($(this).attr('id') === elId) position = key + 1;
	});

	dataLayer.push({
		event: 'offerChoose',
		clickType: type,
		offer: {
			offer: el.find('p.name b').text().trim(),
			id: elId,
			price: el.find('.oneTimeWeb .sum').text().trim(),
			stored: true,
			position,
		},
	});
}

$('.check-zlava-pre-vernych').change(function () {
	var dataLayerVar = 'checked';
	if ($(this).is(':checked')) {
		$('#productsSection .magio-items').addClass('zlava-pre-vernych');
	} else {
		$('#productsSection .magio-items').removeClass('zlava-pre-vernych');
		dataLayerVar = 'unchecked';
	}
	var rpCode = $('#magioProducts .cart.active').find('.title .name').text().trim();
	var selectedData = {
		parent: 'magio',
		uacId: null,
		dataProgram: { rpCode: rpCode, name: rpCode },
	};

	dataLayer.push({
		event: 'loayltyCheck',
		status: dataLayerVar,
		customerDiscount: dataLayerVar === 'checked' ? '30 €' : '0 €',
	});

	changeItemsValues(selectedData);
});

/* beautify preserve:start */
// prettier-ignore

function getPopupContent(type) {
        var content = '';
        var result = '';
        var phoneNum = $("#phoneNumber").val();
        switch (type) {
            case 'inProgress':
                content = '<div class="p-content">' +
                        '<div class="popup-lightbox">' +
                            '<div class="loader-spinning">' +
                                '<i class="t-icon--magenta d-block s3">o</i>' +
                            '</div>' +
                            '<h4 class="headline">Prebieha overovanie Vášho čísla. Ďakujeme za trpezlivosť.</h4>' +
                        '</div>' +
                    '</div>';
                break;

            case 'NO-OFFER':
                if (typeSegment !== null) {
                    content = '<div class="p-content">' +
                            '<div class="popup-lightbox">' +
                                '<div class="cont-icon"><i class="t-icon t-info">&gt;</i></div>' +
                                '<h4 class="headline">Overenie prebehlo úspešne. Pozrite si ponuku vybraných paušálov, ktoré odporúčame.</h4>' +
                                '<a class="btn btn_mag-outline popup-close" data-scroll="odporucame">Pozrieť odporúčané paušály</a>' +
                            '</div>' +
                        '</div>';
                    result = 'Pre číslo <b>' + phoneNum + '</b> momentálne nemáme špeciálnu ponuku na mieru. Pozrite si vybrané paušály, ktoré odporúčame.';
                }
                break;

            case 'SHOW_OFFER':
                content = '<div class="p-content">' +
                        '<div class="popup-lightbox">' +
                            '<img src="../assets/img/verify_success.png" />' +
                            '<p><b>Overenie prebehlo úspešne.</b></p>' +
                            '<h4 class="headline">Získali ste 30 € zľavu na nové zariadenie a ponuku šitú na mieru pre vás.</h4>' +
                            '<a class="btn btn_mag-outline popup-close">Pokračovať</a>' +
                        '</div>' +
                    '</div>';
                result = 'Overenie prebehlo úspešne, pozrite si ponuku šitú na mieru pre toto číslo ' +
                    '<p class="num">' + phoneNum + '</p>' +
                    '<p>' +
                        '<a class="magenta cursor-p show-popup" data-type="otherNumber">' +
                            'Overiť iné číslo ' +
                            '<i class="t-icon--magenta" >e</i>' +
                        '</a>' +
                    '</p>';
                break;

        case 'SHOW_OFFER_NON_HW':
            content =
				'<div class="p-content">' +
				    '<div class="popup-lightbox">' +
				        '<img src="../assets/img/verify_success.png" />' +
				        '<p><b>Overenie prebehlo úspešne.</b></p>' +
				        '<h4 class="headline">Prejdite na nový paušál bez zmeny viazanosti, alebo si k paušálu vezmite aj mobil či iné zariadenie s&nbsp;novou 24&#8722;mesačnou viazanosťou.</h4>' +
                        '<a href="#" class="btn btn_mag-outline non-hw popup-close" data-type="1">Chcem iba paušál</a>' +
                        '<a href="#" class="btn cta_mag non-hw popup-close" data-type="2">Chcem aj nové zariadenie</a>' +
				    '</div>' +
				'</div>';
			result = 'Overenie čísla prebehlo úspešne. Zvolili ste si možnosť k novému paušálu vybrať aj mobil alebo iné zariadenie s novou 24&#8722;mesačnou viazanosťou.' +
				    '<p class="num">' +
				        phoneNum +
				    '</p>' +
				    '<p>' +
				        '<a class="magenta cursor-p show-popup" data-type="otherNumber">' +
				            'Overiť iné číslo ' +
				            '<i class="t-icon--magenta" >e</i>' +
				        '</a>' +
                    '</p>';
            break;                 

            case "otherNumber":
                content = '<div class="p-content">' +
					    '<div class="popup-lightbox">' +
					        '<div class="iconDone"><i class="t-icon--magenta--border s3">ä</i></div>' +
					        '<h4 class="headline">Ste si istý, že chcete overiť iné číslo alebo zrušiť overenie?</h4>' +
					        '<p class="text">Overením iného čísla alebo zrušením overenia sa váš nákupný košík vyprázdni a&nbsp;ponuka vynuluje.</p>' +
                            '<div class="btn-group">' +
                                '<a class="btn cta_mag popup-close reset-form">Overiť iné číslo</a>' +
                                '<a class="btn btn_mag-outline popup-close" >Zavrieť okno</a>' +
                            '</div>' +
					    '</div>' +
					'</div>';
                result = null;
                break;

            case 'wrongPin':
                content = '<div class="p-content">' +
                        '<div class="popup-lightbox">' +
                            '<img src="../assets/img/re-enter.png" />' +
                            '<h4 class="headline">Ľutujeme, zadaný kód nie je správny.</h4>' +
                            '<p class="text">Skúste ho zadať znova, prosím.</p>' +
                            '<a class="btn btn_mag-outline popup-close">Zadať kód znova</a>' +
                        '</div>' +
                    '</div>';
                result = ''
                break;

            case 'EXPIRED_OTP':
                content = '<div class="p-content">' +
                        '<div class="popup-lightbox">' +
                            '<img src="../assets/img/verify_failed.png" />' +
                            '<h4 class="headline">Ľutujeme, zadaný kód nie je správny.</h4>' +
                            '<p class="text">Zadaný kód z SMS už nie je aktuálny. Zadajte telefónne číslo znova</p>' +
                            '<a class="btn btn_mag-outline popup-close">Pokračovať</a>' +
                        '</div>' +
                    '</div>';
                result = 'Ľutujeme, Váš kód z SMS už nie je aktuálny. Zadajte telefónne číslo znova.'
                break;

            case 'OTHER-SEGMENT':
                if (typeSegment === 'b2c') {
                    content = '<div class="p-content">' +
                            '<div class="popup-lightbox">' +
                                '<img src="../assets/img/verify_success.png" />' +
                                '<h4 class="headline">Overenie prebehlo úspešne.</h4>' +
                                '<p>Získali ste 30 € zľavu na nové zariadenie a biznis ponuku šitú na mieru pre Vás.</p>' +
                                '<a href="/biznis/vianoce" class="btn cta_mag">Pozrieť ponuku pre Váš biznis</a>' +
                                '<a class="btn btn_mag-outline popup-close">Ostať na ponuke pre Vás</a>' +
                            '</div>' +
                        '</div>';
                    result = 'Overenie prebehlo úspešne, pozrite si ponuku šitú na mieru pre číslo ' +
                        '<b>' + phoneNum + '</b>' +
                        ' v časti webu pre ' +
                        '<a href="/biznis/vianoce" class="magenta cursor-p">Váš biznis</a>.' +
                        '<p>' +
                            '<a class="magenta cursor-p show-popup" data-type="otherNumber">' +
                                'Overiť iné číslo ' +
                                '<i class="t-icon--magenta" >e</i>' +
                            '</a>' +
                        '</p>';
                } else {
                    content = '<div class="p-content">' +
                            '<div class="popup-lightbox">' +
                                '<img src="../assets/img/verify_success.png" />' +
                                '<h4 class="headline">Overenie prebehlo úspešne.</h4>' +
                                '<p>Získali ste 30 € zľavu na nové zariadenie a ponuku šitú na mieru pre Vás</p>' +
                                '<a href="/vianoce" class="btn cta_mag">Pozrieť ponuku pre Vás</a>' +
                                '<a class="btn btn_mag-outline popup-close">Ostať na ponuke pre Váš biznis</a>' +
                            '</div>' +
                        '</div>';
                    result = 'Overenie prebehlo úspešne, pozrite si ponuku šitú na mieru pre číslo ' +
                        '<b>' + phoneNum + '</b>' +
                        ' v sekcii pre ' +
                        '<a href="/vianoce" class="magenta cursor-p">Vás</a>.' +
                        '<p>' +
                            '<a class="magenta cursor-p show-popup" data-type="otherNumber">' +
                                'Overiť iné číslo ' +
                                '<i class="t-icon--magenta" >e</i>' +
                            '</a>' +
                        '</p>';
                }
                break;

            case 'TARIFF-CHANGE':
                content = '<div class="p-content">' +
					    '<div class="popup-lightbox">' +
					        '<div class="iconDone"><i class="t-icon--magenta--border s3">ä</i></div>' +
					        '<p class="text">Telefónne číslo ' + phoneNum + ' zatiaľ nie je možné predĺžiť. Už teraz s ním však môžete prejsť na nový paušál a užívať si jeho výhody. Vaša viazanosť sa nezmení.</p>' +
                            '<p>Ak si chcete vybrať z vianočnej ponuky zariadení, pokračujte overením iného čísla alebo si zvoľte z paušálov nižšie a získate nové číslo.</p>' +
                            '<div class="btn-group">' +
                            '<a class="btn cta_mag popup-close" >Vybrať si z vianočnej ponuky</a>' +
                            '<a href="/volania/pausal" class="btn btn_mag-outline">Pozrieť moju ponuku paušálov</a>' +
                            '</div>' +
					    '</div>' +
					'</div>';                
            break;

            default:
                content = '<div class="p-content">' +
                        '<div class="popup-lightbox">' +
                            '<img src="../assets/img/verify_failed.png" />' +
                            '<h4 class="headline">Ľutujeme, Vaše číslo nebolo možné overiť.</h4>' +
                            '<p class="text"><b>Skúste ho overiť neskôr</b> alebo si vyberte zariadenie z vianočnej ponuky s novým číslom.</p>' +
                            '<div class="btn-group">' +
                                '<a class="btn btn_mag-outline popup-close">Pokračovať</a>' +
                            '</div>' +
                        '</div>' +
                    '</div>';
                result = 'Vaše číslo <b>' + phoneNum + '</b> nebolo možné z technických príčin overiť.Skúste to neskôr.'
                break;
        }

        return {
            content: content,
            result: result
        };
    }

// prettier-ignore

function getPausalInfo(data, addon, dataOffer) {
        var addons = data.darcek.concat(addon);

        var result =
            '<div class="cart' + 
                (data.id === initRatePlaneId ? ' active' : '') + '"' +
                ' data-id="' + data.id + '"' +
                (dataOffer ? 'data-uac="' + dataOffer.uacId + '"' : '') + 
            '>' +
                '<p class="title">' +
                    '<span class="name">' + data.name + '</span> - ' + 
                    (data.data[0] !== 'Nekonečné' ? 
                        data.data[0] + ' ' + data.data[1] : 
                        '<img src="../assets/img/osmicka_otvoreny-pausal_360px.png" />') +
                '</p>' +
                '<p>' +
                    'Volania: ' + data.volania[0] + ' ' + data.volania[1] +
                '</p>' +
                '<p>' +
                    'SMS a MMS: ' + data.sms[0] + ' ' + data.sms[1] +
                '</p>';

        for (var i = 0; i < addons.length; i++) {
            result +=
                '<p>' +
                    addons[i].name +
                '</p>';
        }

        result +=
			'<p class="price">' +
        (dataOffer && dataOffer.discountedPrice && dataOffer.discountedPrice.toFixed(2).replace('.', ',') !== data.cena
            ? dataOffer.discountedPrice.toFixed(2).replace('.', ',') +
				  ' € mesačne po zľave<br />' +
				  '<span class="line-through-m">' +
				  data.cena +
				  ' €</span>'
				: data.cena + ' € mesačne</span>') +
			'</p>' +
			'</div>';

        return result;
    }

/* beautify preserve:end */

function nba(phone, pin) {
	if (typeof phone === 'undefined') phone = null;
	if (typeof pin === 'undefined') pin = null;
	var url = '';
	if (phone === null && pin === null) {
		url = 'http://json.rdg-dev.eu/api/bd-init-page-bad-request';
		//url = 'http://json.rdg-dev.eu/api/bd-non-hw';
	} else if (pin === null) {
		url = 'http://json.rdg-dev.eu/api/bd-pin';
	} else if (phone === null) {
		var phoneNum = $('#phoneNumber').val();
		if (phoneNum.startsWith('0911')) url = 'http://json.rdg-dev.eu/api/bd-prolongation';
		else if (phoneNum.startsWith('0912')) url = 'http://json.rdg-dev.eu/api/bd-prolongation-with-addon';
		else if (phoneNum.startsWith('0913')) url = 'http://json.rdg-dev.eu/api/bd-tariff-change';
		else if (phoneNum.startsWith('0914')) url = 'http://json.rdg-dev.eu/api/bd-expired-otp';
		else if (phoneNum.startsWith('0915')) url = 'http://json.rdg-dev.eu/api/bd-b2b';
		else if (phoneNum.startsWith('0917')) url = 'http://json.rdg-dev.eu/api/error';
		else if (phoneNum.startsWith('0918')) url = 'http://json.rdg-dev.eu/api/bd-one-offer';
		else if (phoneNum.startsWith('0919')) url = 'http://json.rdg-dev.eu/api/bd-non-hw';
		else if (phoneNum.startsWith('0921')) url = 'http://json.rdg-dev.eu/api/bd-non-hw2';
		else url = 'http://json.rdg-dev.eu/api/bd-no-offer';
	}

	return $.ajax({
		url: url,
		dataType: 'json',
		type: 'GET',
	});
}

$('.scroll-to').click(function (e) {
	e.preventDefault();
	scrollTo($(this.hash));

	var gAttr = $(this).attr('data-ga');
	if (typeof gAttr !== 'undefined') {
		dataLayer.push({
			event: gAttr,
		});
	}
});

function scrollTo(target) {
	if (target.length) {
		var offset = 0;

		if (!target.hasClass('vianocne-akcie')) {
			offset = 90;
		}

		if ($(window).width() > 550) {
			$('html,body').animate(
				{
					scrollTop: target.offset().top - $('#nav-sticky').outerHeight() - offset,
				},
				1000
			);
		} else {
			$('html,body').animate(
				{
					scrollTop: target.offset().top - offset,
				},
				1000
			);
		}
		return false;
	}
}
