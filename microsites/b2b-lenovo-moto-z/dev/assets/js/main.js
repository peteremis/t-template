$(function() {

    /* STICKY NAV */

    $("#nav-sticky").sticky({
        topSpacing: 0,
        widthFromWrapper: true
    });

    $(".scroll-to").click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top - ($("#nav-sticky").outerHeight()-2)
                }, 1000);
                return false;
            }
        }
    });

    var contentSections = $("[id*='sec-']"),
        secondaryNav = $("#nav-sticky");

    function updateSecondaryNavigation() {
        contentSections.each(function() {
            var actual = $(this),
                actualHeight = actual.height() + parseInt(actual.css('paddingTop').replace('px', '')) + parseInt(actual.css('paddingBottom').replace('px', '')),
                actualAnchor = secondaryNav.find('a[href="#' + actual.attr('id') + '"]');

            if ((actual.offset().top - secondaryNav.outerHeight() - 1 <= $(window).scrollTop()) && (actual.offset().top + actualHeight - secondaryNav.outerHeight() > $(window).scrollTop())) {
                actualAnchor.addClass('active');
            } else {
                actualAnchor.removeClass('active');
            }

        });
    }
    updateSecondaryNavigation();

    $(window).scroll(function(event) {
        updateSecondaryNavigation();
    });

    //slide DownUp Configuration
    // toggle link in specs
    $(".toggle-info").click(function() {
        //toggle text on btn
        $(this).text($(this).text() == 'Viac informácií' ? 'Menej informácií' : 'Viac informácií');
        //toggle upDown icon
        $(this).toggleClass('t-ico-down t-ico-up');

        if ($("#sec-9").hasClass("height-transition-hidden"))
            $("#sec-9").slideDownTransition();
        else
            $("#sec-9").slideUpTransition();

    });
    //Bottom page toggle link
    $(".toggle-info-up-dark").click(function() {
        $("#sec-9").slideUpTransition();
        $(".toggle-info").text($(".toggle-info").text() == 'Viac informácií' ? 'Menej informácií' : 'Viac informácií');
        $(".toggle-info").toggleClass('t-ico-down t-ico-up');
    });

});

// slideDown-Up plugin
(function($) {

    $.fn.slideUpTransition = function() {
        return this.each(function() {
            var $el = $(this);
            $el.css("max-height", "0");
            $el.addClass("height-transition-hidden");

        });
    };

    $.fn.slideDownTransition = function() {
        return this.each(function() {
            var $el = $(this);
            $el.removeClass("height-transition-hidden");

            // temporarily make visible to get the size
            $el.css("max-height", "none");
            var height = $el.outerHeight();

            // reset to 0 then animate with small delay
            $el.css("max-height", "0");

            setTimeout(function() {
                $el.css({
                    "max-height": height
                });
            }, 1);
        });
    };
})(jQuery);
