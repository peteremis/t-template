$(function() {

    $(document).ready(function() {
        $("#products_slider").slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            centerMode: false,
            infinite: false,
            swipeToSlide: true,
            swipe: true,
            arrows: true,
            dots: true,
            autoplay: false,
            autoplaySpeed: 5000,
            customPaging: function(slider, i) {
                // this example would render "tabs" with titles
                return '<span class="slide-dot">&nbsp;</span>';
            },
            responsive: [{
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    arrows: false
                }
            }, {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false,
                    infinite: true
                }
            }]
        });

        $(document).on('click', '.button_gray', function(e) {
            e.preventDefault();
        });

        $(".scroll-to").click(function() {
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    $('html,body').animate({
                        scrollTop: target.offset().top - ($("#nav-sticky").outerHeight())
                    }, 1000);
                    return false;
                }
            }
        });

        // init
        var controller = new ScrollMagic.Controller();

        var $hand = $('.hand');
        var handIntro = TweenMax.from($hand, 2.5, {
            yPercent: 0,
            xPercent: 40,
        });
        var screenIntro = TweenMax.to('.screen', 1, {
            alpha: 1,
            ease: Power1.easeIn,
        });

        var introTl = new TimelineMax()
            .add(handIntro)
            .add(screenIntro);

        var containerScene = new ScrollMagic.Scene({
                triggerElement: '.sec-2',
                reverse: true
            })
            .setTween(introTl)
            // .addIndicators()
            .addTo(controller);
    });
});
