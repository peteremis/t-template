initSlider("#vyhody_slider");
$("#sec-5 .all-benefits").click(function (e) {
  e.preventDefault();
  var comps = $("#sec-5 .hide-mobil");
  if (comps.is(":visible")) {
    comps.hide(200);
    $(this).text("Ďaľšie výhody");
  } else {
    comps.show(200);
    $(this).text("Menej výhod");
  }
});

function initSlider(comp) {
  if (!$(comp).hasClass("slick-initialized")) {
    $(comp).slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      arrows: true,
      infinite: false,
      swipeToSlide: true,
      swipe: true,
      touchMove: true,
      draggable: true,
      dots: true,
      responsive: [
        {
          breakpoint: 800,
          settings: {
            slidesToShow: 2,
          },
        },
        {
          breakpoint: 503,
          settings:
            comp === "#vyhody_slider"
              ? "unslick"
              : {
                  slidesToShow: 1,
                },
        },
      ],
    });
  }
}
