$(function () {
    
    /* ANIMATION */
    
    var controller = new ScrollMagic.Controller();

    var tweenTitle = TweenMax.to("#title1", 1, {
        className: "+=anim_title-1"
    });

    var sceneTitle = new ScrollMagic.Scene({
            triggerElement: "#trigger1",
            duration: 1500,
            offset: -100
        })
        .setTween(tweenTitle)
        .addTo(controller);

    var tweenTitle2 = TweenMax.to("#title2", 1, {
        className: "+=anim_title-2"
    });

    var sceneTitle2 = new ScrollMagic.Scene({
            triggerElement: "#trigger2",
            duration: 700,
            offset: -500
        })
        .setTween(tweenTitle2)
        .addTo(controller);

    var tween = TweenMax.to("#animate1", 1, {
        className: "+=anim_players-1"
    });

    var scene = new ScrollMagic.Scene({
            triggerElement: "#trigger1",
            duration: 800,
            offset: -200
        })
        .setTween(tween)
        .addTo(controller);

    var tween2 = TweenMax.to("#animate2", 1, {
        className: "+=anim_stadium"
    });

    var scene_scale = new ScrollMagic.Scene({
            triggerElement: "#trigger1",
            duration: 800
        })
        .setTween("#animate1", {
            scale: 1.4
        })
        .addTo(controller);

    var scene2 = new ScrollMagic.Scene({
            triggerElement: "#trigger1",
            duration: 800,
            offset: -100
        })
        .setTween(tween2)
        .addTo(controller);

    var tween3 = TweenMax.to("#animate4", 1, {
        className: "+=anim_players-2"
    });

    var scene3 = new ScrollMagic.Scene({
            triggerElement: "#trigger2",
            duration: 800,
            offset: -500
        })
        .setTween(tween3)
        .addTo(controller);

    var tween4 = TweenMax.to("#animate3", 1, {
        className: "+=anim_stadium-2"
    });

    var scene4 = new ScrollMagic.Scene({
            triggerElement: "#trigger2",
            duration: 800,
            offset: -500
        })
        .setTween(tween4)
        .addTo(controller);

    var tween5 = TweenMax.to("#animate5", 1, {
        className: "+=anim_players-3"
    });

    var scene5 = new ScrollMagic.Scene({
            triggerElement: "#trigger3",
            duration: 800,
            offset: -150
        })
        .setTween(tween5)
        .addTo(controller);
    
    
    /* PLAYER LIST */
    
    var openedObject = null,
        firstLoad = true,
        initallyOpen = '11',
        initallyOpenClassname = '.player-' + initallyOpen;

    $('.stadium-inner').on('click', function (e) {
        e.preventDefault();

        if ($('.player').is(':animated')) {
            return;
        }
        
        var currentClickedLinkObject = $(e.target).closest('a'),
            classname = currentClickedLinkObject.attr('class'),
            suffix = classname.match(/\d+/),
            stringSuffix = suffix.toString(),
            playerInfoClassName = '.player-' + suffix,
            currentPlayerInfo = $(playerInfoClassName);


        if (openedObject) {
            var classnameOpenedObject = openedObject.attr('class'),
                suffixOpenedObject = classnameOpenedObject.match(/\d+/),
                stringSuffixOpenedObject = suffixOpenedObject.toString();
        }

        if (!openedObject && (stringSuffix !== stringSuffixOpenedObject) && !firstLoad) {

            currentPlayerInfo.animate({
                left: '10px'
            }, 500, function () {
                currentPlayerInfo.addClass('open');
                openedObject = currentPlayerInfo;
            });
        } else {

            if (firstLoad) {
                $(initallyOpenClassname).animate({
                    left: '-400px'
                }, 500, function () {
                    if (initallyOpen !== stringSuffix) {
                        currentPlayerInfo.animate({
                            left: '10px'
                        }, 500, function () {
                            currentPlayerInfo.addClass('open');
                            openedObject = currentPlayerInfo;
                        });
                    }
                });

                firstLoad = false;
            }

            openedObject.animate({
                left: '-400px'
            }, 500, function () {
                currentPlayerInfo.removeClass('open');
                openedObject = null;
                if (stringSuffix !== stringSuffixOpenedObject) {
                    currentPlayerInfo.animate({
                        left: '10px'
                    }, 500, function () {
                        currentPlayerInfo.addClass('open');
                        openedObject = currentPlayerInfo;
                    });
                }
            });

        }

    });

});