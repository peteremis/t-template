phones = [
	{
		"id": "p10_y6",
		"name": "Huawei P10 lite Blue + darček Huawei Y6 2017",
		"pausal": "Happy XXL",
		"monthly": "1.00",
		"price1": "29.04",
		"price2": "49.04",
		"image": "phone_p10_y6.png",
		"url": "https://www.telekom.sk/mob/objednavka/-/scenario/e-shop/hw-happy/vyber-hw/huawei_p10_lite_blue_huawei_y6_2017"
	},
	{
		"id": "e4_lenovo500",
		"name": "Lenovo Moto E4 + darček reproduktor Lenovo 500 2.0",
		"pausal": "Happy S",
		"monthly": "3.00",
		"price1": "1.00",
		"price2": "9.00",
		"image": "phone_e4_lenovo500.png",
		"url": "https://www.telekom.sk/mob/objednavka/-/scenario/e-shop/hw-happy/vyber-hw/lenovo_moto_e4_lenovo_500"
	},
	{
		"id": "e4_jbl",
		"name": "Lenovo Moto E4 + darček slúchadlá JBL T450BT",
		"pausal": "Happy S",
		"monthly": "3.00",
		"price1": "1.00",
		"price2": "9.00",
		"image": "phone_e4_jbl.png",
		"url": "https://www.telekom.sk/mob/objednavka/-/scenario/e-shop/hw-happy/vyber-hw/lenovo_moto_e4_sluchadla_jbl_t450bt"
	},
	{
		"id": "e4_sportg02",
		"name": "Lenovo Moto E4 + darček Lenovo Sports Band G02",
		"pausal": "Happy S",
		"monthly": "3.00",
		"price1": "1.00",
		"price2": "9.00",
		"image": "phone_e4_bandg02.png",
		"url": "https://www.telekom.sk/mob/objednavka/-/scenario/e-shop/hw-happy/vyber-hw/lenovo_moto_e4_lenovo_sports_band_g02"
	},
	{
		"id": "ps4_t3",
		"name": "PlayStation 4 Slim + darčeky Lenovo Moto C a hra",
		"pausal": "Happy XXL",
		"monthly": "1.00",
		"price1": "29.04",
		"price2": "49.04",
		"image": "phone_ps4_motoc.png",
		"url": "https://www.telekom.sk/mob/objednavka/-/scenario/e-shop/hw-happy/vyber-hw/playstation_4_slim_1hra"
	},
	{
		"id": "ps4_shock",
		"name": "PlayStation 4 Slim + darčeky dualshock ovládač a 3 hry",
		"pausal": "Happy XXL",
		"monthly": "1.00",
		"price1": "29.00",
		"price2": "49.00",
		"image": "phone_ps4_shock.png",
		"url": "https://www.telekom.sk/mob/objednavka/-/scenario/e-shop/hw-happy/vyber-hw/playstation_4_slim_3hry"
	},
];

generatePhones(phones);

function generatePhones(phones) {
	var html = '';

	var imageUrl = 'https://m.telekom.sk';
	if (window.location.hostname === 'localhost') {
		imageUrl = 'http://localhost/assets/img/';
	}

	for (var a = 0; a < phones.length; a++) {
		html = $([
			'<div id="device-' + phones[a].id + '" class="box-device device-' + a + '">',
			'<div class="img__holder">',
			'<a href="' + phones[a].url + '"><img src="../assets/img/' + phones[a].image + '" alt=""></a>',
			'</div>',
			'<p class="device__name"><a href="' + phones[a].url + '">' + phones[a].name + '</a></p>',
			'<p class="pricing__title">Napr. k ' + phones[a].pausal + ':</p>',
			'<table class="pricing-table">',
			'<tr class="first-row">',
			'<td>Mesačne</td>',
			'<td>' + phones[a].monthly + ' €</td>',
			'</tr>',
			'<tr class="second-row">',
			'<td>Jednorazovo</td>',
			'<td>',
			'<div class="magenta-text">na webe ' + phones[a].price1 + ' €</div>',
			'</td>',
			'</tr>',
			'<tr class="third-row">',
			'<td>&nbsp;</td>',
			'<td>',
			'<div class="strike-text">v predajni ' + phones[a].price2 + ' €</div>',
			'</td>',
			'</tr>',
			'</table>',
			'<div class="btn-cta">',
			'<a class="btn cta" href="' + phones[a].url + '">PREJSŤ K NÁKUPU</a>',
			'</div>',
			'</div>'
		].join("\n"));

		$(html).appendTo(".box__holder");
	}


}
