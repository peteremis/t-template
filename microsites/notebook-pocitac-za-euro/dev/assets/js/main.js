$(function() {

    $(".tabs-menu a").click(function (event) {
        event.preventDefault();
        if (!$(this).parent().hasClass("current") && !$(this).parent().hasClass("isVariant")) {
        	toggleTabs();
        }

        $(this).parent().addClass("current");
        $(this).parent().siblings().removeClass("current");
        var tab = $(this).attr("href"),
        	parent = $(this).closest('ul').parent().parent();
        parent.find('.tab-content').not(tab).css("display", "none");
        $(tab).fadeIn();

    });

    var toggleTabs = function () {
    	$('.tab__variant').toggleClass('selected');
    };

    $(".scroll-to").click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top - ($("#nav-sticky").outerHeight() - 10)
                }, 1000);
                return false;
            }
        }
    });


    /* PRODUCTS LINKING FROM LANDING PAGE */

    function bingUrlToProd(prodIdName, url) {
        $('#' + prodIdName).on('click', function() {
            window.location.href = url;
        });
    }

    bingUrlToProd('ideapad-l', 'https://www.telekom.sk/notebook-pocitac-za-euro/ideapad');
    bingUrlToProd('ideapad-xl', 'https://www.telekom.sk/notebook-pocitac-za-euro/ideapad?happy=xl');
    bingUrlToProd('ideacentre-l', 'https://www.telekom.sk/notebook-pocitac-za-euro/ideacentre');
    bingUrlToProd('ideacentre-xl', 'https://www.telekom.sk/notebook-pocitac-za-euro/ideacentre?happy=xl');
});
