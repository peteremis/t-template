$(function() {
    var jsonURL = '/documents/10179/10133753/links.json?v=2',
    testURL = '../assets/js/links.json',
    URL = null,
    $secHelp = $('#sec-help'),
    $papListHolder = $('.pap__list'),
    buildedItems = '';

    init();

    function init() {
        checkEnv();
        makeAjaxCall();
    }

    function checkEnv() {
        if (window.location.hostname === 'localhost') {
            URL = testURL;
        } else {
            URL = jsonURL;
        }
    }

    function makeAjaxCall() {
        $.ajax({
            type: "GET",
            async: true,
            crossDomain: true,
            dataType: "json",
            url: URL,
            success: function (items) {
                if (getTopic() !== false) {
                    var papItemArr = getItems(items);
                    //console.log(papItemArr);
                    if (papItemArr !== undefined && papItemArr !== null && papItemArr !== '' && papItemArr !== false) {
                        showSection();
                        for (var i = 0; i < papItemArr.length; i++) {
                            buildedItems += getItemHtml(papItemArr[i]);
                        }
                        appendItemsToParrent(buildedItems);
                    }
                }
            },
            error: function () {

            }
        });
    }

    function getPath() {
        return window.location.pathname.substr(1);
    }
    //console.log(window.location.pathname.substr(1));

    function getTopic() {
        try {
            return papTopic;
        } catch (e) {
            console.log('Topic nie je definovaný. Pridajte premennú papTopic.');
            return false;
        }
    }

    function hideSection() {
        $secHelp.hide();
    }

    function showSection() {
        $secHelp.show();
    }

    function getItems(items) {
        var topic = getTopic();
        return items[topic];
    }

    function getItemHtml(item) {
        return '<a href="' + item['linkURL'] + '" class="pap__li-link">' +
        '<div class="pap__li">' +
        '<p class="link pap__li-text">' + item['linkText'] + '</p>' +
        '</div>' +
        '</a>';
    }

    function appendItemsToParrent(items) {
        $(items).appendTo($papListHolder);
    }
});
