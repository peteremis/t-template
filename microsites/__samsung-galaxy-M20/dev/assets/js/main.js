$(function() {

    //back to to previous url
    $('#a_referrer a').attr('href',document.referrer);

    /* TABS */
    $(".tabs-menu a").click(function(event) {
        event.preventDefault();
        $(this).parent().addClass("current");
        $(this).parent().siblings().removeClass("current");
        var tab = $(this).attr("href"), // select clicked tab. In example "#tab-1" ..
            parent = $(this).closest('ul').parent().parent(); // <tabs-container>
        parent.find('.tab-content').not(tab).css("display", "none"); //hide all not clicked tabs
        $(tab).fadeIn(); //show clicked tab


    });
    /* TABS END */

    var dataSegment = $("[data-segment]").each(function(){
      console.log($(this).attr('data-segment')); //zobraz "data-segment" hodnotu pre kazdu <section>

      closestHead = $($(this).find('.tabs-menu a'));
      closestItemCount = closestHead.length - 1; //vytiahni pocet tabov pre kazdu <section> s atributom "data-segment"
      //console.log(closestItemCount + "heads");

      closestContent = $($(this).find('.tab-content')); //najdi tab-content v danej <section>
      //console.log(closestContent);

      closestAccordion = $(this).find('.accordion'); //najdi .accordion v danej <section>

      accordionItem = '<div class="item">';

      for (var i = 0; i <= closestItemCount; i++) {
          accordionItem += '<div class="heading">' + $(closestHead[i]).text()  + '</div>';
          accordionItem += '<div class="content">' + $(closestContent[i]).html() + '</div></div>';

          if (i !== closestItemCount) {
              accordionItem += '<div class="item">';
          }
      }

      //if data-segment and data-accordion value match, show accordion data
      if($(this).attr('data-segment') === closestAccordion.attr('data-accordion')) {
           $(accordionItem).appendTo(closestAccordion);
      }

      var $items = $('.accordion .item');
      //SET OPENED ITEM
      $($items[0]).addClass('open');

    });

    /* ACCORDION */
    $('.accordion .item .heading').click(function(e) {

        var clickedHead = e.target;
        var $item = $(clickedHead).closest('.item');
        var isOpen = $item.hasClass('open');
        var $content = $item.find('.content');
        var $acPrice = $(clickedHead).find('.ac-price');

        if (isOpen) {
            $content.slideUp(200);
            $item.removeClass('open');
            $acPrice.show();
        } else {
            $content.slideDown(200);
            $item.addClass('open');
            $acPrice.hide();
        }
    });


    $('.accordion .item.open').find('.content').slideDown(200);
    /* ACCORDION END */

    $(".scroll-to").click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top - ($("#nav-sticky").outerHeight() - 10)
                }, 1000);
                return false;
            }
        }
    });

    // $('.info-qm').qtip({
    //     content: {
    //         text: 'Ponuku môžete využiť aj k Magio televízii či k Pevnej linke M, L alebo XL. Prejdite k nákupu, kde si vyberiete ľubovoľnú službu.'
    //     }
    // });

    /* HEAPBOX CALC - START */
    var heapVal = $('#heapbox_heap .holder').prop('rel'); //init value

    $( document ).ready(function() {
        $('ul.heapOptions').find("li").eq(3).find('a').click(); //set default pausal to "ANO L"
    });

    $('.heapOption a').on('click', function(){
        heapVal = $('#heapbox_heap .holder').prop('rel'); //get new value

        function setPrices(MonthlyFeeService, MonthlyFeeWithMob, MonthlyMagenta1fee, magentaPrice, regularPrice, rateplan) {
            $('#MonthlyFeeService').text(MonthlyFeeService + ",00 €");
            $('#MonthylFeeWithMob').text(MonthlyFeeWithMob + ",00 €");
            $('#MonthlyMagenta1fee').text(MonthlyMagenta1fee + ",00 €");
            $('#magentaPrice').text(magentaPrice + ",00 €");
            $('#regularPrice').text(regularPrice + ",00 €");
            $('.btn.selectrp').attr('href', '/mob/objednavka/-/scenario/e-shop/hw-pausal/vyber-hw/samsung_galaxy_m20?vyberrp=' + rateplan);
        }

        switch (heapVal) {
            case "S":
                setPrices(10, 3, 0, 149, 399, 'S');
                break;
            case "M":
                setPrices(20, 3, 0, 119, 399, 'M');
                break;
            case "L":
                setPrices(30, 2, 0, 1, 399, 'L');
                break;     
            case "XL":
                setPrices(40, 1, 0, 1, 399, 'XL');
                break;  
            case "XXL":
                setPrices(70, 1, 0, 1, 399, 'XXL');
                break;             
            default:
                break;
        }
    });
    /* HEAPBOX CALC - END */
});
