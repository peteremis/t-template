(function() {
    /**
     * Magenta Filter 
     * @author  MG
     * @version 1.2, 7/11/2016
     */
    //TODO create final obj with hw and noHw prices
    //TODO add result html to directive
    'use strict';

    var app = angular.module('magentaModule', ['dragularModule', 'ngAnimate', 'uiSwitch']);

    app.controller('stepOneCtrl', ['$scope', '$element', 'dragularService', '$http', '$timeout', '$filter', function($scope, $element, dragularService, $http, $timeout, $filter) {

        var vm = this;

        //load json data
        // $http.get('../assets/js/pausaly.json').success(function(response) {
        //     vm.test = response.programs;
        //     console.log('response', vm.test);
        // });

        //JSON DATA
        vm.pausaly = [{
            "id": 0,
            "type": "",
            "size": "Easy",
            "class": "Easy",
            "pSize": 0,
            "cenaHw": {
                "size0p": 4.50,
                "size2p": 4.50,
                "size3p": 4.50,
                "size4p": 4.50,
                "size5p": 4.50
            },
            "cenaNoHw": {
                "size0p": 4.50,
                "size2p": 4.50,
                "size3p": 4.50,
                "size4p": 4.50,
                "size5p": 4.50
            }
        }, {
            "id": 1,
            "type": "Happy ",
            "class": "XS mini",
            "size": "XS mini",
            "pSize": 0,
            "cenaHw": {
                "size0p": 5.99,
                "size2p": 5.49,
                "size3p": 4.99,
                "size4p": 4.49,
                "size5p": 3.99
            },
            "cenaNoHw": {
                "size0p": 4.99,
                "size2p": 4.49,
                "size3p": 3.99,
                "size4p": 3.49,
                "size5p": 2.99
            }
        }, {
            "id": 2,
            "type": "Happy ",
            "size": "XS",
            "class": "XS",
            "pSize": 0,
            "cenaHw": {
                "size0p": 9.99,
                "size2p": 8.99,
                "size3p": 8.49,
                "size4p": 7.99,
                "size5p": 7.49
            },
            "cenaNoHw": {
                "size0p": 9.99,
                "size2p": 8.99,
                "size3p": 8.49,
                "size4p": 7.99,
                "size5p": 7.49
            }
        }, {
            "id": 3,
            "type": "Happy ",
            "size": "S",
            "class": "S",
            "pSize": 0,
            "cenaHw": {
                "size0p": 16.99,
                "size2p": 15.99,
                "size3p": 14.99,
                "size4p": 13.99,
                "size5p": 12.99
            },
            "cenaNoHw": {
                "size0p": 15.99,
                "size2p": 14.99,
                "size3p": 13.99,
                "size4p": 12.99,
                "size5p": 11.99
            }
        }, {
            "id": 4,
            "type": "Happy ",
            "size": "M",
            "class": "M",
            "pSize": 1,
            "cenaHw": {
                "size0p": 23.99,
                "size2p": 21.99,
                "size3p": 20.99,
                "size4p": 19.99,
                "size5p": 18.99
            },
            "cenaNoHw": {
                "size0p": 19.99,
                "size2p": 17.99,
                "size3p": 16.99,
                "size4p": 15.99,
                "size5p": 14.99
            }
        }, {
            "id": 5,
            "type": "Happy ",
            "size": "L",
            "class": "L",
            "pSize": 1,
            "cenaHw": {
                "size0p": 29.99,
                "size2p": 27.99,
                "size3p": 25.99,
                "size4p": 23.99,
                "size5p": 21.99
            },
            "cenaNoHw": {
                "size0p": 27.99,
                "size2p": 25.99,
                "size3p": 23.99,
                "size4p": 21.99,
                "size5p": 19.99
            }
        }, {
            "id": 6,
            "type": "Happy ",
            "size": " XL vol",
            "class": "XS vol",
            "pSize": 1,
            "cenaHw": {
                "size0p": 29.99,
                "size2p": 27.99,
                "size3p": 21.99,
                "size4p": 21.99,
                "size5p": 21.99
            },
            "cenaNoHw": {
                "size0p": 27.99,
                "size2p": 25.99,
                "size3p": 19.99,
                "size4p": 19.99,
                "size5p": 19.99
            }
        }, {
            "id": 7,
            "type": "Happy ",
            "size": "XL",
            "class": "XL",
            "pSize": 1,
            "cenaHw": {
                "size0p": 39.99,
                "size2p": 35.99,
                "size3p": 33.99,
                "size4p": 31.99,
                "size5p": 29.99
            },
            "cenaNoHw": {
                "size0p": 35.99,
                "size2p": 31.99,
                "size3p": 29.99,
                "size4p": 27.99,
                "size5p": 25.99
            }
        }, {
            "id": 8,
            "type": "Happy ",
            "size": "XXL",
            "class": "XXL",
            "pSize": 1,
            "cenaHw": {
                "size0p": 54.99,
                "size2p": 50.99,
                "size3p": 48.99,
                "size4p": 46.99,
                "size5p": 44.99
            },
            "cenaNoHw": {
                "size0p": 46.99,
                "size2p": 42.99,
                "size3p": 40.99,
                "size4p": 38.99,
                "size5p": 36.99
            }
        }, {
            "id": 9,
            "type": "Happy ",
            "size": "Profi",
            "class": "Profi",
            "pSize": 1,
            "cenaHw": {
                "size0p": 69.99,
                "size2p": 63.99,
                "size3p": 61.99,
                "size4p": 59.99,
                "size5p": 57.99
            },
            "cenaNoHw": {
                "size0p": 59.99,
                "size2p": 53.99,
                "size3p": 51.99,
                "size4p": 49.99,
                "size5p": 47.99
            }
        },
        {
            "id": 10,
            "type": "Happy XL",
            "size": " DÁTA PRE MLADÝCH",
            "class": "DaTAPRE MLADyCH",
            "pSize": 1,
            "cenaHw": {
                "size0p": 19.99,
                "size2p": 19.99,
                "size3p": 19.99,
                "size4p": 19.99,
                "size5p": 19.99
            },
            "cenaNoHw": {
                "size0p": 19.99,
                "size2p": 19.99,
                "size3p": 19.99,
                "size4p": 19.99,
                "size5p": 19.99
            }
        }];
        // TODO create obj for all fix products
        vm.fixnesluzby = [{
            "name": ["Internet", "Televízia", "Pevná linka"],
            "type": ["internet", "tv", "pevnalinka"],
            "balik": "M",
            "size": "M",
            "pSize": 1,
            "cena": 10.0
        }, {
            "name": ["Internet", "Televízia", "Pevná linka"],
            "type": ["internet", "tv", "pevnalinka"],
            "balik": "L",
            "size": "L",
            "pSize": 1,
            "cena": 15.0
        }, {
            "name": ["Internet", "Televízia", "Pevná linka"],
            "type": ["internet", "tv", "pevnalinka"],
            "balik": "XL",
            "size": "X",
            "pSize": 1,
            "cena": 20.0
        }];

        // Kombinacie
        vm.fixneKombi = [{
            "L": 15,
            "LL": 24,
            "LLL": 29,
            "LLM": 27,
            "LLX": 32,
            "LM": 21,
            "LMM": 25,
            "LMX": 30,
            "LX": 28,
            "LXX": 35,
            "M": 10,
            "MM": 18,
            "MMM": 23,
            "MMX": 28,
            "MX": 25,
            "MXX": 33,
            "X": 20,
            "XX": 32,
            "XXX": 38
        }];
        // D&D
        vm.vybrate1 = [];
        vm.vybrate2 = [];
        vm.vybrate2_1 = [];
        vm.vybrate2_2 = [];
        vm.vybrate2_3 = [];
        vm.vybrate2_4 = [];
        vm.vybrate3 = [];
        vm.vybrate3_1 = [];
        vm.vybrate3_2 = [];
        vm.vybrate3_3 = [];
        vm.step1 = [];
        //filter programs with p coeficient
        vm.step1 = $filter('filter')(vm.pausaly, { pSize: 1 });
        //console.log(vm.step1);
        //define containers for d&drop
        var containerVyber = document.querySelector('#containerVyber'),
            containerPausaly = document.querySelector('#containerPausaly'),
            containerVyber2 = document.querySelector('#containerVyber2'),
            containerVyber2_2 = document.querySelector('#containerVyber2_2'),
            containerVyber2_3 = document.querySelector('#containerVyber2_3'),
            containerVyber2_4 = document.querySelector('#containerVyber2_4'),
            containerPausaly2 = document.querySelector('#containerPausaly2'),
            containerVyber3 = document.querySelector('#containerVyber3'),
            containerVyber3_2 = document.querySelector('#containerVyber3_2'),
            containerVyber3_3 = document.querySelector('#containerVyber3_3'),
            containerFixneSluzby = document.querySelector('#containerFixneSluzby');

        dragularService.cleanEnviroment();

        //step 1
        dragularService([containerVyber], {
            containersModel: [vm.vybrate1],
            removeOnSpill: true,
            revertOnSpill: true,
            accepts: acceptsVyber,
            scope: $scope
        });

        dragularService([containerPausaly], {
            containersModel: [vm.step1],
            copy: true,
            accepts: accepts,
            scope: $scope
        });

        // step 2
        dragularService([containerVyber2], {
            containersModel: [vm.vybrate2_1],
            removeOnSpill: true,
            revertOnSpill: true,
            direction: 'vertical',
            scope: $scope,
            accepts: acceptsVyber2,
        });
        dragularService([containerVyber2_2], {
            containersModel: [vm.vybrate2_2],
            removeOnSpill: true,
            revertOnSpill: true,
            scope: $scope,
            accepts: acceptsVyber2_2,
        });
        dragularService([containerVyber2_3], {
            containersModel: [vm.vybrate2_3],
            removeOnSpill: true,
            revertOnSpill: true,
            direction: 'vertical',
            scope: $scope,
            accepts: acceptsVyber2_3,
        });
        dragularService([containerVyber2_4], {
            containersModel: [vm.vybrate2_4],
            removeOnSpill: true,
            revertOnSpill: true,
            scope: $scope,
            accepts: acceptsVyber2_4,
        });
        dragularService([containerPausaly2], {
            containersModel: [vm.pausaly],
            copy: true,
            accepts: accepts,
            scope: $scope,
            moves: moves,
        });

        // step 3
        dragularService([containerVyber3], {
            containersModel: [vm.vybrate3_1],
            removeOnSpill: true,
            revertOnSpill: true,
            scope: $scope,
            accepts: acceptsVyber3,
        });
        dragularService([containerVyber3_2], {
            containersModel: [vm.vybrate3_2],
            removeOnSpill: true,
            revertOnSpill: true,
            scope: $scope,
            accepts: acceptsVyber3_2,
        });
        dragularService([containerVyber3_3], {
            containersModel: [vm.vybrate3_3],
            removeOnSpill: true,
            revertOnSpill: true,
            scope: $scope,
            accepts: acceptsVyber3_3
        });
        dragularService([containerFixneSluzby], {
            containersModel: [vm.fixnesluzby],
            copy: true,
            dontCopyModel: false,
            accepts: accepts,
            scope: $scope
        });


        // fn : allow drag to container from ... || source === containerVyber
        function acceptsVyber(el, target, source) {
            if (vm.vybrate1.length < 2 && (source === containerPausaly || source === target)) {
                return true;
            }
        }
        // drop container 2
        function acceptsVyber2(el, target, source) {
            if (vm.vybrate2_1.length < 2 && (source === containerPausaly2 || source === target)) {
                return true;
            }
        }

        function acceptsVyber2_2(el, target, source) {
            if (vm.vybrate2_2.length < 2 && (source === containerPausaly2 || source === target)) {
                return true;
            }
        }

        function acceptsVyber2_3(el, target, source) {
            if (vm.vybrate2_3.length < 2 && (source === containerPausaly2 || source === target)) {
                return true;
            }
        }

        function acceptsVyber2_4(el, target, source) {
            if (vm.vybrate2_4.length < 2 && (source === containerPausaly2 || source === target)) {
                return true;
            }
        }
        // drop container 3
        function acceptsVyber3(el, target, source) {
            if (vm.vybrate3_1.length < 2 && (source === containerFixneSluzby || source === target)) {
                return true;
            }
        }

        function acceptsVyber3_2(el, target, source) {
            if (vm.vybrate3_2.length < 2 && (source === containerFixneSluzby || source === target)) {
                return true;
            }
        }

        function acceptsVyber3_3(el, target, source) {
            if (vm.vybrate3_3.length < 2 && (source === containerFixneSluzby || source === target)) {
                return true;
            }
        }

        function accepts(el, target, source) {
            return false
                // if (source === target) {
                //     return true;
                // }
        }

        // EVENTY 
        // $scope.$on('dragulardrag', function(e, el) {
        //     e.stopPropagation();
        //     el.className = el.className.replace(' ex-moved', '');
        // });
        // $scope.$on('dragulardrop', function(e, el) {
        //     e.stopPropagation();
        //     $timeout(function() {
        //         el.className += ' ex-moved';
        //     }, 0);
        // });
        // $scope.$on('dragularcloned', myFn('cloned'));
        // $scope.$on('dragulardrag', myFn('drag'));
        // $scope.$on('dragularcancel', myFn('cancel'));
        // $scope.$on('dragulardrop', myFn('drop'));
        // $scope.$on('dragularremove', myFn('remove'));
        // $scope.$on('dragulardragend', myFn('dragend'));
        // $scope.$on('dragularshadow', myFn('shadow'));
        // $timeout(function() {
            //    // console.log('\n2\n', el, '\n3\n', elIndex, '\n4\n', targetModel, '\n5\n', dropIndex);
            //     if (targetModel.length === 1) {
            //         targetModel.splice(dropIndex, 1);
            //console.log('\n2\n', el, '\n3\n', elIndex, '\n4\n', targetModel, '\n5\n', dropIndex);
            //     }
            // }, 10);
        // function myFn(eventName) {
        //     return function() {
        //         console.log(eventName, arguments);
        //     };
        // }

        //fn to enable replace item in vyber array
        $scope.$on('dragulardrop', function(e, el, targetContainer, sourceContainer, conModel, elIndex, targetModel, dropIndex) {
            e.stopPropagation();
            if (dropIndex >= 1) {
                targetModel.splice(0, 1);
            } else if (targetModel.length === 2) {
                targetModel.splice(1, 2);
            }
        });

        function moves(el, source, handle, sibling) {
            if (el.className == 'ex-moved') {
                return false
            } else {
                return true; // elements are always draggable by default
            }
        };
        // end:EVENTY 

        // Remove item from selection
        vm.removeItem1 = function removeItem(event, key) {
            vm.vybrate1.splice(key, 1);
        };

        vm.removeItem2 = function removeItem(key) {
            vm.vybrate2_1.splice(key, 1);
        };
        vm.removeItem2_2 = function removeItem(key) {
            vm.vybrate2_2.splice(key, 1);
        };
        vm.removeItem2_3 = function removeItem(key) {
            vm.vybrate2_3.splice(key, 1);
        };
        vm.removeItem2_4 = function removeItem(key) {
            vm.vybrate2_4.splice(key, 1);
        };

        vm.removeItem3 = function removeItem(key) {
            vm.vybrate3_1.splice(key, 1);
        };
        vm.removeItem3_2 = function removeItem(key) {
            vm.vybrate3_2.splice(key, 1);
        };
        vm.removeItem3_3 = function removeItem(key) {
            vm.vybrate3_3.splice(key, 1);
        };

        // Count products coeficient P
        vm.spoluP = function spoluP(vybrate1, vybrate2, vybrate3) {
            var spolu = 0,
                spolu2 = 0,
                spolu3 = 0,
                spoluPartial = 0;
            //count products for every selection
            for (var count = 0; count < vybrate1.length; count++) {
                spolu += vybrate1[count].pSize;
            }

            for (var count = 0; count < vybrate2.length; count++) {
                spolu2 += vybrate2[count].pSize;
            }

            for (var count = 0; count < vybrate3.length; count++) {
                spolu3 += vybrate3[count].pSize;
            }

            //add products over 3 only if fixne is selected
            if ((spolu + spolu2) >= 3 && spolu3 === 0) {
                spoluPartial = 3;
            } else if ((spolu + spolu2) >= 3 && spolu3 === 1) {
                spoluPartial = 4;
            } else if ((spolu + spolu2) >= 3 && spolu3 === 2) {
                spoluPartial = 5;
            } else if ((spolu + spolu2) >= 3 && spolu3 === 3) {
                spoluPartial = 5;
            } else {
                spoluPartial = spolu + spolu2 + spolu3;
            }

            vm.pocetP = spoluPartial;
            //console.log('pocetP', vm.pocetP);
            return vm.pocetP;
        };

        // Price calculation   
        vm.priceCalc = function priceCalc(vybrate1, vybrate2_1, vybrate2_2, vybrate2_3, vybrate2_4, vybrate3_1, vybrate3_2, vybrate3_3) {
            var spoluHwPlnaCena = 0,
                spoluNoHwPlnaCena = 0,
                spoluHw = 0,
                spoluNoHw = 0,
                rozdielHw = 0;
            var spoluHwPlnaCena2 = 0,
                spoluNoHwPlnaCena2 = 0,
                spoluHw2 = 0,
                spoluNoHw2 = 0;
            var fixneSpolu = 0,
                fixneSpoluPlnaCena = 0,
                kombItem = "",
                kombiArr = [];
            vm.sizeP = "";
            vm.finalNoHwBezZlavy = 0;
            vm.finalNoHwPoZlave = 0;
            vm.usporaNoHw = 0;
            vm.mobilneSpoluPlnaCena = 0;
            vm.mobilneSpoluPoZlave = 0;
            vm.fixneSpoluPlnaCena = 0;
            vm.fixneSpoluPoZlave = 0;
            vm.mobilneSpoluPlnaCenaHw = 0;
            vm.mobilneSpoluPoZlaveHw = 0;
            //concat partial step selection 
            vm.vybrate2 = vybrate2_1.concat(vybrate2_2, vybrate2_3, vybrate2_4);
            var vybrate3 = vybrate3_1.concat(vybrate3_2, vybrate3_3);
            // console.log("1: ", vybrate1, "2: ", vm.vybrate2, "3: ", vybrate3);
            // count number of P
            //select product P coeficient vm.pocetP
            switch (vm.spoluP(vybrate1, vm.vybrate2, vybrate3)) {
                case 0:
                    vm.sizeP = "size0p"
                    break;
                case 1:
                    vm.sizeP = "size0p"
                    break;
                case 2:
                    vm.sizeP = "size2p"
                    break;
                case 3:
                    vm.sizeP = "size3p"
                    break;
                case 4:
                    vm.sizeP = "size4p"
                    break;
                case 5:
                    vm.sizeP = "size5p"
                    break;
                default:
                    vm.sizeP = "size5p"
            }
            // spocitaj 1 vyber
            for (var count = 0; count < vybrate1.length; count++) {
                spoluHwPlnaCena += vybrate1[count].cenaHw["size0p"];
                spoluHw += vybrate1[count].cenaHw[vm.sizeP];
                spoluNoHwPlnaCena += vybrate1[count].cenaNoHw["size0p"];
                spoluNoHw += vybrate1[count].cenaNoHw[vm.sizeP];
            };
            // spocitaj 2 vyber
            for (var count = 0; count < vm.vybrate2.length; count++) {
                spoluHwPlnaCena2 += vm.vybrate2[count].cenaHw["size0p"];
                spoluHw2 += vm.vybrate2[count].cenaHw[vm.sizeP];
                spoluNoHwPlnaCena2 += vm.vybrate2[count].cenaNoHw["size0p"];
                spoluNoHw2 += vm.vybrate2[count].cenaNoHw[vm.sizeP];
            }
            // najdi cenu fixnej kombinacie
            for (var count = 0; count < vybrate3.length; count++) {
                fixneSpoluPlnaCena += vybrate3[count].cena;
                //kombinacia 
                // pomocna fn skontroluj vybrate1 sluzby  usporiadaj podla abecedy a push fixSize to a spoj vyslednu kombinaciu
                kombiArr.push(vybrate3[count].size);
                kombItem = kombiArr.sort().join("");
                fixneSpolu = vm.fixneKombi[0][kombItem];
                // console.log("komb cena", vm.fixneKombi[0][kombItem], " plnaCena",fixneSpoluPlnaCena);
            }
            //console.log('kombinacia', kombItem, ' cena: ', fixneSpolu);

            // HW
            vm.finalBezZlavyHw = (spoluHwPlnaCena + spoluHwPlnaCena2 + fixneSpoluPlnaCena).toFixed(2);
            vm.finalHwPrice = (spoluHw + spoluHw2 + fixneSpolu).toFixed(2);
            vm.usporaHw = (vm.finalBezZlavyHw - vm.finalHwPrice).toFixed(2);

            //noHW
            vm.finalNoHwBezZlavy = (spoluNoHwPlnaCena + spoluNoHwPlnaCena2 + fixneSpoluPlnaCena).toFixed(2);
            vm.finalNoHwPoZlave = (spoluNoHw + spoluNoHw2 + fixneSpolu).toFixed(2);
            vm.usporaNoHw = (vm.finalNoHwBezZlavy - vm.finalNoHwPoZlave).toFixed(2);

            //vypis noHw
            vm.mobilneSpoluPlnaCena = (spoluNoHwPlnaCena + spoluNoHwPlnaCena2).toFixed(2);
            vm.mobilneSpoluPoZlave = (spoluNoHw + spoluNoHw2).toFixed(2);
            //vypis hw
            vm.mobilneSpoluPlnaCenaHw = (spoluHwPlnaCena + spoluHwPlnaCena2).toFixed(2);
            vm.mobilneSpoluPoZlaveHw = (spoluHw + spoluHw2).toFixed(2);

            // Fixne sluzby
            vm.fixneSpoluPlnaCena = fixneSpoluPlnaCena;
            vm.fixneSpoluPoZlave = fixneSpolu;

            if (vm.checkHw.isHw) {
                return vm.finalHwPrice;
            } else {
                return vm.finalNoHwPoZlave;
            }
        };

        //test fn for dev only
        vm.check = function(item) {
            if (vm.vybrate3_1.length == 1) {
                // console.log("true", item);
                angular.element('#name').addClass("alllign");
                return true;
            } else {
                // console.log('false');
                return false;
            }
        };

        // alternative replace item selection by click
        vm.toggleStep1 = function(index, key) {
            vm.vybrate1.splice(key, 1);
            vm.vybrate1.push(vm.step1[index]);
        };

        vm.toggleStep2 = function(index, key, leght21, leght22, leght23, leght24) {
            //console.log('e', leght21,leght22,leght23,leght24);
            if (leght21 == 0) {
                vm.vybrate2_1.push(vm.pausaly[index]);
            }

            if (leght21 == 1 && leght22 == 0) {
                vm.vybrate2_2.push(vm.pausaly[index]);
            }

            if (leght21 == 1 && leght22 == 1 && leght23 == 0) {
                vm.vybrate2_3.push(vm.pausaly[index]);
            }

            if (leght21 == 1 && leght22 == 1 && leght23 == 1 && leght24 == 0) {
                vm.vybrate2_4.push(vm.pausaly[index]);
            }

        };

        vm.toggleStep3 = function(index, key, leght31, leght32, leght33) {
            //console.log('l', leght31,leght32,leght33);
            if (leght31 == 0) {
                vm.vybrate3_1.push(vm.fixnesluzby[index]);
            }

            if (leght31 == 1 && leght32 == 0) {
                vm.vybrate3_2.push(vm.fixnesluzby[index]);
            }

            if (leght31 == 1 && leght32 == 1 && leght33 == 0) {
                vm.vybrate3_3.push(vm.fixnesluzby[index]);
            }
        };

        // fn to check if property value 1
        vm.pValueCheck = function(array) {
            var tmpArr = $filter('filter')(array, { pSize: 1 });
            // console.log('array', array,'tmp',tmpArr.length);
            return tmpArr.length > 0;
        };

        //helper for hw noHW swich
        vm.checkHw = {
            isHw: false,
        };

        //create class name
        vm.createClass = function(item) {
            var classTmp = item.type + item.class;
            var str = classTmp.replace(/\s+/g, '-');
            return str.toLowerCase();
        }
    }]);

    //filter change dot to comma
    app.filter('myCurrency', ['$filter', function($filter) {
        return function(input) {
            input = parseFloat(input);
            input = input.toFixed(input % 1 === 0 ? 0 : 2);
            return input.toString().replace(/\./g, ',') + ' €';
            // return input.toString().replace(/\./g, ',');
        };
    }]);

    // prepinaci switch
    angular.module('uiSwitch', [])
        .directive('switch', function() {
            return {
                restrict: 'AE',
                replace: true,
                transclude: true,
                template: function(element, attrs) {
                    var html = '';
                    html += '<span';
                    html += ' class="switch' + (attrs.class ? ' ' + attrs.class : '') + '"';
                    html += attrs.ngModel ? ' ng-click="' + attrs.disabled + ' ? ' + attrs.ngModel + ' : ' + attrs.ngModel + '=!' + attrs.ngModel + (attrs.ngChange ? '; ' + attrs.ngChange + '()"' : '"') : '';
                    html += ' ng-class="{ checked:' + attrs.ngModel + ', disabled:' + attrs.disabled + ' }"';
                    html += '>';
                    html += '<small></small>';
                    html += '<input type="checkbox"';
                    html += attrs.id ? ' id="' + attrs.id + '"' : '';
                    html += attrs.name ? ' name="' + attrs.name + '"' : '';
                    html += attrs.ngModel ? ' ng-model="' + attrs.ngModel + '"' : '';
                    html += ' style="display:none" />';
                    html += '<span class="switch-text">'; /*adding new container for switch text*/
                    html += attrs.on ? '<span class="on">' + attrs.on + '</span>' : ''; /*switch text on value set by user in directive html markup*/
                    html += attrs.off ? '<span class="off">' + attrs.off + '</span>' : ' '; /*switch text off value set by user in directive html markup*/
                    html += '</span>';
                    return html;
                }
            }
        });


}());
