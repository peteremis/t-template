$(function() {

    $(".scroll-to").click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top - ($("#nav-sticky").outerHeight())
                }, 1000);
                return false;
            }
        }
    });

    $(".sidebar").sticky({
        topSpacing: 10,
        bottomSpacing: 426,
        // widthFromWrapper: '#sideBar'
    });
     $('.vypocet').qtip({
        content: {
            text: 'Výpočet platí pre členov skupiny Magenta 1, v ktorej sú 2 paušály M alebo vyššie a v ktorej zatiaľ nie sú priradené žiadne fixné služby.'
        },
        position: {
            my: 'top center',
            at: 'bottom center',
        }
    });
    $('.happy-xs-mini').qtip({
        content: {
            text: '- Neobmedzené volania na 3 čísla v SR pri aktivácii nového paušálu.<br> - Volania do všetkých sietí v SR a EÚ: 30 minút.<br> - Neobmedzené prichádzajúce hovory v EÚ.<br> - Cena SMS/MMS v SR: 0,10. €<br> - Internet v mobile: 20 MB v SR a EÚ.'
        },
        position: {
            my: 'top center',
            at: 'bottom center',
        }
    });
    $('.happy-xs').qtip({
        content: {
            text: '- Neobmedzené večerné a víkendové volania do siete Telekom.<br> - Volania do všetkých sietí v SR a EÚ: 50 minút.<br> - Neobmedzené prichádzajúce hovory v EÚ.<br> - Cena SMS/MMS v SR: 0,10 €.<br> - Internet v mobile: 200 MB v SR a EÚ.'
        },
        position: {
            my: 'top center',
            at: 'bottom center',
        }
    });

    $('.happy-xldatapre-mladych').qtip({
        content: {
            text: '- Neobmedzené volania do siete Telekom.<br> - Volania do ostatných sietí v SR a EÚ: 100 minút.<br> - Neobmedzené prichádzajúce hovory v EÚ.<br> - Cena SMS/MMS v SR: 0,10 €<br> - Internet v mobile: 5 GB v SR a EÚ.'
        },
        position: {
            my: 'top center',
            at: 'bottom center',
        }
    });
    
    $('.happy-s').qtip({
        content: {
            text: '- Neobmedzené volania do siete Telekom.<br> - Volania do ostatných sietí v SR a EÚ: 100 minút<br> - Neobmedzené prichádzajúce hovory v EÚ.<br> - Cena SMS/MMS v SR: 0,10 €.<br> - Internet v mobile: 400 MB v SR a EÚ.'
        },
        position: {
            my: 'top center',
            at: 'bottom center',
        }
    });

    $('.happy-m').qtip({
        content: {
            text: '- Neobmedzené volania do siete Telekom.<br> - Volania do ostatných sietí v SR a EÚ: 150 minút.<br> - Neobmedzené prichádzajúce hovory v EÚ.<br> - SMS/MMS neobmedzené v SR.<br> - Internet v mobile: 1 GB v SR a EÚ.'
        },
        position: {
            my: 'top center',
            at: 'bottom center',
        }
    });
    $('.happy-l').qtip({
        content: {
            text: '- Neobmedzené volania do siete Telekom.<br> - Volania do ostatných sietí v SR a EÚ: 250 minút.<br> - Neobmedzené prichádzajúce hovory v EÚ.<br> - SMS/MMS neobmedzené v SR.<br> - Internet v mobile: 3 GB v SR a EÚ.'
        },
        position: {
            my: 'top center',
            at: 'bottom center',
        }
    });
    $('.happy-xl-vol').qtip({
        content: {
            text: '- Neobmedzené volania v SR, z EÚ do SR a EÚ.<br> - Volania zo SR do EÚ: 1000 minút.<br> - Neobmedzené prichádzajúce hovory v EÚ.<br> - Cena SMS/MMS v SR: 0,10 €.<br> - Internet v mobile: 1 GB v SR a EÚ.'
        },
        position: {
            my: 'top center',
            at: 'bottom center',
        }
    });
    $('.happy-xl').qtip({
        content: {
            text: '- Neobmedzené volania v SR, z EÚ do SR a EÚ.<br> - Volania zo SR do EÚ: 1000 minút.<br> - Neobmedzené prichádzajúce hovory v EÚ.<br> - SMS/MMS neobmedzené v SR.<br> - Internet v mobile: 6 GB v SR a EÚ.'
        },
        position: {
            my: 'top center',
            at: 'bottom center',
        }
    });
    $('.happy-xxl').qtip({
        content: {
            text: '- Neobmedzené volania v SR, z EÚ do SR a EÚ.<br> - Volania zo SR do EÚ: 1000 minút.<br> - Neobmedzené prichádzajúce hovory v EÚ.<br> - SMS/MMS neobmedzené v SR a EÚ.<br> - Internet v mobile: 9 GB v SR a EÚ.'
        },
        position: {
            my: 'top center',
            at: 'bottom center',
        }
    });
    $('.happy-profi').qtip({
        content: {
            text: '- Neobmedzené volania v SR, z EÚ do SR a EÚ.<br> - Volania zo SR do EÚ: 1000 minút.<br> - Volania v zóne Profi: 200 minút.<br> - Neobmedzené prichádzajúce hovory v EÚ.<br> - SMS/MMS neobmedzené v SR, EÚ a zóne Profi.<br> - Internet v mobile: 16 GB v SR a EÚ + 500 MB v zóne Profi.'
        },
        position: {
            my: 'top center',
            at: 'bottom center',
        }
    });

    $('.step-2-info').qtip({
        content: {
            text: 'Pridať si môžete aj nižší paušál ako M alebo EASY, zľavy však nezískate.'
        },
        position: {
            my: 'top center',
            at: 'bottom center',
        }
    });

    $('.info-neobmedzene-volania').qtip({
        content: {
            text: 'Benefit NEOBMEDZENÉ VOLANIA medzi členmi Magenta1 skupiny sa týka všetkých HAPPY paušálov ako aj Easy karty.'
        },
        position: {
            my: 'top center',
            at: 'bottom center',
        }
    });

    $('.info-neobmedzene-sms').qtip({
        content: {
            text: 'Benefit NEOBMEDZENÉ SMS a MMS medzi členmi Magenta1 skupiny sa týka všetkých HAPPY paušálov ako aj Easy karty.'
        },
        position: {
            my: 'top center',
            at: 'bottom center',
        }
    });

    $('.info-neobmedzene-data').qtip({
        content: {
            text: 'Benefit NEOBMEDZENÉ DÁTA sa týka všetkých HAPPY paušálov ako aj Easy karty.<br> Platí do 31.12.2016. <br> Benefit je potrebné bezplatne aktivovať na stránke <br> https://www.telekom.sk/magenta1-skupina'
        },
        position: {
            my: 'top center',
            at: 'bottom center',
        }
    });


    $('.info-neobmedzene-cloud').qtip({
        content: {
            text: 'Benefit MAGENTA CLOUD, 5 GB úložného priestoru získa každý člen Magenta1 skupiny. Magenta Cloud je dostupný na www.magentajedna.sk.'
        },
        position: {
            my: 'top center',
            at: 'bottom center',
        }
    });

    $('.info-neobmedzene-pojdeto').qtip({
        content: {
            text: 'Benefit Pôjde to pomocník získava každý člen Magenta1 skupiny.'
        },
        position: {
            my: 'top center',
            at: 'bottom center',
        }
    });

});
