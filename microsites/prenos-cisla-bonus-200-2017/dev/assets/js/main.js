$(function() {
    /* ::: Scroll To ::: */
    $(".scroll-to").click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top - ($("#nav-sticky").outerHeight())
                }, 1000);
                return false;
            }
        }
    });
    /* ::: Sticky Nav-ext ::: */
    $("#nav-sticky").sticky({
        topSpacing: 0,
        widthFromWrapper: true
    });

    var contentSections = $("[id*='sec-']"),
        secondaryNav = $("#nav-sticky");

    function updateSecondaryNavigation() {
        contentSections.each(function() {
            var actual = $(this),
                actualHeight = actual.height() + parseInt(actual.css('paddingTop').replace('px', '')) + parseInt(actual.css('paddingBottom').replace('px', '')),
                actualAnchor = secondaryNav.find('a[href="#' + actual.attr('id') + '"]');

            if ((actual.offset().top - secondaryNav.outerHeight() <= $(window).scrollTop()) && (actual.offset().top + actualHeight - secondaryNav.outerHeight() > $(window).scrollTop())) {
                actualAnchor.addClass('active');
            } else {
                actualAnchor.removeClass('active');
            }

        });
    }

    updateSecondaryNavigation();
    $(window).scroll(function(event) {
        updateSecondaryNavigation();
    });

    $('.roaming-info').qtip({
        content: 'Všetky členské štáty EÚ a Island, Nórsko, Lichtenštajnsko',
        position: {
            corner: {
                target: 'leftMiddle',
                tooltip: 'topMiddle'
            }
        }
    }).bind('click', function(event) {
        event.preventDefault();
        return false;
    });

    $('.benefit1').qtip({
        content: 'Bonus na paušál vám uplatníme formou zľavy z mesačného poplatku vo výške 5 € počas 20 mesiacov.',
        position: { corner: { target: "leftMiddle", tooltip: "topMiddle" } }
    }).bind('click', function(event) {
        event.preventDefault();
        return false;
    });

    $('.benefit2').qtip({
        content: 'Bonus na paušál vám uplatníme formou zľavy z mesačného poplatku vo výške 4 € počas 20 mesiacov.',
        position: { corner: { target: "leftMiddle", tooltip: "topMiddle" } }
    }).bind('click', function(event) {
        event.preventDefault();
        return false;
    });

    $('.benefit3').qtip({
        content: 'Bonus na paušál vám uplatníme formou zľavy z mesačného poplatku vo výške 3 € počas 20 mesiacov.',
        position: { corner: { target: "leftMiddle", tooltip: "topMiddle" } }
    }).bind('click', function(event) {
        event.preventDefault();
        return false;
    });

});


$(document).ready(function() {

    if (window.matchMedia('(max-width: 480px)').matches) {
        $("#info-data-navyse").attr("src", "/documents/10179/5125920/icon_info_grey.png");
    } else {}

    if (getParameterByName('data-navyse')) {
        scrollTo('#line-navyse', 50);
    }

    function getParameterByName(name) {
        var found = false;
        if (window.location.href.indexOf(name) > -1) {
            found = true;
        }

        return found;
    }



    $('.tooltip').qtip({
        content: {
            text: 'Vybrané paušály sú:<br> Happy M, L, XL volania, XL, XXL a Profi'
        },
        position: {
            my: 'bottom center',
            at: 'top center'
        }
    });

    $('.happy_xs-info').qtip({
        content: 'Počas pracovných dní v čase od 19.00 hod. do 7.00 hod. Soboty, nedele a štátne sviatky počas celých 24 hodín.',
        position: {
            corner: {
                target: 'leftMiddle',
                tooltip: 'topMiddle'
            }
        }
    }).bind('click', function(event) {
        event.preventDefault();
        return false;
    });

    $('.c-row-1 .c-tile-head img').qtip({
        content: 'Vybraný smartfón ako darček získate k Happy paušálu so Sony Xperia M2.',
        style: { 'font-size': 12 },
        position: {
            corner: {
                target: 'leftMiddle',
                tooltip: 'topMiddle'
            }
        }
    });

    $('#info-data100').qtip({
        content: 'Balíček nie je dostupný pre<br>Happy XS mini.',
        style: { 'font-size': 12 },
        position: {
            corner: {
                target: 'leftMiddle',
                tooltip: 'topMiddle'
            }
        }
    });

    $('#info-data500').qtip({
        content: 'Balíček nie je dostupný pre<br>Happy XS mini.',
        style: { 'font-size': 12 },
        position: {
            corner: {
                target: 'leftMiddle',
                tooltip: 'topMiddle'
            }
        }
    });

    $('#info-data1000').qtip({
        content: 'Balíček nie je dostupný pre<br>Happy XS mini, XS a S.',
        style: { 'font-size': 12 },
        position: {
            corner: {
                target: 'leftMiddle',
                tooltip: 'topMiddle'
            }
        }
    });

    $('#info-data-navyse').qtip({
        content: 'Platí pre paušál Happy M a vyšší.',
        position: {
            corner: {
                target: 'leftMiddle',
                tooltip: 'topMiddle'
            }
        }
    });

    $('#info-zdielanie').qtip({
        content: 'Službu Zdieľanie dát z paušálu na tablete či druhom telefóne dostanete ku každému paušálu Happy L a vyššiemu na 24 mesiacov zadarmo.',
        position: {
            corner: {
                target: 'leftMiddle',
                tooltip: 'topMiddle'
            }
        }
    });

    $('.happy-desc .row-5-head img').qtip({
        content: 'S následnými neobmedzenými<br>e-mailmi a surfovaním',
        position: {
            corner: {
                target: 'leftMiddle',
                tooltip: 'topMiddle'
            }
        }
    });


    $('.c-row-5 .box-header img').qtip({
        content: 'Platí pre odchádzajúce roamingové hovory z EÚ do SR za mesačný poplatok 2 €.',
        position: {
            corner: {
                target: 'leftMiddle',
                tooltip: 'topMiddle'
            }
        }
    });


    $('.c-row-6 .c-tile-1 .c-tile-head img').qtip({
        content: 'Zľava sa uplatňuje 20 mesiacov v náležite rozrátanej sume.<br>Platí len pre paušály Happy.',
        position: {
            corner: {
                target: 'leftMiddle',
                tooltip: 'topMiddle'
            }
        }
    });

    $('.happy-desc .row-8-head img').qtip({
        content: 'Jednorazová zľava, ktorú získate iba pri kúpe paušálu na našom webe.<br>Konkrétna výška zľavy bude vypočítaná pri nákupe.',
        position: {
            corner: {
                target: 'leftMiddle',
                tooltip: 'topMiddle'
            }
        }
    });

    $('#info-powerbank').qtip({
        content: 'Externá batéria v kľúčenke:<br>– zabudovaný nabíjací a dátový kábel<br>– kapacita batérie: 1000 mAh<br>– kompaktné rozmery: 55 x 30 x 20 mm<br>– hmotnosť: len 38 gramov',
        position: {
            corner: {
                target: 'leftMiddle',
                tooltip: 'topMiddle'
            }
        }
    }).bind('click', function(event) {
        event.preventDefault();
        return false;
    });

    $('#info-balik-volania, #info-balik-minuty, #info-balik-sms').qtip({
        content: 'Balík aktivujete na webe počas kúpy Happy paušálu.',
        position: {
            corner: {
                target: 'leftMiddle',
                tooltip: 'topMiddle'
            }
        }
    }).bind('click', function(event) {
        event.preventDefault();
        return false;
    });

    $('#zona-svet').qtip({
        //show: 'click',
        hide: 'unfocus',
        content: '<span class="bold">Zóna svet pre program služieb Profi:</span><br>Aljaška, Rakúsko, Belgicko, Bulharsko, Kanada, Chorvátsko, Cyprus, Česká republika, Dánsko, Estónsko, Fínsko, Francúzsko, Nemecko, Grécko, Maďarsko, Island, Írsko, Taliansko, Lotyšsko, Lichtenštajnsko, Litva, Luxembursko, Malta, Holandsko, Nórsko , Poľsko, Portugalsko, Portoriko, Rumunsko, Slovinsko, Španielsko, Švédsko, Veľká Británia, Americké Panenské ostrovy, USA, Vatikán, Austrália, Kostarika, Guam, Hong Kong, Čína, India, Indonézia, Izrael, Japonsko, Malajzia, Mexiko, Monako, Peru, Kórejská republika, Ruská federácia, Singapur, Južná Afrika, Švajčiarsko, Thajsko, Ukrajina, Uzbekistan, Venezuela, Albánska, Angola, Argentína, Bahamy, Bahrajn, Bangladéš, Bielorusko, Bosna a Hercegovina, Brunej, Burundi, Kambodža, Kolumbia,  Egypt, Francúzska Guyana, Gabon, Ghana, Guadeloupe, Guatemala, Chile , Irán, Etiópska federatívna demokratická republika, Kuvajt, Laos, Libanon, Macao, Macedónsko, Martinik, Maurícius, Moldavsko, Mongolsko, Čierna Hora, Namíbia, Holandské Antily, Niger, Nigéria, Severné Mariány, Pakistan, Palestínske územia, Panama, Filipíny , Réunion, San Maríno, Saudská Arábia, Srbsko, Srí Lanka, Sýria, Taiwan, Tadžikistan, Turecko, Spojené arabské emiráty, Vietnam, Jemen, Zambia',
        position: {
            my: 'middle right',
            at: 'bottom left'
        }
    });

    // ***  slider  ****
    $("#program_slider").slick({
        slidesToShow: 2,
        slidesToScroll: 1,
        centerMode: false,
        infinite: false,
        swipeToSlide: true,
        swipe: true,
        arrows: true,
        dots: false,
        autoplay: false,
        initialSlide: 2,
        autoplaySpeed: 5000,
        touchMove: false,
        draggable: false,
        customPaging: function(slider, i) {
            return '<span class="slide-dot">&nbsp;</span>';
        },
        responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
                infinite: false,
                arrows: true,
                dots: true
            }
        }, {
            breakpoint: 768,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: true,
                infinite: false,
                dots: true
            }
        }]
    });
    // $("#zakladne_slider").slick({
    //         slidesToShow: 1,
    //         slidesToScroll: 1,
    //         centerMode: true,
    //         infinite: false,
    //         swipeToSlide: true,
    //         swipe: true,
    //         arrows: true,
    //         dots: false,
    //         autoplay: false,
    //         // initialSlide: 2,
    //         autoplaySpeed: 5000,
    //         customPaging: function(slider, i) {
    //             return '<span class="slide-dot">&nbsp;</span>';
    //         },
    //         responsive: [{
    //             breakpoint: 1024,
    //             settings: {
    //                 slidesToShow: 2,
    //                 slidesToScroll: 1,
    //                 arrows: false,
    //                 dots: true
    //             }
    //         }, {
    //             breakpoint: 768,
    //             settings: {
    //                 slidesToShow: 1,
    //                 slidesToScroll: 1,
    //                 arrows: false,
    //                 infinite: true,
    //                 dots: true
    //             }
    //         }]
    //     });
    $("#prenarocnych_slider").slick({
        slidesToShow: 2,
        slidesToScroll: 1,
        centerMode: false,
        infinite: false,
        swipeToSlide: true,
        swipe: true,
        arrows: true,
        dots: true,
        autoplay: false,
        draggable: false,
        // initialSlide: 1,
        autoplaySpeed: 5000,
        customPaging: function(slider, i) {
            return '<span class="slide-dot">&nbsp;</span>';
        },
        responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
                infinite: false,
                arrows: true,
                dots: true
            }
        }, {
            breakpoint: 768,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: true,
                infinite: false,
                dots: true
            }
        }]
    });
});
