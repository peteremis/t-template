$(document).ready(function () {


    var dataUrl = "/assets/js/data.json";
    var id_device = window.location.search.slice(1); //na zaklade url zobrazuje data, napr. ?motox4

    if (!id_device.length) {
        if (window.location.hostname == "localhost") {
            alert('Nie je zadane id_device');
        } else {
            window.location.replace("https://www.telekom.sk/internet/mobilny-internet");
        }
    } else {
        var start = id_device.indexOf('-');
        if (start > -1) {
            id_device = id_device.substr(start + 1);
        }

        $.ajax({
            url: dataUrl,
            cache: false,
            //async: false,
            type: "get",
            crossDomain: true,
            success: function (device) {
                showDetail(device, id_device);
            },
            error: function () {
                alert("Chyba pri posielani ... skus este raz!");
            }
        });
    }

    //$('.sec-det').fadeIn(1500).removeClass('hidden');


    function showDetail(device, id_device) {
        var existItem = false;
        for (var a = 0; a < device.length; a++) {
            if (device[a].id == id_device) { //na zaklade url zobrazuje data, napr. ?motox4
                existItem = true;
                var detail = device[a].params;

                var details = '';
                for (var b = 0; b < detail.length; b++) {
                    details +=
                        '<tr class="params">' +
                        '<td>' +
                        '<div>' +
                        detail[b].desc[0] +
                        '</div>' +
                        '</td>' +
                        '<td>' +
                        detail[b].desc[1] +
                        '</td>' +
                        '</tr>';
                }
                $(details).appendTo(".paramsTable");

                //images in gallery
                var thumbnails = $('.thumbnails a');
                $('.full__holder .full').attr('src', device[a].image[0].img[0]);


                for (var c = 1; c <= thumbnails.length; c++) {
                    $('.img' + c + ' ' + 'a').attr('data-full', device[a].image[c - 1].img[0]);
                    $('.img' + c + ' ' + 'a img').attr('src', device[a].image[c - 1].img[1]);
                }
                // $(".img1 a").attr('data-full', device[a].image[0].img1[0]);
                // $(".img1 a img").attr('src', device[a].image[0].img1[1]);
                // $(".img2 a").attr('data-full', device[a].image[1].img2[0]);
                // $(".img2 a img").attr('src', device[a].image[1].img2[1]);

                //badge
                if (device[a].badgeShow == true) {
                    $('.ec-gallery').show()
                } else {
                    $('.ec-gallery').hide()
                }
                $('.ec-gallery').attr('src', device[a].badgeImg);

                //device title
                var deviceName = device[a].name;
                $(".bpis__title").text(deviceName);

                //box mobilny internet S
                if (device[a].internetSshow == true) {
                    $('.box-S').show()
                } else {
                    $('.box-S').hide()
                }
                //box mobilny internet M
                if (device[a].internetMshow == true) {
                    $('.box-M').show()
                } else {
                    $('.box-M').hide()
                }

                //box mobilny internet L
                if (device[a].internetLshow == true) {
                    $('.togleArrow').show()
                } else {
                    $('.togleArrow').hide()
                }

                // mobilny internet S
                $(".internets").text(device[a].internetS[0].internet);
                $(".zlava").text(device[a].internetS[1].zlava);
                $(".doplatok").text(device[a].internetS[2].doplatok);
                $(".jednorazovo").text(device[a].internetS[3].jednorazovo);

                // mobilny internet M
                $(".internetM").text(device[a].internetM[0].internet);
                $(".zlavaM").text(device[a].internetM[1].zlava);
                $(".doplatokM").text(device[a].internetM[2].doplatok);
                $(".jednorazovoM").text(device[a].internetM[3].jednorazovo);

                //toggle arrow text - mobilny internet L
                $(".internetL").text(device[a].internetL[0].internet);
                $(".zlavaL").text(device[a].internetL[1].zlava);
                $(".doplatokL").text(device[a].internetL[2].doplatok);
                $(".jednorazovoL").text(device[a].internetL[3].jednorazovo);

                // btn
                $(".nakupbtn").attr('href', device[a].btn);

                // popis produktu
                $(".tit1").html(device[a].description[0].txt1[0]);
                $(".tit2").html(device[a].description[0].txt1[1]);
                $(".featimg1").attr('src', device[a].description[0].txt1[2]);

                $(".tit3").html(device[a].description[1].txt2[0]);
                $(".tit4").html(device[a].description[1].txt2[1]);
                $(".featimg2").attr('src', device[a].description[1].txt2[2]);

                $(".tit5").html(device[a].description[2].txt3[0]);
                $(".tit6").html(device[a].description[2].txt3[1]);
                $(".featimg3").attr('src', device[a].description[2].txt3[2]);

                $(".tit7").html(device[a].description[3].txt4[0]);
                $(".tit8").html(device[a].description[3].txt4[1]);

            }
        }

        if (!existItem) {
            if (window.location.hostname == "localhost") {
                alert('Not found id_device');
            } else {
                window.location.replace("https://www.telekom.sk/internet/mobilny-internet");
            }
        }

    }



});