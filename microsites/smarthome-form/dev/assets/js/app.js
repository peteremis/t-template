$(document).ready(function () {

  var smarthomeFormName = $('.smarthome-form').parents('.formComposition');

  var smarthomeError = $('#smarthome-error');

  var smarthomeHeader = $('#smarthome-header');
  var smarthomeFinal = $('#smarthome-final');

  var smarthomeTable = $('#smarthome-table');
  var smarthomeButton = smarthomeFormName.find('.finishFormButton');

  var formEmailInput = '#formFieldId140057';
  var formEmailValue = 'jasso@rainside.sk';
  // var formEmailValue = getParameterByName('email');

  var formSubjectInput = '#formFieldId140056';
  var formSubjectValue = 'Smarthome - objednávka';

  var formTextInput = '#formFieldId140058';

  // var formTextValue = "<table border='0'><tr><td colspan='2'>Objednávka SmartHome</td></tr><tr><td colspan='2'>Produkty:</td></tr><tr class='product'><td>Dverový senzor</td><td>1ks</td></tr><tr><td colspan='2'>&nbsp;</td></tr><tr class='monthly'><td>Mesačne:</td><td>1.99</td></tr><tr class='activationFee'><td>Jednorazovo:</td><td>0.00</td></tr><tr class='activationFee'><td>Aktivačný poplatok:</td><td>0.00</td></tr><tr class='paymentMethod'><td>Spôsob platby:</td><td>mesačne</td></tr><tr><td colspan='2'>&nbsp;</td></tr><tr class='name'><td>Meno a priezvisko:</td><td>Test Test</td></tr><tr class='streetNo'><td>Ulica a číslo:</td><td>Test</td></tr><tr class='city'><td>Mesto/obec:</td><td>Test</td></tr><tr class='psc'><td>PSČ:</td><td>82102</td></tr><tr class='phone'><td>Telefónne číslo:</td><td>0903903903</td></tr><tr class='email'><td>E-mail:</td><td>jasso@rainside.sk</td></tr><tr><td colspan='2'>&nbsp;</td></tr><tr class='idNum'><td>Čislo OP:</td><td></td></tr><tr class='birthdayNum'><td>Rodné číslo:</td><td></td></tr><tr class='textInfo'><td>Poznámka:</td><td></td></tr><tr class='shippment'><td colspan='2'>Adresa pre doručenie:</td></tr><tr class='shippment_streetNo'><td>Ulica a číslo:</td><td>Test</td></tr><tr class='shippment_city'><td>Mesto/obec:</td><td>Test</td></tr><tr class='shippment_psc'><td>PSČ:</td><td>11111</td></tr></table>";
  var formTextValue = localStorage.getItem('lsbData');

  var fromLsb = getParameterByName('fromLSB');
  var formId = getParameterByName('formId');
  var formHash = getParameterByName('urlHash');

  // var smarthomeId = getParameterByName('order');
  // var smarthomeId = 1;

  //schovame originalny formular
  smarthomeFormName.hide();

  if (fromLsb !== null) {
    //zobrazime thank you page
    smarthomeFinal.show();

  } else {
    if (formId !== null && formHash !== null) {
      //ide o potvrdenie LSB objednavky

      //zobrazime header a tabulku
      smarthomeHeader.show();
      smarthomeTable.show();

      //schovame buttony pre ukoncenie
      $('#finishButtonShoppingCart, #finishButtonShoppingCart').hide();

      //prevezmeme obsah input boxu (tabulku)
      var smarthomeTableContent = $(formTextInput).find('input').val();

      //presnunieme obsah input boxu do tabulky
      smarthomeTable.append(smarthomeTableContent);

      //zobrazime button pre potvrdenie objednavky
      smarthomeTable.append(smarthomeButton);
    } else {
      //je to nova objednavka

      if (formTextValue !== null) {
        //zobrazime header a tabulku
        smarthomeHeader.show();
        smarthomeTable.show();

        //naplnime hodnoty do input boxu
        formChange(formEmailInput, formEmailValue);
        formChange(formSubjectInput, formSubjectValue);
        formChange(formTextInput, formTextValue);

        //zobrazime tabulky s objednavkou
        smarthomeTable.append(formTextValue);
      } else {

        //chyba cislo objednavky, zobrazime chybu
        smarthomeError.show();
        window.location.href = "https://www.telekom.sk/smarthome";
      }

    }
  }

  function formChange(inputName, value) {
    $(inputName).find('input').val(value).blur().end();
  }

  // popup close with scroll to section
  $(document).on('click', '.agentSubmitButton', function(e) {
    e.preventDefault();
    localStorage.removeItem('lsbData');
  });

});

function getParameterByName(name, url) {
  if (!url) url = window.location.href;
  name = name.replace(/[\[\]]/g, "\\$&");
  var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
    results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return '';
  return decodeURIComponent(results[2].replace(/\+/g, " "));
}