$(function() {
   $('.info-tooltip').qtip({
       content: {
           text: 'PRE MAGIO TV:<br/>' +
'MSTV ID kód je 8-miestne číslo v tvare <b>1Mxxxxxx</b> alebo <b>1Exxxxxx</b>. Nájdete ho na faktúre za služby Telekom.<br/>'+
'PRE MAGIO TV CEZ SATELIT:<br/>'+
'Pokiaľ máte službu Magio SAT, tento kód nájdete na faktúre pod označením TV ID kód. Je to 8-miestne číslo v tvare <b>1Mxxxxxx</b> alebo <b>1Exxxxxx</b>'
       },
       position: {
           my: 'left center',
           at: 'right center'
       }
   }); 
});