$(document).ready(function () {

    var magentaCalc = {
        config: {
            menu: $('#magenta-menu'),
            childClass: $('.child'),
            notInterestedClass: $('.not-interested'),
            disabledElem: $('.disabled'),
            buttonBuy: $('.button-buy'),
            lightbox: $('.lightbox-cover'),
            priceHolder: $('.priceHolder'),
            formHolder: $('.formFieldRender .pageContent .dynamicFormSubFields'),
            form: $('.formComposition'),
            form__internet: $('.form__internet'),
            form__tv: $('.form__tv'),
            form__pL: $('.form__pl'),
            form__mp: $('.form__mp'),
            form__1p: $('.form__1p'),
            form__2p: $('.form__2p'),
            form__3p: $('.form__3p'),
            fi__parent: $('.form__internet').closest('.fieldTypeINPUT'),
            ftv__parent: $('.form__tv').closest('.fieldTypeINPUT'),
            fpL__parent: $('.form__pl').closest('.fieldTypeINPUT'),
            fmp__parent: $('.form__mp').closest('.fieldTypeINPUT'),
            f1p__parent: $('.form__1p').closest('.fieldTypeINPUT'),
            f2p__parent: $('.form__2p').closest('.fieldTypeINPUT'),
            f3p__parent: $('.form__3p').closest('.fieldTypeINPUT'),
            masterListItems: $('.master'),
            mLiCount: $('.master').length - 1,
            myMobProdCount: 0,
            productHolders: $('.productHolder'),
            productHoldersCount: $('.productHolder').length,
            requiredProductsForMagenta: 3,
            clickThrough: false,
            package1: 'o',
            package2: 'o',
            package3: 'o',
            prevPrice: 0,
            price: 0,
            qtipShowed: false
        },
        fixedProdPrice: {
            'ooo': 0,
            'Moo': 10,
            'oMo': 10,
            'ooM': 10,
            'Loo': 15,
            'oLo': 15,
            'ooL': 15,
            'Xoo': 20,
            'oXo': 20,
            'ooX': 20,
            'MMo': 18,
            'MoM': 18,
            'oMM': 18,
            'MMM': 38,
            'LLo': 24,
            'LoL': 24,
            'oLL': 24,
            'LLL': 38,
            'XXo': 32,
            'oXX': 32,
            'XoX': 32,
            'XXX': 38,
            'MLo': 21,
            'MoL': 21,
            'oML': 21,
            'MXo': 25,
            'MoX': 25,
            'oMX': 25,
            'LMo': 21,
            'LoM': 21,
            'oLM': 21,
            'LXo': 28,
            'LoX': 28,
            'oLX': 28,
            'XMo': 25,
            'XoM': 25,
            'oXM': 25,
            'XLo': 28,
            'XoL': 28,
            'oXL': 28,
            'MML': 25,
            'MLM': 25,
            'LMM': 25,
            'MLL': 27,
            'LML': 27,
            'LLM': 27,
            'MMX': 28,
            'MXM': 28,
            'XMM': 28,
            'MXX': 33,
            'XMX': 33,
            'XXM': 33,
            'MLX': 30,
            'MXL': 30,
            'LMX': 30,
            'LXM': 30,
            'XML': 30,
            'XLM': 30,
            'XXL': 35,
            'XLX': 35,
            'LXX': 35
        },
        mobProd1Price: {
            'o': 0,
            'a': 21.99,
            'b': 19.99,
            'c': 19.99,
            'd': 29.99,
            'e': 39.99,
            'f': 52.99
        },
        mobProd2Price: {
            'o': {
                price: 0,
                count: 0
            },
            'a': {
                price: 27.9,
                count: 1
            },
            'b': {
                price: 27.9,
                count: 1
            },
            'c': {
                price: 27.9,
                count: 1
            },
            'd': {
                price: 35.99,
                count: 1
            },
            'e': {
                price: 46.99,
                count: 1
            },
            'f': {
                price: 59.99,
                count: 1
            },
        },
        init: function () {
            this.toggleLightBox();
            this.setAllInputFields();
            this.disableBuyButton();
            this.menuInit();
            this.qtipInit();
            this.disableClick($(this.config.disabledElem));
            this.bindEvents();
            this.hideForm();
            this.hideFormFields();
            this.addCloseBtn();
        },
        menuInit: function () {
            $(this.config.menu).on('click', function (event) {

                event.preventDefault();

                if ($(event.target).parent().hasClass('master')) {
                    $('#magenta-menu ul').addClass('hidden');
                }

                $(event.target).parent().children('ul').toggleClass('hidden');
                $(event.target).parent().children('ul').addClass('is_open');
                $('#magenta-menu').toggleClass('is_closed');

                if ($(event.target).parent().children().length <= 1) {
                    $('#magenta-menu .is_open').toggleClass('hidden');
                    $('#magenta-menu ul').removeClass('is_open');
                    $('#magenta-menu').addClass('is_closed');
                }

            });


            $(document).on('click', function (event) {
                if (!$(event.target).closest('#magenta-menu').length) {
                    if (!$('#magenta-menu').hasClass('is_closed')) {
                        $('#magenta-menu .is_open').toggleClass('hidden');
                        $('#magenta-menu ul').removeClass('is_open');
                        $('#magenta-menu').removeClass('is_closed');
                    }
                }
            });

        },
        qtipInit: function () {
            $('#help-1').qtip({
                content: {
                    text: 'Na úvod vyberte, či máte Magio internet, chcete nový alebo nemáte záujem o internet.<br /><a class="button qtip-button close-btn">Rozumiem</a>'
                },
                events: {
                    render: function (event, api) {
                        $('.close-btn', this).click(function () {
                            api.hide();
                            magentaCalc.toggleLightBox();
                        });
                    }
                },
                show: {
                    event: false,
                    when: false,
                    ready: true
                },
                hide: {
                    event: 'click'
                },
                position: {
                    my: 'top center',
                    at: 'bottom center'
                }
            });
            $('#help-2').qtip({
                content: {
                    text: 'Výberom pevných služieb ste získali nárok na jeden paušál za výhodnejšiu cenu.<br /><a class="button qtip-button close-btn">Rozumiem</a>'
                },
                events: {
                    render: function (event, api) {
                        magentaCalc.toggleLightBox();
                        $('.close-btn', this).click(function () {
                            api.hide();
                            magentaCalc.toggleLightBox();
                        });
                    }
                },
                show: {
                    event: false,
                    when: false
                },
                hide: false,
                position: {
                    my: 'top center',
                    at: 'bottom center'
                }
            });
            $('#help-3').qtip({
                content: {
                    text: 'Máte nárok na 3 paušály do vašej rodiny za výhodnejšiu cenu.<br /><a class="button qtip-button close-btn">Pokračovať vo výbere</a><br /><a href="#" class="close-btn qtip-custom-blue">Nemám záujem</a>'
                },
                events: {
                    render: function (event, api) {
                        magentaCalc.toggleLightBox();
                        $('.close-btn', this).click(function () {
                            api.hide();
                            magentaCalc.toggleLightBox();
                        });
                    }
                },
                show: {
                    event: false,
                    when: false
                },
                hide: false,
                position: {
                    my: 'top center',
                    at: 'bottom center'
                }
            });
        },
        toggleQtip: function (id) {
            var div = $('#' + id);
            if (div.data('visible')) {
                div.qtip('hide');
                div.data('visible', false);
            } else {
                div.qtip('show');
                div.data('visible', true);
            }
        },
        setAllInputFields: function () {
            this.config.fi__parent.find('input').val('Nemám záujem');
            this.config.ftv__parent.find('input').val('Nemám záujem');
            this.config.fpL__parent.find('input').val('Nemám záujem');
            this.config.fmp__parent.find('input').val('Nemám záujem');
            this.config.f1p__parent.find('input').val('Nemám záujem');
            this.config.f2p__parent.find('input').val('Nemám záujem');
            this.config.f3p__parent.find('input').val('Nemám záujem');
        },
        hideForm: function () {
            this.config.form.addClass('hidden');
        },
        showForm: function () {
            this.config.form.removeClass('hidden');
        },
        addCloseBtn: function () {
            this.config.formHolder.append('<div class="dynamicFormItemBox"><a class="formCloseBtn close-icon" href="#"><i class="fa fa-times" aria-hidden="true"></i></a></div>');
            this.config.formHolder.append('<div class="dynamicFormItemBox"><a class="formCloseBtn" href="#">Zavrieť</a></div>');
        },
        toggleLightBox: function () {
            this.config.lightbox.toggleClass('hidden');
        },
        hideFormFields: function () {
            magentaCalc.config.fi__parent.addClass('pos-top');
            magentaCalc.config.ftv__parent.addClass('pos-top');
            magentaCalc.config.fpL__parent.addClass('pos-top');
            magentaCalc.config.fmp__parent.addClass('pos-top');
            magentaCalc.config.f1p__parent.addClass('pos-top');
            magentaCalc.config.f2p__parent.addClass('pos-top');
            magentaCalc.config.f3p__parent.addClass('pos-top');
        },
        disableBuyButton: function () {
            this.config.buttonBuy.addClass('disabled');
        },
        enableBuyButton: function () {
            this.config.buttonBuy.removeClass('disabled');
        },
        bindEvents: function () {
            $('.child').on('click', function (event) {
                magentaCalc.cacheTheClickEvents(event);
            });
            $('.not-interested').on('click', function (event) {
                magentaCalc.cacheTheClickEvents(event);
            });
            this.config.buttonBuy.on('click', function () {
                if ($(this).hasClass('disabled')) {
                    event.preventDefault();
                } else {
                    magentaCalc.config.form.toggleClass('hidden');
                    magentaCalc.toggleLightBox();
                }
            });
        },
        cacheTheClickEvents: function (event) {
            var chosenProgram = null,
                chosenType = null,
                chosenProductHolder = $(event.target).closest('.master').find('.productHolder'),
                typeCode = $(event.target).closest('.master').data('type'),
                order = $(event.target).closest('.master').data('order'),
                master = $(event.target).closest('.master'),
                prodPackage = $(event.target).closest('.package').data('package'),
                prodVariant = $(event.target).closest('.product-variant').data('variant'),
                isChildClass = $(event.target).is('.child') ? true : false,
                isNotIntestedClass = $(event.target).is('.not-interested') ? true : false;

            isChildClass ? chosenProgram = $(event.target).text() : chosenProgram = null;
            isChildClass ? chosenType = $(event.target).closest('.parent').find('.type').text() : chosenType = $(event.target).text();

            chosenProductHolder.text(isChildClass ? chosenProgram : chosenType);
            chosenProductHolder.removeClass('hidden');
            master.data('choosen', isChildClass ? 1 : 0);
            chosenProductHolder.data('product', isChildClass ? 1 : 0);

            if (this.choosenFixProductsCount() > 0) {
                this.enableBuyButton();
            } else {
                this.disableBuyButton();
            }

            if (order <= 2) {
                chosenProductHolder.data('variant', prodVariant);
            }

            if (!this.config.clickThrough) {
                if (order < 2) {
                    this.enableNext(order);
                } else if (order == 2) {
                    this.config.clickThrough = true;
                    if (this.choosenProductsCount() > 0) {
                        this.enableNext(2);
                        this.toggleQtip('help-2');
                    }
                }
            } else if (this.config.clickThrough) {
                if (this.choosenFixProductsCount() < 1) {
                    this.disableMobProd();
                    $(this.config.productHolders[3]).text('Nemám záujem');
                    $(this.config.productHolders[4]).text('Nemám záujem');
                    this.clearInputField(this.config.form__1p.find('input'));
                    this.clearInputField(this.config.form__2p.find('input'));
                    this.clearInputField(this.config.form__3p.find('input'));
                    this.config.buttonBuy.text('MÁM ZÁUJEM');
                    this.config.buttonBuy.closest('li').removeClass('double-line');
                    chosenProductHolder.data('product', 0);
                    $(this.config.productHolders[3]).data('product', 0);
                    $(this.config.productHolders[4]).data('product', 0);
                    this.config.myMobProdCount = 0;
                    this.config.package1 = 'o';
                    this.config.package2 = 'o';
                    this.config.package3 = 'o';
                    $(this.config.productHolders[3]).data('variant', 'o');
                } else if (this.choosenProductsCount() > 0) {
                    this.enableFirstMobProd();
                }
                if (order == 3) {
                    $(this.config.productHolders[3]).data('variant', prodVariant);
                    if ((chosenProductHolder.data('product') == 1)) {
                        this.enableNext(order);
                        if (!this.config.qtipShowed) {
                            this.toggleQtip('help-3');
                            this.config.qtipShowed = true;
                        }
                    } else if ((chosenProductHolder.data('product') == 0)) {
                        this.disableLastMobProd();
                        this.clearInputField(this.config.form__1p.find('input'));
                        this.clearInputField(this.config.form__2p.find('input'));
                        this.clearInputField(this.config.form__3p.find('input'));
                        $(this.config.productHolders[4]).text('Nemám záujem');
                        this.config.buttonBuy.text('MÁM ZÁUJEM');
                        this.config.buttonBuy.closest('li').removeClass('double-line');
                        chosenProductHolder.data('product', 0);
                        $(this.config.productHolders[4]).data('product', 0);
                        this.config.myMobProdCount = 0;
                        this.config.package1 = 'o';
                        this.config.package2 = 'o';
                        this.config.package3 = 'o';
                    }
                }

                if (order == 4 && prodPackage == 3) {
                    this.clearInputField(this.config.form__1p.find('input'));
                    this.clearInputField(this.config.form__2p.find('input'));
                    this.clearInputField(this.config.form__3p.find('input'));
                }

                if (this.choosenMobProductsCount() > 0) {
                    this.config.buttonBuy.html('CHCEM MAGENTU 1');
                    this.config.buttonBuy.closest('li').addClass('double-line');
                } else {
                    this.config.buttonBuy.text('MÁM ZÁUJEM');
                    this.config.buttonBuy.closest('li').removeClass('double-line');

                }

                if (order == 4) {
                    switch (prodPackage) {
                        case 0:
                            this.config.package1 = prodVariant;
                            break;
                        case 1:
                            this.config.package2 = prodVariant;
                            break;
                        case 2:
                            this.config.package3 = prodVariant;
                            break;
                        case 3:
                            this.config.package1 = 'o';
                            this.config.package2 = 'o';
                            this.config.package3 = 'o';
                            break;
                    }

                    this.config.myMobProdCount = this.mobProd2Price[this.config.package1].count + this.mobProd2Price[this.config.package2].count + this.mobProd2Price[this.config.package3].count;
                    var myMobProdText = '';

                    switch (this.config.myMobProdCount) {
                        case 0:
                            myMobProdText = '0 paušál';
                            break;
                        case 1:
                            myMobProdText = '1 paušál';
                            break;
                        case 2:
                            myMobProdText = '2 paušály';
                            break;
                        case 3:
                            myMobProdText = '3 paušály';
                            break;
                    }

                    chosenProductHolder.text(myMobProdText);
                    chosenProductHolder.data('variant', this.config.package1 + this.config.package2 + this.config.package3);
                }
            }

            this.switchStatement(typeCode, chosenType, chosenProgram, prodPackage);
            this.calculatePrice(order, prodVariant, chosenProductHolder.data('variant'));

        },
        switchStatement: function (typeCode, chosenType, chosenProgram, prodPackage) {
            var result = '';
            if (chosenType && chosenProgram) {
                result = chosenType + ' - ' + chosenProgram;
            } else if (!chosenType && chosenProgram) {
                result = chosenProgram;
            } else if (chosenType && !chosenProgram) {
                result = chosenType;
            }

            switch (typeCode) {
                case 0:
                    magentaCalc.config.form__internet.find('input').val(result);
                    break;
                case 1:
                    magentaCalc.config.form__tv.find('input').val(result);
                    break;
                case 2:
                    magentaCalc.config.form__pL.find('input').val(result);
                    break;
                case 3:
                    magentaCalc.config.form__mp.find('input').val(result);
                    break;
                case 4:
                    switch (prodPackage) {
                        case 0:
                            magentaCalc.config.form__1p.find('input').val(result);
                            break;
                        case 1:
                            magentaCalc.config.form__2p.find('input').val(result);
                            break;
                        case 2:
                            magentaCalc.config.form__3p.find('input').val(result);
                            break;
                    }
                    break;
            }
        },
        choosenProductsCount: function () {
            var count = 0;
            for (var i = 0; i < magentaCalc.config.productHoldersCount; i++) {
                count += $(magentaCalc.config.productHolders[i]).data('product');
            }
            return count
        },
        choosenFixProductsCount: function () {
            var count = 0;
            for (var i = 0; i <= 2; i++) {
                count += $(magentaCalc.config.productHolders[i]).data('product');
            }
            return count
        },
        choosenMobProductsCount: function () {
            var count = 0;
            for (var i = 3; i <= 4; i++) {
                count += $(magentaCalc.config.productHolders[i]).data('product');
            }
            return count
        },
        choosenCount: function () {
            var count = 0;
            for (var i = 0; i < (magentaCalc.config.requiredProductsForMagenta - 1); i++) {
                count += $(magentaCalc.config.productHolders[i]).data('choosen');
            }
            return count
        },
        disableClick: function ($parentLI) {
            $parentLI.on('mouseenter.smartmenus mouseleave.smartmenus mousedown.smartmenus focus.smartmenus blur.smartmenus click.smartmenus touchend.smartmenus', 'a', function (e) {
                e.stopPropagation();
                e.preventDefault();
            });
        },
        disableMobProd: function () {
            for (var i = 3; i <= 4; i++) {
                this.disableClick($(this.config.masterListItems[i]));
                $(this.config.masterListItems[i]).addClass('disabled');
            }
        },
        disableLastMobProd: function () {
            this.disableClick($(this.config.masterListItems[4]));
            $(this.config.masterListItems[4]).addClass('disabled');
        },
        enableClick: function ($parentLI) {
            $parentLI.off('mouseenter.smartmenus mouseleave.smartmenus mousedown.smartmenus focus.smartmenus blur.smartmenus click.smartmenus touchend.smartmenus');
        },
        enableFirstMobProd: function () {
            this.enableClick($(this.config.masterListItems[3]));
            $(this.config.masterListItems[3]).removeClass('disabled');
        },
        isChoosen: function (master) {
            (master.data('choosen') == 1) ? 'true' : 'false';
        },
        enableNext: function (order) {
            if (order < magentaCalc.config.mLiCount) {
                $(magentaCalc.config.masterListItems[order + 1]).removeClass('disabled');
                this.enableClick($(magentaCalc.config.masterListItems[order + 1]));
            }
        },
        clearInputField: function (inputObject) {
            inputObject.val('Nemám záujem');
        },
        getProductVariantsSum: function () {
            var result = '';
            for (var i = 0; i <= 2; i++) {
                result += $(this.config.productHolders[i]).data('variant');
            }
            return result;
        },
        calculatePrice: function (order, prodVariant, holderVariant) {

            this.config.price = eval('this.fixedProdPrice[' + '"' + this.getProductVariantsSum() + '"' + ']');

            this.config.price += this.mobProd1Price[$(this.config.productHolders[3]).data('variant')];

            this.config.prevPrice = this.config.price;
            if (order == 4) {
                this.config.price = this.config.prevPrice + this.mobProd2Price[holderVariant[0]].price + this.mobProd2Price[holderVariant[1]].price + this.mobProd2Price[holderVariant[2]].price;
            }

            this.updatePrice(this.config.price);
        },
        updatePrice: function (price) {
            price = price.toFixed(2);
            this.config.priceHolder.text(price + ' €');
        }

    };

    magentaCalc.init();

    $('.formCloseBtn').on('click', function () {
        magentaCalc.hideForm();
        magentaCalc.toggleLightBox();
    });
});
