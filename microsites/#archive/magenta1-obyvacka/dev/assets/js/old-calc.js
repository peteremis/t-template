var global = new Object();
var global1 = new Object();
var ios = false;
global.tv = "o";
global.pc = "o";
global.pl = "o";

global1.tv = "o";
global1.pc = "o";
global1.pl = "o";

global.M = 10;
global.L = 15;
global.XL = 20;
global.o = 0;
global.data = null;

global.text = {
    "tv": [{
        "M": "<span class='text bold magenta'>Magio televízia M</span><br/><span class='text'>TV kanály: <b>Až 61 TV kanálov</b></span>",
        "L": "<span class='text bold magenta'>Magio televízia L</span><br/><span class='text'>TV kanály: <b>Až 88 TV kanálov</b></span>",
        "XL": "<span class='text bold magenta'>Magio televízia XL</span><br/><span class='text'>TV kanály: <b>Až 113 TV kanálov</b></span>"
    }
    ],
    "pc": [{
        "M": "<span class='text bold magenta'>Magio internet M</span><br/><span class='text'>Max. prístupová rýchlosť - Optika: <b>6/0,5 Mbs</b><br/>Max. prístupová rýchlosť - VDSL: <b>4/0,5 Mbs</b><br/>Max. prístupová rýchlosť - DSL: <b>2/0,5 Mbs</b><br/>Mesačný objem dát: <b>Neobmedzený</b><br/>Deezer zdarma: <b>Nie</b></span>",
        "L": "<span class='text bold magenta'>Magio internet L</span><br/><span class='text'>Max. prístupová rýchlosť - Optika: <b>40/4 Mbs</b><br/>Max. prístupová rýchlosť - VDSL: <b>20/2 Mbs</b><br/>Max. prístupová rýchlosť - DSL: <b>5/0,5 Mbs</b><br/>Mesačný objem dát: <b>Neobmedzený</b><br/>Deezer zdarma: <b>Nie</b></span>",
        "XL": "<span class='text bold magenta'>Magio internet XL</span><br/><span class='text'>Max. prístupová rýchlosť - Optika: <b>300/10 Mbs</b><br/>Max. prístupová rýchlosť - VDSL: <b>50/5 Mbs</b><br/>Max. prístupová rýchlosť - DSL: <b>10/1 Mbs</b><br/>Mesačný objem dát: <b>Neobmedzený</b><br/>Deezer zdarma: <b>Áno</b></span>"
    }
    ],
    "pl": [{
        "M": "<span class='text bold magenta'>Doma Happy M</span><br/><span class='text'>Neobmedzené volania: <b>Pevné siete, 19:00 až 7:00 + víkendy a sviatky</b><br/>Predplatené minúty: <b>100 voľných minút do pevných a mobilných sietí</b><br/>Cena po prevolaní predplatených minút v SR: <b>0,13 € / min</b><br/>Jednorázový poplatok za telefón Gigaset A120: <b>9 €</b></span>",
        "L": "<span class='text bold magenta'>Doma Happy L</span><br/><span class='text'>Neobmedzené volania: <b>Pevné siete</b><br/>Predplatené minúty: <b>400 voľných minút do mobilných sietí</b><br/>Cena po prevolaní predplatených minút v SR: <b>0,09 € / min</b><br/>Jednorázový poplatok za telefón Gigaset A120: <b>1 €</b></span>",
        "XL": "<span class='text bold magenta'>Doma Happy XL</span><br/><span class='text'>Neobmedzené volania: <b>Pevné aj mobilné siete v SR</b><br/>Jednorázový poplatok za telefón Gigaset A120: <b>1 €</b></span>"
    }
    ],
}


function loadData2() {
    data =
    {
        "oMo": [{"cena": "10", "par2": "10"}],
        "oMM": [{"cena": "18", "par2": ""}],
        "oML": [{"cena": "21", "par2": ""}],
        "oMXL": [{"cena": "25", "par2": ""}],
        "oLo": [{"cena": "15", "par2": "10"}],
        "oLM": [{"cena": "21", "par2": ""}],
        "oLL": [{"cena": "25", "par2": ""}],
        "oLXL": [{"cena": "28", "par2": ""}],
        "oXLo": [{"cena": "20", "par2": "10"}],
        "oXLM": [{"cena": "25", "par2": ""}],
        "oXLL": [{"cena": "28", "par2": ""}],
        "oXLXL": [{"cena": "32", "par2": ""}],
        "Moo": [{"cena": "10", "par2": "10"}],
        "MoM": [{"cena": "18", "par2": ""}],
        "MoL": [{"cena": "21", "par2": ""}],
        "MoXL": [{"cena": "25", "par2": ""}],
        "MMo": [{"cena": "18", "par2": ""}],
        "MMM": [{"cena": "23", "par2": ""}],
        "MML": [{"cena": "25", "par2": ""}],
        "MMXL": [{"cena": "28", "par2": ""}],
        "MLo": [{"cena": "21", "par2": ""}],
        "MLM": [{"cena": "25", "par2": ""}],
        "MLL": [{"cena": "27", "par2": ""}],
        "MLXL": [{"cena": "30", "par2": ""}],
        "MXLo": [{"cena": "25", "par2": ""}],
        "MXLM": [{"cena": "28", "par2": ""}],
        "MXLL": [{"cena": "30", "par2": ""}],
        "MXLXL": [{"cena": "33", "par2": ""}],
        "Loo": [{"cena": "15", "par2": "10"}],
        "LoM": [{"cena": "21", "par2": ""}],
        "LoL": [{"cena": "24", "par2": ""}],
        "LoXL": [{"cena": "28", "par2": ""}],
        "LMo": [{"cena": "21", "par2": ""}],
        "LMM": [{"cena": "25", "par2": ""}],
        "LML": [{"cena": "27", "par2": ""}],
        "LMXL": [{"cena": "30", "par2": ""}],
        "LLo": [{"cena": "24", "par2": ""}],
        "LLM": [{"cena": "27", "par2": ""}],
        "LLL": [{"cena": "29", "par2": ""}],
        "LLXL": [{"cena": "32", "par2": ""}],
        "LXLo": [{"cena": "28", "par2": ""}],
        "LXLM": [{"cena": "30", "par2": ""}],
        "LXLL": [{"cena": "35", "par2": ""}],
        "LXLXL": [{"cena": "35", "par2": ""}],
        "XLoo": [{"cena": "20", "par2": "10"}],
        "XLoM": [{"cena": "25", "par2": ""}],
        "XLoL": [{"cena": "28", "par2": ""}],
        "XLoXL": [{"cena": "32", "par2": ""}],
        "XLMo": [{"cena": "25", "par2": ""}],
        "XLMM": [{"cena": "28", "par2": ""}],
        "XLML": [{"cena": "30", "par2": ""}],
        "XLMXL": [{"cena": "33", "par2": ""}],
        "XLLo": [{"cena": "28", "par2": ""}],
        "XLLM": [{"cena": "30", "par2": ""}],
        "XLLL": [{"cena": "32", "par2": ""}],
        "XLLXL": [{"cena": "35", "par2": ""}],
        "XLXLo": [{"cena": "32", "par2": ""}],
        "XLXLM": [{"cena": "33", "par2": ""}],
        "XLXLL": [{"cena": "35", "par2": ""}],
        "XLXLXL": [{"cena": "38", "par2": ""}],
        "Moo": [{"cena": "10", "par2": ""}],
        "Loo": [{"cena": "15", "par2": ""}],
        "XLoo": [{"cena": "20", "par2": ""}],
        "oMo": [{"cena": "10", "par2": ""}],
        "oLo": [{"cena": "15", "par2": ""}],
        "oXLo": [{"cena": "20", "par2": ""}],
        "ooM": [{"cena": "10", "par2": ""}],
        "ooL": [{"cena": "15", "par2": ""}],
        "ooXL": [{"cena": "20", "par2": ""}],
        "ooo": [{"cena": "0", "par2": ""}]
    }

    //data = JSON.parse(data);

    return data;
}

$(function () {


    if (navigator.userAgent.match(/(iPad.*|iPhone.*|iPod.*);.*CPU.*OS 7_\d/i)) {
        // je to ios
    }
    $(".ikon").bind("click", function () {

        $("#error").hide();

        var current = $(this).attr("alt");
        if ($(this).parent().hasClass("selected")) {
            eval("global." + $(this).attr("id").split("-")[0] + "='o'");
            $(this).attr("src", mainDir + "/img/icons/" + $(this).attr("id").toLowerCase() + ".png");
            $(this).parent().removeClass("selected");
            evalCena();
            return;
        }
        $(".ikon").each(function (index) {
            if ($(this).attr("alt") == current) {
                $(this).attr("src", mainDir + "/img/icons/" + $(this).attr("id").toLowerCase() + ".png");
                $(this).parent().removeClass("selected");


            }
        });


        $(this).parent().addClass("selected");
        $(this).attr("src", mainDir + "/img/icons/" + $(this).attr("id").toLowerCase() + "_1.png");
        //var cena = eval global.data.oLo[0].cena
        eval("global." + $(this).attr("id").split("-")[0] + "='" + $(this).attr("id").split("-")[1] + "'");
        evalCena();
    })

    setTimeout('resize()', 1000);
    //loadData();

    global.data = loadData2();
    $(".main2").fadeIn(1000);

    //console.log(global.data);

})

$(window).resize(function () {
    resize();
});

function resize() {
    var _width, _height, font, titleFont, font2;
    if ($(window).width() >= 600)
        _width = $(window).width() - $("#main-menu").width();
    else
        _width = $(window).width();
    _height = $(window).height();

    /*_width = 280;*/

    if (_width < 480) {
        font = 20;
        font2 = 20;
        titleFont = 15;
    } else {
        font = 30;
        font2 = 30;
        titleFont = 25;
    }
    $(".main2").width(_width).height(_height);
    $(".ikon").width(_width / 4);
    $(".ikon2").width(_width / 8);
    $(".ikon").css({
        "margin": (_width / 50) + "px"
    })
    $(".title").css({
        "font-size": (_width / titleFont) + "px"
    })
    $(".subtitle, .text").css({
        "font-size": (_width / font) + "px"
    })

    $(".text2").css({
        "font-size": (_width / font2) + "px"
    })

    $(".textLt").css({
        "font-size": (_width / 50) + "px"
    })
    $(".line").height(_height / 30);
    $(".mt3").width($(".mt").width());
    setTimeout("disableContentScroll();", 1);


}

function loadData() {

    if ((window.location.hostname == "m2.telekom.sk")) {
        jsonPath = "http://m2.telekom.sk/spotreba/js/data.json";
    } else {
        jsonPath = "/spotreba/js/data.json";
    }

    $.ajax({
        dataType: "json",
        url: jsonPath,
        /*url: 'http://m2.telekom.sk/spotreba/js/data.json',*/
        /*url: '../data.json',*/
        success: function (data) {
            global.data = data;
            //console.log(global.data);
            $(".main2").fadeIn(1000);
        },
        error: function () {
            alert("chyba pri nacitani dat ... skus este raz!");
            $(".main2").fadeIn(1000, function () {
                resize();
            });

        }
    });
}

function evalCena() {
    var zlava = 0;
    var bcena = eval("global." + global.pc + "+global." + global.tv + "+global." + global.pl);
    var cena = eval("global.data." + global.pc + global.tv + global.pl + "[0].cena");


    if (global.pc != "o") {
        zlava += 10;
        showPackageDescription("pc");
    } else {
        hidePackageDescription("pc");
    }
    if (global.tv != "o") {
        zlava += 10;
        showPackageDescription("tv");
    } else {
        hidePackageDescription("tv");
    }
    if (global.pl != "o") {
        zlava += 10;
        showPackageDescription("pl");
    } else {
        hidePackageDescription("pl");
    }

    $("#bcena").html(bcena + "&nbsp;&euro;");
    $("#vmp").html(cena + "&nbsp;&euro;");
    $("#wzlava").html(zlava + "&nbsp;&euro;");
    $("#mzlava").html((bcena - cena) + "&nbsp;&euro;");


    cena = cena;

    //console.log(arrBalik[1]);

    $(".i").each(function (index) {
        if ($(this).hasClass("selected")) {
            $(this).children(".plus").html("&nbsp;");
        } else {
            global1.tv = global.tv;
            global1.pc = global.pc;
            global1.pl = global.pl;
            var curr = $(this).children(".ikon").attr("id").split("-");
            eval("global1." + curr[0] + " = '" + curr[1] + "'");
            var cenaPlus = eval("global.data." + global1.pc + global1.tv + global1.pl + "[0].cena");
            if ($(this).hasClass("selected")) {
                $(this).children(".plus").html("&nbsp;");
            } else {
                if (cenaPlus - cena >= 0)
                    $(this).children(".plus").html("+ " + (cenaPlus - cena) + " &euro;");
                else
                    $(this).children(".plus").html("&nbsp;");
            }
        }
        ;
    });

    resize();

}

function showPackageDescription(id) {
    var pType = eval("global." + id);
    $("." + id).html(eval("global.text." + id + "[0]." + pType) + '<div class="line"></div>');
    $("." + id).show();
}

function hidePackageDescription(id) {
    var pType = eval("global." + id);
    $("." + id).html("");
    $("." + id).hide();
}

function dostupnost() {

    /*
     if (global.pc == 'o' && global.pl == 'o' && global.tv == 'o') {

     $("#error").show();

     } else {
     var ref = window.open('/chytry-balik/formular?nomenu=' + nomenu +'&internet=' + global.pc +'&phone=' + global.pl + '&tv=' + global.tv + '&bcena=' + $("#bcena").html().replace("&nbsp;&euro;", "") + '&cena=' + $("#vmp").html().replace("&nbsp;&euro;", "") + '&zlava=' + $("#wzlava").html().replace("&nbsp;&euro;", ""), '_self', 'location=no');
     }
     */

    var ref = window.open('/chytry-balik/formular?nomenu=' + nomenu + '&internet=' + global.pc + '&phone=' + global.pl + '&tv=' + global.tv + '&bcena=' + $("#bcena").html().replace("&nbsp;&euro;", "") + '&cena=' + $("#vmp").html().replace("&nbsp;&euro;", "") + '&zlava=' + $("#wzlava").html().replace("&nbsp;&euro;", ""), '_self', 'location=no');

}