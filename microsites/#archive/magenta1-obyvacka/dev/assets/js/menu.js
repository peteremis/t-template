var magentaMenu = {
    init: function () {
        this.cacheDOM();
        this.open();
        this.close();
    },
    cacheDOM: function () {
        this.$mainLi = $('#magenta-menu li');
    },
    open: function () {
        
        this.$mainLi.on('click', function (event) {
            console.log($(event.target).closest('li'));
            var $childUl = $(this).children('ul'),
                hasChild = ($childUl.length > 0); // If > 0, it has child
            if (hasChild) {
                if ($(event.target).closest('ul').closest('li').hasClass('magentaMenu_active') || $(event.target).closest('li').hasClass('magentaMenu_active')) {
                    $(this).toggleClass('magentaMenu_active');
                }
                $(this).addClass('magentaMenu_active');
                $(this).children('ul').addClass('magentaMenu_open');
            };
        });
    },
    close: function () {
        $(document).on('click', function (event) {
            if (!$(event.target).closest('#magenta-menu').length) {
                $('.magentaMenu_open').removeClass('magentaMenu_open');
                $('.magentaMenu_active').removeClass('magentaMenu_active');
            }
            
        });
    }
};

$(document).ready(magentaMenu.init());
