$(function () {
    $('.info').qtip({
        content: {
            text: 'Služby Chytrého balíka sú:<br/>' +
                '■ Magio internet – spoľahlivý a bezpečný internet na doma s rýchlosťou až 300/30 Mb/s<br/>' +
                '■ Magio televízia – až 113 TV staníc, funkcia zastavenie a pretočenie obrazu a 7-dňový archív<br/>' +
                '■ Pevná linka – neobmedzené volania aj na mobily , široká dostupnosť a kvalitný zvuk<br/>' +
                'Všetky služby ponúkame v M, L a XL verzii.'
        },
        position: {
            my: 'left center',
            at: 'right center'
        }
    });

    $(".scroll-to").click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top - ($("#nav-sticky").outerHeight())
                }, 1000);
                return false;
            }
        }
    });

    var contentSections = $("[id*='sec-']"),
        secondaryNav = $("#nav-sticky");

    function updateSecondaryNavigation() {
        contentSections.each(function () {
            var actual = $(this),
                actualHeight = actual.height() + parseInt(actual.css('paddingTop').replace('px', '')) + parseInt(actual.css('paddingBottom').replace('px', '')),
                actualAnchor = secondaryNav.find('a[href="#' + actual.attr('id') + '"]');

            if ((actual.offset().top - secondaryNav.outerHeight() <= $(window).scrollTop()) && (actual.offset().top + actualHeight - secondaryNav.outerHeight() > $(window).scrollTop())) {
                actualAnchor.addClass('active');
            } else {
                actualAnchor.removeClass('active');
            }

        });
    }
    updateSecondaryNavigation();

    $(window).scroll(function (event) {
        updateSecondaryNavigation();
    });


});