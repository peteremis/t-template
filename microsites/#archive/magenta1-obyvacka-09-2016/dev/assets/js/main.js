function isMagentaOffice() {

	var magentaOffice = 0;

	if(window.location.href.indexOf("biznis") > -1) {
		magentaOffice = 1;
	}

	return magentaOffice;
}

$(document).ready(function () {
	// $(".itemHolder, .itemHolder2, .itemHolder3, .itemHolder4, .itemHolder5, .itemHolder6").hide();

	var mobPrice2, mobPrice2a, mobPrice2b, mobPrice3, mobPrice3a, mobPrice3b, mobPrice4, mobPrice4a, mobPrice4b, mobPrice5, mobPrice5a, mobPrice5b, mobPrice6, mobPrice6a, mobPrice6b = 0;

	var mobPrice2hw, mobPrice3hw, mobPrice4hw, mobPrice5hw, mobPrice6hw  = 0;

	var actualStat, prodStatusInternet, prodStatusTv, prodStatusFix = '';

	if (isMagentaOffice() == 1) {
		$('#sec-2').css('background-image', 'url(../assets/img/bg-main_office.png)');
		$('#hero').css('background-image', 'url(../assets/img/kv_office.jpg)');
	} else {
		$('#sec-2').css('background-image', 'url(../assets/img/bg-main.jpg)');
		$('#hero').css('background-image', 'url(../assets/img/kv_v3.png)');
	}

	var magentaCalc = {
		config: {
			menu: $('#magenta-menu'),
			childClass: $('.child'),
			notInterestedClass: $('.not-interested'),
			disabledElem: $('.disabled'),
			buttonBuy: $('.button-buy'),
			lightbox: $('.lightbox-cover'),
			priceHolder: $('.priceHolder'),
			priceHolderZ: $('.priceHolderZ'),
			itemHolder: $('.itemHolder'),
			itemHolder2: $('.itemHolder2'),
			itemHolder3: $('.itemHolder3'),
			itemHolder4: $('.itemHolder4'),
			itemHolder5: $('.itemHolder5'),
			itemHolder6: $('.itemHolder6'),
			price_mobile2: $('.price-mobile2'),
			price_mobile3: $('.price-mobile3'),
			price_mobile4: $('.price-mobile4'),
			price_mobile5: $('.price-mobile5'),
			price_mobile6: $('.price-mobile6'),
			formHolder: $('.formFieldRender .pageContent .dynamicFormSubFields'),
			form: $('.formComposition'),
			form__internet: $('.form__internet'),
			form__tv: $('.form__tv'),
			form__pL: $('.form__pl'),
			form__mp: $('.form__mp'),
			form__1p: $('.form__1p'),
			form__2p: $('.form__2p'),
			form__3p: $('.form__3p'),
			fi__parent: $('.form__internet').closest('.fieldTypeINPUT'),
			ftv__parent: $('.form__tv').closest('.fieldTypeINPUT'),
			fpL__parent: $('.form__pl').closest('.fieldTypeINPUT'),
			fmp__parent: $('.form__mp').closest('.fieldTypeINPUT'),
			f1p__parent: $('.form__1p').closest('.fieldTypeINPUT'),
			f2p__parent: $('.form__2p').closest('.fieldTypeINPUT'),
			f3p__parent: $('.form__3p').closest('.fieldTypeINPUT'),
			masterListItems: $('.master'),
			mLiCount: $('.master').length - 1,
			myMobProdCount: 0,
			myMobProdCount2: 0,
			productHolders: $('.productHolder'),
			productHoldersCount: $('.productHolder').length,
			requiredProductsForMagenta: 3,
			clickThrough: false,
			package1: 'o',
			package2: 'o',
			package3: 'o',
			package4: 'o',
			prevPrice: 0,
			mob2Price: 0,
			fixPrice: 0,
			price: 0,
			qtipShowed: false,
			productLabel: ['Internet', 'Televízia', 'Pevná linka'],
			mobileHw: {'itemHolder2':false, 'itemHolder3':false, 'itemHolder4':false, 'itemHolder5':false, 'itemHolder6':false},
			hwItemHolder2: true,
			hwItemHolder3: true,
			skupina: [0, 0, 0, 0],
			url: "",
			situacia: 0,
			situaciaPrice: [0,25,43,49,18,24],
		},

		fixedProdPrice: {
			'ooo': 0,
			'Moo': 10,
			'oMo': 10,
			'ooM': 10,
			'Loo': 15,
			'oLo': 15,
			'ooL': 15,
			'Xoo': 20,
			'oXo': 20,
			'ooX': 20,
			'MMo': 18,
			'MoM': 18,
			'oMM': 18,
			'MMM': 23,
			'LLo': 24,
			'LoL': 24,
			'oLL': 24,
			'LLL': 29,
			'XXo': 32,
			'oXX': 32,
			'XoX': 32,
			'XXX': 38,
			'MLo': 21,
			'MoL': 21,
			'oML': 21,
			'MXo': 25,
			'MoX': 25,
			'oMX': 25,
			'LMo': 21,
			'LoM': 21,
			'oLM': 21,
			'LXo': 28,
			'LoX': 28,
			'oLX': 28,
			'XMo': 25,
			'XoM': 25,
			'oXM': 25,
			'XLo': 28,
			'XoL': 28,
			'oXL': 28,
			'MML': 25,
			'MLM': 25,
			'LMM': 25,
			'MLL': 27,
			'LML': 27,
			'LLM': 27,
			'MMX': 28,
			'MXM': 28,
			'XMM': 28,
			'MXX': 33,
			'XMX': 33,
			'XXM': 33,
			'MLX': 30,
			'MXL': 30,
			'LMX': 30,
			'LXM': 30,
			'XML': 30,
			'XLM': 30,
			'XXL': 35,
			'XLX': 35,
			'LXX': 35
		},
		fixedProdPriceOffice: {
			'ooo': 0,
			'Moo': 10,
			'oMo': 10,
			'ooM': 20,
			'Loo': 15,
			'oLo': 15,
			'ooL': 30,
			'Xoo': 20,
			'oXo': 20,
			'ooX': 40,
			'MMo': 18,
			'MoM': 25,
			'oMM': 25,
			'MMM': 30,
			'LLo': 24,
			'LoL': 35,
			'oLL': 40,
			'LLL': 45,
			'XXo': 32,
			'oXX': 55,
			'XoX': 45,
			'XXX': 60,
			'MLo': 21,
			'MoL': 30,
			'oML': 35,
			'MXo': 25,
			'MoX': 35,
			'oMX': 45,
			'LMo': 21,
			'LoM': 30,
			'oLM': 30,
			'LXo': 28,
			'LoX': 40,
			'oLX': 50,
			'XMo': 25,
			'XoM': 35,
			'oXM': 35,
			'XLo': 28,
			'XoL': 40,
			'oXL': 45,
			'MML': 35,
			'MLM': 35,
			'LMM': 35,
			'MLL': 40,
			'LML': 40,
			'LLM': 40,
			'MMX': 40,
			'MXM': 40,
			'XMM': 40,
			'MXX': 50,
			'XMX': 50,
			'XXM': 50,
			'MLX': 45,
			'MXL': 45,
			'LMX': 45,
			'LXM': 45,
			'XML': 45,
			'XLM': 45,
			'XXL': 55,
			'XLX': 55,
			'LXX': 55,
			'1oo': 18,
			'2oo': 24,
			'o3o': 25,
		},
		mobProd1Price: {
			'o': {
				price: [0, 0, 0, 0, 0, 0],
				pricehw: [0, 0, 0, 0, 0, 0]
			},
			'a': {
				//Happy L
				price: [27.99, 27.99, 25.99, 23.99, 21.99, 19.99],
				pricehw: [29.99, 29.99, 27.99, 25.99, 23.99, 21.99]
			},
			//Happy XL Volania
			'b': {
				price: [27.99, 27.99, 25.99, 19.99, 19.99, 19.99],
				pricehw: [29.99, 29.99, 27.99, 21.99, 21.99, 21.99]
			},
			//Happy XL
			'd': {
				price: [35.99, 35.99, 31.99, 29.99, 27.99, 25.99],
				pricehw: [39.99, 39.99, 35.99, 33.99, 31.99, 29.99]
			},
			//Happy XXL
			'e': {
				price: [46.99, 46.99, 42.99, 40.99, 38.99, 36.99],
				pricehw: [54.99, 54.99, 50.99, 48.99, 46.99, 44.99]
			},
			//Happy Profi
			'f': {
				price: [59.99, 59.99, 53.99, 51.99, 49.99, 47.99],
				pricehw: [69.99, 69.99, 63.99, 61.99, 59.99, 57.99]
			},
			//Happy M
			'g': {
				price: [19.99, 19.99, 17.99, 16.99, 15.99, 14.99],
				pricehw: [23.99, 23.99, 21.99, 20.99, 19.99, 18.99]
			}
		},
		mobProd2Price: {
			'o': {
				price: [0, 0, 0, 0, 0, 0],
				pricehw: [0, 0, 0, 0, 0, 0],
				count: 0,
				count2: 0,
				label: ''
			},

			'a': {
				price: [27.99, 27.99, 25.99, 23.99, 21.99, 19.99],
				pricehw: [29.99, 29.99, 27.99, 25.99, 23.99, 21.99],
				count: 1,
				count2: 1,
				label: 'Happy L'
			},
			'b': {
				price: [27.99, 27.99, 25.99, 19.99, 19.99, 19.99],
				pricehw: [29.99, 29.99, 27.99, 21.99, 21.99, 21.99],
				count: 1,
				count2: 1,
				label: 'Happy XL Volania'
			},
			'c': {
				price: [15.99, 15.99, 14.99, 13.99, 12.99, 11.99],
				pricehw: [16.99, 16.99, 15.99, 14.99, 13.99, 12.99],
				count: 0,
				count2: 1,
				label: 'Happy S'
			 },
			'd': {
				price: [35.99, 35.99, 31.99, 29.99, 27.99, 25.99],
				pricehw: [39.99, 39.99, 35.99, 33.99, 31.99, 29.99],
				count: 1,
				count2: 1,
				label: 'Happy XL'
			},
			'e': {
				price: [46.99, 46.99, 42.99, 40.99, 38.99, 36.99],
				pricehw: [54.99, 54.99, 50.99, 48.99, 46.99, 44.99],
				count: 1,
				count2: 1,
				label: 'Happy XXL '
			},
			'f': {
				price: [59.99, 59.99, 53.99, 51.99, 49.99, 47.99],
				pricehw: [69.99, 69.99, 63.99, 61.99, 59.99, 57.99],
				count: 1,
				count2: 1,
				label: 'Happy Profi'
			},
			'g': {
				price: [4.99, 4.99, 4.49, 3.99, 3.49, 2.99],
				pricehw: [5.99, 5.99, 5.49, 4.99, 4.49, 3.99],
				count: 0,
				count2: 1,
				label: 'Happy XS mini'
			},
			'h': {
				price: [4.50, 4.50, 4.50, 4.50, 4.50, 4.50],
				pricehw: [4.50, 4.50, 4.50, 4.50, 4.50, 4.50],
				count: 0,
				count2: 1,
				label: 'Easy karta'
			},
			'i': {
				price: [9.99, 9.99, 8.99, 8.49, 7.99, 7.49],
				pricehw: [9.99, 9.99, 8.99, 8.49, 7.99, 7.49],
				count: 0,
				count2: 1,
				label: 'Happy XS'
			},
			'j': {
				price: [19.99, 19.99, 17.99, 16.99, 15.99, 14.99],
				pricehw: [23.99, 23.99, 21.99, 20.99, 19.99, 18.99],
				count: 1,
				count2: 1,
				label: 'Happy M'
			}

		},
		init: function () {

			// this.toggleLightBox();
			this.setAllInputFields();
			this.disableBuyButton();
			this.menuInit();
			this.qtipInit();
			this.disableClick($(this.config.disabledElem));
			this.bindEvents();
			this.hideForm();
			this.hideFormFields();
			this.addCloseBtn();
			this.setpriceHolderHeight();
		},
		menuInit: function () {
			$(this.config.menu).on('click', function (event) {

				event.preventDefault();

				if ($(event.target).parent().hasClass('master')) {
					$('#magenta-menu ul').addClass('hidden');
				}

				$(event.target).parent().children('ul').toggleClass('hidden');
				$(event.target).parent().children('ul').addClass('is_open');
				$('#magenta-menu').toggleClass('is_closed');

				if ($(event.target).parent().children().length <= 1) {
					$('#magenta-menu .is_open').toggleClass('hidden');
					$('#magenta-menu ul').removeClass('is_open');
					$('#magenta-menu').addClass('is_closed');
				}

			});


			$(document).on('click', function (event) {
				if (!$(event.target).closest('#magenta-menu').length) {
					if (!$('#magenta-menu').hasClass('is_closed')) {
						$('#magenta-menu .is_open').toggleClass('hidden');
						$('#magenta-menu ul').removeClass('is_open');
						$('#magenta-menu').removeClass('is_closed');
					}
				}
			});

		},
		qtipInit: function () {
			$('#help-1').qtip({
				content: {
					text: 'Na úvod vyberte, či máte Magio internet, chcete nový alebo nemáte záujem o internet.<br /><a class="button qtip-button close-btn">Rozumiem</a>'
				},
				events: {
					render: function (event, api) {
						$('.close-btn', this).click(function () {
							api.hide();
							// magentaCalc.toggleLightBox();
						});
					}
				},
				show: {
					event: false,
					when: false,
					ready: true
				},
				hide: {
					event: 'click'
				},
				position: {
					my: 'top center',
					at: 'bottom center'
				}
			});
			$('#help-2a').qtip({
				content: {
					text: 'Výberom pevných služieb ste získali nárok na jeden paušál za výhodnejšiu cenu.<br /><a class="button qtip-button close-btn">Rozumiem</a>'
				},
				events: {
					render: function (event, api) {
						// magentaCalc.toggleLightBox();
						$('.close-btn', this).click(function () {
							api.hide();
							// magentaCalc.toggleLightBox();
						});
					}
				},
				show: {
					event: false,
					when: false
				},
				hide: false,
				position: {
					my: 'top center',
					at: 'bottom center'
				}
			});
			$('#help-3a').qtip({
				content: {
					text: 'Máte nárok na 3 paušály do vašej rodiny za výhodnejšiu cenu.<br /><a class="button qtip-button close-btn">Pokračovať vo výbere</a><br /><a href="#" class="close-btn qtip-custom-blue">Nemám záujem</a>'
				},
				events: {
					render: function (event, api) {
						// magentaCalc.toggleLightBox();
						$('.close-btn', this).click(function () {
							api.hide();
							// magentaCalc.toggleLightBox();
						});
					}
				},
				show: {
					event: false,
					when: false
				},
				hide: false,
				position: {
					my: 'top center',
					at: 'bottom center'
				}
			});
		},
		toggleQtip: function (id) {
			var div = $('#' + id);
			if (div.data('visible')) {
				div.qtip('hide');
				div.data('visible', false);
			} else {
				div.qtip('show');
				div.data('visible', true);
			}
		},
		setAllInputFields: function () {
			this.config.fi__parent.find('input').val('Nemám záujem');
			this.config.ftv__parent.find('input').val('Nemám záujem');
			this.config.fpL__parent.find('input').val('Nemám záujem');
			this.config.fmp__parent.find('input').val('Nemám záujem');
			this.config.f1p__parent.find('input').val('Nemám záujem');
			this.config.f2p__parent.find('input').val('Nemám záujem');
			this.config.f3p__parent.find('input').val('Nemám záujem');
		},
		hideForm: function () {
			this.config.form.addClass('hidden');
		},
		showForm: function () {
			this.config.form.removeClass('hidden');
		},
		addCloseBtn: function () {
			this.config.formHolder.append('<div class="dynamicFormItemBox"><a class="formCloseBtn close-icon" href="#"><i class="fa fa-times" aria-hidden="true"></i></a></div>');
			this.config.formHolder.append('<div class="dynamicFormItemBox"><a class="formCloseBtn" href="#">Zavrieť</a></div>');
		},
		toggleLightBox: function () {
			// this.config.lightbox.toggleClass('hidden');
		},
		hideFormFields: function () {
			magentaCalc.config.fi__parent.addClass('pos-top');
			magentaCalc.config.ftv__parent.addClass('pos-top');
			magentaCalc.config.fpL__parent.addClass('pos-top');
			magentaCalc.config.fmp__parent.addClass('pos-top');
			magentaCalc.config.f1p__parent.addClass('pos-top');
			magentaCalc.config.f2p__parent.addClass('pos-top');
			magentaCalc.config.f3p__parent.addClass('pos-top');
		},
		disableBuyButton: function () {
			this.config.buttonBuy.addClass('disabled');
		},
		enableBuyButton: function () {
			this.config.buttonBuy.removeClass('disabled');
		},
		bindEvents: function () {
			$('.child').on('click', function (event) {
				magentaCalc.cacheTheClickEvents(event);
			});
			$('.not-interested').on('click', function (event) {
				magentaCalc.cacheTheClickEvents(event);
			});
			this.config.buttonBuy.on('click', function () {
				if ($(this).hasClass('disabled')) {
					event.preventDefault();
				} else {
					magentaCalc.config.form.toggleClass('hidden');
					// magentaCalc.toggleLightBox();
				}
			});

			$('#internet-sub').on('click', function (event) {
				$('.check-internet').toggleClass("checked");
				if ($('.check-internet').hasClass("checked")) {
					magentaCalc.config.skupina[0] = 1;
				} else {
					magentaCalc.config.skupina[0] = 0;
				}
				magentaCalc.calculatePrice();
			});

			$('#televizia-sub').on('click', function (event) {
				$('.check-televizia').toggleClass("checked");
				if ($('.check-televizia').hasClass("checked")) {
					magentaCalc.config.skupina[1] = 1;
				} else {
					magentaCalc.config.skupina[1] = 0;
				}
				magentaCalc.calculatePrice();
			});

			$('#pevna-sub').on('click', function (event) {
				$('.check-pevna').toggleClass("checked");
				if ($('.check-pevna').hasClass("checked")) {
					magentaCalc.config.skupina[2] = 1;
				} else {
					magentaCalc.config.skupina[2] = 0;
				}
				magentaCalc.calculatePrice();
			});

			$('#happy-sub').on('click', function (event) {
				$('.check-happy').toggleClass("checked");
				if ($('.check-happy').hasClass("checked")) {
					magentaCalc.config.skupina[3] = 1;
				} else {
					magentaCalc.config.skupina[3] = 0;
				}
				magentaCalc.calculatePrice();
			});

			$('#hw-check2').on('click', function (event) {
				$("#hw-check2").toggleClass("checked");
				isHwEnabled2(2);
				magentaCalc.calculatePrice();
			});

			$('#hw-check3').on('click', function (event) {
				$("#hw-check3").toggleClass("checked");
				isHwEnabled2(3);
				var choosen = magentaCalc.config.package1 + magentaCalc.config.package2 + magentaCalc.config.package3 + magentaCalc.config.package4;
				magentaCalc.calculatePrice(4, '', choosen);
			});

			$('#hw-check4').on('click', function (event) {
				$("#hw-check4").toggleClass("checked");
				isHwEnabled2(4);
				var choosen = magentaCalc.config.package1 + magentaCalc.config.package2 + magentaCalc.config.package3 + magentaCalc.config.package4;
				magentaCalc.calculatePrice(4, '', choosen);
			});

			$('#hw-check5').on('click', function (event) {
				$("#hw-check5").toggleClass("checked");
				isHwEnabled2(5);
				var choosen = magentaCalc.config.package1 + magentaCalc.config.package2 + magentaCalc.config.package3 + magentaCalc.config.package4;
				magentaCalc.calculatePrice(4, '', choosen);
			});

			$('#hw-check6').on('click', function (event) {
				$("#hw-check6").toggleClass("checked");
				isHwEnabled2(6);
				var choosen = magentaCalc.config.package1 + magentaCalc.config.package2 + magentaCalc.config.package3 + magentaCalc.config.package4;
				magentaCalc.calculatePrice(4, '', choosen);
			});

		},
		cacheTheClickEvents: function (event) {
			var chosenProgram = null,
				chosenType = null,
				chosenProductHolder = $(event.target).closest('.master').find('.productHolder'),
				typeCode = $(event.target).closest('.master').data('type'),
				order = $(event.target).closest('.master').data('order'),

				master = $(event.target).closest('.master'),
				prodPackage = $(event.target).closest('.package').data('package'),
				prodVariant = $(event.target).closest('.product-variant').data('variant'),
				isChildClass = $(event.target).is('.child') ? true : false,
				isNotIntestedClass = $(event.target).is('.not-interested') ? true : false;

			isChildClass ? chosenProgram = $(event.target).text() : chosenProgram = null;
			isChildClass ? chosenType = $(event.target).closest('.parent').find('.type').text() : chosenType = $(event.target).text();

			actualStat = $(event.target).closest('.parent').find('.type').text();

			if (actualStat == 'Už mám internet') {
				prodStatusInternet = 0;
			} else if (actualStat == 'Chcem internet') {
				prodStatusInternet = 1;
			} else if (actualStat == 'Už mám televíziu') {
				prodStatusTv = 0;
			} else if (actualStat == 'Chcem televíziu') {
				prodStatusTv = 1;
			} else if (actualStat == 'Už mám pevnú linku') {
				prodStatusFix = 0;
			} else if (actualStat == 'Chcem pevnú linku') {
				prodStatusFix = 1;
			}

			chosenProductHolder.text(isChildClass ? chosenProgram : chosenType);
			chosenProductHolder.removeClass('hidden');
			master.data('choosen', isChildClass ? 1 : 0);
			chosenProductHolder.data('product', isChildClass ? 1 : 0);

			prodStatus = $(event.target).closest('.product-variant').data('status');


			
			if (this.choosenFixProductsCount() > 0) {

				this.enableBuyButton();
			} else {
				this.disableBuyButton();

			}

			if (order <= 2) {
				if (($(this.config.productHolders[3]).data('variant') == "o" && this.choosenFixProductsCount() == 0)) {
					
					this.disableLastMobProd();

					this.clearInputField(this.config.form__1p.find('input'));
					this.clearInputField(this.config.form__2p.find('input'));
					this.clearInputField(this.config.form__3p.find('input'));
					$(this.config.productHolders[4]).text('Nemám záujem');
					this.config.buttonBuy.text('MÁM ZÁUJEM');
					this.config.buttonBuy.closest('li').removeClass('double-line');
					chosenProductHolder.data('product', 0);
					$(this.config.productHolders[4]).data('product', 0);
					this.config.myMobProdCount = 0;
					this.config.package1 = 'o';
					this.config.package2 = 'o';
					this.config.package3 = 'o';
					this.config.package4 = 'o';
					this.mob2Price = 0;
					
				}
				chosenProductHolder.data('variant', prodVariant);
			}

			if (order == 2) {
				this.enableFirstMobProd();
			}

			if (!this.config.clickThrough) {
				if (order < 2) {
					this.enableNext(order);
				} else if (order == 2) {
					this.config.clickThrough = true;
					if (this.choosenProductsCount() > 0) {
						this.enableNext(2);

						this.toggleQtip('help-2');
					}
				}
			} else if (this.config.clickThrough) {
				if (this.choosenProductsCount() < 1) {

					//this.disableMobProd();
					//$(this.config.productHolders[3]).text('Nemám záujem');
					//$(this.config.productHolders[4]).text('Nemám záujem');
					//this.clearInputField(this.config.form__1p.find('input'));
					//this.clearInputField(this.config.form__2p.find('input'));
					//this.clearInputField(this.config.form__3p.find('input'));
					//this.config.buttonBuy.text('MÁM ZÁUJEM');
					//this.config.buttonBuy.closest('li').removeClass('double-line');
					chosenProductHolder.data('product', 0);
					$(this.config.productHolders[3]).data('product', 0);
					$(this.config.productHolders[4]).data('product', 0);
					this.config.myMobProdCount = 0;
					this.config.package1 = 'o';
					this.config.package2 = 'o';
					this.config.package3 = 'o';
					this.config.package4 = 'o';
					this.mob2Price = 0;
					$(this.config.productHolders[3]).data('variant', 'o');
				} else if (this.choosenProductsCount() > 0) {
					this.enableFirstMobProd();
					this.enableNext(3);

				}

				if (order == 3) {

					this.showCMB();

					$(this.config.productHolders[3]).data('variant', prodVariant);
					if ((chosenProductHolder.data('product') == 1)) {
						this.enableNext(order);
						if (!this.config.qtipShowed) {
							this.toggleQtip('help-3');
							this.config.qtipShowed = true;
						}
					} else if ((chosenProductHolder.data('product') == 0 && this.choosenFixProductsCount() == 0)) {

						this.disableLastMobProd();
						this.mob2Price = 0;
						this.clearInputField(this.config.form__1p.find('input'));
						this.clearInputField(this.config.form__2p.find('input'));
						this.clearInputField(this.config.form__3p.find('input'));
						$(this.config.productHolders[4]).text('Nemám záujem');
						this.config.buttonBuy.text('MÁM ZÁUJEM');
						this.config.buttonBuy.closest('li').removeClass('double-line');
						chosenProductHolder.data('product', 0);
						$(this.config.productHolders[4]).data('product', 0);
						this.config.myMobProdCount = 0;
						this.config.package1 = 'o';
						this.config.package2 = 'o';
						this.config.package3 = 'o';
						this.config.package4 = 'o';
					}
				}

				if (order == 4 && prodPackage == 4) {
					this.clearInputField(this.config.form__1p.find('input'));
					this.clearInputField(this.config.form__2p.find('input'));
					this.clearInputField(this.config.form__3p.find('input'));
				}

				if (this.choosenMobProductsCount() > 0) {

					this.config.buttonBuy.html('CHCEM MAGENTU 1');
					this.config.buttonBuy.closest('li').addClass('double-line');

				} else {
					this.config.buttonBuy.text('MÁM ZÁUJEM');
					this.config.buttonBuy.closest('li').removeClass('double-line');

				}

				if (order == 4) {
					switch (prodPackage) {
						case 0:
							this.config.package1 = prodVariant;
							break;
						case 1:
							this.config.package2 = prodVariant;
							break;
						case 2:
							this.config.package3 = prodVariant;
							break;
						case 3:
							this.config.package4 = prodVariant;
							break;
						case 4:
							this.config.package1 = 'o';
							this.config.package2 = 'o';
							this.config.package3 = 'o';
							this.config.package4 = 'o';
							break;
					}

					this.config.myMobProdCount = this.mobProd2Price[this.config.package1].count + this.mobProd2Price[this.config.package2].count + this.mobProd2Price[this.config.package3].count + this.mobProd2Price[this.config.package4].count;
					this.config.myMobProdCount2 = this.mobProd2Price[this.config.package1].count2 + this.mobProd2Price[this.config.package2].count2 + this.mobProd2Price[this.config.package3].count2 + this.mobProd2Price[this.config.package4].count2;
					var myMobProdText = '';

					switch (this.config.myMobProdCount2) {
						case 0:
							myMobProdText = 'Bez paušálu';
							
							break;
						case 1:
							
							myMobProdText = '1 paušál';
							break;
						case 2:
							myMobProdText = '2 paušály';
							break;
						case 3:
							myMobProdText = '3 paušály';
							break;
						case 4:
							myMobProdText = '4 paušály';
							break;
					}

					chosenProductHolder.text(myMobProdText);
					chosenProductHolder.data('variant', this.config.package1 + this.config.package2 + this.config.package3 + this.config.package4);

				}

			}

			this.switchStatement(typeCode, chosenType, chosenProgram, prodPackage);
			this.calculatePrice(order, prodVariant, chosenProductHolder.data('variant'));

		},
		showCMB: function () {
			if (window.innerWidth > 1053) {
				if ($('.cmb__area').css("display") == "inline-block") {
					$('.cmb__area__head').text('Potrebujete poradiť a získať tak výhodnejšie služby?');
					$('#cmb__holder').show();
				}
			}
		},
		setpriceHolderHeight: function () {
			if (window.innerWidth < 1053) {
				var $priceHolder = $('.priceHolder2'),
					priceHolderHeight = $priceHolder.height();

				$priceHolder.closest('li').css('height', priceHolderHeight);
			}
		},
		switchStatement: function (typeCode, chosenType, chosenProgram, prodPackage) {
			var result = '';
			if (chosenType && chosenProgram) {
				result = chosenType + ' - ' + chosenProgram;
			} else if (!chosenType && chosenProgram) {
				result = chosenProgram;
			} else if (chosenType && !chosenProgram) {
				result = chosenType;
			}

			switch (typeCode) {
				case 0:
					magentaCalc.config.form__internet.find('input').val(result);
					break;
				case 1:
					magentaCalc.config.form__tv.find('input').val(result);
					break;
				case 2:
					magentaCalc.config.form__pL.find('input').val(result);
					break;
				case 3:
					magentaCalc.config.form__mp.find('input').val(result);
					break;
				case 4:
					switch (prodPackage) {
						case 0:
							magentaCalc.config.form__1p.find('input').val(result);
							break;
						case 1:
							magentaCalc.config.form__2p.find('input').val(result);
							break;
						case 2:
							magentaCalc.config.form__3p.find('input').val(result);
							break;
					}
					break;
			}
		},
		choosenProductsCount: function () {
			var count = 0;
			for (var i = 0; i < magentaCalc.config.productHoldersCount; i++) {
				count += $(magentaCalc.config.productHolders[i]).data('product');
			}
			return count
		},
		choosenFixProductsCount: function () {
			var count = 0;
			for (var i = 0; i <= 2; i++) {
				count += $(magentaCalc.config.productHolders[i]).data('product');
			}
			var fix = this.getProductVariantsSum();
			if (fix[0] == "o" && this.config.skupina[0] == 1){
				count++;
			}
			if (fix[1] == "o" && this.config.skupina[1] == 1){
				count++;
			}
			if (fix[2] == "o" && this.config.skupina[2] == 1){
				count++;
			}


			return count
		},
		choosenMobProductsCount: function () {
			var count = 0;
			var a = this.mobProd2Price[this.config.package1].count + this.mobProd2Price[this.config.package2].count + this.mobProd2Price[this.config.package3].count + this.mobProd2Price[this.config.package4].count;
			count = a;
			if ($(this.config.productHolders[3]).data('variant') == "o" && this.config.skupina[3] == 1){
				count++;
			}else if ($(this.config.productHolders[3]).data('variant') != "o"){
				count++;
			}
			if (count > 3) count = 3;
			return count
		},
		choosenCount: function () {
			var count = 0;
			for (var i = 0; i < (magentaCalc.config.requiredProductsForMagenta - 1); i++) {
				count += $(magentaCalc.config.productHolders[i]).data('choosen');
			}
			return count
		},
		disableClick: function ($parentLI) {
			$parentLI.on('mouseenter.smartmenus mouseleave.smartmenus mousedown.smartmenus focus.smartmenus blur.smartmenus click.smartmenus touchend.smartmenus', 'a', function (e) {
				e.stopPropagation();
				e.preventDefault();
			});
		},
		disableMobProd: function () {
			for (var i = 3; i <= 4; i++) {
				this.disableClick($(this.config.masterListItems[i]));
				$(this.config.masterListItems[i]).addClass('disabled');
			}
		},
		disableLastMobProd: function () {
			this.disableClick($(this.config.masterListItems[4]));
			$(this.config.masterListItems[4]).addClass('disabled');
		},
		enableClick: function ($parentLI) {
			$parentLI.off('mouseenter.smartmenus mouseleave.smartmenus mousedown.smartmenus focus.smartmenus blur.smartmenus click.smartmenus touchend.smartmenus');
		},
		enableFirstMobProd: function () {
			this.enableClick($(this.config.masterListItems[3]));
			$(this.config.masterListItems[3]).removeClass('disabled');
		},
		isChoosen: function (master) {
			(master.data('choosen') == 1) ? 'true' : 'false';
		},
		enableNext: function (order) {
			if (order < magentaCalc.config.mLiCount) {
				$(magentaCalc.config.masterListItems[order + 1]).removeClass('disabled');
				this.enableClick($(magentaCalc.config.masterListItems[order + 1]));
			}
		},
		clearInputField: function (inputObject) {
			inputObject.val('Nemám záujem');
		},
		getProductVariantsSum: function () {
			var result = '';
			for (var i = 0; i <= 2; i++) {
				result += $(this.config.productHolders[i]).data('variant');

			}
			return result;
		},

		calcDiscount: function () {
			var result = 0;
			var variant = this.getProductVariantsSum();

			if (isMagentaOffice() == 1) {

				if (magentaCalc.config.situacia>0) {
					result = magentaCalc.config.situaciaPrice[magentaCalc.config.situacia];

					variant = variant.replace(1, 'o');
					variant = variant.replace(2, 'o');
					variant = variant.replace(3, 'o');
				}

				var moProd1 = variant.charAt(0) + 'oo';
				var moProd2 = 'o' + variant.charAt(1) + 'o';
				var moProd3 = 'oo' + variant.charAt(2);

				result+= eval('this.fixedProdPriceOffice[' + '"' + moProd1 + '"' + ']');
				result+= eval('this.fixedProdPriceOffice[' + '"' + moProd2 + '"' + ']');
				result+= eval('this.fixedProdPriceOffice[' + '"' + moProd3 + '"' + ']');

			} else {
				for (var i = 0; i <= 2; i++) {
					if (variant[i] == "M") {
						result += 10;
					} else if (variant[i] == "L") {
						result += 15;
					} else if (variant[i] == "X") {
						result += 20;
					}
				}
			}

			return result;
		},

		calculatePrice: function (order, prodVariant, holderVariant) {

			mobPrice2hw = 0;
			mobPrice3hw = 0;
			mobPrice4hw = 0;
			mobPrice5hw = 0;
			mobPrice6hw = 0;

			var prod = ((this.choosenFixProductsCount() + this.choosenMobProductsCount()));

			if (prod > 5) prod = 5;

			if (isMagentaOffice() == 1) {
				this.config.price = magentaCalc.updateOfficePrice();
			} else {
				this.config.price = eval('this.fixedProdPrice[' + '"' + this.getProductVariantsSum() + '"' + ']');
			}

			if (magentaCalc.config.mobileHw.itemHolder2) {
				mobPrice2 = this.mobProd1Price[$(this.config.productHolders[3]).data('variant')].pricehw[prod];
				this.config.price += mobPrice2;
				mobPrice2hw = 1;
			} else {
				mobPrice2 = this.mobProd1Price[$(this.config.productHolders[3]).data('variant')].price[prod];
				this.config.price += mobPrice2;
			}

			this.config.prevPrice = this.config.price;

			if (order == 4) {

				if (magentaCalc.config.mobileHw.itemHolder3) {
					mobPrice3a = this.mobProd2Price[holderVariant[0]].pricehw[prod];
					mobPrice3b = this.mobProd2Price[holderVariant[0]].pricehw[0];
					mobPrice3hw = 1;
				} else {
					mobPrice3a = this.mobProd2Price[holderVariant[0]].price[prod];
					mobPrice3b = this.mobProd2Price[holderVariant[0]].price[0];
				}
				mobPrice3 = mobPrice3a;

				if (magentaCalc.config.mobileHw.itemHolder4) {
					mobPrice4a = this.mobProd2Price[holderVariant[1]].pricehw[prod];
					mobPrice4b = this.mobProd2Price[holderVariant[1]].pricehw[0];
					mobPrice4hw = 1;
				} else {
					mobPrice4a = this.mobProd2Price[holderVariant[1]].price[prod];
					mobPrice4b = this.mobProd2Price[holderVariant[1]].price[0];
				}
				mobPrice4 = mobPrice4a;

				if (magentaCalc.config.mobileHw.itemHolder5) {
					mobPrice5a = this.mobProd2Price[holderVariant[2]].pricehw[prod];
					mobPrice5b = this.mobProd2Price[holderVariant[2]].pricehw[0];
					mobPrice5hw = 1;
				} else {
					mobPrice5a = this.mobProd2Price[holderVariant[2]].price[prod];
					mobPrice5b = this.mobProd2Price[holderVariant[2]].price[0];
				}
				mobPrice5 = mobPrice5a;

				if (magentaCalc.config.mobileHw.itemHolder6) {
					mobPrice6a = this.mobProd2Price[holderVariant[3]].pricehw[prod];
					mobPrice6b = this.mobProd2Price[holderVariant[3]].pricehw[0];
					mobPrice6hw = 1;
				} else {
					mobPrice6a = this.mobProd2Price[holderVariant[3]].price[prod];
					mobPrice6b = this.mobProd2Price[holderVariant[3]].price[0];
				}
				mobPrice6 = mobPrice6a;

				this.config.price = this.config.prevPrice + mobPrice3a + mobPrice4a + mobPrice5a + mobPrice6a;
				this.mob2Price = mobPrice3b + mobPrice4b + mobPrice5b + mobPrice6b;


			}else {

				if (magentaCalc.config.mobileHw.itemHolder3) {
					mobPrice3 = this.mobProd2Price[this.config.package1].pricehw[prod];
					mobPrice3hw = 1;
				} else {
					mobPrice3 = this.mobProd2Price[this.config.package1].price[prod];
				}

				if (magentaCalc.config.mobileHw.itemHolder4) {
					mobPrice4 = this.mobProd2Price[this.config.package2].pricehw[prod];
					mobPrice4hw = 1;
				} else {
					mobPrice4 = this.mobProd2Price[this.config.package2].price[prod];
				}

				if (magentaCalc.config.mobileHw.itemHolder5) {
					mobPrice5 = this.mobProd2Price[this.config.package3].pricehw[prod];
					mobPrice5hw = 1;
				} else {
					mobPrice5 = this.mobProd2Price[this.config.package3].price[prod];
				}

				if (magentaCalc.config.mobileHw.itemHolder6) {
					mobPrice6 = this.mobProd2Price[this.config.package4].pricehw[prod];
					mobPrice6hw = 1;
				} else {
					mobPrice6 = this.mobProd2Price[this.config.package4].price[prod];
				}

				this.config.price = this.config.prevPrice + mobPrice3 + mobPrice4 + mobPrice5 + mobPrice6;

			}
			if (typeof this.mob2Price == "undefined") {
				this.mob2Price = 0;
			}

		
			this.updatePrice(this.config.price);
			this.updateBasket(this.getProductVariantsSum());
			

		},
		updateOfficePrice: function () {
			magentaCalc.config.situacia = 0;

			if ($(this.config.productHolders[0]).data('variant') == 1 && $(this.config.productHolders[1]).data('variant') == 3) {
				magentaCalc.config.situacia = 2;
			} else if ($(this.config.productHolders[0]).data('variant') == 2 && $(this.config.productHolders[1]).data('variant') == 3) {
				magentaCalc.config.situacia = 3;
			} else if ($(this.config.productHolders[0]).data('variant') == 1) {
				magentaCalc.config.situacia = 4;
			} else if ($(this.config.productHolders[0]).data('variant') == 2) {
				magentaCalc.config.situacia = 5;
			} else if ($(this.config.productHolders[1]).data('variant') == 3) {
				magentaCalc.config.situacia = 1;
			}

			var variant = this.getProductVariantsSum();

			if (magentaCalc.config.situacia>0) {
				variant = variant.replace(1, 'o');
				variant = variant.replace(2, 'o');
				variant = variant.replace(3, 'o');
			}

			return eval('this.fixedProdPriceOffice[' + '"' + variant + '"' + ']') + magentaCalc.config.situaciaPrice[magentaCalc.config.situacia];

		},

		updatePrice: function (price) {
			price = price.toFixed(2);
			var prod = ((this.choosenFixProductsCount() + this.choosenMobProductsCount()));
			if (prod > 5) prod = 5;
			//this.getProductVariantsSum()

			if (magentaCalc.config.mobileHw.itemHolder2) {
				mobPrice2a = this.mobProd1Price[$(this.config.productHolders[3]).data('variant')].pricehw[0];
			} else {
				mobPrice2a = this.mobProd1Price[$(this.config.productHolders[3]).data('variant')].price[0];
			}

			var normalPrice = (this.calcDiscount() + mobPrice2a + this.mob2Price ).toFixed(2);

			//price += this.mobProd1Price[$(this.config.productHolders[3]).data('variant')].price[prod];
			// this.config.priceHolder.text("PRED ZĽAVOU SPOLU: " + normalPrice + ' € ');
			// this.config.priceHolderZ.text("PO ZĽAVE SPOLU: " + price + ' €');
			this.config.priceHolder.text(normalPrice + ' € ');
			this.config.priceHolderZ.text(price + ' €');
			var x = 0;
			for (var i = 0; i < this.config.skupina.length; i++){
				x+=this.config.skupina[i];
			}

			this.config.url = "https://www.telekom.sk/magenta1/mam-zaujem?internet=" +
				$("#productHolderInternet").text() + "&internet-status=" + prodStatusInternet +
				"&tv=" + $("#productHolderTV").text() + "&tv-status=" + prodStatusTv +
				"&hlas=" + $("#productHolderPL").text() + "&hlas-status=" + prodStatusFix +
				"&skupina=" + x + "&moj-pausal=" +
				$("#productHolderMP").text() +
				"&fix=" + price +
				"&pausal1=" + this.mobProd2Price[this.config.package1].label +
				"&pausal2=" + this.mobProd2Price[this.config.package2].label +
				"&pausal3=" + this.mobProd2Price[this.config.package3].label +
				"&pausal4=" + this.mobProd2Price[this.config.package4].label +
				"&m1price=" + mobPrice2 +
				"&m1hw=" + mobPrice2hw +
				"&p1price=" + mobPrice3 +
				"&p1hw=" + mobPrice3hw +
				"&p2price=" + mobPrice4 +
				"&p2hw=" + mobPrice4hw +
				"&p3price=" + mobPrice5 +
				"&p3hw=" + mobPrice5hw +
				"&p4price=" + mobPrice6 +
				"&p4hw=" + mobPrice6hw +
				"&cena=" + normalPrice + "&zlava=" + price;

			$(".zaujem").attr("onclick", 'location.href="' + this.config.url + '"');
		},

		updateBasket: function (variants) {
			var prod = ((this.choosenFixProductsCount() + this.choosenMobProductsCount()));
			if (prod > 5) prod = 5;
			// fix
			var result = "";
			var plus = "";
			var normalPrice = this.calcDiscount();

			if (isMagentaOffice() == 1) {
				var price = magentaCalc.updateOfficePrice();
			} else {
				var price = eval('this.fixedProdPrice[' + '"' + this.getProductVariantsSum() + '"' + ']');
			}

			for (var i = 0; i < variants.length; i++) {

				if (variants[i] != "o") {
					result += this.config.productLabel[i] + " + ";
				}
			}

			result = result.slice(0, -2);
			result = "<span class='left'>" + result + "</span><br/><span class='left'>Cena</span><span class='right'>" + normalPrice + " €</span><br/><span class='left' style='clear:both'>Po zľave</span><span class='right'>" + price + " €</span>";
			if (result != "") {
				this.config.itemHolder.html(result);
				this.config.itemHolder.show();
				//this.config.itemHolder2.html("");
				//this.config.itemHolder2.height(this.config.itemHolder.height());
				this.config.itemHolder2.show();

			} else {
				this.config.itemHolder.hide();
				this.config.itemHolder2.hide();

			}

			//mobprod1
			var mp1 = $(this.config.productHolders[3]).data('variant');
			if (mp1 != "o") {

				mobPrice2a = this.mobProd1Price[mp1].price[0];
				mobPrice2b = this.mobProd1Price[mp1].price[prod];

				if (this.config.mobileHw.itemHolder2) {
					mobPrice2a = this.mobProd1Price[mp1].pricehw[0];
					mobPrice2b = this.mobProd1Price[mp1].pricehw[prod];
				}

				this.config.itemHolder2.html("<span class='left'>" + $("#productHolderMP").text() + "</span><br/><span class='left'>Cena</span><span class='right'>" + mobPrice2a + " €</span><br/><span class='left'>Po zľave</span><span class='right'>" +  mobPrice2b + " €</span>");
				this.config.price_mobile2.show();
			} else {
				this.config.price_mobile2.hide();
			}

			this.config.itemHolder3.html("");
			this.config.itemHolder4.html("");
			this.config.itemHolder5.html("");
			this.config.itemHolder6.html("");

			// 0 1 2
			if (this.config.package1 != "o") {

				mobPrice3a = this.mobProd2Price[this.config.package1].price[0];
				mobPrice3b = this.mobProd2Price[this.config.package1].price[prod];

				if (this.config.mobileHw.itemHolder3) {
					mobPrice3a = this.mobProd2Price[this.config.package1].pricehw[0];
					mobPrice3b = this.mobProd2Price[this.config.package1].pricehw[prod];
				}

				this.config.itemHolder3.html("<span class='left'>" + this.mobProd2Price[this.config.package1].label + "</span><br/><span class='left'>Cena</span><span class='right'>" + mobPrice3a + " €</span><br/><span class='left'>Po zľave</span><span class='right'>" + mobPrice3b + " €</span>");
				this.config.price_mobile3.show();
			} else {
				this.config.price_mobile3.hide();
			}

			if (this.config.package2 != "o") {
				mobPrice4a = this.mobProd2Price[this.config.package2].price[0];
				mobPrice4b = this.mobProd2Price[this.config.package2].price[prod];

				if (this.config.mobileHw.itemHolder4) {
					mobPrice4a = this.mobProd2Price[this.config.package2].pricehw[0];
					mobPrice4b = this.mobProd2Price[this.config.package2].pricehw[prod];
				}

				this.config.itemHolder4.html("<span class='left'>" + this.mobProd2Price[this.config.package2].label + "</span><br/><span class='left'>Cena</span><span class='right'>" + mobPrice4a + " €</span><br/><span class='left'>Po zľave</span><span class='right'>" + mobPrice4b + " €</span>");

				this.config.price_mobile4.show();
			} else {
				this.config.price_mobile4.hide();
			}

			if (this.config.package3 != "o") {
				mobPrice5a = this.mobProd2Price[this.config.package3].price[0];
				mobPrice5b = this.mobProd2Price[this.config.package3].price[prod];

				if (this.config.mobileHw.itemHolder5) {
					mobPrice5a = this.mobProd2Price[this.config.package3].pricehw[0];
					mobPrice5b = this.mobProd2Price[this.config.package3].pricehw[prod];
				}

				this.config.itemHolder5.html("<span class='left'>" + this.mobProd2Price[this.config.package3].label + "</span><br/><span class='left'>Cena</span><span class='right'>" + mobPrice5a + " €</span><br/><span class='left'>Po zľave</span><span class='right'>" + mobPrice5b + " €</span>");

				this.config.price_mobile5.show();
			} else {
				this.config.price_mobile5.hide();
			}

			if (this.config.package4 != "o") {

				mobPrice6a = this.mobProd2Price[this.config.package4].price[0];
				mobPrice6b = this.mobProd2Price[this.config.package4].price[prod];

				if (this.config.mobileHw.itemHolder6) {
					mobPrice6a = this.mobProd2Price[this.config.package4].pricehw[0];
					mobPrice6b = this.mobProd2Price[this.config.package4].pricehw[prod];
				}

				this.config.itemHolder6.html("<span class='left'>" + this.mobProd2Price[this.config.package4].label + "</span><br/><span class='left'>Cena</span><span class='right'>" + mobPrice6a + " €</span><br/><span class='left'>Po zľave</span><span class='right'>" + mobPrice6b + " €</span>");

				this.config.price_mobile6.show();
			} else {
				this.config.price_mobile6.hide();

			}
			
		}

	};

	magentaCalc.init();

	function isHwEnabled() {
		if ($("#hw-check").hasClass('checked')) {
			return true;
		} else {
			return false;
		}
	}

	function isHwEnabled2(pausal) {
		var item = ['itemHolder']+pausal;
		var hwEnabled = false;

		magentaCalc.config.mobileHw[item] = hwEnabled;

		if ($("#hw-check" + pausal).hasClass('checked')) {
			hwEnabled = true;
			magentaCalc.config.mobileHw[item] = hwEnabled;
		}

		return hwEnabled;
	}

	$('.formCloseBtn').on('click', function () {
		magentaCalc.hideForm();
		// magentaCalc.toggleLightBox();
	});


	$('.info').qtip({
		content: {
			text: 'Služby Chytrého balíka sú:<br/>' +
			'■ Magio internet – spoľahlivý a bezpečný internet na doma s rýchlosťou až 300/30 Mb/s<br/>' +
			'■ Magio televízia – až 113 TV staníc, funkcia zastavenie a pretočenie obrazu a 7-dňový archív<br/>' +
			'■ Pevná linka – neobmedzené volania aj na mobily , široká dostupnosť a kvalitný zvuk<br/>' +
			'Všetky služby ponúkame v M, L a XL verzii.'
		},
		position: {
			my: 'left center',
			at: 'right center'
		}
	});

	$(".scroll-to").click(function () {
		if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
			if (target.length) {
				$('html,body').animate({
					scrollTop: target.offset().top - ($("#nav-sticky").outerHeight())
				}, 1000);
				return false;
			}
		}
	});

	var contentSections = $("[id*='sec-']"),
		secondaryNav = $("#nav-sticky");

	function updateSecondaryNavigation() {
		contentSections.each(function () {
			var actual = $(this),
				actualHeight = actual.height() + parseInt(actual.css('paddingTop').replace('px', '')) + parseInt(actual.css('paddingBottom').replace('px', '')),
				actualAnchor = secondaryNav.find('a[href="#' + actual.attr('id') + '"]');

			if ((actual.offset().top - secondaryNav.outerHeight() <= $(window).scrollTop()) && (actual.offset().top + actualHeight - secondaryNav.outerHeight() > $(window).scrollTop())) {
				actualAnchor.addClass('active');
			} else {
				actualAnchor.removeClass('active');
			}

		});
	}

	updateSecondaryNavigation();

	$(window).scroll(function (event) {
		updateSecondaryNavigation();
	});


});
