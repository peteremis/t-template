$(document).ready(function () {

	console.log('' + isMagentaOffice());

	var sluzby = {
		'internet-1': ['OFFICE INTERNET 1', 'Maximálna rýchlosť do 40 Mb/s', 'Profesionálny router v cene', 'Minimálna agregácia'],
		'internet-2': ['OFFICE INTERNET 2', 'Maximálna rýchlosť do 300 Mb/s', 'Profesionálny router v cene', 'Minimálna agregácia'],
		'internet-m': ['MAGIO INTERNET M', 'Rýchlosť do 6/0,5 Mb/s', 'Bez časových a dátových obmedzení'],
		'internet-l': ['MAGIO INTERNET L', 'Rýchlosť do 40/4 Mb/s', 'Bez časových a dátových obmedzení'],
		'internet-x': ['MAGIO INTERNET XL', 'Rýchlosť do 300/30 Mb/s', 'Bez časových a dátových obmedzení'],
		'pevna-m': ['PEVNÁ LINKA M', 'NEOBMEDZENÉ VOLANIA<br>do pevných sietí v SR<br>a do mobilnej siete Telekom', '100 MINÚT<br>do ostatných mobilných<br>sietí v SR a do<br>Európskej únie'],
		'pevna-l': ['PEVNÁ LINKA L', 'NEOBMEDZENÉ VOLANIA<br>do všetkých pevných<br>a mobilných sietí v SR', '400 MINÚT<br>do Európskej únie'],
		'pevna-x': ['PEVNÁ LINKA XL', 'NEOBMEDZENÉ VOLANIA<br>do všetkých pevných <br>a mobilných sietí v SR', 'NEOBMEDZENÉ VOLANIA<br>do Európskej únie, USA a KANADY', '200 MINÚT<br>do sveta'],
		'tv-3': ['MAGIO TV BIZNIS', 'Výnimočná televízia<br>na komerčné účely', 'Jedinečný športový obsah', 'Aj pre IPTV a<br>satelitnú technológiu'],
		'tv-m': ['MAGIO TV M', '61 TV STANÍC', '3 x programový TV<br>balík podľa výberu', 'Mobilná TV Magio GO<br>zadarmo'],
		'tv-l': ['MAGIO TV L', '88 TV STANÍC', '6 x programový TV<br>balík podľa výberu', '1 x Extra balík L', 'Mobilná TV Magio GO<br>zadarmo'],
		'tv-x': ['MAGIO TV XL', '113 TV STANÍC', '6 x programový TV<br>balík podľa výberu', '1 x Extra balík L', '1 x Extra balík XL', 'Mobilná TV Magio GO<br>zadarmo'],

		'happy-xsmini': ['<br>HAPPY XS MINI',
			'neobmedzené prichádzajúce<br>roamingové volania<br>v susedných štátoch',
			'30 minút volania do všetkých sietí<br>v SR a do EÚ, odchádzajúce volania<br>zo susedných štátov do celej EÚ',
			'30 minút prichádzajúce<br>roamingové volania vo zvyšku EÚ', '', ''],

		'happy-xs': ['HAPPY XS',
			'neobmedzené volania do siete<br>Telekom mimo špičky a cez víkend',
			'neobmedzené prichádzajúce roamingové<br>volania v susedných štátoch',
			'50 minút volania do ostatných sietí<br>v SR a do EÚ odchádzajúce volania<br>zo susedných štátov do celej EÚ',
			'50 minút prichádzajúce<br>roamingové volania vo zvyšku EÚ', ''],

		'happy-s': ['HAPPY S',
			'neobmedzené volania do siete<br>Telekom a pevných sietí v SR',
			'neobmedzené prichádzajúce roamingové<br>volania v susedných štátoch',
			'100 minút volania do ostatných sietí<br>v SR a do EÚ odchádzajúce volania<br>zo susedných štátov do celej EÚ',
			'100 minút prichádzajúce roamingové<br>volania vo zvyšku EÚ', ''],

		'happy-m': ['<br>HAPPY M',
			'neobmedzené volania do siete<br>Telekom a pevných sietí v SR',
			'neobmedzené prichádzajúce roamingové<br>volania v susedných štátoch',
			'150 minút volania do ostatných sietí<br>v SR a do EÚ odchádzajúce volania<br>zo susedných štátov do celej EÚ',
			'150 minút prichádzajúce roamingové<br>volania vo zvyšku EÚ', ''],

		'happy-l': ['<br>HAPPY L',
			'neobmedzené volania do siete<br>Telekom a pevných sietí v SR',
			'neobmedzené prichádzajúce roamingové<br>volania v susedných štátoch',
			'250 minút volania do ostatných sietí<br>v SR a do EÚ odchádzajúce volania<br>zo susedných štátov do celej EÚ',
			'250 minút prichádzajúce roamingové<br>volania vo zvyšku EÚ',
			''],

		'happy-xl2': ['<br>HAPPY XL VOLANIA',
			'neobmedzené volania do<br>všetkých sietí v SR',
			'neobmedzené prichádzajúce roamingové<br>volania v susedných štátoch',
			'1000 minút odchádzajúce volania<br>zo SR a zo susedných štátov do EÚ',
			'1000 minút prichádzajúce<br>roamingové volania vo zvyšku EÚ',
			''],

		'happy-xl': ['<br>HAPPY XL',
			'neobmedzené volania<br>do všetkých sietí v SR',
			'neobmedzené prichádzajúce roamingové<br>volania v susedných štátoch',
			'1000 minút odchádzajúce volania zo SR a zo susedných štátov do EÚ',
			'1000 minút prichádzajúce<br>roamingové volania vo zvyšku EÚ',
			''],

		'happy-xxl': ['HAPPY XXL',
			'neobmedzené volania<br>do všetkých sietí v SR',
			'neobmedzené prichádzajúce roamingové<br>volania v susedných štátoch',
			'1000 minút odchádzajúce volania zo SR a zo susedných štátov do EÚ',
			'1000 minút prichádzajúce<br>roamingové volania vo zvyšku EÚ',
			'100 minút odchádzajúce<br>roamingové volania vo zvyšku EÚ'],

		'happy-profi': ['HAPPY PROFI',
			'neobmedzené volania<br>do všetkých sietí v SR',
			'neobmedzené prichádzajúce roamingové<br>volania v susedných štátoch',
			'1000 minút odchádzajúce volania zo SR a zo susedných štátov do EÚ a zóny Profi',
			'1000 minút prichádzajúce roamingové<br>volania vo zvyšku EÚ a zóne Profi',
			'200 minút odchádzajúce roamingové<br>volania vo zvyšku EÚ a zóne Profi'],

		'happy-easy': ['EASY KARTA', 'Easy karta bez viazanosti s<br>platnosťou kreditu 12 mes.', 'Volania za 0,09 € / min. a SMS <br>za 0,06 €,max. 50 centov / deň', '4G internet 0,10 € / 1 MB<br> s rýchlosťou 150 Mb/s, <br>max. 50 centov / deň', '', ''],

	};

	if (isMagentaOffice()) {
		sluzby['pevna-m'] = ['BIZNIS LINKA M', 'NEOBMEDZENÉ VOLANIA<br>do pevnej a <br>mobilnej siete Telekom', '200 MINÚT<br>do pevných a mobilných sietí<br>v SR, EÚ, USA a v Kanade'];
		sluzby['pevna-l'] = ['BIZNIS LINKA L', 'NEOBMEDZENÉ VOLANIA<br>do pevných a <br>mobilných sietí SR', '400 MINÚT<br>do pevných a mobilných sietí<br>v EÚ, USA a v Kanade'];
		sluzby['pevna-x'] = ['BIZNIS LINKA XL', 'NEOBMEDZENÉ VOLANIA<br>do pevných a <br>mobilných sietí SR', 'Neobmedzené volania<br>do pevných a mobilných sietí<br>v EÚ, USA a v Kanade', '400 MINÚT do zóny Svet'];
	}

	$("#skupina-check").toggleClass("checked");
	$(".skupina-mame").toggle();

	$('#skupina-check').on('click', function (event) {
		$(".skupina-mame").toggle();
		$("#skupina-check").toggleClass("checked");
	});

	$('.internet-main').on('click', function (event) {
		$(".internet-sub").toggle();
	});

	$('.televizia-main').on('click', function (event) {
		$(".televizia-sub").toggle();
	});

	$('.pevna-main').on('click', function (event) {
		$(".pevna-sub").toggle();
	});

	$('.happy-main').on('click', function (event) {
		$(".happy-sub").toggle();
	});

	$("#magenta-menu .master .not-interested").click(function () {
		$(".bublina").hide();

	});

	$("#magenta-menu .master .product-variant").click(function () {

		$(".bublina").hide();

		if ($(this).attr("data-variant")) {

			if ($(this).attr("data-variant") == 'o' || $(this).attr("data-variant") == 'oooo') return;

			var typID = $(this).closest('.master').attr("data-type");
			var pausalID = $(this).attr("data-variant").toLowerCase();

			var bublinaContent = '';
			var bublinaTyp = '';

			if (typID == 0) {
				bublinaTyp = 'internet';
			}

			if (typID == 1) {
				bublinaTyp = 'tv';
			}

			if (typID == 2) {
				bublinaTyp = 'pevna';
			}

			if (typID == 3) {
				pausalID = convertHappy3(pausalID);
				bublinaTyp = 'happy';
			}

			if (typID == 4) {
				pausalID = convertHappy4(pausalID);
				bublinaTyp = 'happy';
			}

			bublinaContent = poskladajBublinu(sluzby[bublinaTyp + '-' + pausalID]);

			$("#bublina-" + bublinaTyp).html(bublinaContent);
			$(".bublina-" + bublinaTyp).show();

			var bublinaClass = 'desc-' + bublinaTyp + '-' + pausalID;

			$("#bublina-" + bublinaTyp).removeClass(function (index, css) {
				return (css.match(/(^|\s)desc-\S+/g) || []).join(' ');
			});

			if (!$("#bublina-" + bublinaTyp).hasClass(bublinaClass)) {
				$("#bublina-" + bublinaTyp).addClass(bublinaClass);
			}

		}

	});


	function poskladajBublinu(content) {

		var html = '';

		$.each(content, function (index, value) {
			html += '<p>' + value + '</p>';
		});

		return html;
	}

	function convertHappy3(id) {
		var happy = '';

		switch (id) {
			case 'g':
				happy = 'm';
				break;

			case 'a':
				happy = 'l';
				break;

			case 'b':
				happy = 'xl2';
				break;

			case 'd':
				happy = 'xl';
				break;

			case 'e':
				happy = 'xxl';
				break;

			case 'f':
				happy = 'profi';
				break;

			default:
				happy = id;
		}

		return happy;
	}

	function convertHappy4(id) {
		var happy = '';

		switch (id) {
			case 'g':
				happy = 'xsmini';
				break;

			case 'i':
				happy = 'xs';
				break;

			case 'c':
				happy = 's';
				break;

			case 'j':
				happy = 'm';
				break;

			case 'a':
				happy = 'l';
				break;

			case 'b':
				happy = 'xl2';
				break;

			case 'd':
				happy = 'xl';
				break;

			case 'e':
				happy = 'xxl';
				break;

			case 'f':
				happy = 'profi';
				break;

			case 'h':
				happy = 'easy';
				break;

			default:
				happy = id;
		}

		return happy;
	}

});
