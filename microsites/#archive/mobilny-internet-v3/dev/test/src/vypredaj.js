$(document).ready(function () {

	$("#activate-vp1, #activate-vp2, #activate-vp3, #activate-vp4, #activate-vp5, #activate-vp6, #activate-vp7, #activate-vp8, #activate-vp9, #activate-vp10, #activate-vp11, #activate-vp12, #activate-vp13, #activate-vp14, #activate-vp15, #activate-vp16, #activate-vp17, #activate-vp18, #activate-vp19, #activate-vp20, #activate-vp21, #activate-vp22, #activate-vp22, #activate-vp23, #activate-vp24, #activate-vp25, #activate-vp26, #activate-vp27, #activate-vp28, #activate-vp29, #activate-vp30, #activate-vp31, #activate-vp32, #activate-vp33, #activate-vp34, #activate-vp35, #activate-vp36, #activate-vp37, #activate-vp38, #activate-vp39, #activate-vp40").fancybox({
		'hideOnContentClick': true
	});
    
    loadPhones();
    loadDetail();

});

function getParameterByName(name) {
	var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
	return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
}

function loadPhones(happy) {

	$("#product-holder").empty();

	$.ajax({
		type: "get",
		async: false,
		crossDomain: true,
		dataType: "json",
		data: "",
		//url: 'https://www.telekom.sk/documents/10179/506515/phones.json',
		url: 'https://m.telekom.sk/wb/vypredaj-tablet/phones.php',
		success: function (phones) {
			var html = '';
			var button_order = '';
			var button_vypr = '';
			var button_xsmini = '';
			var xsmini = 0;

			phones.sort(function (a, b) {
				return parseFloat(b.available) - parseFloat(a.available);
			});

			phones.sort(function (a, b) {
				return parseFloat(b.xsmini) - parseFloat(a.xsmini);
			});

			for (var a = 0; a < phones.length; a++) {

				xsmini = 0;

				var url = phones[a].url;

				if (happy===1) {
					url = phones[a].urlHappy;
				}

				button_order = '<div class="btn-order">' + '<a class="button button_lima" href="' + url + '"><span>KÚPIŤ</span></a>' + '</div>';
				button_vypr = '<div class="btn-order">' + '<div class="vypredane">VYPREDANÉ</div>' + '</div>';

				var xsminiDetailUrl = '/vypredaj-telefonov/detail?id=' + phones[a].id;
				button_xsmini = '<div class="btn-order">' + '<a class="button" href="' + xsminiDetailUrl + '"><span>VIAC INFO</span></a>' + '</div>';

				if (phones[a].xsmini == 1) {
					button_order = button_xsmini;
					xsmini = 1;
				}

				if (phones[a].available != 1) {
					button_order = button_vypr;
				}

				if (phones[a].visible == 1) {

					var price1 = phones[a].price1;
					var price2 = phones[a].price2;

					var priceMonth = phones[a].priceMonth;

					if (happy===1) {

						price1 = phones[a].priceHappy1;
						price2 = phones[a].priceHappy2;
						priceMonth = phones[a].priceMonthHappy;
					}

					html +=
						'<div class="phone">' +
						'<div class="images">';

					html +=
						'<div class="ec-holder-t"><img src="/documents/10179/4919806/ec-gift.png" alt="Darcek externa batéria"> </div>' +
						'<img class="img-phone" src="' + phones[a].image + '">' +
						'<div class="img-info"><a href="#' + phones[a].id + '" id="activate-' + phones[a].id + '"><img src="https://www.telekom.sk/documents/10179/462918/vypredaj_info.png"></a></div>' +
						'</div>' +
						'<h3>' + phones[a].name + '</h3>' +
						'<div class="desc">' + phones[a].description['desc1'] + '</div>' +
						'<div class="desc">' + phones[a].description['desc2'] + '</div>' +
						'<div class="desc">' + phones[a].description['desc3'] + '</div>';

					html += '<div class="price">' +
						'<div class="price1">' + price1 + '</div>' +
						'<div class="price2">' + price2 + '</div>' +
						'<div class="price-eur">€</div><br>' +
						' úvodná platba</div>';

					html += '<div class="price-border"></div>';

					html += '<div class="price-desc">' + priceMonth + ' € mesačný doplatok <br>' + phones[a].priceDesc + '</div>';

					html = html + button_order +
						'</div>';

					button_order = '';
				}

			}

			$(html).appendTo("#product-holder");
		},
		error: function () {
			//alert("Chyba pri posielani ... skus este raz!");
		}
	});
}

function loadDetail() {

	$("#detail").empty();

	$.ajax({
		type: "get",
		async: false,
		crossDomain: true,
		dataType: "json",
		data: "",
		//url: 'https://www.telekom.sk/documents/10179/506515/detail.json',
		url: 'https://m.telekom.sk/wb/vypredaj-tablet/detail.php',
		success: function (phones) {
			var html = "";
			for (var a = 0; a < phones.length; a++) {

				xsmini = 0;

				if (typeof phones[a].xsminiOne !== 'undefined') {
					xsmini = 1;
				}

				html +=
					'<div class="detail-lightbox" id="' + phones[a].id + '">' +
					'<div class="detail-header">' +
					'<h2>' + phones[a].name + '</h2>' +
					'</div>' +
					'<div class="detail-desc">' +
					'<div class="images-detail">';

				html += '<div class="detail-img"><img src="' + phones[a].image + '"></div>' +
					'</div>' +
					'<div class="detail-text">' +
					'<table class="detail-table">';

				var detail = phones[a].description;

				for (var b = 0; b < detail.length; b++) {
					html +=
						'<tr>' +
						'<td><div class="odrazka"></div></td>' +
						'<td>' + detail[b].desc[0] + '</td>' +
						'<td class="row-2">' + detail[b].desc[1] + '</td>' +
						'</tr>';
				}
				html +=
					'</table>' +
					'</div>' +
					'</div>' +
					'</div>';
			}

			$(html).appendTo("#detail");
		},
		error: function () {
			//alert("Chyba pri posielani ... skus este raz!");
		}
	});
}