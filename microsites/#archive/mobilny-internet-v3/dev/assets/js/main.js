$(function () {
    $(".fancy").fancybox();

    $('.fancybox-media').fancybox({
        openEffect: 'none',
        closeEffect: 'none',
        helpers: {
            media: {}
        }
    });

    $(document).on('click', '.popup-modal-dismiss', function(e) {
        e.preventDefault();
        $.fancybox.close(); 
    });

    $(document).on('click', '.popup-modal-scroll', function(e) {
        e.preventDefault();
        $.fancybox.close(); 
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top - ($("#nav-sticky").outerHeight())
                }, 1000);
                return false;
            }
        }
    }); 
    
    $("#nav-sticky").sticky({
        topSpacing: 0,
        widthFromWrapper: true
    });

    $(".scroll-to").click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top - ($("#nav-sticky").outerHeight())
                }, 1000);
                return false;
            }
        }
    });

    var contentSections = $("[id*='sec-']"),
        secondaryNav = $("#nav-sticky");

    function updateSecondaryNavigation() {
        contentSections.each(function () {
            var actual = $(this),
                actualHeight = actual.height() + parseInt(actual.css('paddingTop').replace('px', '')) + parseInt(actual.css('paddingBottom').replace('px', '')),
                actualAnchor = secondaryNav.find('a[href="#' + actual.attr('id') + '"]');

            if ((actual.offset().top - secondaryNav.outerHeight() <= $(window).scrollTop()) && (actual.offset().top + actualHeight - secondaryNav.outerHeight() > $(window).scrollTop())) {
                actualAnchor.addClass('active');
            } else {
                actualAnchor.removeClass('active');
            }

        });
    }
    updateSecondaryNavigation();

    $(window).scroll(function (event) {
        updateSecondaryNavigation();
    });

    $('.info-1').qtip({
        content: {
            text: 'Pri programoch Mobilný internet S/M/L sa po vyčerpaní objemu dát 1 GB/5 GB/15 GB uplatňuje obmedzenie rýchlosti dátových prenosov max. na 64 kb/s.'
        }
    });
    $('.info-tablet').qtip({
        content: {
            text: 'Vybrané paušály sú:<br> Happy M, L, XL volania, XL, XXL a Profi'
        }
    });

    $('#product-holder').owlCarousel({
        dots: false,
        nav: true,
        navText: ["", ""],
        responsive: {
            // breakpoint from 0 up
            0: {
                items: 1
            },
            // breakpoint from 480 up
            600: {
                items: 2
            },
            // breakpoint from 768 up
            768: {
                items: 3
            }
        }
    });
    
    var slider2 = $('#sliderbox').bxSlider({
        useCSS: false,
        autoReload: true,
        captions: false,
        pager: false,
        infiniteLoop: false,
        slideWidth: 215,
        slideMargin: 5,
        controls: false,
        maxSlides: 4,
        breaks: [{
            screen: 0,
            slides: 1,
            pager: false
        }, {
            screen: 500,
            slides: 2,
            pager: false,
            controls: false
        }, {
            screen: 768,
            slides: 3,
            pager: false,
            controls: false
        }]
    });



    $('#box-prev').click(function () {
        slider2.goToPrevSlide();
        return false;
    });

    $('#box-next').click(function () {
        slider2.goToNextSlide();
        return false;
    });

    $(".order-wear a").click(function () {
        $('html, body').animate({
            scrollTop: $(".c-row-2").offset().top
        }, 1000);
    });

    $("a#_1, a#_2, a#_3, a#_4, a#_5").fancybox({
        'hideOnContentClick': true,
        'maxWidth': 800
    });


	$('#neobmedzene-data .zavri').click(function(){
		$.fancybox.close();
		return false;
	});

	$(".lightbox").fancybox({
		// autoDimensions: true
		'width'  : 430,
		'height'  : 460,
		'autoSize': false
	});


	$('#magenta1-select').on('change', function() {
		if (this.value==1) {
			$('#magenta1-1').show();
			$('#magenta1-0').hide();
		} else {
			$('#magenta1-0').show();
			$('#magenta1-1').hide();
		}

	});

	$('#zasady-tooltip').qtip({
		content: {
			text: 'Vďaka Zásadám férového používania (anglická skratka FUP) zabezpečujeme rovnakú kvalitu ' +
			'všetkým zákazníkom a predchádzame tomu, aby jeden zákazník čerpal nadmerne dáta na úkor ostatných. <br>' +
			'Preto sú naše neobmedzené dáta v objeme 100 GB. ' +
			'Po ich prečerpaní neplatíte nič navyše, ale prenos dát sa spomalí.'
		},
		position: {
			my: 'left center',
			at: 'right center'
		},
		style: {
			classes: 'tooltip tooltip-left'
		}
	});


});
