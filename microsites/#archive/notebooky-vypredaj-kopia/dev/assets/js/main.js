$(function() {
    $('.popup').fancybox({
        padding: 11,
        margin: 0,
        closeBtn: false,
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.7)'
                }
            }
        }
    });

    $('.p-close').on('click', function(e) {
        e.preventDefault();
        $.fancybox.close();
    });


    $('.info-1').qtip({
        content: {
            text: 'Ak vyčerpáte predplatené dáta, za ďalšie neplatíte nič navyše. Obmedzí sa len rýchlosť dátových prenosov na max. 64 kb/s.'
        }
    });


    // gallery carousel
    $('.main-gallery').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        centerMode: true,
        asNavFor: '.slider-nav'
    });
    $('.slider-nav').slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        asNavFor: '.main-gallery',
        dots: false,
        centerMode: false,
        focusOnSelect: true,
        arrows: false,
        responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 5,
                slidesToScroll: 3,
                infinite: true,
                dots: false
            }
        }, {
            breakpoint: 400,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1
            }
        }, ]
    });
    $('.main-gallery2').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        centerMode: true,
        asNavFor: '.slider-nav2'
    });
    $('.slider-nav2').slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        asNavFor: '.main-gallery2',
        dots: false,
        centerMode: false,
        focusOnSelect: true,
        arrows: false,
        responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 5,
                slidesToScroll: 3,
                infinite: true,
                dots: false
            }
        }, {
            breakpoint: 400,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1
            }
        }, ]
    });
    $('.main-gallery3').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        centerMode: true,
        asNavFor: '.slider-nav3'
    });
    $('.slider-nav3').slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        asNavFor: '.main-gallery3',
        dots: false,
        centerMode: false,
        focusOnSelect: true,
        arrows: false,
        responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 5,
                slidesToScroll: 3,
                infinite: true,
                dots: false
            }
        }, {
            breakpoint: 400,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1
            }
        }, ]
    });

    $('.main-gallery4').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        centerMode: true,
        asNavFor: '.slider-nav4'
    });
    $('.slider-nav4').slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        asNavFor: '.main-gallery4',
        dots: false,
        centerMode: false,
        focusOnSelect: true,
        arrows: false,
        responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 5,
                slidesToScroll: 3,
                infinite: true,
                dots: false
            }
        }, {
            breakpoint: 400,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1
            }
        }, ]
    });
    //set active class to first slide
    $('.slider-nav .slick-slide').eq(0).addClass('slick-active');

     //slide DownUp Configuration
    // toggle link in specs
    $(".toggle-info").click(function() {
        //toggle upDown icon
        $(this).toggleClass('t-ico-down t-ico-up');

        if ($(".toggle-content").hasClass("height-transition-hidden"))
            $(".toggle-content").slideDownTransition();
        else
            $(".toggle-content").slideUpTransition();

    });
});


// slideDown-Up plugin
(function($) {

    $.fn.slideUpTransition = function() {
        return this.each(function() {
            var $el = $(this);
            $el.css("max-height", "0");
            $el.addClass("height-transition-hidden");

        });
    };

    $.fn.slideDownTransition = function() {
        return this.each(function() {
            var $el = $(this);
            $el.removeClass("height-transition-hidden");

            // temporarily make visible to get the size
            $el.css("max-height", "none");
            var height = $el.outerHeight();

            // reset to 0 then animate with small delay
            $el.css("max-height", "0");

            setTimeout(function() {
                $el.css({
                    "max-height": height
                });
            }, 1);
        });
    };

    /* TOOLTIP */
        $('.office-tooltip').qtip({
            content: {
                text: '■ pre 3 zariadenia v rodine: počítač, tablet aj telefón<br />' +
                      '■ 1 TB ukladacieho priestoru OneDrive<br />' +
                      '■ 60 minút na Skype volania do celého sveta mesačne',
            },
            position: {
                my: 'bottom center',
                at: 'top center'
            },
            style: { classes: 'tooltip' }
        });
})(jQuery);
