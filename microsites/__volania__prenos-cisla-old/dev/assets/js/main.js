$(function() {
    /* ::: Scroll To ::: */
    $(".scroll-to").click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top - ($("#nav-sticky").outerHeight())
                }, 1000);
                return false;
            }
        }
    });
    /* ::: Sticky Nav-ext :::
    $("#nav-sticky").sticky({
        topSpacing: 0,
        widthFromWrapper: true
    });

    var contentSections = $("[id*='sec-']"),
        secondaryNav = $("#nav-sticky");

    function updateSecondaryNavigation() {
        contentSections.each(function() {
            var actual = $(this),
                actualHeight = actual.height() + parseInt(actual.css('paddingTop').replace('px', '')) + parseInt(actual.css('paddingBottom').replace('px', '')),
                actualAnchor = secondaryNav.find('a[href="#' + actual.attr('id') + '"]');

            if ((actual.offset().top - secondaryNav.outerHeight() <= $(window).scrollTop()) && (actual.offset().top + actualHeight - secondaryNav.outerHeight() > $(window).scrollTop())) {
                actualAnchor.addClass('active');
            } else {
                actualAnchor.removeClass('active');
            }

        });
    }

    updateSecondaryNavigation();
    $(window).scroll(function(event) {
        updateSecondaryNavigation();
    });*/
});


$(function() {
    /* ::: Slider Oblubene ::: */
    $("#program_slider").slick({
        slidesToShow: 2,
        slidesToScroll: 1,
        centerMode: false,
        infinite: false,
        swipeToSlide: true,
        swipe: true,
        arrows: true,
        dots: false,
        autoplay: false,
        initialSlide: 2,
        autoplaySpeed: 5000,
        touchMove: false,
        draggable: false,
        customPaging: function(slider, i) {
            return '<span class="slide-dot">&nbsp;</span>';
        },
        responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
                arrows: true,
                dots: true
            }
        }, {
            breakpoint: 768,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: true,
                infinite: false,
                dots: true
            }
        }]
    });
    /* ::: Slider Zakladne ::: */
    $("#zakladne_slider").slick({
        slidesToShow: 2,
        slidesToScroll: 1,
        centerMode: false,
        infinite: false,
        swipeToSlide: true,
        swipe: true,
        arrows: true,
        dots: false,
        autoplay: false,
        // initialSlide: 1,
        autoplaySpeed: 5000,
        touchMove: false,
        draggable: false,
        customPaging: function(slider, i) {
            return '<span class="slide-dot">&nbsp;</span>';
        },
        responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
                arrows: true,
                dots: true
            }
        }, {
            breakpoint: 768,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: true,
                infinite: false,
                dots: true
            }
        }]
    });
    /* ::: Slider Prenarocnych ::: */
    $("#prenarocnych_slider").slick({
        slidesToShow: 2,
        slidesToScroll: 1,
        centerMode: false,
        infinite: false,
        swipeToSlide: true,
        swipe: true,
        arrows: true,
        dots: true,
        autoplay: false,
        draggable: false,
        autoplaySpeed: 5000,
        customPaging: function(slider, i) {
            return '<span class="slide-dot">&nbsp;</span>';
        },
        responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
                arrows: true,
                dots: true
            }
        }, {
            breakpoint: 768,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: true,
                infinite: false,
                dots: true
            }
        }]
    });

    /* ::: PopUp Conf ::: */
    $('.p-close').on('click', function(e) {
        e.preventDefault();
        $.fancybox.close();
    });
    // popup close with scroll to section
    $(document).on('click', '.popup-modal-dismiss', function(e) {
        e.preventDefault();
        $.fancybox.close();
    });

    $(document).on('click', '.popup-modal-scroll', function(e) {
        e.preventDefault();
        $.fancybox.close();
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top - ($("#nav-sticky").outerHeight())
                }, 1000);
                return false;
            }
        }
    });

    /* ::: PopUp Data ::: */
    $('.popup').fancybox({
        padding: 11,
        margin: 0,
        closeBtn: false,
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.7)'
                }
            }
        }
    });

    $("a#activate-order100").fancybox({
        padding: 11,
        margin: 0,
        closeBtn: false,
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.7)'
                }
            }
        }
    });

    $("#activate-order500").fancybox({
        padding: 11,
        margin: 0,
        closeBtn: false,
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.7)'
                }
            }
        }
    });

    $("#activate-order1000").fancybox({
        padding: 11,
        margin: 0,
        closeBtn: false,
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.7)'
                }
            }
        }
    });

    $("a#info-order1000a").fancybox({
        padding: 11,
        margin: 0,
        closeBtn: false,
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.7)'
                }
            }
        }
    });

    $("#info-order100").fancybox({
        padding: 11,
        margin: 0,
        closeBtn: false,
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.7)'
                }
            }
        }
    });

    $("#info-order500").fancybox({
        padding: 11,
        margin: 0,
        closeBtn: false,
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.7)'
                }
            }
        }
    });

    $("#info-order1000").fancybox({
        padding: 11,
        margin: 0,
        closeBtn: false,
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.7)'
                }
            }
        }
    });

    $("#activate-data100").fancybox({
        padding: 11,
        margin: 0,
        closeBtn: false,
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.7)'
                }
            }
        }
    });

    $("#activate-data500").fancybox({
        padding: 11,
        margin: 0,
        closeBtn: false,
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.7)'
                }
            }
        }
    });

    $("#activate-lightbox-bz").fancybox({
        padding: 11,
        margin: 0,
        closeBtn: false,
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.7)'
                }
            }
        }
    });

    $("#activate-lightbox-zd").fancybox({
        padding: 11,
        margin: 0,
        closeBtn: false,
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.7)'
                }
            }
        }
    });



    /* ::: ToolTips ::: */
    var generateTooltip = function(element, text) {
        $(element).qtip({
            content: {
                text: text
            },
            position: {
                my: 'bottom center',
                at: 'top center'
            }
        });
    }

    $('.pausaly-info').qtip({
        content: {
            text: 'Vybrané paušály, pre ktoré platí akcia: Happy M, L, XL volania, L, XL, XXL a Profi.'
        },
        position: {
            my: 'left center',
            at: 'right center'
        }
    });

    $('.clip-info').qtip({
        content: 'So službou CLIP uvidíte na displeji svojho mobilu telefónne číslo volajúceho, pokiaľ nemá aktivovanú službu CLIR (zamedzenie zobrazenia telefónneho čísla). Ak máte uložené telefónne číslo volajúceho v pamäti, uvidíte jeho meno.',
        position: {
            corner: {
                target: 'leftMiddle',
                tooltip: 'topMiddle'
            },
            position: {
                my: 'bottom center',
                at: 'top center'
            }
        }
    }).bind('click', function(event) {
        event.preventDefault();
        return false;
    });

    $('.ec-beta').qtip({
        content: {
            text: 'Služba immmr je zatiaľ v pilotnej prevádzke. Pomôžte nám ju zlepšiť. Svoje podnety posielajte na <a href="mailto:data@telekom.sk">data@telekom.sk</a>'
        },
        hide: {
            event: 'unfocus'
        },
        position: {
            my: 'left center',
            at: 'right center'
        }
    });

    $('.roaming-info').qtip({
        content: 'Susedné štáty: Česko, Maďarsko, Poľsko a Rakúsko.',
        position: {
            corner: {
                target: 'leftMiddle',
                tooltip: 'topMiddle'
            }
        }
    }).bind('click', function(event) {
        event.preventDefault();
        return false;
    });


    $('.tooltip').qtip({
        content: {
            text: 'Vybrané paušály sú:<br> Happy M, L, XL volania, XL, XXL a Profi'
        },
        position: {
            my: 'bottom center',
            at: 'top center'
        }
    });

    $('.akcie-info').qtip({
        content: '<div class="tag-left">Novým zákazníkom objem dát navýšime pri aktivácii paušálu a verným od najbližšieho zúčtovacieho obdobia po 15. 3. 2017, najneskôr však do 9.4.2017.</div>',
        position: {
            corner: {
                target: 'leftMiddle',
                tooltip: 'topMiddle'
            }
        }
    }).bind('click', function(event) {
        event.preventDefault();
        return false;
    });


    $('.happy_xs-info').qtip({
        content: 'Počas pracovných dní v čase od 19.00 hod. do 7.00 hod. Soboty, nedele a štátne sviatky počas celých 24 hodín.',
        position: {
            corner: {
                target: 'leftMiddle',
                tooltip: 'topMiddle'
            }
        }
    }).bind('click', function(event) {
        event.preventDefault();
        return false;
    });

    $('#info-balik-volania, #info-balik-minuty, #info-balik-sms').qtip({
        content: 'Balík aktivujete na webe počas kúpy Happy paušálu.',
        position: {
            corner: {
                target: 'leftMiddle',
                tooltip: 'topMiddle'
            }
        }
    }).bind('click', function(event) {
        event.preventDefault();
        return false;
    });

    /* ::: Order Form for Mobile*/
    var orderUrl = '/mam-zaujem/volania/happy/formular?pausal=';

    $('#order-happy-xs-mini').click(function(event) {
        /*event.preventDefault();*/
        if (window.matchMedia('(max-width: 600px)').matches) {
            $(this).attr("href", orderUrl + 'Happy XS mini');
        } else {}
    });

    $('#order-happy-xs').click(function(event) {
        /*event.preventDefault();*/
        if (window.matchMedia('(max-width: 600px)').matches) {
            $(this).attr("href", orderUrl + 'Happy XS');
        } else {}
    });

    $('#order-happy-s').click(function(event) {
        /*event.preventDefault();*/
        if (window.matchMedia('(max-width: 600px)').matches) {
            $(this).attr("href", orderUrl + 'Happy S');
        } else {}
    });

    $('#order-happy-m').click(function(event) {
        /*event.preventDefault();*/
        if (window.matchMedia('(max-width: 600px)').matches) {
            $(this).attr("href", orderUrl + 'Happy M');
        } else {}
    });

    $('#order-happy-l').click(function(event) {
        /*event.preventDefault();*/
        if (window.matchMedia('(max-width: 600px)').matches) {
            $(this).attr("href", orderUrl + 'Happy L');
        } else {}

    });

    $('#order-happy-xl-call').click(function(event) {
        /*event.preventDefault();*/
        if (window.matchMedia('(max-width: 600px)').matches) {
            $(this).attr("href", orderUrl + 'Happy XL Volania');
        } else {}

    });

    $('#order-happy-xl').click(function(event) {
        /*event.preventDefault();*/
        if (window.matchMedia('(max-width: 600px)').matches) {
            $(this).attr("href", orderUrl + 'Happy XL');
        } else {}

    });

    $('#order-happy-xxl').click(function(event) {
        /*event.preventDefault();*/
        if (window.matchMedia('(max-width: 600px)').matches) {
            $(this).attr("href", orderUrl + 'Happy XXL');
        } else {}

    });

    $('#order-happy-profi').click(function(event) {
        /*event.preventDefault();*/
        if (window.matchMedia('(max-width: 600px)').matches) {
            $(this).attr("href", orderUrl + 'Happy Profi');
        } else {}

    });

});

/* SECONDARY RADIO BTNS */
//default magenta color for radio box
var defaultCheckboxColor = $('input[name=rb-pair]:checked', '#secondaryRadio').attr('id');
$('.secondaryRadio').find('label[for='+  defaultCheckboxColor  +']').css({'background-color':'#e20074',
              'color':'#fff'});

//on click set magenta color to selected radio box and make other radio boxes transparent
$(".secondaryRadio input").click( function(){
  var getAttr = $(this).attr('id');
  var label = $('.secondaryRadio').find('label[for='+  getAttr  +']');
  var allCheckboxes = $(".secondaryRadio label").css({'background-color':'transparent',
'color':'#000'})

   if( $(this).is(':checked')) {
     $(label).css({'background-color':'#e20074',
                   'color':'#fff'});
   }

   //If radiobtn and div IDs match, show div.
   var btnID = $(this).attr('id');
   //console.log(btnID);
   var checkedDiv = $('.detailHolder[id='+btnID+']');
   //console.log(checkedDiv);
   var divID = $('.detailHolder').attr('id');
   var objLength = $(this).length;
   //console.log(objLength);
   if (btnID = divID && objLength > 0) {
     //console.log("pravda");
     //console.log(checkedDiv);
     $('.detailHolder').hide();
     $(checkedDiv).show();
   }
});
/* SECONDARY RADIO BTNS END */

/* URL FOR ADS TARGETING */
if (window.location.href == window.location.origin + "/volania/prenos-cisla?bonus-za-prenos") {
  var checkbox1unchecked = $('input[id="check-1"]').prop('checked', false);
  $('.detailHolder[id="check-1"]').hide();
  var checkbox2checked = $('input[id="check-2"]').prop('checked', true);
  $('.detailHolder[id="check-2"]').show();

  var colorForUnchecked = $('input[name=rb-pair]', '#secondaryRadio').not(':checked').attr('id');
  $('.secondaryRadio').find('label[for='+  colorForUnchecked  +']').css({'background-color':'transparent',
                'color':'#000'});
  var colorForChecked = $('input[name=rb-pair]:checked', '#secondaryRadio').attr('id');
  $('.secondaryRadio').find('label[for='+  colorForChecked  +']').css({'background-color':'#e20074',
                'color':'#fff'});
}
/* URL FOR ADS TARGETING */

/* SCROLL TO #ID BASED ON URL */
if (window.location.href == window.location.origin + "/volania/prenos-cisla?data-navyse" || window.location.href == window.location.origin + "/volania/prenos-cisla?bonus-za-prenos" ) {
  $(document).ready(function(e) {
    $('html, body').animate({
      scrollTop: $("#sec-mkr").offset().top + 'px'
    }, 1000);
  });
}
/* SCROLL TO #ID BASED ON URL */

/* TOGGLE ARROW */
$('.toggle-arrow').click(function(event) {
    event.preventDefault();
  $(event.target).next('.arrow-content').toggle(200);
  $(this).find('.arrow-right').toggleClass('arrow-rotate');
});
/* TOGGLE ARROW END */
