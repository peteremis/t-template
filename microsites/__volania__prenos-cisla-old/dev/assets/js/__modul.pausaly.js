var plansModule = (function() {

    var planArr = [];
    var PLANJSONURL = '../../../assets/js/plans.json';
    var activePlan = 'planWithMob';
    var planRowsPC = '';
    var planRowsMob = '';
    var selectedMobTableHead = 1;

    var getDataFromJSON = {
        getData: function() {
            $.ajax({
                type: 'GET',
                url: PLANJSONURL,

                success: function(response) {
                    //console.log(response);
                    objMethods.assignArrToLocalArr(response);
                    processResponse.addRows(planArr);
                    processResponse.addRowsMob(planArr);
                    displayDOMElements.displayPlanRows();
                    DOMElementsMethods.showAllObjCount();
                    DOMElementsMethods.showFilteredObjCount(planArr);

                    $('.plan-show-detail').live('click', function (e) {
                        e.preventDefault();
                        DOMElementsMethods.showRowDet(this);
                    });

                    $('.plm__col .plan-show-detail').live('click', function(e) {
                        e.preventDefault();
                        DOMElementsMethods.fillMobDetailWithObj(objMethods.getObjById(DOMElementsMethods.getDataAttrFromRow(this)));
                    });

                    $(".plm__col .plan-show-detail").live('click', function() {
                        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                            var target = $(this.hash);
                            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                            if (target.length) {
                                $('html,body').animate({
                                    scrollTop: target.offset().top
                                }, 1000, function() {
                                    DOMElementsMethods.showMobileDetail();
                                });
                                return false;
                            }
                        }
                    });

                    $('#plm__plan-back').live('click', function(e) {
                        e.preventDefault();
                        DOMElementsMethods.hideMobileDetail();
                    });

                    $('.plan-hide-detail').live('click', function (e) {
                        e.preventDefault();
                        DOMElementsMethods.hideRowDet(this);
                    });

                    $('.plm__head-item').on('click', function(e) {
                        e.preventDefault();
                        DOMElementsMethods.toggleMobTableHead(this);
                        processResponse.addRowsMob(planArr);
                        processResponse.addRowsMob(objMethods.returnFilteredArr());
                        displayDOMElements.displayPlanRows();
                    });

                    $('.plph__ico').on('click', function(e) {
                        e.preventDefault();

                        var $this = $(this);

                        if ($this.attr('id') === 'sort-by-price') {
                            if ($this.hasClass('unset')) {
                                processResponse.addRows(planArr.sort(objMethods.returnSortBasedOnActivePlan($this, 'asc')));
                                DOMElementsMethods.setSortingHeadTo($this, 'asc');
                            } else if ($this.hasClass('asc')) {
                                processResponse.addRows(planArr.sort(objMethods.returnSortBasedOnActivePlan($this, 'desc')));
                                DOMElementsMethods.setSortingHeadTo($this, 'desc');
                            } else {
                                processResponse.addRows(planArr.sort(objMethods.returnSortBasedOnActivePlan($this, 'asc')));
                                DOMElementsMethods.setSortingHeadTo($this, 'asc');
                            }
                        } else {
                            if ($this.hasClass('unset')) {
                                processResponse.addRows(planArr.sort(objMethods.returnSortBasedOnActivePlan($this, 'asc')));
                                DOMElementsMethods.setSortingHeadTo($this, 'asc');
                            } else if ($this.hasClass('asc')) {
                                processResponse.addRows(planArr.sort(objMethods.returnSortBasedOnActivePlan($this, 'desc')));
                                DOMElementsMethods.setSortingHeadTo($this, 'desc');
                            } else {
                                processResponse.addRows(planArr.sort(objMethods.returnSortBasedOnActivePlan($this, 'asc')));
                                DOMElementsMethods.setSortingHeadTo($this, 'asc');
                            }
                        }
                        processResponse.addRows(objMethods.returnFilteredArr());
                        displayDOMElements.displayPlanRows();
                        DOMElementsMethods.resetSortingHeadIco($this);
                    })

                    $('.ps__btn-holder .btn').on('click', function(e) {
                        e.preventDefault();
                        DOMElementsMethods.togglePlanSwitcher(this);
                        processResponse.addRows(planArr);
                        processResponse.addRows(objMethods.returnFilteredArr());
                        processResponse.addRowsMob(planArr);
                        processResponse.addRowsMob(objMethods.returnFilteredArr());
                        displayDOMElements.displayPlanRows();
                        DOMElementsMethods.resetAllSortingHeadIcos();
                    });

                    $('.filter-checkbox').on('click', function() {
                        processResponse.addRows(objMethods.returnFilteredArr());
                        processResponse.addRowsMob(objMethods.returnFilteredArr());
                        displayDOMElements.displayPlanRows();
                        DOMElementsMethods.showAllObjCount();
                    });

                },
                error: function(xhr, textStatus, errorThrown) {
                }
            });
        }
    }

    var processResponse = {
        addRows: function(arr) {
            planRowsPC = '';
            for(var key in arr) {
                planRowsPC += generateDOM.generateRow(arr[key]);
            }
        },
        addRowsMob: function(arr) {
            planRowsMob = '';
            for(var key in arr) {
                planRowsMob += generateDOM.generateMobRow(arr[key]);
            }
        }
    }

    var sorters = {
        byPlanMainPriceAsc: function(a,b) {
            return (a.planMainPrice - b.planMainPrice);
        },
        byPlanMainPriceDesc: function(a,b) {
            return (b.planMainPrice - a.planMainPrice);
        },
        byplanMainPriceNoMobAsc: function(a,b) {
            return (a.planMainPriceNoMob - b.planMainPriceNoMob);
        },
        byplanMainPriceNoMobDesc: function(a,b) {
            return (b.planMainPriceNoMob - a.planMainPriceNoMob);
        },
        byDataAsc: function(a,b) {
            return (a.dataSorting - b.dataSorting);
        },
        byDataDesc: function(a,b) {
            return (b.dataSorting - a.dataSorting);
        }
    };

    var objMethods = {
        assignArrToLocalArr: function(arr) {
            planArr = arr;
        },
        returnSortBasedOnActivePlan: function(elem, sortType) {
            var $elem = $(elem);

            if ($elem.attr('id') === 'sort-by-price') {
                if (activePlan === 'planWithMob') {
                    if (sortType === 'asc') {
                        return sorters.byPlanMainPriceAsc
                    } else return sorters.byPlanMainPriceDesc
                } else {
                    if (sortType === 'asc') {
                        return sorters.byplanMainPriceNoMobAsc
                    } else return sorters.byplanMainPriceNoMobDesc
                }
            } else if ($elem.attr('id') === 'sort-by-data') {
                if (sortType === 'asc') {
                    return sorters.byDataAsc
                } else return sorters.byDataDesc
            }
        },
        findOne: function (haystack, arr) {
            return arr.some(function (v) {
                return haystack.indexOf(v) >= 0;
            });
        },
        returnFilteredArr: function() {
            var selectedIDs = [];
            var tempArr = [];

            $('.filter-checkbox:checked').each(function() {
                selectedIDs.push(this.id);

            });

            planArr.filter(function(e) {
                if (objMethods.findOne(e.planCategory, selectedIDs)){
                    tempArr.push(e);
                }
            });

            /*
            if (tempArr.length === 0) {
                tempArr = planArr;
            }
            */

            DOMElementsMethods.showFilteredObjCount(tempArr);

            return tempArr;
        },
        returnObjCount: function(obj) {
            return obj.length;
        },
        getObjById: function(id) {
            for (var obj in planArr) {
                if (planArr[obj].id === id) {
                    return planArr[obj];
                }
            }
        }
    }

    var generateDOM = {
        showSmsOverviewDetIfAvailable: function(val) {
           if (val === false) {
               return ''
           } else {
             return '<p class="plan-sms-det">'+ val +'</p>'
           }
        },
        showsmsOverviewDetOnlyMobIfAvailable: function(val) {
           if (val === false) {
               return ''
           } else {
             return '<p class="plan-sms-det mg-b-10">'+ val +'</p>'
           }
        },
        addRibbonClassIfAvailable: function(val) {
            if (val === false) {
                return '<div class="plp__plan row">'
            } else {
                return '<div class="plp__plan row plp__plan_w-ribbon">'
            }
        },
        showRibbonIfAvailable: function(val) {
            if (val === false) {
                return ''
            } else {
                return '<p class="plp__plan-ribbon">'+ val +'</p>'
            }
        },
        showSMSSaleIfAvailable: function(val) {
            if (activePlan === 'planWithMob') {
                if (val !== false) {
                    return '<p class="plan-sale">'+ val +'</p>'
                } else return ''
            } else return ''
        },
        showPriceAccordingPlanType: function(obj) {
            if (activePlan === 'planWithMob') {
                return obj.planMainPrice
            } else return obj.planMainPriceNoMob
        },
        generateRow: function(obj) {
            return this.addRibbonClassIfAvailable(obj.ribbon)+
                this.showRibbonIfAvailable(obj.ribbon) +
                '<div class="plpp-inner plpp__overview">' +
                '<div class="plc__col plc__col-1">' +
                '<p class="plan-price">'+ helperMethods.numberWithCommas(this.showPriceAccordingPlanType(obj)) +' €</p>' +
                '<p class="plan-name">'+ obj.planName +'</p>' +
                this.showSMSSaleIfAvailable(obj.webSale) +
                '</div>' +
                '<div class="plc__col plc__col-2">' +
                '<p class="plan-data">'+ obj.data +' '+ obj.dataLabel +'</p>' +
                '<p class="plan-data-location">'+ obj.dataLocation +'</p>' +
                '</div>' +
                '<div class="plc__col plc__col-3">' +
                '<div class="col-md-6">' +
                '<p class="plan-call-tit plan-call-tit-1">'+ obj.callOverviewTitle1 +'</p>' +
                '<p class="plan-call-det plan-call-det-1">'+ obj.callOverviewDet1 +'</p>' +
                '</div>' +
                '<div class="col-md-6">' +
                '<p class="plan-call-tit plan-call-tit-2">'+ obj.callOverviewTitle2 +'</p>' +
                '<p class="plan-call-det plan-call-det-2">'+ obj.callOverviewDet2 +'</p>' +
                '</div>' +
                '</div>' +
                '<div class="plc__col plc__col-4">' +
                '<p class="plan-sms-tit">'+ obj.smsOverviewTitle +'</p>' +
                this.showSmsOverviewDetIfAvailable(obj.smsOverviewDet) +
                '</div>' +
                '<div class="plc__col plc__col-5">' +
                '<a href="#" class="plan-show-detail">Zobraziť detail</a>' +
                '<a href="'+obj.chooseBtnLink+'" class="btn cta plan-choose-btn"><span class="ico-basket"></span>VYBRAŤ</a>' +
                '</div>' +
                '</div>' +
                '<div class="plpp-inner plpp__detail hidden">' +
                '<div class="plc__col plc__col-1">' +
                '<p class="plan-price">'+ helperMethods.numberWithCommas(obj.planMainPrice) +' €</p>' +
                '<p class="plan-price plan-price-no-mob hidden">'+ helperMethods.numberWithCommas(obj.planMainPriceNoMob) +' €</p>' +
                '<p class="plan-name">'+ obj.planName +'</p>' +
                this.showSMSSaleIfAvailable(obj.webSale) +
                '</div>' +
                '<div class="plc__col plc__col-2">' +
                '<p class="plan-data">'+ obj.data +' '+ obj.dataLabel +'</p>' +
                '<p class="plan-data-location">'+ obj.dataLocation +'</p>' +
                '</div>' +
                '<div class="plc__col plc__col-3">' +
                '<p class="plan-call__header plan-call__header_outgoing">Odchádzajúce volania</p>' +
                '<div class="plan-call-col-holder plan-call-col-holder_outgoing">' +
                '<div class="col-md-6">' +
                '<p class="plan-call-tit plan-call-tit_more plan-call-tit_more-1">'+ obj.callOutgoingTitle1 +'</p>' +
                '<p class="plan-call-det plan-call-det_more plan-call-det_more-1">'+ obj.callOutgoingDet1 +'</p>' +
                '</div>' +
                '<div class="col-md-6">' +
                '<p class="plan-call-tit plan-call-tit_more plan-call-tit_more-2">'+ obj.callOutgoingTitle2 +'</p>' +
                '<p class="plan-call-det plan-call-det_more plan-call-det-2">'+ obj.callOutgoingDet2 +'</p>' +
                '</div>' +
                '</div>' +
                '<p class="plan-call__header plan-call__header_incoming">Prichádzajúce volania</p>' +
                '<div class="plan-call-col-holder plan-call-col-holder_incoming">' +
                '<div class="col-md-12">' +
                '<p class="plan-call-tit plan-call-tit_more plan-call-tit_more-1">'+ obj.callIncomingTitle +'</p>' +
                '<p class="plan-call-det plan-call-det_more plan-call-det_more-1">'+ obj.callIncomingDet +'</p>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '<div class="plc__col plc__col-4">' +
                '<p class="plan-sms-tit">'+ obj.smsOverviewTitle +'</p>' +
                this.showsmsOverviewDetOnlyMobIfAvailable(obj.smsOverviewDetOnlyMob) +
                '<p class="plan-sms-det plan-sms-det_more">'+ obj.smsDetMore +'</p>' +
                '</div>' +
                '<div class="plc__col plc__col-5">' +
                '<a href="#" class="plan-hide-detail">Skryť detail</a>' +
                '<a href="'+obj.chooseBtnLink+'" class="btn cta plan-choose-btn"><span class="ico-basket"></span>VYBRAŤ</a>' +
                '</div>' +
                '</div>' +
                '</div>'
        },
        addMobRibbonClassIfAvailable: function(val, id) {
            if (val === false) {
                return '<div class="plm__plan row" data-objid="'+ id +'">'
            } else {
                return '<div class="plm__plan row plm__plan_w-ribbon" data-objid="'+ id +'">'
            }
        },
        showMobRibbonIfAvailable: function(val) {
            if (val === false) {
                return ''
            } else {
                return '<p class="plm__plan-ribbon">'+ val +'</p>'
            }
        },
        showSmsOverviewTextInRowIfAvailable: function(val) {
            if (val === false) {
                return ''
            } else {
                return '<p class="plan-data-location">' + val + '</p>'
            }
        },
        returnRowBasedOnSelectedMobHead: function(obj) {
            console.log('triggered');

            switch(selectedMobTableHead) {
                case 0:
                    return '<p class="plan-data">'+ obj.callOverviewTitle1 +'</p>' +
                           '<p class="plan-data-location">'+ obj.callOverviewDet1 +'</p>' +
                           '<p class="plan-data">'+ obj.callOverviewTitle2 +'</p>' +
                           '<p class="plan-data-location">'+ obj.callOverviewDet2 +'</p>';
                    break;
                case 2:
                    return '<p class="plan-data">'+ obj.smsOverviewTitle +'</p>' +
                           this.showSmsOverviewTextInRowIfAvailable(obj.smsOverviewDet);
                    break;
                default:
                    return '<p class="plan-data">'+ obj.data +' '+ obj.dataLabel +'</p>' +
                           '<p class="plan-data-location">'+ obj.dataLocation +'</p>';
            }
        },
        generateMobRow: function(obj) {
            return  this.addMobRibbonClassIfAvailable(obj.ribbon, obj.id) +
                    this.showMobRibbonIfAvailable(obj.ribbon) +
                    '<div class="plmp-inner">' +
                    '<div class="plm__col plm__col-1">' +
                    '<p class="plan-price">'+ helperMethods.numberWithCommas(this.showPriceAccordingPlanType(obj)) +' €</p>' +
                    '<p class="plan-name">'+ obj.planName +'</p>' +
                    '</div>' +
                    '<div class="plm__col plm__col-2">' +
                    this.returnRowBasedOnSelectedMobHead(obj) +
                    '</div>' +
                    '<div class="plm__col plm__col-3">' +
                    '<a href="#plan-list_mob" class="plan-show-detail plan-show-detail_tab">Zobraziť detail</a>' +
                    '<a href="#plan-list_mob" class="plan-show-detail plan-show-detail_mob">Detail</a>' +
                    '<a href="#" class="btn cta plan-choose-btn"><span class="ico-basket"></span>VYBRAŤ</a>' +
                    '</div>' +
                    '</div>' +
                    '</div>'
        },
        addMobDetRibbonClassIfAvailable: function(val) {
            if (val === false) {
                return '<div class="plmp__head">'
            } else {
                return '<div class="plmp__head plmp__head_w-ribbon">'
            }
        },
        showMobDetRibbonIfAvailable: function(val) {
            if (val === false) {
                return ''
            } else {
                return '<p class="plmp__ribbon">'+ val +'</p>'
            }
        },
        showSmsOverviewTextIfAvailable: function(val) {
            if (val === false) {
                return ''
            } else {
                return '<p class="plmp__call-desc plmp_small">'+ val +'</p>'
            }
        },
        showDetailSaleIfAvailable: function(val) {
            if (val === false) {
                return ''
            } else {
                return '<p class="plan-sale">'+ val +'</p>'
            }
        },
        generateMobDet: function(obj) {
            return this.addMobDetRibbonClassIfAvailable(obj.ribbon) +
                '<a href="#" id="plm__plan-back"><img src="../assets/img/ico-back.png" alt=""></a>' +
                '<p class="plm__plan-name">'+ obj.planNameForMobDet +'</p>' +
                this.showMobDetRibbonIfAvailable(obj.ribbon) +
                '</div>' +
                '<div class="plmp__body">' +
                '<div class="plmp__body-row row plmp__row-data">' +
                '<div class="plmp__body-col plmp__body-col-1">' +
                '<div class="plmpd-icons">' +
                '<img src="../assets/img/ico-plm-data.png" alt="">' +
                '<p class="plmpd-title">Dáta</p>' +
                '</div>' +
                '</div>' +
                '<div class="plmp__body-col plmp__body-col-2">' +
                '<p class="plmp__data">'+ obj.data +' '+ obj.dataLabel +'</p>' +
                '<p class="plmp__data-location plmp_small">'+ obj.dataLocation +'</p>' +
                '</div>' +
                '</div>' +
                '<div class="plmp__body-row row plmp__row-call">' +
                '<div class="plmp__body-col plmp__body-col-1">' +
                '<div class="plmpd-icons">' +
                '<img src="../assets/img/ico-plm-outgoing.png" alt="">' +
                '<p class="plmpd-title">Odchádzajúce volania</p>' +
                '</div>' +
                '<div class="plmpd-icons">' +
                '<img src="../assets/img/ico-plm-incoming.png" alt="">' +
                '<p class="plmpd-title">Prichádzajúce volania</p>' +
                '</div>' +
                '</div>' +
                '<div class="plmp__body-col plmp__body-col-2">' +
                '<p class="plmp__call-tit">' + obj.callOutgoingTitle1 + '</p>' +
                '<p class="plmp__call-desc plmp_small">' + obj.callOutgoingDet1 + '</p>' +
                '<div class="plmp__sep"></div>' +
                '<p class="plmp__call-tit">' + obj.callOutgoingTitle2 + '</p>' +
                '<p class="plmp__call-desc plmp_small">' + obj.callOutgoingDet2 + '</p>' +
                '</div>' +
                '</div>' +
                '<div class="plmp__body-row row plmp__row-sms">' +
                '<div class="plmp__body-col plmp__body-col-1">' +
                '<div class="plmpd-icons">' +
                '<img src="../assets/img/ico-plm-sms.png" alt="">' +
                '<p class="plmpd-title">SMS|MMS</p>' +
                '</div>' +
                '</div>' +
                '<div class="plmp__body-col plmp__body-col-2">' +
                '<p class="plmp__call-tit">'+ obj.smsOverviewTitle +'</p>' +
                this.showSmsOverviewTextIfAvailable(obj.smsOverviewDet) +
                '<div class="plmp__sep"></div>' +
                '<p class="plmp__call-desc plmp_small">'+ obj.smsDetMore +'</p>' +
                '</div>' +
                '</div>' +
                '<div class="plmp__body-row row plmp__row-price">' +
                '<div class="plmp__body-col plmp__body-col-1">' +
                '<div class="plmpd-icons">' +
                '<p class="plmpd-title">Cena mesačne:</p>' +
                '</div>' +
                '</div>' +
                '<div class="plmp__body-col plmp__body-col-2">' +
                '<p class="plmp__price">'+  helperMethods.numberWithCommas(this.showPriceAccordingPlanType(obj)) +' €</p>' +
                this.showDetailSaleIfAvailable(obj.webSale) +
                '</div>' +
                '</div>' +
                '<div class="plmp__btn-holder">' +
                '<a href="'+ obj.chooseBtnLink +'" class="btn cta plan-choose-btn"><span class="ico-basket"></span>VYBRAŤ</a>' +
                '</div>' +
                '</div>' +
                '</div>'
        }
    }

    var displayDOMElements = {
        displayPlanRows: function () {
            $('#plp__body').html(planRowsPC);
            $('#plm__body').html(planRowsMob);
        }
    }

    var helperMethods = {
        numberWithCommas: function(x) {
            var parts = x.toString().split(".");
            parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            return parts.join(",");
        }
    }

    var DOMElementsMethods = {
        showRowDet: function(elem) {
            $(elem).closest('.plpp__overview').addClass('hidden');
            $(elem).closest('.plp__plan').find('.plpp__detail').removeClass('hidden');
        },
        hideRowDet: function(elem) {
            $(elem).closest('.plpp__detail').addClass('hidden');
            $(elem).closest('.plp__plan').find('.plpp__overview').removeClass('hidden');
        },
        setSortingHeadTo: function(headElem, sortingType) {
            var $elem = $(headElem);
            if ($elem.hasClass('unset')) {
                $elem.removeClass('unset');
                $elem.addClass(sortingType);
            }
            else if ($elem.hasClass('asc') && sortingType === 'desc') {
                $elem.removeClass('asc');
                $elem.addClass('desc');
            } else if ($elem.hasClass('desc') && sortingType === 'asc') {
                $elem.removeClass('desc');
                $elem.addClass('asc');
            }
        },
        toggleSortingHeadArrow: function(elem) {
            var $elem = $(elem);
            if ($elem.hasClass('asc')) {
                $elem.removeClass('asc');
                $elem.addClass('desc');
            } else {
                $elem.removeClass('desc');
                $elem.addClass('asc');
            }
        },
        togglePlanSwitcher: function(elem) {
            var $elem = $(elem);
            var $sibling = $(elem).siblings('.btn');

            if (!$elem.hasClass('ps__btn_selected')) {
                $elem.addClass('ps__btn_selected');
                $sibling.removeClass('ps__btn_selected');
            }
            this.setActivePlan();

        },
        toggleMobTableHead: function(elem) {
            var $elem = $(elem);
            var $siblings = $(elem).siblings('.plm__head-item');

            if (!$elem.hasClass('plm__head-item_selected')) {
                $elem.addClass('plm__head-item_selected');
                $siblings.removeClass('plm__head-item_selected');
            }

            this.setActiveMobileTableRow();
        },
        resetSortingHeadIco: function(elem) {
            var $headerItems = $('.plph__ico');
            var $elem = $(elem);

            $headerItems.each(function() {
                var $this = $(this);
               if ($this.attr('id') !== $elem.attr('id')) {
                   if ($this.hasClass('asc')) {
                       $this.removeClass('asc');
                   } else if ($this.hasClass('desc')) {
                       $this.removeClass('desc');
                   }

                   $(this).addClass('unset');
               }
            });
        },
        resetAllSortingHeadIcos: function() {
            var $headerItems = $('.plph__ico');

            $headerItems.each(function() {
                var $this = $(this);

                if ($this.hasClass('desc')) {
                    $this.removeClass('desc');
                }

                if ($this.hasClass('asc')) {
                    $this.removeClass('asc');
                }

                if (!$this.hasClass('unset')) {
                    $this.addClass('unset');
                }
            });
        },
        setActivePlan: function() {
            var $planBtn = $('.ps__btn_selected');

            if ($planBtn.attr('id') === 'plan-mobile') {
                activePlan = 'planWithMob';
            } else activePlan = 'planWithoutMob';
        },
        setActiveMobileTableRow: function() {
            selectedMobTableHead = Number($('.plm__head-item_selected').data('headid'));
        },
        showAllObjCount: function() {
            var $allObj = $('#allObj');

            $allObj.html(objMethods.returnObjCount(planArr));
        },
        showFilteredObjCount: function(obj) {
            var $filtObj = $('#filteredObjCount');

            $filtObj.html(objMethods.returnObjCount(obj))
        },
        showMobileDetail: function() {
            var $plmDetail = $('#plm__plan-detail');
            var $plmOverlay = $('#plm__overlay');

            $plmDetail.removeClass('hidden');
            $plmOverlay.removeClass('hidden');
        },
        fillMobDetailWithObj: function(obj) {
            $('#plm__plan-detail').html(generateDOM.generateMobDet(obj));
        },
        hideMobileDetail: function() {
            var $plmDetail = $('#plm__plan-detail');
            var $plmOverlay = $('#plm__overlay');

            $plmDetail.addClass('hidden');
            $plmOverlay.addClass('hidden');
        },
        getDataAttrFromRow: function(elem) {
            var $elem = $(elem);

            return Number($elem.closest('.plm__plan.row').data('objid'));
        },
        bindSticky: function() {
            var sticky = new Waypoint.Sticky({
                element: $('#plm-sticky')[0]
            })

            $('#sec-prolong').waypoint(function(direction) {
                $('#plm-sticky').toggleClass('stuck', direction === 'up');
                $('#plm-sticky').toggleClass('sticky-surpassed', direction === 'down');
            }, {
                offset: function() {
                    return $('#plm-sticky').outerHeight();
                }
            });
        },
        bindStickyDesktop: function() {
            var sticky = new Waypoint.Sticky({
                element: $('.plp__head')[0]
            })

            $('#sec-prolong').waypoint(function(direction) {
                $('.plp__head').toggleClass('stuck', direction === 'up');
                $('.plp__head').toggleClass('sticky-surpassed', direction === 'down');
            }, {
                offset: function() {
                    return $('.plp__head').outerHeight();
                }
            });
        },
        addTooltips: function() {
            $('#ec-1').qtip({
                content: 'Vyberte si k paušálu aj nový mobil alebo tablet a získate naň zľavu až do výšky 20 €.<br /> <b>Platí iba kúpe cez web!</b>',
                position: {
                    my: 'bottom center',
                    at: 'top center'
                }
            });
            $('#ec-2').qtip({
                content: '<b>Prvých 14 dní bez záväzkov!</b><br />Pri kúpe cez web môžete vrátiť službu a tovar bez udania dôvodu až do 14 dní!',
                position: {
                    my: 'bottom center',
                    at: 'top center'
                }
            });
            $('#ec-3').qtip({
                content: 'Pri kúpe cez web získate <b>doručenie kuriérom zadarmo</b> kdekoľvek v rámci Slovenska.',
                position: {
                    my: 'bottom center',
                    at: 'top center'
                }
            })
        }
    }

    function init() {
        getDataFromJSON.getData();
        DOMElementsMethods.bindSticky();
        DOMElementsMethods.bindStickyDesktop();
        DOMElementsMethods.addTooltips();
    }

    return {
        init: init
    };
})();

$(document).ready(plansModule.init);
