// $('.selectCategory').heapbox({
//     // "heapsize":"500px"
// });
// var newOption = $('<option/>', {  
// 	value: 'ASD',
// 	text: 'ASD'
// });
// $(".selectCategory").append(newOption);

    //add new option to source selectbox


//update heapbox
//$(".selectCategory").heapbox("update");

$(document).ready(function () {

    /**
     * DEFINE ALL CATEGORIES HERE
     * example: <option value="zakladnyBalik">Základný balík</option>
    */

    // DEFINE ALL CATEGORIES HERE AND SAME CATEGORIES IN HTML!
    var categories = {
        0: ['zakladnyBalik', 'Základný balík programov', '<span class="zbheading"></span>', null],
        1: ['extraBalikLTukiTV', '1x Extra balík L + exkluzívna detská Ťuki TV', 'Balík je zahrunutý v cene balíčka.', null],
        2: ['extrabalikXL', 'Extra balík XL', 'Balík je zahrunutý v cene balíčka.', null],
        3: ['hudobnyAdetsky', 'Hudobný a detský balík', 'Sledujte najväčšie hudobné hity domácej aj svetovej scény. Pre najmladších sme pripravili rozprávky a detské zábavné programy.', null],
        4: ['volnyCas', 'Voľný čas', 'Pohodlne sa usaďte a vyberte si obľúbenú filmovú TV stanicu z najrôznejších žánrov.', null],
        5: ['stylAfakty', 'Štýl a fakty', 'Pestrý mix programov pre vašu informovanosť z tých najrôznejších oblastí.', null],
        6: ['dokumentarny', 'Dokumentárny', 'Pre všetkých fanúšikov dokumentárnych programov sme pripravili balík nabitý faktami.', null],
        7: ['filmovy', 'Filmový balík', 'Za filmovými trhákmi už nemusíte jazdiť do kina. Najlepšie filmové zážitky môžu začať rovno u vás doma.', null],
        8: ['sportovy', 'Športový balík', 'Najkvalitnejšie športové zážitky potešia všetkých vášnivých fanúšikov.', null],
        9: ['hboCinemaxHboGO', 'HBO + Cinemax + HBO GO', 'Filmové novinky a trháky, najsledovanejšie seriály, hodiny a hodiny zábavy. To a ešte viac vám ponúkame s naším prémiovým balíčkom. Sledujte HBO kdekoľvek vďaka internetovej televízii HBO GO.<br>Prvé 2 mesiace 0 € | potom 9,98 € mesačne | viazanosť 12 mesiacov', '<span class="tvbadge">Prémiový balík</span><b>9,98€</b>'],
        10: ['hboHboGo', 'HBO + HBO GO', 'Filmové novinky a trháky, najsledovanejšie seriály, hodiny a hodiny zábavy. To a ešte viac vám ponúkame s naším prémiovým balíčkom. Sledujte HBO kdekoľvek vďaka internetovej televízii HBO GO.<br>Prvých 6 mesiacov 7,99 € | zvyšné mesiace 8,99 € mesačne', '<span class="tvbadge">Prémiový balík</span><b>7,99€</b>'],
        11: ['madarskyExtra', 'Maďarský Extra balík', 'Vždy vám na vašom televíznom prijímači chýbali maďarské programy? S prémiovým balíčkom je to už minulosťou a vy si môžete vychutnať to najlepšie, čo vám maďarská televízia ponúka.<br>Prvých 9 mesiacov 2,99€ | zvyšné mesiace 3,49€ mesačne', '<span class="tvbadge">Prémiový balík</span><b>2,99€</b>'],
        12: ['hdsportovy', 'HD Športový', 'Športové zážitky v nádhernej farebnosti s ostrými detailami.<br>Prvých 9 mesiacov 4,50 € | zvyšné mesiace 5,99 € mesačne', '<span class="tvbadge">Prémiový balík</span><b>4,50€</b>'],
        13: ['hdrodinny', 'HD Rodinný', 'Balíček ako stvorený pre rodinnú pohodu. Mama, otec, sestra, brat – každý si tu príde na svoje. Ba čo viac, vysielanie v HD kvalite zabezpečí tie najkvalitnejšie zážitky z vašich obľúbených programov.<br>Prvých 9 mesiacov 4,50 € | zvyšné mesiace 5,99 € mesačne', '<span class="tvbadge">Prémiový balík</span><b>4,50€</b>'],
        14: ['hdrodinnyHDsportovy', 'HD Rodinný + HD Športový', 'Prvých 9 mesiacov 7,00 € | zvyšné mesiace 9,98 € mesačne', '<span class="tvbadge">Prémiový balík</span><b>7,00€</b>'],
        15: ['spravodajsky', 'Spravodajský balík', 'Chcete byť v neustálom obraze o tom, čo sa deje vo svete z pohodlia vášho domova? Vďaka prémiovému balíku môžete sledovať najaktuálnejší prehľad správ a udalostí nepretržite 24 hodín denne.<br>Prvých 6 mesiacov 2,99 € | zvyšné mesiace 3,48 € mesačne', '<span class="tvbadge">Prémiový balík</span><b>2,99€</b>'],
        16: ['filmovyPremium', 'Filmový Prémium', 'Prvých 6 mesiacov 3,98 € | zvyšné mesiace 4,99 € mesačne', '<span class="tvbadge">Prémiový balík</span><b>3,98€</b>']
      
    };
    // DEFINE RATEPLAN M CATEGORIES HERE 
    var categories_M =
      '['+
          '{"value":"vsetky","text":"Všetky"},'+
          '{"value":"zakladnyBalik","text":"Základný balík programov"},'+
          '{"value":"hudobnyAdetsky","text":"Hudobný a detský balík"},'+
          '{"value":"volnyCas","text":"Voľný čas"},'+
          '{"value":"stylAfakty","text":"Štýl a fakty"},'+
          '{"value":"dokumentarny","text":"Dokumentárny balík"},'+
          '{"value":"filmovy","text":"Filmový balík"},'+
          '{"value":"sportovy","text":"Športový balík"},'+
          '{"value":"hboCinemaxHboGO","text":"HBO + Cinemax + HBO GO (prémiový balík)"},'+
          '{"value":"hboHboGo","text":"HBO + HBO GO (prémiový balík)"},'+
          '{"value":"madarskyExtra","text":"Maďarský Extra balík (prémiový balík)"},'+
          '{"value":"hdsportovy","text":"HD Športový (prémiový balík)"},'+
          '{"value":"hdrodinny","text":"HD Rodinný (prémiový balík)"},'+
          '{"value":"hdrodinnyHDsportovy","text":"HD Rodinný + HD Športový (prémiový balík)"},'+
          '{"value":"spravodajsky","text":"Spravodajský balík (prémiový balík)"},'+
          '{"value":"filmovyPremium","text":"Filmový Prémium (prémiový balík)"}'+
        ']';
    // DEFINE RATEPLAN L CATEGORIES HERE
    var categories_L =
      '['+
          '{"value":"vsetky","text":"Všetky"},'+
          '{"value":"zakladnyBalik","text":"Základný balík programov"},'+
          '{"value":"extraBalikLTukiTV","text":"1x Extra balík L + exkluzívna detská Ťuki TV"},'+
          '{"value":"hudobnyAdetsky","text":"Hudobný a detský balík"},'+
          '{"value":"volnyCas","text":"Voľný čas"},'+
          '{"value":"stylAfakty","text":"Štýl a fakty"},'+
          '{"value":"dokumentarny","text":"Dokumentárny balík"},'+
          '{"value":"filmovy","text":"Filmový balík"},'+
          '{"value":"sportovy","text":"Športový balík"},'+
          '{"value":"hboCinemaxHboGO","text":"HBO + Cinemax + HBO GO (prémiový balík)"},'+
          '{"value":"hboHboGo","text":"HBO + HBO GO (prémiový balík)"},'+
          '{"value":"madarskyExtra","text":"Maďarský Extra balík (prémiový balík)"},'+
          '{"value":"hdsportovy","text":"HD Športový (prémiový balík)"},'+
          '{"value":"hdrodinny","text":"HD Rodinný (prémiový balík)"},'+
          '{"value":"hdrodinnyHDsportovy","text":"HD Rodinný + HD Športový (prémiový balík)"},'+
          '{"value":"spravodajsky","text":"Spravodajský balík (prémiový balík)"},'+
          '{"value":"filmovyPremium","text":"Filmový Prémium (prémiový balík)"}'+
        ']';
    // DEFINE RATEPLAN L CATEGORIES HERE
    var categories_XL = '['+
            '{"value":"vsetky","text":"Všetky"},'+
            '{"value":"zakladnyBalik","text":"Základný balík programov"},'+
            '{"value":"extraBalikLTukiTV","text":"1x Extra balík L + exkluzívna detská Ťuki TV"},'+
            '{"value":"extrabalikXL","text":"Extra balík XL"},'+
            '{"value":"hudobnyAdetsky","text":"Hudobný a detský balík"},'+
            '{"value":"volnyCas","text":"Voľný čas"},'+
            '{"value":"stylAfakty","text":"Štýl a fakty"},'+
            '{"value":"dokumentarny","text":"Dokumentárny balík"},'+
            '{"value":"filmovy","text":"Filmový balík"},'+
            '{"value":"sportovy","text":"Športový balík"},'+
            '{"value":"hboCinemaxHboGO","text":"HBO + Cinemax + HBO GO (prémiový balík)"},'+
            '{"value":"hboHboGo","text":"HBO + HBO GO (prémiový balík)"},'+
            '{"value":"madarskyExtra","text":"Maďarský Extra balík (prémiový balík)"},'+
            '{"value":"hdsportovy","text":"HD Športový (prémiový balík)"},'+
            '{"value":"hdrodinny","text":"HD Rodinný (prémiový balík)"},'+
            '{"value":"hdrodinnyHDsportovy","text":"HD Rodinný + HD Športový (prémiový balík)"},'+
            '{"value":"spravodajsky","text":"Spravodajský balík (prémiový balík)"},'+
            '{"value":"filmovyPremium","text":"Filmový Prémium (prémiový balík)"}'+
        ']';
    var categories_SATM = '['+
        '{"value":"vsetky","text":"Všetky"},'+
        '{"value":"zakladnyBalik","text":"Základný balík programov"},'+
        '{"value":"hboCinemaxHboGO","text":"HBO + Cinemax + HBO GO (prémiový balík)"},'+
        '{"value":"hboHboGo","text":"HBO + HBO GO (prémiový balík)"},'+
        '{"value":"madarskyExtra","text":"Maďarský Extra balík (prémiový balík)"}'+
    ']';

    /**
     * core functionality below
     * do not edit this lines
    */

    var dataUrl = 'https://www.telekom.sk/documents/10179/17602602/tvdata.json?v=2';
    var allData = []; //store for all data
    var allItemsWrapper= $('#itemsHolder');
    var $ratePlanInput = $('.selectRatePlan');
    var $searchQueryInput = $('#searchQuery');
    var $filterCategoryInput = $('.selectCategory');
    var $filterCategoryInput_heapbox = $('#heapbox_selectCategory');
    var ratePlanInputVal = $('.selectRatePlan').find('option:selected').val();
    var filterCategoryInputVal = $('.selectCategory').find('option:selected').val(); //get category name from selected option
    var typingTimer;
    var isLoading = true;

    if (window.location.hostname == "localhost") {
        //only for dev purposes
        dataUrl = "/assets/js/tvdata.json";
    } else {
        dataUrl;
    }

    loadingSpinner(true);

    //sort html items in selected category
    var sortHTML_by_name = function(a, b) {
        return $(a).text().toLowerCase().localeCompare($(b).text().toLowerCase());
    }

    // sort items in array
    function sortArray_by_name( a, b ) {
        if ( a.title < b.title ){
          return -1;
        }
        if ( a.title > b.title ){
          return 1;
        }
        return 0;
    }

    function getDataOnInit(data, onChange_ratePlanInputVal) {
        // allItemsWrapper.empty(); //reset
        allData = []; //reset
        for (let index = 0; index < data.length; index++) {
            const items = data[index];
            if (typeof items.ratePlan === 'object') {
                if (items.ratePlan[0][onChange_ratePlanInputVal !== undefined ? onChange_ratePlanInputVal : "M"] === "ano") { //SET "M" AS A DEFAULT RATE PLAN
                        allData.push(items);
                }
            } else {
                console.log("Please, define rate plans for: " + items.title)
            }
        }
        $filterCategoryInput.heapbox("set", eval("categories_"+ratePlanInputVal)); // on init, set heapbox categories for rateplan "M"
        showAllItems(allData);
    }

    function setZakladnyBalikSubehading(ratePlanInputVal) {
        if (ratePlanInputVal === "XL") {
            $('.zbheading').text('Balík je v cene Magio TV XL. Obsahuje 6 programových balíkov podľa vlastného výberu.');
        } else if (ratePlanInputVal === "L") {
            $('.zbheading').text('Balík je v cene Magio TV L. Obsahuje 6 programových balíkov podľa vlastného výberu.');
        } else if (ratePlanInputVal === "M") {
            $('.zbheading').text('Balík je v cene Magio TV XL. Obsahuje 3 programové balíky podľa vlastného výberu.');
        } else if (ratePlanInputVal === "SATM") {
            $('.zbheading').text('V našom pestrom balíčku programov je všetko, na čo si spomeniete. Filmy, šport, dokumenty, hudba alebo program pre vašich najmladších členov domácnosti.');
        }
    }

    $.ajax({
        url: dataUrl,
        cache: false,
        //async: false,
        type: "get",
        crossDomain: true,
        success: function (data) {
            getDataOnInit(data, ratePlanInputVal); // get data on init page load
            setZakladnyBalikSubehading(ratePlanInputVal); //set default subheading text for zakladny balik
            $ratePlanInput.on('change', function () { //on rate plan change
                allItemsWrapper.empty(); //reset
                allData = []; //reset
                loadingSpinner(true);

                ratePlanInputVal = $(this).find('option:selected').val();
                $searchQueryInput.val('') //reset search input field
                $filterCategoryInput_heapbox.find('ul li a[rel="vsetky"]').click(); //reset filter category heapbox
                $filterCategoryInput.heapbox("set", eval("categories_"+ratePlanInputVal)); //set new heapbox categories if user change rate plan
                getDataOnInit(data, ratePlanInputVal);
                $('p.tvHead span').text("MAGIO TV " + ratePlanInputVal);
                $('#kinoActivate').text("AKTIVOVAŤ MAGIO TV " + ratePlanInputVal).attr('href', '#');
                setZakladnyBalikSubehading(ratePlanInputVal); 
            });
        },
        error: function () {
            alert("Get data error ... try again!");
        }
    });
    
    // generate single item
    function createItem (id, img, title, desc, holder) {
        var singleItem = '';
        singleItem +=
        '<div id="item-'+id+'" class="item col-xs-6 col-sm-3 col-md-3 col-lg-2">'+
        '<div class="itemWrapper">'+
            '<div class="imgWrapper">'+
                '<img src="'+img+'" alt="">'+
            '</div>'+
            '<div class="text">'+
                '<p class="itemTitle">'+title+'</p>'+
                '<p class="desc">'+(desc !== undefined ? desc : "Detail nie je k dispozícii")+'</p>'+
            '</div>'+
        '</div>'+
        '</div>';
        $(singleItem).stop().fadeIn(500).css("display","inline-block").appendTo(holder);
    }

    //generate items category <div> holder 
    function generateItemHolder (category, title, text, price) {
        var GenerateCategory = '';
        GenerateCategory +=
        '<div class="itemHolder bggrey col-xs-12" id="'+category+'">'+
            '<p class="categoryHeading"><span  class="subtitle">' +title+ (price !== null ? price : '') +'</span>'+
            '<br>'+text+'</p>'+
            '<div class="sortItem">' +
                '<a href="#" class="btn btn_mag-outline sort">A > Z</a>'+
            '</div>'+
            '<div class="allItems"></div>'+
        '</div>';
        if ($('#'+category+'').length == 0) {
            $(GenerateCategory).appendTo(allItemsWrapper);
        }
    }

    //append all items to holder
    function showAllItems(data) {
        $.each( categories, function( i, category ) {
            var JSONcategoryName = category[0]; //i.e: #zakladnyBalik
            var categoryTitle = category[1];
            var categoryText = category[2];
            var categoryPrice = category[3];
            if ((JSONcategoryName === "extrabalikXL" && ratePlanInputVal !== "XL") 
                || (JSONcategoryName === "extraBalikLTukiTV" && ratePlanInputVal === "M")) {
                        return; // dont show XL and L specific categories for lower plans
            } else {
                //data.sort( sortArray_by_name ); //apply movie sorting by title on init

              $.each(data, function(a, item) {
                var listItemCategories = item.category[0];
                var listItemRatePlans = item.ratePlan[0];
                if (ratePlanInputVal === "M" || ratePlanInputVal === "L" || ratePlanInputVal === "XL") {
                    if (listItemCategories.cable !== undefined) {
                        if (listItemCategories.cable[JSONcategoryName] == "ano" && listItemRatePlans[ratePlanInputVal] == "ano") {
                            //item is assigned to category, generate item and its holder
                            generateItemHolder(
                            JSONcategoryName,
                            categoryTitle,
                            categoryText,
                            categoryPrice
                            );
                            createItem(
                            item.id,
                            item.img,
                            item.title,
                            item.desc,
                            "#" + JSONcategoryName + ' .allItems'
                            );
                        }
                    }
                } else {
                    if (listItemCategories.satelite !== undefined) {
                        if (listItemCategories.satelite[JSONcategoryName] == "ano" && listItemRatePlans[ratePlanInputVal] == "ano") {
                            //item is assigned to category, generate item and its holder
                            generateItemHolder(
                              JSONcategoryName,
                              categoryTitle,
                              categoryText,
                              categoryPrice
                            );
                            createItem(
                              item.id,
                              item.img,
                              item.title,
                              item.desc,
                              "#" + JSONcategoryName + ' .allItems'
                            );
                        }
                    }
                }
               
              });
            }
        });
        loadingSpinner(false) //hide loading bar
    }

    //assign item to category
    function showAllItems_FromSelectedCategory(data, filterCategoryInputVal) {
        allItemsWrapper.empty(); //reset
        if (filterCategoryInputVal === 'vsetky') {
            showAllItems(data)
        } else {
            $.each( categories, function( i, category ) {
                var categoryTitle = category[1];
                var categoryText = category[2];
                var categoryPrice = category[3];
                $.each( data, function( a, item ) {
                    var listItemCategories = item.category[0];
                    if (ratePlanInputVal === "M" || ratePlanInputVal === "L" || ratePlanInputVal === "XL") {
                        if (listItemCategories.cable !== undefined) {
                            if (category[0] === filterCategoryInputVal
                                && listItemCategories.cable[filterCategoryInputVal] == "ano") {
                                    //product is assigned to category
                                    generateItemHolder (filterCategoryInputVal, categoryTitle, categoryText, categoryPrice);
                                    createItem (item.id, item.img, item.title, item.desc, "#"+filterCategoryInputVal + ' .allItems');
                            }
                        }
                    } else {
                        if (listItemCategories.satelite !== undefined) {
                            if (category[0] === filterCategoryInputVal 
                                && listItemCategories.satelite[filterCategoryInputVal] == "ano") { 
                                    //product is assigned to category
                                    generateItemHolder (filterCategoryInputVal, categoryTitle, categoryText, categoryPrice);
                                    createItem (item.id, item.img, item.title, item.desc, "#"+filterCategoryInputVal + ' .allItems');
                            }
                        }
                    }
                });
            });
        }
        $searchQueryInput.val('') //reset search input field
    }

    // list items on user category change
    $filterCategoryInput.on('change', function(e) {
        filterCategoryInputVal = $(this).find('option:selected').val();
        showAllItems_FromSelectedCategory(allData, filterCategoryInputVal);
        setZakladnyBalikSubehading(ratePlanInputVal);
    });

    // search by user query
    // search is executed by .JSON "title" attribute 
    function filterByUserInput($thisInput) {
        var searchInputVal = $thisInput.val();
        var categoryInputVal = $('.selectCategory').find('option:selected').val();
        allItemsWrapper.empty(); //reset
        loadingSpinner(true);
        
        clearTimeout(typingTimer);
        if (searchInputVal.length > 0) {
            typingTimer = setTimeout( function() {
                userDoneTyping(searchInputVal, categoryInputVal);
                setZakladnyBalikSubehading(ratePlanInputVal);
            }, 1000);
        } else {
            //on user query clear, show all items from active category
            allItemsWrapper.empty(); //reset
            if (categoryInputVal === 'vsetky') {
                showAllItems(allData);
            } else {
                //search by user query in selected catgory
                $.each( categories, function( i, category ) {
                    var categoryName = category[0]; //i.e: #zakladnyBalik
                    var categoryTitle = category[1];
                    var categoryText = category[2];
                    var categoryPrice = category[3];
                    
                    $.each( allData, function( a, item ) {
                        var listItemCategories = item.category[0];
                        //if M,L,XL plan selected, then search only in cable category
                        if (ratePlanInputVal === "M" || ratePlanInputVal === "L" || ratePlanInputVal === "XL") {
                            if ((item.title.toLowerCase().indexOf(searchInputVal.toLowerCase()) !== -1) 
                                && categoryName === categoryInputVal
                                && (listItemCategories.cable !== undefined ? listItemCategories.cable[categoryInputVal] == "ano" : null)) { 
                                    //product is assigned to category, search for product
                                    generateItemHolder (categoryInputVal, categoryTitle, categoryText, categoryPrice);
                                    createItem (item.id, item.img, item.title, item.desc, "#"+categoryInputVal + ' .allItems');
                                }
                        } else {
                        //if SAT plan selected, then search only in satelite category
                            if ((item.title.toLowerCase().indexOf(searchInputVal.toLowerCase()) !== -1) 
                                && categoryName === categoryInputVal 
                                && (listItemCategories.satelite !== undefined ? listItemCategories.satelite[categoryInputVal] == "ano" : null)) { 
                                    //product is assigned to category, search for product
                                    generateItemHolder (categoryInputVal, categoryTitle, categoryText, categoryPrice);
                                    createItem (item.id, item.img, item.title, item.desc, "#"+categoryInputVal + ' .allItems');
                            }
                        }
                    });
                });
                loadingSpinner(false);
            }
            setZakladnyBalikSubehading(ratePlanInputVal);
        }
    }

    //remove input query diacritic
    function normalizeText (text) {
        return text.toLowerCase().replace(/[\u0300-\u036f]/g, ""); 
    }

    function userDoneTyping(searchInputVal, categoryInputVal) {
        allItemsWrapper.empty(); //reset
        var foundAnyResult;

        for (let index = 0; index < allData.length; index++) {
            const item = allData[index];
            var listItemCategories = item.category[0];
            var normalizedItemTitle = normalizeText(item.title); //remove diacritic
            var normalizedSubTitle = normalizeText(item.subtitle);
            var normalizedSearchQuery = normalizeText(searchInputVal);

            //search in cable categories
            if (ratePlanInputVal === "M" || ratePlanInputVal === "L" || ratePlanInputVal === "XL") {
                Object.keys(listItemCategories.cable !== undefined ? listItemCategories.cable : '').forEach(function(categoryName) {
                    var categTitle; //get item category title;
                    var categText;
                    var categPrice;

                    $.each( categories, function( indx, category ) {
                        if (category[0] === categoryName) {
                            categTitle = category[1];
                            categText = category[2];
                            categPrice = category[3]
                        }
                    })
                    
                    if ((categoryName === "extrabalikXL" && ratePlanInputVal !== "XL") 
                        || (categoryName === "extraBalikLTukiTV" && ratePlanInputVal === "M")) {
                                return; // dont show XL and L specific categories for lower plans
                    } else {
                        if(((normalizedItemTitle.indexOf(normalizedSearchQuery) !== -1) || (normalizedSubTitle.indexOf(normalizedSearchQuery) !== -1))
                            && (categoryInputVal === 'vsetky' ? true : (categoryName === categoryInputVal))
                            && item.category[0].cable[categoryName] === "ano") {
                                foundAnyResult = true;
                                generateItemHolder (categoryName, categTitle, categText, categPrice);
                                createItem (item.id, item.img, item.title, item.desc, "#"+categoryName + ' .allItems');
                        }
                    }
                });
            } else {
            //search in satelite categories
                Object.keys(listItemCategories.satelite !== undefined ? listItemCategories.satelite : '').forEach(function(categoryName) {
                    var categTitle; //get item category title;
                    var categText;
                    var categPrice;
                    
                    $.each( categories, function( indx, category ) {
                        if (category[0] === categoryName) {
                            categTitle = category[1];
                            categText = category[2];
                            categPrice = category[3]
                        }
                    })
                    
                    if ((categoryName === "extrabalikXL" && ratePlanInputVal !== "XL") 
                        || (categoryName === "extraBalikLTukiTV" && ratePlanInputVal === "M")) {
                                return; // dont show XL and L specific categories for lower plans
                    } else {
                        if(((normalizedItemTitle.indexOf(normalizedSearchQuery) !== -1) || (normalizedSubTitle.indexOf(normalizedSearchQuery) !== -1))
                            && (categoryInputVal === 'vsetky' ? true : (categoryName === categoryInputVal))
                            && item.category[0].satelite[categoryName] === "ano") {
                                foundAnyResult = true;
                                generateItemHolder (categoryName, categTitle, categText, categPrice);
                                createItem (item.id, item.img, item.title, item.desc, "#"+categoryName + ' .allItems');
                        }
                    }
                }); 
            } 
        }
        loadingSpinner(false);
        // if no item match, throw error
        if (foundAnyResult !== true) {
            generateItemHolder ("queryHolder", "Výsledky hľadania:", '', null);
            $('#queryHolder').append('<div class="searchError"><p>Lutujeme, vami zvolený výraz sme nenašli. Skúste vyhľadávanie znova.</p></div>')
        }
    }

    // start search on user query input (by title)
    $searchQueryInput.on('keyup', function() {
        $thisInput = $(this);
        filterByUserInput($thisInput);
    });

    //show or hide loading spinner
    function loadingSpinner(isLoading) {
        if (isLoading) {
            $('.dataLoading').show(); //hide loading bar
        } else {
            $('.dataLoading').hide(); //hide loading bar
        }
    }

    // heapbox slide up animation fix
    $('li.heapOption a').on('click', function() {
        $(this).closest('.heap').slideUp("slow");
    });
 
    // sort HTML items by title, from A to Z
    $('body').on('click', 'a.sort', function (e) {
        e.preventDefault();
        $this = $(this).closest(".itemHolder").find('.allItems');
        var list = $this.children('.item');
        list.sort(sortHTML_by_name);
        $this.html(list);
    })
});
    