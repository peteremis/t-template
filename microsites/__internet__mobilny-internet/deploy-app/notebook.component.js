'use strict';

// Register `tabletSlider` component, along with its associated controller and template
angular.
module('notebooksList').
component('notebooksList', {
    templateUrl: '/documents/10179/10074077/notebook.template.html',
    controller: ['$resource', 'dataService', '$timeout',
        function NotebooksListController($resource, dataService, $timeout) {
            var self = this;
            self.allNotebooks = [];
            self.notebooksLoaded = false; // disable slick
            self.notebooksUpdate = function() {
                dataService.allNotebooks().then(function(data) {
                    self.allNotebooks = data;
                    self.notebooksLoaded = true; // enable slick
                });
            };
            self.currentSlideNum = 1;
            self.slickNbConfig = {
                slidesToShow: 3,
                slidesToScroll: 1,
                centerMode: false,
                infinite: false,
                swipeToSlide: true,
                swipe: true,
                arrows: true,
                dots: true,
                autoplay: false,
                initialSlide: 0,
                touchMove: false,
                draggable: false,
                // customPaging: function(slider, i) {
                //     return '<span class="slide-dot"><!-- dot --></span>';
                // },
                responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                        arrows: true,
                        dots: false,
                    }
                }, {
                    breakpoint: 769,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        arrows: false,
                        infinite: false,
                        dots: false
                    }
                }],
                method: {},
                event: {
                    beforeChange: function(event, slick, currentSlide, nextSlide) {},
                    afterChange: function(event, slick, currentSlide, nextSlide) {
                        self.currentSlideNum = currentSlide + 1; // save current index each time
                    },
                    init: function(event, slick) {
                        self.allIndex = slick.slideCount;
                        if ($(window).width() < 769) {
                            $(".as2 .slick-dots li").css("width", 100 / $(".as2 .slick-dots li").length + '%');
                        };
                    }
                }
            };

            self.pcloseNotebook = function(data) {
                $.fancybox.close();
                //console.log('popup close:');
            };

            // INIT
            self.init = function() {
                //wait until tab2 is loaded
                $timeout(function() {
                    self.notebooksUpdate();
                }, 500);
                //console.log('test nb init');
            };
            // runs once per controller instantiation
            self.init();
        }
    ]
});
