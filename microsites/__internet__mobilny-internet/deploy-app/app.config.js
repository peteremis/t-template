'use strict';

angular.
    module('tabletsListApp').
    config(['$locationProvider',
        function config($locationProvider) {
            $locationProvider.hashPrefix('!');
        }
    ]);
 