 /* sticky navigation */
 $(function() {
     $("#nav-sticky").sticky({
         topSpacing: 0,
         widthFromWrapper: true
     });

     $(".scroll-to").click(function() {
         if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
             var target = $(this.hash);
             target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
             if (target.length) {
                 $('html,body').animate({
                     scrollTop: target.offset().top - ($("#nav-sticky").outerHeight())
                 }, 1000);
                 return false;
             }
         }
     });

     var contentSections = $("[id*='sec-']"),
         secondaryNav = $("#nav-sticky");

     function updateSecondaryNavigation() {
         contentSections.each(function() {
             var actual = $(this),
                 actualHeight = actual.height() + parseInt(actual.css('paddingTop').replace('px', '')) + parseInt(actual.css('paddingBottom').replace('px', '')),
                 actualAnchor = secondaryNav.find('a[href="#' + actual.attr('id') + '"]');

             if ((actual.offset().top - secondaryNav.outerHeight() <= $(window).scrollTop()) && (actual.offset().top + actualHeight - secondaryNav.outerHeight() > $(window).scrollTop())) {
                 actualAnchor.addClass('active');
             } else {
                 actualAnchor.removeClass('active');
             }
         });
     }
     updateSecondaryNavigation();

     $(window).scroll(function(event) {
         updateSecondaryNavigation();
     });
 });
 /* tooltips */
 $(function() {
     $('.info-1').qtip({
         content: {
             text: 'Pri programoch Mobilný internet S/M/L sa po vyčerpaní objemu dát 1 GB/5 GB/15 GB uplatňuje obmedzenie rýchlosti dátových prenosov max. na 64 kb/s.'
         }
     });
     $('.info-tablet').qtip({
         content: {
             text: 'Vybrané paušály sú:<br> Happy M, L, XL volania, XL, XXL a Profi,<br> ÁNO M, ÁNO L, ÁNO XL a ANO XXL.'
         }
     });

     $('#zasady-tooltip').qtip({
         content: {
             text: 'Vďaka Zásadám férového používania (anglická skratka FUP) zabezpečujeme rovnakú kvalitu ' +
                 'všetkým zákazníkom a predchádzame tomu, aby jeden zákazník čerpal nadmerne dáta na úkor ostatných. <br>' +
                 'Preto sú naše neobmedzené dáta v objeme 100 GB. ' +
                 'Po ich prečerpaní neplatíte nič navyše, ale prenos dát sa spomalí.'
         },
         position: {
             my: 'left center',
             at: 'right center'
         },
         style: {
             classes: 'tooltip tooltip-left'
         }
     });
 });
 /* popup */
 $(function() {
     $('.popup').fancybox({
         padding: 11,
         margin: 0,
         closeBtn: false,
         helpers: {
             overlay: {
                 css: {
                     'background': 'rgba(0, 0, 0, 0.7)'
                 }
             }
         }
     });

     $('.p-close, .close-btn').on('click', function(e) {
         e.preventDefault();
         $.fancybox.close();
     });
 });

setTimeout(function(){
  $(window).dispatchEvent(new Event('resize'));
}, 0);

 /* tabs & sliders */
 $(document).ready(function() {

     /* ::: DataDevice ::: */
     // var $dataDeviceSlider = $('.data-device-slider');
     // var $dataDevicePagin = $('.data-device-pagin');
     // var $dataDevicePaginPrev = $('.data-device-pagin-prev');
     // var $dataDevicePaginNext = $('.data-device-pagin-next');

     /* ::: TAB CONFIG - FIX ::: */
     $('ul.tabs li a').on('click', function(e) {
     //     // $('.as1').css("visibility", "hidden");
     //     // $('.as2').css("visibility", "hidden");
     //     // var currentAttrValue = $(this).attr('href');
     //     // $('.tab ' + currentAttrValue).show().siblings().hide();
     //     // $(this).parent('li').addClass('current').siblings().removeClass('current');
         $('.as1').slick('setPosition', 0);
         $('.as2').slick('setPosition', 0);
     //     // setTimeout(function() {
     //     //     $('.as1').css("visibility", "visible");
     //     //     $('.as2').css("visibility", "visible");
     //     // }, 200);
         e.preventDefault();
     });

     /* ::: GET CURRENT SLIDE NUM ::: */
     // $dataDeviceSlider.on('init reInit afterChange', function(event, slick, currentSlide, nextSlide) {
     //     //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
     //     var i = (currentSlide ? currentSlide : 0) + 1;
     //     $dataDevicePagin.text('(' + i + '/' + slick.slideCount + ')');
     // });


     /* ::: PAGINATION INDICATOR ::: */
     // if ($(window).width() < 769) {
     //     $dataDeviceSlider.on('init', function(event, slick) {
     //         if ($(window).width() < 769) {
     //             $('.data-device-slider .slick-dots li').css("width", 100 / $('.dataDevice__item').length + '%');
     //         }
     //     });
     // };

     /* ::: PAGINATION ::: */
     /* pagination dataDevice */
     // $dataDevicePaginPrev.click(function() {
     //     $dataDeviceSlider.slick('slickPrev');
     // });
     // $dataDevicePaginNext.click(function() {
     //     $dataDeviceSlider.slick('slickNext');
     // });

     /* ::: Slider DataDevice ::: */
     // $dataDeviceSlider.slick({
     //     slidesToShow: 3,
     //     slidesToScroll: 1,
     //     centerMode: false,
     //     infinite: false,
     //     swipeToSlide: true,
     //     swipe: true,
     //     arrows: true,
     //     dots: true,
     //     autoplay: false,
     //     initialSlide: 0,
     //     touchMove: false,
     //     draggable: false,
     //     // customPaging: function(slider, i) {
     //     //     return '<span class="slide-dot"><!-- dot --></span>';
     //     // },
     //     responsive: [{
     //         breakpoint: 1024,
     //         settings: {
     //             slidesToShow: 3,
     //             slidesToScroll: 1,
     //             arrows: true,
     //             dots: true
     //         }
     //     }, {
     //         breakpoint: 769,
     //         settings: {
     //             slidesToShow: 1,
     //             slidesToScroll: 1,
     //             arrows: false,
     //             infinite: false,
     //             dots: true
     //         }
     //     }]
     // });
 });


 /* auto scroll from hash */
 $(function() {
     // if (window.location.hash) scroll(0, 0);
     // setTimeout(function() {
     //     scroll(0, 0);
     // }, 1);

     // if (window.location.hash) {
     //     $('html, body').animate({
     //         scrollTop: $(window.location.hash).offset().top + 'px'
     //     }, 1000);
     // }

     var urlHash = window.location.hash;
     // console.log(urlHash);

     if (urlHash == "#telefony") {
         // console.log('nb');
         $('html, body').animate({
             scrollTop: $("#sec-6").offset().top + 'px'
         }, 1000);
     }

     if (urlHash == "#notebooky") {
         // console.log('nb');
         $('html, body').animate({
             scrollTop: $("#sec-6").offset().top + 'px'
         }, 1000);

         $('#link-tab3').parent('li').addClass('current').siblings().removeClass('current');
         $('#link-tab3').trigger('click');
     }

     if (urlHash == "#tablety") {
         // console.log('nb');
         $('html, body').animate({
             scrollTop: $("#sec-6").offset().top + 'px'
         }, 1000);

         $('#link-tab2').parent('li').addClass('current').siblings().removeClass('current');
         $('#link-tab2').trigger('click');
     }
 });


 /* TOGGLE ARROW */
 $('.toggle-arrow, .arrow-right').click(function(event) {
   event.preventDefault();
   if ($(this).hasClass('arrow-right')) {
     return;
   }
   $(event.target).closest('.togleArrow').find('.arrow-content').toggle(200);
   $(this).find('.arrow-right').toggleClass('arrow-rotate');
 });
 /* TOGGLE ARROW END */


 /* TABS */
$(".tabs-menu a").click(function(event) {
    event.preventDefault();
    $(this).parent().addClass("current");
    $(this).parent().siblings().removeClass("current");
    var tab = $(this).attr("href"), // select clicked tab. In example "#tab-1" ..
        parent = $(this).closest('ul').parent().parent(); // <tabs-container>
    parent.find('.tab-content').not(tab).css("display", "none"); //hide all not clicked tabs
    $(tab).fadeIn(); //show clicked tab


});
/* TABS END */

/* FROM TABS TO ACCORDION */
var dataSegment = $("[data-segment]").each(function(){
  //console.log($(this).attr('data-segment')); //zobraz "data-segment" hodnotu pre kazdu <section>

  closestHead = $($(this).find('.tabs-menu a'));
  closestItemCount = closestHead.length - 1; //vytiahni pocet tabov pre kazdu <section> s atributom "data-segment"
  //console.log(closestItemCount + "heads");

  closestContent = $($(this).find('.tab-content')); //najdi tab-content v danej <section>
  //console.log(closestContent);

  closestAccordion = $(this).find('.accordion'); //najdi .accordion v danej <section>

  accordionItem = '<div class="item">';

  for (var i = 0; i <= closestItemCount; i++) {
      accordionItem += '<div class="heading">' + $(closestHead[i]).text()  + '</div>';
      accordionItem += '<div class="content">' + $(closestContent[i]).html() + '</div></div>';

      if (i !== closestItemCount) {
          accordionItem += '<div class="item">';
      }
  }

  //if data-segment and data-accordion value match, show accordion data
  if($(this).attr('data-segment') === closestAccordion.attr('data-accordion')) {
       $(accordionItem).appendTo(closestAccordion);
  }

  var $items = $('.accordion .item');
  //SET OPENED ITEM
  $($items[0]).addClass('open');
  $($items[1]).addClass('open');
  $($items[2]).addClass('open');

});
/* FROM TABS TO ACCORDION END */

/* ACCORDION */
$('.accordion .item .heading').click(function(e) {

    var clickedHead = e.target;
    var $item = $(clickedHead).closest('.item');
    var isOpen = $item.hasClass('open');
    var $content = $item.find('.content');
    var $acPrice = $(clickedHead).find('.ac-price');

    if (isOpen) {
        $content.slideUp(200);
        $item.removeClass('open');
        $acPrice.show();
    } else {
        $content.slideDown(200);
        $item.addClass('open');
        $acPrice.hide();
    }
});


$('.accordion .item.open').find('.content').slideDown(200);
/* ACCORDION END */
