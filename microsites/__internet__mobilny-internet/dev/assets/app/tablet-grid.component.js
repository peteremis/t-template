'use strict';

// Register `tabletSlider` component, along with its associated controller and template
angular.
module('tabletGrid').
component('tabletGrid', {
    templateUrl: '../assets/app/tablet-grid.template.html',
    controller: ['$resource', 'dataService', '$timeout',
        function TabletListController($resource, dataService, $timeout) {
            var self = this;
            self.allTablets = [];
            self.tabletLoaded = false; // disable slick
            self.tabletUpdate = function() {
                dataService.allTablets().then(function(data) {
                    //console.log('recieved tablet data: ', data);
                    self.allTablets = data;
                    self.tabletLoaded = true; // enable slick
                });
            };
            self.currentSlideNum = 1;

            self.pclose = function(data) {
                $.fancybox.close();
                //console.log('popup close:');
            };
            //Init
            self.init = function() {
                //wait until tab2 is loaded
                $timeout(function() {
                    self.tabletUpdate();
                }, 500);
                // console.log('test tablet init');
            };
            // runs once per controller instantiation
            self.init();
        }
    ]
});
