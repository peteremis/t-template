'use strict';

// Register `tabletSlider` component, along with its associated controller and template
angular.
module('phoneSlider').
component('phoneSlider', {
    templateUrl: '../assets/app/phone-slider.template.html',
    controller: ['$resource', 'dataService', '$timeout',
        function PhoneListController($resource, dataService, $timeout) {
            var self = this;
            self.allPhones = [];
            self.phoneLoaded = false; // disable slick
            self.phoneUpdate = function() {
                dataService.allPhones().then(function(data) {
                    //console.log('recieved phone data: ', data);
                    self.allPhones = data;
                    self.phoneLoaded = true; // enable slick
                });
            };
            self.currentSlideNum = 1;
            self.slickPhoneConfig = {
                slidesToShow: 3,
                slidesToScroll: 1,
                centerMode: false,
                infinite: false,
                swipeToSlide: true,
                swipe: true,
                arrows: true,
                dots: true,
                autoplay: false,
                initialSlide: 0,
                touchMove: false,
                draggable: false,
                // customPaging: function(slider, i) {
                //     return '<span class="slide-dot"><!-- dot --></span>';
                // },
                responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                        arrows: true,
                        dots: false
                    }
                }, {
                    breakpoint: 769,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        arrows: false,
                        infinite: false,
                        dots: false
                    }
                }],
                method: {},
                event: {
                    beforeChange: function(event, slick, currentSlide, nextSlide) {},
                    afterChange: function(event, slick, currentSlide, nextSlide) {
                        self.currentSlideNum = currentSlide + 1; // save current index each time
                    },
                    init: function(event, slick) {
                        //slick.slickGoTo(self.currentIndex); // slide to correct index when init
                        self.allIndex = slick.slideCount;
                        if ($(window).width() < 769) {
                            $(".as1 .slick-dots li").css("width", 100 / $(".as1 .slick-dots li").length + '%');
                        }
                    }
                }
            };
            self.pclosePhone = function(data) {
                $.fancybox.close();
                //console.log('popup close:');
            };
            //Init
            self.init = function() {
                //wait until tab3 is loaded
                $timeout(function() {
                    self.phoneUpdate();
                }, 500);
                // console.log('test tablet init');
            };
            // runs once per controller instantiation
            self.init();
        }
    ]
});
