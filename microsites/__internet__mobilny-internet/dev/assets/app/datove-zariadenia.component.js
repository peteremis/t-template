'use strict';

// Register `tabletSlider` component, along with its associated controller and template
angular.
module('datoveZariadenia').
component('datoveZariadenia', {
    templateUrl: '../assets/app/datove-zariadenia.html',
    controller: ['$resource', 'dataService', '$timeout',
        function DatoveZariadeniaController($resource, dataService, $timeout) {
            var self = this;
            self.datoveZariadenia = [];
            self.zariadeniaLoaded = false; // disable slick
            self.zariadeniaUpdate = function() {
                dataService.datoveZariadenia().then(function(data) {
                    self.datoveZariadenia = data;
                    self.zariadeniaLoaded = true; // enable slick
                });
            };
            self.currentSlideNum = 1;
            self.slickDatoveZariadeniaConfig = {
                slidesToShow: 3,
                slidesToScroll: 1,
                centerMode: false,
                infinite: false,
                swipeToSlide: true,
                swipe: true,
                arrows: true,
                dots: true,
                autoplay: false,
                initialSlide: 0,
                touchMove: false,
                draggable: false,
                // customPaging: function(slider, i) {
                //     return '<span class="slide-dot"><!-- dot --></span>';
                // },
                responsive: [{
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                        arrows: true,
                        dots: false,
                    }
                }, {
                    breakpoint: 769,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        arrows: false,
                        infinite: false,
                        dots: false
                    }
                }],
                method: {},
                event: {
                    beforeChange: function(event, slick, currentSlide, nextSlide) {},
                    afterChange: function(event, slick, currentSlide, nextSlide) {
                        self.currentSlideNum = currentSlide + 1; // save current index each time
                    },
                    init: function(event, slick) {
                        self.allIndex = slick.slideCount;
                        if ($(window).width() < 769) {
                            $(".as2 .slick-dots li").css("width", 100 / $(".as2 .slick-dots li").length + '%');
                        };
                    }
                }
            };

            self.pcloseZariadenia = function(data) {
                $.fancybox.close();
                //console.log('popup close:');
            };

            // INIT
            self.init = function() {
                //wait until tab2 is loaded
                $timeout(function() {
                    self.zariadeniaUpdate();
                }, 500);
                //console.log('test nb init');
            };
            // runs once per controller instantiation
            self.init();
        }
    ]
});
