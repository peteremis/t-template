'use strict';

angular.module('core.device').service('dataService', ['$http',
    function($http) {
        var tabletsJson = '../assets/data/tablets.json';
        var notebooksJson = '../assets/data/notebooks.json';
        var phonesJson = '../assets/data/phones.json';
        var datoveZariadenia = '../assets/data/datove-zariadenia.json';
        this.allTablets = function() {
            return $http.get(tabletsJson, {
                cache: true
            }).then(function(res) {
                return res.data;
            });
        };
        this.allNotebooks = function() {
            return $http.get(notebooksJson, {
                cache: true
            }).then(function(res) {
                return res.data;
            });
        };
        this.allPhones = function() {
            return $http.get(phonesJson, {
                cache: true
            }).then(function(res) {
                return res.data;
            });
        };
        this.datoveZariadenia = function() {
            return $http.get(datoveZariadenia, {
                cache: true
            }).then(function(res) {
                return res.data;
            });
        };
    }
]);
