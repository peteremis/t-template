'use strict';

// Define the `vypredajHwApp` module
angular.module('tabletsListApp', [
	'core',
	'tabletSlider',
	'notebooksList',
	'tabletGrid',
	'phoneSlider',
	'datoveZariadenia'
]);
