$(function () {
    $('.info').qtip({
        content: 'Chytrý balík tvoria služby Magio televízia, internet alebo Pevná linka, ktoré sa dajú kúpiť samostatne alebo v kombinácii za výhodnejšiu cenu.',
        position: {
            my: 'center left',
            at: 'center right'
        }
    });
});

$(window).load(function () {
    $('.twenty__content').twentytwenty();
});
