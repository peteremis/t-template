$(document).ready(function() {
  function showB2b() {
    $("#b2b").addClass("is-checked");
    $("#b2c").removeClass("is-checked");
    $("#tab-b2b").show();
    $("#tab-b2c").hide();
    $("#hero").addClass("desk_b2b");
    $("#hero").removeClass("desk");
    $("#hero-mobile").addClass("hero__mobile_b2b");
    $("#hero-mobile").removeClass("hero__mobile");
    // set default tab
    $(".tab-selector").removeClass("is-checked");
    $(".my-tab-content").css("display", "none");
    $("#sec-akcie-b2b").css("display", "block");
    $("#but-b2b-a").addClass("is-checked");
  }
  function showB2c() {
    $("#b2c").addClass("is-checked");
    $("#b2b").removeClass("is-checked");
    $("#tab-b2c").show();
    $("#tab-b2b").hide();
    $("#hero").addClass("desk");
    $("#hero").removeClass("desk_b2b");
    $("#hero-mobile").addClass("hero__mobile");
    $("#hero-mobile").removeClass("hero__mobile_b2b");
    // set default tab
    $(".tab-selector").removeClass("is-checked");
    $(".my-tab-content").css("display", "none");
    $("#sec-akcie").css("display", "block");
    $("#but-b2c-a").addClass("is-checked");
  }

  /* TABS B2B B2C*/
  $("#b2b").click(function(event) {
    event.preventDefault();
    showB2b();
  });

  $("#b2c").click(function(event) {
    event.preventDefault();
    showB2c();
  });

  /* TABS */
  $(".tab-selector").click(function(event) {
    $(".tab-selector").removeClass("is-checked");
    $(this).addClass("is-checked");
    var tab = $(this).attr("href");
    $(".my-tab-content").css("display", "none");
    $(tab).show();
  });

  /* SCROLL */
  $(document).on("click", 'a[href^="#"]', function(event) {
    event.preventDefault();

    $("html, body").animate(
      {
        scrollTop: $($.attr(this, "href")).offset().top
      },
      1000
    );
  });
  /* GET PARAMETERS FROM URL */
  function getUrlParameter(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)");
    var results = regex.exec(location.search);
    return results === null
      ? ""
      : decodeURIComponent(results[1].replace(/\+/g, " "));
  }

  var segment = getUrlParameter("category");
  if (segment == "biznis") {
    showB2b();
  } else {
    showB2c();
  }
  var tab = getUrlParameter("tab");
  if (tab == "apka-b2c") {
    setTimeout(function() {
      $("#but-b2c-b").click();
    }, 200);
  }
  if (tab == "stores-b2c") {
    setTimeout(function() {
      $("#but-b2c-c").click();
    }, 200);
  }
});
