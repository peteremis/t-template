$(function () {
    var fixedLinkFull = $('.fixed-link__full'),
        fixedLinkCompact = $('.fixed-link__compact');
    
    if (localStorage.chatStat === 'compact') {
        fixedLinkFull.addClass('hidden');
        fixedLinkCompact.removeClass('hidden');
    }
        
    $('#toggle-chat').on('click', function () {
        fixedLinkFull.toggleClass('hidden');
        fixedLinkCompact.toggleClass('hidden');
        localStorage.chatStat = 'compact';
    });
    $('.fixed-link__compact').on('click', function () {
        fixedLinkFull.toggleClass('hidden');
        fixedLinkCompact.toggleClass('hidden');
        localStorage.chatStat = 'full';
    });
    $('#chat').on('click', function () {
        fixedLinkFull.toggleClass('hidden');
        livechatoo.embed.page('online');
    });
});

//////// CMB

localStorage.removeItem('wininv');

var fixedLink = document.getElementById('fixed-link'),
    livechat = document.getElementById('livechat'),
    chat = document.getElementById('chat'),
    cmb_callback = function (rule, cmb_type, img_name) {

    fixedLink.style.display = 'table-cell';
    livechat.style.display = 'table-cell';
    $(function () {
        var cmb__block = document.getElementById('cmb__block'),
            cmbBtn = document.getElementById('cmb__btn');
        if (cmb__block !== null) {
            cmb__block.style.display = 'inline-block';
            cmbBtn.onclick = function (e) {
                e.preventDefault();
                livechatoo.wininv.show(rule, img_name);
            };
        }
    });
    livechat.onclick = function () {
        livechatoo.wininv.show(rule, img_name);
    };
};

//////// CMB END

//////// CHAT

var chat_callback = function (data) {

    if (data.available) {
        fixedLink.style.display = 'table-cell';
        chat.style.display = 'table-cell';
    }
};

//////// CHAT END

livechatoo.wininv.init('telekom');
//livechatoo.wininv.status('telekom_manual', cmb_callback);
