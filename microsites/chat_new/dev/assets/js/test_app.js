$(function () {
    $('#toggle-chat').on('click', function () {
        $('.fixed-link__full').toggleClass('hidden');
        $('.fixed-link__compact').toggleClass('hidden');
    });
    $('.fixed-link__compact').on('click', function () {
        $('.fixed-link__full').toggleClass('hidden');
        $('.fixed-link__compact').toggleClass('hidden');
    });
    $('#chat').on('click', function () {
        $('.fixed-link__full').toggleClass('hidden');
        livechatoo.embed.page('online');
    });

    if ((document.getElementById('cmb__block') != null || document.getElementById('cmb__block') != undefined) && cmbBlockShow ) {
        document.getElementById('cmb__block').style.display = 'block';
        document.getElementById('cmb__btn').onclick = function () {
            livechatoo.wininv.show(rule, img_name);
        };
    }

});


var cmbBlockShow = false;

//////// CMB

var cmb_callback = function (rule, cmb_type, img_name) {

    cmbBlockShow = true;

    document.getElementById('fixed-link').style.display = 'table-cell';
    document.getElementById('livechat').style.display = 'table-cell';
    document.getElementById('livechat').onclick = function () {
        livechatoo.wininv.show(rule, img_name);
    };
}

//////// CMB END

//////// CHAT

var chat_callback = function (data) {

    if (data.available) {
        document.getElementById('fixed-link').style.display = 'table-cell';
        document.getElementById('chat').style.display = 'table-cell';
    }
}

//////// CHAT END

livechatoo.wininv.init('telekom');
livechatoo.wininv.status('telekom_test', cmb_callback);
