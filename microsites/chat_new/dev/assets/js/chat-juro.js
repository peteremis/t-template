$(function () {
    var fixedLinkFull = $('.fixed-link__full'),
        fixedLinkCompact = $('.fixed-link__compact');

    if (localStorage.chatStat === 'compact') {
        fixedLinkFull.addClass('hidden');
        fixedLinkCompact.removeClass('hidden');
    }

    $('#toggle-chat').on('click', function () {
        fixedLinkFull.toggleClass('hidden');
        fixedLinkCompact.toggleClass('hidden');
        localStorage.chatStat = 'compact';
    });
    $('.fixed-link__compact').on('click', function () {
        fixedLinkFull.toggleClass('hidden');
        fixedLinkCompact.toggleClass('hidden');
        localStorage.chatStat = 'full';
    });
    $('#chat').on('click', function () {
        fixedLinkFull.toggleClass('hidden');
        livechatoo.embed.page('online');
    });
});

//////// CMB

var fixedLink = document.getElementById('fixed-link'),
    livechat = document.getElementById('livechat'),
    chat = document.getElementById('chat'),
    addZero = function (i) {
        if (i < 10) {
            i = "0" + i;
        }
        return i;
    },
    currentdate = new Date(),
    currentHour = addZero(currentdate.getHours()) + "" + addZero(currentdate.getMinutes()),
    currentDay = currentdate.getDay(),
    offTime2 = "2130",
    offTime2End = "2200",
    callOffTime = function () {
        if (currentHour >= offTime2 && currentHour <= offTime2End) {
            return true;
        } else {
            return false;
        }
    },
    cmb_callback = function (rule, cmb_type, img_name) {

        if (fixedLink !== null) {
            fixedLink.style.display = 'table-cell';
        }

        if (livechat !== null) {
            livechat.style.display = 'table-cell';
        }

        $(function () {
            var cmb__block = document.getElementById('cmb__block'),
                cmbBtn = document.getElementById('cmb__btn');
            if (cmb__block !== null) {
                cmb__block.style.display = 'inline-block';
                cmbBtn.onclick = function (e) {
                    e.preventDefault();
                    livechatoo.wininv.show(rule, img_name);
                };
            }
        });
        livechat.onclick = function () {
            livechatoo.wininv.show(rule, img_name);
        };
    };

//////// CMB END

//////// CHAT

var chat_callback = function (data) {

    if (data.available) {
        fixedLink.style.display = 'table-cell';
        chat.style.display = 'table-cell';
    }
};

//////// CHAT END

livechatoo.wininv.init('ares');
livechatoo.wininv.status('juro_test_genesys', cmb_callback);



livechatoo.embed.init({
    lang: 'sk',
    layout: 'ares',
    side: 'right',
    side: 'ares',
    departments: '30',
    css_file: "https://www.juro.sk/templates/css/chat.css"
});