$(function () {
    var fixedLinkFull = $('.fixed-link__full'),
    fixedLinkCompact = $('.fixed-link__compact');

    if (localStorage.chatStat === 'compact') {
        fixedLinkFull.addClass('hidden');
        fixedLinkCompact.removeClass('hidden');
    }

    $('#toggle-chat').on('click', function () {
        fixedLinkFull.toggleClass('hidden');
        fixedLinkCompact.toggleClass('hidden');
        localStorage.chatStat = 'compact';
    });
    $('.fixed-link__compact').on('click', function () {
        fixedLinkFull.toggleClass('hidden');
        fixedLinkCompact.toggleClass('hidden');
        localStorage.chatStat = 'full';
    });
    $('#chat').on('click', function () {
        fixedLinkFull.toggleClass('hidden');
        livechatoo.embed.page('online');
    });
});

//////// CMB

var fixedLink = document.getElementById('fixed-link'),
livechat = document.getElementById('livechat'),
chat = document.getElementById('chat'),
addZero = function (i) {
    if (i < 10) {
        i = "0" + i;
    }
    return i;
},
currentdate = new Date(),
currentHour = addZero(currentdate.getHours()) + "" + addZero(currentdate.getMinutes()),
currentDay = currentdate.getDay(),
offTime2 = "2130",
offTime2End = "2200",
callOffTime = function () {
    if (currentHour >= offTime2 && currentHour <= offTime2End) {
        return true;
    } else {
        return false;
    }
},
cmb_callback = function (rule, cmb_type, img_name) {

    if ($('#chat-only').length == 0) {

        fixedLink.style.display = 'table-cell';
        livechat.style.display = 'table-cell';
        $(function () {
            var cmb__block = document.getElementById('cmb__block'),
            cmbBtn = document.getElementById('cmb__btn'),
            cmbProlong = document.getElementById('prolong-cmb'),
            cmbHomePage = document.getElementsByClassName('home-page-cmb');
            cmbLink1 = document.getElementsByClassName('cmb-link1');
            cmbLink2 = document.getElementsByClassName('cmb-link2');
            if (cmb__block !== null) {
                cmb__block.style.display = 'inline-block';

                if ($('#refresh__btn').length > 0) {
                    $('#refresh__btn').addClass('hidden');
                }

                cmbBtn.onclick = function (e) {
                    e.preventDefault();
                    livechatoo.wininv.show(rule, img_name);
                };
            }

            if ($('.cmb__link').length > 0) {
                $('.cmb__link').style.display = 'inline-block';
                $('.cmb__link').on('click', function (e) {
                    e.preventDefault();
                    livechatoo.wininv.show(rule, img_name);
                });
            }

            if (cmbProlong !== null) {
                cmbProlong.onclick = function (e) {
                    e.preventDefault();
                    livechatoo.wininv.show(rule, img_name);
                };
            }

            if (cmbLink1 !== null) {
                cmbLink1.onclick = function (e) {
                    e.preventDefault();
                    livechatoo.wininv.show(rule, img_name);
                };
            }

            if (cmbLink2 !== null) {
                cmbLink2.onclick = function (e) {
                    e.preventDefault();
                    livechatoo.wininv.show(rule, img_name);
                };
            }

            if (cmbHomePage !== null) {
                for (var i = 0; i < cmbHomePage.length; i++) {
                    cmbHomePage[i].onclick = function (e) {
                        e.preventDefault();
                        livechatoo.wininv.show(rule, img_name);
                    };
                }
            }
        });

        livechat.onclick = function () {
            livechatoo.wininv.show(rule, img_name);
        };

    }
};

//////// CMB END

//////// CHAT

var chat_callback = function (data) {

    if (data.available) {
        fixedLink.style.display = 'table-cell';
        chat.style.display = 'table-cell';
    }
};

//////// CHAT END

// livechatoo.wininv.init('telekom');
// livechatoo.wininv.status('telekom_manual', cmb_callback);
