$(function() {

    $('.popup').fancybox({
        padding: 10,
        margin: 0,
        parent: "#content",
        closeBtn: false,
        helpers : {
            overlay : {
                css : {
                    'background' : 'rgba(0, 0, 0, 0.7)'
                }
            }
        }
    });

    $('.close-end a').click(function(){
        $.fancybox.close();
    });

    $('#show_legal').click(function(){
        $(this).hide();
        $('#legal_more').show(200);
    });

    /* SEC-AKCIE LOAD CONTENT */
    $( ".sec-akcie" ).load( "templates/_aktualne-akcie.html", function() {
    });
    
});
