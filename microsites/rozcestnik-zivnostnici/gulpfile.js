var gulp            = require('gulp'),
    sass            = require('gulp-sass'),
    sourcemaps      = require('gulp-sourcemaps'),
    uglify          = require('gulp-uglify'),
    browserSync     = require('browser-sync').create(),
    reload          = browserSync.reload,
    includer        = require('gulp-html-ssi'),
    replace         = require('gulp-token-replace'),
    runSequence     = require('run-sequence');
    es              = require('event-stream'),
    globalAssets    = '../../global-assets/theme/stwbmain',
    globalTemplates = '../../global-assets/templates/*.html',
    localTemplates  = 'dev/templates/*.html';


gulp.task('htmlSSI', function() {
       es.merge(gulp.src(globalTemplates), gulp.src(localTemplates))
        .pipe(includer())
        .pipe(gulp.dest('dev/server/'));
});


gulp.task('sass', function () {
    return gulp.src('dev/assets/sass/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass.sync().on('error', sass.logError))
        .pipe(sass({outputStyle: 'compact'}))
        .pipe(sourcemaps.write('/maps'))
        .pipe(gulp.dest('dev/server/assets/css'));
});

gulp.task('js', function () {
    return gulp.src('dev/assets/js/*.js')
        .pipe(uglify())
        .on('error', console.error.bind(console))
        .pipe(gulp.dest('dev/server/assets/js'));
});

gulp.task('dev-replace-path-html', function(){
  var config = require('./config/dev-config.json');
  return gulp.src(['dev/server/*.html'])
    .pipe(replace({global:config}))
    .pipe(gulp.dest('dev/server/'))
});

gulp.task('dev-replace-path-css', function(){
  var config = require('./config/dev-config.json');
  return gulp.src(['dev/server/assets/css/*.css'])
    .pipe(replace({global:config}))
    .pipe(gulp.dest('dev/server/assets/css/'))
});

gulp.task('dev-replace-path-js', function(){
  var config = require('./config/dev-config.json');
  return gulp.src(['dev/server/assets/js/*.js'])
    .pipe(replace({global:config}))
    .pipe(gulp.dest('dev/server/assets/js/'))
});

gulp.task('dist-replace-path-html', function(){
  var config = require('./config/dist-config.json');
  return gulp.src(['dev/server/*.html'])
    .pipe(replace({global:config}))
    .pipe(gulp.dest('deploy/server/'))
});

gulp.task('dist-replace-path-css', function(){
  var config = require('./config/dist-config.json');
  return gulp.src(['dev/server/assets/css/*.css'])
    .pipe(replace({global:config}))
    .pipe(gulp.dest('deploy/server/assets/css/'))
});

gulp.task('dist-replace-path-js', function(){
  var config = require('./config/dist-config.json');
  return gulp.src(['dev/server/assets/js/*.js'])
    .pipe(replace({global:config}))
    .pipe(gulp.dest('deploy/server/assets/js/'))
});

gulp.task('dev-replace', ['dev-replace-path-html', 'dev-replace-path-js', 'dev-replace-path-css']);
gulp.task('dist-replace', ['dist-replace-path-html', 'dist-replace-path-js', 'dist-replace-path-css']);

gulp.task('serve', function () {
    
    browserSync.init({
        server: {
            baseDir: ["./dev", "./dev/server", "./../../global-assets"]
        }
    });
    
    gulp.watch('dev/assets/sass/**', ['reload-css']);
    gulp.watch('dev/assets/sass/**').on('change', reload);
    gulp.watch('../../global-assets/sass/**', ['reload-css']);
    gulp.watch('../../global-assets/sass/**').on('change', reload);
    gulp.watch('dev/assets/js/*.js', ['reload-js']);
    gulp.watch('dev/assets/js/*.js').on('change', reload);
    gulp.watch('dev/templates/*.html', ['reload-html']);
    gulp.watch('dev/templates/*.html').on('change', reload);
    gulp.src('dev/assets/img/**')
    .pipe(gulp.dest('dev/server/assets/img'));
});

gulp.task('prepare-deploy', ['sass', 'js', 'htmlSSI'], function() {
    return gulp.src('dev/templates/_main-content.html')
    .pipe(gulp.dest('deploy'));
    gulp.src('dev/server/assets/img/**')
    .pipe(gulp.dest('deploy/img'));
    gulp.src('dev/server/assets/js/main.js')
    .pipe(gulp.dest('deploy/assets'));
    gulp.src('dev/server/assets/css/style.css')
    .pipe(gulp.dest('deploy/assets'));
});

gulp.task('deploy', function(callback) {
    runSequence('prepare-deploy',
                'dist-replace',
                 callback);
});

gulp.task('reload-html', function(callback) {
    runSequence('htmlSSI',
                'dev-replace-path-html',
                 callback);
});

gulp.task('reload-css', function(callback) {
    runSequence('sass',
                'dev-replace-path-css',
                 callback);
});

gulp.task('reload-js', function(callback) {
    runSequence('js',
                'dev-replace-path-js',
                 callback);
});

gulp.task('default', function(callback) {
    runSequence(['sass', 'js', 'htmlSSI'],
                'dev-replace',
                'serve',
                 callback);
});