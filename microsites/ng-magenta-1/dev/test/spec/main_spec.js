describe('main', function() {

  // Load the module that contains the `phoneList` component before each test
  beforeEach(module('mag-1'));

  // Test the controller
  describe('MainController', function() {

    it('should create password with number 15', inject(function($componentController) {
      var ctrl = $componentController('main');

      expect(ctrl.pass).toBe(15);
    }));

  });

});