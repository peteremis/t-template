(function() {
    'use strict';

    angular
        .module('mag-1')
        .factory('appConfig', appConfig);

    appConfig.$inject = ['$rootScope'];
    function appConfig($rootScope) {
        var service = {
            setCalcInputVisibility: setCalcInputVisibility,
            getCalcInputVisibility: getCalcInputVisibility,
            setCurrStep: setCurrStep,
            getCurrStep: getCurrStep,
            increaseStep: increaseStep,
            decreaseStep: decreaseStep,
            initAppConfig: initAppConfig,
            setPageType: setPageType,
            getPageType: getPageType
        };

        return service;

        // Zakladne nastavenia aplikacie
        var calcInputVisibility;

        // Aktualny krok vo vyperovom procese
        var currStep;

        //Min a max step
        var minStep;
        var maxStep;

        // Typ stranky: crossroads alebo chooser
        var pageType;

        ////////////////

        function initAppConfig() {
            calcInputVisibility = true;
            currStep = 1;
            // Step moze byt v rozsahu 1-5
            minStep = 1;
            maxStep = 5;
        }

        function setCalcInputVisibility(val) {
            calcInputVisibility = val;
        }

        function getCalcInputVisibility() {
            return calcInputVisibility;
        }

        function setCurrStep(val) {
            currStep = val;
        }

        function getCurrStep() {
            return currStep;
        }

        function increaseStep() {
            if (currStep < maxStep) {
                currStep++;
                //Potrebne broadcastnut event, aby ostatne komponenty vedeli ze krok sa zmenil
                $rootScope.$broadcast('stepChanged');
            }
        }

        function decreaseStep() {
            if (currStep > minStep) {
                currStep--;
                //Potrebne broadcastnut event, aby ostatne komponenty vedeli ze krok sa zmenil
                $rootScope.$broadcast('stepChanged');
            }
        }

        function setPageType(val) {
            pageType = val;
        }

        function getPageType() {
            return pageType;
        }
    }
})();