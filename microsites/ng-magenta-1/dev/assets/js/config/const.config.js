(function() {
    'use strict';

    angular.module('mag-1')
            .constant('APP_CONST', {
                URL: {
                    API: 'http://localhost:7000/response',
                    DATA_JSON: '../assets/js/data/data.json',
                    CALC_INTRO: '/calc-intro',
                    CALC_CHOOSER: '/calc-chooser',
                    CALC_CROSSROADS: '/calc-crossroads',
                    CALC_INTERNET: '/calc-internet',
                    CALC_LANDLINE: '/calc-landline',
                    CALC_PLAN: '/calc-crossroads',
                    CALC_TV: '/calc-tv',
                    LANDING: '/landing'
                }
            });
})();