angular.module('mag-1').config(['$locationProvider', '$routeProvider', 'APP_CONST',
    function config($locationProvider, $routeProvider, APP_CONST) {
        $locationProvider.hashPrefix('!');

        $routeProvider.when(APP_CONST.URL.LANDING, {
            template: '<landing></landing>'
        }).when(APP_CONST.URL.CALC_INTRO, {
            template: '<calc-intro></calc-intro>'
        }).when(APP_CONST.URL.CALC_CROSSROADS, {
            template: '<calc-crossroads></calc-crossroads>'
        }).when(APP_CONST.URL.CALC_CHOOSER, {
            template: '<calc-chooser></calc-chooser>'
        }).otherwise(APP_CONST.URL.LANDING);
    }
]);