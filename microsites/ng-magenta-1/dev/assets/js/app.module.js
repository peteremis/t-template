(function() {
    'use strict';

    angular.module('mag-1', [
        'ngAnimate',
        'ngRoute'
        ])
    .run(['user', 'dataChooser', 'appConfig', function(user, dataChooser, appConfig) {
        // Inicializacia user service a user objectu pri spusteni aplikacie
        user.initService();
        dataChooser.initService();
        appConfig.initAppConfig();
    }]);
})();