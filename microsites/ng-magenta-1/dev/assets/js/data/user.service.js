(function() {
    'use strict';

    angular
        .module('mag-1')
        .factory('user', user);

    user.$inject = ['http', 'APP_CONST', 'localStorage', '$rootScope', 'appConfig'];

    function user(http, APP_CONST, localStorage, $rootScope, appConfig) {
        var service = {
            getUser: getUser,
            getUserMsid: getUserMsid,
            getUserDataFromServer: getUserDataFromServer,
            initService: initService,
            setUserMsid: setUserMsid,
            setUserProps: setUserProps,
            getUserGroup: getUserGroup,
            getServiceAccordingToStep: getServiceAccordingToStep,
            hasUserSelectedService: hasUserSelectedService,
            setService: setService,
            isKnownUser: isKnownUser,
            updateLocalStorage: updateLocalStorage
        };

        return service;

        var user;

        ////////////////

        $rootScope.$on('userDeleted', initService);

        function initService() {

            if (localStorage.getObj('user')) {
                user = localStorage.getObj('user');
            } else {

                // Inicializacia user objektu ak sa nenachadza v localStorage browseru
                user = {
                    msid: '',
                    plan: {
                        has: '',
                        interested: '',
                        frendHas: false,
                        notInterested: false
                    },
                    internet: {
                        has: '',
                        interested: '',
                        frendHas: false,
                        notInterested: false
                    },
                    tv: {
                        has: '',
                        interested: '',
                        frendHas: false,
                        notInterested: false
                    },
                    landLine: {
                        has: '',
                        interested: '',
                        frendHas: false,
                        notInterested: false
                    },
                    knownUser: false,
                    //User group moze byt podla identifikacie:
                    // - splna vsetky poziadavky, cize ma narok na mag1 (1)
                    // - nema narok na mag1 (2)
                    // - nie je telekom zakaznik (3)
                    // - ma mensi pausal ako m alebo easy (4)
                    group: 0
                }

                localStorage.setObj('user', user);
            }
        }

        function updateLocalStorage() {
            localStorage.setObj('user', user);
        }

        function setUserProps(type, key, value) {
            user[type][key] = value;
            updateLocalStorage();
        }

        function setUserMsid(msid) {
            // Nastavuje sa kvoli prvotnej inicializacii usera, pred ajaxcallom
            user.msid = msid;
        }

        function getUserDataFromServer(msid) {
            http.getData(APP_CONST.URL.API, msid)
                .then(function(response) {
                    normalizeUser(response.data);
                });
        }

        function normalizeUser(data) {
            // Normalizacia dat prichadzajuce z api
            user.msid = data.msid;
            user.plan.has = data.plan;
            user.internet.has = data.internet;
            user.tv.has = data.tv;
            user.landLine.has = data.landline;
            user.knownUser = true;

            //TODO: nastavit podmientku pre usergroupu
            //Zatial nastavene fixne na ma narok
            user.group = 1;

            localStorage.setObj('user', user);
        }

        function getUser() {
            return user;
        }

        function getUserMsid() {
            return user.msid;
        }

        function getUserGroup() {
            return user.group;
        }
        
        function getServiceAccordingToStep() {
            var currStep = appConfig.getCurrStep();

            switch(currStep) {
                case 1:
                    return user.plan;
                    break;
                case 2:
                    return user.internet;
                    break;
                case 3:
                    return user.tv;
                    break;
                case 4:
                    return user.landLine;
                    break;
            }
        }

        function hasUserSelectedService() {
            var service = getServiceAccordingToStep();

            if (service.has !== '' || service.interested !== '' || service.frendHas || service.notInterested) {
                return true;
            } else {
                return false;
            }
        }

        function setService(service, value) {
            var currStep = appConfig.getCurrStep();

            switch(currStep) {
                case 1:
                    return user.plan[service] = value;
                    break;
                case 2:
                    return user.internet[service] = value;
                    break;
                case 3:
                    return user.tv[service] = value;
                    break;
                case 4:
                    return user.landLine[service] = value;
                    break;
            }

            localStorage.setObj('user', user);
        }

        function isKnownUser() {
            return user.knownUser;
        }
    }
})();