(function() {
    'use strict';

    angular.module('mag-1')
        .constant('PLAN_CONST', {
            'SERVICE_BOX': [{show: true, active: false, value: 'MÁM MOBILNÝ PAUŠÁL'},
                            {show: true, active: false, value: 'MÁM ZÁUJEM O NOVÝ PAUŠÁL'},
                            {show: true, active: false, value: 'ZNÁMY MÁ TELEKOM'},
                            {show: true, active: false, value: 'NEMÁM ZÁUJEM'}]
        })
        .constant('INTERNET_CONST', {
            'SERVICE_BOX': [{show: true, active: false, value: 'MÁM INTERNET'},
                {show: true, active: false, value: 'MÁM ZÁUJEM O NOVÝ PAUŠÁL'},
                {show: true, active: false, value: 'ZNÁMY MÁ TELEKOM'},
                {show: true, active: false, value: 'NEMÁM ZÁUJEM'}]
        })
        .constant('TV_CONST', {
            'SERVICE_BOX': [{show: true, active: false, value: 'MÁM TV'},
                {show: true, active: false, value: 'MÁM ZÁUJEM O NOVÝ PAUŠÁL'},
                {show: true, active: false, value: 'ZNÁMY MÁ TELEKOM'},
                {show: true, active: false, value: 'NEMÁM ZÁUJEM'}]
        })
        .constant('LANDLINE_CONST', {
            'SERVICE_BOX': [{show: true, active: false, value: 'MÁM PEVNU'},
                {show: true, active: false, value: 'MÁM ZÁUJEM O NOVÝ PAUŠÁL'},
                {show: true, active: false, value: 'ZNÁMY MÁ TELEKOM'},
                {show: true, active: false, value: 'NEMÁM ZÁUJEM'}]
        });
})();