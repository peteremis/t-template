(function() {
    'use strict';

    angular
        .module('mag-1')
        .factory('dataChooser', dataChooser);

    dataChooser.$inject = ['http', 'APP_CONST', 'appConfig'];

    function dataChooser(http, APP_CONST, appConfig) {
        var service = {
            initService: initService,
            getData: getData,
            resetActivePropForCurrentService: resetActivePropForCurrentService,
            getDataAccordingToStep: getDataAccordingToStep,
            hasServiceAccordingToStepActiveProduct: hasServiceAccordingToStepActiveProduct
        };

        return service;

        var data;

        ////////////////

        function initService() {
                http.getData(APP_CONST.URL.DATA_JSON)
                .then(function(response) {
                    data = response.data;
                });
        }

        function getData() {
            return data;
        }

        function resetActivePropForCurrentService() {
            var tempData = getDataAccordingToStep();

            tempData.forEach(function(elem) {
                elem.active = false;
            })

            setDataAccordingToStep(tempData);

        }
        
        function getDataAccordingToStep() {
            var currStep = appConfig.getCurrStep();

            switch(currStep) {
                case 1:
                    return data.plan;
                    break;
                case 2:
                    return data.internet;
                    break;
                case 3:
                    return data.tv;
                    break;
                case 4:
                    return data.landLine;
                    break;
            }
        }

        function setDataAccordingToStep(serviceData) {
            var currStep = appConfig.getCurrStep();

            switch(currStep) {
                case 1:
                    return data.plan = serviceData;
                    break;
                case 2:
                    return data.internet = serviceData;
                    break;
                case 3:
                    return data.tv = serviceData;
                    break;
                case 4:
                    return data.landLine = serviceData;
                    break;
            }
        }

        function hasServiceAccordingToStepActiveProduct() {
            var tempData = getDataAccordingToStep();
            var active = 0;
            tempData.forEach(function(elem) {
                if(elem.active) {
                    active++;
                }
            })

            if(active > 0) {
                return true;
            } else {
                return false;
            }
        }

    }
})();