(function() {
    'use strict';

    angular
        .module('mag-1')
        .component('serviceCrossroads', {
            templateUrl: '../assets/js/components/service-crossroads/service-crossroads.component.html',
            controller: serviceCrossroadsController,
            controllerAs: '$ctrl'
        });

    serviceCrossroadsController.$inject = ['$location', '$rootScope', 'appConfig', 'dataChooser', 'user', 'APP_CONST', 'PLAN_CONST', 'INTERNET_CONST', 'TV_CONST', 'LANDLINE_CONST']
    function serviceCrossroadsController($location, $rootScope, appConfig, dataChooser, user, APP_CONST, PLAN_CONST, INTERNET_CONST, TV_CONST, LANDLINE_CONST) {
        var $ctrl = this;
        $ctrl.step = 0;
        $ctrl.serviceBoxLinks = [];

        var userGroup = 0;

        ////////////////

        //Definuje spravenie pre boxy na crossroads strankach
            //Prvotny case definuje cislo boxu na ktory user klikol
            //Druhy case resetuje vsetky hodnoty na false
            //Treti case definuje v ktorom kroku sa user nachadza
        $ctrl.redirect = function(link) {
            switch(link) {
                case 1:
                case 2:
                    $location.path(APP_CONST.URL.CALC_CHOOSER);
                    switch (appConfig.getCurrStep()){
                        case 1:
                            user.setUserProps('plan', 'frendHas', false);
                            user.setUserProps('plan', 'notInterested', false);
                            break;
                        case 2:
                            user.setUserProps('internet', 'frendHas', false);
                            user.setUserProps('internet', 'notInterested', false);
                            break;
                        case 3:
                            user.setUserProps('tv', 'frendHas', false);
                            user.setUserProps('tv', 'notInterested', false);
                            break;
                        case 4:
                            user.setUserProps('landLine', 'frendHas', false);
                            user.setUserProps('landLine', 'notInterested', false);
                            break;
                    }
                    break;
                case 3:
                    switch (appConfig.getCurrStep()){
                        case 1:
                            user.setUserProps('plan', 'frendHas', true);
                            user.setUserProps('plan', 'notInterested', false);
                            user.setUserProps('plan', 'interested', '');
                            break;
                        case 2:
                            user.setUserProps('internet', 'frendHas', true);
                            user.setUserProps('internet', 'notInterested', false);
                            user.setUserProps('internet', 'interested', '');
                            break;
                        case 3:
                            user.setUserProps('tv', 'frendHas', true);
                            user.setUserProps('tv', 'notInterested', false);
                            user.setUserProps('tv', 'interested', '');
                            break;
                        case 4:
                            user.setUserProps('landLine', 'frendHas', true);
                            user.setUserProps('landLine', 'notInterested', false);
                            user.setUserProps('landLine', 'interested', '');
                            break;
                    }
                    appConfig.increaseStep();
                    break;
                case 4:
                    switch (appConfig.getCurrStep()){
                        case 1:
                            user.setUserProps('plan', 'notInterested', true);
                            user.setUserProps('plan', 'frendHas', false);
                            user.setUserProps('plan', 'interested', '');
                            break;
                        case 2:
                            user.setUserProps('internet', 'notInterested', true);
                            user.setUserProps('internet', 'frendHas', false);
                            user.setUserProps('internet', 'interested', '');
                            break;
                        case 3:
                            user.setUserProps('tv', 'notInterested', true);
                            user.setUserProps('tv', 'frendHas', false);
                            user.setUserProps('tv', 'interested', '');
                            break;
                        case 4:
                            user.setUserProps('landLine', 'notInterested', true);
                            user.setUserProps('landLine', 'frendHas', false);
                            user.setUserProps('landLine', 'interested', '');
                            break;
                    }
                    appConfig.increaseStep();
                    break;
            }
            //Je potrebne nahrat zmeny do localstorage-u
            user.updateLocalStorage();
            resetActiveBoxes();
            initPage();
        }

        function setActiveBox() {
            var tempData = user.getServiceAccordingToStep();
            if (tempData.interested === '' && !tempData.frendHas && !tempData.notInterested) {
                $ctrl.serviceBoxLinks[0].active = true;
            } else if (tempData.interested !== '') {
                $ctrl.serviceBoxLinks[1].active = true;
            } else if (tempData.frendHas) {
                $ctrl.serviceBoxLinks[2].active = true;
            } else if (tempData.notInterested) {
                $ctrl.serviceBoxLinks[3].active = true;
            }
        }

        function resetActiveBoxes() {
            $ctrl.serviceBoxLinks.forEach(function(elem) {
                elem.active = false;
            })
        }

        function hideServiceBox(serviceBoxNum) {
            //Pri niektorych scenaroch treba skryt niektore serviceBox elementy
            //hodnota od 1 po serviceBox max
            $ctrl.serviceBoxLinks[serviceBoxNum - 1].show = false;
        }

        function loadConstants() {
            //Podla toho v akom kroku sme, nahra konstanty zo suboru crossroads.const.js pre rozcestnik

            switch(appConfig.getCurrStep()) {
                case 1:
                    $ctrl.serviceBoxLinks = PLAN_CONST.SERVICE_BOX;
                    break;
                case 2:
                    $ctrl.serviceBoxLinks = INTERNET_CONST.SERVICE_BOX;
                    break;
                case 3:
                    $ctrl.serviceBoxLinks = TV_CONST.SERVICE_BOX;
                    break;
                case 4:
                    $ctrl.serviceBoxLinks = LANDLINE_CONST.SERVICE_BOX;
                    break;
            }
        }

        $rootScope.$on('stepControlClicked',initPage);

        function initPage() {
            loadConstants();

            //Ak user nepatri do skupiny 1, cize sme ho nevedeli identifikovat,
            //tak na rozcestniku nezobrazujeme box "Mam pausal", "Mam internet", atd
            userGroup = user.getUserGroup();
            if (userGroup !== 1) {
                hideServiceBox(1);
            }

            setActiveBox();
        }

        $ctrl.$onInit = function() {
            initPage();
        };
        $ctrl.$onChanges = function() { };
        $ctrl.$onDestroy = function() { };
    }
})();