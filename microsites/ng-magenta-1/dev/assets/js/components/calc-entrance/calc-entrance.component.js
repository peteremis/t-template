(function() {
    'use strict';

    angular
        .module('mag-1')
        .component('calcEntrance', {
            templateUrl: '../assets/js/components/calc-entrance/calc-entrance.component.html',
            controller: calcEntranceController,
            controllerAs: '$ctrl'
        });

    calcEntranceController.$inject = ['$location', 'user', 'APP_CONST', 'appConfig'];

    function calcEntranceController($location, user, APP_CONST, appConfig) {
        var $ctrl = this;   

        ////////////////

        $ctrl.redirect = function() {
            appConfig.setCalcInputVisibility(false);
            $location.path(APP_CONST.URL.CALC_INTRO);
        }   

        $ctrl.sendMsid = function(msid) {
            if ($ctrl.form.$valid) {
                user.setUserMsid(msid);
                $ctrl.redirect();
            }
        } 

        $ctrl.$onInit = function() { };
        $ctrl.$onChanges = function() { };
        $ctrl.$onDestroy = function() { };
    }
})();