(function() {
    'use strict';

    angular
        .module('mag-1')
        .component('stepIndicator', {
            templateUrl: '../assets/js/components/step-indicator/step-indicator.component.html',
            controller: stepIndicatorController,
            controllerAs: '$ctrl'
        });

    stepIndicatorController.$inject = ['$rootScope', 'appConfig', 'localStorage'];

    function stepIndicatorController($rootScope, appConfig, localStorage) {
        var $ctrl = this;

        ////////////////

        function updateActiveElem() {
            var step = appConfig.getCurrStep();
            if (step > 0 || step < 6) {
                angular.element('.active-step').removeClass('active-step');
                angular.element('.step-' + step).addClass('active-step');
            }
        }

        $rootScope.$on('stepChanged', function() {
            updateActiveElem()
        });

        $ctrl.crearLocalStorage = function() {
            localStorage.clearData('user');
        }

        $ctrl.$onInit = function() {
            updateActiveElem();
        };
        $ctrl.$onChanges = function() { };
        $ctrl.$onDestroy = function() { };
    }
})();