(function() {
    'use strict';

    angular
        .module('mag-1')
        .component('phoneNum', {
            templateUrl: '../assets/js/components/phone-num/phone-num.component.html',
            controller: phoneNumController,
            controllerAs: '$ctrl'
        });

    phoneNumController.$inject = ['user'];
    function phoneNumController(user) {
        var $ctrl = this;
        $ctrl.msid = '';

        ////////////////

        $ctrl.$onInit = function() {
            $ctrl.msid = user.getUserMsid();
        };

        $ctrl.$onChanges = function() { };
        $ctrl.$onDestroy = function() { };
    }
})();