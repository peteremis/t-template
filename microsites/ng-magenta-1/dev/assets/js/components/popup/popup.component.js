(function() {
    'use strict';

    angular
        .module('mag-1')
        .component('popup', {
            templateUrl: '../assets/js/components/popup/popup.component.html',
            controller: popupNumController,
            controllerAs: '$ctrl'
        });

    function popupNumController() {
        var $ctrl = this;

        ////////////////

        /*
        $('.popup').fancybox({
        padding: 0,
        margin: 0,
        closeBtn: false,
        parent: "#content",
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.7)'
                }
            }
        }
    });
         */

        $ctrl.$onInit = function() {
            $('.popup').fancybox({
                padding: 0,
                margin: 0,
                closeBtn: false,
                parent: "#content",
                helpers: {
                    overlay: {
                        css: {
                            'background': 'rgba(0, 0, 0, 0.7)'
                        }
                    }
                }
            });
        };
        $ctrl.$onChanges = function() { };
        $ctrl.$onDestroy = function() { };
    }
})();