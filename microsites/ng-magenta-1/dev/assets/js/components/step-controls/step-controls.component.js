(function() {
    'use strict';

    angular
        .module('mag-1')
        .component('stepControls', {
            templateUrl: '../assets/js/components/step-controls/step-controls.component.html',
            controller: stepControlsController,
            controllerAs: '$ctrl'
        });

    stepControlsController.$inject = ['$location', '$rootScope','appConfig', 'APP_CONST', 'user'];
    function stepControlsController($location, $rootScope, appConfig, APP_CONST, user) {
        var $ctrl = this;
        var currPageType = '';
        var knownUser = false;

        ////////////////

        $ctrl.prev = function() {

            //Spat tlacitko zobrazuje iba stranku crossroads, stranku chooser ignoruje
            showOnlyCrossroads();

        }

        $ctrl.next = function() {

            if(knownUser) {
                showOnlyChooser()
            } else {
                showAllPages();
            }
        }

        function showOnlyCrossroads() {
            if (currPageType === 'crossroads') {
                appConfig.decreaseStep();
            } else if (currPageType === 'chooser') {
                $location.path(APP_CONST.URL.CALC_CROSSROADS);
            }

            $rootScope.$broadcast('stepControlClicked');
        }

        function showOnlyChooser() {
            if (currPageType === 'chooser') {
                appConfig.increaseStep();
            } else if (currPageType === 'crossroads') {
                $location.path(APP_CONST.URL.CALC_CHOOSER);
            }

            $rootScope.$broadcast('stepControlClicked');
        }

        function showAllPages() {
            if (currPageType === 'chooser') {
                if(appConfig.getCurrStep() < 5) {
                    $location.path(APP_CONST.URL.CALC_CROSSROADS);
                }
                appConfig.increaseStep();
            } else if (currPageType === 'crossroads') {
                if(user.getServiceAccordingToStep().frendHas || user.getServiceAccordingToStep().notInterested || !user.hasUserSelectedService()) {
                    $location.path(APP_CONST.URL.CALC_CROSSROADS);
                    appConfig.increaseStep();
                } else {
                    $location.path(APP_CONST.URL.CALC_CHOOSER);
                }

                if (!user.hasUserSelectedService()) {
                    user.setService('notInterested', true);
                }
            }
            $rootScope.$broadcast('stepControlClicked');
        }

        $ctrl.$onInit = function() {
            currPageType = appConfig.getPageType();
            knownUser = user.isKnownUser();
        };
        $ctrl.$onChanges = function() { };
        $ctrl.$onDestroy = function() { };
    }
})();