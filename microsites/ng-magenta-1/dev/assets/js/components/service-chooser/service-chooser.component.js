(function() {
    'use strict';

    angular
        .module('mag-1')
        .component('serviceChooser', {
            templateUrl: '../assets/js/components/service-chooser/service-chooser.component.html',
            controller: serviceChooserController,
            controllerAs: '$ctrl'
        });

    serviceChooserController.$inject = ['$rootScope', 'user', 'dataChooser', 'appConfig'];
    function serviceChooserController($rootScope, user, dataChooser, appConfig) {
        var $ctrl = this;
        $ctrl.data = null;

        ////////////////

        function stepPlan() {
            $ctrl.data = dataChooser.getData().plan;
            setActiveProp(user.getUser().plan.has);
        }

        function stepInternet() {
            $ctrl.data = dataChooser.getData().internet;
            setActiveProp(user.getUser().internet.has);
        }

        function stepTv() {
            $ctrl.data = dataChooser.getData().tv;
            setActiveProp(user.getUser().tv.has);
        }

        function stepLandLine() {
            $ctrl.data = dataChooser.getData().landLine;
            setActiveProp(user.getUser().landLine.has);
        }

        function setActiveProp(dataByType) {
            if (user.isKnownUser()) {
                $ctrl.data.forEach(function(elem) {
                    if(elem.shortName === dataByType) {
                        elem.active = true;
                    }
                })
            } else if (!dataChooser.hasServiceAccordingToStepActiveProduct()) {
                $ctrl.data[0].active = true;
            }
        }

        function resetDataActiveProp() {
            //Je potrebne resetnut aj priamo v service
            dataChooser.resetActivePropForCurrentService();
            $ctrl.data.forEach(function(elem) {
                elem.active = false;
            });
        }

        $ctrl.selectProduct = function(product) {
            resetDataActiveProp();
            product.active = true;
            user.setService('interested', product.shortName);
        }

        $rootScope.$on('stepControlClicked',initPage);

        function initPage() {
            var currStep = appConfig.getCurrStep();

            switch(currStep) {
                case 1:
                    stepPlan();
                    break;
                case 2:
                    stepInternet();
                    break;
                case 3:
                    stepTv();
                    break;
                case 4:
                    stepLandLine();
                    break;
            }
        }
        
        $ctrl.$onInit = function() {
            initPage();
        };
        $ctrl.$onChanges = function() { };
        $ctrl.$onDestroy = function() { };
    }
})();