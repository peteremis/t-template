(function() {
    'use strict';

    angular
        .module('mag-1')
        .component('calcIntro', {
            templateUrl: '../assets/js/pages/calc-intro/calc-intro.component.html',
            controller: calcIntroController,
            controllerAs: '$ctrl'
        });

    calcIntroController.$inject = ['$location', 'user', 'APP_CONST', 'appConfig'];
    function calcIntroController($location, user, APP_CONST, appConfig) {
        var $ctrl = this;

        $ctrl.showInput = appConfig.getCalcInputVisibility();
        
        ////////////////

        $ctrl.redirect = function(location) {
            $location.path(location);
        }

        $ctrl.sendMsid = function(msid) {
            if ($ctrl.formIntro.$valid && $ctrl.showInput) {
                user.setUserMsid(msid);
                $ctrl.redirect(APP_CONST.URL.CALC_PLAN);
            } else if (!$ctrl.showInput) {
                if (user.getUserGroup() === 1) {
                    $ctrl.redirect(APP_CONST.URL.CALC_CHOOSER);
                } else {
                    $ctrl.redirect(APP_CONST.URL.CALC_PLAN);
                }
            }
        }

        $ctrl.$onInit = function() {
            var localUser = user.getUser();

            if (localUser.msid) {
                user.getUserDataFromServer(localUser.msid);
            }
        };
        $ctrl.$onChanges = function() { };
        $ctrl.$onDestroy = function() { };
    }
})();