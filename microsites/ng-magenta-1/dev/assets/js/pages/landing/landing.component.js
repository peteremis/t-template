(function() {
    'use strict';

    angular
        .module('mag-1')
        .component('landing', {
            templateUrl: '../assets/js/pages/landing/landing.component.html',
            controller: LandingController,
            controllerAs: '$ctrl'
        });

    LandingController.$inject = ['$location', 'APP_CONST', 'appConfig'];
    function LandingController($location, APP_CONST, appConfig) {
        var $ctrl = this;

        ////////////////

        $ctrl.toCalc = function() {
            appConfig.setCalcInputVisibility(true);
            $location.path(APP_CONST.URL.CALC_INTRO);
        }

        $ctrl.$onInit = function() { };
        $ctrl.$onChanges = function() { };
        $ctrl.$onDestroy = function() { };
    }
})();