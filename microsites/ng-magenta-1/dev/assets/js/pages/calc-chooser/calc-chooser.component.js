(function() {
    'use strict';

    angular
        .module('mag-1')
        .component('calcChooser', {
            templateUrl: '../assets/js/pages/calc-chooser/calc-chooser.component.html',
            controller: calcChooserController,
            controllerAs: '$ctrl'
        });

    calcChooserController.$inject = ['appConfig'];
    function calcChooserController(appConfig) {
        var $ctrl = this;

        ////////////////

        $ctrl.$onInit = function() {
            appConfig.setPageType('chooser');
        };
        $ctrl.$onChanges = function() { };
        $ctrl.$onDestroy = function() { };
    }
})();