(function() {
    'use strict';

    angular
        .module('mag-1')
        .factory('http', http);

    http.$inject = ['$http', 'APP_CONST'];
    function http($http, APP_CONST) {
        var service = {
            getData:getData
        };
        
        return service;

        ////////////////
        function getData(url, msid) {
            return $http({
                method: 'GET',
                url: url,
                cache: true
            });
        }
    }
})();