(function() {
    'use strict';

    angular
        .module('mag-1')
        .factory('localStorage', localStorage);

    localStorage.$inject = ['$window', '$rootScope'];
    function localStorage($window, $rootScope) {
        var service = {
            setData: setData,
            setObj: setObj,
            getData: getData,
            getObj: getObj,
            clearData: clearData
        };

        return service;

        ////////////////

        function setData(key, value) {
            $window.localStorage.setItem(key, value);
        }

        function setObj(key, obj) {
            $window.localStorage.setItem(key, angular.toJson(obj));
        }
        
        function getData(key) {
            return $window.localStorage.getItem(key);
        }

        function getObj(key) {
            return angular.fromJson($window.localStorage.getItem(key));
        }

        function clearData(key) {
            $window.localStorage.removeItem(key);

            //Je potrebne skontrolovat, ci mazeme usera. Ak ano, tak je potrebna reinicializacia user objectu.
            if (key === 'user') {
                $rootScope.$broadcast('userDeleted');
            }
        }
    }
})();