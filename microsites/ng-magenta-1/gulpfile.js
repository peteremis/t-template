var gulp = require('gulp'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    uglify = require('gulp-uglify'),
    browserSync = require('browser-sync').create(),
    reload = browserSync.reload,
    includer = require('gulp-html-ssi'),
    runSequence = require('run-sequence'),
    es = require('event-stream'),
    replace = require('gulp-replace'),
    config = require('./config.json'),
    globalTemplates = '../../global-assets/templates/*.html',
    localTemplates = 'dev/templates/*.html',
    jasmineBrowser = require('gulp-jasmine-browser'),
    watch = require('gulp-watch'),
    jasmine = require('gulp-jasmine-livereload-task');
var print = require('gulp-print');
var gutil = require('gulp-util');
var rename = require('gulp-rename');
var cleanCSS = require('gulp-clean-css');
//var git = require('gulp-git');
var argv = require('yargs').argv;
var runSequence = require('run-sequence');
var prompt = require('gulp-prompt');
var embedTemplates = require('gulp-angular-embed-templates');
var copycat = require('gulp-copycat');
var concat = require('gulp-concat');
var del = require('del');
var htmlreplace = require('gulp-html-replace');
var gulpIgnore = require('gulp-ignore');
var myVar = 'update';

var ignoreFiles = '*.spec.js';

//Base tasks html + css + js
gulp.task('htmlSSI', function() {
    es.merge(gulp.src(globalTemplates), gulp.src(localTemplates))
        .pipe(includer())
        .pipe(gulp.dest('dev/server/'));
});

gulp.task('sass', function() {
    return gulp.src('dev/assets/sass/style.scss')
        .pipe(sass({ outputStyle: 'compact' }))
        .pipe(sourcemaps.init())
        .pipe(sass.sync().on('error', sass.logError))
        .pipe(sourcemaps.write('/maps'))
        .pipe(gulp.dest('dev/server/assets/css'))
        .pipe(browserSync.stream());
});

gulp.task('sass-deploy', function() {
    return gulp.src('dev/assets/sass/content.scss')
        .pipe(sass({ outputStyle: 'compact' }))
        .pipe(sass.sync().on('error', sass.logError))
        .pipe(gulp.dest('dev/server/assets/css'))
        .pipe(browserSync.stream());
});

gulp.task('js-build', function () {
    gulp.src(['dev/assets/lib/js/angular-main/*.js', 'dev/assets/lib/js/angular-plugins/*.js', 'dev/assets/js/app.module.js', 'dev/assets/js/config/**/*.js', 'dev/assets/js/data/**/*.js', 'dev/assets/js/services/**/*.js', 'dev/assets/js/pages/**/*.js', 'dev/assets/js/components/**/*.js'])
        .pipe(gulpIgnore.exclude(ignoreFiles))
        .pipe(replace(/\.\.\/assets\/js\/components\/(?![^\/\n]+\.html?$)[^\/\n]+\/?/gm, ''))
        .pipe(embedTemplates())
        .pipe(concat('app.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('deploy'));
});

gulp.task('replace-app', function() {
    gulp.src('deploy/_main-content.html')
    .pipe(htmlreplace({js: config.codeFolder + 'app.min.js'}))
    .pipe(gulp.dest('deploy'))
});

gulp.task('js', ['js-lib'], function() {
    return gulp.src(['dev/assets/js/**/*.js', 'dev/assets/js/**/*.html'])
        .on('error', console.error.bind(console))
        .pipe(gulp.dest('dev/server/assets/js'));
});

gulp.task('js-lib', function() {
    return gulp.src('dev/assets/lib/**/*.js')
        .on('error', console.error.bind(console))
        .pipe(gulp.dest('dev/server/assets/lib/'));
});

//Browser-sync & watcher
//Static server

gulp.task('browser-sync', ['sass', 'js', 'htmlSSI'], function() {
    browserSync.init({
        server: {
            baseDir: ["./dev", "./dev/server", "./../../global-assets"]
        }
    });
});

//Novy zaklad: gulp watch
gulp.task('watch', ['browser-sync'], function() {
    gulp.watch('dev/assets/sass/*.scss', ['sass']);
    gulp.watch('../../global-assets/sass/*.scss', ['sass']);
    gulp.watch('dev/assets/js/**/*.js', ['js']);
    gulp.watch('dev/assets/js/**/*.js').on('change', reload);
    gulp.watch('dev/templates/*.html', ['htmlSSI']);
    gulp.watch('dev/server/*.html').on('change', reload);
    gulp.src('dev/assets/img/**')
        .pipe(gulp.dest('dev/server/assets/img'));
});

//deploy to dist convert and back
//convert deploy 2 dev html
gulp.task('conv', function() {
    gulp.src(['convert/_input.html'])
        .pipe(replace(config.imgFolder, '../assets/img/'))
        .pipe(replace(config.codeFolder, '../assets/js/'))
        .pipe(gulp.dest('convert/dev/'));
});
//convert dev 2 deploy html
gulp.task('conv-back', function() {
    gulp.src(['convert/dev-src/_input.html'])
        .pipe(replace('../assets/img/', config.imgFolder))
        .pipe(replace('../assets/js/', config.codeFolder))
        .pipe(gulp.dest('convert/deploy/'));
});

//Deploy helpers

gulp.task('dist-replace-path-html', function() {
    return gulp.src(['dev/templates/_main-content.html'])
        .pipe(replace('../assets/img/', config.imgFolder))
        .pipe(replace('../assets/js/', config.codeFolder))
        .pipe(gulp.dest('deploy'));
});

gulp.task('dist-replace-path-css', function() {
    gulp.src(['dev/server/assets/css/*.css'])
        .pipe(replace('../img/', config.imgFolder))
        .pipe(gulp.dest('deploy'));
});

gulp.task('dist-replace-path-css-deploy', function() {
    gulp.src(['dev/server/assets/css/content.css'])
        .pipe(replace('../img/', config.imgFolder))
        .pipe(gulp.dest('deploy'));
});

gulp.task('dist-replace-path-js', function() {
    gulp.src(['dev/server/assets/js/*.js'])
        .pipe(replace('../img/', config.imgFolder))
        .pipe(gulp.dest('deploy'));
});

gulp.task('prepare-deploy', ['sass', 'htmlSSI'], function() {
    return gulp.src('dev/templates/_main-content.html')
        .pipe(gulp.dest('deploy'));
});

gulp.task('dist-replace', ['dist-replace-path-html', 'dist-replace-path-css']);
gulp.task('dist-replace-deploy', ['dist-replace-path-html', 'dist-replace-path-css-deploy']);

//Print CSS path

gulp.task('print', function() {
    gulp.src('dev/server/assets/css/style.css')
        .pipe(print(function(filepath) {
            return "\n@import url(" + config.globalCSS + "global-core.css);\n@import url(" + config.codeFolder + "content.css?v=1);";
        }))
});

//minify Css
gulp.task('minify:css', function() {
    return gulp.src('deploy/content.css')
        .pipe(cleanCSS())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest('deploy'));
});

//Deploy

gulp.task('deploy', function(callback) {
    runSequence(
        'del-deploy',
        'sass-deploy',
        'prepare-deploy',
        'js-build',
        'dist-replace-deploy',
        'replace-app',
        'print',
        callback);
});

gulp.task('del-deploy', function() {
    del(['deploy/**']);
});


//Serve

gulp.task('serve', function() {

    browserSync.init({
        server: {
            baseDir: ["./dev", "./dev/server", "./../../global-assets"]
        }
    });

    gulp.watch('dev/assets/sass/*.scss', ['sass']);
    gulp.watch('../../global-assets/sass/*.scss', ['sass']);
    gulp.watch('dev/assets/js/**/*.js', ['js']);
    gulp.watch('dev/assets/js/**/*.js').on('change', reload);
    gulp.watch('dev/assets/js/**/*.html', ['js']);
    gulp.watch('dev/assets/js/**/*.html').on('change', reload);
    gulp.watch('dev/templates/*.html', ['htmlSSI']);
    gulp.watch('dev/server/*.html').on('change', reload);
    gulp.src('dev/assets/img/**')
        .pipe(gulp.dest('dev/server/assets/img'));
});

// DEFAUL Task

gulp.task('default', function(callback) {
    runSequence(['sass', 'js', 'htmlSSI'],
        'serve',
        callback);
});


//Git tasky

gulp.task('otazka', function() {
    // just source anything here - we just wan't to call the prompt for now
    return gulp.src('config.json')
        .pipe(prompt.prompt({
            type: 'input',
            name: 'commit',
            message: 'Please enter commit message...'
        }, function(res) {
            myVar = res.commit;
    }));
});

gulp.task('add', function() {
    console.log('adding...');
    return gulp.src('./*')
        .pipe(git.add());
});

gulp.task('commit', function() {
    return gulp.src('./*')
        .pipe(git.commit([config.pageUrl, myVar]));
});

gulp.task('push', function() {
    console.log('pushing...');
    git.push('origin', 'master', function(err) {
        if (err) throw err;
    });
});

gulp.task('git', function(callback) {
    runSequence('otazka', 'add', 'commit', 'push', callback);
});

//  Jasmine tests


var testOptions = {
 tags: {
    source: [
    {
        begin: /<!--\s*ccs:test\s*-->/gi, // beginning of source tag: <!-- ccs:name -->
        end: /<!--\s*\/ccs:test\s*-->/gi	 // end of source tag: <!-- /ccs:name -->
    }
    ],
    dest: [
    {
        begin: /<!--\s*ccd:test\s*-->/gi, // beginning of destination tag: <!-- ccd:name -->
        end: /<!--\s*\/ccd:test\s*-->/gi	 // end of destination tag: <!-- /ccd:name -->
    }
    ],
 }
};


gulp.task('copySourcesToTest', function() {
    return gulp.src(['dev/templates/_main-content.html', 'dev/test/test.html'])
            .pipe(copycat())
            .pipe(concat('index.html'))
            .pipe(gulp.dest('dev/test/'))
});

gulp.task('test-server', function() {
    browserSync.init({
        server: {
            baseDir: ["./dev/test", "./dev/"]
        }
    });

    var filesForTest = ['dev/server/assets/js/*.js', 'dev/server/assets/js/*_spec.js'];
    gulp.src(filesForTest)
        .pipe(jasmineBrowser.specRunner());
});

gulp.task('watch-test', function() {
    gulp.watch('dev/assets/js/**/*.js').on('change', reload);
    gulp.watch('dev/test/spec/**/*.js').on('change', reload);
});

gulp.task('test', function() {
    runSequence('copySourcesToTest', 'test-server', 'watch-test');
});
