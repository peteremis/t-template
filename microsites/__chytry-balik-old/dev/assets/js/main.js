$(document).ready(function() {
  // setTimeout(function() {
  //     var url = window.location.href;
  //     var tabPos = url.indexOf('#tab-');
  //     if (tabPos >= 0)
  //     {
  //         var tabId = url.substr(tabPos, 6);
  //         $('.tabs-menu a').each(function(){
  //             if ($(this).attr('href') == tabId)
  //             {
  //                 $(this).parent().addClass("current");
  //                 $(this).parent().siblings().removeClass("current");
  //                 var parent = $(this).closest('ul').parent().parent();
  //                 parent.find('.tab-content').not(tabId).css("display", "none");
  //                 $(tabId).fadeIn();
  //                 $('html, body').animate({
  //                     scrollTop: ($("#sec-4").offset().top - 75) + 'px'
  //                 }, 1000);
  //                 return false;
  //             }
  //         });
  //     }
  // }, 300);

  $("#nav-sticky").sticky({
    topSpacing: 0,
    widthFromWrapper: true
  });

  $(".scroll-to").click(function() {
    if (
      location.pathname.replace(/^\//, "") ==
        this.pathname.replace(/^\//, "") &&
      location.hostname == this.hostname
    ) {
      var target = $(this.hash);
      target = target.length ? target : $("[name=" + this.hash.slice(1) + "]");
      if (target.length) {
        $("html,body").animate(
          {
            scrollTop: target.offset().top - $("#nav-sticky").outerHeight()
          },
          1000
        );
        return false;
      }
    }
  });

  var contentSections = $("[id*='sec-']"),
    secondaryNav = $("#nav-sticky");

  function updateSecondaryNavigation() {
    contentSections.each(function() {
      var actual = $(this),
        actualHeight =
          actual.height() +
          parseInt(actual.css("paddingTop").replace("px", "")) +
          parseInt(actual.css("paddingBottom").replace("px", "")),
        actualAnchor = secondaryNav.find(
          'a[href="#' + actual.attr("id") + '"]'
        );

      if (
        actual.offset().top - secondaryNav.outerHeight() <=
          $(window).scrollTop() &&
        actual.offset().top + actualHeight - secondaryNav.outerHeight() >
          $(window).scrollTop()
      ) {
        actualAnchor.addClass("active");
      } else {
        actualAnchor.removeClass("active");
      }
    });
  }
  updateSecondaryNavigation();

  $(window).scroll(function(event) {
    updateSecondaryNavigation();
  });

  /* TABS */
  $(".tabs-menu a").click(function(event) {
    event.preventDefault();
    $(this)
      .parent()
      .addClass("current");
    $(this)
      .parent()
      .siblings()
      .removeClass("current");
    var tab = $(this).attr("href"), // select clicked tab. In example "#tab-1" ..
      parent = $(this)
        .closest("ul")
        .parent()
        .parent(); // <tabs-container>
    parent
      .find(".tab-content")
      .not(tab)
      .css("display", "none"); //hide all not clicked tabs
    $(tab).fadeIn(); //show clicked tab
  });
  /* TABS END */

  /* FROM TABS TO ACCORDION */
  var $accordion = $(".accordion"),
    $heads = $(".tabs-menu a"),
    $contents = $(".tab-content"),
    itemCount = $($heads).length - 1,
    accordionCode = '<div class="item">';

  for (var i = 0; i <= itemCount; i++) {
    accordionCode += '<div class="heading">' + $($heads[i]).text() + "</div>";
    accordionCode +=
      '<div class="content">' + $($contents[i]).html() + "</div></div>";

    if (i !== itemCount) {
      accordionCode += '<div class="item">';
    }
  }

  $(accordionCode).appendTo($accordion);

  var $items = $(".accordion .item");
  //SET OPENED ITEM
  $($items[0]).addClass("open");
  /* FROM TABS TO ACCORDION END */

  /* ACCORDION */
  $(".accordion .item .heading").click(function(e) {
    var clickedHead = e.target;
    var $item = $(clickedHead).closest(".item");
    var isOpen = $item.hasClass("open");
    var $content = $item.find(".content");
    var $acPrice = $(clickedHead).find(".ac-price");

    if (isOpen) {
      $content.slideUp(200);
      $item.removeClass("open");
      $acPrice.show();
    } else {
      $content.slideDown(200);
      $item.addClass("open");
      $acPrice.hide();
    }
  });

  $(".accordion .item.open")
    .find(".content")
    .slideDown(200);
  /* ACCORDION END */

  var toggleTabs = function() {
    $(".tab__variant").toggleClass("selected");
  };

  $(".popup").fancybox({
    padding: 10,
    margin: 0,
    parent: "#content",
    closeBtn: false,
    helpers: {
      overlay: {
        css: {
          background: "rgba(0, 0, 0, 0.7)"
        }
      }
    }
  });

  $(".close-end a").click(function() {
    $.fancybox.close();
  });

  $("#content .tab .tab-acc-sub").click(function() {
    window.location.href = "/" + $(this).attr("data-href");
  });

  $("#show_legal").click(function() {
    $(this).hide();
    $("#legal_more").show(200);
  });

  /* SEC-AKCIE LOAD CONTENT */
  $(".sec-akcie").load("templates/_aktualne-akcie.html", function() {});
});
