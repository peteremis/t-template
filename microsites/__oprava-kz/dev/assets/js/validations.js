$(function () {
  var $form = $("#myForm");
  var sendFormBtn = $(".sendfrm");
  var printFormBtn = $(".printfrm");
  // initialize the validation plugin
  var formValidator = $form.validate({
    rules: {
      businessName: {
        required: true,
      },
      residence: {
        required: true,
      },
      contactName: {
        required: true,
      },
      device: {
        required: true,
      },
      ico: {
        required: true,
        number: true,
      },
      dateSale: {
        required: true,
      },
      imei: {
        required: true,
        minlength: 14,
      },
      contactEmail: {
        required: true,
        email: true,
        customEmailValidation: true,
      },
      mail: {
        required: true,
        email: true,
        customEmailValidation: true,
      },
      contactPhoneNum: {
        required: true,
        customNumberValidation: true,
        maxlength: 13,
      },
      replacementDevice: {
        required: true,
      },
      repair: {
        required: true,
      },
      lock: {
        required: true,
      },
      warranty: {
        required: true,
      },
      failure: {
        required: true,
      },
    },
    messages: {
      businessName: {
        required: "Vyplňte obchodné meno.",
      },
      ico: {
        required: "Vyplňte IČO.",
      },
      residence: {
        required: "Vyplňte adresu sídla firmy.",
      },
      contactName: {
        required: "Vyplňte meno kontaktnej osoby pre kuriéra.",
      },
      contactPhoneNum: {
        required: "Vyplňte mobilné telefónne číslo.",
        customNumberValidation:
          "Vyplňte tel. číslo v tvare 0911222333 alebo +421911222333.",
        maxlength: "Telefónne číslo môže obsahovať maximálne 13 číslic.",
      },
      contactEmail: {
        required: "Vyplňte kontaktný e-mail.",
        email: "Vyplňte e-mail v tvare vas@email.sk",
        customEmailValidation: "Vyplňte email v správnom tvare: vas@email.sk",
      },
      mail: {
        required: "Vyplňte e-mail splnomocnenej osoby pre ST.",
        email: "Vyplňte e-mail v tvare vas@email.sk",
        customEmailValidation: "Vyplňte email v správnom tvare: vas@email.sk",
      },
      device: {
        required: "Vyplňte typ zariadenia",
      },
      dateSale: {
        required: "Vyplňte dátum predaja zariadenia.",
      },
      imei: {
        required: "Vyplňte IMEI číslo zariadenia.",
      },
      replacementDevice: {
        required: "Vyberte si jednu z možností",
      },
      repair: {
        required: "Vyberte si jednu z možností",
      },
      lock: {
        required: "Vyberte si jednu z možností",
      },
      warranty: {
        required: "Vyberte si jednu z možností",
      },
    },
    errorClass: "error",
    validClass: "valid",
    highlight: function (element, errorClass, validClass) {
      $(element)
        .parents(".form-group")
        .addClass(errorClass)
        .removeClass(validClass);
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element)
        .parents(".form-group")
        .removeClass(errorClass)
        .addClass(validClass);
    },
  });

  // custom phone number validation rule
  $.validator.addMethod("customNumberValidation", function (value, element) {
    if ($(element).val() === "") {
      return true;
    } else {
      return /^(((\+4219)|(09))[0-9]{2}\s*[0-9]{3}\s*[0-9]{3})$/.test(value);
    }
  });

  // custom email validation rule
  $.validator.addMethod("customEmailValidation", function (value, element) {
    return (
      /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
        value
      ) && /^(?=.{1,64}@.{4,64}$)(?=.{6,100}$).*/.test(value)
    );
  });

  function checkFailure() {
    if (
      $("#failure1").prop("checked") == false &&
      $("#failure2").prop("checked") == false &&
      $("#failure3").prop("checked") == false &&
      $("#failure4").prop("checked") == false &&
      $("#failure5").prop("checked") == false &&
      $("#failure6").prop("checked") == false &&
      $("#failure7").prop("checked") == false &&
      $("#failure8").prop("checked") == false &&
      $("#failure9").prop("checked") == false &&
      $("#failure10").prop("checked") == false &&
      $("#failure11").prop("checked") == false &&
      $("#failure12").prop("checked") == false &&
      $("#failure13").val() == ""
    ) {
      $("#failure").addClass("error").removeClass("valid");
      $("#form-message").html(
        "<b class='error' >Vyberte si jednu z možností</b>"
      );
      return false;
    } else {
      $("#failure").addClass("valid").removeClass("error");
      $("#form-message").html("");
      return true;
    }
  }
  // SUBMIT FORM
  sendFormBtn.on("click", function (e) {
    e.preventDefault();
    //check if form is valid & send form ...
    var sendFall = checkFailure();
    if ($form.valid() == true && sendFall == true) {
      var data = $form.serialize();
      $.ajax({
        url: "https://mb.telekom.sk/api/service-request",
        method: "POST",
        data: data,
        cache: false,
        complete: function () {
          $form.hide();
          var sucessMessage =
            "<div class='row'><div class='col-md-12 send-message'><strong>Formúlár bol úspešne odoslaný.</strong></div></div>";
          $("#detail").html(sucessMessage);
          // window.location.href =
          //   "https://backvm.telekom.sk/www/app-cmb/h2a/mam-zaujem/";
        },
        error: function (er) {
          console.log(er);
        },
      });
    } else {
      e.preventDefault();
    }
  });
  // PRINT FORM
  printFormBtn.on("click", function (e) {
    e.preventDefault();
    var printFall = checkFailure();
    if ($form.valid() == true && printFall == true) {
      $form.submit();
    }
  });
});
