$(function () {

    var currentdate = new Date();
    var currentHour = addZero(currentdate.getHours()) + "" + addZero(currentdate.getMinutes());
    var currentDay = currentdate.getDay();

    function addZero(i) {
        if (i < 10) {
            i = "0" + i;
        }
        return i;
    }

    var offTime1 = "0830",
        offTime2 = "2130";

    var callOffTime = function () {
        if (currentHour < offTime1 || currentHour > offTime2 || currentDay == 0) {
            return true;
        } else {
            return false;
        }
    }

    /*    var liveChatPresent = function () {
            if (($('#livechat').length) > 0) {
                return true;
            } else {
                return false;
            }
        }*/

    var showContainer = function () {
        $('.fixed-link__full').css('display', 'table-cell');
    }

    var showChat = function () {
        document.getElementById('chat').style.display = 'table-cell';
    }

    var showLiveChat = function () {
        document.getElementById('livechat').style.display = 'table-cell';
    }

    livechatoo.wininv.init('telekom');

    
    var rules = livechatoo.wininv.rules;
    var currentRuleID = null;
    
    for (key in rules) {
        if (rules[key].name == 'telekom_manual') {
            currentRuleID = key;
        }
    }

    $('#livechat').click(function () {
        livechatoo.wininv.show(livechatoo.wininv.rules[parseInt(currentRuleID)]);
        return false;
    });

    window.chat_onload_callback = function (data) {
        // data.available dostupne od verzii v1.7.2 a v1.8.1

        //var result = liveChatPresent();
        var result = callOffTime();
        if (data.available == true && result == true) {
            showContainer();
            showChat();
        } else if (data.available == false && result == false || data.available == undefined && result == false) {
            showContainer();
            showLiveChat();
        } else if (data.available == true && result == false) {
            showContainer();
            showChat();
            showLiveChat();
        }
    }

    $('#toggle-chat').on('click', function () {
        $('.fixed-link__full').toggleClass('hidden');
        $('.fixed-link__compact').toggleClass('hidden');
    });

    $('.fixed-link__compact').on('click', function () {
        $('.fixed-link__full').toggleClass('hidden');
        $('.fixed-link__compact').toggleClass('hidden');
    });

    $('#chat').on('click', function () {
        $('.fixed-link__full').toggleClass('hidden');
    });

});
