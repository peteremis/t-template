$(function() {
  // PERSONALIZED VIDEO - START
  function getQueryParams(qs) {
    qs = qs.split("+").join(" ");
    var params = {},
      tokens,
      re = /[?&]?([^=]+)=([^&]*)/g;
    while ((tokens = re.exec(qs))) {
      params[decodeURIComponent(tokens[1])] = decodeURIComponent(tokens[2]);
    }
    return params;
  }

  var queryParam = getQueryParams(document.location.search);
  var videoHolder = $("#personalizedPlayer");

  if (queryParam.pvq !== undefined) {
    var splitted = queryParam.pvq.split("-");
    var video_url =
      "https://fubuki.blob.core.windows.net/output/" +
      splitted[0] +
      "/" +
      splitted[1] +
      ".mp4";

    videoHolder.attr(
      "poster",
      "https://fubuki.blob.core.windows.net/output/" +
        splitted[0] +
        "/" +
        splitted[1] +
        ".jpeg"
    );

    let source = $(
      '<video class="personalised-video" width="100%" height="auto" controls autoplay><source type="video/mp4"></source></video>'
    );

    source.attr("src", video_url);
    videoHolder.children("source").remove();
    videoHolder.append(source);
  }
  // PERSONALIZED VIDEO - END
});
