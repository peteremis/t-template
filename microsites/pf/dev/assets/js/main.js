$(function() {
    // video player
    $(".video-container").click(function() {
        $(".video-embed").css({ "opacity": "1", "display": "block" });
        $(".video-embed")[0].src += "&autoplay=1";
        $(".play_btn").css({ "opacity": "0", "display": "none" });
        $(this).unbind("click");
    });
    $(".play_btn").click(function() {
        $(".video-embed").css({ "opacity": "1", "display": "block" });
        $(".video-embed")[0].src += "&autoplay=1";
        $(".play_btn").css({ "opacity": "0", "display": "none" });
        $(this).unbind("click");
    });
});
