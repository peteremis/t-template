/* GET PARAMETERS FROM URL */
function getUrlParameter(name) {
  name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
  var regex = new RegExp("[\\?&]" + name + "=([^&#]*)");
  var results = regex.exec(location.search);
  return results === null
    ? ""
    : decodeURIComponent(results[1].replace(/\+/g, " "));
}

$(document).ready(function () {
  $(".lightbox").fancybox();
  // Other address
  $("#other-address").change(function () {
    if ($("#other-address").is(":checked")) {
      $("#delivery-adress").show();
    } else {
      $("#delivery-adress").hide();
    }
  });
  // Archiv
  $("#hbo").change(function () {
    if ($("#hbo").is(":checked")) {
      if ($("#hbogo").is(":checked")) {
        $("#hbogo").click();
      }
    }
  });
  $("#hbogo").change(function () {
    if ($("#hbogo").is(":checked")) {
      if ($("#hbo").is(":checked")) {
        $("#hbo").click();
      }
    }
  });
  // Archiv
  $("#archiv-basic").change(function () {
    if ($("#archiv-basic").is(":checked")) {
      if ($("#archiv-premium").is(":checked")) {
        $("#archiv-premium").click();
      }
    }
  });
  $("#archiv-premium").change(function () {
    if ($("#archiv-premium").is(":checked")) {
      if ($("#archiv-basic").is(":checked")) {
        $("#archiv-basic").click();
      }
    }
  });
  // Magio Kino
  $("#kino-m").change(function () {
    if ($("#kino-m").is(":checked")) {
      if ($("#kino-l").is(":checked")) {
        $("#kino-l").click();
      }
    }
  });
  $("#kino-l").change(function () {
    if ($("#kino-l").is(":checked")) {
      if ($("#kino-m").is(":checked")) {
        $("#kino-m").click();
      }
    }
  });
  $(".channel-package").change(function (e) {
    var myprop = "[" + e.target.id + "]";
    if ($(this).is(":checked")) {
      $(myprop).removeClass("my-unchecked");
      $(myprop).addClass("my-checked");
    } else {
      $(myprop).addClass("my-unchecked");
      $(myprop).removeClass("my-checked");
    }

    if ($(".channel-package:checked").size() > 2) {
      $(".channel-package-price").text("1,97 €");
      $(".my-checked:lt(3)").text("zadarmo");
      $("#error-channel-package").text("");
      $("#choice-package").css({
        "border-top": "none",
        "border-bottom": "none",
      });
    } else {
      $(".channel-package-price").text("zadarmo");
    }
  });
  // GET DATA FROM URL
  service = getUrlParameter("sluzba");
  device = getUrlParameter("zariadenie");

  if (service == "magio-go-m" || service == "") {
    $("#service-header").text("M");
    $(".channel-package-price").text("zadarmo");
    service = "magio-go-m";
    var info = `
    <div class="information_text">
      Balík Magio GO M obsahuje viac ako 60 TV staníc, z toho 29 v
      HD​ kvalite. K službe Magio GO M si môžete navoliť kombináciu
      programových balíkov podľa seba. Zároveň si môžete k službe
      aktivovať aj Magio GO Archív alebo Magio GO Kino.
    </div>
    <div class="information_text">
      V prípade, ak nemáte smart TV a radi by ste Magio GO sledovali
      aj na televízii, odporúčame vám pridať k vašej objednávke aj
      Magio GO TV Box.
    </div>
    <div class="information_text dark">
      Pozrieť obsah
      <a class="popupbtn lightbox" href="#staniceZaklad">základného balíka.</a>
    </div>  
    `;
    $("#package-info").html(info);
    $("#package-title").text("Výber programových balíkov");
    $("#package-desc").html(
      "K internetovej televízii Magio GO M si môžete vybrať 3 programové balíky z tejto ponuky v cene služby. <br />Ďalšie sú za poplatok 1,97 € mesačne."
    );
    $("#choice-package").show();
    $("#package-list").hide();
  }
  if (service == "magio-go-l") {
    $("#service-header").text("L");

    var info = `
    <div class="information_text">
      Balík Magio GO L obsahuje viac ako 95 TV staníc​ v HD aj v SD kvalite. 
    </div>
    <div class="information_text">
      K službe Magio GO L máte automaticky základný balík programov, 6 programových balíkov a extra balík L. Zároveň si môžete k službe aktivovať aj Magio GO Archív alebo Magio GO Kino. 
      V prípade, ak nemáte smart TV a radi by ste Magio GO sledovali aj na televízii, odporúčame vám pridať k vašej objednávke aj Magio GO TV Box.
    </div>
    <div class="information_text dark">
      Pozrieť obsah
      <a class="popupbtn lightbox" href="#staniceZaklad">základného balíka.</a><br>
      Pozrieť obsah
      <a class="popupbtn lightbox" href="#staniceExtraL">extra balíka L.</a>
    </div>  
    `;
    $("#package-info").html(info);
    $("#package-title").text("Programové balíky v cene služby");
    $("#package-desc").text("");
    $("#choice-package").hide();
    $("#package-list").show();
  }
  if (service == "magio-go-xl") {
    $("#service-header").text("XL");
    var info = `
    <div class="information_text">
      Balík Magio GO XL obsahuje viac ako 135 TV staníc​ v HD aj v SD kvalite. 
    </div>
    <div class="information_text">
      K službe Magio GO XL máte automaticky základný balík programov, 6 programových balíkov, extra balík L a extra balík XL. Zároveň si môžete aktivovať aj Magio GO Archív alebo Magio GO Kino. 
      V prípade, ak nemáte smart TV a radi by ste Magio GO sledovali aj na televízii, odporúčame vám pridať k vašej objednávke aj Magio GO TV Box.
    </div>
    <div class="information_text dark">
      Pozrieť obsah
      <a class="popupbtn lightbox" href="#staniceZaklad">základného balíka.</a><br>
      Pozrieť obsah
      <a class="popupbtn lightbox" href="#staniceExtraL">extra balíka L.</a><br>
      Pozrieť obsah
      <a class="popupbtn lightbox" href="#staniceExtraXL">extra balíka XL.</a>
    </div>  
    `;
    $("#package-info").html(info);
    $("#package-title").text("Programové balíky v cene služby");
    $("#package-desc").text("");
    $("#choice-package").hide();
    $("#package-list").show();
  }

  if (device == "true") {
    $("#box").click();
  }
  var $form = $("#personalDataForm");

  // IF SUBMIT FORM

  $(".submitbtn").on("click", function (e) {
    e.preventDefault();
    var pocetKanalov;
    if (service == "magio-go-m") {
      pocetKanalov = $(".channel-package:checked").size();
    } else {
      pocetKanalov = 6;
    }

    if (pocetKanalov >= 3) {
      if ($("#personalDataForm").valid()) {
        //sluzba

        if (service == "magio-go-xl") {
          $("#formFieldId154556 input").val("Magio GO XL").blur();
        }
        if (service == "magio-go-l") {
          $("#formFieldId154556 input").val("Magio GO L").blur();
        }
        if (service == "magio-go-m") {
          $("#formFieldId154556 input").val("Magio GO M").blur();
        }

        //programove balicky
        var programBalik = "";
        if ($("#dokumentarny").is(":checked")) {
          programBalik += "Dokumentárny ";
        }
        if ($("#styl").is(":checked")) {
          programBalik += "Štýl a fakty ";
        }
        if ($("#hudobny").is(":checked")) {
          programBalik += "Hudobný + detský ";
        }
        if ($("#volnycas").is(":checked")) {
          programBalik += "Voľný čas ";
        }
        if ($("#filmovy").is(":checked")) {
          programBalik += "Filmový ";
        }
        if ($("#sportovy").is(":checked")) {
          programBalik += "Športový ";
        }
        $("#formFieldId154557 input").val(programBalik).blur();
        //premium baliky
        var premiumBalik = "";
        if ($("#hbo").is(":checked")) {
          premiumBalik = "HBO";
        }
        if ($("#hbogo").is(":checked")) {
          premiumBalik = "HBO a HBO GO";
        }
        if ($("#cinemax").is(":checked")) {
          premiumBalik += "cinemax";
        }
        $("#formFieldId154558 input").val(premiumBalik).blur();
        //zariadenie
        if ($("#box").is(":checked")) {
          $("#formFieldId154559 input").val("mám záujem o Magio Box").blur();
        } else {
          $("#formFieldId154559 input").val("nemám záujem o Magio Box").blur();
        }
        //Archiv
        var magioArchiv = "";
        if ($("#archiv-basic").is(":checked")) {
          magioArchiv = "Magio GO Archív Basic";
        }
        if ($("#archiv-premium").is(":checked")) {
          magioArchiv = "Magio GO Archív Premium";
        }
        $("#formFieldId154560 input").val(magioArchiv).blur();
        //Kino
        var magioKino = "";
        if ($("#kino-m").is(":checked")) {
          magioKino = "Magio Kino M";
        }
        if ($("#kino-l").is(":checked")) {
          magioKino = "Magio Kino L";
        }
        $("#formFieldId154561 input").val(magioKino).blur();
        //osobne udaje
        var firstName = $form.find("#firstName").val();
        $("#formFieldId154562 input").val(firstName).blur();
        var lastName = $form.find("#lastName").val();
        $("#formFieldId154563 input").val(lastName).blur();
        var userEmail = $form.find("#email").val();
        $("#formFieldId154595 input").val(userEmail).blur();
        var phoneNum = $form.find("#phoneNum").val();
        $("#formFieldId154594 input").val(phoneNum).blur();
        var address = $form.find("#address").val();
        $("#formFieldId154564 input").val(address).blur();
        var streetNum = $form.find("#streetNum").val();
        $("#formFieldId154565 input").val(streetNum).blur();
        var city = $form.find("#city").val();
        $("#formFieldId154566 input").val(city).blur();
        var zipcode = $form.find("#zipcode").val();
        $("#formFieldId154567 input").val(zipcode).blur();
        var birthNum = $form.find("#birthNum").val();
        $("#formFieldId154574 input").val(birthNum).blur();
        var userIdNum = $form.find("#userIdNum").val();
        $("#formFieldId154575 input").val(userIdNum).blur();
        //dorucenie
        var myAddress = $form.find("#my-address").val();
        $("#formFieldId154576 input").val(myAddress).blur();
        var myStreetNum = $form.find("#my-streetNum").val();
        $("#formFieldId154577 input").val(myStreetNum).blur();
        var myCity = $form.find("#my-city").val();
        $("#formFieldId154578 input").val(myCity).blur();
        var myZipcode = $form.find("#my-zipcode").val();
        $("#formFieldId154579 input").val(myZipcode).blur();
        //submit
        setTimeout(function () {
          $(".tlacidlo_odoslat").eq(0).click();
        }, 800);
      }
    } else {
      $([document.documentElement, document.body]).animate(
        {
          scrollTop: $("#choice-package").offset().top,
        },
        1000
      );
      $("#error-channel-package").text(
        "Je potrebné si zvoliť aspoň 3 programové balíky."
      );
      $("#choice-package").css({
        "border-top": "1px solid red",
        "border-bottom": "1px solid red",
      });
    }
  });
});
