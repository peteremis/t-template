var activeStatus = [];

$(function () {

  // var apiUrl = "http://www.telekom.localhost/tmsp/api/level1";

  var apiUrl = "https://backvm.telekom.sk/tmsp/api";

  if (window.location.hostname == "localhost") {
    apiUrl = "http://www.telekom.localhost/tmsp/api";
  }


  var $back = $('.back');
  var $levelButton = $('.level-button');

  var level = 1;
  var formId = '';

  getFormSelect(level, formId);

  function getFormSelect(level, formId) {

    $.ajax({
      type: "GET",
      async: true,
      crossDomain: false,
      url: apiUrl + '/form',
      data: {
        'level': level,
        'form': formId
      },
      success: function (response) {
        parseResponse(response.level, response.data, response.type, response.form)
      },
      error: function () {
        console.log('Error');
      }
    });

  }

  function parseResponse(level, data, type, form) {

    if (type == 'select') {
      $back.attr("data-form", 0);
      parseSelect(level, data);
    } else {

      $back.attr("data-form", 1);

      $('.formContent').show();
      $('.activation').hide();

      parseInput(data, form)

    }

  }

  function parseInput(data) {

    var formtBoxHtmlIncident = $(".tmsp-box-incident");
    var formtBoxHtmlCustomer = $(".tmsp-box-customer");
    var htmlDetail = '';

    var formIdHtml = '<input type="hidden" name="tmsp-form_id" value="' + formId + '">';

    formtBoxHtmlIncident.empty();
    formtBoxHtmlCustomer.empty();

    formtBoxHtmlCustomer.append(formIdHtml);
    formtBoxHtmlIncident.append(formIdHtml);

    $.each(data, function (index, value) {
      // console.log(value);

      htmlDetail = generateInput(value);

      if (value.variable_group == "2") {
        formtBoxHtmlIncident.append(htmlDetail);
      } else {
        formtBoxHtmlCustomer.append(htmlDetail);
      }

    });

  }

  function parseSelect(level, data) {

    var selectBoxHtml = $("#select-level-" + level);
    var selectBoxButton = $("#button-level-" + level);

    // console.log(selectBoxButton);

    var htmlDetail = '';

    selectBoxHtml.empty();
    selectBoxButton.addClass('disabled');

    $.each(data, function (index, value) {
      // console.log(value);

      htmlDetail = generateSelect(level, value);
      selectBoxHtml.append(htmlDetail);
    });

  }

  function generateInput(data) {

    var note = data['note'];
    var variableType = data['variable_type'];

    var response = '';

    if (variableType == 'input') {
      response =
        '<div class="form-content">\n' +
        '\t<div class="label">\n' +
        '\t\t<label for="tmsp-' + data.id + '">' + data.description + '</label>\n' +
        '\t</div>\n' +
        '\t<input class="tmsp-input" type="text" id="tmsp-' + data.id + '" name="tmsp-' + data.id + '" value="">\n' +
        '\t<small class="form-text text-muted">' + note + '</small>' +
        '</div>\n';
    } else if (variableType == 'select') {

      var selectStart = '<select name="tmsp-' + data.id + '">';
      var selectEnd = "</select>";

      var options = '';

      $.each(data.selectbox, function (index, selectbox) {
        options += '<option value="' + selectbox.value + '">' + selectbox.text + '</option>';
      });

      var select = selectStart + options + selectEnd;

      response =
        '<div class="form-content">\n' +
        '\t<div class="label">\n' +
        '\t\t<label for="tmsp-' + data.id + '">' + data.description + '</label>\n' +
        '\t</div>\n' + select +
        '\t<div><small class="form-text text-muted">' + note + '</small></div>' +
        '</div>\n';

    } else if (variableType == 'textarea') {
      '<div class="form-content">\n' +
      '\t<div class="label">\n' +
      '\t\t<label for="tmsp-' + data.id + '">' + data.description + '</label>\n' +
      '\t</div>\n' +
      '\t<input class="tmsp-input" type="text" id="tmsp-' + data.id + '" name="tmsp-' + data.id + '" value="">\n' +
      '\t<small class="form-text text-muted">' + note + '</small>' +
      '</div>\n';
    }

    return response;
  }

  function generateSelect(level, data) {

    var response =
      '<div class="rbCheckbox activation-type-container">\n' +
      '\t<label for="select-' + level + '" data-select="' + data.id + '" data-level="' + level + '" class="activationChoice">\n' +
      '\t\t<span class="">\n' +
      '\t\t\t<div class="radio" id="uniform-' + data.id + '">\n' +
      '\t\t\t<span id="tmsp-select-' + data.id + '" class="tmsp-select">\n' +
      '\t\t\t\t<input id="' + data.id + '" type="radio" name="select-' + level + '" value="' + data.id + '">\n' +
      '\t\t\t\t</span>\n' +
      '\t\t\t</div>\n' +
      '\t\t\t<span class="text">' + data.description + '</span>\n' +
      '\t\t</span>\n' +
      '\t</label>\n' +
      '</div>\n';

    return response;

  }

  $(document).on('click', '.activationChoice', function () {

    $('.tmsp-select').removeClass('checked');

    var id = $(this).attr("data-select");
    // var level = $(this).attr("data-level");
    var level = $(this).data("level");

    var button = $('#button-level-' + level);

    if (button.hasClass('disabled')) {
      button.removeClass('disabled')
    }

    var active = $('#tmsp-select-' + id);
    active.addClass('checked');

    // activeStatus[level] = parseInt(id);
    activeStatus[level] = id;
    console.log(activeStatus);

  });

  $(document).on('click', '.level-button', function (e) {

    e.preventDefault();

    var actualLevel = $back.attr('data-level');
    var action = 'show';

    $back.show();
    showHideLevel(actualLevel, action);
  });

  $(document).on('click', '.back', function (e) {

    e.preventDefault();

    var formShow = $back.attr('data-form');
    var actualLevel = $back.attr('data-level');

    // var selectBoxButton = $("#button-level-" + (actualLevel-1));
    // console.log(selectBoxButton);

    // var selectBoxHtml = $("#select-level-" + (actualLevel-1));
    // selectBoxHtml.empty();
    // console.log(selectBoxHtml);

    if ((actualLevel - 1) == 1) {
      $back.hide();
    }

    // ("#button-level-" + (actualLevel - 1)).addClass('disabled');

    if (formShow == 1) {
      $back.attr("data-form", 0);

      $('.formContent').hide();
      $('.activation').show();

      // selectBoxButton.addClass('disabled');
      showHideLevel(actualLevel, 'hide')

    } else {
      showHideLevel(actualLevel, 'hide');
    }

  });

  $(document).on('click', '#form-button', function (e) {

    e.preventDefault();

    var formId = '#tmsp-form';
    var values = $(formId).serialize();

    $('.formContent').hide();
    $('.dakujeme').show();

    console.log(values);

    $.ajax({
      url: apiUrl + '/send',
      data: values,
      type: 'POST',
      contentType: 'application/json',
      dataType: 'json',
      error: function () {

      },
      success: function (res) {
        console.log('OK');
      }
    });

  });

  function showHideLevel(actualLevel, action) {

    var prevLevel = (actualLevel - 1);
    var nextLevel = (+actualLevel + +1);

    var actualLevelSelector = $('#level-' + actualLevel);
    var nextLevelSelector = $('#level-' + nextLevel);
    var prevLevelSelector = $('#level-' + prevLevel);

    console.log('actual: ' + actualLevel);
    console.log('prev: ' + prevLevel);
    // console.log(actualLevelSelector);
    // console.log(prevLevelSelector);

    if (action == 'show') {
      $back.attr("data-level", nextLevel);

      nextLevelSelector.show();
      actualLevelSelector.hide();

      formId = activeStatus[actualLevel];
      getFormSelect(nextLevel, formId);

    } else {
      console.log('hide: ' + prevLevel);
      $back.attr("data-level", prevLevel);

      prevLevelSelector.show();
      actualLevelSelector.hide();
    }

  }

});