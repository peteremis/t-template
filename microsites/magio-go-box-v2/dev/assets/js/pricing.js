$(function() {
    var $radioType1 = $('.radio-type1'),
    $radio1 = $('#radio1'),
    $radio2 = $('#radio2'),
    $radioType2 = $('.radio-type2'),
    $radio3 = $('#radio3'),
    $radio4 = $('#radio4'),
    $selectType = $('#selectType'),
    $select1 = $('#select1'),
    $select2 = $('#select2'),
    $lightboxArchivePremium = $('#lightbox-archive-go-premium'),
    $lightboxArchive = $('#lightbox-archive-go');

    var pricingObj1 = {
        "pricing-action-1": '',
        "pricing-method-1": "",
        "price-1": "69,99 €",
        "pricing-method-second-line-1": "jednorazovo pri kúpe",
        "pricing-method-2": "mesačne",
        "price-2": "0 €",
        "pricing-method-second-line-2": "ku ktorejkoľvek Magio televízii"
    }

    var pricingObj2 = {
        "pricing-action-1": '',
        "pricing-method-1": "mesačne",
        "price-1": "3,00 €",
        "pricing-method-second-line-1": "+ 1 € jednorazovo pri kúpe",
        "pricing-method-2": "mesačne",
        "price-2": "0 €",
        "pricing-method-second-line-2": "ku ktorejkoľvek Magio televízii"
    }

    var pricingObj3 = {
        "feature1": "50 TV staníc",
        "feature2": "7-dňový Archív Magio GO Premium",
        "pricing-method-3": "jednorazovo",
        "pricing-action-3": 'Zvýhodnená cena k Magio TV',
        "price-3": "29,90 €",
        "pricing-method-second-line-3": "",
        "magio-go-type": "Magio GO Premium",
        "pricing-method-4": "mesačne",
        "price-4": "5,99 €",
        "pricing-method-second-line-4": ""
    }

    var pricingObj4 = {
        "feature1": "50 TV staníc",
        "feature2": "7-dňový Archív Magio GO Premium",
        "pricing-method-3": "mesačne",
        "price-3": "3,00 €",
        "pricing-action-3": '',
        "pricing-method-second-line-3": "+ 1 € jednorazovo pri kúpe",
        "magio-go-type": "Magio GO Premium",
        "pricing-method-4": "mesačne",
        "price-4": "5,99 €",
        "pricing-method-second-line-4": ""
    }

    var pricingObj5 = {
        "feature1": "12 TV staníc",
        "feature2": "7-dňový Archív Magio GO",
        "pricing-method-3": "jednorazovo",
        "price-3": "29,90 €",
        "pricing-method-second-line-3": "",
        "magio-go-type": "Magio GO",
        "pricing-method-4": "mesačne",
        "price-4": "3,99 €",
        "pricing-method-second-line-4": ""
    }

    var pricingObj6 = {
        "feature1": "12 TV staníc",
        "feature2": "7-dňový Archív Magio GO",
        "pricing-method-3": "mesačne",
        "price-3": "3,00 €",
        "pricing-method-second-line-3": "+ 1 € jednorazovo pri kúpe",
        "magio-go-type": "Magio GO",
        "pricing-method-4": "mesačne",
        "price-4": "3,99 €",
        "pricing-method-second-line-4": ""
    }

    function changeArchiveLinkAccordingly() {
        if (getSelectedOptionValue() === 'select1') {
            $lightboxArchive.hide();
            $lightboxArchivePremium.show();
        } else {
            $lightboxArchivePremium.hide();
            $lightboxArchive.show();
        }
    }

    function changeTexts(obj) {
        for (var key in obj) {
            $('#'+key).text(obj[key]);
        }
    }

    function getSelectedOptionValue() {
        return $("#selectType option:selected").attr('value');
    }

    function changePricingDetails() {
        if (getSelectedOptionValue() === 'select1') {
            if ($radio3.is(':checked')) {
                changeTexts(pricingObj3);
                changeArchiveLinkAccordingly();
            } else {
                changeTexts(pricingObj4);
                changeArchiveLinkAccordingly();
            }
        } else {
            if ($radio3.is(':checked')) {
                changeTexts(pricingObj5);
                changeArchiveLinkAccordingly();
            } else {
                changeTexts(pricingObj6);
                changeArchiveLinkAccordingly();
            }
        }
    }

    $radioType1.on('change', function() {
        if ($radio1.is(':checked')) {
            changeTexts(pricingObj1);
        } else {
            changeTexts(pricingObj2);
        }
    });

    $selectType.on('change', function() {
        changePricingDetails();
    });

    $radioType2.on('change', function() {
        changePricingDetails();
    });

});
