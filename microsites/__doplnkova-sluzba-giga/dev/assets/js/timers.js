var wholeTime = [0.3 * 60, 0.46 * 60, 1.13 * 60, 9.53 * 60];
var length = Math.PI * 2 * 100;
var intervalTimer = new Array();

startTimer();

function update(count, value, timePercent) {
  var pointer = document.querySelectorAll(".e-pointer")[count];
  var offset = -length - (length * value) / timePercent;
  var progressBar = document.querySelectorAll(".e-c-progress")[count];
  progressBar.style.strokeDashoffset = offset;

  //pointer.style.transform = `rotate(${360 * value / timePercent}deg)`;
  var rotate = (360 * value) / timePercent + "deg";
  var ua = window.navigator.userAgent;
  var msie = ua.indexOf("MSIE ");
  if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
    // If Internet Explorer
    pointer.style.transform = "none";
    pointer.style.display = "none";
  } // If another browser
  else {
    pointer.style.transform = "rotate(" + rotate + ")";
  }

  var pointerVal = count == 0 ? "all 700ms" : "all 1s";
  pointer.style.transition = pointerVal;
  progressBar.style.transition = pointerVal;
}

function timer(count, wholeTime) {
  //counts time, takes seconds
  var remainTime = Date.now() + wholeTime * 1000;

  intervalTimer[count] = setInterval(function () {
    var timeLeft = Math.round((remainTime - Date.now()) / 1000);

    if (timeLeft <= 0) {
      clearInterval(intervalTimer[count]);
      //displayTimeLeft(count, wholeTime);
      var displayOutput = document.querySelectorAll(".controlls")[count];
      var timerBg = document.querySelectorAll(".e-c-base")[count];
      var time = document.querySelectorAll(".display-remain-time")[count];

      setTimeout(function () {
        if (count == 0) {
          //change BG color
          timerBg.style.fill = "#e20074";
        } else {
          timerBg.style.fill = "#1062ac";
        }

        //apend text after counter finish, hide time
        var finalTextElement = document.createElement("p");
        finalTextElement.innerHTML =
          '<p style="color: #fff; font-weight: 600; font-size: 22px;">Stiahnuté za ' +
          wholeTime.toFixed(0) +
          " sekúnd</p>";
        displayOutput.appendChild(finalTextElement);
        time.style.display = "none";
      }, 1000);
    }

    displayTimeLeft(count, timeLeft);
  }, 1000);
}

function displayTimeLeft(count, timeLeft) {
  //displays time on the input
  var hours = Math.floor(timeLeft / 3600);
  var minutes = Math.floor((timeLeft % 3600) / 60);
  var seconds = timeLeft % 60;

  var displayString =
    (hours < 10 ? "0" : "") +
    hours +
    ":" +
    (minutes < 10 ? "0" : "") +
    minutes +
    ":" +
    (seconds < 10 ? "0" : "") +
    seconds.toFixed(0);
  var displayOutput = document.querySelectorAll(".display-remain-time")[count];
  displayOutput.textContent = displayString;
  update(count, timeLeft, wholeTime[count]);
}

function startTimer() {
  var parentComp = document.getElementById("timers");
  var comp = document.querySelector("#timers .timers-container");

  for (i = 0; i < wholeTime.length; i++) {
    if (i == 0) {
      var progressBar = comp.querySelector(".e-c-progress");
      progressBar.style.strokeDasharray = length;
    } else {
      var clone = comp.cloneNode(true);
      parentComp.appendChild(clone);

      var progressBar = clone.querySelector(".e-c-progress");
      progressBar.style.strokeDasharray = length;
    }
    displayTimeLeft(i, wholeTime[i]);
    timer(i, wholeTime[i]);
  }
}

//text after timers

var Timer1Text =
  '<div class="text-under-timer">' +
  '<p style="margin:0px"><b>Doplnková služba Giga</b></p>' +
  '<p style="margin:0px">Rýchlosť do: 1 Gbit/100 Mbs<p>' +
  "</div>";
$(".timers-container").eq(0).append(Timer1Text);

var Timer2Text =
  '<div class="text-under-timer">' +
  '<p style="margin:0px"><b>Magio internet XL</b></p>' +
  '<p style="margin:0px">Rýchlosť: 600/60 Mbs<p>' +
  "</div>";
$(".timers-container").eq(1).append(Timer2Text);

var Timer3Text =
  '<div class="text-under-timer">' +
  '<p style="margin:0px"><b>Magio internet L</b></p>' +
  '<p style="margin:0px">Rýchlosť: 250/25 Mbs<p>' +
  "</div>";
$(".timers-container").eq(2).append(Timer3Text);

var Timer4Text =
  '<div class="text-under-timer">' +
  '<p style="margin:0px"><b>Magio internet M</b></p>' +
  '<p style="margin:0px">Rýchlosť: 30/3 Mbs<p>' +
  "</div>";
$(".timers-container").eq(3).append(Timer4Text);
