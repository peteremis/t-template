$(function() {


    $('#rychlyInternet, #rychlyInternet2').qtip({
        content: 'Po aktivácii služby je maximálna dosiahnuteľná rýchlosť pri sťahovaní dát 950 Mbit/s a maximálna dosiahnuteľná rýchlosť pri odosielaní dát 100 Mbit/s.',
        style: {
            classes: 'qtip-tipsy'
        },
        position: {
            my: 'bottom center',
            at: 'top center'
        }
    });



});

/* TABS */
$(".tabs-menu a").click(function(event) {
    event.preventDefault();
    $(this).parent().addClass("current");
    $(this).parent().siblings().removeClass("current");
    var tab = $(this).attr("href"), // select clicked tab. In example "#tab-1" ..
        parent = $(this).closest('ul').parent().parent(); // <tabs-container>
    parent.find('.tab-content').not(tab).css("display", "none"); //hide all not clicked tabs
    $(tab).fadeIn(); //show clicked tab


});
/* TABS END */

/* FROM TABS TO ACCORDION */
var dataSegment = $("[data-segment]").each(function(){
  //console.log($(this).attr('data-segment')); //zobraz "data-segment" hodnotu pre kazdu <section>

  closestHead = $($(this).find('.tabs-menu a'));
  closestItemCount = closestHead.length - 1; //vytiahni pocet tabov pre kazdu <section> s atributom "data-segment"
  //console.log(closestItemCount + "heads");

  closestContent = $($(this).find('.tab-content')); //najdi tab-content v danej <section>
  //console.log(closestContent);

  closestAccordion = $(this).find('.accordion'); //najdi .accordion v danej <section>

  accordionItem = '<div class="item">';

  for (var i = 0; i <= closestItemCount; i++) {
      accordionItem += '<div class="heading">' + $(closestHead[i]).text()  + '</div>';
      accordionItem += '<div class="content">' + $(closestContent[i]).html() + '</div></div>';

      if (i !== closestItemCount) {
          accordionItem += '<div class="item">';
      }
  }

  //if data-segment and data-accordion value match, show accordion data
  if($(this).attr('data-segment') === closestAccordion.attr('data-accordion')) {
       $(accordionItem).appendTo(closestAccordion);
  }

  var $items = $('.accordion .item');
  //SET OPENED ITEM
  $($items[0]).addClass('open');

});
/* FROM TABS TO ACCORDION END */

/* ACCORDION */
$('.accordion .item .heading').click(function(e) {

    var clickedHead = e.target;
    var $item = $(clickedHead).closest('.item');
    var isOpen = $item.hasClass('open');
    var $content = $item.find('.content');
    var $acPrice = $(clickedHead).find('.ac-price');

    if (isOpen) {
        $content.slideUp(200);
        $item.removeClass('open');
        $acPrice.show();
    } else {
        $content.slideDown(200);
        $item.addClass('open');
        $acPrice.hide();
    }
});


$('.accordion .item.open').find('.content').slideDown(200);
/* ACCORDION END */


/* TOGGLE ARROW */
$('.toggle-arrow, .arrow-right').click(function(event) {
    event.preventDefault();
    if ($(this).hasClass('arrow-right')) {
      return;
    }
    $(event.target).closest('.togleArrow').find('.arrow-content').toggle(200);
    $(this).find('.arrow-right').toggleClass('arrow-rotate');
  });
/* TOGGLE ARROW END */


/* scroll to + sticky nav */
$("#nav-sticky").sticky({
    topSpacing: 0,
    widthFromWrapper: true
  });
  
  $(".scroll-to").click(function() {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top - ($("#nav-sticky").outerHeight())
        }, 1000);
        return false;
      }
    }
  });
  
  var contentSections = $("[id*='sec-']"),
    secondaryNav = $("#nav-sticky");
  
  function updateSecondaryNavigation() {
    contentSections.each(function() {
      var actual = $(this),
        actualHeight = actual.height() + parseInt(actual.css('paddingTop').replace('px', '')) + parseInt(actual.css('paddingBottom').replace('px', '')),
        actualAnchor = secondaryNav.find('a[href="#' + actual.attr('id') + '"]');
  
      if ((actual.offset().top - secondaryNav.outerHeight() <= $(window).scrollTop()) && (actual.offset().top + actualHeight - secondaryNav.outerHeight() > $(window).scrollTop())) {
        actualAnchor.addClass('active');
      } else {
        actualAnchor.removeClass('active');
      }
  
    });
  }
  updateSecondaryNavigation();
  
  $(window).scroll(function(event) {
    updateSecondaryNavigation();
  });
  /* scroll to + sticky nav END */