//// Vars

var priceListOptions = "",
    priceList        = {
                        0: ["5", "10", "20"],
                        1: ["50", "70", "100"]},
    discount         = 20,
    spCPU            = 5.5,
    spRAM            = 13.7216,
    spHDDProPlus     = 0.949,
    spHDDPro         = 0.511,
    spHDD            = 0.292,
    ipCPU            = 0,
    ipRAM            = 0,
    ipHDDProPlus     = 0,
    ipHDDPro         = 0,
    ipHDD            = 0,
    ipVmWare         = 2.7648, // COnstant Value
    ipWindows        = 16.9,   // COnstant Value
    priceCPU         = 0,
    priceRAM         = 0,
    priceHDDProPlus  = 0,
    priceHDDPro      = 0,
    priceHDD         = 0,
    priceOS          = 0,
    priceOSHDD       = 0,
    priceVmWare      = 0,
    priceWindows     = 0,
    priceTotal       = 0,
    paramWindows     = 1,
    paramCPU         = 2,
    totalHTML        = $("#total");
    
//// Functions
var changeOptions = function (value) {
    priceListOptions = "";
    for (var x in priceList[value]) {
        priceListOptions += "<option>" + priceList[value][x] + "</option>";
    }

    $("#osHdd").html(priceListOptions);
}
// Calc the individual items
var calcIp = function (sP) {
    // individual price = standard price * (discount% / 100)
    // discount % = 100% - discount
    return sP * ((100 - discount) / 100);
}

var calcCPU = function () {
    priceCPU = (Number($("#cpu").val()) * paramCPU) * ipCPU;
}

var calcRAM = function () {
    priceRAM = Number($("#ram").val()) * ipRAM;
}

var calcHDDProPlus = function () {
    priceHDDProPlus = Number($("#hddProPlus").val()) * ipHDDProPlus;
}

var calcHDDPro = function () {
    priceHDDPro = (Number($("#hddPro").val()) + Number($("#osHdd").val())) * ipHDDPro;
}

var calcHDD = function () {
    priceHDD = Number($("#hdd").val()) * ipHDD;
}

var calcVmWare = function () {
    priceVmWare = ipVmWare * Number($("#ram").val());
}

var calcWindows = function () {
    if (Number($("#os").val()) === 1) {
        priceWindows = ipWindows * paramWindows;
    }
    else {
        priceWindows = 0;
    }
}

var calcTotal = function () {
    priceTotal = Math.ceil(priceCPU + priceRAM + priceHDDProPlus + priceHDDPro + priceHDD + priceVmWare + priceWindows);
}

var calcAll = function () {
    calcCPU();
    calcRAM();
    calcHDDProPlus();
    calcHDDPro();
    calcHDD();
    calcVmWare();
    calcWindows();
    calcTotal();
    writeTotal();
}
// Write the total into HTML
var writeTotal = function () {
    totalHTML.html(priceTotal);
}

// Calculate the individual prices
ipCPU        = calcIp(spCPU);
ipRAM        = calcIp(spRAM);
ipHDDProPlus = calcIp(spHDDProPlus);
ipHDDPro     = calcIp(spHDDPro);
ipHDD        = calcIp(spHDD);

// Adding click functions
$(".prev").click(function () {
    $(this).siblings("select").find("option:selected").prev().prop("selected", true);
    calcAll();
});

$(".next").click(function () {
    $(this).siblings("select").find("option:selected").next().prop("selected", true);
    calcAll();
});

// Click function for custom slectboxes
$(".prev-custom").click(function () {
    $(this).siblings("select").find("option:selected").prev().prop("selected", true);
    changeOptions($(this).siblings("select").val());
    calcAll();
});

$(".next-custom").click(function () {
    $(this).siblings("select").find("option:selected").next().prop("selected", true);
    changeOptions($(this).siblings("select").val());
    calcAll();
});

//// Run the app

changeOptions(0); // Set the default value to 0

calcAll();