$(function () {
    $('.neobmedzene-volania-m').qtip({
        content: {
            text:
                'Neobmedzené hovory vám poskytujeme vďaka <br> Zásadám férového používania: <br> 1000 minút - do mobilnej siete Telekom',
        },
        position: {
            my: 'top center',
            at: 'bottom center',
        },
    });
    $('.neobmedzene-volania-l').qtip({
        content: {
            text:
                'Neobmedzené hovory vám poskytujeme vďaka<br> Zásadám férového používania:<br> 1000 minút - do mobilných sietí v SR',
        },
        position: {
            my: 'top center',
            at: 'bottom center',
        },
    });
    $('.neobmedzene-volania-xl').qtip({
        content: {
            text:
                'Neobmedzené hovory vám poskytujeme vďaka <br> Zásadám férového používania:<br> 1000 minút - do mobilných sietí v SR<br> 1000 minút - do EÚ',
        },
        position: {
            my: 'top center',
            at: 'bottom center',
        },
    });

    $('#nav-sticky').sticky({
        topSpacing: 0,
        widthFromWrapper: true,
    });

    $('.scroll-to').click(function () {
        if (
            location.pathname.replace(/^\//, '') ==
                this.pathname.replace(/^\//, '') &&
            location.hostname == this.hostname
        ) {
            var target = $(this.hash);
            target = target.length
                ? target
                : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate(
                    {
                        scrollTop:
                            target.offset().top -
                            ($('#nav-sticky').outerHeight() - 10),
                    },
                    1000
                );
                return false;
            }
        }
    });

    var contentSections = $("[id*='sec-']"),
        secondaryNav = $('#nav-sticky');

    function updateSecondaryNavigation() {
        contentSections.each(function () {
            var actual = $(this),
                actualHeight =
                    actual.height() +
                    parseInt(actual.css('paddingTop').replace('px', '')) +
                    parseInt(actual.css('paddingBottom').replace('px', '')),
                actualAnchor = secondaryNav.find(
                    'a[href="#' + actual.attr('id') + '"]'
                );

            if (
                actual.offset().top - secondaryNav.outerHeight() <=
                    $(window).scrollTop() &&
                actual.offset().top +
                    actualHeight -
                    secondaryNav.outerHeight() >
                    $(window).scrollTop()
            ) {
                actualAnchor.addClass('active');
            } else {
                actualAnchor.removeClass('active');
            }
        });
    }

    updateSecondaryNavigation();

    $(window).scroll(function (event) {
        updateSecondaryNavigation();
    });

    $('.fancy').fancybox({});

    $('.tooltip').click(function (event) {
        event.preventDefault();
    });

    var targets = $('[rel~=tooltip]'),
        target = false,
        tooltip = false,
        title = false;

    targets.bind('mouseenter', function () {
        target = $(this);
        tip = target.attr('title');
        tooltip = $('<div id="tooltip"></div>');

        if (!tip || tip == '') return false;

        target.removeAttr('title');
        tooltip.css('opacity', 0).html(tip).appendTo('body');

        var init_tooltip = function () {
            if ($(window).width() < tooltip.outerWidth() * 1.5)
                tooltip.css('max-width', $(window).width() / 2);
            else tooltip.css('max-width', 340);

            var pos_left =
                    target.offset().left +
                    target.outerWidth() / 2 -
                    tooltip.outerWidth() / 2,
                pos_top = target.offset().top - tooltip.outerHeight() - 20;

            if (pos_left < 0) {
                pos_left = target.offset().left + target.outerWidth() / 2 - 20;
                tooltip.addClass('left');
            } else tooltip.removeClass('left');

            if (pos_left + tooltip.outerWidth() > $(window).width()) {
                pos_left =
                    target.offset().left -
                    tooltip.outerWidth() +
                    target.outerWidth() / 2 +
                    20;
                tooltip.addClass('right');
            } else tooltip.removeClass('right');

            if (pos_top < 0) {
                var pos_top = target.offset().top + target.outerHeight();
                tooltip.addClass('top');
            } else tooltip.removeClass('top');

            tooltip
                .css({
                    left: pos_left,
                    top: pos_top,
                })
                .animate(
                    {
                        top: '+=10',
                        opacity: 1,
                    },
                    50
                );
        };

        init_tooltip();
        $(window).resize(init_tooltip);

        var remove_tooltip = function () {
            tooltip.animate(
                {
                    top: '-=10',
                    opacity: 0,
                },
                50,
                function () {
                    $(this).remove();
                }
            );

            target.attr('title', tip);
        };

        target.bind('mouseleave', remove_tooltip);
        tooltip.bind('click', remove_tooltip);
    });

    /* TABS */
    $('.tabs-menu a').click(function (event) {
        event.preventDefault();
        $(this).parent().addClass('current');
        $(this).parent().siblings().removeClass('current');
        var tab = $(this).attr('href'), // select clicked tab. In example "#tab-1" ..
            parent = $(this).closest('ul').parent().parent(); // <tabs-container>
        parent.find('.tab-content').not(tab).css('display', 'none'); //hide all not clicked tabs
        $(tab).fadeIn(); //show clicked tab
    });
    /* TABS END */

    /* FROM TABS TO ACCORDION */
    var $accordion = $('.accordion'),
        $heads = $('.tabs-menu a'),
        $contents = $('.tab-content'),
        itemCount = $($heads).length - 1,
        accordionCode = '<div class="item">';

    for (var i = 0; i <= itemCount; i++) {
        accordionCode +=
            '<div class="heading">' + $($heads[i]).text() + '</div>';
        accordionCode +=
            '<div class="content">' + $($contents[i]).html() + '</div></div>';

        if (i !== itemCount) {
            accordionCode += '<div class="item">';
        }
    }

    $(accordionCode).appendTo($accordion);

    var $items = $('.accordion .item');
    //SET OPENED ITEM
    $($items[0]).addClass('open');
    /* FROM TABS TO ACCORDION END */

    /* ACCORDION */
    $('.accordion .item .heading').click(function (e) {
        var clickedHead = e.target;
        var $item = $(clickedHead).closest('.item');
        var isOpen = $item.hasClass('open');
        var $content = $item.find('.content');
        var $acPrice = $(clickedHead).find('.ac-price');

        if (isOpen) {
            $content.slideUp(200);
            $item.removeClass('open');
            $acPrice.show();
        } else {
            $content.slideDown(200);
            $item.addClass('open');
            $acPrice.hide();
        }
    });

    $('.accordion .item.open').find('.content').slideDown(200);
    /* ACCORDION END */

    var toggleTabs = function () {
        $('.tab__variant').toggleClass('selected');
    };

    $('.popup').fancybox({
        padding: 10,
        margin: 0,
        parent: '#content',
        closeBtn: false,
        helpers: {
            overlay: {
                css: {
                    background: 'rgba(0, 0, 0, 0.7)',
                },
            },
        },
    });

    $('.close-end a').click(function () {
        $.fancybox.close();
    });

    $('#content .tab-acc-sub').click(function () {
        window.location.href = '/' + $(this).attr('data-href');
    });

    $('#show_legal').click(function () {
        $(this).hide();
        $('#legal_more').show(200);
    });

    /* SEC-AKCIE LOAD CONTENT */
    //$(".sec-akcie").load("templates/_aktualne-akcie.html", function() {});
});
