$(document).ready(function () {
  //back to to previous url
  $("#a_referrer a").attr("href", document.referrer);

  /* TABS */
  $(".tabs-menu a").click(function (event) {
    event.preventDefault();
    $(this).parent().addClass("current");
    $(this).parent().siblings().removeClass("current");
    var tab = $(this).attr("href"), // select clicked tab. In example "#tab-1" ..
      parent = $(this).closest("ul").parent().parent(); // <tabs-container>
    parent.find(".tab-content").not(tab).css("display", "none"); //hide all not clicked tabs
    $(tab).fadeIn(); //show clicked tab
  });

  /* TOOLTIP */
  $("#info-akontacia").qtip({
    content:
      "Akontácia je úvodná suma, ktorú zaplatíte pri kúpe v predajni alebo cez web.",
    style: {
      classes: "qtip-tipsy",
    },

    position: {
      my: "bottom center",
      at: "top center",
    },
  });

  $("#info-splatka").qtip({
    content: "Mesačnú splátku budete platiť každý mesiac po dobu 24 mesiacov.",
    style: {
      classes: "qtip-tipsy",
    },

    position: {
      my: "bottom center",
      at: "top center",
    },
  });

  $("#info-zlava").qtip({
    content:
      "Za každú Magio službu kúpenú cez web, získate automaticky zľavu 10 €. Za Magio TV, internet a&nbsp;Pevnú linku tak môžete získať zľavu až 30&nbsp;€. Zľava sa rozdelí na polovicu a&nbsp;bude odrátaná druhý a&nbsp;tretí mesiac z&nbsp;mesačného poplatku.",
    style: {
      classes: "qtip-tipsy",
    },

    position: {
      my: "bottom center",
      at: "top center",
    },
  });
  $("#info-akontacia-b2b").qtip({
    content:
      "Akontácia je úvodná suma, ktorú zaplatíte pri kúpe v predajni alebo cez web.",
    style: {
      classes: "qtip-tipsy",
    },

    position: {
      my: "bottom center",
      at: "top center",
    },
  });

  /* TOGGLE ARROW */
  $(".toggle-arrow, .arrow-right").click(function (event) {
    event.preventDefault();
    if ($(this).hasClass("arrow-right")) {
      return;
    }
    $(event.target).closest(".togleArrow").find(".arrow-content").toggle(200);
    $(this).find(".arrow-right").toggleClass("arrow-rotate");
  });

  $(".toggle-arrow-main, .arrow-right-main").click(function (event) {
    event.preventDefault();
    if ($(this).hasClass("arrow-right-main")) {
      return;
    }
    $(event.target)
      .closest(".togleArrow-main")
      .find(".arrow-content-main")
      .toggle(200);
    $(this).find(".arrow-right-main").toggleClass("arrow-rotate");
  });
  /* TOGGLE ARROW END */

  /* SCROLL */
  $(".scroll-to").click(function () {
    if (
      location.pathname.replace(/^\//, "") ==
        this.pathname.replace(/^\//, "") &&
      location.hostname == this.hostname
    ) {
      var target = $(this.hash);
      target = target.length ? target : $("[name=" + this.hash.slice(1) + "]");
      if (target.length) {
        $("html,body").animate(
          {
            scrollTop:
              target.offset().top - ($("#nav-sticky").outerHeight() - 10),
          },
          1000
        );
        return false;
      }
    }
  });

  /* GET PARAMETERS FROM URL */
  function getUrlParameter(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)");
    var results = regex.exec(location.search);
    return results === null
      ? ""
      : decodeURIComponent(results[1].replace(/\+/g, " "));
  }
  /*SET PRICE AND BUTONS */
  function setPrice(myrpln) {
    pricelist = {
      detailPageURL: "https://www.telekom.sk/playstation-4-pro",
      buyBtnURL:
        "https://www.telekom.sk/fix/objednavka/-/scenario/e-shop/chytry-balik",
      prices: {
        magioM: {
          rpAvailability: true,
          monthlyWeb: 10,
          oneTimeWeb: 149,
          eyecatcherURL: "/documents/10179/17001051/ec-webzlava.png",
          rplnPrice: 10,
        },
        magioL: {
          rpAvailability: true,
          monthlyWeb: 10,
          oneTimeWeb: 119,
          eyecatcherURL: "/documents/10179/17001051/ec-webzlava.png",
          rplnPrice: 15,
        },
        magioXL: {
          rpAvailability: true,
          monthlyWeb: 10,
          oneTimeWeb: 49,
          eyecatcherURL: "/documents/10179/17001051/ec-webzlava.png",
          rplnPrice: 20,
        },
      },
    };

    $("#" + myrpln)
      .click()
      .focus(); // SET DEFAULT RATEPLAN (CLICK ON THE BUTTON)
  }

  /* SET NEW PRICES ON PAGE */
  function setNewPrices(newRpln) {
    // EEC //
    $("#eec").attr("src", pricelist.prices[newRpln].eyecatcherURL);
    //B2B buyBtn //
    if (newRpln == "magioM") {
      var my_rp = "M";
    }
    if (newRpln == "magioL") {
      var my_rp = "L";
    }
    if (newRpln == "magioXL") {
      var my_rp = "XL";
    }

    $("#buyBtn").attr(
      "href",
      pricelist.buyBtnURL + "?internet=" + my_rp + "&televizia=" + my_rp
    ); //SET URL TO BUY BUTTON

    //B2C Ceny//
    $("#oneTimeWebPrice").text(
      pricelist.prices[newRpln].oneTimeWeb.toFixed(2).replace(".", ",") + " €"
    );
    $("#MonthylFeeMobile").text(
      pricelist.prices[newRpln].monthlyWeb.toFixed(2).replace(".", ",") + " €"
    );
    $("#MonthlyFeeService").text(
      pricelist.prices[newRpln].rplnPrice - 5 + ",90 € s web zľavou"
    );
    $("#MonthlyRegularPrice").text(
      pricelist.prices[newRpln].rplnPrice + ",90 € zvyšné mesiace"
    );
  }

  $(".rpln").click(function () {
    var rateplan = $(this).prop("id"); // GET NEW VALUE FROM BUTTON
    $(".rpln").removeClass("active-plan");
    $(this).addClass("active-plan");
    setNewPrices(rateplan);
  });

  var selectedrp = getUrlParameter("selectedrp"); // GET DATA FROM URL

  if (selectedrp == "") {
    selectedrp = "magioL";
  } // DEFAULT RATEPLAN B2C

  setPrice(selectedrp);
});
