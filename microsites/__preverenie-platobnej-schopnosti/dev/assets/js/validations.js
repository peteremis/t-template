$(function () {
  /* GET PARAMETERS FROM URL */
  function getUrlParameter(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)");
    var results = regex.exec(location.search);
    return results === null
      ? ""
      : decodeURIComponent(results[1].replace(/\+/g, " "));
  }

  var interaction = getUrlParameter("iid");
  if (interaction != null) {
    $("#iid").val(interaction);
  }

  var $form = $("#myForm");
  var sendFormBtn = $(".sendfrm");
  // initialize the validation plugin
  var formValidator = $form.validate({
    rules: {
      customer: {
        minlength: 10,
        maxlength: 10,
        number: true,
      },
      contactEmail: {
        required: true,
        email: true,
        customEmailValidation: true,
      },
      contactPhoneNum: {
        required: true,
        customNumberValidation: true,
        maxlength: 13,
      },
      orderNumber: {
        required: true,
        minlength: 7,
        maxlength: 7,
      },
      file1: {
        required: true,
      },
    },
    messages: {
      contactPhoneNum: {
        required: "Zadajte mobilné telefónne číslo.",
        customNumberValidation:
          "Zadajte tel. číslo v tvare 0911222333 alebo +421911222333.",
        maxlength: "Telefónne číslo môže obsahovať maximálne 13 číslic.",
      },
      contactEmail: {
        required: "Zadajte kontaktný e-mail.",
        email: "Zadajte e-mail v tvare vas@email.sk",
        customEmailValidation: "Zadajte email v správnom tvare: vas@email.sk",
      },
      customer: {
        number: "Číslo adresáta obsahuje len čísla.",
        minlength: "Číslo adresáta obsahuje 10 znakov.",
        maxlength: "Číslo adresáta obsahuje 10 znakov.",
      },
      orderNumber: {
        required: "Zadajte číslo webovej objednávky.",
        minlength: "Zadajte číslo webovej objednávky.",
        maxlength: "Zadajte číslo webovej objednávky.",
      },
      file1: {
        required: "Priložte požadovaný dokument.",
      },
      file2: {
        required: "Priložte požadovaný dokument.",
      },
      file3: {
        required: "Priložte požadovaný dokument.",
      },
    },
    errorClass: "error",
    validClass: "valid",
    highlight: function (element, errorClass, validClass) {
      $(element)
        .parents(".form-group")
        .addClass(errorClass)
        .removeClass(validClass);
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element)
        .parents(".form-group")
        .removeClass(errorClass)
        .addClass(validClass);
    },
  });

  // custom phone number validation rule
  $.validator.addMethod("customNumberValidation", function (value, element) {
    if ($(element).val() === "") {
      return true;
    } else {
      return /^(((\+4219)|(09))[0-9]{2}\s*[0-9]{3}\s*[0-9]{3})$/.test(value);
    }
  });

  // custom email validation rule
  $.validator.addMethod("customEmailValidation", function (value, element) {
    return (
      /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
        value
      ) && /^(?=.{1,64}@.{4,64}$)(?=.{6,100}$).*/.test(value)
    );
  });
  $(".add-other-doc").on("click", function () {
    $(".other-doc").show();
    $(".add-other-doc-wrapp").hide();
  });

  sendFormBtn.on("click", function (e) {
    e.preventDefault();
    //check if form is valid & send form ...
    if ($form.valid() == true) {
      $form.submit();
      sendFormBtn.hide();
      setTimeout(function () {
        sendFormBtn.show();
      }, 3000);
    } else {
      e.preventDefault();
    }
  });

  // TOOL TIP

  $("#customerCode").qtip({
    content:
      "Ak ste náš zákazník, číslo adresáta nájdete v pravom hornom rohu faktúry. Ak ste novým zákazníkom, kód adresáta nevypĺňajte.",
    style: {
      classes: "qtip-tipsy",
    },
    position: {
      corner: {
        target: "leftMiddle",
        tooltip: "leftMiddle",
      },
    },
    // position: {
    //     my: 'bottom center',
    //     at: 'top center'
    // }
  });
  $("#order").qtip({
    content: "Číslo nájdete v potvrdzujúcom e-maile vašej objednávky.",
    style: {
      classes: "qtip-tipsy",
    },
    position: {
      corner: {
        target: "leftMiddle",
        tooltip: "leftMiddle",
      },
    },
    // position: {
    //     my: 'bottom center',
    //     at: 'top center'
    // }
  });
});
