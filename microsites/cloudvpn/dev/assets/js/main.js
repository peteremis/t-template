$(function () {
     /* HERO VIDEO */
     $(".video-container").click(function() {
       $(".video-embed").css({ "opacity": "1", "display": "block" });
       $(".video-embed")[0].src += "&autoplay=1";
       $(".play_btn").css({ "opacity": "0", "display": "none" });
       $(this).unbind("click");
     });
     $(".play_btn").click(function() {
       $(".video-embed").css({ "opacity": "1", "display": "block" });
       $(".video-embed")[0].src += "&autoplay=1";
       $(".play_btn").css({ "opacity": "0", "display": "none" });
       $(this).unbind("click");
     });


        // $('.video').live('click',function(event) {
        //     event.preventDefault();
        //     src = $(this).next('.hiddenVideo').find('iframe').attr('src');
        //     $(this).next('.hiddenVideo').find('iframe').height(parseInt($(this).height()));
        //     $(this).next('.hiddenVideo').find('iframe').width(parseInt($(this).width()));
        //     $(this).next('.hiddenVideo').find('iframe').attr('src', src + '&autoplay=1');
        //     var embedCode = $(this).next('.hiddenVideo').html();
        //     $(this).html(embedCode);
        //     $(this).next('.hiddenVideo').empty();
        // });

    /* STICKY NAV */

    $("#nav-sticky").sticky({
        topSpacing: 0,
        widthFromWrapper: true
    });

    $(".scroll-to").click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top - ($("#nav-sticky").outerHeight())
                }, 1000);
                return false;
            }
        }
    });

    var contentSections = $("[id*='sec-']"),
        secondaryNav = $("#nav-sticky");

    function updateSecondaryNavigation() {
        contentSections.each(function () {
            var actual = $(this),
                actualHeight = actual.height() + parseInt(actual.css('paddingTop').replace('px', '')) + parseInt(actual.css('paddingBottom').replace('px', '')),
                actualAnchor = secondaryNav.find('a[href="#' + actual.attr('id') + '"]');

            if ((actual.offset().top - secondaryNav.outerHeight() <= $(window).scrollTop()) && (actual.offset().top + actualHeight - secondaryNav.outerHeight() > $(window).scrollTop())) {
                actualAnchor.addClass('active');
            } else {
                actualAnchor.removeClass('active');
            }

        });
    }
    updateSecondaryNavigation();

    $(window).scroll(function (event) {
        updateSecondaryNavigation();
    });
});
