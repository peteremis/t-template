$(document).ready(function () {
  /* TABS */
  $(".tab-selector").click(function (event) {
    $(".tab-selector").removeClass("is-checked");
    $(this).addClass("is-checked");
    var tab = $(this).attr("href");
    $(".my-tab-content").css("display", "none");
    $(tab).show();
  });

  /* SCROLL */
  $(document).on("click", 'a[href^="#"]', function (event) {
    event.preventDefault();

    $("html, body").animate(
      {
        scrollTop: $($.attr(this, "href")).offset().top,
      },
      1000
    );
  });
});
