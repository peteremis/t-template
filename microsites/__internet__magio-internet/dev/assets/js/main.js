$(function () {
  $(".rpln-m").qtip({
    content: {
      text:
        "Rýchlosť internetu je závislá od technológie, ktorá je dostupná na adrese zriadenia služby.<br>Technológie, ktoré poskytujeme:<br>Optika: 30/3 Mb/s<br>Optika k domu - VDSL: 10/1 Mb/s<br>ADSL: 4/0,5 Mb/s",
    },
    position: {
      my: "top center",
      at: "bottom center",
    },
  });
  $(".rpln-l").qtip({
    content: {
      text:
        "Rýchlosť internetu je závislá od technológie, ktorá je dostupná na adrese zriadenia služby.<br>Technológie, ktoré poskytujeme:<br>Optika: 250/25 Mb/s<br>Optika k domu - VDSL: 40/4 Mb/s<br>ADSL: 8/1 Mb/s",
    },
    position: {
      my: "top center",
      at: "bottom center",
    },
  });
  $(".rpln-xl").qtip({
    content: {
      text:
        "Rýchlosť internetu je závislá od technológie, ktorá je dostupná na adrese zriadenia služby.<br>Technológie, ktoré poskytujeme:<br>Optika: 600/60 Mb/s<br>Optika k domu - VDSL: 90/9 Mb/s<br>ADSL: 15/1 Mb/s",
    },
    position: {
      my: "top center",
      at: "bottom center",
    },
  });

  /* scroll to + sticky nav */
  $("#nav-sticky").sticky({
    topSpacing: 0,
    widthFromWrapper: true,
  });

  $(".scroll-to").click(function () {
    if (
      location.pathname.replace(/^\//, "") ==
        this.pathname.replace(/^\//, "") &&
      location.hostname == this.hostname
    ) {
      var target = $(this.hash);
      target = target.length ? target : $("[name=" + this.hash.slice(1) + "]");
      if (target.length) {
        $("html,body").animate(
          {
            scrollTop: target.offset().top - $("#nav-sticky").outerHeight(),
          },
          1000
        );
        return false;
      }
    }
  });

  var contentSections = $("[id*='sec-']"),
    secondaryNav = $("#nav-sticky");

  function updateSecondaryNavigation() {
    contentSections.each(function () {
      var actual = $(this),
        actualHeight =
          actual.height() +
          parseInt(actual.css("paddingTop").replace("px", "")) +
          parseInt(actual.css("paddingBottom").replace("px", "")),
        actualAnchor = secondaryNav.find(
          'a[href="#' + actual.attr("id") + '"]'
        );

      if (
        actual.offset().top - secondaryNav.outerHeight() <=
          $(window).scrollTop() &&
        actual.offset().top + actualHeight - secondaryNav.outerHeight() >
          $(window).scrollTop()
      ) {
        actualAnchor.addClass("active");
      } else {
        actualAnchor.removeClass("active");
      }
    });
  }
  updateSecondaryNavigation();

  $(window).scroll(function (event) {
    updateSecondaryNavigation();
  });
  /* scroll to + sticky nav END */
});

$(document).ready(function () {
  $("#slider_internet").slick({
    slidesToShow: 3,
    slidesToScroll: 3,
    centerMode: false,
    infinite: false,
    swipe: true,
    arrows: true,
    dots: true,
    autoplay: false,
    initialSlide: 0,
    autoplaySpeed: 5000,
    touchMove: false,
    draggable: false,

    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          arrows: true,
          dots: true,
        },
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          arrows: true,
          dots: true,
        },
      },
    ],
  });

  $(".popup").fancybox({
    padding: 10,
    margin: 30,
    closeBtn: false,
    width: 550,
    height: "auto",
    autoSize: false,
    parent: "#content",
    helpers: {
      overlay: {
        css: {
          background: "rgba(0, 0, 0, 0.7)",
        },
      },
    },
  });

  $(".p-close, .link-close, .text-close, .close-btn").on("click", function (e) {
    e.preventDefault();
    $.fancybox.close();
  });
});
