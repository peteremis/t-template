$(function() {


    /* TOGGLE JAVASCRIPT CODE */
    var initCodeBlock = $('.code-block').find('code[class*=language-javascript]').closest('pre');
    initCodeBlock.hide();

    $('.code-block').on('click', function(e){
      var jsH6element = e.target;
      var elToHide = $(jsH6element).next();
      if (elToHide.hasClass('language-javascript')) {
        elToHide.slideToggle();
      }
    });
      /* TOGGLE JAVASCRIPT CODE END */

  /* HEAPBOX */
  $('#heap').heapbox();

  /* HEAPBOX NO ARROW */
  $('#heap-noArrow').heapbox();

/* FANCYBOX*/
  $(".fancy").fancybox();

  $('.fancy-kred').fancybox({
      maxWidth: 320
  });

  $('.fancybox-media').fancybox({
      openEffect: 'none',
      closeEffect: 'none',
      helpers: {
          media: {}
      }
  });
/* FANCYBOX END*/








/* POPUP */
    $('.popup').fancybox({
        padding: 0,
        margin: 0,
        closeBtn: false,
        parent: "#content",
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.7)'
                }
            }
        }
    });

    $('.p-close, .link-close, .text-close, .close-btn').on('click', function(e) {
        e.preventDefault();
        $.fancybox.close();
    });
/* POPUP END */





/* TABS */
$(".tabs-menu a").click(function(event) {
    event.preventDefault();
    $(this).parent().addClass("current");
    $(this).parent().siblings().removeClass("current");
    var tab = $(this).attr("href"), // select clicked tab. In example "#tab-1" ..
        parent = $(this).closest('ul').parent().parent(); // <tabs-container>
    parent.find('.tab-content').not(tab).css("display", "none"); //hide all not clicked tabs
    $(tab).fadeIn(); //show clicked tab


});
/* TABS END */

var dataSegment = $("[data-segment]").each(function(){
  //console.log($(this).attr('data-segment')); //zobraz "data-segment" hodnotu pre kazdu <section>

  closestHead = $($(this).find('.tabs-menu a'));
  closestItemCount = closestHead.length - 1; //vytiahni pocet tabov pre kazdu <section> s atributom "data-segment"
  //console.log(closestItemCount + "heads");

  closestContent = $($(this).find('.tab-content')); //najdi tab-content v danej <section>
  //console.log(closestContent);

  closestAccordion = $(this).find('.accordion'); //najdi .accordion v danej <section>

  accordionItem = '<div class="item">';

  for (var i = 0; i <= closestItemCount; i++) {
      accordionItem += '<div class="heading">' + $(closestHead[i]).text()  + '</div>';
      accordionItem += '<div class="content">' + $(closestContent[i]).html() + '</div></div>';

      if (i !== closestItemCount) {
          accordionItem += '<div class="item">';
      }
  }

  //if data-segment and data-accordion value match, show accordion data
  if($(this).attr('data-segment') === closestAccordion.attr('data-accordion')) {
       $(accordionItem).appendTo(closestAccordion);
  }

  var $items = $('.accordion .item');
  //SET OPENED ITEM
  $($items[0]).addClass('open');

});



/* FROM TABS TO ACCORDION - OLD */
  //  var $accordion = $(".accordion"),
  //  $heads = $('.tabs-menu.default a'),
  //  $contents = $('.tab-content.default'),
  //  itemCount = $($heads).length - 1,
  //  accordionCode = '<div class="item">';
  //
  // loop for defaultAccordion
  //  for (var i = 0; i <= itemCount; i++) {
  //      accordionCode += '<div class="heading">' + $($heads[i]).text()  + '</div>';
  //      accordionCode += '<div class="content">' + $($contents[i]).html() + '</div></div>';
  //
  //      if (i !== itemCount) {
  //          accordionCode += '<div class="item">';
  //      }
  //  }
  //
  //  $(accordionCode).appendTo($accordion);
  //
  //  var $items = $('.accordion .item');
  //  //SET OPENED ITEM
  //  $($items[0]).addClass('open');
/* FROM TABS TO ACCORDION END - OLD */


/* ACCORDION */
$('.accordion .item .heading').click(function(e) {

    var clickedHead = e.target;
    var $item = $(clickedHead).closest('.item');
    var isOpen = $item.hasClass('open');
    var $content = $item.find('.content');
    var $acPrice = $(clickedHead).find('.ac-price');

    if (isOpen) {
        $content.slideUp(200);
        $item.removeClass('open');
        $acPrice.show();
    } else {
        $content.slideDown(200);
        $item.addClass('open');
        $acPrice.hide();
    }
});


$('.accordion .item.open').find('.content').slideDown(200);
/* ACCORDION END */




/* TOOLTIP */
$('#info-balik-volania, #info-tvDostupnost').qtip({
    content: 'Balík aktivujete na webe počas kúpy Happy paušálu.',
    style: {
        classes: 'qtip-tipsy'
    },
    position: {
        corner: {
            target: 'leftMiddle',
            tooltip: 'topMiddle'
        }
    }
    // position: {
    //     my: 'bottom center',
    //     at: 'top center'
    // }
});




/* SLIDER SLICK.js */
$("#program_slider").slick({
    slidesToShow: 2,
    slidesToScroll: 1,
    centerMode: false,
    infinite: false,
    swipeToSlide: true,
    swipe: true,
    arrows: true,
    dots: true,
    autoplay: false,
    initialSlide: 0,
    autoplaySpeed: 5000,
    touchMove: false,
    draggable: false,
    // customPaging: function(slider, i) {
    //     return '<span class="slide-dot">&nbsp;</span>';
    // },
    responsive: [{
        breakpoint: 1024,
        settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            arrows: true,
            dots: true
        }
    }, {
        breakpoint: 768,
        settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
            infinite: false,
            dots: true
        }
    }]
});
/* SLIDER SLICK.js END */


/* TOGGLE ARROW */
$('.toggle-arrow, .arrow-right').click(function(event) {
  event.preventDefault();
  if ($(this).hasClass('arrow-right')) {
    return;
  }
  $(event.target).closest('.togleArrow').find('.arrow-content').toggle(200);
  $(this).find('.arrow-right').toggleClass('arrow-rotate');
});
/* TOGGLE ARROW END */


/* RANGE INPUT SLIDER */
  //live update number on page
  $('.range-slider').each(function(){
	var range = $(this).find('.input-range'); // range input
  //console.log($(range));
   var value = $(this).find('.range-value'); // range display
   //console.log(value);
	// set / display initial starting range value
	var startingVal = range.prop('value')
	value.html(startingVal + '&euro;');
	// live update range value
	range.on('input', function(){
		roundAmount()
	});
	// live update range value - IE
	range.on('change', function(){
		roundAmount()
	});
	// round number based on range amount
	function roundAmount() {
		var liveVal = range.prop('value')
		 value.html(liveVal + '&euro;');
	};
function checkBrowsers () {
  //check if browser = Edge
	if (/Edge\/\d./i.test(navigator.userAgent)){
   	$('input[type="range"]').css('background-image', 'none')
	}
  //check if browser = MSIE
  if (/MSIE 9/i.test(navigator.userAgent) || /rv:11.0/i.test(navigator.userAgent) || /rv:10.0/i.test(navigator.userAgent)) {
		 // This is internet explorer 9,10 or 11 fix
		 $('.input-range').css('height', '20px')
	}
  //check if browser = Firefox
  if (/Firefox\/\d./i.test(navigator.userAgent)){
     $('input[type="range"]').css(
       {'background-image':'transparent',
        'height':'1px'}
     );
  }
}

$('input[type="range"]').on('input', function () {
    var val = ($(this).val() - $(this).attr('min')) / ($(this).attr('max') - $(this).attr('min'));

    $(this).css('background-image', '-webkit-gradient(linear, left top, right top, '
                + 'color-stop(' + val + ', #94A14E), '
                + 'color-stop(' + val + ', #C5C5C5)'
                + ')'
                );
	// check browser on slider change
	//remove BG if EDGE or FIREFOX
	  checkBrowsers();

});
	// check browser on initial load
	//remove BG if EDGE or FIREFOX
	  checkBrowsers();
  });
/* RANGE INPUT SLIDER END */



/* SEARCH BAR - X click */
  //delete value on X button click
$('.close-bttn').click(function(event){
        event.preventDefault();
        $('input[id="search-bar"]').val('');
    });
  // delete value on focus and hide magnifying-glass
$('input[id="search-bar"]').focus(function(){
        $(this).val('');
        $('#magnifying-glass').hide();
        $('#search-bar').css('padding-left','10px')
    });
// on focusout, get magnifying-glass back
    $('input[id="search-bar"]').focusout(function(){
            $('#magnifying-glass').show();
            $('#search-bar').css('padding-left','35px')
        });
/* SEARCH BAR - X click END */

/* SWITCHERY */
var elem = document.querySelector('.js-switch');
var init = new Switchery(elem, { color: '#6bb324', secondaryColor: '#ededed' });
$('.switchery').parent().closest('span').addClass('hide-span');
/* SWITCHERY END */

/* SECONDARY RADIO BTNS */
//default magenta color for radio box
var defaultCheckboxColor = $('input[name=rb-pair]:checked', '#secondaryRadio').attr('id');
$('.secondaryRadio').find('label[for='+  defaultCheckboxColor  +']').css({'background-color':'#e20074',
              'color':'#fff'});

//on click set magenta color to selected radio box and make other radio boxes transparent
$(".secondaryRadio input").click( function(){
  var getAttr = $(this).attr('id');
  var label = $('.secondaryRadio').find('label[for='+  getAttr  +']');
  var allCheckboxes = $(".secondaryRadio label").css({'background-color':'transparent',
'color':'#000'})

   if( $(this).is(':checked')) {
     $(label).css({'background-color':'#e20074',
                   'color':'#fff'});
   }
});
/* SECONDARY RADIO BTNS END */


/* FILTERED SEARCH select2 plugin */
$("#filter1").select2({
	closeOnSelect:true
  });

//fix for remove automatically created heapBox
$('.select2-container').prev().remove();

$("#filter1").change(function(){
       //alert($(this).val());
       var val = $('#filter1').val();

        $('div#multiselect').each(function(){
          var divAttr = $(this).attr('class');
          //console.log("div:" + divAttr);
          var getValue = val;

         	if (getValue == null) {
              $('div#multiselect').hide();
             } else {
              for (i = 0; i <= getValue.length; i++) {
                //console.log(getValue[i]);
              if (getValue[i] === divAttr) {
                $('div[class='+getValue[i]+']').show();
                //console.log("true");
              } else {
               var show = $('div[class='+getValue[i]+']').show();
               $(show).siblings().hide();
              }
             }
            }
        });
});

$("#filter1")
        .val('M') //je potrebne nastavit pociatocnu hodnotu filtra, napr. pre pausal M, value="M"
        .trigger('change');
/* FILTERED SEARCH select2 plugin END */



});
