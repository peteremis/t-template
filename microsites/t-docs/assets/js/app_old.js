$(function () {

  /* TOGGLE JAVASCRIPT CODE */
  var initCodeBlock = $('.code-block').find('code[class*=language-javascript]').closest('pre');
  initCodeBlock.hide();

  $('.code-block').on('click', function(e){
    var jsH6element = e.target;
    var elToHide = $(jsH6element).next();
    if (elToHide.hasClass('language-javascript')) {
      elToHide.slideToggle();
    }
  });
    /* TOGGLE JAVASCRIPT CODE END */


    /* HEAPBOX */
    $('#heap').heapbox();

    /* TOOLTIP */
    $('#info-balik-volania').qtip({
        content: 'Balík aktivujete na webe počas kúpy Happy paušálu.',
        style: {
            classes: 'qtip-tipsy'
        },
        position: {
            corner: {
                target: 'leftMiddle',
                tooltip: 'topMiddle'
            }
        }
        // position: {
        //     my: 'bottom center',
        //     at: 'top center'
        // }
    });

    /* TABS */
    $(".tabs-menu a").click(function(event) {
        event.preventDefault();
        $(this).parent().addClass("current");
        $(this).parent().siblings().removeClass("current");
        var tab = $(this).attr("href"), // select clicked tab. In example "#tab-1" ..
            parent = $(this).closest('ul').parent().parent(); // <tabs-container>
        parent.find('.tab-content').not(tab).css("display", "none"); //hide all not clicked tabs
        $(tab).fadeIn(); //show clicked tab
    });
    /* TABS END */

    /* FROM TABS TO ACCORDION */
       var $accordion = $('.accordion'),
       $heads = $('.tabs-menu a'),
       $contents = $('.tab-content'),
       itemCount = $($heads).length - 1,
       accordionCode = '<div class="item">';

       for (var i = 0; i <= itemCount; i++) {
           accordionCode += '<div class="heading">' + $($heads[i]).text()  + '</div>';
           accordionCode += '<div class="content">' + $($contents[i]).html() + '</div></div>';

           if (i !== itemCount) {
               accordionCode += '<div class="item">';
           }
       }

       $(accordionCode).appendTo($accordion);

       var $items = $('.accordion .item');
       //SET OPENED ITEM
       $($items[0]).addClass('open');
    /* FROM TABS TO ACCORDION END */

    /* ACCORDION */
    $('.accordion .item .heading').click(function(e) {

        var clickedHead = e.target;
        var $item = $(clickedHead).closest('.item');
        var isOpen = $item.hasClass('open');
        var $content = $item.find('.content');
        var $acPrice = $(clickedHead).find('.ac-price');

        if (isOpen) {
            $content.slideUp(200);
            $item.removeClass('open');
            $acPrice.show();
        } else {
            $content.slideDown(200);
            $item.addClass('open');
            $acPrice.hide();
        }
    });

    $('.accordion .item.open').find('.content').slideDown(200);
    /* ACCORDION END */

    $('.btn, .cta, .link, .play').on('click', function(e){
        e.preventDefault();
    });

    /* POPUP */

    $('.popup').fancybox({
        padding: 0,
        margin: 0,
        closeBtn: false,
        helpers : {
            overlay : {
                css : {
                    'background' : 'rgba(0, 0, 0, 0.7)'
                }
            }
        }
    });

    $('.p-close, .link-close, .text-close').on('click', function(e) {
        e.preventDefault();
        $.fancybox.close();
    });
    /* POPUP END*/

    /* FANCYBOX*/
      $(".fancy").fancybox();

      $('.fancy-kred').fancybox({
          maxWidth: 320
      });

      $('.fancybox-media').fancybox({
          openEffect: 'none',
          closeEffect: 'none',
          helpers: {
              media: {}
          }
      });
    /* FANCYBOX END*/

    /* ::: Slider ::: */
    $("#program_slider").slick({
        slidesToShow: 2,
        slidesToScroll: 1,
        centerMode: false,
        infinite: false,
        swipeToSlide: true,
        swipe: true,
        arrows: true,
        dots: true,
        autoplay: false,
        initialSlide: 0,
        autoplaySpeed: 5000,
        touchMove: false,
        draggable: false,
        // customPaging: function(slider, i) {
        //     return '<span class="slide-dot">&nbsp;</span>';
        // },
        responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
                arrows: true,
                dots: true
            }
        }, {
            breakpoint: 768,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: true,
                infinite: false,
                dots: true
            }
        }]
    });
});




/* TOGGLE ARROW */
var arrow = $('.arrow-right');
var arrowContent = $('.arrow-content');

$('.toggle-arrow').click(function(event) {
  arrow.toggleClass('arrow-rotate');
  arrowContent.toggle(200);
    event.preventDefault();
});
/* TOGGLE ARROW END */



/* RANGE INPUT SLIDER */
  //live update number on page
	var range = $('.input-range'); // range input
   var value = $('.range-value'); // range display
	// set / display initial starting range value
	var startingVal = range.prop('value')
	value.html(startingVal + '&euro;');
	// live update range value
	range.on('input', function(){
		roundAmount()
	});
	// live update range value - IE
	range.on('change', function(){
		roundAmount()
	});
	// round number based on range amount
	function roundAmount() {
		var liveVal = range.prop('value')
		 value.html(liveVal + '&euro;');
	};

function checkBrowsers () {
  //check if browser = Edge
	if (/Edge\/\d./i.test(navigator.userAgent)){
   	$('input[type="range"]').css('background-image', 'none')
	}
  //check if browser = MSIE
  if (/MSIE 9/i.test(navigator.userAgent) || /rv:11.0/i.test(navigator.userAgent) || /rv:10.0/i.test(navigator.userAgent)) {
		 // This is internet explorer 9,10 or 11 fix
		 $('.input-range').css('height', '20px')
	}
  //check if browser = Firefox
  if (/Firefox\/\d./i.test(navigator.userAgent)){
     $('input[type="range"]').css(
       {'background-image':'transparent',
        'height':'1px'}
     );
  }
}

$('input[type="range"]').on('input', function () {
    var val = ($(this).val() - $(this).attr('min')) / ($(this).attr('max') - $(this).attr('min'));

    $(this).css('background-image',
                '-webkit-gradient(linear, left top, right top, '
                + 'color-stop(' + val + ', #94A14E), '
                + 'color-stop(' + val + ', #C5C5C5)'
                + ')'
                );
	// check browser on slider change
	//remove BG if EDGE or FIREFOX
	  checkBrowsers();

});
	// check browser on initial load
	//remove BG if EDGE or FIREFOX
	  checkBrowsers();

/* RANGE INPUT SLIDER END */



/* SEARCH BAR - X click */
  //delete value on X button click
$('.close-bttn').click(function(event){
        event.preventDefault();
        $('input[id="search-bar"]').val('');
    });
  // delete value on focus and hide magnifying-glass
$('input[id="search-bar"]').focus(function(){
        $(this).val('');
        $('#magnifying-glass').hide();
        $('#search-bar').css('padding-left','10px')
    });
// on focusout, get magnifying-glass back
    $('input[id="search-bar"]').focusout(function(){
            $('#magnifying-glass').show();
            $('#search-bar').css('padding-left','35px')
        });
/* SEARCH BAR - X click END */

/* SWITCHERY */
var elem = document.querySelector('.js-switch');
var init = new Switchery(elem, { color: '#6bb324', secondaryColor: '#ededed' });
$('.switchery').parent().closest('span').addClass('hide-span');
/* SWITCHERY END */

/* SECONDARY RADIO BTNS */
//default magenta color for radio box
var defaultCheckboxColor = $('input[name=rb-pair]:checked', '#secondaryRadio').attr('id');
$('.secondaryRadio').find('label[for='+  defaultCheckboxColor  +']').css({'background-color':'#e20074',
              'color':'#fff'});

//on click set magenta color to selected radio box and make other radio boxes transparent
$(".secondaryRadio input").click( function(){
  var getAttr = $(this).attr('id');
  var label = $('.secondaryRadio').find('label[for='+  getAttr  +']');
  var allCheckboxes = $(".secondaryRadio label").css({'background-color':'transparent',
'color':'#000'})

   if( $(this).is(':checked')) {
     $(label).css({'background-color':'#e20074',
                   'color':'#fff'});
   }
});
/* SECONDARY RADIO BTNS END */
