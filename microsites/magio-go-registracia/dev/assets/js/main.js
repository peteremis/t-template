$(function () {

  var $header = $('.header');
  var $mainPage = $('#main-page');
  var $smsPage1 = $('#sms-page-1');
  var $smsPage2 = $('#sms-page-2');

  var $faqPage = $('#faq-page');

  var $smsPageInfo = $('#sms-page-info');
  var $smsPageInfoPremium = $('#sms-page-info-premium');
  var $smsPageInfoArchive = $('#sms-page-info-archive');
  var $smsPageInfoStandard = $('#sms-page-info-standard');
  var $smsPageSms = $('#sms-page-sms');

  var $accountPage1 = $('#account-page-1');
  var $accountPage2 = $('#account-page-2');

  var $successPage = $('#success-page');

  var $trialPageSuccess = $('#trial-page-success');
  var $trialPageError = $('#trial-page-error');

  var $trialPage1 = $('#trial-page-1');

  var $voucherPage1 = $('#voucher-page-1');
  var $voucherPage2 = $('#voucher-page-2');
  var $voucherPage3 = $('#voucher-page-3');

  var $agreeCheckbox = $("#agree");
  var $fullsms = $("#fullsms");
  var $agreeError = $("#agree-error");

  var smsToken;

  var apiURL = 'https://backvm.telekom.sk';
  var apiFolder = 'magiogo';

  if (document.location.host.contains('localhost')) {
    apiURL = 'http://backvm.telekom.localhost';
    // apiFolder = 'magiogot';

  } else {

    if (window.location.href.indexOf("magiogot") > -1) {
      apiFolder = 'magiogot';
    }

  }

  var apiURLRegistration = apiURL + '/' + apiFolder + '/register';
  var apiURLCheckCode = apiURL + '/' + apiFolder + '/validate';
  var apiURLCheckVoucher = apiURL + '/' + apiFolder + '/voucher';

  var language = 'sk';

  var errorCodeDefault = 'Zadajte prosím overovací kód';
  var errorMsisdnfault = 'Zadajte prosím vaše telefónne číslo';

  /*
  *
  * MSISDN
  */

  var $msisdnError = $('#msisdn-error');
  var $msisdn;

  var $msisdnCodeError = $('#msisdn-code-error');
  var $msisdnCode = $('#msisdn-code');

  var $customerMsisdn = $('.customer-msisdn');

  /*
  *
  * Customer
  *
  */
  var $customerNameInput = $('#customer-name');
  var $customerSurnameInput = $('#customer-surname');
  var $customerEmailInput = $('#customer-email');
  var $customerMsisdnInput = $('#customer-msisdn');
  var $customerMarketing = $('#marketing-checkbox');

  /*
  *
  * Voucher
  */

  var $voucherUsedError = $('#voucher-used-error');
  var $voucherCodeError = $('#voucher-code-error');
  var $voucherCode = $('#voucher-code');

  var accountType;
  accountType = 'trial';

  var magioPackage = 'standard';
  showMagioInfo(magioPackage);

  var magioInfo;

  // hideMainPage();

  // $faqPage.show();

  // $smsPage1.show();
  // $smsPage2.show();
  // $smsPageSms.show();
  // $smsPageInfo.show();
  // $smsPageInfoStandard.show();
  // $smsPageInfoPremium.show();

  // $accountPage1.show();
  // $accountPage2.show();

  // $voucherPage1.show();
  // $voucherPage2.show();
  // $voucherPage3.show();

  // $trialPage1.show();
  // $trialPageSuccess.show();
  // $trialPageError.show();

  function showMainPage() {
    $header.hide();
    $mainPage.show();
  }

  function hideMainPage() {
    $header.show();
    $mainPage.hide();
  }

  $(".back-sms-page-1").click(function () {
    $mainPage.show();
    $smsPage1.hide();
    clearError()
  });

  $(".back-sms-page-2").click(function () {
    $smsPage1.show();
    $smsPage2.hide();
    clearError()
  });

  $("#main-page .cta_mag").click(function () {
    hideMainPage();
    $smsPage1.show();
  });


  /*
  *
  *  Main page
  *
  */

  $("#voucherButton").click(function (e) {
    hideMainPage();
    $voucherPage1.show();

    accountType = 'voucher';
    e.preventDefault();
  });

  $("#trialButton").click(function (e) {
    hideMainPage();
    $trialPage1.show();

    accountType = 'trial';
    e.preventDefault();
  });

  /*
  *
  *  Account
  *
  */

  $("#account-back-1").click(function (e) {
    $accountPage1.hide();

    if (accountType == 'trial') {
      $trialPage1.show();

    } else if (accountType == 'voucher') {
      $voucherPage1.show();

    } else {
      showMainPage();
    }

    e.preventDefault();
  });


  $("#account-back-2").click(function (e) {

    console.log('back');

    $accountPage2.hide();
    $accountPage1.show();

    $msisdnCode.val('');

    clearError();
    e.preventDefault();
  });

  /*
  *
  *  FAQ
  *
  */

  $("#faq-back").click(function (e) {
    $faqPage.hide();
    showMainPage();

    e.preventDefault();
  });

  $("#go-faq-page").click(function (e) {
    console.log('xxx');
    hideMainPage();
    $faqPage.show();

    e.preventDefault();
  });


  /*
  *
  *  Trial
  *
  */

  $("#trial-back-1").click(function (e) {
    $trialPage1.hide();
    showMainPage();

    e.preventDefault();
  });

  $(".trial-page-success-back").click(function (e) {
    $trialPageSuccess.hide();
    $successPage.show();

    e.preventDefault();
  });

  $(".trial-page-error-back").click(function (e) {
    $trialPageError.hide();
    $mainPage.show();

    e.preventDefault();
  });

  /*
  *
  *  Voucher
  *
  */

  $("#voucher-back-1").click(function (e) {
    $voucherPage1.hide();
    showMainPage();

    e.preventDefault();
    clearError()
  });

  $("#voucher-back-2").click(function (e) {
    $voucherPage2.hide();
    $accountPage1.show();

    e.preventDefault();
    clearError()
  });

  $("#voucher-back-3").click(function (e) {
    $voucherPage3.hide();
    $accountPage1.show();

    e.preventDefault();
    clearError()
  });

  $("#goto-voucher-2").click(function (e) {
    $voucherPage1.hide();
    $accountPage1.show();

    e.preventDefault();
  });

  $("#goto-trial-2").click(function (e) {
    $trialPage1.hide();
    $accountPage1.show();

    e.preventDefault();
  });

  /*
  *
  *  SMS
  *
  */

  $("#sms-order-page-1").click(function (e) {
    magioPackage = $("input[name='magioType']:checked").val();
    // $mainPage.hide();

    showMagioInfo(magioPackage);

    $smsPage1.hide();
    $smsPage2.show();

    e.preventDefault();
  });

  $("#sms-page-2 .cta_mag").click(function (e) {
    $smsPage2.hide();
    $smsPageSms.show();

    e.preventDefault();
  });

  $('#modal-magio-premium').click(function (e) {
    magioInfo = 'premium';
    showMagioInfo(magioInfo);

    $smsPageInfo.show();
    $smsPage1.hide();

    e.preventDefault();
  });

  $('#modal-magio-standard').click(function (e) {
    magioInfo = 'standard';
    showMagioInfo(magioInfo);

    $smsPageInfo.show();
    $smsPage1.hide();

    e.preventDefault();
  });

  $('#btn-info-premium').click(function (e) {
    showMagioInfo('standard');

    $smsPageInfoPremium.hide();
    $smsPage2.hide();

    e.preventDefault();
  });

  $('.info-close').click(function (e) {

    $smsPageInfo.hide();
    $smsPage1.show();

    e.preventDefault();
  });

  $('.sms-close').click(function (e) {
    $smsPageSms.hide();
    $smsPage2.show();

    e.preventDefault();
  });

  $('.back-sms-page-stanice').click(function (e) {
    $smsPageInfoStandard.hide();
    $smsPageInfoPremium.hide();
    $smsPageInfoArchive.hide();

    // $smsPage1.show();
    $smsPageInfo.show();

    e.preventDefault();
  });

  $('.back-sms-final').click(function (e) {
    $smsPageSms.hide();
    $mainPage.show();

    e.preventDefault();
  });

  $('#stanice-info').click(function (e) {

    console.log(magioInfo);

    $smsPageInfo.hide();
    if (magioInfo == 'standard') {
      $smsPageInfoStandard.show();
    } else {
      $smsPageInfoPremium.show();
    }

    e.preventDefault();
  });

  $('#stanice-archive').click(function (e) {
    $smsPageInfo.hide();
    $smsPageInfoArchive.show();

    e.preventDefault();
  });

  $fullsms.click(function (e) {

    console.log('cc');

    if ($agreeCheckbox.is(':not(:checked)')) {
      $agreeError.show();
      return false;
    }

    $agreeError.hide();

  });

  $('#date-go-standard').click(function (e) {

    console.log('st');
    $('#uniform-sms-go-standard span').addClass('checked');
    $('#sms-go-standard').prop("checked", true);

    $('#uniform-sms-go-premium span').removeClass('checked');
    $('#sms-go-premium').prop("checked", false);

    e.preventDefault();
  });

  $('#date-go-premium').click(function (e) {

    console.log('pr');
    $('#uniform-sms-go-standard span').removeClass('checked');
    $('#sms-go-premium').prop("checked", true);

    $('#uniform-sms-go-premium span').addClass('checked');
    $('#sms-go-premium').prop("checked", true);

    e.preventDefault();
  });

  function showMagioInfo(magioType) {

    var magio = getMagioInfo(magioType);

    $('.magioInfoname').html(magio.magioName);
    $('.magioInfoDesc').html(magio.magioDesc);
    $('.magioInfoPrice').html(magio.magioPrice);
    $('.magioInfoNumber').html(magio.magioNumber);
    $('.magioInfoSms').html(magio.magioSms);

    $('#fullsms').attr("href", magio.magioFullSms);

  }

  function getMagioInfo(magioType) {

    var magioName = 'Magio GO L';
    var magioDesc = 'Prémiový balík slovenských a zahraničných TV staníc vrátane exkluzívnych Digi Sport či detskej Ťuki TV. S prístupom do 7 dňového archívu Basic a množstvom zaujímavých funkcií.';
    var magioPrice = '25,90';
    var magioNumber = '95+';
    var magioSms = 'L';
    // var magioFullSms = 'sms:8866?&amp;body=MAGIOGO%20P%20ZAP';
    // var magioFullSms = 'sms:8866?body=MAGIOGO P ZAP';
    // var magioFullSms = 'sms://8866?&body=MAGIOGO P ZAP';
    var magioFullSms = 'sms:8866?&body=MAGIOGO L ZAP';

    if (magioType == 'standard') {
      magioName = 'Magio GO Štandard';
      magioDesc = 'Základný balík slovenských a českých TV staníc s prístupom do 7&nbsp;dňového archívu a množstvom zaujímavých funkcií.';
      magioPrice = '4';
      magioNumber = '12';
      magioSms = 'S';
      // magioFullSms = 'sms:8866?body=MAGIOGO S ZAP';
      magioFullSms = 'sms:8866?&body=MAGIOGO S ZAP';
    }

    var response = {
      "magioName": magioName,
      "magioDesc": magioDesc,
      "magioPrice": magioPrice,
      "magioNumber": magioNumber,
      "magioSms": magioSms,
      "magioFullSms": magioFullSms
    };

    return response;

  }

  $('#msisdn-send').on('click', function (event) {

    var phoneValue = $customerMsisdnInput.val();
    var valid = true;

    if (phoneValue.length <= 8 || phoneValue.length > 12) {
      $msisdnError.show().html(errorMsisdnfault);
      valid = false;
    }

    if (valid) {

      $msisdn = phoneValue;

      $customerMsisdn.html($msisdn);

      registerCustomer($customerNameInput.val(), $customerSurnameInput.val(), $customerEmailInput.val(), $customerMsisdnInput.val(), accountType, $customerMarketing.val(), language, apiURLRegistration)
        .success(function (data) {

          if (data.response == 'OK') {
            $accountPage1.hide();
            $accountPage2.show();

            smsToken = data.token;
          } else {
            $msisdnError.show().html(data.desc);
          }

          console.log(data);
        })
        .fail(function (data) {
          $msisdnError.show().html(data.responseJSON.desc);
          console.log(data);
        });

    }

    event.preventDefault();

  });

  $('#msisdn-code-send').on('click', function (event) {

    var codeValue = $msisdnCode.val();
    var valid = true;

    if (codeValue.length != 6 || codeValue == '' || codeValue == '______') {
      $msisdnCodeError.show().html(errorCodeDefault);
      valid = false;
    }

    if (valid) {

      checkCode(smsToken, codeValue, language, apiURLCheckCode)
        .success(function (data) {

          if (data.response == 'OK') {
            $accountPage2.hide();

            if (accountType == 'trial') {
              $trialPageSuccess.show();
            } else {
              $voucherPage2.show();
            }
          } else {
            $msisdnCodeError.show().html(data.desc);
          }

        })
        .fail(function (data) {
          $msisdnCodeError.show().html(data.responseJSON.desc);
        });

    }

    event.preventDefault();

  });

  $('#voucher-code-send').on('click', function (event) {

    var codeValue = $voucherCode.val();
    var valid = true;

    console.log('code' + codeValue);

    if (codeValue == '' || codeValue == '____________') {
      $voucherCodeError.show();
      valid = false;
    }

    if (valid) {
      checkVoucher(smsToken, codeValue, language, apiURLCheckVoucher)
        .success(function (data) {

          if (data.response == 'OK') {
            console.log('OK');
            $voucherPage2.hide();
            $voucherPage3.show();
            $voucherUsedError.hide();
          } else {
            $voucherUsedError.show();
          }

        })
        .fail(function (data) {
          $voucherUsedError.show();
        });
    }

    event.preventDefault();

  });

  $('#voucher-success').on('click', function (event) {

    $voucherPage3.hide();
    $successPage.show();
    event.preventDefault();

  });

  $('#trial-success-close').on('click', function (event) {

    $trialPageCode.hide();
    $successPage.show();
    event.preventDefault();

  });

  function clearError() {
    $msisdnError.hide();
    $msisdnCodeError.hide();
    $voucherCodeError.hide();
    $voucherUsedError.hide();
    $agreeError.hide();
  }

});

function checkPhone(evt) {
  var charCode = (evt.which) ? evt.which : event.keyCode
  if (charCode > 31 && (charCode < 48 || charCode > 57))
    return false;

  // phoneHelp.hide();
  return true;
}


function checkPhoneCode(evt) {
  var charCode = (evt.which) ? evt.which : event.keyCode
  if (charCode > 31 && (charCode < 48 || charCode > 57))
    return false;

  // phoneHelp.hide();
  return true;
}

registerCustomer = function (name, surname, email, msisdn, service, marketing, language, apiUrl) {

  var values = {
    "name": name,
    "surname": surname,
    "email": email,
    "msisdn": msisdn,
    "service": service,
    "language": language,
    "marketing": marketing,
  };

  return $.ajax({
    type: "POST",
    url: apiUrl,
    data: JSON.stringify(values),
    dataType: "json",
    contentType: "application/json;charset=utf-8"
  });
}

checkCode = function (smsToken, code, language, apiUrl) {

  var values = {
    "token": smsToken,
    "code": code,
    "language": language,
  };

  return $.ajax({
    type: "POST",
    url: apiUrl,
    data: JSON.stringify(values),
    dataType: "json",
    contentType: "application/json;charset=utf-8"
  });
}


checkVoucher = function (smsToken, voucher, language, apiUrl) {

  var values = {
    "token": smsToken,
    "voucher": voucher,
    "language": language,
  };

  console.log('voucher');
  console.log(values);
  return $.ajax({
    type: "POST",
    url: apiUrl,
    data: JSON.stringify(values),
    dataType: "json",
    contentType: "application/json;charset=utf-8"
  });
}

/* tabs & sliders */
$(document).ready(function () {
  /* ::: DataDevice ::: */
  var $dataDeviceSlider = $('.data-device-slider');
  var $dataDevicePagin = $('.data-device-pagin');
  var $dataDevicePaginPrev = $('.data-device-pagin-prev');
  var $dataDevicePaginNext = $('.data-device-pagin-next');

  /* ::: TAB CONFIG ::: */
  $('ul.tabs li a').on('click', function (e) {

    console.log('xxx');

    $('.as1').css("visibility", "hidden");
    $('.as2').css("visibility", "hidden");
    var currentAttrValue = $(this).attr('href');
    $('.tab ' + currentAttrValue).show().siblings().hide();
    $(this).parent('li').addClass('current').siblings().removeClass('current');
    $('.as1').slick('setPosition', 0);
    $('.as2').slick('setPosition', 0);
    setTimeout(function () {
      $('.as1').css("visibility", "visible");
      $('.as2').css("visibility", "visible");
    }, 200);
    e.preventDefault();
  });

  /* ::: GET CURRENT SLIDE NUM ::: */
  $dataDeviceSlider.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
    //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
    var i = (currentSlide ? currentSlide : 0) + 1;
    $dataDevicePagin.text('(' + i + '/' + slick.slideCount + ')');
  });


  /* ::: PAGINATION INDICATOR ::: */
  if ($(window).width() < 769) {
    $dataDeviceSlider.on('init', function (event, slick) {
      if ($(window).width() < 769) {
        $('.data-device-slider .slick-dots li').css("width", 100 / $('.dataDevice__item').length + '%');
      }
    });
  }
  ;

});
