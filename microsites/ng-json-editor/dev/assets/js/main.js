 /* sticky navigation */
 $(function() {
     $("#nav-sticky").sticky({
         topSpacing: 0,
         widthFromWrapper: true
     });

     $(".scroll-to").click(function() {
         if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
             var target = $(this.hash);
             target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
             if (target.length) {
                 $('html,body').animate({
                     scrollTop: target.offset().top - ($("#nav-sticky").outerHeight())
                 }, 1000);
                 return false;
             }
         }
     });

     var contentSections = $("[id*='sec-']"),
         secondaryNav = $("#nav-sticky");

     function updateSecondaryNavigation() {
         contentSections.each(function() {
             var actual = $(this),
                 actualHeight = actual.height() + parseInt(actual.css('paddingTop').replace('px', '')) + parseInt(actual.css('paddingBottom').replace('px', '')),
                 actualAnchor = secondaryNav.find('a[href="#' + actual.attr('id') + '"]');

             if ((actual.offset().top - secondaryNav.outerHeight() <= $(window).scrollTop()) && (actual.offset().top + actualHeight - secondaryNav.outerHeight() > $(window).scrollTop())) {
                 actualAnchor.addClass('active');
             } else {
                 actualAnchor.removeClass('active');
             }
         });
     }
     updateSecondaryNavigation();

     $(window).scroll(function(event) {
         updateSecondaryNavigation();
     });
 });

 /* tooltips */
 $(function() {
     $('.info-1').qtip({
         content: {
             text: 'Pri programoch Mobilný internet S/M/L sa po vyčerpaní objemu dát 1 GB/5 GB/15 GB uplatňuje obmedzenie rýchlosti dátových prenosov max. na 64 kb/s.'
         }
     });
     $('.info-tablet').qtip({
         content: {
             text: 'Vybrané paušály sú:<br> Happy M, L, XL volania, XL, XXL a Profi'
         }
     });

     $('#zasady-tooltip').qtip({
         content: {
             text: 'Vďaka Zásadám férového používania (anglická skratka FUP) zabezpečujeme rovnakú kvalitu ' +
                 'všetkým zákazníkom a predchádzame tomu, aby jeden zákazník čerpal nadmerne dáta na úkor ostatných. <br>' +
                 'Preto sú naše neobmedzené dáta v objeme 100 GB. ' +
                 'Po ich prečerpaní neplatíte nič navyše, ale prenos dát sa spomalí.'
         },
         position: {
             my: 'left center',
             at: 'right center'
         },
         style: {
             classes: 'tooltip tooltip-left'
         }
     });
 });
 /* popup */
 $(function() {
     $('.popup').fancybox({
         padding: 11,
         margin: 0,
         closeBtn: false,
         helpers: {
             overlay: {
                 css: {
                     'background': 'rgba(0, 0, 0, 0.7)'
                 }
             }
         }
     });

     $('.p-close').on('click', function(e) {
         e.preventDefault();
         $.fancybox.close();
     });
 });

 /* tabs & sliders */
 $(document).ready(function() {
     /* ::: DataDevice ::: */
     var $dataDeviceSlider = $('.data-device-slider');
     var $dataDevicePagin = $('.data-device-pagin');
     var $dataDevicePaginPrev = $('.data-device-pagin-prev');
     var $dataDevicePaginNext = $('.data-device-pagin-next');

     /* ::: TAB CONFIG ::: */
     $('ul.tabs li a').on('click', function(e) {
         $('.as2').css("visibility", "hidden");
         var currentAttrValue = $(this).attr('href');
         $('.tab ' + currentAttrValue).show().siblings().hide();
         $(this).parent('li').addClass('current').siblings().removeClass('current');
         setTimeout(function() {
             $('.as2').slick('setPosition', 0);
             $('.as2').css("visibility", "visible");
             //console.log('click');
         }, 200);
         e.preventDefault();
     });

     /* ::: GET CURRENT SLIDE NUM ::: */
     $dataDeviceSlider.on('init reInit afterChange', function(event, slick, currentSlide, nextSlide) {
         //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
         var i = (currentSlide ? currentSlide : 0) + 1;
         $dataDevicePagin.text('(' + i + '/' + slick.slideCount + ')');
     });


     /* ::: PAGINATION INDICATOR ::: */
     if ($(window).width() < 769) {
         $dataDeviceSlider.on('init', function(event, slick) {
             if ($(window).width() < 769) {
                 $('.data-device-slider .slick-dots li').css("width", 100 / $('.dataDevice__item').length + '%');
             }
         });
     };

     /* ::: PAGINATION ::: */
     /* pagination dataDevice */
     $dataDevicePaginPrev.click(function() {
         $dataDeviceSlider.slick('slickPrev');
     });
     $dataDevicePaginNext.click(function() {
         $dataDeviceSlider.slick('slickNext');
     });

     /* ::: Slider DataDevice ::: */
     $dataDeviceSlider.slick({
         slidesToShow: 4,
         slidesToScroll: 1,
         centerMode: false,
         infinite: false,
         swipeToSlide: true,
         swipe: true,
         arrows: true,
         dots: true,
         autoplay: false,
         initialSlide: 0,
         touchMove: false,
         draggable: false,
         customPaging: function(slider, i) {
             return '<span class="slide-dot"><!-- dot --></span>';
         },
         responsive: [{
             breakpoint: 1024,
             settings: {
                 slidesToShow: 3,
                 slidesToScroll: 1,
                 arrows: true,
                 dots: true
             }
         }, {
             breakpoint: 769,
             settings: {
                 slidesToShow: 1,
                 slidesToScroll: 1,
                 arrows: false,
                 infinite: false,
                 dots: true
             }
         }]
     });

 });
