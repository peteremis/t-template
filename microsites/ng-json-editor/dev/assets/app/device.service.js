(function() {

    'use strict';

    angular.module('core')
        .service('dataService', ['$http',
            function($http) {
                var tabletsJson = '../assets/data/tablets.json';
                var notebooksJson = '../assets/data/notebooks.json';
                this.allTablets = function() {
                    return $http.get(tabletsJson, {
                        cache: true
                    }).then(function(res) {
                        return res.data;
                    });
                };
                this.allNotebooks = function() {
                    return $http.get(notebooksJson, {
                        cache: true
                    }).then(function(res) {
                        return res.data;
                    });
                };
            }
        ])
        .factory('province', province);

    function province() {
        function getProvinces() {
            return [{
                "name": "Alberta",
                "value": "alberta"
            }, {
                "name": "British Columbia",
                "value": "british_columbia"
            }, {
                "name": "Manitoba",
                "value": "manitoba"
            }, {
                "name": "New Brunswick",
                "value": "new_brunswick"
            }, {
                "name": "Newfoundland and Labrador",
                "value": "newfoundland_and_labrador"
            }];
        }

        return {
            getProvinces: getProvinces
        }
    }

})();
