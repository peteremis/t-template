'use strict';

angular.
module('core')
    .filter('uniqueFilter', function() {
        // we will return a function which will take in a collection
        // and a keyname
        return function(collection, keyname) {
            // we define our output and keys array;
            // console.log('col',collection,'key', keyname);
            var output = [],
                keys = [];
            // we utilize angular's foreach function
            // this takes in our original collection and an iterator function
            angular.forEach(collection, function(item) {
                // we check to see whether our object exists
                var key = item[keyname];
                console.log('i', item);
                // if it's not already part of our keys array
                if (keys.indexOf(key) === -1) {
                    // add it to our keys array
                    keys.push(key);
                    // push this item to our final output array
                    output.push(item);
                }
            });
            // return our array which should be devoid of
            // any duplicates
            return output;
        };
    })
    .filter('capitalize', function() {
        return function(input) {
            return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
        }
    }).filter('deviceFilter', function() {
        return function(items, types) {
            var filtered = [];

            angular.forEach(items, function(item) {
                if (types.luxury == false && types.double_suite == false) {
                    filtered.push(item);
                } else if (types.luxury == true && types.double_suite == false && item.type == 'luxury') {
                    filtered.push(item);
                } else if (types.double_suite == true && types.luxury == false && item.type == 'double suite') {
                    filtered.push(item);
                }
            });

            return filtered;
        };
    });
