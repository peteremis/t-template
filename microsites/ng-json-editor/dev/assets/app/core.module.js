(function() {

    'use strict';

    // Define the `core` module
    angular.module('core', ['ngResource', 'slickCarousel', 'formly', 'formlyBootstrap', 'ngAria', 'ngAnimate', 'ngMaterial']);

})();
