(function() {

    'use strict';
    angular.module('jsonEditor')
        .run(function(formlyConfig) {
            formlyConfig.setType({
                name: 'customSelect',
                extends: 'select',
                defaultOptions: {
                    templateOptions: {
                        label: 'Custom Select Title',
                        options: [
                            { name: 'meno 1 ', value: 'name_1' },
                            { name: 'meno 2 ', value: 'name_2' },
                            { name: 'meno 3 ', value: 'name_3' }
                        ]
                    }
                }
            })
        });
    // Register `jsonEditor` component, along with its associated controller and template
    angular.
    module('jsonEditor').
    component('jsonEditor', {
        templateUrl: '../assets/app/jsonEditor.template.html',
        controller: ['$resource', 'dataService', '$timeout', 'province',
            function NotebooksListController($resource, dataService, $timeout, province) {
                var vm = this;

                vm.options = {};
                // funcation assignment
                vm.saveItem = saveItem;

                // The model object that we reference
                vm.tablet = {
                    id: 'tb001',
                    age: 0,
                    visible: true,
                    available: true,
                    name: 'tablet model',
                    imageUrl: 'http://www.telekom.sk/imageUrl',
                    formUrl: 'http://www.telekom.sk/formUrl',
                    infoUrl: "#",
                    ec: {
                        ecLeftVisible: false,
                        ecLeftUrl: "",
                        ecRightVisible: false,
                        ecRightUrl: ""
                    },
                    price: {
                        priceBase: 49,
                        priceDesc: "k Mobilnému internetu M"
                    },
                    description: {
                        desc1: "10\" FHD IPS displej",
                        desc2: "IP52 vodeodolný, prachuodolný",
                        desc3: "Výdrž batérie až 12 hodín"
                    },
                    descLightBox: [
                        { desc: ["Pamäť RAM:", "2 GB"] },
                        { desc: ["Pevný disk: ", "eMMC 32 GB"] },
                        { desc: ["Displej:", "10\" displej s technológiou IPS (In-Plane Switching), rozlíšenie HD 1920 x 1200, LED-backlit TFT LCD s integrovanou funkciou multitouch"] },
                        { desc: ["Webkamera:", "5 Mpx (predná s pevným zaostrením) + 8 Mpx (zadná s automatickým zaostrovaním)"] },
                        { desc: ["Čítačka kariet:", "microSD, 64 GB"] },
                        { desc: ["Mechanika:", "Nie"] },
                        { desc: ["Procesor:", "Štvorjadrový MediaTek™ 1.3 GHz 64-bit"] },
                        { desc: ["WiFi:", "802.11 a/b/g/n/ac"] },
                        { desc: ["GPS:", "GPS"] },
                        { desc: ["Bluetooth:", "áno, verzia BT 4.0"] },
                        { desc: ["HDMI:", "nie"] },
                        { desc: ["USB:", "microUSB"] },
                        { desc: ["Operačný systém:", "Android 6.0"] },
                        { desc: ["Extra numerická klávesnica:", "nie"] },
                        { desc: ["Rozmery v x š x h (mm):", "171 x 247 x 8,9 mm"] },
                        { desc: ["Hmotnosť:", "509 g"] },
                        { desc: ["Obsah balenia:", "tablet, nabíjačka, dátový USB kábel, používateľská príručka"] }
                    ]
                };

                // the 'fields' attribute on the  element
                vm.fields = [{
                        key: 'id',
                        type: 'input',
                        templateOptions: {
                            type: 'text',
                            label: 'ID',
                            placeholder: 'zadaj id tb001 ...',
                            required: true
                        }
                    }, {
                        key: 'age',
                        type: 'input',
                        templateOptions: {
                            type: 'number',
                            label: 'age',
                            placeholder: 'age',
                            required: true
                        }
                    }, {
                        key: 'visible',
                        type: 'checkbox',
                        templateOptions: {
                            label: 'visible',
                        },
                    }, {
                        key: 'available',
                        type: 'checkbox',
                        templateOptions: {
                            label: 'available',
                        },
                    }, {
                        key: 'name',
                        type: 'input',
                        templateOptions: {
                            label: 'name',
                            placeholder: 'name'
                        }
                    }, {
                        key: 'imageUrl',
                        type: 'input',
                        templateOptions: {
                            label: 'imageUrl',
                            placeholder: 'Enter your insurance policy number'
                        }
                    }, {
                        key: 'formUrl',
                        type: 'input',
                        templateOptions: {
                            label: 'formUrl',
                            placeholder: 'formUrl'
                        }
                    }, {
                        key: 'infoUrl',
                        type: 'input',
                        templateOptions: {
                            label: 'infoUrl',
                            placeholder: 'infoUrl'
                        }
                    }, {
                        key: 'priceBase',
                        type: 'input',
                        model: vm.tablet.price,
                        templateOptions: {
                            label: 'priceBase'
                        }
                    }, {
                        key: 'priceDesc',
                        type: 'input',
                        model: vm.tablet.price,
                        templateOptions: {
                            label: 'priceDesc'
                        }
                    }, {
                        template: '<hr>'
                    }, {
                        key: 'desc1',
                        type: 'input',
                        model: vm.tablet.description,
                        templateOptions: {
                            label: '1 hlavna specka'
                        }
                    }, {
                        key: 'desc2',
                        type: 'input',
                        model: vm.tablet.description,
                        templateOptions: {
                            label: '2 hlavna specka'
                        }
                    }, {
                        key: 'desc3',
                        type: 'input',
                        model: vm.tablet.description,
                        templateOptions: {
                            label: '3 hlavna specka'
                        }
                    }, {
                        template: '<hr>'
                    }, {
                        key: "desc[0]",
                        model: vm.tablet.descLightBox[0],
                        type: 'input',
                        templateOptions: {
                            label: 'desc-Label'
                        }
                    }, {
                        key: "desc[1]",
                        model: vm.tablet.descLightBox[0],
                        type: 'input',
                        templateOptions: {
                            label: 'desc-Value'
                        }
                    }, {
                        template: '<hr>'
                    },
                    {
                        key: "desc[0]",
                        model: vm.tablet.descLightBox[1],
                        type: 'input',
                        templateOptions: {
                            label: 'desc-Label'
                        }
                    }, {
                        key: "desc[1]",
                        model: vm.tablet.descLightBox[1],
                        type: 'input',
                        templateOptions: {
                            label: 'desc-Value'
                        }
                    }, {
                        template: '<hr>'
                    }, {
                        key: "desc[0]",
                        model: vm.tablet.descLightBox[2],
                        type: 'input',
                        templateOptions: {
                            label: 'desc-Label'
                        }
                    }, {
                        key: "desc[1]",
                        model: vm.tablet.descLightBox[2],
                        type: 'input',
                        templateOptions: {
                            label: 'desc-Value'
                        }
                    }, {
                        template: '<hr>'
                    }, {
                        key: "desc[0]",
                        model: vm.tablet.descLightBox[3],
                        type: 'input',
                        templateOptions: {
                            label: 'desc-Label'
                        }
                    }, {
                        key: "desc[1]",
                        model: vm.tablet.descLightBox[3],
                        type: 'input',
                        templateOptions: {
                            label: 'desc-Value'
                        }
                    }, {
                        template: '<hr>'
                    }, {
                        key: "desc[0]",
                        model: vm.tablet.descLightBox[4],
                        type: 'input',
                        templateOptions: {
                            label: 'desc-Label'
                        }
                    }, {
                        key: "desc[1]",
                        model: vm.tablet.descLightBox[4],
                        type: 'input',
                        templateOptions: {
                            label: 'desc-Value'
                        }
                    }, {
                        template: '<hr>'
                    }, {
                        key: "desc[0]",
                        model: vm.tablet.descLightBox[5],
                        type: 'input',
                        templateOptions: {
                            label: 'desc-Label'
                        }
                    }, {
                        key: "desc[1]",
                        model: vm.tablet.descLightBox[5],
                        type: 'input',
                        templateOptions: {
                            label: 'desc-Value'
                        }
                    }, {
                        template: '<hr>'
                    }, {
                        key: "desc[0]",
                        model: vm.tablet.descLightBox[6],
                        type: 'input',
                        templateOptions: {
                            label: 'desc-Label'
                        }
                    }, {
                        key: "desc[1]",
                        model: vm.tablet.descLightBox[6],
                        type: 'input',
                        templateOptions: {
                            label: 'desc-Value'
                        }
                    }, {
                        template: '<hr>'
                    }, {
                        key: "desc[0]",
                        model: vm.tablet.descLightBox[7],
                        type: 'input',
                        templateOptions: {
                            label: 'desc-Label'
                        }
                    }, {
                        key: "desc[1]",
                        model: vm.tablet.descLightBox[7],
                        type: 'input',
                        templateOptions: {
                            label: 'desc-Value'
                        }
                    }, {
                        template: '<hr>'
                    }, {
                        key: "desc[0]",
                        model: vm.tablet.descLightBox[8],
                        type: 'input',
                        templateOptions: {
                            label: 'desc-Label'
                        }
                    }, {
                        key: "desc[1]",
                        model: vm.tablet.descLightBox[8],
                        type: 'input',
                        templateOptions: {
                            label: 'desc-Value'
                        }
                    }, {
                        template: '<hr>'
                    }, {
                        key: "desc[0]",
                        model: vm.tablet.descLightBox[9],
                        type: 'input',
                        templateOptions: {
                            label: 'desc-Label'
                        }
                    }, {
                        key: "desc[1]",
                        model: vm.tablet.descLightBox[9],
                        type: 'input',
                        templateOptions: {
                            label: 'desc-Value'
                        }
                    }, {
                        template: '<hr>'
                    }, {
                        key: "desc[0]",
                        model: vm.tablet.descLightBox[10],
                        type: 'input',
                        templateOptions: {
                            label: 'desc-Label'
                        }
                    }, {
                        key: "desc[1]",
                        model: vm.tablet.descLightBox[10],
                        type: 'input',
                        templateOptions: {
                            label: 'desc-Value'
                        }
                    }, {
                        template: '<hr>'
                    }, {
                        key: "desc[0]",
                        model: vm.tablet.descLightBox[11],
                        type: 'input',
                        templateOptions: {
                            label: 'desc-Label'
                        }
                    }, {
                        key: "desc[1]",
                        model: vm.tablet.descLightBox[11],
                        type: 'input',
                        templateOptions: {
                            label: 'desc-Value'
                        }
                    }, {
                        template: '<hr>'
                    }, {
                        key: "desc[0]",
                        model: vm.tablet.descLightBox[12],
                        type: 'input',
                        templateOptions: {
                            label: 'desc-Label'
                        }
                    }, {
                        key: "desc[1]",
                        model: vm.tablet.descLightBox[12],
                        type: 'input',
                        templateOptions: {
                            label: 'desc-Value'
                        }
                    }, {
                        template: '<hr>'
                    }, {
                        key: "desc[0]",
                        model: vm.tablet.descLightBox[13],
                        type: 'input',
                        templateOptions: {
                            label: 'desc-Label'
                        }
                    }, {
                        key: "desc[1]",
                        model: vm.tablet.descLightBox[13],
                        type: 'input',
                        templateOptions: {
                            label: 'desc-Value'
                        }
                    }, {
                        template: '<hr>'
                    }, {
                        key: "desc[0]",
                        model: vm.tablet.descLightBox[14],
                        type: 'input',
                        templateOptions: {
                            label: 'desc-Label'
                        }
                    }, {
                        key: "desc[1]",
                        model: vm.tablet.descLightBox[14],
                        type: 'input',
                        templateOptions: {
                            label: 'desc-Value'
                        }
                    }, {
                        template: '<hr>'
                    }, {
                        key: "desc[0]",
                        model: vm.tablet.descLightBox[15],
                        type: 'input',
                        templateOptions: {
                            label: 'desc-Label'
                        }
                    }, {
                        key: "desc[1]",
                        model: vm.tablet.descLightBox[15],
                        type: 'input',
                        templateOptions: {
                            label: 'desc-Value'
                        }
                    }, {
                        template: '<hr>'
                    }, {
                        key: "desc[0]",
                        model: vm.tablet.descLightBox[16],
                        type: 'input',
                        templateOptions: {
                            label: 'desc-Label'
                        }
                    }, {
                        key: "desc[1]",
                        model: vm.tablet.descLightBox[16],
                        type: 'input',
                        templateOptions: {
                            label: 'desc-Value'
                        }
                    },
                ];
                //console.log(vm.tablet);
                // Tests the input based on a helpful regular expression
                // https://github.com/omarshammas/jquery.formance
                function validateDriversLicence(value) {
                    return /[A-Za-z]\d{4}[\s|\-]*\d{5}[\s|\-]*\d{5}$/.test(value);
                };

                vm.originalFields = angular.copy(vm.addTabletFields);

                //console.log(vm.originalFields);
                // function definition
                function saveItem(tablet) {
                    console.log(tablet);
                }

                /* -----------------------------------------
                    //https://jsbin.com/foqixe/edit?html,js,output

                vm.tablet = new Tablet();
                vm.tablets = Tablet.query();

                vm.save = function() {
                    vm.tablet.$save();
                    vm.tablets.push(vm.tablet);
                    vm.tablets = new Tablet();

                };

                templateOptions: {
                        label: 'name',
                        options: province.getProvinces()
                    }
*/




            }
        ]
    });


})();
