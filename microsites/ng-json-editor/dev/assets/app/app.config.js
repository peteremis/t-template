'use strict';

angular.
    module('jsonEditorApp').
    config(['$locationProvider',
        function config($locationProvider) {
            $locationProvider.hashPrefix('!');
        }
    ]);
 