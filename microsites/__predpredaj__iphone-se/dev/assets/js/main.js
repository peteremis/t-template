$(function () {
  /* HEAPBOX CALC - START */
  var heapVal = $("#heapbox_heap .holder").prop("rel"); //init value

  $(document).ready(function () {
    $("ul.heapOptions").find("li").eq(1).find("a").click(); //set default pausal to "ANO L"
  });

  $(".heapOption a").on("click", function () {
    heapVal = $("#heapbox_heap .holder").prop("rel"); //get new value
    function setPrices(oneTime128, oneTime64, monthly128, monthly64) {
      $("#oneTime64").text(oneTime64 + ",00 €");
      $("#oneTime128").text(oneTime128 + ",00 €");
      $("#monthly64").text(monthly64 + ",00 €");
      $("#monthly128").text(monthly128 + ",00 €");
    }

    switch (heapVal) {
      case "XXL":
        setPrices(99, 49, 1, 1);
        break;
      case "XL":
        setPrices(149, 99, 6, 6);
        break;
      case "L":
        setPrices(299, 249, 6, 6);
        break;
      case "M":
        setPrices(349, 299, 7, 7);
        break;
      default:
        break;
    }
  });
  /* HEAPBOX CALC - END */
});
