var pricelist = {
  is_orderable: 1,
  is_visible: 1,
  prices: {
    M: {
      oc: 209.0,
      oc_sale_web: 179.0,
      rc: 8,
      rc_sale_web: 8,
      rpln: {
        taxless_price: 9.08,
        tax_price: 10.9,
      },
    },
    L: {
      oc: 179.0,
      oc_sale_web: 149.0,
      rc: 8,
      rc_sale_web: 8,
      rpln: {
        taxless_price: 13.25,
        tax_price: 15.9,
      },
    },
    XL: {
      oc: 109.0,
      oc_sale_web: 79.0,
      rc: 6,
      rc_sale_web: 8,
      rpln: {
        taxless_price: 17.41,
        tax_price: 20.9,
      },
    },
  },
};
