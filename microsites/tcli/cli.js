#!/usr/bin/env node

var mkdirp = require('mkdirp');
var copydir = require('copy-dir');
var path = require('path');
var fs = require('fs');

var args = process.argv.slice(2).toString();

var newFolder = './' + args;
var templateFolder = path.resolve(__dirname, './template/');

if (fs.existsSync(newFolder)) {
    console.log('Project with the name ' + args + ' already exists.');
} else {

    mkdirp(newFolder, function (err) {

        if (err) console.error(err)

    });

    copydir.sync(templateFolder, newFolder);

}
