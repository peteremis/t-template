var gulp = require('gulp'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    uglify = require('gulp-uglify'),
    browserSync = require('browser-sync').create(),
    reload = browserSync.reload,
    includer = require('gulp-html-ssi'),
    runSequence = require('run-sequence'),
    es = require('event-stream'),
    replace = require('gulp-replace'),
    config = require('./config.json'),
    globalTemplates = '../../global-assets/templates/*.html',
    localTemplates = 'dev/templates/*.html',
    jasmineBrowser = require('gulp-jasmine-browser'),
    watch = require('gulp-watch'),
    jasmine = require('gulp-jasmine-livereload-task');
var print = require('gulp-print');
var gutil = require('gulp-util');
var rename = require('gulp-rename');
var cleanCSS = require('gulp-clean-css');
//var git = require('gulp-git');
var argv = require('yargs').argv;
var runSequence = require('run-sequence');
var prompt = require('gulp-prompt');
var embedTemplates = require('gulp-angular-embed-templates');
var copycat = require('gulp-copycat');
var concat = require('gulp-concat');
var del = require('del');
var htmlreplace = require('gulp-html-replace');
var gulpIgnore = require('gulp-ignore');
var myVar = 'update';
var htmlmin = require('gulp-htmlmin');
var jsonminify = require('gulp-jsonminify');
var pump = require('pump');

var ignoreFiles = '*.spec.js';

//Base tasks html + css + js
gulp.task('htmlSSI', function() {
    es.merge(gulp.src(globalTemplates), gulp.src(localTemplates))
        .pipe(includer())
        .pipe(gulp.dest('dev/server/'));
});

gulp.task('sass', function() {
    return gulp.src('dev/assets/sass/style.scss')
        .pipe(sass({ outputStyle: 'compact' }))
        .pipe(sourcemaps.init())
        .pipe(sass.sync().on('error', sass.logError))
        .pipe(sourcemaps.write('/maps'))
        .pipe(gulp.dest('dev/server/assets/css'))
        .pipe(browserSync.stream());
});

gulp.task('sass-deploy', function() {
    return gulp.src('dev/assets/sass/content.scss')
        .pipe(sass({ outputStyle: 'compact' }))
        .pipe(sass.sync().on('error', sass.logError))
        .pipe(gulp.dest('dev/server/assets/css'))
        .pipe(browserSync.stream());
});

gulp.task('js-build', function () {
    gulp.src(['tmp/app.module.js', 'tmp/app.min.js', 'tmp/app-with-template.min.js'])
        .pipe(gulpIgnore.exclude(ignoreFiles))
        .pipe(concat('app.min.js'))
        .pipe(gulp.dest('deploy'));
});

gulp.task('compress', function (cb) {
    pump([
          gulp.src('deploy/app.min.js'),
          uglify(),
          gulp.dest('deploy/min')
      ],
      cb
    );
});

gulp.task('js-lib-build', function () {
    gulp.src(['dev/assets/lib/js/angular-plugins/*.js'])
        .pipe(gulpIgnore.exclude(ignoreFiles))
        .pipe(concat('lib.min.js'))
        .pipe(gulp.dest('deploy'));
});

gulp.task('js-build-temp', function () {
    gulp.src(['tmp/app.module.js', 'tmp/js/*.js'])
        .pipe(gulpIgnore.exclude(ignoreFiles))
        .pipe(concat('app.min.js'))
        .pipe(gulp.dest('tmp'));
});

gulp.task('js-build-with-template-temp', function () {
    gulp.src(['tmp/templates/*.js'])
        .pipe(gulpIgnore.exclude(ignoreFiles))
        .pipe(embedTemplates())
        .pipe(concat('app-with-template.min.js'))
        .pipe(gulp.dest('tmp'));
});

gulp.task('replace-app', function() {
    gulp.src('deploy/_main-content.html')
    .pipe(htmlreplace({
        js: [config.codeFolder + 'angular.min.js',config.codeFolder + 'lib.min.js', config.codeFolder + 'app.min.js']
    }))
    .pipe(gulp.dest('deploy'))
});

gulp.task('js', ['js-lib'], function() {
    return gulp.src(['dev/assets/js/**/*.js', 'dev/assets/js/**/*.html'])
        .on('error', console.error.bind(console))
        .pipe(gulp.dest('dev/server/assets/js'));
});

gulp.task('js-lib', function() {
    return gulp.src('dev/assets/lib/**/*.js')
        .on('error', console.error.bind(console))
        .pipe(gulp.dest('dev/server/assets/lib/'));
});

//Browser-sync & watcher
//Static server

gulp.task('browser-sync', ['sass', 'js', 'htmlSSI'], function() {
    browserSync.init({
        server: {
            baseDir: ["./dev", "./dev/server", "./../../global-assets"]
        }
    });
});

//Novy zaklad: gulp watch
gulp.task('watch', ['browser-sync'], function() {
    gulp.watch('dev/assets/sass/*.scss', ['sass']);
    gulp.watch('../../global-assets/sass/*.scss', ['sass']);
    gulp.watch('dev/assets/js/**/*.js', ['js']);
    gulp.watch('dev/assets/js/**/*.js').on('change', reload);
    gulp.watch('dev/templates/*.html', ['htmlSSI']);
    gulp.watch('dev/server/*.html').on('change', reload);
    gulp.src('dev/assets/img/**')
        .pipe(gulp.dest('dev/server/assets/img'));
});

//deploy to dist convert and back
//convert deploy 2 dev html
gulp.task('conv', function() {
    gulp.src(['convert/_input.html'])
        .pipe(replace(config.imgFolder, '../assets/img/'))
        .pipe(replace(config.codeFolder, '../assets/js/'))
        .pipe(gulp.dest('convert/dev/'));
});
//convert dev 2 deploy html
gulp.task('conv-back', function() {
    gulp.src(['convert/dev-src/_input.html'])
        .pipe(replace('../assets/img/', config.imgFolder))
        .pipe(replace('../assets/js/', config.codeFolder))
        .pipe(gulp.dest('convert/deploy/'));
});

//Deploy helpers

gulp.task('dist-replace-path-popup-templates', function() {
    return gulp.src(['dev/assets/js/components/popup/templates/*.html'])
        .pipe(replace('../assets/img/', config.imgFolder))
        .pipe(htmlmin({collapseWhitespace: true}))
        .pipe(gulp.dest('deploy/popup_templates'));
});

gulp.task('dist-replace-path-html', function() {
    return gulp.src(['dev/templates/_main-content.html'])
        .pipe(replace('../assets/img/', config.imgFolder))
        .pipe(replace('../assets/js/components/calc-entrance/', config.componentsFolder))
        .pipe(replace('../assets/js/components/phone-num/', config.componentsFolder))
        .pipe(replace('../assets/js/components/popup/', config.componentsFolder))
        .pipe(replace('../assets/js/components/service-crossroads/', config.componentsFolder))
        .pipe(replace('../assets/js/components/service-chooser/', config.componentsFolder))
        .pipe(replace('../assets/js/components/step-controls/', config.componentsFolder))
        .pipe(replace('../assets/js/components/step-indicator/', config.componentsFolder))
        .pipe(replace('../assets/js/config/', config.configFolder))
        .pipe(replace('../assets/js/pages/calc-crossroads/', config.pagesFolder))
        .pipe(replace('../assets/js/pages/calc-chooser/', config.pagesFolder))
        .pipe(replace('../assets/js/pages/calc-intro/', config.pagesFolder))
        .pipe(replace('../assets/js/pages/landing/', config.pagesFolder))
        .pipe(replace('../assets/js/pages/summary/', config.pagesFolder))
        .pipe(replace('../assets/js/pages/interested/', config.pagesFolder))
        .pipe(replace('../assets/js/data/', config.dataFolder))
        .pipe(replace('../assets/js/services/', config.servicesFolder))
        .pipe(replace('../assets/js/', config.codeFolder))
        .pipe(gulp.dest('deploy'));
});

gulp.task('dist-replace-path-css', function() {
    gulp.src(['dev/server/assets/css/*.css'])
        .pipe(replace('../img/', config.imgFolder))
        .pipe(gulp.dest('deploy'));
});

gulp.task('dist-replace-path-css-deploy', function() {
    gulp.src(['dev/server/assets/css/content.css'])
        .pipe(replace('../img/', config.imgFolder))
        .pipe(replace('/assets/img/', config.imgFolder))
        .pipe(gulp.dest('deploy'));
});

gulp.task('dist-replace-path-js', function() {
    gulp.src(['dev/server/assets/js/*.js'])
        .pipe(replace('../assets/img/', config.imgFolder))
        .pipe(replace('../assets/js/data/', config.jsonFolder))
        .pipe(gulp.dest('tmp'));
    gulp.src(['dev/server/assets/js/config/*.js'])
        .pipe(replace('../assets/img/', config.imgFolder))
        .pipe(gulp.dest('tmp/js'));
    gulp.src(['dev/server/assets/js/data/*.js'])
        .pipe(replace('../assets/img/', config.imgFolder))
        .pipe(gulp.dest('tmp/js'));
        gulp.src(['dev/server/assets/js/services/*.js'])
        .pipe(replace('../assets/img/', config.imgFolder))
        .pipe(gulp.dest('tmp/js'));
    gulp.src(['dev/server/assets/js/components/calc-entrance/*.js'])
        .pipe(replace('../assets/img/', config.imgFolder))
        .pipe(replace('../assets/js/components/calc-entrance/', ''))
        .pipe(gulp.dest('tmp/templates'));
    gulp.src(['dev/server/assets/js/components/phone-num/*.js'])
        .pipe(replace('../assets/img/', config.imgFolder))
        .pipe(replace('../assets/js/components/phone-num/', ''))
        .pipe(gulp.dest('tmp/templates'));
    gulp.src(['dev/server/assets/js/components/popup/*.js'])
        .pipe(replace('../assets/img/', config.imgFolder))
        .pipe(replace('../assets/js/components/popup/templates/', config.popupTemplatesFolder))
        .pipe(replace('../assets/js/components/popup/', ''))
        .pipe(gulp.dest('tmp/templates'));
    gulp.src(['dev/server/assets/js/components/service-crossroads/*.js'])
        .pipe(replace('../assets/img/', config.imgFolder))
        .pipe(replace('../assets/js/components/service-crossroads/', ''))
        .pipe(gulp.dest('tmp/templates'));
    gulp.src(['dev/server/assets/js/components/service-chooser/*.js'])
        .pipe(replace('../assets/img/', config.imgFolder))
        .pipe(replace('../assets/js/components/service-chooser/', ''))
        .pipe(gulp.dest('tmp/templates'));
    gulp.src(['dev/server/assets/js/components/step-controls/*.js'])
        .pipe(replace('../assets/img/', config.imgFolder))
        .pipe(replace('../assets/js/components/step-controls/', ''))
        .pipe(gulp.dest('tmp/templates'));
    gulp.src(['dev/server/assets/js/components/step-indicator/*.js'])
        .pipe(replace('../assets/img/', config.imgFolder))
        .pipe(replace('../assets/js/components/step-indicator/', ''))
        .pipe(gulp.dest('tmp/templates'));
    gulp.src(['dev/server/assets/js/pages/calc-crossroads/*.js'])
        .pipe(replace('../assets/img/', config.imgFolder))
        .pipe(replace('../assets/js/pages/calc-crossroads/', ''))
        .pipe(gulp.dest('tmp/templates'));
    gulp.src(['dev/server/assets/js/pages/calc-chooser/*.js'])
        .pipe(replace('../assets/img/', config.imgFolder))
        .pipe(replace('../assets/js/pages/calc-chooser/', ''))
        .pipe(gulp.dest('tmp/templates'));
    gulp.src(['dev/server/assets/js/pages/calc-intro/*.js'])
        .pipe(replace('../assets/img/', config.imgFolder))
        .pipe(replace('../assets/js/pages/calc-intro/', ''))
        .pipe(gulp.dest('tmp/templates'));
    gulp.src(['dev/server/assets/js/pages/landing/*.js'])
        .pipe(replace('../assets/img/', config.imgFolder))
        .pipe(replace('../assets/js/pages/landing/', ''))
        .pipe(gulp.dest('tmp/templates'));
    gulp.src(['dev/server/assets/js/pages/summary/*.js'])
        .pipe(replace('../assets/img/', config.imgFolder))
        .pipe(replace('../assets/js/pages/summary/', ''))
        .pipe(gulp.dest('tmp/templates'));
    gulp.src(['dev/server/assets/js/pages/interested/*.js'])
        .pipe(replace('../assets/img/', config.imgFolder))
        .pipe(replace('../assets/js/pages/interested/', ''))
        .pipe(gulp.dest('tmp/templates'));
    gulp.src(['dev/assets/js/data/data.json'])
        .pipe(replace('../assets/img/', config.imgFolder))
        .pipe(jsonminify())
        .pipe(gulp.dest('deploy'));        
});

gulp.task('dist-replace-path-html-controller', function() {
    gulp.src(['dev/server/assets/js/components/calc-entrance/*.html'])
        .pipe(replace('../assets/img/', config.imgFolder))
        .pipe(gulp.dest('tmp/templates'));
    gulp.src(['dev/server/assets/js/components/phone-num/*.html'])
        .pipe(replace('../assets/img/', config.imgFolder))
        .pipe(gulp.dest('tmp/templates'));
    gulp.src(['dev/server/assets/js/components/popup/*.html'])
        .pipe(replace('../assets/img/', config.imgFolder))
        .pipe(gulp.dest('tmp/templates'));
    gulp.src(['dev/server/assets/js/components/service-crossroads/*.html'])
        .pipe(replace('../assets/img/', config.imgFolder))
        .pipe(gulp.dest('tmp/templates'));
    gulp.src(['dev/server/assets/js/components/service-chooser/*.html'])
        .pipe(replace('../assets/img/', config.imgFolder))
        .pipe(gulp.dest('tmp/templates'));
    gulp.src(['dev/server/assets/js/components/step-controls/*.html'])
        .pipe(replace('../assets/img/', config.imgFolder))
        .pipe(gulp.dest('tmp/templates'));
    gulp.src(['dev/server/assets/js/components/step-indicator/*.html'])
        .pipe(replace('../assets/img/', config.imgFolder))
        .pipe(gulp.dest('tmp/templates'));
    gulp.src(['dev/server/assets/js/pages/calc-crossroads/*.html'])
        .pipe(replace('../assets/img/', config.imgFolder))
        .pipe(gulp.dest('tmp/templates'));
    gulp.src(['dev/server/assets/js/pages/calc-chooser/*.html'])
        .pipe(replace('../assets/img/', config.imgFolder))
        .pipe(gulp.dest('tmp/templates'));
    gulp.src(['dev/server/assets/js/pages/calc-intro/*.html'])
        .pipe(replace('../assets/img/', config.imgFolder))
        .pipe(gulp.dest('tmp/templates'));
    gulp.src(['dev/server/assets/js/pages/landing/*.html'])
        .pipe(replace('../assets/img/', config.imgFolder))
        .pipe(gulp.dest('tmp/templates'));
    gulp.src(['dev/server/assets/js/pages/summary/*.html'])
        .pipe(replace('../assets/img/', config.imgFolder))
        .pipe(gulp.dest('tmp/templates'));
    gulp.src(['dev/server/assets/js/pages/interested/*.html'])
        .pipe(replace('../assets/img/', config.imgFolder))
        .pipe(gulp.dest('tmp/templates'));
});

gulp.task('prepare-deploy', ['sass', 'htmlSSI'], function() {
    return gulp.src('dev/templates/_main-content.html')
    .pipe(gulp.dest('deploy'));
});

gulp.task('dist-replace', ['dist-replace-path-html', 'dist-replace-path-css']);
gulp.task('dist-replace-deploy', ['dist-replace-path-html', 'dist-replace-path-css-deploy']);

//Print CSS path

gulp.task('print', function() {
    gulp.src('dev/server/assets/css/style.css')
        .pipe(print(function(filepath) {
            return "\n@import url(" + config.globalCSS + "global-core.css);\n@import url(" + config.codeFolder + "content.css?v=1);";
        }))
});

//minify Css
gulp.task('minify:css', function() {
    return gulp.src('deploy/content.css')
        .pipe(cleanCSS())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest('deploy'));
});

//Deploy

gulp.task('deploy', function(callback) {
    runSequence(
        'del-deploy',
        'sass-deploy',
        'prepare-deploy',
        'dist-replace-path-js',
        'js-build',
        'js-build-with-template',
        'dist-replace-deploy',
        'replace-app',
        'print',
        callback);
});

gulp.task('deploy-test', function(callback) {
    runSequence(
        'del-deploy',
        'sass-deploy',
        'prepare-deploy',
        'dist-replace-path-js',
        'dist-replace-path-html-controller',
        'js-build-temp',
        'js-build-with-template-temp',
        'js-build',
        'js-lib-build',
        'dist-replace-deploy',
        'dist-replace-path-popup-templates',
        'replace-app',
        'print',
        callback);
});

gulp.task('deploy-1', function(callback) {
    runSequence(
        'del-deploy',
        'sass-deploy',
        'prepare-deploy',
        'dist-replace-path-js',
        'dist-replace-path-html-controller',
        callback);
});

gulp.task('deploy-2', function(callback) {
    runSequence(
        'js-build-temp',
        'js-build-with-template-temp',
        callback);
});

gulp.task('deploy-3', function(callback) {
    runSequence(
        'js-build',
        'js-lib-build',
        'dist-replace-deploy',
        'dist-replace-path-popup-templates',
        'replace-app',
        'print',
        callback);
});

gulp.task('del-deploy', function() {
    del(['deploy/**']);
    del(['tmp/**']);
});


//Serve

gulp.task('serve', function() {

    browserSync.init({
        server: {
            baseDir: ["./dev", "./dev/server", "./../../global-assets"]
        }
    });

    gulp.watch('dev/assets/sass/*.scss', ['sass']);
    gulp.watch('../../global-assets/sass/*.scss', ['sass']);
    gulp.watch('dev/assets/js/**/*.js', ['js']);
    gulp.watch('dev/assets/js/**/*.js').on('change', reload);
    gulp.watch('dev/assets/js/**/*.html', ['js']);
    gulp.watch('dev/assets/js/**/*.html').on('change', reload);
    gulp.watch('dev/templates/*.html', ['htmlSSI']);
    gulp.watch('dev/server/*.html').on('change', reload);
    gulp.src('dev/assets/img/**')
        .pipe(gulp.dest('dev/server/assets/img'));
});

// DEFAUL Task

gulp.task('default', function(callback) {
    runSequence(['sass', 'js', 'htmlSSI'],
        'serve',
        callback);
});


//Git tasky

gulp.task('otazka', function() {
    // just source anything here - we just wan't to call the prompt for now
    return gulp.src('config.json')
        .pipe(prompt.prompt({
            type: 'input',
            name: 'commit',
            message: 'Please enter commit message...'
        }, function(res) {
            myVar = res.commit;
    }));
});

gulp.task('add', function() {
    console.log('adding...');
    return gulp.src('./*')
        .pipe(git.add());
});

gulp.task('commit', function() {
    return gulp.src('./*')
        .pipe(git.commit([config.pageUrl, myVar]));
});

gulp.task('push', function() {
    console.log('pushing...');
    git.push('origin', 'master', function(err) {
        if (err) throw err;
    });
});

gulp.task('git', function(callback) {
    runSequence('otazka', 'add', 'commit', 'push', callback);
});

//  Jasmine tests


var testOptions = {
 tags: {
    source: [
    {
        begin: /<!--\s*ccs:test\s*-->/gi, // beginning of source tag: <!-- ccs:name -->
        end: /<!--\s*\/ccs:test\s*-->/gi	 // end of source tag: <!-- /ccs:name -->
    }
    ],
    dest: [
    {
        begin: /<!--\s*ccd:test\s*-->/gi, // beginning of destination tag: <!-- ccd:name -->
        end: /<!--\s*\/ccd:test\s*-->/gi	 // end of destination tag: <!-- /ccd:name -->
    }
    ],
 }
};


gulp.task('copySourcesToTest', function() {
    return gulp.src(['dev/templates/_main-content.html', 'dev/test/test.html'])
            .pipe(copycat())
            .pipe(concat('index.html'))
            .pipe(gulp.dest('dev/test/'))
});

gulp.task('test-server', function() {
    browserSync.init({
        server: {
            baseDir: ["./dev/test", "./dev/"]
        }
    });

    var filesForTest = ['dev/server/assets/js/*.js', 'dev/server/assets/js/*_spec.js'];
    gulp.src(filesForTest)
        .pipe(jasmineBrowser.specRunner());
});

gulp.task('watch-test', function() {
    gulp.watch('dev/assets/js/**/*.js').on('change', reload);
    gulp.watch('dev/test/spec/**/*.js').on('change', reload);
});

gulp.task('test', function() {
    runSequence('copySourcesToTest', 'test-server', 'watch-test');
});
