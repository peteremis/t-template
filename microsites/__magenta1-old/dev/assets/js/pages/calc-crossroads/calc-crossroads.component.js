(function() {
    'use strict';

    angular
        .module('mag-1')
        .component('calcCrossroads', {
            templateUrl: '../assets/js/pages/calc-crossroads/calc-crossroads.component.html',
            controller: calcCrossroadsController,
            controllerAs: '$ctrl'
        });

    calcCrossroadsController.$inject = ['appConfig'];
    function calcCrossroadsController(appConfig) {
        var $ctrl = this;

        ////////////////

        $ctrl.$onInit = function() {
            appConfig.setPageType('crossroads');
            $('#fixed-link .fixed-link__table').hide();
            $('#IMS_box1').hide();
        };
        $ctrl.$onChanges = function() { };
        $ctrl.$onDestroy = function() { };
    }
})();