(function() {
  "use strict";

  angular.module("mag-1").component("landing", {
    templateUrl: "../assets/js/pages/landing/landing.component.html",
    controller: LandingController,
    controllerAs: "$ctrl"
  });

  LandingController.$inject = [
    "$location",
    "APP_CONST",
    "appConfig",
    "$timeout",
    "$anchorScroll",
    "dataChooser"
  ];
  function LandingController(
    $location,
    APP_CONST,
    appConfig,
    $timeout,
    $anchorScroll,
    dataChooser
  ) {
    var $ctrl = this;

    ////////////////

    $ctrl.$onInit = function() {
      $ctrl.dataAno = dataChooser.getDataPlan("áno");

      appConfig.setPageType("landing");
      appConfig.setCalcInputVisibility(1);
      initJS();
    };

    $ctrl.reloadActionMobile = function(el) {
      var url = angular.element(el.querySelector(".detail-page")).attr("href");
      window.location.href = url;
    };

    function initJS() {
      /* scroll to */

      var urlHash = window.location.href;
      var lastChar = urlHash.lastIndexOf("?");

      $timeout(function() {
        if (lastChar > 0) {
          urlHash = "#" + urlHash.substr(lastChar + 1);
          if (urlHash.length < 30) {
            var element = angular.element(urlHash);
            angular
              .element("html,body")
              .animate({ scrollTop: element.offset().top - 60 }, "slow");
          }
        }
        angular.forEach(angular.element(".circle"), function(value, key) {
          var comp = angular.element(value);
          comp.qtip({
            content: comp.attr("alt"),
            style: {
              classes: "qtip-tipsy"
            },
            position: {
              corner: {
                target: "leftMiddle",
                tooltip: "topMiddle"
              }
            }
          });
        });
      }, 200);

      $(".scroll-to").click(function() {
        //if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname)
        {
          var id_comp = $(this).attr("data-id");
          var target = $("#" + id_comp); //vrati #sec- po kliknuti na NAV item
          target = target.length
            ? target
            : $("[name=" + this.hash.slice(1) + "]");
          if (target.length) {
            $("html,body").animate(
              {
                scrollTop: target.offset().top - $("#nav-sticky").height() //zoscrolluj na sekciu a odrataj vysku NAV
              },
              1000
            );
            return false;
          }
        }
      });

      /* TOGGLE ARROW */
      $(".toggle-arrow, .arrow-right").click(function(event) {
        event.preventDefault();
        if ($(this).hasClass("arrow-right")) {
          return;
        }
        $(event.target)
          .closest(".togleArrow")
          .find(".arrow-content")
          .toggle(200);
        $(this)
          .find(".arrow-right")
          .toggleClass("arrow-rotate");
      });

      // media query event handler
      if (matchMedia) {
        var mediaQueryMobil = window.matchMedia("(max-width: 500px)");
        mediaQueryMobil.addListener(WidthChange);
        WidthChange(mediaQueryMobil);
      }

      // media query change
      function WidthChange(mediaQueryMobil) {
        if (mediaQueryMobil.matches) {
          // window width is less than 500px
          $(".arrow-content").hide();
          $(".toggle-arrow")
            .find(".arrow-right")
            .removeClass("arrow-rotate");
        } else {
          // window width is more than 500px
          $(".open .arrow-content").show();
          $(".open .toggle-arrow")
            .find(".arrow-right")
            .addClass("arrow-rotate");
        }
      }
      /* TOGGLE ARROW END */

      /* Landing Page POPUP */
      /*
           $('.popup').fancybox({
             padding: 0,
             margin: 0,
             width: 300,
             maxWidth: 300,
             closeBtn: false,
             parent: "#content",
             height: 'auto',
             autoSize: false,
             helpers: {
               overlay: {
                 css: {
                   'background': 'rgba(0, 0, 0, 0.7)'
                 }
               }
             }
           });
           $('.p-close, .link-close, .text-close, .close-btn').on('click', function(e) {
             e.preventDefault();
             $.fancybox.close();
           });
           */
      /* Landing Page POPUP END */

      /* STICKY NAV START */
      $(window).scroll(function() {
        var $this = $(this),
          $NavSticky = $(".nav-sticky");
        if ($this.scrollTop() > 482) {
          $NavSticky.css({ position: "fixed", top: "0" });
        } else {
          $NavSticky.css({ position: "relative" });
        }
      });
      $("#fixed-link .fixed-link__table").show();
      $("#IMS_box1").show();
      $("#cmb__block").appendTo("#cmb__holder");
    }

    $("#giveaway").heapbox({
      onChange: function(val, elm) {
        var giveAwayTextHolder = $(".giveawayText span");
        switch (val) {
          case "S":
            giveAwayTextHolder.text("250 MB");
            break;
          case "M":
            giveAwayTextHolder.text("500 MB");
            break;
          case "M_DATA":
            giveAwayTextHolder.text("4 GB");
            break;
          case "L":
            giveAwayTextHolder.text("5 GB");
            break;
          case "XL":
            giveAwayTextHolder.text("10 GB");
            break;
          case "XXL":
            giveAwayTextHolder.text("15 GB");
            break;
          default:
            giveAwayTextHolder.text("250 MB");
            break;
        }
      }
    });

    $ctrl.$onChanges = function() {};
    $ctrl.$onDestroy = function() {};
  }
})();
