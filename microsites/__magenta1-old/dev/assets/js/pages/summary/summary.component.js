(function() {
    'use strict';

    angular
        .module('mag-1')
        .component('summary', {
            templateUrl: '../assets/js/pages/summary/summary.component.html',
            controller: summaryController,
            controllerAs: '$ctrl'
        });

    summaryController.$inject = ['$rootScope','$location','appConfig','user','APP_CONST','calculate', '$http', '$routeParams','$timeout', '$window'];
    function summaryController($rootScope,$location,appConfig,user,APP_CONST,calculate, $http, $routeParams,$timeout, $window) {
        var $ctrl = this;
        $ctrl.msid = user.getUserMsid();
        $ctrl.discountAll = null;
        var token = null;

        ////////////////

        $ctrl.reload = function(action) {
            switch (action) {
                case 1:
                    appConfig.setCurrStep(5);
                    $location.path(APP_CONST.URL.CALC_CROSSROADS+ '/' + appConfig.getStepName(5));                
                    break;
                case 2:
                    if (appConfig.getSummaryUrl() == null) {
                        var userSave =  angular.copy(user.getUser());
                        userSave.msid = '';
                        $http({
                            method : "POST",
                            url : APP_CONST.URL.API_LINK,
                            data: userSave
                        })
                        .then(function mySuccess(response) {
                            token = response.data.token;
                            //var url = $location.absUrl().split('#')[0] + '#' + APP_CONST.URL.SUMMARY + '/token=' + token;
                            var url = appConfig.getCalculateUrl(token);
                            appConfig.setSummaryUrl(url);
                            $location.path(APP_CONST.URL.INTERESTED);
                        }, function myError(response) {
        
                        });
                    } else {
                        $location.path(APP_CONST.URL.INTERESTED);
                    }
                    
                    break;                
            }
        }        

        $ctrl.$onInit = function() {
            /*
            if ($location.absUrl().indexOf('p_auth=') !== -1) {
                $window.location.href = '/#' + APP_CONST.URL.SUMMARY;
            }
            */
            
            appConfig.setPageType('summary');
            $timeout(function(){
                angular.element('html,body').scrollTop(0);    
            }, 50);
            appConfig.setSummaryUrl(null);

            if (typeof $routeParams.param != 'undefined') {
                $http({
                    method : "GET",
                    url : APP_CONST.URL.API_LINK + '?' + $routeParams.param
                })
                .then(function mySuccess(response) {
                    token = $routeParams.param;
                    //var url = $location.absUrl().split('#')[0] + '#' + APP_CONST.URL.SUMMARY + '/token=' + token;
                    var url = appConfig.getCalculateUrl(token);
                   
                    appConfig.setSummaryUrl(url);
                    user.setUser(response.data);
                    pageInit();
                }, function myError(response) {

                });               
            } else {
                pageInit();
            }
            $('#fixed-link .fixed-link__table').show();
            $('#IMS_box1').show();
        };

        function pageInit() {
            $('#summary-sec-1 .toggle-arrow, #summary-sec-1 .arrow-right').click(function(event) {
                event.preventDefault();
                if ($(this).hasClass('arrow-right')) {
                  return;
                }
                $(event.target).closest('.togleArrow').find('.arrow-content').toggle(200);
                
                $(this).find('.arrow-right').toggleClass('arrow-rotate');
              });

            for (var i=1; i <= 5; i++) {
                if (user.getUserToStep(i).selected == 0) {
                    appConfig.setCurrStep(i);
                    $location.url(APP_CONST.URL.CALC_CROSSROADS + '/' + appConfig.getStepName(i));
                }
            }
            addQTip('#qt_pojdeto', 'Technickú pomoc Pôjdeto zadarmo.<br>S balíkom služieb Pôjdeto Light získate neobmedzenú technickú pomoc pre všetkých členov skupiny.');
            addQTip('#qt_magentaCloud', 'Úložisko vo veľkosti 20 GB zadarmo.<br>Uložte si bezpečne svoje dokumenty, fotky či videá s Magenta Cloud až do 20 GB zadarmo.');
            addQTip('#qt_freeTv', 'V prípade výpadku Magio televízie nám zavolajte na bezplatnú non-stop linku 0800 123 777 a ak sa nám nepodarí poruchu okamžite odstrániť, na 48 hodín vám zadarmo poskytneme mobilnú televíziu Magio Go spolu s neobmedzenými mobilnými dátami.');
            addQTip('#qt_freeInternet', 'V prípade výpadku Magio internetu nám zavolajte na bezplatnú non-stop linku 0800 123 777 a ak sa nám nepodarí poruchu okamžite odstrániť, na 48 hodín vám zadarmo poskytneme neobmedzený mobilný internet.');            
            
            $ctrl.discountAll = calculate.getAllDiscounts();
            $ctrl.discountAll.user.anoDataText = getDataText($ctrl.discountAll.user.anoData);
            $ctrl.discountAll.anoDataText = getDataText($ctrl.discountAll.anoData);
            
            var userPlan = user.getUser().plan;
            var memberMin = $ctrl.discountAll.members.min;
            if ((userPlan.selected == 1 && (userPlan.has == 1 || userPlan.has == 8)) || (userPlan.selected == 2 && (userPlan.interested == 1 || userPlan.interested == 8))) {
                memberMin += 100;
                $ctrl.discountAll.user.anoDataText += ' + 100 minút';
            }

            if (memberMin > 0) {
                $ctrl.discountAll.anoDataText += ' + ' + memberMin + ' minút';
            }
            
            $ctrl.hasAno = user.hasAno();
            $ctrl.hint = getHint();
        }

        function getDataText(val) {
            return val < 1 ? (val*1000) + ' MB' : val + ' GB';
        }

        function addQTip(id, text) {
            $(id).qtip({
                content: text,
                style: {
                    classes: 'qtip-tipsy'
                },
                position: {
                    corner: {
                        target: 'leftMiddle',
                        tooltip: 'topMiddle'
                    }
                }
            });
        }

        function getHint() {
            var result = null;
            var level = $ctrl.discountAll.level;
            var landServiceCount = $ctrl.discountAll.countLandServices;
            if (!user.hasAno) {
                if (level < 5)
                {
                    var userData = user.getUser();

                    var landService = '';
                    var userData = user.getUser();
                    if (landServiceCount < 2) {
                        if (userData.internet.selected == 4 && userData.tv.selected != 4 && userData.landLine.selected != 4)
                            landService = 'internet';
                        else if (userData.internet.selected != 4 && userData.tv.selected == 4 && userData.landLine.selected != 4)
                            landService = 'TV';
                        else if (userData.internet.selected != 4 && userData.tv.selected != 4 && userData.landLine.selected == 4)
                            landService = 'pevnú linku';
                        else if (userData.internet.selected == 4 && userData.tv.selected == 4 && userData.landLine.selected != 4)
                            landService = 'internet alebo TV';
                        else if (userData.internet.selected == 4 && userData.tv.selected != 4 && userData.landLine.selected == 4)
                            landService = 'internet alebo pevnú linku';
                        else if (userData.internet.selected != 4 && userData.tv.selected == 4 && userData.landLine.selected == 4)
                            landService = 'TV alebo pevnú linku';
                        else if (userData.internet.selected == 4 && userData.tv.selected == 4 && userData.landLine.selected == 4)
                            landService = 'internet, TV alebo pevnú linku';
                    }

                    result = '';
                    var memberCount = $ctrl.discountAll.members.plan.length;
                    if (memberCount < 4) {
                        memberCount = (4 - memberCount);
                        
                        if ((5 - level) < memberCount)
                            memberCount = 5 - level;

                        var memberText = '';
                        if (memberCount == 1)
                            memberText = 'člena';
                        else if (memberCount < 3)
                            memberText = 'členov';                        

                        result += memberCount + ' ' + memberText;
                    }
                    
                    if (landServiceCount < 2) {
                        if (result.length > 0)
                            result += ' a ';
                        result += landService;
                    }

                    result = 'Pre nižšie mesačné poplatky pridajte ' + result;

                }
            } else {
                if (landServiceCount < 1) {
                    result = 'Pridajte 1 pevnú službu a získate 2x viac dát a volaní k vášmu paušálu ÁNO.';    
                }
            }
            return result;
        }

        $ctrl.showPopup = function(popupId, data) {
            
            if (popupId == 'summary_link') {
                var userSave =  angular.copy(user.getUser());
                userSave.msid = '';
                $http({
                    method : "POST",
                    url : APP_CONST.URL.API_LINK,
                    data: userSave
                })
                .then(function mySuccess(response) {
                    token = response.data.token;
                    //var url = $location.absUrl().split('#')[0] + '#' + APP_CONST.URL.SUMMARY + '/token=' + token;
                    var url = appConfig.getCalculateUrl(token);
                    data = { 'urlSave': url };
                    appConfig.setSummaryUrl(url);
                    appConfig.setPopup(popupId, data);
                    $rootScope.$broadcast('showPopup');                    
                }, function myError(response) {

                }); 
            }
            else {
                appConfig.setPopup(popupId, data);
                $rootScope.$broadcast('showPopup');
            }
        }        

        $ctrl.$onChanges = function() { };
        $ctrl.$onDestroy = function() { };
    }
   
})();