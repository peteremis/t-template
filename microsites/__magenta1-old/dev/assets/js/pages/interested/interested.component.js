(function() {
    'use strict';

    angular
        .module('mag-1')
        .component('interested', {
            templateUrl: '../assets/js/pages/interested/interested.component.html',
            controller: interestedController,
            controllerAs: '$ctrl'
        });

    interestedController.$inject = ['$location', 'APP_CONST', 'calculate', 'appConfig', 'user', 'localStorage','$timeout'];
    function interestedController($location, APP_CONST, calculate, appConfig, user, localStorage,$timeout) {
        var $ctrl = this;
        $ctrl.discount = null;
        var isLsb = false;

        ////////////////

        $ctrl.reload = function(action) {
            switch (action) {
                case 1:
                    $location.path(APP_CONST.URL.SUMMARY);
                    break;                
            }
        }

        $ctrl.$onInit = function() {
            //isLsb = true;
            
            $timeout(function(){
                angular.element('html,body').scrollTop(0);    
            }, 50);

            for (var i=1; i <= 5; i++) {
                if (user.getUserToStep(i).selected == 0) {
                    appConfig.setCurrStep(i);
                    $location.url(APP_CONST.URL.CALC_CROSSROADS + '/' + appConfig.getStepName(i));
                }
            }
            
            appConfig.setPageType('interested');
            $ctrl.discount = calculate.getDiscount(5);

            $ctrl.callMeText = isLsb ? 'Doplniť formulár' : 'Vyriešte to za mňa';
            $('#fixed-link .fixed-link__table').show();
            $('#IMS_box1').show(); 
        };

        $ctrl.showPopup = function(popupId, data) {
            appConfig.setPopup(popupId, data);
            $rootScope.$broadcast('showPopup');
        }
        
        $ctrl.sendForm = function(compId) {
            var inputVal = angular.element('#' + compId).val();
            
            localStorage.setData('m1c_form_url', $location.absUrl().split('#')[1]);
            $ctrl.showError = checkInputVal(inputVal);
            if ($ctrl.showError == null) {
                formChange('form.dynamicFormMainForm .formM1Calc-subject', 'CallMeBack_others:' + inputVal + ': M1 zaujem - ' + appConfig.getSummaryUrl());
                formChange('form.dynamicFormMainForm .formM1Calc-email', APP_CONST.MAIL.LEAD);
                formChange('form.dynamicFormMainForm .formM1Calc-text', 'Magenta1-kalkulacka');
                //$('#p_p_id_dynamicForm_WAR_eccadmin_ .formCompositionRenderDisplay').css('display','block');
                if (!isLsb) {
                    angular.element('.dynamicFormPages .finishFormButton').triggerHandler('click');
                }
            }                
        }

        function formChange(inputName, value) {
            $(inputName)
                .find('input').val(value).blur().show().end()
                .find('label').show().end()
                .closest('li').show().end();
        }
        
        function checkInputVal(inputVal) {
            var result = null;

            if (inputVal.length < 10) {
                result = 'Minimálny počet znakov je 10!';
            } else if (parseInt(inputVal) != inputVal) {
                result = 'Musíte zadať číslo';
            }
            
            return result;
        }

        $ctrl.$onChanges = function() { };
        $ctrl.$onDestroy = function() { };
    }
})();