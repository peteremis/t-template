(function() {
    'use strict';

    angular
        .module('mag-1')
        .component('calcIntro', {
            templateUrl: '../assets/js/pages/calc-intro/calc-intro.component.html',
            controller: calcIntroController,
            controllerAs: '$ctrl'
        });

    calcIntroController.$inject = ['$location', 'user', 'APP_CONST', 'appConfig', '$timeout', '$window'];
    function calcIntroController($location, user, APP_CONST, appConfig, $timeout, $window) {
        var $ctrl = this;

        ////////////////

        $ctrl.$onInit = function() {
            appConfig.setPageType('intro');
            
            $timeout(function(){
                angular.element('html,body').scrollTop(0);    
            }, 50);
            $('#fixed-link .fixed-link__table').show();
            $('#IMS_box1').show();
        };
        $ctrl.$onChanges = function() { };
        $ctrl.$onDestroy = function() { };
    }
})();