(function() {
    'use strict';

    angular
        .module('mag-1')
        .factory('appConfig', appConfig);

    appConfig.$inject = ['$rootScope', '$location', 'APP_CONST'];
    function appConfig($rootScope, $location, APP_CONST) {
        var service = {
            setCalcInputVisibility: setCalcInputVisibility,
            getCalcInputVisibility: getCalcInputVisibility,
            setCurrStep: setCurrStep,
            getCurrStep: getCurrStep,
            getCurrStepName: getCurrStepName,
            increaseStep: increaseStep,
            decreaseStep: decreaseStep,
            initAppConfig: initAppConfig,
            setPageType: setPageType,
            getPageType: getPageType,
            getPopup: getPopup,
            setPopup: setPopup,
            getShowHelp: getShowHelp,
            setShowHelp: setShowHelp,
            getStepName: getStepName,
            getStepFromName: getStepFromName,
            getSummaryUrl: getSummaryUrl,
            setSummaryUrl: setSummaryUrl,
            getCalculateUrl: getCalculateUrl
        };

        return service;

        // Zakladne nastavenia aplikacie
        var calcInputVisibility;

        // Aktualny krok vo vyperovom procese
        var currStep;

        //Min a max step
        var minStep;
        var maxStep;

        // Typ stranky: crossroads alebo chooser
        var pageType;
        var showHelp;
        var summaryUrl = null;

        var popup = {};

        ////////////////

        function getCalculateUrl(token) {
            var url = $location.absUrl().split('#')[0];
            var searchForm = url.indexOf('?');
            if (searchForm > 0) {
                url = url.substr(0, searchForm);
            }
            return url + '#' + APP_CONST.URL.SUMMARY + '/token=' + token;             
        }

        function initAppConfig() {
            // 0 = nezobraz ziadny input;
            // 1 = zobraz input pre mobil
            // 2 = zobraz input pre kod od sms
            calcInputVisibility = 1;
            currStep = 1;
            // Step moze byt v rozsahu 1-5
            minStep = 1;
            maxStep = 5;
            showHelp = false;
        }

        function setShowHelp(val) {
            showHelp = val;    
        }

        function getShowHelp() {
            return showHelp;    
        }        

        function setPopup(popupId, data) {
            popup = {
                'popupId': popupId,
                'data': data
            }
        }

        function getPopup(key) {
            return popup;
        }

        function setCalcInputVisibility(val) {
            calcInputVisibility = val;
        }

        function getCalcInputVisibility() {
            return calcInputVisibility;
        }

        function setCurrStep(val) {
            currStep = val;
        }

        function getCurrStep() {
            return currStep;
        }

        function getCurrStepName() {
            switch (currStep)
            {
                case 1:
                    return 'plan';
                    break;
                case 2:
                    return 'internet';
                    break;                    
                case 3:
                    return 'tv';
                    break;
                case 4:
                    return 'landLine';
                    break;                                        
                case 5:
                    return 'members';
                    break;                    
            }
        }        

        function increaseStep() {
            if (currStep < maxStep) {
                currStep++;
                //Potrebne broadcastnut event, aby ostatne komponenty vedeli ze krok sa zmenil
                $rootScope.$broadcast('stepChanged');
            }
        }

        function decreaseStep() {
            if (currStep > minStep) {
                currStep--;
                //Potrebne broadcastnut event, aby ostatne komponenty vedeli ze krok sa zmenil
                $rootScope.$broadcast('stepChanged');
            }
        }

        function setPageType(val) {
            pageType = val;
        }

        function getPageType() {
            return pageType;
        }

        function getStepName(step) {
            switch (step) {
                case 1:
                    return 'vas-pausal';
                    break;
                case 2: 
                    return 'internet';
                    break;
                case 3: 
                    return 'magio-tv';
                    break;
                case 4: 
                    return 'pevna-linka';
                    break; 
                case 5: 
                    return 'clenovia';
                    break; 
            }
        }
        
        function getStepFromName(stepName) {
            switch (stepName) {
                case 'vas-pausal':
                    return 1;
                    break;
                case 'internet': 
                    return 2;
                    break;
                case 'magio-tv': 
                    return 3;
                    break;
                case 'pevna-linka': 
                    return 4;
                    break; 
                case 'clenovia': 
                    return 5;
                    break; 
            }
        }
        
        function getSummaryUrl() {
            return summaryUrl;
        }

        function setSummaryUrl(url) {
            summaryUrl = url;
        }                
    }
})();