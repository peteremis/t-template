(function() {
    'use strict';

    angular
        .module('mag-1')
        .factory('http', http);

    http.$inject = ['$http', 'APP_CONST'];
    function http($http, APP_CONST) {
        var service = {
            getData:getData,
            getUserData: getUserData
        };

        return service;

        ////////////////
        function getData(url, msid) {
            return $http({
                method: 'GET',
                url: url,
                params: { msid: msid },
                cache: true
            });
        }

        function getUserData(url, msid) {
            return $http.get(url+'?msid=' + msid ).then(function (response) {
                return response.data;
            });            
        }        
    }
})();