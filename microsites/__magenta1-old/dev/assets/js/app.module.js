(function() {
    'use strict';

    angular.module('mag-1', ['ngAnimate','ngRoute','ngSanitize','slick','ngclipboard'])
    .constant('APP_CONST', {
        URL: {
            API_SEND_SMS: 'https://m2.telekom.sk/api/magenta1/send-sms',
            API_CHECK_CODE: 'https://m2.telekom.sk/api/magenta1/check-code',
            DATA_JSON: '../assets/js/data/data.json?v1',
            CALC_INTRO: '/kalkulacka',
            CALC_CHOOSER: '/kalkulacka/vyber-sluzby',
            CALC_CROSSROADS: '/kalkulacka/vyber',
            LANDING: '/',
            SUMMARY: '/kalkulacka/sumar',
            INTERESTED: '/kalkulacka/formular',
            API_LINK: 'https://backvm.telekom.sk/www/magenta1/storage.php'
        },
        MAIL: {
            LEAD: 'gen12@telekom.sk'
        }
    })
    .constant('PLAN_CONST', {
        'SERVICE_BOX': [{show: true, active: false, value: 'MÁM MOBILNÝ PAUŠÁL'},
                        {show: true, active: false, value: 'MÁM ZÁUJEM O NOVÝ PAUŠÁL'},
                        {show: true, active: false, value: 'BLÍZKY MÁ TELEKOM'},
                        {show: true, active: false, value: 'NEMÁM ZÁUJEM'}],
        "TITLE": "Máte mobilný paušál od Telekomu?",
        "DESC": "Na vytvorenie skupiny potrebujete paušál <b>ÁNO S</b> a vyšší alebo <b>Happy S</b> a vyšší."
    })
    .constant('INTERNET_CONST', {
        'SERVICE_BOX': [{show: true, active: false, value: 'MÁM MAGIO INTERNET'},
            {show: true, active: false, value: 'MÁM ZÁUJEM O INTERNET'},
            {show: true, active: false, value: 'BLÍZKY MÁ MAGIO INTERNET'},
            {show: true, active: false, value: 'NEMÁM ZÁUJEM'}],
        "TITLE": "Máte Magio Internet od Telekomu?",
        "DESC": "Ak pridáte do Magenta 1 skupiny službu Internet, cena vášho paušálu Happy bude nižšia.",
        "DESC2": "Ak pridáte do Magenta 1 skupiny Magio Internet, Magio TV alebo Pevnú linku, získate viac dát a volaní k vášmu paušálu ÁNO.",
        "DESC3": "Ak pridáte do Magenta 1 skupiny Magio Internet, Magio TV alebo Pevnú linku a jeden paušál M alebo vyšší, získate nižšiu cenu na svojom paušále Happy."
    })
    .constant('TV_CONST', {
        'SERVICE_BOX': [{show: true, active: false, value: 'MÁM MAGIO TV'},
            {show: true, active: false, value: 'MÁM ZÁUJEM O MAGIO TV'},
            {show: true, active: false, value: 'BLÍZKY MÁ MAGIO TV'},
            {show: true, active: false, value: 'NEMÁM ZÁUJEM'}],
        "TITLE": "Máte Magio televíziu od Telekomu?",
        "DESC": "Pridajte do svojej Magenta 1 skupiny službu Magio TV. Cena vášho paušálu Happy bude ešte nižšia.",
        "DESC2": "Ak pridáte do Magenta 1 skupiny Magio Internet, Magio TV alebo Pevnú linku, získate viac dát a volaní k vášmu paušálu ÁNO.",
        "DESC3": "Ak pridáte do Magenta 1 skupiny Magio Internet, Magio TV alebo Pevnú linku a jeden paušál M alebo vyšší, získate nižšiu cenu na svojom paušále Happy."
    })
    .constant('LANDLINE_CONST', {
        'SERVICE_BOX': [{show: true, active: false, value: 'MÁM TELEKOM PEVNÚ LINKU'},
            {show: true, active: false, value: 'MÁM ZÁUJEM O PEVNÚ LINKU'},
            {show: true, active: false, value: 'BLÍZKY MÁ TELEKOM PEVNÚ LINKU'},
            {show: true, active: false, value: 'NEMÁM ZÁUJEM'}],
        "TITLE": "Máte pevnú linku od Telekomu?",
        "DESC": "Služba pevná linka vám taktiež zníži cenu paušálu Happy, ak ju pridáte do svojej Magenta 1 skupiny.",
        "DESC2": "Ak pridáte do Magenta 1 skupiny Magio Internet, Magio TV alebo Pevnú linku, získate viac dát a volaní k vášmu paušálu ÁNO.",
        "DESC3": "Ak pridáte do Magenta 1 skupiny Magio Internet, Magio TV alebo Pevnú linku a jeden paušál M alebo vyšší, získate nižšiu cenu na svojom paušále Happy."
    })
    .constant('MEMBERS_CONST', {
        'SERVICE_BOX': [{show: true, active: false, value: 'MÁM BLÍZKYCH V TELEKOME'},
            {show: true, active: false, value: 'NEVIEM, ČI MAJÚ TELEKOM'},
            {show: true, active: false, value: 'MOJI BLÍZKI NEMAJÚ TELEKOM'}],
        "TITLE": "Máte priateľov alebo rodinu v Telekome?",
        "DESC": "Pridajte blízkych, ktorí majú paušál alebo EASY kartu v Telekome. Aj oni získajú výhody Magenta 1 skupiny. Všetci členovia si navzájom volajú zadarmo."
    })
    .filter('convNum', [
        function() {
            return function(value) {
                if (!isNaN(parseFloat(value)) && isFinite(value) && Math.floor(value) !== value) {
                    value = value.toFixed(2);
                    value = value.toString().replace(/\./g,',');
                }

                return value;
        };
    }])
    .config(['$locationProvider', '$routeProvider', 'APP_CONST',
    function config($locationProvider, $routeProvider, APP_CONST) {

        $locationProvider.hashPrefix('');
        //$locationProvider.html5Mode(true);

        $routeProvider.when(APP_CONST.URL.LANDING, {
            template: '<landing></landing>',
            resolve: {
                json_data: function($http, APP_CONST, dataChooser) {
                    if (!dataChooser.getDataLoad()) {
                        return $http.get(APP_CONST.URL.DATA_JSON).then(function(response) {
                            dataChooser.initService(response.data.data);
                        });
                    }
                }
            }            
        }).when(APP_CONST.URL.CALC_INTRO, {
            template: '<calc-intro></calc-intro>',
            resolve: {
                json_data: function($http, APP_CONST, dataChooser) {
                    if (!dataChooser.getDataLoad()) {
                        return $http.get(APP_CONST.URL.DATA_JSON).then(function(response) {
                            dataChooser.initService(response.data.data);
                        });
                    }
                }
            }            
        }).when(APP_CONST.URL.CALC_CROSSROADS + '/:param?', {
            template: '<calc-crossroads></calc-crossroads>',
            resolve: {
                json_data: function($http, APP_CONST, dataChooser, calculate) {
                    if (!dataChooser.getDataLoad() || !calculate.getDataLoad()) {
                        return $http.get(APP_CONST.URL.DATA_JSON).then(function(response) {
                            dataChooser.initService(response.data.data);
                            calculate.initService(response.data.calculate);
                        });
                    }
                }
            }
        }).when(APP_CONST.URL.CALC_CHOOSER + '/:param?/:param2?', {
            template: '<calc-chooser></calc-chooser>',
            resolve: {
                json_data: function($http, APP_CONST, dataChooser, calculate) {
                    if (!dataChooser.getDataLoad() || !calculate.getDataLoad()) {
                        return $http.get(APP_CONST.URL.DATA_JSON).then(function(response) {
                            dataChooser.initService(response.data.data);
                            calculate.initService(response.data.calculate);
                        });
                    }
                }
            }
        }).when(APP_CONST.URL.SUMMARY + '/:param?', {
            template: '<summary></summary>',
            resolve: {
                json_data: function($http, APP_CONST, dataChooser, calculate) {
                    if (!dataChooser.getDataLoad() || !calculate.getDataLoad()) {
                        return $http.get(APP_CONST.URL.DATA_JSON).then(function(response) {
                            dataChooser.initService(response.data.data);
                            calculate.initService(response.data.calculate);
                        });
                    }
                }
            }
        }).when(APP_CONST.URL.INTERESTED + '/:param?', {
            template: '<interested></interested>',
            resolve: {
                json_data: function($http, APP_CONST, dataChooser, calculate) {
                    if (!dataChooser.getDataLoad() || !calculate.getDataLoad()) {
                        return $http.get(APP_CONST.URL.DATA_JSON).then(function(response) {
                            dataChooser.initService(response.data.data);
                            calculate.initService(response.data.calculate);
                        });
                    }
                }
            }
        }).otherwise(APP_CONST.URL.LANDING);

        /*
        if (window.history && window.history.pushState) {
            $locationProvider.html5Mode({
                enabled: true,
                requireBase: false
            });
        }
        */
    }])
    .run(['user', 'appConfig', '$rootScope', '$window', '$location', 'localStorage', '$timeout',
    function(user, appConfig, $rootScope, $window, $location, localStorage, $timeout) {

        // initialise google analytics
        $window.ga('create', 'UA-55238177-1', 'auto');

        var thnx = angular.element('.fieldTypeTHANK_YOU_PAGE');
        if (thnx.length) {
            $timeout(function(){
                thnx.hide(200);
            }, 5000);    
        }

        /*
        // track pageview on state change
        $rootScope.$on('$routeChangeSuccess', function (event) {
            $window.ga('send', {
                'hitType': 'VirtualPageview',
                'appName' : 'Magenta1-kalkulacka',
                'screenName' : $location.url(),
                'hitCallback': function() {
                  console.log('GA hitCallback sent!', $location.url());
                }
            });
        });
        */

        $rootScope.$on('$routeChangeSuccess', function (event) {
            $window.ga('send', {
                'hitType': 'pageview',
                'page' : $location.url(),
                'hitCallback': function() {
                    //console.log('GA hitCallback sent!', $location.url());
                }
            });
        });

        var m1c_form_url = localStorage.getData('m1c_form_url');
        if (m1c_form_url) {
            localStorage.clearData('m1c_form_url');
            $location.url(m1c_form_url);
        }

        // Inicializacia user service a user objectu pri spusteni aplikacie
        user.initService();
        appConfig.initAppConfig();
    }]);
})();
