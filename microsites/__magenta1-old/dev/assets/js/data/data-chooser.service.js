(function() {
    'use strict';

    angular
        .module('mag-1')
        .factory('dataChooser', dataChooser);

    dataChooser.$inject = ['APP_CONST', 'appConfig', '$timeout'];

    function dataChooser(APP_CONST, appConfig, $timeout) {
        var service = {
            initService: initService,
            getData: getData,
            resetActivePropForCurrentService: resetActivePropForCurrentService,
            getDataAccordingToStep: getDataAccordingToStep,
            getDataFromStep: getDataFromStep,
            hasServiceAccordingToStepActiveProduct: hasServiceAccordingToStepActiveProduct,
            getFieldValue: getFieldValue,
            getDataLoad: getDataLoad,
            getDataPlan: getDataPlan
        };

        return service;

        var data;
        var dataLoaded = false;

        ////////////////

        function initService(appData) {
            data = appData;
            dataLoaded = true;
            /*
            $http({
                method : "GET",
                url : APP_CONST.URL.DATA_JSON
            }).then(function mySuccess(response) {
                    data = response.data;
            }, function myError(response) {
                //error
            }).then(function(response) {
                //complete
            });
            */                
        }

        function getDataLoad() {
            return dataLoaded;
        }

        function getData() {
            return data;
        }

        function resetActivePropForCurrentService() {
            var tempData = getDataAccordingToStep();

            tempData.forEach(function(elem) {
                elem.active = false;
            })

            //setDataAccordingToStep(tempData);

        }

        function getDataPlan(selected) {
            var result = [];
            var happyS = {};
            if (typeof selected === "number") {
                angular.forEach(data.plan, function(value,key){
                    if (selected != 2) {
                        if (value.id == 6) {
                            happyS = value;  
                        }
                        else if (value.id == 1 || value.id > 5) {
                            if (value.id == 12) result.push(happyS);
                            result.push(value);
                        }
                    } else {
                        if (value.id == 1 || (value.id >= 7 && value.id <= 11)) result.push(value);
                    }
                });
            } else {
                angular.forEach(data.plan, function(value,key){
                    if (value.namePrefix === selected) {
                        result.push(value);   
                    }
                });    
            }
            return result;
        }

        function getDataAccordingToStep() {
            var currStep = appConfig.getCurrStep();

            switch(currStep) {
                case 1:
                    return getDataPlan(1);
                    break;
                case 2:
                    return data.internet;
                    break;
                case 3:
                    return data.tv;
                    break;
                case 4:
                    return data.landLine;
                    break;
                case 5:
                    return data.plan;
                    break;                    
            }
        }

        function getFieldValue(typeService, searchBy, searchValue, outField) {
            var dataTmp = data[typeService];
            var result = null;
            angular.forEach(dataTmp, function(value, key) {
                if (value[searchBy] === searchValue)
                    result = value[outField];
            });

            return result;
        }

        function getDataFromStep(currStep, selected) {

            switch(currStep) {
                case 1:
                    return getDataPlan(selected);
                    break;
                case 2:
                    return data.internet;
                    break;
                case 3:
                    return data.tv;
                    break;
                case 4:
                    return data.landLine;
                    break;
                case 5:
                    return data.plan;
                    break;                    
            }
        }        

        function setDataAccordingToStep(serviceData) {
            var currStep = appConfig.getCurrStep();

            switch(currStep) {
                case 1:
                    return data.plan = serviceData;
                    break;
                case 2:
                    return data.internet = serviceData;
                    break;
                case 3:
                    return data.tv = serviceData;
                    break;
                case 4:
                    return data.landLine = serviceData;
                    break;
            }
        }

        function hasServiceAccordingToStepActiveProduct() {
            var tempData = getDataAccordingToStep();
            var active = 0;
            tempData.forEach(function(elem) {
                if(elem.active) {
                    active++;
                }
            })

            if(active > 0) {
                return true;
            } else {
                return false;
            }
        }

    }
})();