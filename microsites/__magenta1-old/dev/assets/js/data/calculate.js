(function() {
    'use strict';

    angular
        .module('mag-1')
        .factory('calculate', calculate);

    calculate.$inject = ['user', 'APP_CONST','appConfig','dataChooser'];

    function calculate(user, APP_CONST,appConfig,dataChooser) {
        var service = {
            initService: initService,
            getMagentaData: getMagentaData,
            getPriceSmart: getPriceSmart,
            getDiscount: getDiscount,
            getMagentaPrices: getMagentaPrices,
            getAllDiscounts: getAllDiscounts,
            getDataLoad: getDataLoad
        };

        return service;

        ////////////////

        var magenta_data;
        var chytry_balik;
        var magenta_level;
        var dataLoaded = false;
        var has_plan_m = null;
        var idConfig;

        function initService(appData) {
            magenta_data = appData['magenta'];
            chytry_balik = appData['chytry-balik'];
            dataLoaded = true;
            // setup
            idConfig = {
                defaultId: 7,
                mAndMore :[7,8,9,10,11,12,13,14,15,16,17,18],
                happyCreatedM1: {
                    s: 6,
                    m: 12,
                    l: 13,
                    xl_vol: 14,
                    xl_data:15,
                    xl: 16,
                    xxl: 17,
                    profi: 18
                },
                happyNoM: {
                    xs_mini: 4,
                    xs: 5,
                    s: 6,
                },
                ano: {
                    s: 1,
                    m: 7,
                    m_data: 8,
                    l: 9,
                    xl: 10,
                    xxl:11
                },
                anoAddData: {
                    s:  0.25,
                    m: 0.5,
                    m_data: 4,
                    l: 5,
                    xl: 10,
                    xxl: 15
                },
                other: {
                    easy: 2,
                    bez: 3
                }
            };            
        }
  
        function getDataLoad() {
            return dataLoaded;
        }        

        function getMagentaData() {
            return magenta_data;
        }

        function getPriceSmart(step) {

            return calculateSmartPrice(getSelectedProgram(step, 3));

        }

        function getSelectedProgram(step, action) {
            var data = {
                'internet': '',
                'tv': '',
                'landLine': ''
            };            

            switch (step) {
                case 5:
                case 4:
                    data.landLine = geSelectedName(user.getUserToStep(4), action);
                case 3:
                    data.tv = geSelectedName(user.getUserToStep(3), action);
                case 2:
                    data.internet = geSelectedName(user.getUserToStep(2), action);
            }

            return data;
        }
        
        function checkHappy(id, type) {
            var result = false;
            if (type == 'm1' || type == 'm') {
                angular.forEach(idConfig.happyCreatedM1, function(value, key){
                    if (id == value) {
                        if (type == 'm1') {
                            result = true;
                        } else if (type == 'm' && key != 's') {
                            result = true;
                        }
                    }
                });
            }
            
            return result;
        }
        
        function checkInProgram(id, type) {
            var result = false;
            angular.forEach(type, function(value, key){
                if (id == value) {
                    result = true;
                }
            });
            return result;                
        }        

        function getDiscount(step) {
            var discount_magenta = 0;
            var user_data = user.getUser();
            magenta_level = getLevel(step);
            //console.log(user_data.members);
            if (user_data.plan.selected == 0)
                return {};

            var planId = geSelectedName(user_data.plan, 0);
            
            if (planId === 0)
                planId = idConfig.defaultId;
            var userWithPhone = user_data.plan.withPhone;

            if (user_data.plan.selected == 4) {
                planId = idConfig.defaultId;
                userWithPhone = true;
            }

            var prices_magenta = [];
            var price_magenta = magenta_data['with-phone'][planId][0];
            var priceNew_magenta = magenta_data['with-phone'][planId][0];
            var anoData = 0;

            has_plan_m = check_plan_m(user_data.members.list);
            
            if (checkInProgram(planId, idConfig.happyCreatedM1)) { // programy happy
                if (userWithPhone) {
                    prices_magenta = magenta_data['with-phone'][planId];
                } else {
                    prices_magenta = magenta_data['without-phone'][planId];
                }
                
                if (prices_magenta.length > 0) {
                    var can_calc = true;

                    if (planId == idConfig.happyCreatedM1.s) { // happy S
                        if (step < 5 || !has_plan_m) {
                            can_calc = false;
                        }
                    }
                    
                    if (can_calc) {
                        price_magenta = prices_magenta[0];
                        priceNew_magenta = prices_magenta[magenta_level - 1];

                        discount_magenta = Math.round((price_magenta - priceNew_magenta) * 100) / 100;                    
                    }
                }
            } else if (step > 1) {
                if (getProgramCode(getSelectedProgram(step, 0)) !== '000') {
                    anoData = getAnoData(planId);
                }
            }

            var landProgramActual = getProgramCode(getSelectedProgram(step, 1));
            var landProgramNew = getProgramCode(getSelectedProgram(step, 3));            
            
            var landPriceActual = getSmartPrice(landProgramActual);
            var landPriceNew = getSmartPrice(landProgramNew);
            
            var wantToService = getSelectedProgram(step, 2); 
            
            // vypocet ceny bez chytreho balika
            angular.forEach(wantToService, function(value, key){
                if (value == 1)
                    landPriceActual += 10.9;
                else if (value == 2)
                    landPriceActual += 15.9;
                else if (value == 3)
                    landPriceActual += 20.9;                                        
            });
            var discount_chytryBalik = Math.round((landPriceActual - landPriceNew) * 100) / 100;
            
            var result = {
                "level": magenta_level,
                "discount_magenta": discount_magenta,
                "price_magenta": price_magenta,
                "priceNew_magenta": priceNew_magenta,
                "anoData" : anoData,
                "discount_chytryBalik": discount_chytryBalik,
                "price_chytryBalik": landPriceActual,
                "priceNew_chytryBalik": landPriceNew,
                "discount_all": discount_magenta + discount_chytryBalik,
                "plan_name": (planId !== 0 ? dataChooser.getFieldValue('plan', 'id', planId, 'name') : null),
                "planHas": (user_data.plan.selected),
                "landServices": []
            }

            return result;
        }

        function getAnoData(id) {
            var result = 0;
            
            angular.forEach(idConfig.ano, function(value, key) {
                if (id == value) {
                    result = idConfig.anoAddData[key];
                }
            });
                  
            return result;
        }        

        function getSmartPrice(code) {
            var result = 0
            angular.forEach(chytry_balik, function(value, key) {
                if (key === code)
                    result = value;
            });
            return result;    
        }

        function getProgramCode(data) {
            var result = '';
            angular.forEach(data, function(value, key) {
                if (value !== '')
                    result += value;
                else
                    result += '0';
            });
            return result;
        }

        function getAllDiscounts() {
            var user_data = user.getUser();
            var members = user_data.members.list;
            var magentaPrices = getMagentaPrices();
            magenta_level = getLevel(5);
            var member_data = dataChooser.getDataFromStep(5);
            
            // ceny bez zlavy
            var prices = {
                withPhone: [],
                withoutPhone: []
            };
            var data = magenta_data['with-phone'];
            
            angular.forEach(data, function(value, key) {
                prices.withPhone.push(value[0]);
            });

            data = magenta_data['without-phone'];
            angular.forEach(data, function(value, key) {
                prices.withoutPhone.push(value[0]);
            });

            var countLandServices = 0;
            if (user_data.internet.selected < 4)
                countLandServices++;
            if (user_data.tv.selected < 4)
                countLandServices++;            
            if (user_data.landLine.selected < 4)
                countLandServices++;                
            
            var userDiscount = getDiscount(5);
            var allDiscounts = {
                'level': magenta_level,
                'countLandServices': countLandServices,
                'user': userDiscount,
                'members': {
                    'discounts': 0,
                    'plan': [],
                    'landServices': []
                },
                'discounts': 0,
                'anoData': 0
            };

            if (user_data.internet.selected < 3) {
                allDiscounts.user.landServices.push({
                    'has': (user_data.internet.selected == 1 ? true : false),
                    'name': 'Internet ' + (user_data.internet.selected == 1 ? getLandServiceName(user_data.internet.has) : getLandServiceName(user_data.internet.interested)),
                });
            }
            else if (user_data.internet.selected == 3 ) {
                allDiscounts.members.landServices.push({
                    'name': 'Internet ' + getLandServiceName(user_data.internet.frendHas)
                });
            }

            if (user_data.tv.selected < 3) {
                allDiscounts.user.landServices.push({
                    'has': (user_data.tv.selected == 1 ? true : false),
                    'name': 'Magio TV ' + (user_data.tv.selected == 1 ? getLandServiceName(user_data.tv.has) : getLandServiceName(user_data.tv.interested)),
                });
            }
            else if (user_data.tv.selected == 3 ) {
                allDiscounts.members.landServices.push({
                    'name': 'Magio TV ' + getLandServiceName(user_data.tv.frendHas)
                });
            }

            if (user_data.landLine.selected < 3) {
                allDiscounts.user.landServices.push({
                    'has': (user_data.landLine.selected == 1 ? true : false),
                    'name': 'Pevná linka ' + (user_data.landLine.selected == 1 ? getLandServiceName(user_data.landLine.has) : getLandServiceName(user_data.landLine.interested)),
                });
            }
            else if (user_data.landLine.selected == 3 ) {
                allDiscounts.members.landServices.push({
                    'name': 'Pevná linka ' + getLandServiceName(user_data.landLine.frendHas)
                });
            }            

            var discount = 0;
            var anoData = 0;
            var tmp_memberPrice = 0;
            var tmp_memberNewPrice = 0;

            if (has_plan_m == null) {
                has_plan_m = check_plan_m(members);
            }
            var membersMin = 0;
            if (user_data.members.selected == 1) {
                for (var i = 0; i < 4; i++)
                {
                    if (!angular.equals(members[i],{}))
                    {
                        var member = {
                            'anoData': 0
                        };
                        var id = members[i].id;
                        if ((checkInProgram(id, idConfig.happyNoM) && has_plan_m) || (checkInProgram(id, idConfig.happyCreatedM1) && id != idConfig.happyCreatedM1.s)) { // happy
                            if (members[i].withPhone) {
                                discount = prices.withPhone[id-3] - magentaPrices.withPhone[id-3];
                                member.price = prices.withPhone[id-3];
                                member.newPrice = magentaPrices.withPhone[id-3];
                            }
                            else {
                                discount = prices.withoutPhone[id-3] - magentaPrices.withoutPhone[id-3];
                                member.price = prices.withoutPhone[id-3];
                                member.newPrice = magentaPrices.withoutPhone[id-3];                            
                            }
                            discount = Math.round(discount * 100) / 100;
                            tmp_memberPrice += member.price;
                            tmp_memberNewPrice += member.newPrice;
                            member.discount = discount;
                            allDiscounts.members.discounts += discount;
                        }
                        else if (checkInProgram(id, idConfig.other)) {
                            member.price = prices.withPhone[id-3];
                            member.newPrice = magentaPrices.withPhone[id-3];   
                        }
                        else { // ano
                            if (countLandServices > 0) {
                                member.anoData = getAnoData(id);
                                anoData += member.anoData;
                            }
                            if (id == idConfig.ano.s || id == idConfig.ano.m_data) {
                                membersMin += 100;    
                            }
                        }
                        
                        member.name = member_data[id-1].name;
                        allDiscounts.members.plan.push(member);
                    }
                }
            }

            allDiscounts.anoData = anoData + allDiscounts.user.anoData;
            allDiscounts.members.price = Math.round(tmp_memberPrice * 100) / 100;
            allDiscounts.members.min = membersMin;
            allDiscounts.members.newPrice = Math.round(tmp_memberNewPrice * 100) / 100;
            allDiscounts.discounts = allDiscounts.members.discounts + allDiscounts.user.discount_all;
            
            return allDiscounts;
        }

        function check_plan_m(members) {
            for (var i = 0; i < 4; i++)
            {
                if (!angular.equals(members[i],{})) {
                    var id = members[i].id;
                    if (idConfig.mAndMore.includes(id)) return true;
                }
            }

            return false;
        }        

        function getLandServiceName(id) {
            var result = '';
            if (id == 1) result = 'M';
            else if (id == 2) result = 'L';
            else if (id == 3) result = 'XL';
            return result;
        }

        function getLevel(step) {
            var countService = 0;
            var countLandService = 0;
            var userData = user.getUser();
            switch (step) {
                case 5:
                    var members = userData.members.list;

                    members.forEach(function(elem) {
                        if ((!angular.equals(elem,{})) && idConfig.mAndMore.includes(elem.id)) {
                            countService++;
                        }
                    });
                case 4:
                    if (userData.landLine.selected > 0 &&  userData.landLine.selected < 4) {
                        countLandService++;
                        countService++;
                    }
                case 3:
                    if (userData.tv.selected > 0 &&  userData.tv.selected < 4) {
                        countLandService++;
                        countService++;
                    }                    
                case 2:
                    if (userData.internet.selected > 0 &&  userData.internet.selected < 4) {
                        countLandService++;
                        countService++;
                    }
                case 1:
                    if (idConfig.mAndMore.includes(geSelectedName(userData.plan, 0))) {
                        countService++;
                    }                    
            }
            
            // if (userData.plan.selected == 1 && userData.plan.has == 6) countService--;

            var level = 1;
            if (countService == 2)
                level = 2;
            else if (countService == 3 || (countService > 3 && countLandService == 0))
                level = 3;                
            else if ((countService == 4 && countLandService > 0) || (countService > 4 && countLandService == 1))
                level = 4;                
            else if (countService > 4 && countLandService > 1)
                level = 5;
            
            //console.log('level:',level)
            return level;
        }

        function getMagentaPrices() {
            magenta_level = getLevel(5);
            var prices = {
                withPhone:[],
                withoutPhone: []
            };
            var data = magenta_data['with-phone'];

            angular.forEach(data, function(value, key) {
                prices.withPhone.push(value[magenta_level - 1]);
            });

            data = magenta_data['without-phone'];
            angular.forEach(data, function(value, key) {
                prices.withoutPhone.push(value[magenta_level - 1]);
            });
            
            return prices;
        }

        function calculateSmartPrice(dataSelected) {
            var price = [10,15,20];

            var countSelected = 0;
            if (dataSelected.internet !== '')
                countSelected++;
            if (dataSelected.tv !== '' && appConfig.getCurrStep() > 3)
                countSelected++;
            
            if (countSelected == 1) {
                var tmpSelect = dataSelected.internet !== '' ?  dataSelected.internet : dataSelected.tv;
                
                if (tmpSelect === 1)
                    price = ['+ 8,30','+ 11,30','+ 15,30'];
                if (tmpSelect === 2)
                    price = ['+ 6,30','+ 9,30','+ 13,30'];
                if (tmpSelect === 3)
                    price = ['+ 5,30','+ 8,30','+ 12,30'];
            }
            else if(countSelected > 1) { 
                if (dataSelected.internet === 1) { 
                    if (dataSelected.tv === 1) // 19.20
                        price = ['+ 5,60','+ 7,60','+ 10,60'];
                    if (dataSelected.tv === 2) // 22.20
                        price = ['+ 4,60','+ 6,60','+ 9,60'];
                    if (dataSelected.tv === 3) // 26.20
                        price = ['+ 3,60','+ 5,60','+ 8,60'];
                }
                else if (dataSelected.internet === 2) { 
                    if (dataSelected.tv === 1) // 22.20
                        price = ['+ 4,60','+ 6,60','+ 9,60'];
                    if (dataSelected.tv === 2) // 25.20
                        price = ['+ 3,60','+ 5,60','+ 8,60'];
                    if (dataSelected.tv === 3) // 29.20
                        price = ['+ 2,60','+ 4,60','+ 7,60'];
                }
                else if (dataSelected.internet === 3) { 
                    if (dataSelected.tv === 1) // 26.20
                        price = ['+ 3,60','+ 5,60','+ 8,60'];
                    if (dataSelected.tv === 2) // 29.20
                        price = ['+ 2,60','+ 4,60','+ 7,60'];
                    if (dataSelected.tv === 3) // 33.20
                        price = ['+ 1,60','+ 3,60','+ 6,60'];
                }
            }

            return price;
        }

        function geSelectedName(userSelected, action) {
            var selected = userSelected.selected;
            if ((action == 1 && selected != 1) // vrati len sluzby, ktore ma
                || (action == 2 && selected != 2) // vrati len sluzby, ktore chce
                || (action == 3 && selected > 2)) // vrati len sluzby, ktore ma alebo chce
                return '';                

            switch (selected) {
                case 1:
                    return userSelected['has'];
                    break;
                case 2:
                    return userSelected['interested'];
                    break;                    
                case 3:
                    return userSelected['frendHas'];
                    break;                                        
                default:
                    return '';
            }
        }
   }
})();