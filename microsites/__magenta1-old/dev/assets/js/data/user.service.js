(function() {
    'use strict';

    angular
        .module('mag-1')
        .factory('user', user);

    user.$inject = ['APP_CONST', 'localStorage', '$rootScope', 'appConfig', 'dataChooser'];

    function user(APP_CONST, localStorage, $rootScope, appConfig, dataChooser) {
        var service = {
            getUser: getUser,
            getUserMsid: getUserMsid,
            initService: initService,
            setUserMsid: setUserMsid,
            setUserProps: setUserProps,
            getUserGroup: getUserGroup,
            getServiceAccordingToStep: getServiceAccordingToStep,
            getUserToStep: getUserToStep,
            hasUserSelectedService: hasUserSelectedService,
            setService: setService,
            isKnownUser: isKnownUser,
            updateLocalStorage: updateLocalStorage,
            setMember: setMember,
            userReset: userReset,
            normalizeUser: normalizeUser,
            setUser: setUser,
            setAno: setAno,
            hasAno: hasAno
        };

        return service;

        var user;

        ////////////////

        if (!$rootScope.userDeleted) $rootScope.userDeleted = $rootScope.$on('userDeleted', initService);

        function initService() {
            
            if (localStorage.getObj('user')) {
                user = localStorage.getObj('user');
            } else {
                // Inicializacia user objektu ak sa nenachadza v localStorage browseru
                userReset()
            }
        }

        function userReset() {
            user = {
                msid: '',
                plan: {
                    has: 0,
                    interested: 0,
                    frendHas: 0,
                    withPhone: true,
                    selected: 0,
                    hasApi: false
                },
                internet: {
                    has: 0,
                    interested: 0,
                    frendHas: 0,
                    selected: 0,
                    hasApi: false
                },
                tv: {
                    has: 0,
                    interested: 0,
                    frendHas: 0,
                    selected: 0,
                    hasApi: false
                },
                landLine: {
                    has: 0,
                    interested: 0,
                    frendHas: 0,
                    selected: 0,
                    hasApi: false
                },
                members: {
                    list: [{},{},{},{}],
                    selected: 0
                },
                knownUser: false,
                //User group moze byt podla identifikacie:
                // - splna vsetky poziadavky, cize ma narok na mag1 (1)
                // - nema narok na mag1 (2)
                // - nie je telekom zakaznik (3)
                // - ma mensi pausal ako m alebo easy (4)
                group: 0,
                ano: true
            }

            localStorage.setObj('user', user);
        }

        function hasAno() {
            return user.ano;
        }

        function setAno(value) {
            user.ano = value;
        }

        function updateLocalStorage() {
            localStorage.setObj('user', user);
        }

        function setUserProps(type, key, value) {
            user[type][key] = value;
            updateLocalStorage();
        }

        function setUserMsid(msid) {
            // Nastavuje sa kvoli prvotnej inicializacii usera, pred ajaxcallom
            user.msid = msid;
        }

        function getIdProgram(apiValue, json) {
            var result = 0;
            
            if (apiValue.length > 0) {
                angular.forEach(json, function(value, key){
                    var jsonValue = value.name.toLowerCase();
                    if (apiValue == jsonValue) {
                        result = value.id;
                    }
                });
            }
            
            return result;
        }

        function normalizeUser(data) {
            // Normalizacia dat prichadzajuce z api
            var dataJson = dataChooser.getData();
            user.msid = data.msisdn;
            
            user.plan.has = getIdProgram(data.plan.toLowerCase(), dataJson.plan);
 
            if (data.plan !== '') {
                user.plan.selected = 1;
                user.plan.hasApi = true;
            } else {
                user.plan.selected = 0;
            }

            user.internet.has =  getIdProgram(data.internet.toLowerCase(), dataJson.internet);
            if (data.internet !== '') {
                user.internet.selected = 1;
                user.internet.hasApi = true;
            } else {
                user.internet.selected = 0;
            }

            user.tv.has = getIdProgram(data.tv.toLowerCase(), dataJson.tv);
            if (data.tv !== '') {
                user.tv.selected = 1;
                user.tv.hasApi = true;
            } else {
                user.tv.selected = 0;
            }

            user.landLine.has = getIdProgram(data.landline.toLowerCase(), dataJson.landLine);
            if (data.landline !== '') {
                user.landLine.selected = 1;
                user.landLine.hasApi = true;
             } else {
                user.landLine.selected = 0;
             }

            user.knownUser = true;

            //TODO: nastavit podmientku pre usergroupu
            user.group = 1;

            localStorage.setObj('user', user);
        }

        function setUser(newUser) {
            user = newUser;
            updateLocalStorage();
        }
        
        function getUser() {
            return user;
        }

        function getUserMsid() {
            return user.msid;
        }

        function getUserGroup() {
            return user.group;
        }
        
        function getServiceAccordingToStep() {
            var currStep = appConfig.getCurrStep();

            switch(currStep) {
                case 1:
                    return user.plan;
                    break;
                case 2:
                    return user.internet;
                    break;
                case 3:
                    return user.tv;
                    break;
                case 4:
                    return user.landLine;
                    break;
                case 5:
                    return user.members;
                    break;                    
            }
        }

        function getUserToStep(step) {
            if (step < 1) step = 1;
            switch(step) {
                case 1:
                    return user.plan;
                    break;
                case 2:
                    return user.internet;
                    break;
                case 3:
                    return user.tv;
                    break;
                case 4:
                    return user.landLine;
                    break;
                case 5:
                    return user.members;
                    break;                    
            }       
        }

        function hasUserSelectedService() {
            var service = getServiceAccordingToStep();

            if (service.has !== '' || service.interested !== '' || service.frendHas !== '') {
                return true;
            } else {
                return false;
            }
        }

        function setService(service, value) {
            var currStep = appConfig.getCurrStep();

            switch(currStep) {
                case 1:
                    return user.plan[service] = value;
                    break;
                case 2:
                    return user.internet[service] = value;
                    break;
                case 3:
                    return user.tv[service] = value;
                    break;
                case 4:
                    return user.landLine[service] = value;
                    break;
                case 5:
                    return user.members[service] = value;
                    break;                    
            }

            localStorage.setObj('user', user);
        }

        function setMember(key, service, value) {
            if (service != null)
                user.members.list[key][service] = value;
            else 
                user.members.list[key] = {};
            localStorage.setObj('user', user);
        }

        function isKnownUser() {
            return user.knownUser;
        }
    }
})();