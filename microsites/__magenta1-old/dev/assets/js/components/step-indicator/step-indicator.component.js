(function() {
    'use strict';

    angular
        .module('mag-1')
        .component('stepIndicator', {
            templateUrl: '../assets/js/components/step-indicator/step-indicator.component.html',
            controller: stepIndicatorController,
            controllerAs: '$ctrl'
        });

    stepIndicatorController.$inject = ['$rootScope', '$route', '$location', 'appConfig', 'localStorage', 'user','dataChooser','APP_CONST'];

    function stepIndicatorController($rootScope, $route, $location, appConfig, localStorage, user, dataChooser, APP_CONST) {
        var $ctrl = this;
        $rootScope.programSelected = {};
        $ctrl.userInfo = '';
        $ctrl.step = 1;    

        ////////////////
        
        function updateActiveElem() {
            var step = appConfig.getCurrStep();
            
            {
                var currPageType = appConfig.getPageType();
                if (currPageType === 'chooser') {
                    $rootScope.programSelected[step] = getShortName(step);
                }
                else
                    $rootScope.programSelected[step] = '';
                
                angular.element('.active-step-current').removeClass('active-step-current');
                angular.element('.active-step').removeClass('active-step');
                angular.element('.step-' + step).addClass('active-step');
                angular.element('.step-' + step).addClass('active-step-current');
                
                for (var i = 1; i < step; i++)
                {
                    angular.element('.step-' + i).addClass('active-step');
                    if (i < step)
                    {
                        $rootScope.programSelected[i] = getShortName(i);    
                    }
                }
            }
        }

        function getShortName(step)
        {
            if (step == 5) return '';
            var result = '';
            var userData = user.getUserToStep(step);
            var shortSelected = '';

            if (userData.selected == 1)
                shortSelected = userData.has;    
            else if (userData.selected == 2)
                shortSelected = userData.interested;
            else if (userData.selected == 3)
                shortSelected = userData.frendHas;             
            
            if (shortSelected > 0)
            {
                var data = dataChooser.getDataFromStep(step);
                
                data.forEach(function(elem) {
                    
                    if (elem.id == shortSelected) {
                        result = elem.namePrefix + ' <span class="text-bold">' + elem.shortName + '</span>';
                    }
                });
            }
            else 
            {
                result = 'Nemám záujem';
            }
            
            return result;
        }

        if (!$rootScope.stepChanged) $rootScope.stepChanged = $rootScope.$on('stepChanged', function() {
            updateActiveElem()
        });

        $ctrl.clearLocalStorage = function() {
            localStorage.clearData('user');
        }



        $ctrl.gotoToStep = function(step) {
            var currPageType = appConfig.getPageType();
            if (appConfig.getCurrStep() > step) {
                appConfig.setCurrStep(step);
                if (user.getUserToStep(step).selected != 4) {
                    if (currPageType == 'chooser')
                        $location.url(APP_CONST.URL.CALC_CHOOSER + "/" + appConfig.getStepName(appConfig.getCurrStep()));
                    else
                        $location.path(APP_CONST.URL.CALC_CHOOSER + "/" + appConfig.getStepName(appConfig.getCurrStep()));
                }   
                else {
                    if (currPageType == 'chooser')
                        $location.path(APP_CONST.URL.CALC_CROSSROADS + "/" + appConfig.getStepName(appConfig.getCurrStep()));
                    else
                        $location.url(APP_CONST.URL.CALC_CROSSROADS + "/" + appConfig.getStepName(appConfig.getCurrStep()));
                } 
            }
        }

        $ctrl.$onInit = function() {
            $ctrl.step = appConfig.getCurrStep();
            updateActiveElem();
        };
        $ctrl.$onChanges = function() { };
        $ctrl.$onDestroy = function() { };
    }
})();