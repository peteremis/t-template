(function() {
    'use strict';

    angular
        .module('mag-1')
        .component('serviceCrossroads', {
            templateUrl: '../assets/js/components/service-crossroads/service-crossroads.component.html',
            controller: serviceCrossroadsController,
            controllerAs: '$ctrl'
        });

    serviceCrossroadsController.$inject = ['$routeParams','$location', '$rootScope', 'appConfig', 'user', 'APP_CONST', 'PLAN_CONST', 'INTERNET_CONST', 'TV_CONST', 'LANDLINE_CONST', 'MEMBERS_CONST', '$timeout']
    function serviceCrossroadsController($routeParams,$location, $rootScope, appConfig, user, APP_CONST, PLAN_CONST, INTERNET_CONST, TV_CONST, LANDLINE_CONST, MEMBERS_CONST, $timeout ) {
        var $ctrl = this;
        $ctrl.step = 0;
        $ctrl.serviceBoxLinks = [];
        $ctrl.serviceBoxTitle = null;

        var userGroup = 0;

        ////////////////

        //Definuje spravenie pre boxy na crossroads strankach
            //Prvotny case definuje cislo boxu na ktory user klikol
            //Druhy case resetuje vsetky hodnoty na false
            //Treti case definuje v ktorom kroku sa user nachadza
        $ctrl.redirect = function(link) {
            user.setService('selected', link);
            user.updateLocalStorage();

            if ($ctrl.step < 5) {
                switch(link) {
                    case 1:
                    case 2:
                    case 3:
                        $location.path(APP_CONST.URL.CALC_CHOOSER + '/' + appConfig. getStepName(appConfig.getCurrStep()));
                        break;
                    case 4:
                        appConfig.increaseStep();
                        initThisComp();
                        $location.url(APP_CONST.URL.CALC_CROSSROADS + '/' + appConfig. getStepName(appConfig.getCurrStep()));                        
                        break;
                }
            }
            else {
                if (link == 1)
                    $location.path(APP_CONST.URL.CALC_CHOOSER + '/' + appConfig. getStepName(appConfig.getCurrStep()));
                else
                    $location.path(APP_CONST.URL.SUMMARY);
            }
        }

        function setActiveBox() {
            resetActiveBoxes();
            var tempSelected = user.getServiceAccordingToStep().selected;
            
            if (tempSelected > 0) {
                $ctrl.serviceBoxLinks[tempSelected - 1].active = true;
            }
        }

        function resetActiveBoxes() {
            $ctrl.serviceBoxLinks.forEach(function(elem) {
                elem.active = false;
            })
        }

        function hideServiceBox(serviceBoxNum) {
            //Pri niektorych scenaroch treba skryt niektore serviceBox elementy
            //hodnota od 1 po serviceBox max
            $ctrl.serviceBoxLinks[serviceBoxNum - 1].show = false;
        }

        function loadConstants() {
            //Podla toho v akom kroku sme, nahra konstanty zo suboru crossroads.const.js pre rozcestnik
            var userPlan = user.getUser().plan;
            switch(appConfig.getCurrStep()) {
                case 1:
                    $ctrl.serviceBoxLinks = PLAN_CONST.SERVICE_BOX;
                    $ctrl.serviceBoxTitle = [PLAN_CONST.TITLE, PLAN_CONST.DESC];
                    break;
                case 2:
                    $ctrl.serviceBoxLinks = INTERNET_CONST.SERVICE_BOX;
                    var desc = !user.hasAno() ? INTERNET_CONST.DESC : INTERNET_CONST.DESC2;
                    if (userPlan.selected == 1 && userPlan.has == 6) desc = INTERNET_CONST.DESC3;
                    $ctrl.serviceBoxTitle = [INTERNET_CONST.TITLE, desc];
                    break;
                case 3:
                    $ctrl.serviceBoxLinks = TV_CONST.SERVICE_BOX;
                    var desc = !user.hasAno() ? TV_CONST.DESC : TV_CONST.DESC2;
                    if (userPlan.selected == 1 && userPlan.has == 6) desc = INTERNET_CONST.DESC3;
                    $ctrl.serviceBoxTitle = [TV_CONST.TITLE, desc];
                    break;
                case 4:
                    $ctrl.serviceBoxLinks = LANDLINE_CONST.SERVICE_BOX;
                    var desc = !user.hasAno() ? LANDLINE_CONST.DESC : LANDLINE_CONST.DESC2;
                    if (userPlan.selected == 1 && userPlan.has == 6) desc = INTERNET_CONST.DESC3;
                    $ctrl.serviceBoxTitle = [LANDLINE_CONST.TITLE, desc];
                    break;
                case 5:
                    $ctrl.serviceBoxLinks = MEMBERS_CONST.SERVICE_BOX;
                    $ctrl.serviceBoxTitle = [MEMBERS_CONST.TITLE, MEMBERS_CONST.DESC];
                    break;                    
            }
        }

        $ctrl.showPopup = function(popupId, data) {
            appConfig.setPopup(popupId, data);
            $rootScope.$broadcast('showPopup');
        }        

        //$rootScope.$on('gotoCrossroads',initThisComp);

        function initThisComp() {
            $ctrl.step = appConfig.getCurrStep();

            loadConstants();
            $rootScope.$broadcast('getDiscount');
            //Ak user nepatri do skupiny 1, cize sme ho nevedeli identifikovat,
            //tak na rozcestniku nezobrazujeme box "Mam pausal", "Mam internet", atd
            userGroup = user.getUserGroup();
            
            if ($ctrl.step == 1) {
                hideServiceBox(3);
            }

            setActiveBox();
        }

        $ctrl.$onInit = function() {
            if (typeof $routeParams.param != 'undefined') {
                appConfig.setCurrStep(appConfig.getStepFromName($routeParams.param));
            }
            else { 
                appConfig.setCurrStep(1);
                $location.url(APP_CONST.URL.CALC_CROSSROADS + '/' + appConfig.getStepName(1));
            }

            for (var i=1; i <= appConfig.getCurrStep(); i++) {
                if (user.getUserToStep(i).selected == 0) {
                    appConfig.setCurrStep(i);
                    $location.url(APP_CONST.URL.CALC_CROSSROADS + '/' + appConfig.getStepName(i));
                }
            }
            
            if (!appConfig.getShowHelp()) {
                appConfig.setShowHelp(true);
                $timeout(function(){
                    angular.element('.help-menu').show();
                    $timeout(function(){
                        angular.element('.help-menu__item').css('width', '200px');
                        if ($(window).width() < 900) {
                            $timeout(function(){
                                angular.element('.help-menu__item').css('width', '45px');
                            }, 2000);
                        }
                    }, 1000);
                }, 5000);
            }
            else {
                angular.element('.help-menu').show();
                if ($(window).width() > 992) {
                    angular.element('.help-menu__item').css('width', '200px');
                } else {
                    angular.element('.help-menu__item').css('width', '45px');
                }
            }            

            initThisComp();
            var sticky_top = 70;

            if ($(window).width() < 800) {
                sticky_top = 44;
            }
            $('html,body').scrollTop(sticky_top + 1);
        
            setSticks(sticky_top);
            
            $('.right').css({ 'left':'0', 'right':'0','margin-left': 'auto','margin-right': 'auto'});
                
            $( window ).scroll(function() {
                setSticks(sticky_top)
            });            
        };

        function setSticks(sticky_top) {
            var step_indicator = $('step-indicator');
            var step_controls = $('step-controls');
            var heightWindow = $(window).height();
            var widthWindows = $(window).width();
            var heightFooter = $('#footer').height();

            if (widthWindows < 992) {
                angular.element('.help-menu').css({ 'position': 'fixed', 'bottom': '90px','top': 'inherit' });
            }
            else {
                angular.element('.help-menu').css({ 'position': 'absolute', 'top': '80px', 'bottom': 'inherit' });
            }

            if ($(window).scrollTop() > sticky_top) {
                step_indicator.addClass("stick-top");
                if (widthWindows < 800) {
                    $('.phone-num').addClass('phone-num-2');
                }
                else {
                    $('.phone-num').removeClass('phone-num-2');
                }

                if (widthWindows > 991) {
                    angular.element('.help-menu').css({ 'position': 'fixed', 'top': '80px','bottom': 'inherit' });
                }
            }
            else {
                step_indicator.removeClass("stick-top");
                $('.phone-num').removeClass('phone-num-2');
            }

            var comp = $('#content .stick-cont');
            var height = 870;

            if (appConfig.getCurrStep() == 5) {
                if (widthWindows < 768) {
                    height = 1960;
                }
                else if (widthWindows < 800) {
                    height = 1150;
                }
                else if (widthWindows < 992) {
                    height = 1100;
                }
                else {
                    height = 850;
                }
            }

            comp.css('height', (height) + 'px');

            if (comp.length) {
                if (comp.offset().top + height - heightWindow < $(window).scrollTop()) {
                    step_controls.removeClass("stick-bottom");
                    if (widthWindows < 992) {
                        angular.element('.help-menu').css({ 'position': 'absolute' });
                    }
                }
                else {
                    step_controls.addClass("stick-bottom");
                    //comp.removeClass('stick-cont-2');
                }
            }
        }
        
        $ctrl.$onChanges = function() { };
        $ctrl.$onDestroy = function() { 
            $('.right').css({ 'left':'100%'});
        };
    }
})();