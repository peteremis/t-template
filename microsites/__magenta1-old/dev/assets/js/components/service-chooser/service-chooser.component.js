(function() {
    'use strict';

    angular
        .module('mag-1')
        .component('serviceChooser', {
            templateUrl: '../assets/js/components/service-chooser/service-chooser.component.html',
            controller: serviceChooserController,
            controllerAs: '$ctrl'
        });

    serviceChooserController.$inject = ['$routeParams','$rootScope', 'user', 'dataChooser', 'appConfig', 'calculate', '$timeout', '$location', 'APP_CONST'];
    function serviceChooserController($routeParams, $rootScope, user, dataChooser, appConfig, calculate, $timeout, $location, APP_CONST) {
        var $ctrl = this;
        $ctrl.data = null;
        $ctrl.dataMembers = [];
        $ctrl.title = null;
        $ctrl.titleDesc = null;
        $ctrl.step = 0;
        $ctrl.selected = 0;
        $ctrl.showSelectProgram = true;
        $ctrl.withPhone = true;
        $ctrl.withPhoneMember = [true,true,true,true];
        var selectedMember = 0;
        var crossroadsSelected = 0;

        ////////////////

        function calculateSmartPrice() {
            var newPrice = [10,15,20];
            if (user.getUserToStep($ctrl.step).selected < 3)
                newPrice = calculate.getPriceSmart(appConfig.getCurrStep());

            $ctrl.data.forEach(function(elem) {
                elem.price = newPrice[elem.id-1];
            });
        }

        $ctrl.setPricePhone = function() {
            addQTip();
            user.setService('withPhone', $ctrl.withPhone);
        }

        $ctrl.setPricePhoneMember = function(x) {
            addQTip();
            user.setMember(x, 'withPhone', $ctrl.withPhoneMember[x]);

        }

        $ctrl.showPopup = function(popupId, data, member) {

            var result =  angular.copy(data);
            if (member != null) {
                result.price = $ctrl.withPhoneMember[member] ? data.newPrice : data.newPriceWithoutPhone;
                result.withPhone = $ctrl.withPhoneMember[member];
            }
            else if (popupId == 'popup_detail') {
                result.price = $ctrl.withPhone ? data.price : data.priceWithoutPhone;
                result.withPhone = $ctrl.withPhone;
            }

            appConfig.setPopup(popupId, result);
            result = null;
            $rootScope.$broadcast('showPopup');
        }

        $ctrl.showPopupProgram = function() {
            var id_comp = (appConfig.getCurrStep() < 5) ? '#popup_program' : '#popup_program_members';

            var windowHeight = $(window).height();
            var popupWidth = 300;
            //popup program_members
            if (id_comp == "#popup_program_members") {
                if (windowHeight < 640) {
                  popupWidth = 320;
                    $('#popup_program_members .program_members_content').css('height', windowHeight - 40 - 30 - 40);
                }
            }

            $.fancybox.open([{
                href: id_comp,
                padding: 0,
                margin: 0,
                scrolling: 'no',
                closeBtn: false,
                parent: "#content",
                width: popupWidth,
                maxWidth: 600,
                height: 'auto',
                autoSize: false,
                helpers: {
                    overlay: {
                        css: {
                            'background': 'rgba(0, 0, 0, 0.7)'
                        }
                    }
                }
            }]);
        }


        $ctrl.closePopup = function() {
            $.fancybox.close();
        }

        function stepPlan() {
            var userPlan = user.getUser().plan;
            $ctrl.selected = userPlan.selected;
            $ctrl.data = dataChooser.getDataFromStep(1, userPlan.selected);
            var title = dataChooser.getData().planTitle;
            var titleKey = 'title' + crossroadsSelected;
            if (user.getServiceAccordingToStep().hasApi) titleKey += '_';
            $ctrl.title = title[titleKey];
            $ctrl.titleDesc = title['desc'];
            $ctrl.titleDesc = userPlan.selected != 2 ? title['desc'] : title['desc2'];
            $ctrl.withPhone = userPlan.withPhone;
            
            if (typeof $routeParams.param2 != 'undefined') {
                var program = $routeParams.param2;
                if (program.startsWith('ano-')) {
                    var newName = 'ÁNO ' + program.substr(4).toUpperCase();
                    
                    $ctrl.data.forEach(function(elem) {
                        if (elem.name === newName) {
                            user.setUserProps('plan', 'interested', elem.id);
                        }
                    });
                }
            }             
            
            setActiveProp(userPlan);
        }

        function stepInternet() {
            $ctrl.data = dataChooser.getDataFromStep(2);
            var title = dataChooser.getData().internetTitle;
            var titleKey = 'title' + crossroadsSelected;
            if (user.getServiceAccordingToStep().hasApi) titleKey += '_';
            $ctrl.title = title[titleKey];
            $ctrl.titleDesc = !user.hasAno() ? title['desc'] : title['desc2'];
            var userPlan = user.getUser().plan;
            if (userPlan.selected == 1 && userPlan.has == 6) $ctrl.titleDesc = title['desc3'];
            setActiveProp(user.getUser().internet);
        }

        function stepTv() {
            $ctrl.data = dataChooser.getDataFromStep(3);
            calculateSmartPrice();
            var title = dataChooser.getData().tvTitle;
            var titleKey = 'title' + crossroadsSelected;
            if (user.getServiceAccordingToStep().hasApi) titleKey += '_';
            $ctrl.title = title[titleKey];
            $ctrl.titleDesc = !user.hasAno() ? title['desc'] : title['desc2'];
            var userPlan = user.getUser().plan;
            if (userPlan.selected == 1 && userPlan.has == 6) $ctrl.titleDesc = title['desc3'];
            setActiveProp(user.getUser().tv);
        }

        function stepLandLine() {
            $ctrl.data = dataChooser.getDataFromStep(4);
            calculateSmartPrice();
            var title = dataChooser.getData().landLineTitle;
            var titleKey = 'title' + crossroadsSelected;
            if (user.getServiceAccordingToStep().hasApi) titleKey += '_';
            $ctrl.title = title[titleKey];
            $ctrl.titleDesc = !user.hasAno() ? title['desc'] : title['desc2'];
            var userPlan = user.getUser().plan;
            if (userPlan.selected == 1 && userPlan.has == 6) $ctrl.titleDesc = title['desc3'];
            setActiveProp(user.getUser().landLine);
        }

        function stepMembers() {
            $ctrl.showSelectProgram = false;
            $ctrl.data = dataChooser.getDataFromStep(5);
            
            $ctrl.title = dataChooser.getData().membersTitle.title1;
            $ctrl.titleDesc = !user.hasAno() ? dataChooser.getData().membersTitle['desc'] : dataChooser.getData().membersTitle['desc2'];
            setAllMembers();
            //setActiveProp(user.getUser().members);
        }

        function setAllMembers() {
            var members = user.getServiceAccordingToStep().list;
            setNewPrices();

            for (var i = 0; i < 4; i++)
            {

                if (!angular.equals(members[i],{}))
                {
                    $ctrl.dataMembers[i] = $ctrl.data[members[i].id-1];
                    $ctrl.withPhoneMember[i] = members[i].withPhone;
                }
                else
                {
                    $ctrl.dataMembers[i] = null;
                }
            }
        }

        function setNewPrices() {
            var prices = calculate.getMagentaPrices();
            
            $ctrl.data.forEach(function(elem) {
                
                if (elem.id == 3) {
                    elem.newPrice = elem.price;
                    elem.newPriceWithoutPhone = elem.priceWithoutPhone;
                }
                else if (elem.id == 1 || (elem.id >= 7 && elem.id <= 10)) {
                    elem.newPrice = elem.price;
                    elem.newPriceWithoutPhone = elem.price;
                }
                else if (elem.id >= 4 &&  elem.id <= 6) {
                    elem.newPrice = prices.withPhone[elem.id-3];
                    elem.newPriceWithoutPhone = prices.withoutPhone[elem.id-3];  
                }
                else if (elem.id > 10) {
                    elem.newPrice = prices.withPhone[elem.id-3];
                    elem.newPriceWithoutPhone = prices.withoutPhone[elem.id-3];                      
                }
            });
        }

        function setActiveProp(dataByType) {
            resetDataActiveProp();
            var selectedDataByType = dataByType[getCrossroadsType()];
            
            var found = false;
            $ctrl.data.forEach(function(elem) {
                
                if (elem.id === selectedDataByType)
                {
                    elem.active = true;
                    found = true;
                }
            })

            if (!found)
            {
                $ctrl.data[0].active = true;
                dataByType[getCrossroadsType()] = $ctrl.data[0].id;
            }

        }

        function getCrossroadsType()
        {
            var result = '';
            if (crossroadsSelected == 1)
                result = 'has';
            else if (crossroadsSelected == 2)
                result = 'interested';
            else if (crossroadsSelected == 3)
                result = 'frendHas';

            return result;
        }

        function resetDataActiveProp() {

            //Je potrebne resetnut aj priamo v service
            dataChooser.resetActivePropForCurrentService();
            $ctrl.data.forEach(function(elem) {
                elem.active = false;
            });
        }

        /*
        if (!$rootScope.selectProduct) $rootScope.selectProduct = $rootScope.$on('selectProduct',
            function(event,args){
                $ctrl.selectProduct(args);
            });
        */

        $ctrl.selectProduct = function(product, popup) {
            addQTip();
            if ($ctrl.step != 5)
            {
                resetDataActiveProp();
                product.active = true;
                if (crossroadsSelected == 1)
                {
                    user.setService('has', product.id);
                }
                else if (crossroadsSelected == 2)
                {
                    user.setService('interested', product.id);
                }
                else if (crossroadsSelected == 3)
                {
                    user.setService('frendHas', product.id);
                }

                if ($ctrl.step == 1) {
                    
                    if (product.namePrefix == 'áno') {
                        user.setAno(true);
                    } else {
                        user.setAno(false);
                    }
                }

                user.updateLocalStorage();
                $rootScope.$broadcast('getDiscount');
                $rootScope.$broadcast('stepChanged');
            }
            else
            {
                product.active = true;
                $ctrl.dataMembers[selectedMember] = product;
                resetDataActiveProp();
                user.setMember(selectedMember,'id', product.id);
                user.setMember(selectedMember,'withPhone', $ctrl.withPhoneMember[selectedMember]);
                user.updateLocalStorage();
                //setNewPrices();
                setAllMembers();
                $rootScope.$broadcast('getDiscount');
            }

            if (popup) {
                $.fancybox.close();
            }
        }

        $ctrl.removeMember = function(member) {
            user.setMember((member), null, null);
            $ctrl.dataMembers[member] = null;
            $ctrl.selectMember(member);
            $ctrl.withPhoneMember[member] = true;
            setNewPrices();
            $rootScope.$broadcast('getDiscount');
        }

        $ctrl.selectMember = function(member) {

            angular.element('.members .select-box-area.select-box-area__active').removeClass('select-box-area__active');
            angular.element('.members .select-box-area').eq(member).addClass('select-box-area__active');
            selectedMember = member;

            if ($(window).width() > 991) {
                $ctrl.showSelectProgram = true;

                $timeout(function(){
                    $('#content .slide-select-box').removeClass (function (index, css) {
                        return (css.match (/(^|\s)slide-select-arrow-\S+/g) || []).join(' ');
                     }).addClass('slide-select-arrow-' + member);
                }, 100);

                angular.element('.slide-select-box__wrap').show();
            }
            else {
                angular.element('#popup_program_members .slide-select-box__wrap').show();
                $ctrl.showPopupProgram();
            }

        }

        //$rootScope.$on('gotoChooser', initThisComp);

        function initThisComp() {
            $ctrl.step = appConfig.getCurrStep();
            $ctrl.selected = user.getServiceAccordingToStep().selected;

            addQTip();

            $rootScope.$broadcast('getDiscount');
            crossroadsSelected = user.getServiceAccordingToStep().selected;
            switch($ctrl.step) {
                case 1:
                    stepPlan();
                    break;
                case 2:
                    stepInternet();
                    break;
                case 3:
                    stepTv();
                    break;
                case 4:
                    stepLandLine();
                    break;
                case 5:
                    stepMembers();
                    break;
            }
        }

        $ctrl.$onInit = function() {
            if (typeof $routeParams.param != 'undefined') {
                appConfig.setCurrStep(appConfig.getStepFromName($routeParams.param));
            } else {
                appConfig.setCurrStep(1);
                $location.url(APP_CONST.URL.CALC_CHOOSER + '/' + appConfig.getStepName(1));
            }

            
            if (typeof $routeParams.param2 != 'undefined' &&  $routeParams.param2.startsWith('ano-')) {
                user.setUserProps('plan', 'selected', 2);
            } else {
                for (var i=1; i <= appConfig.getCurrStep(); i++) {
                    if (user.getUserToStep(i).selected == 0) {
                        appConfig.setCurrStep(i);
                        $location.url(APP_CONST.URL.CALC_CROSSROADS + '/' + appConfig.getStepName(i));
                    }
                }
            }

            setSticks();
            initThisComp();

            var sticky_top = 70;

            if ($(window).width() < 800) {
                sticky_top = 44;
            }
            $('html,body').scrollTop(sticky_top + 1);

            setSticks(sticky_top);

            //$('.right').css({ 'left':'0', 'right':'0','margin-left': 'auto','margin-right': 'auto'});

            $( window ).scroll(function() {
                setSticks(sticky_top)
            });

            if (!appConfig.getShowHelp()) {
                appConfig.setShowHelp(true);
                $timeout(function(){
                    angular.element('.help-menu').show();
                    $timeout(function(){
                        angular.element('.help-menu__item').css('width', '200px');
                        if ($(window).width() < 900) {
                            $timeout(function(){
                                angular.element('.help-menu__item').css('width', '45px');
                            }, 2000);
                        }
                    }, 1000);
                }, 5000);
            }
            else {
                angular.element('.help-menu').show();
                if ($(window).width() > 992) {
                    angular.element('.help-menu__item').css('width', '200px');
                } else {
                    angular.element('.help-menu__item').css('width', '45px');
                }
            }
        };

        function setSticks(sticky_top) {
            var step_indicator = $('step-indicator');
            var step_controls = $('step-controls');
            var heightWindow = $(window).height();
            var widthWindows = $(window).width();
            var heightFooter = $('#footer').height();

            if (widthWindows < 992) {
                angular.element('.help-menu').css({ 'position': 'fixed', 'bottom': '90px','top': 'inherit' });
            }
            else {
                angular.element('.help-menu').css({ 'position': 'absolute', 'top': '80px', 'bottom': 'inherit' });
            }

            if ($(window).scrollTop() > sticky_top) {
                step_indicator.addClass("stick-top");
                if (widthWindows < 800) {
                    $('.phone-num').addClass('phone-num-2');
                }
                else {
                    $('.phone-num').removeClass('phone-num-2');
                }

                if (widthWindows > 991) {
                    angular.element('.help-menu').css({ 'position': 'fixed', 'top': '80px','bottom': 'inherit' });
                }
            }
            else {
                step_indicator.removeClass("stick-top");
                $('.phone-num').removeClass('phone-num-2');
            }

            var comp = $('#content .stick-cont');
            var height = 870;

            if (appConfig.getCurrStep() == 5) {
                if (widthWindows < 768) {
                    height = 1960;
                }
                else if (widthWindows < 800) {
                    height = 1150;
                }
                else if (widthWindows < 992) {
                    height = 1100;
                }
                else {
                    height = 850;
                }
            }

            comp.css('height', (height) + 'px');

            if (comp.length) {
                if (comp.offset().top + height - heightWindow < $(window).scrollTop()) {
                    step_controls.removeClass("stick-bottom");
                    if (widthWindows < 992) {
                        angular.element('.help-menu').css({ 'position': 'absolute' });
                    }
                }
                else {
                    step_controls.addClass("stick-bottom");
                    //comp.removeClass('stick-cont-2');
                }
            }
        }

        function addQTip(comp) {
            $timeout(function(){
                angular.forEach(angular.element('.circle'), function(value, key) {
                    var comp = angular.element(value);
                    comp.qtip({
                        content: comp.attr('alt'),
                        style: {
                            classes: 'qtip-tipsy'
                        },
                        position: {
                            corner: {
                                target: 'leftMiddle',
                                tooltip: 'topMiddle'
                            }
                        }
                    });                    
                });
            }, 100);
        }

        $ctrl.compare = function(prop, val, type){
            return function(item) {
                if (type == 'g')
                    return item[prop] > val;
                else
                    return item[prop] < val;
            }
        }

        $ctrl.$onChanges = function() { };
        $ctrl.$onDestroy = function() { };
    }
})();
