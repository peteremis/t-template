(function() {
    'use strict';

    angular
        .module('mag-1')
        .component('calcEntrance', {
            templateUrl: '../assets/js/components/calc-entrance/calc-entrance.component.html',
            controller: calcEntranceController,
            controllerAs: '$ctrl'
        });

    calcEntranceController.$inject = ['$rootScope','$http','$location', 'user', 'APP_CONST', 'appConfig', '$timeout'];

    function calcEntranceController($rootScope,$http, $location, user, APP_CONST, appConfig, $timeout) {
        var $ctrl = this;
        var token = null;   

        ////////////////

        $ctrl.redirect = function() {
            appConfig.setCalcInputVisibility(0);
            user.userReset();
            if ($ctrl.currentPage == 'landing') {
                $location.path(APP_CONST.URL.CALC_INTRO);
            }
            else {
                $location.path(APP_CONST.URL.CALC_CROSSROADS + '/vas-pausal');
            }
        }   

        $ctrl.sendForm = function() {
            if ($ctrl.showInput == 0) {
                if ($ctrl.currentPage == 'intro') {
                    if (user.getUserGroup() == 1) {
                        $location.path(APP_CONST.URL.CALC_CHOOSER + '/vas-pausal');
                    } else {
                        $location.path(APP_CONST.URL.CALC_CROSSROADS + '/vas-pausal');
                    }
                }
                else {
                    $location.path(APP_CONST.URL.CALC_INTRO);
                }
            }             
            else if ($ctrl.form.$valid) {
                $ctrl.showWait = true;
                
                if ( $ctrl.showInput == 1) {
                    $ctrl.showInput = 0;
                    
                    $http({
                        method : "GET",
                        url : APP_CONST.URL.API_SEND_SMS,
                        params: { msisdn: $ctrl.msid.substr(1) }
                    }).then(function mySuccess(response) {
                        $ctrl.showWait = false;
                        if (response.data.status == 'ok') {
                            token = response.data.token;
                            
                            $ctrl.showInput = 2;
                            $ctrl.inputLabel = 'Zadajte kód z SMS'; 
                            $ctrl.buttonText = 'ODOSLAŤ SMS KÓD';
                            setTimeout(function() {
                                $('#form_smscode').focus();
                            }, 100);
                        } else {
                            $ctrl.showPopup('web_service_error', null);                            
                        }
                    }, function myError(response) {
                        $ctrl.showWait = false;
                        $ctrl.showPopup('web_service_error', null);
                    });

                }
                else if ( $ctrl.showInput == 2) {
                    appConfig.setCalcInputVisibility(0);
                    $ctrl.showInput = 0;
                    $http({
                        method : "GET",
                        url : APP_CONST.URL.API_CHECK_CODE,
                        params: { 'token': token, 'code': $ctrl.smscode }
                    }).then(function mySuccess(response) {
                        $ctrl.showWait = false;
                        if (response.data.magenta1) {
                            var status = response.data.magenta1;

                            /*
                            if (status == 'COMPANY-IN-OTHER-COMMUNITY') {
                                status = 'success';
                                response.data.plan='Áno L';
                            }
                            */
                            
                            if (status == 'success') {                                            
                                user.normalizeUser(response.data);
                                
                                if ($ctrl.currentPage == 'landing') {
                                    $location.path(APP_CONST.URL.CALC_INTRO);                   
                                }
                                else {
                                    if (user.getUserGroup() == 1) {
                                    $location.path(APP_CONST.URL.CALC_CHOOSER + '/vas-pausal');
                                    } else {
                                        $location.path(APP_CONST.URL.CALC_CROSSROADS + '/vas-pausal');
                                    }                    
                                }
                            }
                            else {
                                user.userReset();
                                $ctrl.showInput = 1;
                                appConfig.setCalcInputVisibility(1);
                                $ctrl.inputLabel = 'Zadajte svoje Telekom telefónne číslo';
                                setTimeout(function() {
                                    $('#form_msid').focus();
                                }, 100);
                                
                                if (status == 'unknown_number') {
                                    $ctrl.showPopup('phone_not_t', null);
                                } else if (status == 'ALREADY-COMMUNITY-MEMBER') {
                                    $ctrl.showPopup('phone_has_m1', null);
                                } else if (status == 'COMPANY-IN-OTHER-COMMUNITY') {
                                    $ctrl.showPopup('phone_not_use', null);                                    
                                } else {
                                    $ctrl.showPopup('phone_not_code', null);    
                                }
                                $timeout(function(){
                                    $ctrl.showWait = false;
                                }, 500);                                                
                            }
                        } else {
                            $ctrl.showInput = 2;
                            setTimeout(function() {
                                $('#form_smscode').focus();
                                $ctrl.showPopup('sms_code_error', null);
                            }, 100);
                        }
                    }, function myError(response) {
                        $ctrl.showWait = false;
                        $ctrl.showPopup('web_service_error', null);
                    }).then(function(response) {
                        //complete
                    });
                }
            }
          
        }
        
        $ctrl.showPopup = function(popupId, data) {
            appConfig.setPopup(popupId, data);
            $rootScope.$broadcast('showPopup');
        }        

        $ctrl.$onInit = function() { 
            $ctrl.showInput = appConfig.getCalcInputVisibility();
            $ctrl.currentPage = appConfig.getPageType();
            $ctrl.buttonText = 'ZISTIŤ MOJE VÝHODY';
            $ctrl.inputLabel = 'Zadajte svoje Telekom telefónne číslo';
            $ctrl.showWait = false;
        };
        $ctrl.$onChanges = function() { };
        $ctrl.$onDestroy = function() { };
    }
})();