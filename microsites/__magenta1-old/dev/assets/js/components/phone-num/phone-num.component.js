(function() {
    'use strict';

    angular
        .module('mag-1')
        .component('phoneNum', {
            templateUrl: '../assets/js/components/phone-num/phone-num.component.html',
            controller: phoneNumController,
            controllerAs: '$ctrl'
        });

    phoneNumController.$inject = ['user','appConfig','APP_CONST','$location'];
    function phoneNumController(user,appConfig,APP_CONST,$location) {
        var $ctrl = this;
        $ctrl.msid = '';

        ////////////////

        $ctrl.changeNum = function() {
            appConfig.setCurrStep(1);
            $location.path(APP_CONST.URL.CALC_INTRO);
        };

        $ctrl.$onInit = function() {
            $ctrl.msid = user.getUserMsid();
        };

        $ctrl.$onChanges = function() { };
        $ctrl.$onDestroy = function() { };
    }
})();