(function() {
    'use strict';

    angular
        .module('mag-1')
        .component('stepControls', {
            templateUrl: '../assets/js/components/step-controls/step-controls.component.html',
            controller: stepControlsController,
            controllerAs: '$ctrl'
        });

    stepControlsController.$inject = ['$route','$location', '$rootScope','appConfig', 'APP_CONST', 'user','calculate'];
    function stepControlsController($route,$location, $rootScope, appConfig, APP_CONST, user, calculate) {
        var $ctrl = this;
        $ctrl.nextText = 'POKRAČOVAŤ';
        var currPageType = '';
        var currStep = 0;

        ////////////////

        $ctrl.prev = function() {
            currStep = appConfig.getCurrStep();
            currPageType = appConfig.getPageType();
            var userPrev =  null;

            if (currStep > 1)
                userPrev = user.getUserToStep(currStep-1);

            if (currPageType === 'crossroads')
            {
                if (currStep > 1)
                {
                    appConfig.decreaseStep();
                    if (userPrev.selected == 4) {
                        //$rootScope.$broadcast('stepControlClicked');
                        //$route.reload();
                        $location.url(APP_CONST.URL.CALC_CROSSROADS + '/' + appConfig.getStepName(appConfig.getCurrStep()));            
                        return;
                    }
                    else {
                        //$location.path(APP_CONST.URL.CALC_CHOOSER);
                        $location.url(APP_CONST.URL.CALC_CHOOSER + '/' + appConfig.getStepName(appConfig.getCurrStep()));            
                        return;
                    }
                }
            }
            else
            {
                $location.url(APP_CONST.URL.CALC_CROSSROADS + '/' + appConfig.getStepName(appConfig.getCurrStep()));            
                return;
            }
        }

        $ctrl.next = function() {
            currStep = appConfig.getCurrStep();
            currPageType = appConfig.getPageType();

            var userStep = user.getUserToStep(currStep);

            if (currPageType == 'crossroads')
            {
                if ((currStep < 5 && userStep.selected > 0 && userStep.selected < 4)
                    || (currStep == 5 && userStep.selected == 1)) {
                    $location.url(APP_CONST.URL.CALC_CHOOSER + '/' + appConfig.getStepName(appConfig.getCurrStep()));
                    return;
                }
                else {
                    if (currStep < 5) {
                        user.setService('selected', 4);
                        user.updateLocalStorage();
                        appConfig.increaseStep();
                        $location.url(APP_CONST.URL.CALC_CROSSROADS + '/' + appConfig.getStepName(appConfig.getCurrStep()));
                        //$route.reload();
                        return;
                    }
                    else {
                        user.setService('selected', 3);
                        user.updateLocalStorage();
                        $location.url(APP_CONST.URL.SUMMARY);
                        return;                        
                    }
                }                    
            }
            else if (currPageType == 'chooser') 
            {
                if (currStep < 5) {
                    var userNextStep = user.getUserToStep(currStep + 1);
                    appConfig.increaseStep();
                    if (currStep < 4 && userNextStep.selected == 1 && user.isKnownUser()) {
                        //$rootScope.$broadcast('gotoChooser');
                        $location.url(APP_CONST.URL.CALC_CHOOSER + '/' + appConfig.getStepName(appConfig.getCurrStep()));
                        //$route.reload();
                        return;
                    }
                    else {
                        //$location.path(APP_CONST.URL.CALC_CROSSROADS);
                        $location.url(APP_CONST.URL.CALC_CROSSROADS + '/' + appConfig.getStepName(appConfig.getCurrStep()));    
                        return;
                    }
                }
                else {
                    $location.url(APP_CONST.URL.SUMMARY);
                    return;
                }
            }
        }
        
        if (!$rootScope.getDiscount) $rootScope.getDiscount = $rootScope.$on('getDiscount', getDiscount);
        //$rootScope.$on('getDiscount',getDiscount);

        function getDiscount() {
            var step = appConfig.getCurrStep();

            if (step == 1 && appConfig.getPageType() == 'crossroads') {
                $rootScope.magenta1 = {
                    discount: 0,
                    data: 0
                };
                return;
            }
            else {
                var tmpDiscount;
                if (appConfig.getPageType() == 'chooser') {
                    tmpDiscount = calculate.getDiscount(step);
                }
                else {
                    tmpDiscount = calculate.getDiscount(step - 1);
                }
                var tmpData = 0;
                if (tmpDiscount.anoData > 0) {
                    tmpData = (tmpDiscount.anoData < 1 ? (tmpDiscount.anoData * 1000) + ' MB' : tmpDiscount.anoData + ' GB');
                    if (tmpDiscount.plan_name == 'ÁNO M Dáta' || tmpDiscount.plan_name == 'ÁNO S') tmpData += ' + 100 min';
                }
                $rootScope.magenta1 = {
                    discount: tmpDiscount.discount_all,
                    data: tmpData
                };

            }

            $ctrl.selectText = getSelectText(step);
        }

        $ctrl.$onInit = function() {
            currPageType = appConfig.getPageType();
            $rootScope.magenta1 = {
                discount: 0,
                data: 0
            };
            getDiscount();
            var step = appConfig.getCurrStep();

            if ((currPageType === 'chooser' && step == 5)
                || (currPageType === 'crossroads' && step == 5 && user.getUserToStep(5).selected != 1)) { 
                $ctrl.nextText = 'ZHRNUTIE';
            }
        };

        function getSelectText(step) {
            var userData = user.getUser();
            var textArray = [];
            var result = '';
            if (appConfig.getPageType() === 'crossroads')
                step--;

            switch (step) {
                case 5:
                case 4:
                    if (userData.landLine.selected < 4)
                        textArray.push('Pevná linka');
                case 3:
                    if (userData.tv.selected < 4)
                        textArray.push('TV');
                case 2:
                    if (userData.internet.selected < 4)
                        textArray.push('Internet');
                case 1:
                    if (userData.plan.selected < 4)
                        textArray.push('Váš paušál');
            }
            if (textArray.length > 0) {
                textArray.reverse();
                angular.forEach(textArray,function(value, key) {
                    if (key > 0)
                        result += ' + ';
                    result += value;
                });
            }
            return result;
        }

        $ctrl.$onChanges = function() { };
        $ctrl.$onDestroy = function() { };
    }
})();