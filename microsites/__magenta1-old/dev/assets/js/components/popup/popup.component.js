(function() {
    'use strict';

    angular
        .module('mag-1')
        .component('popup', {
            templateUrl: '../assets/js/components/popup/popup.component.html',
            controller: popupController,
            controllerAs: '$ctrl'
        });

    popupController.$inject = ['appConfig', '$rootScope', '$timeout', '$location', 'APP_CONST', 'localStorage'];
    function popupController(appConfig, $rootScope, $timeout, $location, APP_CONST, localStorage) {
        var $ctrl = this;
        $ctrl.template = '';
        $rootScope.popupData = null;
        $ctrl.step = 0;
        $ctrl.showError = null;

        ////////////////

        if (!$rootScope.showPopup) $rootScope.showPopup = $rootScope.$on('showPopup', showPopup);

        //$rootScope.$on('showPopup', showPopup222);

        function showPopup() {

            var popup_data = appConfig.getPopup();
            $rootScope.popupData = popup_data.data;

            $ctrl.step = appConfig.getCurrStep();

            var popupWidth = 312;
            var windowHeihgt = $(window).height();

            //popup phone_not_t
            if (popup_data.popupId == "phone_not_t") {
                if (windowHeihgt < 521) {
                    $('#phone_not_t .nott-content').css('height', windowHeihgt - 40 - 18 - 40);
                }
            }

            //popup phone_not_use
            if (popup_data.popupId == "phone_not_use") {
                if (windowHeihgt < 505) {
                    $('#phone_not_use .notuse-content').css('height', windowHeihgt - 40 - 68 - 40);
                }
            }

            //popup_detail
            if (popup_data.popupId == "popup_detail") {
                popupWidth = 460;
                if ($ctrl.step == 1 || $ctrl.step == 5) {
                    if (windowHeihgt < 678) {
                    $('#popup_detail .detail-content').css('height', windowHeihgt - 190);
                    }

                    if (popup_data.data.id == 0) {
                        $rootScope.popupData.callOut = 'Volania a SMS';
                    }
                    else {
                        $rootScope.popupData.callOut = "Odchádzajúce volania";
                    }
                }
                else {
                    popup_data.popupId = 'popup_detail_2';
                }
            }

            //popup summary_link
            if (popup_data.popupId == "summary_link") {
                 popupWidth = 420;

                if (windowHeihgt < 575) {
                    $('#summary_link .summary-content').css('height', windowHeihgt - 40 - 65 - 40);
                }
            }

            //popup poplatky
            if (popup_data.popupId == "poplatky") {
                popupWidth = 460;

                if (windowHeihgt < 472) {
                    $('#poplatky .pojdeto_content').css('height', windowHeihgt - 40 - 68 - 40);
                }
            }

            $.fancybox.open([{
                href: '#' + popup_data.popupId,
                padding: 0,
                margin: 0,
                scrolling: 'no',
                closeBtn: false,
                parent: "#content",
                width: popupWidth,
                maxWidth: 500,
                height: 'auto',
                autoSize: false,
                helpers: {
                    overlay: {
                        css: {
                            'background': 'rgba(0, 0, 0, 0.7)'
                        }
                    }
                }
            }]);
        }

        $ctrl.reload = function(val) {
            $.fancybox.close();
            var pageType = appConfig.getPageType();
            if  (val == 1) {
                appConfig.setCalcInputVisibility(0);
                if (pageType == 'intro')
                    val = 2;
            }

            switch (val) {
                case 1:
                    $location.url(APP_CONST.URL.CALC_INTRO);
                    break;
                case 2:
                    $location.url(APP_CONST.URL.CALC_CROSSROADS);
                    break;                    
            }
        }

        $ctrl.sendForm = function(compId) {
            //angular.element('form.dynamicFormMainForm .formM1Calc-subject .defaultValueField').val('Test3');
            //angular.element('form.dynamicFormMainForm .formM1Calc-email .defaultValueField').val('andrej.novota@telekom.sk');
            //angular.element('form.dynamicFormMainForm .formM1Calc-text .defaultValueField').val(angular.element('#' + compId).val());
            
            localStorage.setData('m1c_form_url', $location.absUrl().split('#')[1]);
            if (compId != 'email') {
                var inputVal = angular.element('#' + compId).val();
                $ctrl.showError = checkInputVal(inputVal);
                if ($ctrl.showError == null) {
                    formChange('form.dynamicFormMainForm .formM1Calc-subject', 'CallMeBack_others:' + inputVal + ': Magenta 1 kalkulacka – Potrebujem pomoc');
                    formChange('form.dynamicFormMainForm .formM1Calc-email', APP_CONST.MAIL.LEAD);
                    formChange('form.dynamicFormMainForm .formM1Calc-text', 'Magenta1-kalkulacka');
                    angular.element('.dynamicFormPages .finishFormButton').triggerHandler('click');
                }
            } else {
                var mailVal = angular.element('#send_email').val();
                $ctrl.showError = checkMailVal(mailVal);
                if ($ctrl.showError == null) {
                    var textVal = 'Vážený zákazník,'
                        + '<br><br>'
                        + 'posielame Vám link na výpočet Vašej Magenta 1 skupiny: '
                        + '<br>'
                        +  $rootScope.popupData.urlSave
                        + '<br><br>'
                        + 'S výberom služieb a vytvorením Magenta 1 skupiny Vám radi pomôžeme.'
                        + '<br>'
                        + 'Kliknite na <a href="' + $rootScope.popupData.urlSave + '">link</a>, potom na tlačidlo „Mám záujem“ a nechajte nám svoj kontakt. Ozveme sa Vám.'
                        + '<br><br>'
                        + 'Link je platný po dobu 30 dní.'
                        + '<br><br><br>'
                        + 'Želáme pekný deň.'
                        + '<br><br>'
                        + 'Slovak Telekom, a.s'
                        + '<br>'
                        + '<a href="https://www.telekom.sk">www.telekom.sk</a>'
                        + '<br><br><br>'
                        + '<span style="font-style: italic">Tento e-mail Vám bol zaslaný na vyžiadanie. Prosíme, neodpovedajte naň, bol vygenerovaný automaticky. Pokiaľ ste o zaslanie linku na výpočet Magenta 1 skupiny nežiadali, tento email prosím ignorujte.</span>'
                    formChange('form.dynamicFormMainForm .formM1Calc-subject', 'Vaša Magenta 1 skupina - Pozrite sa, čo môžete získať');
                    formChange('form.dynamicFormMainForm .formM1Calc-email', mailVal);
                    formChange('form.dynamicFormMainForm .formM1Calc-text', textVal);
                    angular.element('.dynamicFormPages .finishFormButton').triggerHandler('click');                    
                }
            }
        }

        function formChange(inputName, value) {
            $(inputName)
                .find('input').val(value).blur().show().end()
                .find('label').show().end()
                .closest('li').show().end();
        }        

        function checkInputVal(inputVal) {
            var result = null;

            if (inputVal.length < 10) {
                result = 'Minimálny počet znakov je 10!';
            } else if (parseInt(inputVal) != inputVal) {
                result = 'Musíte zadať číslo';
            }
            
            return result;
        }

        function checkMailVal(mail) {
            var result = null;

            var filter = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i
            if (!filter.test(mail))
                result = 'Nesprávny formát emailovej adresy';

            return result;
        }

        $ctrl.closePopup = function() {
            $timeout(function(){
                $.fancybox.close();
            }, 100);
        }

        $ctrl.toggle = function(id) {
            $(id).closest('.togleArrow').find('.arrow-content').toggle(200);
            $(id).find('.arrow-right').toggleClass('arrow-rotate');        
        }

        $ctrl.$onInit = function() {
            var template = appConfig.getPageType();

            if (template == 'intro')
                template = 'landing';
            else if (template == 'crossroads')
                template = 'chooser';

            $('.popup-cont .toggle-arrow, .popup-cont .arrow-right').click(function(event) {
                event.preventDefault();
                if ($(this).hasClass('arrow-right')) {
                    return;
                }
                $(event.target).closest('.togleArrow').find('.arrow-content').toggle(200);
                
                $(this).find('.arrow-right').toggleClass('arrow-rotate');
            });                            

            $ctrl.template = '../assets/js/components/popup/templates/popup_' + template + '.html';
        };

        $ctrl.$onChanges = function() { };
        $ctrl.$onDestroy = function() { };
    }
})();
