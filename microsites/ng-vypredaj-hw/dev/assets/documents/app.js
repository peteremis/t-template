var PrintingSolutionApp = angular.module('PrintingSolutionApp', ['ui.router', 'angularUtils.directives.dirPagination', 'ngAnimate'])
    //custom filter for multiple keywords search
    .filter('searchFilter', function($filter) {

        return function(inputArray, searchText, booleanOp) {
            booleanOp = booleanOp || 'AND';

            var searchTerms = (searchText || '').toLowerCase().split(/\s+/);

            if (booleanOp === 'AND') {
                var result = inputArray;
                searchTerms.forEach(function(searchTerm) {
                    result = $filter('filter')(result, searchTerm);
                });

            } else {
                var result = [];
                searchTerms.forEach(function(searchTerm) {
                    result = result.concat($filter('filter')(inputArray, searchTerm));
                });
            }

            return result;
        };
    });
//filter to capitalize first letter
PrintingSolutionApp.filter('capitalize', function() {
    return function(input) {
        return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
    }
});

PrintingSolutionApp.config(['$stateProvider', '$urlRouterProvider', registerRoutes]);
PrintingSolutionApp.run(['$rootScope', '$location', function($rootScope, $location) {
    $rootScope.$location = $location;

}]);

// $scope.sendCategory = function(category) {
//         return (category.toLowerCase().replace(/\s+/g, '-'));
//     };
function registerRoutes($stateProvider, $urlRouterProvider) {

    // root authenticated state
    $stateProvider
        .state('home', {
            url: '/',
            //article content page
            views: {
                "viewB": {
                    templateUrl: 'templates/category-grid.html',
                    controller: function($scope, $state) {
                        $scope.page = "Home";
                    }
                }
            }
        })
        .state('categoryPage', {
            url: '/category/:categoryName',
            views: {
                "viewA": {
                    templateUrl: "templates/page-title-category.html",
                    controller: function($scope, $stateParams) {
                        $scope.categoryName = $stateParams.categoryName;
                    }
                },
                "viewB": {
                    templateUrl: 'templates/category-grid.html',
                    controller: function($scope, $stateParams) {
                        $scope.categoryName = $stateParams.categoryName;
                    }
                }
            }
        })
        .state('articlePage', {
            url: '/articles/:articleYear/:articlePageName',
            views: {
                "viewA": {
                    templateUrl: "templates/page-title-article.html",
                    controller: function($scope, $stateParams) {
                        prettyUrl = $stateParams.articlePageName.toLowerCase().replace(/\s+/g, '-');
                        $scope.articleTitleUrl = '#/articles/' + $stateParams.articleYear + '/' + prettyUrl;
                    }
                },
                "viewB": {
                    templateUrl: function($stateParams) {
                        prettyUrl = $stateParams.articlePageName.toLowerCase().replace(/\s+/g, '-');
                        articleDetailPageUrl = 'articles/' + $stateParams.articleYear + '/' + prettyUrl;
                        return articleDetailPageUrl;
                    },
                    controller: function($scope, $state) {
                        $scope.page = "Article";
                    }
                }
            }
        })
        .state("searchResults", {
            url: "/search-results/:searchText",
            views: {
                "viewA": {
                    templateUrl: "templates/page-title-search.html",
                    controller: function($scope, $state, $stateParams) {
                        $scope.page = "Search Results";
                        $scope.searchText = $stateParams.searchText;
                    }
                },
                "viewB": {
                    templateUrl: "templates/search-results.html",
                    controller: function($scope, $state, $stateParams) {
                        $scope.page = "Search Results";
                        $scope.searchText = $stateParams.searchText;
                    }
                }
            }
        })
    $urlRouterProvider.otherwise('/');
}


//default controler from samsungprinters.com on article detail page
PrintingSolutionApp.controller('AppCtrl', function($scope, $http) {

});


PrintingSolutionApp.controller('PrintAppCtrl', function($scope, $http, $state) {
    $scope.printerItems = [];
    //load json data
    $http.get('./data/post-data.json').success(function(response) {
        $scope.printerItems = response;
    });

    $scope.filters = {};

    //fn reset filter array
    $scope.resetFilters = function() {
        $scope.filters = {};
        $scope.categoryName = {};
    };

    $state.transitionTo("home");
    //fn search load search result page after click
    $scope.search = function() {
        $state.transitionTo("searchResults", {
            searchText: $scope.searchText
        }, {
            reload: true,
            notify: true
        });

    };

    //fn for lower case category title and replate space with dash(-)
    $scope.sendCategory = function(category) {
        return (category.toLowerCase().replace(/\s+/g, '-'));
    };
    //fn for scroll to top 
    $scope.scrollTop = function() {
        var scrollDuration = 0.5;

        const scrollHeight = window.scrollY,
            scrollStep = Math.PI / (scrollDuration * 60),
            cosParameter = scrollHeight / 2;

        var scrollCount = 0,
            scrollMargin,
            scrollInterval = setInterval(function() {
                if (window.scrollY != 0) {
                    scrollCount = scrollCount + 1;
                    scrollMargin = cosParameter - cosParameter * Math.cos(scrollCount * scrollStep);
                    window.scrollTo(0, (scrollHeight - scrollMargin));
                } else clearInterval(scrollInterval);
            }, 15);
    };

    //auto scroll on top on category and page change
    $scope.$watchGroup(['printerItems.articles','artTitle.title'], function() {
        var scrollDuration = 0.5;

        const scrollHeight = window.scrollY,
            scrollStep = Math.PI / (scrollDuration * 60),
            cosParameter = scrollHeight / 2;

        var scrollCount = 0,
            scrollMargin,
            scrollInterval = setInterval(function() {
                if (window.scrollY != 0) {
                    scrollCount = scrollCount + 1;
                    scrollMargin = cosParameter - cosParameter * Math.cos(scrollCount * scrollStep);
                    window.scrollTo(0, (scrollHeight - scrollMargin));
                } else clearInterval(scrollInterval);
            }, 15);
    });

});
