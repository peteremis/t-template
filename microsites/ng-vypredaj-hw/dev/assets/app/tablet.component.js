'use strict';

// Register `tabletSlider` component, along with its associated controller and template
angular.
module('tabletList').
component('tabletList', {
    templateUrl: '../assets/app/tablet.template.html',
    controller: ['$resource', '$timeout', 'deviceService', '$location',
        function tabletListController($resource, $timeout, deviceService, $location,) {
            var self = this;

            console.log('tablet directive componnetn');

            self.tabletAll = [];
            /* Load Tablets */
            deviceService.getAllTablets().then(function(device) {
                self.tabletAll = device;
                // console.log('tablety', self.tabletAll);
                $timeout(function() {
                    self.scrollTo();
                });
            });

            //scrollTo from url hash
            self.scrollTo = function() {
                var $target = $('#' + $location.$$hash);
                if ($target.length > 0) {
                    var scrollPos = $target.offset().top;
                    $("body,html").animate({ scrollTop: scrollPos }, "slow");
                }
            };

            // INIT
            self.init = function() {
                //wait until tab2 is loaded
                $timeout(function() {
                    //self.notebooksUpdate();
                }, 500);
                //console.log('test nb init');
            };
            // runs once per controller instantiation
            self.init();
        }
    ]
});
