'use strict';

// Define the `vypredajHwApp` module
var app = angular.module('vypredajHwApp', [
  'ngAnimate',
  'ngRoute',
  'core',
  'deviceList',
  'deviceDetail',
  'phoneList',
  'tabletList',
  'angularModalService',
  'isteven-multi-select',
]);


