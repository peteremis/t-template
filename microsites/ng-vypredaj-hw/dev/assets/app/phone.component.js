'use strict';

// Register `tabletSlider` component, along with its associated controller and template
angular.
module('phoneList').
component('phoneList', {
    templateUrl: '../assets/app/phone.template.html',
    controller: ['PhoneService', '$resource', '$timeout',
        function phoneListController(PhoneService, $resource, $timeout) {
            var self = this;

            console.log('phone directive component');

            // INIT
            self.init = function() {
                //wait until tab2 is loaded
                $timeout(function() {
                    //self.notebooksUpdate();
                }, 500);
                //console.log('test nb init');
            };
            // runs once per controller instantiation
            self.init();
        }
    ]
});
