'use strict';

angular.
module('vypredajHwApp').
config(['$locationProvider', '$routeProvider',
    function config($locationProvider, $routeProvider) {
        $locationProvider.hashPrefix('!');

        $routeProvider.
        when('/vyber-hw', {
            template: '<device-list></device-list>',
        }).
        when('/vyber-hw/tablet/:tabletId', {
            template: '<device-detail></device-detail>',
            resolve: {
                tablet: ['$route', 'deviceService', '$location', function($route, deviceService, $location) {
                    deviceService.getTablet({ deviceId: $route.current.params.tabletId }).then(function(tablet) {
                        var tabletItem = tablet;
                        if (angular.isUndefined(tabletItem)) {
                            $location.path('/vyber-hw');
                        } else {
                            //console.log(tabletItem);
                            if (tabletItem.visible) {
                                return tabletItem
                            } else{
                                 $location.path('/vyber-hw');
                            }
                            
                        }
                    });
                }]
            }
        }).
        when('/vyber-hw/:deviceId', {
            template: '<device-detail></device-detail>',
            resolve: {
                phone: ['$route', 'deviceService', '$location', function($route, deviceService, $location) {
                    deviceService.getDevice({ deviceId: $route.current.params.deviceId }).then(function(phone) {
                        var phoneItem = phone;
                        if (angular.isUndefined(phoneItem)) {
                            $location.path('/vyber-hw');
                        } else {
                            //console.log('found phoneItem', phoneItem);
                            if (phoneItem.visible) {
                                return phoneItem
                            } else {
                                 $location.path('/vyber-hw');
                            }
                        }
                    });
                }]
            }
        }).
        otherwise('/vyber-hw');
    }
]).
run(['$rootScope', '$location', function($rootScope, $location) {
    $rootScope.$on('$routeChangeError', function(event, current, previous, error) {
        console.log('e::::', event, current, previous, error);
        if (error.status === 404) {
            $location.path('/vyber-hw');
        }
    });
}]);
