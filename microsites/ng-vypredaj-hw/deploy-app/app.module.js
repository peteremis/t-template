'use strict';

// Define the `vypredajHwApp` module
var app = angular.module('vypredajHwApp', [
  'ngAnimate',
  'ngRoute',
  'core',
  'deviceList',
  'deviceDetail',
  'angularModalService',
  'isteven-multi-select',
]);


