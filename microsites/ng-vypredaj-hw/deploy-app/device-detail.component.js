'use strict';

angular.
module('deviceDetail').
component('deviceDetail', {
    templateUrl: '/documents/10179/10521551/device-detail.template.html',
    controller: ['$routeParams', 'deviceService',
        function DeviceDetailController($routeParams, deviceService) {
            var self = this;
            self.phone = {};
            self.tablet = {};
            self.easyOneTimePrice = 0;
            self.easyMontPrice = 0;

            self.programOneTimePrice = 0;
            self.programMonthPrice = 0;

            self.programPrice = 5.99;
            self.easyPrice = 1;
            self.deviceType = "Telefón";
            self.deviceTypeTab = "telefónu";

            deviceService.getDevice({ deviceId: $routeParams.deviceId }).then(function(device) {
                if (!angular.isUndefined(device)) {
                    self.device = device;
                    self.easyOneTimePrice = self.easyPrice + device.price.easy;
                    self.programOneTimePrice = device.price.xsMiniOne;
                    self.programMonthPrice = self.programPrice + device.price.xsMiniMonth;
                } else {
                    // if phone id not found try find tablet
                    self.getTabletService();
                }
            });

            self.getTabletService = function() {
                deviceService.getTablet({ deviceId: $routeParams.tabletId }).then(function(device) {
                    self.device = device;
                    self.deviceType = "Tablet";
                    self.deviceTypeTab = "tabletu";
                    self.easyOneTimePrice = self.easyPrice + device.price.easy;
                    self.programOneTimePrice = device.price.xsMiniOne;
                    self.programMonthPrice = self.programPrice + device.price.xsMiniMonth;
                });
            };

            self.pclose = function(data) {
                $.fancybox.close();
                //console.log(':::popup close:::');
            };
 
            //scrollTop on detail page load
            $('html, body').animate({ scrollTop: 0 }, 500);
        }
    ]
});
