'use strict';

angular.
module('core.device').
factory('PhoneService', ['$resource',
    function($resource) {
        return $resource('/documents/10179/10521248/:deviceId.json', {}, {
            query: {
                method: 'GET',
                params: { deviceId: 'phone-list' },
                isArray: true
            }
        });
    }
]);


angular.module('core.device').service('deviceService', function($http, $resource) {
    var service = {
        getAllDevice: function() {
            return $http.get('/documents/10179/10521248/phone-list.json', { cache: true }).then(function(resp) {
                return resp.data;
            });
        },
        getAllTablets: function() {
            return $http.get('/documents/10179/10521248/tablet-list.json', { cache: true }).then(function(resp) {
                return resp.data;
            });
        },
        getDevice: function(id) {
            //console.log('id service phone', id,!angular.isUndefined(id));
            function deviceMatchesParam(device) {
                return device.deviceId === id.deviceId;
            }
            if (!angular.isUndefined(id)) {
                // console.log('id',id);
                return service.getAllDevice().then(function(device) {
                    return device.find(deviceMatchesParam)
                });
            }
        },
        getTablet: function(id) {

            function deviceMatchesParam(device) {
                return device.deviceId === id.deviceId;
            }

            if (!angular.isUndefined(id)) {
                // console.log('id service', angular.isUndefined(id.deviceId));
                return service.getAllTablets().then(function(device) {
                    return device.find(deviceMatchesParam)
                });
            }
        }
    }

    return service;
})
