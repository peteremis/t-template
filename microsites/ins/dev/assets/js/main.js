$(function() {
    var address = '',
    addressType = '',
    lat = '',
    long = '',
    internet = '',
    tv = '',
    date = '',
    marker = '',
    availability = '',
    $cityForm = $('#city-form'),
    $cityInput = $('.city-input'),
    $cityResultList = $('.city-form__result_list'),
    $streetForm = $('#street-form'),
    $streetInput = $('.street-input'),
    $streetResultList = $('.street-form__result_list'),
    $numberInput = $('.number-input'),
    $btn = $('.sendReq'),
    $errorMsg = $('.error-msg'),
    $infoMsg = $('.info-msg'),
    $mapHolder = $('.map-holder'),
    $resetBtn = $('.reset'),
    isMapInitialised = false,
    hasStreet = true,
    cityId = '',
    streetId = '',
    isInfoVisible = false,
    isErrorVisible = false,
    boxElem = '',
/*
    URL_CITY = 'https://backvm.telekom.sk/shop/ins/?searchTermKey=citySubstring',
    URL_STREET = 'https://backvm.telekom.sk/shop/ins/?searchTermKey=streetSubstring',
    URL_FINAL = 'https://backvm.telekom.sk/shop/ins/?searchTermKey=streetNoSubstring',
*/
    URL_CITY = 'https://backvm.telekom.sk/shop/ins/index2.php?searchTermKey=citySubstring',
    URL_STREET = 'https://backvm.telekom.sk/shop/ins/index2.php?searchTermKey=streetSubstring',
    URL_FINAL = 'https://backvm.telekom.sk/shop/ins/index2.php?searchTermKey=streetNoSubstring',
    MIN_KEY_DOWN = 3,
    MSG_CANT_CONNECT = 'Nepodarilo sa načítať informácie, skúste to prosím ešte raz.',
    MSG_CANT_FIND_ADDRESS = 'Na zadanej adrese sme nenašli plánované pripojenia. Pre ďalšie overenie pokračujte na túto <a href="/chytry-balik">stránku</a>.',
    // API_KEY = 'AIzaSyD4BHKV9ijIn6nCRo8QBbFfW3eepI3LAl4';
    API_KEY = 'AIzaSyCA_2lhYPA0npRr9MlyRWpN_mIzr8AcYdU';


    function hideList($input) {
        var $tempParent = $input.next('.results');
        $tempParent.hide();
    }

    function showList($input) {
        var $tempParent = $input.next('.results');
        $tempParent.show();
    }

    function emptyElem($elem) {
        $($elem).html('');
    }

    function addTextToInput($input, text) {
        $input.val(text);
    }

    function clearInput($input) {
        $input.val('');
    }

    function showErrorMsg(msg) {
        $errorMsg.text(msg);
        isErrorVisible = true;
    }

    function hideErrorMsg() {
        $errorMsg.text('');
        isErrorVisible = false;
    }

    function showInfoMsg(msg) {
        $infoMsg.html(msg);
        isInfoVisible = true;
    }

    function hideInfoMsg() {
        $infoMsg.html('');
        isInfoVisible = false;
    }

    function hideMap() {
        $mapHolder.hide();
    }

    function showMap() {
        $mapHolder.show();
    }

    function preventSubmitOnEnterPress($input) {
        $input.keypress(function(e) {
            var code = (e.keyCode ? e.keyCode : e.which);
            if (code == 13) {
                return false;
            }
        });
    }

    function clearResultOnArrowKey($input, $result) {
        $input.keydown(function(e) {
            var code = (e.keyCode ? e.keyCode : e.which);
            if (code == 38 || code == 40) {
                emptyElem($result);
            }
        });
    }

    function resetOnBackSpace($input, resetCityInput, resetStreetInput) {
        $input.on('keydown', function(e) {
            if (e.which == 8 || e.which == 46) {
                if (resetCityInput) {
                    resetFields();
                } else if (resetStreetInput) {
                    clearInput($numberInput);

                    emptyElem($streetResultList);

                    switchInputState($numberInput, true);

                    hideMap();
                    emptyElem('.box');
                }
            }
        });
    }

    function resetFields() {
        clearInput($cityInput);
        clearInput($streetInput);
        clearInput($numberInput);

        emptyElem($cityResultList);
        emptyElem($streetResultList);

        switchInputState($streetInput, true);
        switchInputState($numberInput, true);

        hideMap();
        emptyElem('.box');
    }

    function bindReset($elem) {
        $elem.on('click', function(e) {
            e.preventDefault();
            resetFields();
        });
    }

    var generateCityList = function(results) {
        var htmlToAppend = '';
        for (var i = 0; i < results.length; i++) {


            if (results[i].numOfStreets == 0) {
                hasStreet = false;
            } else {
                hasStreet = true;
            }

            htmlToAppend += '<li class="' + (hasStreet ? 'hasStreet' : '') + '" data-id="' + results[i].id + '">' + results[i].name + '</li>';
        }

        return htmlToAppend;
    }

    var generateStreetList = function(results) {
        var htmlToAppend = '';
        for (var i = 0; i < results.length; i++) {
            htmlToAppend += '<li data-id="' + results[i].id + '">' + results[i].street_name + '</li>';
        }

        return htmlToAppend;
    }

    function generateBox() {

        var url = '<a id="zaujem" class="button scroll-to" href="#sec-2" style="display: inline-block;">Mám záujem</a>';
        if (addressType=='real') {
	        url = '<a class="button" href="https://www.telekom.sk/fix/objednavka/-/scenario/e-shop/chytry-balik?invoiceNr=&city='+ $cityInput.val() +'&zipCode=&street=' + $streetInput.val() + '&streetNum=' + $numberInput.val() + '&usePlannedTechnologies=false" style="display: inline-block;">Mám záujem</a>';
        }

        boxElem = '<div id="ulicebox" class="ulicebox"><div class="row">' + address + '</div></div>' +
        '<div class="sluzby" style="display: block;"><div id="sluzby-header">DOSTUPNÉ SLUŽBY</div>' +
        '<div id="internet" class="internet"><img src="https://m.telekom.sk/wb/novepokrytie/img/icon_internet.png"><span class="black">Magio Internet</span> ' + internet + '</div>' +
        '<div id="tv" class="tv"><img src="https://m.telekom.sk/wb/novepokrytie/img/icon_tv.png"><span class="black">Magio televízia</span> ' + tv + '</div><p></p> '+ availability +'</div>' +
        '<div class="zaujem" style="display: block;">' +
        url +
        '</div>';
    }

    function bindClickEvent($result, $input) {
        $result.on('click', function(e) {
            e.preventDefault();

            var choosenElem = $(e.target).text();
            $input.val(choosenElem);

            emptyElem($result);
            hideList($input);

            if ($input.hasClass('city-input')) {

                if ($(e.target).hasClass('hasStreet')) {
                    switchInputState($streetInput);
                    cityId = $(e.target).data('id');
                } else {
                    addTextToInput($streetInput, 'Obec nemá ulice');
                }
                switchInputState($numberInput);
            } else if ($input.hasClass('street-input')) {
                streetId = $(e.target).data('id');
                switchInputState($numberInput);
            }

        });
    }

    function bindKeyup($input, $result) {
        $input.on('keyup', function() {
            if (isErrorVisible) {
                hideErrorMsg();
            }
            if (isInfoVisible) {
                hideInfoMsg();
            }

            if ($(this).val() == '') {
                emptyElem($result);

                if ($input.hasClass('city-input')) {
                    clearInput($streetInput);
                    clearInput($numberInput);
                    switchInputState($streetInput, true);
                    switchInputState($numberInput, true);
                    switchBtnState($btn, true);
                } else if ($input.hasClass('street-input')) {
                    clearInput($numberInput);
                    switchInputState($numberInput, true);
                    switchBtnState($btn, true);
                }
            }
        });
    }

    function bindNumKeyUp() {
        $numberInput.on('keyup', function() {
            if ($(this).val() != '') {
                switchBtnState($btn, false);
            } else {
                switchBtnState($btn, true);
            }
        });
    }

    function switchInputState($input, disable) {
        if (disable) {
            if (!$input.attr('disabled')) {
                $input.attr('disabled', true);
            }
            if (!$input.hasClass('disabled')) {
                $input.addClass('disabled');
            }
        } else {
            if ($input.attr('disabled')) {
                $input.attr('disabled', false);
            }
            if ($input.hasClass('disabled')) {
                $input.removeClass('disabled');
            }
        }
    }

    function switchBtnState($btn, disable) {
        if (disable) {
            if (!$btn.attr('disabled')) {
                $btn.attr('disabled', true);
            }
            if (!$btn.hasClass('disabled')) {
                $btn.addClass('disabled');
            }
        } else {
            if ($btn.attr('disabled')) {
                $btn.attr('disabled', false);
            }

            if ($btn.hasClass('disabled')) {
                $btn.removeClass('disabled');
            }
        }
    }

    function hideDropdownIfClickedElsewhere() {
        $(document).mouseup(function(e) {
            var container = $('.results'),
            elemToHide = $('.results ul');

            if (!container.is(e.target) && container.has(e.target).length === 0) {
                elemToHide.html('');
            }
        });
    }

    function bindAutocomplete($input, $result, addCityId, generateFunction, url, minLength) {
        $input.autocomplete({
            source: function(request, response) {
                $input.addClass('loading');
                emptyElem($result);

                var cityData = {
                    searchTermValue: request.term
                },
                streetData = {
                    searchTermValue: request.term,
                    cityId: cityId
                },
                tempData = {};

                addCityId ? $.extend(true, tempData, streetData) : $.extend(true, tempData, cityData)

                $.ajax({
                    url: url,
                    data: tempData,
                    dataType: "json",
                    success: function(data) {

                        if (data.length == 0) {
                            showInfoMsg(MSG_CANT_FIND_ADDRESS);
                        }

                        var temphtmlToAppend = '';

                        temphtmlToAppend = generateFunction(data);

                        $(temphtmlToAppend).appendTo($result);

                        showList($input);

                        $input.removeClass('loading');
                    },
                    error: function() {
                        showErrorMsg(MSG_CANT_CONNECT);
                        $input.removeClass('loading');
                    }
                });
            },
            minLength: minLength
        });
    }

    function submitAddress() {

        var tempVal = $numberInput.val();

        $.ajax({
            type: "GET",
            async: true,
            crossDomain: true,
            dataType: "json",
            data: {
                searchTermValue: tempVal,
                cityName: $cityInput.val(),
                streetName: $streetInput.val(),
                cityId: cityId,
                streetId: streetId
            },
            url: URL_FINAL,

            success: function(response) {

                if (response.length == 0) {
                    showInfoMsg(MSG_CANT_FIND_ADDRESS);
                } else {

                    address = response.address;
                    addressType = response.addressType;
                    internet = response.internet;
                    tv = response.tv;
                    availability = response.date;

                    generateBox();
	                  $('.box').empty();

	                  $(boxElem).appendTo('.box');

                    showMap();
                    getLongLat(address);

                    initMap();
                }
            },
            error: function() {
                showErrorMsg(MSG_CANT_CONNECT);
            }
        });
    }

    $btn.on('click', function() {
        submitAddress();
    });

    function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 17,
            center: {
                lat: 48.891132,
                lng: 18.042297
            },
            disableDefaultUI: true
        });

        var geocoder = new google.maps.Geocoder();

        geocodeAddress(geocoder, map);

    }

    function geocodeAddress(geocoder, resultsMap) {
        geocoder.geocode({
            'address': address
        }, function(results, status) {
            if (status === 'OK') {
                resultsMap.setCenter(results[0].geometry.location);
                marker = new google.maps.Marker({
                    map: resultsMap,
                    position: results[0].geometry.location
                });
            } else {
                alert('Geocode was not successful for the following reason: ' + status);
            }
        })

    };

    function getLongLat(address) {
        $.ajax({
            type: "GET",
            async: true,
            crossDomain: true,
            dataType: "json",
            data: {
                address: address,
                key: API_KEY
            },
            url: 'https://maps.googleapis.com/maps/api/geocode/json',

            success: function(response) {
                lat = response.results[0].geometry.location.lat;
                long = response.results[0].geometry.location.lng;
            },
            error: function() {
            }
        });
    }

    $(".scroll-to").click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top - ($("#nav-sticky").outerHeight())
                }, 1000);
                return false;
            }
        }
    });

    switchInputState($streetInput, true);
    switchInputState($numberInput, true);
    switchBtnState($btn, true);

    bindAutocomplete($cityInput, $cityResultList, false, generateCityList, URL_CITY, MIN_KEY_DOWN);
    bindAutocomplete($streetInput, $streetResultList, true, generateStreetList, URL_STREET, MIN_KEY_DOWN);

    preventSubmitOnEnterPress($cityInput);
    preventSubmitOnEnterPress($streetInput);
    preventSubmitOnEnterPress($numberInput);

    resetOnBackSpace($cityInput, true, false);
    resetOnBackSpace($streetInput, false, true);

    bindClickEvent($cityResultList, $cityInput);
    bindKeyup($cityInput, $cityResultList);

    bindClickEvent($streetResultList, $streetInput);
    bindKeyup($streetInput, $streetResultList);

    bindReset($resetBtn);

    bindNumKeyUp();

    hideDropdownIfClickedElsewhere();

});
