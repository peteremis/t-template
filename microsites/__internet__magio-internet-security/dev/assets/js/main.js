$(function() {
    $toggleLink = $('.more-items-link'),
    $moreItem = $('.more-items');

    $toggleLink.on('click', function(e) {
        e.preventDefault();
        if ($toggleLink.hasClass('show')) {
            $toggleLink.removeClass('show');
            $toggleLink.addClass('hide');
            $moreItem.removeClass('hidden');
        } else {
            $toggleLink.removeClass('hide');
            $toggleLink.addClass('show');
            $moreItem.addClass('hidden');
        }
    });

    /* ::: Scroll To ::: */
    $(".scroll-to").click(function() {
        var parentNav = $(this).closest('.main-navigation').attr('id');
        var offset = 0;

        if (parentNav === 'nav-sticky-custom') {
            offset = 68;
        }

        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: (target.offset().top - ($("#nav-sticky").outerHeight()) - offset)
                }, 1000);
                return false;
            }
        }
    });
    
    /* ::: Sticky Nav-ext ::: */
    $("#nav-sticky").sticky({
        topSpacing: 0,
        widthFromWrapper: true
    });

    var contentSections = $("[id*='sec-']"),
        secondaryNav = $("#nav-sticky");

    function updateSecondaryNavigation() {
        contentSections.each(function() {
            var actual = $(this),
                actualHeight = actual.height() + parseInt(actual.css('paddingTop').replace('px', '')) + parseInt(actual.css('paddingBottom').replace('px', '')),
                actualAnchor = secondaryNav.find('a[href="#' + actual.attr('id') + '"]');

            if ((actual.offset().top - secondaryNav.outerHeight() <= $(window).scrollTop()) && (actual.offset().top + actualHeight - secondaryNav.outerHeight() > $(window).scrollTop())) {
                actualAnchor.addClass('active');
            } else {
                actualAnchor.removeClass('active');
            }

        });
    }

    updateSecondaryNavigation();
    $(window).scroll(function(event) {
        updateSecondaryNavigation();
    });
});
