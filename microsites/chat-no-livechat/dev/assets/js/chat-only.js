$(function () {

    var currentdate = new Date();
    var currentHour = addZero(currentdate.getHours()) + "" + addZero(currentdate.getMinutes());
    var currentDay = currentdate.getDay();

    function addZero(i) {
        if (i < 10) {
            i = "0" + i;
        }
        return i;
    }

    var offTime1 = "0830",
        offTime2 = "2130";

    var callOffTime = function () {
        if (currentHour < offTime1 || currentHour > offTime2 || currentDay == 0) {
            return true;
        } else {
            return false;
        }
    }

    /*    var liveChatPresent = function () {
            if (($('#livechat').length) > 0) {
                return true;
            } else {
                return false;
            }
        }*/

    var showContainer = function () {
        $('.fixed-link__full').css('display', 'table-cell');
    }

    var showChat = function () {
        document.getElementById('chat').style.display = 'table-cell';
    }

    var showLiveChat = function () {
        document.getElementById('livechat').style.display = 'table-cell';
    }

    livechatoo.wininv.init();

    $('#livechat').click(function () {
        livechatoo.wininv.show(livechatoo.wininv.rules[75])
        return false;
    });

    $('#toggle-chat').on('click', function () {
        $('.fixed-link__full').toggleClass('hidden');
        $('.fixed-link__compact').toggleClass('hidden');
    });

    $('.fixed-link__compact').on('click', function () {
        $('.fixed-link__full').toggleClass('hidden');
        $('.fixed-link__compact').toggleClass('hidden');
    });

    $('#chat').on('click', function () {
        $('.fixed-link__full').toggleClass('hidden');
    });

    if (callOffTime() == false) {
        showContainer();
        showLiveChat();
    }

});
