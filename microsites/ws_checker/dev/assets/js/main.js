$(function() {

	var apiUrl = 'https://backvm.telekom.sk/shop/ins/ws.php';

	if (window.location.hostname == 'localhost') {
		apiUrl = 'https://backvm.telekom.sk/shop/ins/ws.php';
		// apiUrl = 'http://shop.telekom.localhost/ws.php';
	}

	var address = '',
		addressType = '',
		lat = '',
		long = '',
		internetSpeed = '',
		tvChannels = '',
		date = '',
		marker = '',
		availability = '',
		isAvailable = '',

		vdslData = '',
		gponData = '',
		opticData = '',

		$token = $('.token-input'),
		$cityInput = $('.city-input'),
		$cityResultList = $('.city-form__result_list'),
		$streetInput = $('.street-input'),
		$streetInputLabel = $('.street-input-label'),
		$streetResultList = $('.street-form__result_list'),
		$numberInput = $('.number-input'),
		$numberInputLabel = $('.number-input-label'),
		$btn = $('.sendReq'),
		$errorMsg = $('.error-msg'),
		$infoMsg = $('.info-msg'),
		$mapHolder = $('.map-holder'),
		$resetBtn = $('.reset'),
		isMapInitialised = false,
		hasStreet = true,
		cityId = '',
		streetId = '',
		isInfoVisible = false,
		isErrorVisible = false,
		insText = '',
		boxElem = '',
		availableServicesHolder = $('.av-services'),
		/*
			URL_CITY = 'https://backvm.telekom.sk/shop/ins/?searchTermKey=citySubstring',
			URL_STREET = 'https://backvm.telekom.sk/shop/ins/?searchTermKey=streetSubstring',
			URL_FINAL = 'https://backvm.telekom.sk/shop/ins/?searchTermKey=streetNoSubstring',
		*/
		URL_CITY = apiUrl + '?searchTermKey=citySubstring',
		URL_STREET = apiUrl + '?searchTermKey=streetSubstring',
		URL_FINAL = apiUrl + '?searchTermKey=streetNoSubstring',
		MIN_KEY_DOWN = 3,
		MSG_CANT_CONNECT = 'Nepodarilo sa načítať informácie, skúste to prosím ešte raz.',
		MSG_CANT_FIND_ADDRESS = '<b>Na zadanej adrese sme nenašli žiadne dostupné služby. Zadajte inú adresu alebo skontrolujte zadané údaje a skúste to znovu.</b>',
		API_KEY = 'AIzaSyAxxGXewHnIIPPZRLBhiUQqyxKXGyGeeus';

	// $cityInput.val('Bratislava');
	// $streetInput.val('Bazovského');
	// $numberInput.val('3');

	// $streetInput.val('Teslova');
	// $numberInput.val('43');

	// $streetInput.val('Bajkalská');
	// $numberInput.val('3');

	// $token.val('WgD0HsWV');

	// submitAddress();

	function resultsList_Behavior(results, input) {
		$.each($(results).find('li'), function() {
			if ($(this).text().toLowerCase() === $cityInput.val().toLowerCase()) {
				$(this).click();
				if ($streetInput.is(':disabled')) {
					$numberInput.focus()
				} else {
					$streetInput.focus()
				}
			} else if ($(this).text().toLowerCase() === $streetInput.val().toLowerCase()) {
				$(this).click();
				$numberInput.focus()
			} else if ($(this).text() === $numberInput.val()) {
				$(this).click();
			}

			$(this).on('click', function() {
				if($(input).hasClass('city-input')) {
					switchInputState($streetInput, false);
					$streetInput.focus()
				} else if ($(input).hasClass('street-input')) {
					switchInputState($numberInput, false);
					$numberInput.focus()
				}
			})
		})
	}

	function hideList($input) {
		var $tempParent = $input.next('.results');
		$tempParent.hide();
	}

	function showList($input) {
		var $tempParent = $input.next('.results');
		resultsList_Behavior($tempParent, $input)
		$tempParent.show();
	}

	function emptyElem($elem) {
		$($elem).html('');
	}

	function addTextToInput($input, text) {
		$input.val(text);
	}

	function clearInput($input) {
		$input.val('');
	}

	function showErrorMsg(msg) {
		$errorMsg.show();
		$errorMsg.text(msg);
		isErrorVisible = true;
	}

	function hideErrorMsg() {
		$errorMsg.hide();
		$errorMsg.text('');
		isErrorVisible = false;
	}

	function showInfoMsg(msg) {
		$infoMsg.show();
		$infoMsg.html(msg);
		isInfoVisible = true;
	}

	function hideInfoMsg() {
		$infoMsg.hide();
		$infoMsg.html('');
		isInfoVisible = false;
	}

	function hideMap() {
		$mapHolder.hide();
	}

	function showMap() {
		$mapHolder.show();
	}

	function preventSubmitOnEnterPress($input) {
		$input.keypress(function(e) {
			var code = (e.keyCode ? e.keyCode : e.which);
			if (code == 13) {
				return false;
			}
		});
	}

	function clearResultOnArrowKey($input, $result) {
		$input.keydown(function(e) {
			var code = (e.keyCode ? e.keyCode : e.which);
			if (code == 38 || code == 40) {
				emptyElem($result);
			}
		});
	}

	function resetOnBackSpace($input, resetCityInput, resetStreetInput) {
		$input.on('keydown', function(e) {
			if (e.which == 8 || e.which == 46) {
				if (resetCityInput) {
					resetFields();
				} else if (resetStreetInput) {
					clearInput($numberInput);

					emptyElem($streetResultList);

					switchInputState($numberInput, true);
					switchInputState($numberInputLabel, true);

					hideMap();
					emptyElem('.box');
				}
			}
		});
	}

	function resetFields() {
		insText = '';
		clearInput($cityInput);
		clearInput($streetInput);
		clearInput($numberInput);

		emptyElem($cityResultList);
		emptyElem($streetResultList);

		switchInputState($streetInput, true);
		switchInputState($streetInputLabel, true);

		switchInputState($numberInput, true);
		switchInputState($numberInputLabel, true);

		hideMap();
		emptyElem('.box');
	}

	function bindReset($elem) {
		$elem.on('click', function(e) {
			e.preventDefault();
			resetFields();
		});
	}

	var generateCityList = function(results) {
		var htmlToAppend = '';
		for (var i = 0; i < results.length; i++) {
			if (results[i].numOfStreets == 0) {
				hasStreet = false;
			} else {
				hasStreet = true;
			}
			htmlToAppend += '<li class="' + (hasStreet ? 'hasStreet' : '') + '" data-id="' + results[i].id + '">' + results[i].name + '</li>';
		}

		return htmlToAppend;
	}

	var generateStreetList = function(results) {
		var htmlToAppend = '';
		if (results !== null) {
			for (var i = 0; i < results.length; i++) {
				htmlToAppend += '<li data-id="' + results[i].id + '">' + results[i].street_name + '</li>';
			}
		}
		return htmlToAppend;
	}

	/**
	 * 2019 update: new behavior added
	**/
	function generateAvailableServices() {
		availableServicesHolder = $('.av-services');

		function generateHeading(availabilityClass, text) {
			hideInfoMsg();
			hideErrorMsg();

			var heading =
			'<div class="av-services_heading '+availabilityClass+' text-center">'+
				'<p>Overenie prebehlo úspešne.<br><b>Na adrese <span class="av-services_address">'+address+'</span>'+text+'</b></p>'+
			'</div>';
			$(heading).prependTo(availableServicesHolder);
		}

		function unavailableService(UnavailableText) {
			var UnavailableService =
			'<div class="row av-services_content unavailable">'+
				'<div class="col-sm-12 col-md-1">'+
					'<img src="https://www.telekom.sk/documents/10179/17656899/failure.png" alt="done">'+
				'</div>'+
				'<div class="col-sm-12 col-md-11">'+
					'<p class="service-name"><b>'+UnavailableText+'</b></p>'+
				'</div>'+
			'</div>';
			$(UnavailableService).appendTo(availableServicesHolder);
		}

		function serviceIsAvailable(service, serviceName, toggleElementID, blueToggleLink, price) {

			var content =
			'<div class="row av-services_content">' +
			'	<div class="col-sm-12 col-md-1">' +
					'<img src="https://www.telekom.sk/documents/10179/17656899/done.png" alt="done">' +
				'</div>' +
				'<div class="col-sm-12 col-md-7">' +
					'<p class="service-name" id="service-'+service+'"><b>'+serviceName+'</b></p>' +
					'<div class="togleArrow">' +
						blueToggleLink+
					'</div>'+
				'</div>'+
				'<div class="col-sm-12 col-md-4 text-right">'+
					'<p><button class="btn-order">MÁM ZÁUJEM</button></p>'+
				'</div>'+
			'</div>';
			$(content).appendTo(availableServicesHolder);
		}

		if (vdslData.code === 'available' || opticData.code === 'available' || gponData.code === 'available' ) {
			generateHeading('available',' sú dostupné tieto služby:');
		} else {
			generateHeading('unavailable',' nie sú dostupné žiadne služby.');
		}

		function generateServiceContent(service, serviceName, toggleElementID, blueToggleLink, price, infoText, insText) {
			console.log(service);
			console.log(vdslData);

			if (service=='vdsl') {
				$('.sec-'+service+'').show();

				if (vdslData['code']=='available') {
					serviceIsAvailable(
						service,
						serviceName + ' ' + vdslData['speed'],
						toggleElementID,
						blueToggleLink,
						price,
					);
				} else {
					unavailableText = 'VDSL linka nie je dostupná na uvedenej adrese';
					unavailableService(unavailableText);
				}
			}

			if (service=='gpon') {
				$('.sec-'+service+'').show();

				if (gponData['code']=='available') {
					serviceIsAvailable(
						service,
						serviceName + ' ' + gponData['speed'],
						toggleElementID,
						blueToggleLink,
						price,
					);
				} else {
					unavailableText = 'GPON nie je dostupný na uvedenej adrese';
					unavailableService(unavailableText);
				}
			}

			if (service=='optic') {
				$('.sec-'+service+'').show();

				if (opticData['code']=='available') {
					serviceIsAvailable(
						service,
						opticData['description'] +' - '+ opticData['value'],
						toggleElementID,
						blueToggleLink,
						price,
					);
				} else if (opticData['code']=='partialy') {
					serviceIsAvailable(
						service,
						opticData['description'] +' - '+ opticData['value'],
						toggleElementID,
						blueToggleLink,
						price,
					);
				} else {
					unavailableText = 'Optika nie je dostupná na uvedenej adrese';
					unavailableService(unavailableText);
				}
			}

		};

		generateServiceContent(
			'vdsl',
			'VDSL',
			'toggleContentTV',
			'V prípade Vášho záujmu o niektorú zo služieb nás prosím kontaktujte',
			'10,90',
			'',
			''
		);
		generateServiceContent(
			'gpon',
			'GPON',
			'toggleContentInternet',
			'V prípade Vášho záujmu o niektorú zo služieb nás prosím kontaktujte',
			'10,90',
			'<p><b>Spoľahlivosť a bezpečnosť</b></p><p>Surfujte doma bez výpadkov naraz na počítači, notebooku, telefóne alebo tablete cez bezpečnú bezdrôtovú Wi-Fi sieť alebo kábel.</p><p><b>Neobmedzený internet</b></p><p>Magio internet na doma nemá žiadne časové ani dátové obmedzenia a je ideálny pre časté a intenzívne využívanie.</p><p><b>Doplnková služba Giga</b></p><p>Získajte rýchlosť pripojenia až do 1 Gbit/s s Doplnkovou službou Giga. Len za 5 eur mesačne bez viazanosti k Magio internetu XL.</p><p><b>Magio internet security</b></p><p>Nedovoľte nikomu prekĺznuť do vášho počítača a ukradnúť vaše súkromné súbory, údaje, fotografie alebo heslá.</p>',
			''
		);
		generateServiceContent(
			'optic',
			'',
			'toggleContentVoice',
			'V prípade Vášho záujmu o niektorú zo služieb nás prosím kontaktujte',
			'10,90',
			'<p><b>Neobmedzené volania</b></p><p>Užívajte si neobmedzené volania na mobily, do pevných sietí a Európskej únie z pohodlia svojho domova.</p><p><b>Spoľahlivosť a kvalita</b></p><p>S rokmi overenou Pevnou linkou od Telekomu sa môžete spoľahnúť na stabilné pripojenie s vysoko kvalitným zvukom.</p><p><b>CLIP a CLIR sú samozrejmosťou a zadarmo</b></p><p>Aj na Pevnej linke môžete bezplatne využívať službu CLIP, s ktorou vidíte, aké číslo vám volá alebo CLIR, ktorá zabráni zobrazovaniu vášho čísla ak si to neželáte.</p>',
			''
		);

		//handle submit btn behavior
		$btn.unbind('click');
		$btn.text('Overiť inú adresu');
		switchInputState($streetInput, true);
		switchInputState($numberInput, true);
		switchInputState($cityInput, true);
		$btn.on('click', function(e) {
			if ($btn.text() === 'Overiť inú adresu') {
				$('.sec-tv, .sec-internet, .sec-voice').show();
				$(availableServicesHolder).empty();
				$('.av-services-btns').empty();
				resetFields();
				switchInputState($cityInput, false);
				switchInputState($streetInput, true);
				switchInputState($numberInput, true);
			}

			var bindSubmit = function() {
				submitAddress();
			}
			$btn.text('Overiť');
			$btn.bind('click', bindSubmit);
		});

		if ($('.av-services-btns').children().length === 0) {
			// $(chytryBalikBtn).appendTo('.av-services-btns'); //DISABLED
		} else {
			return;
		}
	}

	function bindClickEvent($result, $input) {
		$result.on('click', function(e) {
			e.preventDefault();

			var choosenElem = $(e.target).text();
			$input.val(choosenElem);

			emptyElem($result);
			hideList($input);

			if ($input.hasClass('city-input')) {
				// console.log($result);
				// console.log($input);

				if ($(e.target).hasClass('hasStreet')) {
					switchInputState($streetInput);
					switchInputState($streetInputLabel);
					$streetInput.val('')
					cityId = $(e.target).data('id');
				} else {
					addTextToInput($streetInput, 'Obec nemá ulice');
					switchInputState($streetInput, true);
					switchInputState($numberInput);
					$numberInput.focus()
					switchInputState($numberInputLabel);
				}
			} else if ($input.hasClass('street-input')) {
				streetId = $(e.target).data('id');
				switchInputState($numberInput);
				switchInputState($numberInputLabel);

			}
		});
	}

	function bindKeyup($input, $result) {
		$input.on('keyup', function() {
			if (isErrorVisible) {
				hideErrorMsg();
			}
			if (isInfoVisible) {
				hideInfoMsg();
			}

			if ($(this).val() == '') {
				emptyElem($result);

				if ($input.hasClass('city-input')) {
					clearInput($streetInput);
					clearInput($numberInput);

					switchInputState($streetInput, true);
					switchInputState($streetInputLabel, true);

					switchInputState($numberInput, true);
					switchInputState($numberInputLabel, true);

					switchBtnState($btn, true);
				} else if ($input.hasClass('street-input')) {
					clearInput($numberInput);

					switchInputState($numberInput, true);
					switchInputState($numberInputLabel, true);

					switchBtnState($btn, true);
				}
			} else {
			}
		});
	}

	function bindNumKeyUp() {
		$numberInput.on('keyup', function() {
			if ($(this).val() != '') {
				switchBtnState($btn, false);
				// 	// Filter non-digits from input value.
				// if (/\D/g.test(this.value)){
				// 	this.value = this.value.replace(/\D/g, '');
				// }
			} else {
				switchBtnState($btn, true);
			}
		});
	}

	function switchInputState($input, disable) {
		//test
		// return;

		if (disable) {

			if (!$input.attr('disabled')) {
				$input.attr('disabled', true);
			}
			if (!$input.hasClass('disabled')) {
				$input.addClass('disabled');
			}

		} else {

			if ($input.attr('disabled')) {
				$input.attr('disabled', false);
			}
			if ($input.hasClass('disabled')) {
				$input.removeClass('disabled');
			}

		}
	}

	function switchBtnState($btn, disable) {
		//test
		return;

		if (disable) {
			if (!$btn.attr('disabled')) {
				$btn.attr('disabled', true);
			}
			if (!$btn.hasClass('disabled')) {
				$btn.addClass('disabled');
			}
		} else {
			if ($btn.attr('disabled')) {
				$btn.attr('disabled', false);
			}

			if ($btn.hasClass('disabled')) {
				$btn.removeClass('disabled');
			}
		}
	}

	function hideDropdownIfClickedElsewhere() {
		$(document).mouseup(function(e) {
			var container = $('.results'),
				elemToHide = $('.results ul');

			if (!container.is(e.target) && container.has(e.target).length === 0) {
				elemToHide.html('');
			}
		});
	}

	function bindAutocomplete($input, $result, addCityId, generateFunction, url, minLength) {
		var $tempParent = $input.next('.results');
			$input.autocomplete({
				source: function(request, response) {
					$input.addClass('loading');
					emptyElem($result);

					var cityData = {
							searchTermValue: request.term
						},
						streetData = {
							searchTermValue: request.term,
							cityId: cityId
						},
						tempData = {};

					addCityId ? $.extend(true, tempData, streetData) : $.extend(true, tempData, cityData)

					$.ajax({
						url: url,
						data: tempData,
						dataType: "json",
						success: function(data) {
							if (data !== null && jQuery.isEmptyObject(data) === false) {
								response($.map(data, function(item) {
									if (item.name) {
										return {
											label: item['name'],
											value: item['name'],
										  }
									} else if (item.streetNoCount) {
										return {
											label: item['street_name'],
											value: item['street_name']
										  }
									}
								}));
							} else {
								showInfoMsg(MSG_CANT_FIND_ADDRESS);
							}

							var temphtmlToAppend = '';
								temphtmlToAppend = generateFunction(data);
							$(temphtmlToAppend).appendTo($result);

							showList($input);

							$input.removeClass('loading');
						},
						error: function() {
							console.log("error")
							showErrorMsg(MSG_CANT_CONNECT);
							$input.removeClass('loading');
						}
					});

				},
				select: function(event , ui) {
					$input.val(ui.item.value);
					resultsList_Behavior($tempParent, $input)
				},
				focus: function(event, ui) {
					$.each($tempParent.find('li'), function() {
						if ($(this).text() === ui.item.value) {
							$(this).css({'background-color':'#f5f5f5'})
						} else {
							$(this).css({'background-color':'inherit'})
						}
					})
				},

				minLength: minLength
			});
	}

	function submitAddress() {
		$('#ajaxLoadingSpinnerOverlay').show();
		var tempVal = $numberInput.val();
		$.ajax({
			type: "GET",
			async: true,
			crossDomain: true,
			dataType: "json",
			data: {
				token: $token.val(),
				searchTermValue: tempVal,
				cityName: $cityInput.val(),
				streetName: $streetInput.val(),
				cityId: cityId,
				streetId: streetId
			},
			url: URL_FINAL,
			success: function (response) {
				hideInfoMsg();
				hideErrorMsg();

				if (typeof(response.optic) === "undefined" || response.optic == null) {
					// if (response.length == 0) {
					$('#ajaxLoadingSpinnerOverlay').hide();
					showInfoMsg(MSG_CANT_FIND_ADDRESS);
/*
					$.fancybox({
						href: '#not-found',
						modal: true
					});
*/
					return false;
				} else {

					console.log(response);

					$('#ajaxLoadingSpinnerOverlay').hide();

					vdslData = response.vdsl;
					opticData = response.optic;
					gponData = response.gpon;

					$(availableServicesHolder).empty();
					generateAvailableServices();

				}
			},
			error: function(response) {
				$('#ajaxLoadingSpinnerOverlay').hide();
				hideInfoMsg();
				hideErrorMsg();
				showErrorMsg(response.responseJSON['error']);
			}
		});
	}

	$btn.on('click', function() {
		submitAddress();
	});

	$(".scroll-to").click(function () {
		if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
			if (target.length) {
				$('html,body').animate({
					scrollTop: target.offset().top - ($("#nav-sticky").outerHeight())
				}, 1000);
				return false;
			}
		}
	});

	switchInputState($streetInput, true);
	switchInputState($numberInput, true);
	switchBtnState($btn, true);

	bindAutocomplete($cityInput, $cityResultList, false, generateCityList, URL_CITY, MIN_KEY_DOWN);
	bindAutocomplete($streetInput, $streetResultList, true, generateStreetList, URL_STREET, MIN_KEY_DOWN);

	preventSubmitOnEnterPress($cityInput);
	preventSubmitOnEnterPress($streetInput);
	preventSubmitOnEnterPress($numberInput);

	resetOnBackSpace($cityInput, true, false);
	resetOnBackSpace($streetInput, false, true);

	bindClickEvent($cityResultList, $cityInput);
	bindKeyup($cityInput, $cityResultList);

	bindClickEvent($streetResultList, $streetInput);
	bindKeyup($streetInput, $streetResultList);


	bindReset($resetBtn);

	bindNumKeyUp();

	hideDropdownIfClickedElsewhere();

	$('.not-found-form').show();
	$('.not-found-success').hide();

	$('.popup').fancybox({
		padding: 11,
		margin: 0,
		closeBtn: true,
		hideOnOverlayClick: true,
		helpers: {
			overlay: {
				css: {
					'background': 'rgba(0, 0, 0, 0.7)'
				}
			}
		}
	});

	$('.p-close').on('click', function (e) {
		e.preventDefault();
		$.fancybox.close();
	});

	$('#not-found-send').on('click', function (e) {

		var citiVal = $cityInput.val();
		var streetVal = $streetInput.val();
		var numberVal = $numberInput.val();

		var internetVal = $('#nf-internet').attr('checked');
		var tvVal = $('#nf-tv').attr('checked');
		var pevnaVal = $('#nf-pevna').attr('checked');
		var phoneVal = $('#nf-phone').val();

		$.ajax({
			type: "GET",
			async: true,
			crossDomain: true,
			dataType: "json",
			data: {
				phone: phoneVal,
				mesto: citiVal,
				ulica: streetVal,
				cislo: numberVal,
				tv: tvVal,
				internet: internetVal,
				pevna: pevnaVal
			},
			url: "https://backvm.telekom.sk/www/checker/email.php",

			success: function (response) {
				$('.not-found-form').hide();
				$('.not-found-success').show();
			},
			error: function () {
				showErrorMsg(MSG_CANT_CONNECT);
			}
		});

		e.preventDefault();
		// $.fancybox.close();
	});

	/* TOGGLE ARROW */
	$(document).on('click', '.toggle-arrow, .arrow-right', function(event) {
		event.preventDefault();
		if ($(this).hasClass('arrow-right')) {
		return;
		}
		$(event.target).closest('.togleArrow').find('.arrow-content').toggle(200);
		$(this).find('.arrow-right').toggleClass('arrow-rotate');
	});
	/* TOGGLE ARROW END */

});
