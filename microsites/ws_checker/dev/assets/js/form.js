$(document).ready(function() {

  var scrollingSpeed = 1200;

  var form = $("#p_p_id_dynamicForm_WAR_eccadmin_").contents();
  $("#c-form").append(form);

  $('.formCompositionRenderDisplay h2').hide();

  $('.labelField').width(250);

  $('.prevPageButtonSpan').hide();
  $('.pageMiddle').hide();

  $('#zaujem').click(function () {
    scrollTo('.c-row-4', 0);
    return false;
  });

  function scrollTo(id, height) {
    height = height ? height : 0;
    $('html,body').animate({
        scrollTop: $(id).offset().top - height
      },
      scrollingSpeed,
      function(){
        disabled = false;
      }
    );
  }


});