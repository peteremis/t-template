$(function() {
  var pricesTable = $("#pricesTable");

  // if LSB agent active, remove columns
  var isLSB = false;
  if (isLSB) {
    $(".onWeb").remove();
    $(".inStore").attr("colspan", 2);

    pricesTable
      .find("thead tr th:nth-of-type(3), tbody tr td:nth-of-type(3)")
      .css({
        "border-left": "10px solid #fff",
        "border-right": "10px solid #fff"
      });
  }

  //prevent tooltip on click
  $("a.ttp").on("click", function(e) {
    e.preventDefault();
  });
  //generate tooltips function
  function generateTooltip(id, text) {
    $(id).qtip({
      content: "<p class='text-center'>" + text + "</p>",
      style: {
        classes: "qtip-tipsy center"
      },
      position: {
        my: "bottom center",
        at: "top center"
      }
    });
  }
  generateTooltip(
    "#ttp-deposit",
    "Akontácia je úvodná suma, ktorú zaplatíte pri kúpe v predajni alebo cez web."
  );
  generateTooltip(
    "#ttp-monthlyPayment",
    "Mesačnú splátku budete platiť každý mesiac po dobu 24 mesiacov."
  );
  generateTooltip(
    "#ttp-priceTogether",
    "V tomto stĺpci nájdete informáciu, koľko zaplatíte za zariadenie spolu po sčítaní akontácie a mesačných splátok."
  );

  //select input on table row click
  pricesTable.find("tbody tr").on("click", function(e) {
    e.preventDefault();

    if (!$(this).hasClass("disabled")) {
      // remove "checked" class from not selected radio btns
      $.each($(pricesTable).find("tbody tr"), function(key, item) {
        $(item)
          .find("input[type=radio]")
          .closest("span")
          .removeClass("checked");
      });

      // add "selected" class to selected radio btn
      $(this)
        .find("input[type=radio]")
        .closest("span")
        .addClass("checked");
      // inser backend function ...
    }
  });
});
