$(function () {

    $("div#wikiScrollUpDiv").click(function () {
        $("html, body").animate({
            scrollTop: $("#wikiSearchInput").offset().top - 100
        }, 500, function () {
            $("#wikiSearchInput").focus();
        });
    });

    $(".tiles.right a").addClass("tiles__custom");

    var rightTiles = $(".tiles__custom"),
        browserWidth = window.innerWidth,
        resized = false;


    $(window).resize(function () {
        browserWidth = window.innerWidth;

        if (browserWidth < 555 && resized == false) {
            rightTiles.appendTo(".tiles.left");
            resized = true;
        } else if (browserWidth > 555 && resized == true) {
            rightTiles.appendTo(".tiles.right");
            resized = false;
        }
    });
});
