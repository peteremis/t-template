$(function() {

     $('.popup').fancybox({
         padding: 0,
         margin: 0,
         maxWidth: 600,
         closeBtn: false,
         parent: "#content",
         helpers: {
             overlay: {
                 css: {
                     'background': 'rgba(0, 0, 0, 0.7)'
                 }
             }
         }
     });

     $('.p-close, .link-close, .text-close, .close-btn').on('click', function(e) {
         e.preventDefault();
         $.fancybox.close();
     });

});
