$(document).ready(function () {

var dataUrl = "/assets/js/detail.json";
var id = window.location.search.slice(1); //na zaklade ID zobrazuje obsah

	$.ajax({
  url: dataUrl,
  cache: false,
	type: "get",
	crossDomain: true,
  success: function(phones){
		showDetail(phones, id);
  },
	error: function () {
		alert("Chyba pri posielani ... skus este raz!");
	}
});

$('#device-detail-page').fadeIn(1500).removeClass('hidden');

// back btn
$("#back-btn").click(function (){
  window.history.back();
});

//$('#back-btn').attr('href',document.referrer);


function showDetail(phones, id) {

	for (var a = 0; a < phones.length; a++) {
		if (phones[a].id == id) {

			var detail = phones[a].description;

			var details = '';
			for (var b = 0; b < detail.length; b++) {
				details +=
				'<tr class="params">'+
					'<td>'+
						'<div>'+
						 detail[b].desc[0] +
						'</div>'+
					'</td>'+
					'<td>'+
						 detail[b].desc[1] +
					'</td>'+
					'</tr>';
			}
			$(details).appendTo(".paramsTable");


			var phoneName = phones[a].name;
			$(".device-detail-desc__title h3 span").text(phoneName);
			$(".device-detail-desc__subTitle span").text(phones[a].subTitle)
			$(".phone-img").attr('src', phones[a].image);

			//if badge src = empty, hide badge
			$(".ec-img").attr('src', phones[a].badge);
			if ($(".ec-img").attr('src') === "") {
				$(".ec-img").hide();
				console.log("pravda")
			} else {
				$(".ec-img").show();
			}

			//box Happy XS Mini
			if(phones[a].happyShow == true) {
				$('.easypecka').show()
			}
			//box EasyPecka
			if (phones[a].easyPeckaShow == true) {
				$('.happyxs').show()
			}

			// easypecka prices
			$(".price1").text(phones[a].easyPecka[0].skreditom);
			$(".price2").text(phones[a].easyPecka[1].telefon);
			$(".price3").text(phones[a].easyPecka[2].priKupe);
			$(".price4").text(phones[a].easyPecka[3].mesacne);

			// happy xs mini prices
			$(".price5").text(phones[a].happyXSmini[0].pausal);
			$(".price6").text(phones[a].happyXSmini[1].kpausaluJednorazovo);
			$(".price7").text(phones[a].happyXSmini[2].kpausaluMEsacne);
			$(".price8").text(phones[a].happyXSmini[3].priKupe);
			$(".price9").text(phones[a].happyXSmini[4].mesacne);

			$("#peckaUrl").attr('href', phones[a].easyPecka[4].kupitsEASYPECKA);
			$("#xsminiUrl").attr('href', phones[a].happyXSmini[5].kupitsHAPPY);

		}
	}

}

});
