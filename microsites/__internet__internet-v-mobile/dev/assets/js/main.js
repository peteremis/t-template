$(document).ready(function () {


  $('.sec3-tip-30dni').qtip({
    content: 'Po 30 dňoch sa balík pri dostatočnej výške kreditu automaticky obnoví.',
    position: {
      my: 'bottom center',
      at: 'right top'
    }
  });

  $('.sec3-tip-30dni-xmas').qtip({
    content: 'Po 30 dňoch sa balík pri dostatočnej<br> výške kreditu automaticky obnoví.<br><b>1 GB dát zadarmo získate každý<br> mesiac aj po skončení Vianoc</b><br> dovtedy, kým budete mať balík s <br>automatickou obnovou aktívny',
    position: {
      my: 'bottom center',
      at: 'right top'
    }
  });

  $('.sec3-tip-1gb10-fup').qtip({
    content: 'S balíkom 1 GB na 10 dní môžete na Slovensku a v celej EÚ presurfovať 1 GB za 2,00€ / 10 dní. Po minutí 1 GB dát v EÚ mimo SR bude vaša dátová prevádzka pozastavená. Pre ďalšie surfovanie je potrebné dokúpiť si niektorý z dátových balíčkov.',
    position: {
      my: 'bottom center',
      at: 'right top'
    }
  });

  $('.sec3-tip-2gb-fup').qtip({
    content: 'S balíkom 2 GB na deň môžete v EÚ mimo Slovenska presurfovať 731,43 MB za 1,50€ / 24 hodín. Po minutí 731,43 MB dát v EÚ mimo SR v rámci dňa sa vám začne započítavať príplatok 0,0042 € za 1 MB prenesených dát až do výšky 2GB. Po spotrebovaní 2GB bude dátová prevádzka pozastavená. Pre ďalšie surfovanie je potrebné dokúpiť si niektorý z dátových balíčkov.',
    position: {
      my: 'bottom center',
      at: 'right top'
    }
  });

  $('.sec3-tip-1gb30-fup').qtip({
    content: 'S balíkom 1 GB na 30 dní môžete na Slovensku a v celej EÚ presurfovať 1 GB za 3,00€ / 30 dní. Po minutí 1 GB dát v EÚ mimo SR bude vaša dátová prevádzka pozastavená. Pre ďalšie surfovanie je potrebné dokúpiť si niektorý z dátových balíčkov.',
    position: {
      my: 'bottom center',
      at: 'right top'
    }
  });

  $('.sec3-tip-3gb-fup').qtip({
    content: 'S balíkom 3 GB na 30 dní môžete na Slovensku a v celej EÚ presurfovať 3 GB za 6,00€ / 30 dní. Po minutí 3 GB dát v EÚ mimo SR bude vaša dátová prevádzka pozastavená. Pre ďalšie surfovanie je potrebné dokúpiť si niektorý z dátových balíčkov.',
    position: {
      my: 'bottom center',
      at: 'right top'
    }
  });

  $('.sec3-tip-5gb-fup').qtip({
    content: 'S balíkom 5 GB na 30 dní môžete v EÚ mimo Slovenska presurfovať 3,81 GB za 8,00€ / 30 dní. Po minutí 3,81 GB dát v EÚ mimo SR v rámci dňa sa vám začne započítavať príplatok 0,0042 € za 1 MB prenesených dát až do výšky 5GB. Po spotrebovaní 5GB bude dátová prevádzka pozastavená. Pre ďalšie surfovanie je potrebné dokúpiť si niektorý z dátových balíčkov.',
    position: {
      my: 'bottom center',
      at: 'right top'
    }
  });

  $('.sec3-tip-300mb-fup').qtip({
    content: 'S balíkom 300 MB môžete na Slovensku a v celej EÚ presurfovať 300 MB za 0,50€ / deň. Po minutí 300 MB dát v EÚ mimo SR v rámci dňa bude vaša dátová prevádzka pozastavená. Pre ďalšie surfovanie je potrebné dokúpiť si niektorý z dátových balíčkov.',
    position: {
      my: 'bottom center',
      at: 'right top'
    }
  });

  $('.sec3-tip-unl-fup').qtip({
    content: 'S balíkom Nekonečné dáta na deň môžete v EÚ mimo Slovenska presurfovať 975,24 MB za 2,00€&nbsp;/&nbsp;deň. Po minutí 975,24 MB dát v EÚ mimo SR v rámci dňa sa vám začne započítavať príplatok 0,0042 € za 1 MB prenesených dát.',
    position: {
      my: 'bottom center',
      at: 'right top'
    }
  });


  /* scroll to + sticky nav */
  $("#nav-sticky").sticky({
    topSpacing: 0,
    widthFromWrapper: true
  });

  $(".scroll-to").click(function () {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        if ($('#nav-sticky').is(':visible')) {
          $('html,body').animate({
            scrollTop: target.offset().top - ($("#nav-sticky").outerHeight())
          }, 1000);
        } else {
          $('html,body').animate({
            scrollTop: target.offset().top - (0)
          }, 1000);
        }
        return false;
      }
    }
  });

  var contentSections = $("[id*='sec-']"),
    secondaryNav = $("#nav-sticky");

  function updateSecondaryNavigation() {
    contentSections.each(function () {
      var actual = $(this),
        actualHeight = actual.height() + parseInt(actual.css('paddingTop').replace('px', '')) + parseInt(actual.css('paddingBottom').replace('px', '')),
        actualAnchor = secondaryNav.find('a[href="#' + actual.attr('id') + '"]');

      if ((actual.offset().top - secondaryNav.outerHeight() <= $(window).scrollTop()) && (actual.offset().top + actualHeight - secondaryNav.outerHeight() > $(window).scrollTop())) {
        actualAnchor.addClass('active');
      } else {
        actualAnchor.removeClass('active');
      }

    });
  }

  updateSecondaryNavigation();

  $(window).scroll(function (event) {
    updateSecondaryNavigation();
  });
  /* scroll to + sticky nav END */

  $('.popup').fancybox({
    padding: 11,
    margin: 0,
    parent: "#content",
    closeBtn: false,
    helpers: {
      overlay: {
        css: {
          'background': 'rgba(0, 0, 0, 0.7)'
        }
      }
    }
  });

  $('.p-close').on('click', function (e) {
    e.preventDefault();
    $.fancybox.close();
  });

  $('.as1a').slick({
    infinite: false,
    slidesToShow: 2,
    slidesToScroll: 1,
    dots: true,
    responsive: [{
      breakpoint: 700,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true
      }
    }]
  });

  $('.as2a').slick({
    infinite: false,
    slidesToShow: 2,
    slidesToScroll: 1,
    dots: true,
    responsive: [{
      breakpoint: 700,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true
      }
    }]
  });

  $('.as3a').slick({
    infinite: false,
    slidesToShow: 2,
    slidesToScroll: 1,
    dots: true,
    responsive: [{
      breakpoint: 700,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true
      }
    }]
  });

  var $sliders = $('.app-slider');

  $(".tabs-container").each(function(){

    var $this = $(this);
    var slick = $this.find( $sliders ).slick({
      slidesToShow: 2,
      slidesToScroll: 1,
      centerMode: false,
      infinite: false,
      swipe: true,
      arrows: true,
      dots: true,
      autoplay: false,
      initialSlide: 0,
      touchMove: false,
      draggable: false,

      responsive: [{
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          arrows: true,
          dots: true,
        }
      }, {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          arrows: true,
          dots: true,
        }
      }]
    });

  });

  /* ::: DataDevice ::: */
  var $dataDeviceSlider = $('.data-device-slider');
  var $dataDevicePagin = $('.data-device-pagin');

  /* ::: TAB CONFIG ::: */
  $('ul.tabs li a').on('click', function (e) {
    var currentAttrValue = $(this).attr('href');
    $('.tab ' + currentAttrValue).show().siblings().hide();
    $(this).parent('li').addClass('current').siblings().removeClass('current');
    e.preventDefault();

    $('.as1').slick('setPosition');
    $('.as2').slick('setPosition');
    $('.as3').slick('setPosition');
  });

  /* ::: GET CURRENT SLIDE NUM ::: */
  $dataDeviceSlider.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
    //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
    var i = (currentSlide ? currentSlide : 0) + 1;
    $dataDevicePagin.text('(' + i + '/' + slick.slideCount + ')');
  });

  /* ::: PAGINATION INDICATOR ::: */
  if ($(window).width() < 769) {
    $dataDeviceSlider.on('init', function (event, slick) {
      if ($(window).width() < 769) {
        $('.data-device-slider .slick-dots li').css("width", 100 / $('.dataDevice__item').length + '%');
      }
    });
  };

  /* TOGGLE ARROW */
  $('.toggle-arrow, .arrow-right').click(function(event) {
    event.preventDefault();
    if ($(this).hasClass('arrow-right')) {
      return;
    }
    $(event.target).closest('.togleArrow').find('.arrow-content').toggle(200);
    $(this).find('.arrow-right').toggleClass('arrow-rotate');
  });
  /* TOGGLE ARROW END */

});
