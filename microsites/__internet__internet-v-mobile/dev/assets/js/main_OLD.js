$(function() {

    /* scroll to + sticky nav */
    $("#nav-sticky").sticky({
        topSpacing: 0,
        widthFromWrapper: true
    });

    $(".scroll-to").click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
              if ($('#nav-sticky').is(':visible')) {
                $('html,body').animate({
                scrollTop: target.offset().top - ($("#nav-sticky").outerHeight())
                }, 1000);
              } else {
                $('html,body').animate({
                scrollTop: target.offset().top - (0)
                }, 1000);
              }
                return false;
            }
        }
    });



    var contentSections = $("[id*='sec-']"),
        secondaryNav = $("#nav-sticky");

    function updateSecondaryNavigation() {
        contentSections.each(function () {
            var actual = $(this),
                actualHeight = actual.height() + parseInt(actual.css('paddingTop').replace('px', '')) + parseInt(actual.css('paddingBottom').replace('px', '')),
                actualAnchor = secondaryNav.find('a[href="#' + actual.attr('id') + '"]');

            if ((actual.offset().top - secondaryNav.outerHeight() <= $(window).scrollTop()) && (actual.offset().top + actualHeight - secondaryNav.outerHeight() > $(window).scrollTop())) {
                actualAnchor.addClass('active');
            } else {
                actualAnchor.removeClass('active');
            }

        });
    }
    updateSecondaryNavigation();

      $(window).scroll(function (event) {
          updateSecondaryNavigation();
      });
    /* scroll to + sticky nav END */

    $('.popup').fancybox({
        padding: 11,
        margin: 0,
        parent: "#content",
        closeBtn: false,
        helpers : {
            overlay : {
                css : {
                    'background' : 'rgba(0, 0, 0, 0.7)'
                }
            }
        }
    });

    $('.p-close').on('click', function(e) {
        e.preventDefault();
        $.fancybox.close();
    });


    $('.as1').slick({
        infinite: false,
        slidesToShow: 2,
        slidesToScroll: 1,
        dots: true,
        responsive: [{
            breakpoint: 700,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: true
            }
        }]
    });

    $('.as2').slick({
        infinite: false,
        slidesToShow: 2,
        slidesToScroll: 1,
        dots: true,
        responsive: [{
            breakpoint: 700,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: true
            }
        }]
    });


    function getSliderSettings(){
        return {
            infinite: false,
            slidesToShow: 2,
            slidesToScroll: 1,
            dots: true,
            responsive: [{
                breakpoint: 700,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true
                }
            }]
        }
    }


    $(".tabs-menu a").click(function (event) {
        $('.as2').css("visibility", "hidden");
        $('.as1').css("visibility", "hidden");
        event.preventDefault();
        // if (!$(this).parent().hasClass("current") && !$(this).parent().hasClass("isVariant")) {
        //     toggleTabs();
        // }

        $(this).parent().addClass("current");
        $(this).parent().siblings().removeClass("current");
        var tab = $(this).attr("href"),
        parent = $(this).closest('ul').parent().parent();
        parent.find('.tab-content').not(tab).css("display", "none");
        $(tab).fadeIn();

 

        setTimeout(function(){
            $('.as2').css("visibility", "visible");
            $('.as1').css("visibility", "visible");
            $('.as2').slick('setPosition');
            $('.as1').slick('setPosition');
        }, 200);


    });
    // var toggleTabs = function () {
    //     $('.tab__variant').toggleClass('selected');
    // };

    var dataSegment = $("[data-segment]").each(function(){
      //console.log($(this).attr('data-segment')); //zobraz "data-segment" hodnotu pre kazdu <section>

      closestHead = $($(this).find('.tabs-menu a'));
      closestItemCount = closestHead.length - 1; //vytiahni pocet tabov pre kazdu <section> s atributom "data-segment"
      //console.log(closestItemCount + "heads");

      closestContent = $($(this).find('.tab-content')); //najdi tab-content v danej <section>
      //console.log(closestContent);

      closestAccordion = $(this).find('.accordion'); //najdi .accordion v danej <section>

      accordionItem = '<div class="item">';

      for (var i = 0; i <= closestItemCount; i++) {
          accordionItem += '<div class="heading">' + $(closestHead[i]).text()  + '</div>';
          accordionItem += '<div class="content">' + $(closestContent[i]).html() + '</div></div>';

          if (i !== closestItemCount) {
              accordionItem += '<div class="item">';
          }
      }

      //if data-segment and data-accordion value match, show accordion data
      if($(this).attr('data-segment') === closestAccordion.attr('data-accordion')) {
           $(accordionItem).appendTo(closestAccordion);
      }

      var $items = $('.accordion .item');
      //SET OPENED ITEM
      $($items[0]).addClass('open');

    });

    /* ACCORDION */
    $('.accordion .item .heading').click(function(e) {

        var clickedHead = e.target;
        var $item = $(clickedHead).closest('.item');
        var isOpen = $item.hasClass('open');
        var $content = $item.find('.content');
        var $acPrice = $(clickedHead).find('.ac-price');

        if(isOpen) {
            $content.slideUp(200);
            $item.removeClass('open');
            $acPrice.show();
        } else {
            $content.slideDown(200);
            $item.addClass('open');
            $acPrice.hide();
        }

    });

    $('.accordData .subItem .subHeading').click(function(e) {

        var clickedHead = e.target;
        var $item = $(clickedHead).closest('.subItem');
        var isOpen = $item.hasClass('open');
        var $content = $item.find('.subContent');
        var $acPrice = $(clickedHead).find('.ac-price');

        if(isOpen) {
            $content.slideUp(200);
            $item.removeClass('open');
            $acPrice.show();
        } else {
            $content.slideDown(200);
            $item.addClass('open');
            $acPrice.hide();
        }

    });
    //$('.accordData .subItem.open').find('.subContent').slideDown(200);

    $('.accordion .item.open').find('.content').slideDown(200);

    /* SCROLL TO SURF */


    var browserWidth = window.innerWidth;
    var surf1000Slide = 1;

    if (browserWidth <= 600) {
        surf1000Slide = 2;
    }

    /* accordion data class remove - slider fix START */
        $('.accordion').find('.app-slider').removeClass('as1 as2');
    /* accordion data class remove - slider fix END */

    
    function slideAndOpen(browserWidth, surf1000Slide) {
        if (browserWidth <= 500) {
            $('#surf1000acc').trigger('click');
        } else {
            setTimeout(function(){
            $('.as2').slick('slickGoTo', surf1000Slide);
            }, 400);
        }
      }

    $('#scrollToSurf').on('click', function() {
        $('#tabEasy a').trigger('click');
        slideAndOpen(browserWidth, surf1000Slide);
    });

    var urlHash = window.location.hash;

    //scroll to surf and show #tabEasy
    if (urlHash == "#easykarta") {
        $('html, body').animate({
            scrollTop: $("#sec-2").offset().top + 'px'
        }, 1000);
        $('#tabEasy a').trigger('click');
        slideAndOpen(browserWidth, surf1000Slide)

    }
    /* SCROLL TO SURF END */

});
