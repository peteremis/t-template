$(function() {
  var container = $("#mapoptic"),
    width = container.width(),
    height = container.height();

  var map = new Datamap({
    scope: "svk",
    responsive: true,
    fills: {
      defaultFill: "#f5a6ce" // The keys in this object map to the "fillKey" of [data] or [bubbles]
    },
    element: document.getElementById("mapoptic"),
    geographyConfig: {
      highlightFillColor: "#e20074",
      popupTemplate: function(geography, data) {
        return (
          '<div class="hoverinfo"><b>' +
          geography.properties.name +
          " kraj</b><br/>" +
          geography.properties.optic +
          "</div> "
        );
      }
    },
    setProjection: function(element) {
      var projection = d3.geo
        .mercator()
        .scale(width * 8.45) //7300
        .center([19.680074266824, 48.63670023565])
        .translate([width / 2, height / 2]);

      var path = d3.geo.path().projection(projection);

      return {
        path: path,
        projection: projection
      };
    }
  });

  map.labels({
    labelColor: "white",
    fontSize: 12
  });

  $(window).on("resize", function() {
    map.resize();
  });
});
