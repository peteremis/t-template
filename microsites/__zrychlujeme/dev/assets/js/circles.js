$(document).ready(function() {
  /* CIRCLE PROGRESS */
  var createCircle = function(start, $elem) {
    $elem.circleProgress({
      startAngle: (Math.PI / 2) * 3,
      animationStartValue: start,
      value: start,
      size: 200,
      emptyFill: "#c8cacc",
      fill: {
        color: "#747a80",
      },
    });
    // .on("circle-animation-progress", function(event, animationProgress, stepValue) {
    //   var instance = $(this).data("circle-progress");
    //   console.log(instance);
    //   instance.fill = { gradient: [["rgba(0,0,0,1)", 0.5], ["rgba(255,255,255,1)", 0.5]] };
    // });
  };

  var updateCircle = function($elem, start, end) {
    $elem.circleProgress({
      animationStartValue: start,
      value: end,
    });
  };

  var animNumber = function(currentNumber, numValue, $elem, type) {
    currentNumber = currentNumber.replace(",", ".");

    $({
      numberValue: currentNumber,
    }).animate(
      {
        numberValue: numValue,
      },
      {
        duration: 1500,
        easing: "linear",
        step: function() {
          if (type === "decimal") {
            $elem.text((Math.ceil(this.numberValue * 10) / 10).toString().replace(".", ","));
          } else {
            $elem.text(Math.ceil(this.numberValue));
            if (Math.ceil(this.numberValue) > 590) {
              $elem.text(600);
            }
          }
        },
      },
    );
  };

  function appendCirclesToTabs(tabID, circlesToAppend) {
    $(".tab-content " + tabID).empty();
    $(".accordion " + tabID).empty();

    var width = window.innerWidth;
    if (width < 768) {
      $(".tab-content " + tabID).empty();
      $(".accordion " + tabID).append(circlesToAppend); //append circles for mobile view here
    } else {
      $(".accordion " + tabID).empty();
      $(".tab-content " + tabID).append(circlesToAppend); //apend circles for dekstop view here
    }
  }

  function generate3circles(
    opticOrVdsl,
    btn,
    planDownloadM,
    downloadRateM,
    planUploadM,
    uploadRateM,
    descriptionM,
    startMb_M,
    endMb_M,
    newSpeedDownloadM,
    newSpeedUploadM,
    planDownloadL,
    downloadRateL,
    planUploadL,
    uploadRateL,
    descriptionL,
    startMb_L,
    endMb_L,
    newSpeedDownloadL,
    newSpeedUploadL,
    planDownloadXL,
    downloadRateXL,
    planUploadXL,
    uploadRateXL,
    descriptionXL,
    startMb_XL,
    endMb_XL,
    newSpeedDownloadXL,
    newSpeedUploadXL,
  ) {
    var generateCirclesDiv =
      '<div class="grid-g__col_4 speed">' +
      '<div class="speed__img ' +
      opticOrVdsl +
      'M">' +
      '<p class="speed__text"><span class="' +
      planDownloadM +
      '">' +
      downloadRateM +
      '</span>/<span class="' +
      planUploadM +
      '">' +
      uploadRateM +
      "</span><small>Mb/s</small></p>" +
      "</div>" +
      '<p class="speed__title">MAGIO INTERNET <b>M</b></p>' +
      '<p class="speed__desc">' +
      descriptionM +
      "</p>" +
      "</div>" +
      '<div class="grid-g__col_4 speed">' +
      '<div class="speed__img ' +
      opticOrVdsl +
      'L">' +
      '<p class="speed__text"><span class="' +
      planDownloadL +
      '">' +
      downloadRateL +
      '</span>/<span class="' +
      planUploadL +
      '">' +
      uploadRateL +
      "</span><small>Mb/s</small></p>" +
      "</div>" +
      '<p class="speed__title">MAGIO INTERNET <b>L</b></p>' +
      '<p class="speed__desc">' +
      descriptionL +
      "</p>" +
      "</div>" +
      '<div class="grid-g__col_4 speed">' +
      '<div class="speed__img ' +
      opticOrVdsl +
      'XL">' +
      '<p class="speed__text"><span class="' +
      planDownloadXL +
      '">' +
      downloadRateXL +
      '</span>/<span class="' +
      planUploadXL +
      '">' +
      uploadRateXL +
      "</span><small>Mb/s</small></p>" +
      "</div>" +
      '<p class="speed__title">MAGIO INTERNET <b>XL</b></p>' +
      '<p class="speed__desc">' +
      descriptionXL +
      "</p>" +
      "</div>" +
      '<div class="btn__holder">' +
      '<a class="button btn_mag btn_spd ' +
      btn +
      '" href="#"><span>ZOBRAZ NOVÚ RÝCHLOSŤ</span></a>' +
      "</div>";

    appendCirclesToTabs("." + opticOrVdsl, generateCirclesDiv);

    var circleM = $("." + opticOrVdsl + "M");
    var circleL = $("." + opticOrVdsl + "L");
    var circleXL = $("." + opticOrVdsl + "XL");
    var $button = $("." + btn);
    var downloadRateMnumber = $("." + planDownloadM).text();
    var uploadRateMnumber = $("." + planUploadM).text();
    var downloadRateLnumber = $("." + planDownloadL).text();
    var uploadRateLnumber = $("." + planUploadL).text();
    var downloadRateXLnumber = $("." + planDownloadXL).text();
    var uploadRateXLnumber = $("." + planUploadXL).text();

    createCircle(startMb_M, circleM);
    createCircle(startMb_L, circleL);
    createCircle(startMb_XL, circleXL);

    //animate new speeds on button click
    $button.on("click", function(e) {
      e.preventDefault();
      //circle M
      updateCircle(circleM, startMb_M, endMb_M);
      animNumber(downloadRateMnumber, newSpeedDownloadM, $("." + planDownloadM), "int");
      animNumber(uploadRateMnumber, newSpeedUploadM, $("." + planUploadM), "decimal");
      //circle L
      updateCircle(circleL, startMb_L, endMb_L);
      animNumber(downloadRateLnumber, newSpeedDownloadL, $("." + planDownloadL), "int");
      animNumber(uploadRateLnumber, newSpeedUploadL, $("." + planUploadL), "decimal");
      //circle XL
      updateCircle(circleXL, startMb_XL, endMb_XL);
      animNumber(downloadRateXLnumber, newSpeedDownloadXL, $("." + planDownloadXL), "int");
      animNumber(uploadRateXLnumber, newSpeedUploadXL, $("." + planUploadXL), "int");

      $button.unbind().bind("click", function(e) {
        e.preventDefault();
      });
    });
  }

  /*******************/
  /**** SETTINGS *****/
  /*******************/

  var lastWidth = $(window).width();
  //define tab classes
  var tabOptic = "optic";
  var tabVDSL = "vdsl";

  /* ON WINDOW RESIZE RELOAD CIRCLES */
  $(window).resize(function() {
    if ($(window).width() != lastWidth) {
      // tab OPTIC internet
      generate3circles(
        tabOptic,
        "optic_speed_btn",
        "downloadM",
        15,
        "uploadM",
        1,
        "z 15/1 Mb/s na 30/3 Mb/s",
        0.015,
        0.03,
        30,
        3,
        "downloadL",
        60,
        "uploadL",
        6,
        "z 60/6 Mb/s na 120/12 Mb/s",
        0.06,
        0.12,
        120,
        12,
        "downloadXL",
        300,
        "uploadXL",
        30,
        "z 300/30 Mb/s na 600/60 Mb/s",
        0.3,
        0.6,
        600.0,
        60.0,
      );
      // tab VDSL internet
      generate3circles(
        tabVDSL,
        "vdsl_speed_btn",
        "vdsl_downloadM",
        8,
        "vdsl_uploadM",
        1,
        "z 8/1 Mb/s na 10/1 Mb/s",
        0.08,
        0.1,
        10,
        1,
        "vdsl_downloadL",
        30,
        "vdsl_uploadL",
        3,
        "z 30/3 Mb/s na 40/4 Mb/s",
        0.3,
        0.4,
        40,
        4,
        "vdsl_downloadXL",
        80,
        "vdsl_uploadXL",
        8,
        "z 80/8 Mb/s na 90/9 Mb/s",
        0.8,
        0.9,
        90,
        9,
      );

      lastWidth = $(window).width();
    }
  });

  /* INIT LOAD CIRCLES */
  generate3circles(
    tabOptic,
    "optic_speed_btn",
    "downloadM",
    15,
    "uploadM",
    1,
    "z 15/1 Mb/s na 30/3 Mb/s",
    0.015,
    0.03,
    30,
    3,
    "downloadL",
    60,
    "uploadL",
    6,
    "z 60/6 Mb/s na 120/12 Mb/s",
    0.06,
    0.12,
    120,
    12,
    "downloadXL",
    300,
    "uploadXL",
    30,
    "z 300/30 Mb/s na 600/60 Mb/s",
    0.3,
    0.6,
    600.0,
    60.0,
  );

  generate3circles(
    tabVDSL,
    "vdsl_speed_btn",
    "vdsl_downloadM",
    8,
    "vdsl_uploadM",
    1,
    "z 8/1 Mb/s na 10/1 Mb/s",
    0.08,
    0.1,
    10,
    1,
    "vdsl_downloadL",
    30,
    "vdsl_uploadL",
    3,
    "z 30/3 Mb/s na 40/4 Mb/s",
    0.3,
    0.4,
    40,
    4,
    "vdsl_downloadXL",
    80,
    "vdsl_uploadXL",
    8,
    "z 80/8 Mb/s na 90/9 Mb/s",
    0.8,
    0.9,
    90,
    9,
  );
});
