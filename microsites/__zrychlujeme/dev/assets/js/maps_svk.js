$(function() {
  function createMap(mapIDattr, mapParentWidth, regionDone, dataProperty) {
    var accordionContainer = $(".accordion #" + mapIDattr);
    accordionContainer.empty();
    var container = $("#" + mapIDattr);
    container.empty();

    var height = 400;
    var width = mapParentWidth;
    var mapWrapperDiv;

    //map wrapper for mobile
    if (window.innerWidth <= 768) {
      var a = document.querySelector(".mapssection");
      var x = a.querySelector(".accordion");
      var y = x.querySelector("#" + mapIDattr);
      mapWrapperDiv = y;
    } else {
      //map wrapper for desktop
      mapWrapperDiv = document.getElementById(mapIDattr);
    }

    var map = new Datamap({
      scope: "svk",
      responsive: true,
      fills: {
        defaultFill: "#f5a6ce",
        //speedUpDone: "#6bb324"
      },
      data: {
        // BA: { fillKey: "speedUpDone" },
      },
      element: mapWrapperDiv,
      geographyConfig: {
        highlightFillColor: "#e20074",
        popupTemplate: function(geography, data) {
          return '<div class="hoverinfo"><b>' + geography.properties.name + " kraj</b><br/>" + geography.properties[dataProperty] + "</div> ";
        },
      },
      setProjection: function(element) {
        var projection = d3.geo
          .mercator()
          .scale(width * 8.45) //7300
          .center([19.680074266824, 48.63670023565])
          .translate([width / 2, height / 2]);

        var path = d3.geo.path().projection(projection);

        return {
          path: path,
          projection: projection,
        };
      },
    });

    //highligh region to green if speedUp in this region was implemented
    regionDone.forEach(function(region) {
      var region_changeColor = {};
      region_changeColor[region] = "#6bb324";
      map.updateChoropleth(region_changeColor);
    });

    map.labels({
      labelColor: "white",
      fontSize: 12,
    });

    //map.resize();
  }

  //first tab init load
  $(window).on("resize", function() {
    createMap("mapoptic", $(".container-fixed").width(), ["BA", "ZA", "BB", "TT", "TN", "PO", "NR", "KE"], "optic");
  });

  //init map load after tab changed;
  //MOBILE view
  $(window).on("resize", function() {
    if (window.innerWidth <= 768) {
      createMap("mapvdsl", $(".container-fixed").width(), ["TN", "PO", "NR", "KE", "TT", "BB", "ZA", "BA"], "vdsl");
    }
  });

  //init map load after tab changed;
  //DESKTOP view
  $("a[href='#vdslspeed']").on("click", function() {
    createMap("mapvdsl", $(".container-fixed").width(), ["TN", "PO", "NR", "KE", "TT", "BB", "ZA", "BA"], "vdsl");
    $(window).on("resize", function() {
      createMap("mapvdsl", $(".container-fixed").width(), ["TN", "PO", "NR", "KE", "TT", "BB", "ZA", "BA"], "vdsl");
    });
  });
});
