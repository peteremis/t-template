$(function () {

    let $esales_form = $('#esales_forms #esales_form');
    let $esales_form_input = $('#esales_forms #esales_form input');
    let $esales_form_submit = $('#esales_formSubmitBtn');

    // initialize the validation plugin
    $esales_form.validate({ 
        rules: {
            firstName: {
                required: true,
                minlength: 2,
                lettersonly: true
            },
            lastName: {
                required: true,
                minlength: 2,
                lettersonly: true
            },
            email: {
                required: true,
                email: true,
                customEmailValidation: true
            },
            phoneNum: {
                required: true,
                customNumberValidation: true
            }
        },
        messages: {
            firstName: {
                required: 'Toto pole je povinné. Napíšte vaše meno.',
                minlength: 'Uveďte minimálne 2 znaky.',
                lettersonly: 'Vaše meno nemôže obsahovať číslo.'
            },
            lastName: {
                required: 'Toto pole je povinné. Napíšte vaše priezvisko.',
                minlength: 'Uveďte minimálne 2 znaky.',
                lettersonly: 'Vaše priezvisko nemôže obsahovať číslo.'
            },
            email: {
                required: 'Toto pole je povinné. Napíšte váš e-mail.',
                email: 'Napíšte váš e-mail v tvare vas@email.sk',
                customEmailValidation: "Prosím, uveďte váš e-mail v správnom tvare vas@email.sk"
            },
            phoneNum: {
                required: 'Toto pole je povinné. Napíšte vaše telefónne číslo.',
                customNumberValidation: "Prosím uveďte tel.číslo v tvare 0911 222 333."
            }
        },
        errorClass: "error",
        validClass: "valid",
        highlight: function (element, errorClass, validClass) {
            $(element).parents('.form-group').addClass(errorClass).removeClass(validClass)
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents('.form-group').removeClass(errorClass).addClass(validClass)
        }
    });
    // custom phone number validation rule
    $.validator.addMethod("customNumberValidation",
        function (value, element) {
            return /^(((\+4219)|(09))[0-9]{2}\s*[0-9]{3}\s*[0-9]{3})$/.test(value)
        }
    );
    // custom email validation rule
    $.validator.addMethod("customEmailValidation",
        function (value, element) {
            return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value) &&
                /^(?=.{1,64}@.{4,64}$)(?=.{6,100}$).*/.test(value)
        }
    );
    // custom validation allow letters only
    jQuery.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-z]+$/i.test(value);
      }
    ); 

    //check if all input fields are filled
    let allInputs = [];
    let allInputsFilled = null;
    function checkIfAllInputsFilled() {
        allInputs = [];
        $esales_form_input.each(function(index, input) {
            if ($(input).val().length !== 0) {
                allInputs[index] = true;
            } else {
                allInputs[index] = false;
            }
        });

        allInputsFilled = allInputs.reduce(function(a, b) { 
            return (a === b) ? a : false; 
        })
    }

    // on input keyup, check if form is valid and all input fields are filled
    // enable / disable submit button
    $esales_form_input.on('keyup', function(e) {
        checkIfAllInputsFilled();

        if (allInputsFilled === true && $esales_form.valid()) {
            if ($esales_form_submit.hasClass('disabled') === true) {
                $esales_form_submit.removeClass('disabled').prop('disabled', false);
            }
        } else {
            if ($esales_form_submit.hasClass('disabled') !== true) {
                $esales_form_submit.addClass('disabled').prop('disabled', true);
            }
        }
    });

    // submit form
    $esales_form_submit.on("click", function (e) {
        e.preventDefault();
        alert("formular je validny a moze byt odoslany ...");
        // SEND FORM ...
    });


    // form input labels behavior
    $esales_form_input.focus(function () {
        $(this).parents('.form-group').addClass('focused');
    });
    $esales_form_input.blur(function () {
        let inputValue = $(this).val();
        if (inputValue === "" && $(this).valid() === false) {
            $(this).removeClass('filled');
            $(this).parents('.form-group').removeClass('focused');
        } else {
            $(this).addClass('filled');
        }
    });
});
