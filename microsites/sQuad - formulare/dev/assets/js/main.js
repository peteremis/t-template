$(function () {

    const $esales_form = $('#esales_forms #esales_form');
    const $esales_form_input = $('#esales_forms #esales_form input');
    const $esales_form_submit = $('#esales_formSubmitBtn');
    const $termsAgreeCheckbox = $('#termsAgree');

    // initialize the validation plugin
    $esales_form.validate({ 
        rules: {
            firstName: {
                required: true,
                minlength: 3,
            },
            lastName: {
                required: true,
                minlength: 2,
            },
            email: {
                required: true,
                email: true,
                customEmailValidation: true
            },
            phoneNum: {
                required: true,
                customNumberValidation: true,
                maxlength: 10
            },
            phoneNumCourier: {
                customNumberValidation: true
            },
            birthNum: {
                birthNumValidation: true
            },
            userId: {
                maxlength: 8,
                userIdValidation: true
            },
            streetName: {
                required: true,
                minlength: 3
            },
            streetNo: {
                required: true,
                streetNoValidation: true
            },
            zipCodeNo: {
                required: true,
                zipCodeNo: true
            },
            cityName: {
                required: true,

            },
            termsAgree: {
                required: true
            }
        },
        messages: {
            firstName: {
                required: 'Napíšte svoje meno.',
                minlength: 'Meno musí mať aspoň 3 znaky.'
            },
            lastName: {
                required: 'Napíšte svoje priezvisko.',
                minlength: 'Priezvisko musí mať aspoň 3 znaky.'
            },
            email: {
                required: 'Napíšte váš e-mail.',
                email: 'Napíšte svoj e-mail v tvare vas@email.sk',
                customEmailValidation: 'Napíšte email v správnom tvare: vas@email.sk'
            },
            phoneNum: {
                required: 'Napíšte svoje mobilné telefónne číslo.',
                customNumberValidation: 'Napíšte tel. číslo v tvare 0911 222 333.',
                maxlength: 'Telefónne číslo môže obsahovať maximálne 10 číslic.'
            },
            phoneNumCourier: {
                customNumberValidation: 'Napíšte tel. číslo v tvare 0911 222 333.',
            },
            birthNum: {
                birthNumValidation: 'Vložte správne rodné číslo.' // TODO
            },
            userId: {
                maxlength: 'Vložte maximálne 8 znakov.',
                userIdValidation: 'Formát občianskeho preukazu nie je platný.'
            },
            streetName: {
                required: 'Napíšte ulicu vášho trvalého bydliska.',
                minlength: 'Ulica musí mať aspoň 3 znaky.'
            },
            streetNo: {
                required: 'Napíšte číslo domu.',
                streetNoValidation: 'Číslo domu musí obsahovať aspoň 1 číslo.'
            },
            zipCodeNo: {
                required: 'PSČ musí byť v tvare 83106.',
                zipCodeNo: 'PSČ musí byť v tvare 831 06.'
            },
            cityName: {
                required: 'Napíšte mesto.',
            },
            termsAgree: {
                required: 'Zaškrtnite prosím súhlas s podmienkami.'
            }
        },
        errorClass: "error",
        validClass: "valid",
        highlight: function (element, errorClass, validClass) {
            $(element).parents('.form-group').addClass(errorClass).removeClass(validClass)
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents('.form-group').removeClass(errorClass).addClass(validClass)
        },
        // error form field

        errorPlacement: function(error, element) {
            var placement = $(element).data('error');
            if (placement) {
                //$(placement).append(error)
            } else {
                //error.insertAfter(element);
                for (let index = 0; index < element.length; index++) {
                    const el = element[index];
                    if ($(el).attr('id') === $termsAgreeCheckbox.attr('id')) {
                        error.insertAfter($(el).closest('.form-group').find('label'));
                    } else {
                        error.insertAfter($(el).closest('.form-group').find('input'));
                    }
                    
                }
            }
        }
    });
    
    // custom phone number validation rule
    $.validator.addMethod("customNumberValidation",
        function (value, element) {
            if ($(element).val() === '') {
                return true;
            } else {
                return /^(((\+4219)|(09))[0-9]{2}\s*[0-9]{3}\s*[0-9]{3})$/.test(value)
            }
        }
    );

    // custom email validation rule
    $.validator.addMethod("customEmailValidation",
        function (value, element) {
            return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value) &&
                /^(?=.{1,64}@.{4,64}$)(?=.{6,100}$).*/.test(value)
        }
    );

    // custom birth number validation rule
    $.validator.addMethod("birthNumValidation",
        function (value, element) {
            if ($(element).val() === '') {
                return true;
            } else {
                // birthNumValidator.js
                return birthNumValidate(value, BIRTH_NUMBER_OVERRIDE)
            }
        }
    );

    // custom userId validation rule
    $.validator.addMethod("userIdValidation",
        function (value, element) {
            if ($(element).val() === '') {
                return true;
            } else {
                return /[A-Z]{2}\d{6}/.test(value);
            }
        }
    );

    // custom street number validation rule
    $.validator.addMethod("streetNoValidation",
        function (value, element) {
                //return /^[0-9a-zA-Z\/]+$/.test(value);
                return /\d+[\/]?\w|\d+$/.test(value);
        }
    );

    // custom ZIP Code validation rule
    $.validator.addMethod("zipCodeNo",
        function (value, element) {
                return /^\d{3}(\s|\/)?\d{2}$/.test(value);
        }
    );


    //https://stackoverflow.com/questions/150033/regular-expression-to-match-non-english-characters
    const digitsOnly = /[1234567890]/;
    const textCharsOnly = /[a-zA-z]|[^\u0000-\u007F]+/;
    const disallowedSpecialChars = /[\W_]/;
    const allowedSlovakSpecialChars = /[ľščťžýáíéúäôóňďŕřĺĽŠČŤŽÝÁÍÉÚÄÔÓŇĎŔĹŘ]/;

    const helperFunctions = {
        allowOnlySlovakSpecialChars: function(charCode) {
            console.log(allowedSlovakSpecialChars.test(String.fromCharCode(charCode)))
            if (allowedSlovakSpecialChars.test(String.fromCharCode(charCode)) || disallowedSpecialChars.test(String.fromCharCode(charCode))) {
                if (allowedSlovakSpecialChars.test(String.fromCharCode(charCode))) {
                    return true;
                } else if (disallowedSpecialChars.test(String.fromCharCode(charCode))) {
                    return false;
                }
            } else {
                return true;
            }
        },
        allowNumbers: function(charCode){
            console.log(digitsOnly.test(String.fromCharCode(charCode)))
            return digitsOnly.test(String.fromCharCode(charCode));
        },
    
        allowText: function(charCode){
            return textCharsOnly.test(String.fromCharCode(charCode));
        },
        // prevent numeric input
        allowOnlyTextCharacters: function(e) {
            var charCode = (e.keyCode ? e.keyCode : e.which);
                if (this.allowText(charCode) === false || this.allowOnlySlovakSpecialChars(charCode) === false) {
                    e.preventDefault();
                }
        },
        // prevent alphabet characters input
        allowOnlyNumbers: function(e) {
            var charCode = (e.keyCode ? e.keyCode : e.which);
                if (this.allowNumbers(charCode) === false || this.allowOnlySlovakSpecialChars(charCode) === false) {
                    e.preventDefault();
                }
        }
    }

    // INPUT KEYPRESS
    $($esales_form_input).keypress(function(e) {
        if ($(this).attr('name').indexOf('Name') !== -1) {
            helperFunctions.allowOnlyTextCharacters(e);
        } else if ($(this).attr('name').indexOf('Num') !== -1) {
            helperFunctions.allowOnlyNumbers(e);
        } else if ($(this).attr('name').indexOf('zip') !== -1) {
            if($(this).val().length <= 4) {
                var res = $(this).val().replace(/(\d{3})/g, '$1 ').trim(); //format ZIP CODE to "XXX XX"
                $(this).val(res);
            }
            helperFunctions.allowOnlyNumbers(e);
        }
    });

    // THANK YOU PAGE
    const thankYouPage = '#thankYouPage';
    const $esales_formWrapper = $('.esales_form-wrapper');
    const generatedThankYouPage = 
        '<div id="thankYouPage" class="esales_form-wrapper--thankYouPage">'+
            '<h3>Ďakujeme za Vašu objednávku,</h3>'+
            '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. </p>'+
        '</div>'

    // SUBMIT FORM
    $esales_form_submit.on("click", function (e) {
        e.preventDefault();
        //check if form is valid
        if ($esales_form.valid()) {
            // SEND FORM ...
            // ON SUCCESS > SHOW THANK YOU PAGE
            $esales_form.hide();
            $esales_formWrapper.append(generatedThankYouPage).find(thankYouPage).show();
        } else {
            alert("formular nie je validny ...");
            e.preventDefault();
        }
    });


    // FORM INPUTS & LABELS BEHAVIOR
    function add_inputPrefix($inputEl, prefix, inputId) {
       
        if ($inputEl.attr('id') === inputId) {
            if ($inputEl.val().length > 0) {
                return;
            } else {
                $inputEl.val(prefix);
            }
        }
    }

    $esales_form_input.focus(function () {
        $(this).parents('.form-group').addClass('focused');
        add_inputPrefix($(this), "09", 'phoneNum'); // phoneNum input field add prefix "09" on focus
        add_inputPrefix($(this), "09", 'phoneNumCourier'); //phoneNumCourier input field add prefix "09" on focus
    });
    
    $esales_form_input.blur(function () {
        let inputValue = $(this).val();
        if (inputValue === "") {  //  && $(this).valid() === false
            $(this).removeClass('filled');
            $(this).parents('.form-group').removeClass('focused');
        } else {
            $(this).addClass('filled');
        }
    });
});