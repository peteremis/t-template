
    let BIRTH_NUMBER_REGEX = "^[0-9]{6}/?[0-9]{3,4}$";
    let BIRTH_NUMBER_OVERRIDE = "0000";

    function getMonthFromBirthNumber(value) {
        let month = parseInt(value.substring(2, 4));
        let year = getYearFromBirthNumber(value);

        if (month > 70 && month < 83 && year > 2003) {
            // A woman after 2004.
            month = month - 70;
        }

        if (month > 50 && month < 63) {
            // A woman.
            month = month - 50;
        }

        if (month > 20 && month < 33 && year > 2003) {
            // A man after 2004.
            month = month - 20;
        }

        return month;
    }

    function getYearFromBirthNumber(value) {
        let rawValue = value.replace("/", "");
        let year = parseInt(value.substring(0, 2));

        if (rawValue.length == 9 || year >= 54) {
            year += 1900;
        } else if (year < 54) {
            year += 2000;
        }

        return year;
    }

    function getDayFromBirthNumber(value){
        return parseInt(value.substring(4, 6));
    }

    function birthNumValidate(value, BIRTH_NUMBER_OVERRIDE) {
        if (value == null || value.trim() == '') {
            return true;
        }
        if (!value.match(BIRTH_NUMBER_REGEX) && !value.match(BIRTH_NUMBER_OVERRIDE)) {
            return false;
        }

        let month = getMonthFromBirthNumber(value);

        let day = getDayFromBirthNumber(value);

        if (month > 12 || day > 31) {
            return false;
        }

        let rawValue = value.replace("/", "");

        if (rawValue.length == 10 && rawValue.slice(6, 10).match(BIRTH_NUMBER_OVERRIDE)) {
            // foreigner, don't check modulo 11
            return true;
        }

        // it is ok if modulo is 0 or if modulo is 10 and last digit is 0
        let lastNumber = 0;
        if (rawValue.length == 10) {
            lastNumber = parseInt(rawValue.substring(9, 10));

            let m = parseInt(rawValue.substring(0, 9)) % 11;
            if (m == 10)
                m = 0;
            if (m != lastNumber)
                return false;
        } else if (rawValue.length == 9 && parseInt(rawValue.substring(0, 2)) > 54) {
            return false;
        }
        return true;
    }