$('#select_program').heapbox({
    "onChange":function(val, elm) {
        $('.bonus .bonus-item').hide(200);
        $('.bonus .bonus-item').eq(elm.val()).show(200);
     }
});

$(".scroll-to").click(function () {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);

        target = target.length ? target : $('.' + this.hash.slice(1));
        if (target.length) {
            $('html,body').animate({
                scrollTop: target.offset().top
            }, 1000);
            return false;
        }
    }
}); 
