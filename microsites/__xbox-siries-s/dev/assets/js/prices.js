var pricelist = {
    "is_orderable": 1,
    "is_visible": 1,
    "prices": {
        "M": {
            "oc": 149.00,
            "oc_sale_web": 119.00,
            "rc": 6,
            "rc_sale_web": 6,
            "rpln": {
                "taxless_price": 9.08,
                "tax_price": 10.90
                }
            },
        "L": {
            "oc": 129.00,
            "oc_sale_web": 99.00,
            "rc": 6,
            "rc_sale_web": 6,
            "rpln": {
                "taxless_price": 13.25,
                "tax_price": 15.90
                }
            },
        "XL": {
            "oc": 89.00,
            "oc_sale_web": 59.00,
            "rc": 6,
            "rc_sale_web": 6,
            "rpln": {
                "taxless_price": 17.41,
                "tax_price": 20.90
                }
            }
    }
};