$(document).ready(function () {

    function showPopup() {
        $('.form-popup').show();
        $('.fp__overlay').show();
    }
    
    function hidePopup() {
        $('.form-popup').hide();
        $('.fp__overlay').hide();
    }

    $('.fp__close').on('click', function () {
        $('.form-popup').hide();
        $('.fp__overlay').hide();
    });

    if ($('.portlet-msg-success').length > 0) {
        
        showPopup();
        setTimeout(hidePopup, 10000);

    }



});