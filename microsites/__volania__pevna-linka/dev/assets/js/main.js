$(function() {

    $('.rpln-m').qtip({
        content: {
            text:
                'Neobmedzené hovory vám poskytujeme vďaka <br> Zásadám férového používania: <br> 1000 minút - do mobilnej siete Telekom',
        },
        position: {
            my: 'top center',
            at: 'bottom center',
        },
    });
    $('.rpln-l').qtip({
        content: {
            text:
                'Neobmedzené hovory vám poskytujeme vďaka<br> Zásadám férového používania:<br> 1000 minút - do mobilných sietí v SR',
        },
        position: {
            my: 'top center',
            at: 'bottom center',
        },
    });
    $('.rpln-xl').qtip({
        content: {
            text:
                'Neobmedzené hovory vám poskytujeme vďaka <br> Zásadám férového používania:<br> 1000 minút - do mobilných sietí v SR<br> 1000 minút - do EÚ',
        },
        position: {
            my: 'top center',
            at: 'bottom center',
        },
    });

      /* scroll to + sticky nav */
  $("#nav-sticky").sticky({
    topSpacing: 0,
    widthFromWrapper: true,
  });

  $(".scroll-to").click(function () {
    if (
      location.pathname.replace(/^\//, "") ==
        this.pathname.replace(/^\//, "") &&
      location.hostname == this.hostname
    ) {
      var target = $(this.hash);
      target = target.length ? target : $("[name=" + this.hash.slice(1) + "]");
      if (target.length) {
        $("html,body").animate(
          {
            scrollTop: target.offset().top - $("#nav-sticky").outerHeight(),
          },
          1000
        );
        return false;
      }
    }
  });

  var contentSections = $("[id*='sec-']"),
    secondaryNav = $("#nav-sticky");

  function updateSecondaryNavigation() {
    contentSections.each(function () {
      var actual = $(this),
        actualHeight =
          actual.height() +
          parseInt(actual.css("paddingTop").replace("px", "")) +
          parseInt(actual.css("paddingBottom").replace("px", "")),
        actualAnchor = secondaryNav.find(
          'a[href="#' + actual.attr("id") + '"]'
        );

      if (
        actual.offset().top - secondaryNav.outerHeight() <=
          $(window).scrollTop() &&
        actual.offset().top + actualHeight - secondaryNav.outerHeight() >
          $(window).scrollTop()
      ) {
        actualAnchor.addClass("active");
      } else {
        actualAnchor.removeClass("active");
      }
    });
  }
  updateSecondaryNavigation();

  $(window).scroll(function (event) {
    updateSecondaryNavigation();
  });
  /* scroll to + sticky nav END */

});
