var gulp            = require('gulp'),
    sass            = require('gulp-sass'),
    sourcemaps      = require('gulp-sourcemaps'),
    uglify          = require('gulp-uglify'),
    browserSync     = require('browser-sync').create(),
    reload          = browserSync.reload,
    includer        = require('gulp-html-ssi'),
    es              = require('event-stream'),
    globalAssets    = '../../global-assets/theme/stwbmain',
    globalTemplates = '../../global-assets/templates/*.html',
    localTemplates  = 'dev/templates/*.html';


gulp.task('htmlSSI', function() {
        es.merge(gulp.src(globalTemplates), gulp.src(localTemplates))
        .pipe(includer())
        .pipe(gulp.dest('dev/server/'));
});


gulp.task('sass', function () {
    gulp.src('dev/assets/sass/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass.sync().on('error', sass.logError))
        .pipe(sass({outputStyle: 'compact'}))
        .pipe(sourcemaps.write('/maps'))
        .pipe(gulp.dest('dev/server/assets/css'));
});

gulp.task('js', function () {
    return gulp.src('dev/assets/js/*.js')
        .pipe(uglify())
        .on('error', console.error.bind(console))
        .pipe(gulp.dest('dev/server/assets/js'));
});

gulp.task('serve', function () {
    
    browserSync.init({
        server: {
            baseDir: ["./dev", "./dev/server", "./../../global-assets"]
        }
    });
    
    gulp.watch('dev/assets/sass/**', ['sass']);
    gulp.watch('dev/assets/sass/**').on('change', reload);
    gulp.watch('../../global-assets/sass/**', ['sass']);
    gulp.watch('../../global-assets/sass/**').on('change', reload);
    gulp.watch('dev/assets/js/*.js', ['js']);
    gulp.watch('dev/assets/js/*.js').on('change', reload);
    gulp.watch('dev/templates/*.html', ['htmlSSI']);
    gulp.watch('dev/templates/*.html').on('change', reload);
    gulp.src('dev/assets/img/**')
    .pipe(gulp.dest('dev/server/assets/img'));
});

gulp.task('deploy', function() {
    gulp.src('dev/templates/_main-content.html')
    .pipe(gulp.dest('deploy'));
    gulp.src('dev/server/assets/img/**')
    .pipe(gulp.dest('deploy/img'));
    gulp.src('dev/server/assets/js/main.js')
    .pipe(gulp.dest('deploy/assets'));
    gulp.src('dev/server/assets/css/style.css')
    .pipe(gulp.dest('deploy/assets'));
});

gulp.task('default', ['sass', 'js', 'htmlSSI', 'serve']);