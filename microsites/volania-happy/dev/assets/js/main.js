$(function () {

    $("#nav-sticky").sticky({
        topSpacing: 0,
        widthFromWrapper: true
    });

    $(".scroll-to").click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top - ($("#nav-sticky").outerHeight())
                }, 1000);
                return false;
            }
        }
    });

    var contentSections = $("[id*='sec-']"),
        secondaryNav = $("#nav-sticky");

    function updateSecondaryNavigation() {
        contentSections.each(function () {
            var actual = $(this),
                actualHeight = actual.height() + parseInt(actual.css('paddingTop').replace('px', '')) + parseInt(actual.css('paddingBottom').replace('px', '')),
                actualAnchor = secondaryNav.find('a[href="#' + actual.attr('id') + '"]');

            if ((actual.offset().top - secondaryNav.outerHeight() <= $(window).scrollTop()) && (actual.offset().top + actualHeight - secondaryNav.outerHeight() > $(window).scrollTop())) {
                actualAnchor.addClass('active');
            } else {
                actualAnchor.removeClass('active');
            }

        });
    }
    updateSecondaryNavigation();
    
    $(window).scroll(function (event) {
        updateSecondaryNavigation();
    });

    if (getParameterByName('data-navyse')) {
        scrollTo('#line-navyse', 50);
    }

    function getParameterByName(name) {
        var found = false;
        if(window.location.href.indexOf(name) > -1) {
            found=true;
        }

        return found;
    }

    $('#vianocne-akcie').click(function () {
        scrollTo('.c-row-3', 0);
        return false;
    });

    $('.c-row-1 .c-tile-head img').qtip({
        content: 'Vybraný smartfón ako darček získate k Happy paušálu so Sony Xperia M2.',
        style: { 'font-size': 12 },
        position: {
            corner: {
                target: 'leftMiddle',
                tooltip: 'topMiddle'
            }
        }
    })

    $('#balicky .left img').qtip({
        content: 'Balíček nie je dostupný pre<br>Happy XS a S',
        style: { 'font-size': 12 },
        position: {
            corner: {
                target: 'leftMiddle',
                tooltip: 'topMiddle'
            }
        }
    })

    $('#info-data-navyse').qtip({
        content: 'Platí pre paušál Happy M a vyšší.',
        position: {
            corner: {
                target: 'leftMiddle',
                tooltip: 'topMiddle'
            }
        }
    })


    $('#info-zdielanie').qtip({
        content: 'Službu Zdieľanie dát z paušálu na tablete či druhom telefóne dostanete ku každému paušálu Happy L a vyššiemu na 24 mesiacov zadarmo.',
        position: {
            corner: {
                target: 'leftMiddle',
                tooltip: 'topMiddle'
            }
        }
    })

    $('.happy-desc .row-5-head img').qtip({
        content: 'S následnými neobmedzenými<br>e-mailmi a surfovaním',
        position: {
            corner: {
                target: 'leftMiddle',
                tooltip: 'topMiddle'
            }
        }
    })


    $('.c-row-5 .box-header img').qtip({
        content: 'Platí pre odchádzajúce roamingové hovory z EÚ do SR za mesačný poplatok 2 €.',
        position: {
            corner: {
                target: 'leftMiddle',
                tooltip: 'topMiddle'
            }
        }
    })



    $('.c-row-6 .c-tile-1 .c-tile-head img').qtip({
        content: 'Zľava sa uplatňuje 20 mesiacov v náležite rozrátanej sume.<br>Platí len pre paušály Happy.',
        position: {
            corner: {
                target: 'leftMiddle',
                tooltip: 'topMiddle'
            }
        }
    })

    $('.happy-desc .row-8-head img').qtip({
        content: 'Jednorazová zľava, ktorú získate iba pri kúpe paušálu na našom webe.<br>Konkrétna výška zľavy bude vypočítaná pri nákupe.',
        position: {
            corner: {
                target: 'leftMiddle',
                tooltip: 'topMiddle'
            }
        }
    })
    
    $('.benefit1').qtip({
        content: '"i" Bonus na paušál vám uplatníme formou zľavy z mesačného poplatku vo výške 7,50 € počas 20 mesiacov. Bonus na telefón vo výške 50 € dostanete okamžite pri prenose čísla formou zľavy z kúpnej ceny telefónu.',
        position: {
            corner: {
                target: 'leftMiddle',
                tooltip: 'topMiddle'
            }
        }
    })
    
    $('.benefit2').qtip({
        content: '"i" Bonus na paušál vám uplatníme formou zľavy z mesačného poplatku vo výške 5 € počas 20 mesiacov. Bonus na telefón vo výške 50 € dostanete okamžite pri prenose čísla formou zľavy z kúpnej ceny telefónu.',
        position: {
            corner: {
                target: 'leftMiddle',
                tooltip: 'topMiddle'
            }
        }
    })
    
    $('.benefit3').qtip({
        content: '"i" Bonus na paušál vám uplatníme formou zľavy z mesačného poplatku vo výške 2,5 € počas 20 mesiacov. Bonus na telefón vo výške 50 € dostanete okamžite pri prenose čísla formou zľavy z kúpnej ceny telefónu.',
        position: {
            corner: {
                target: 'leftMiddle',
                tooltip: 'topMiddle'
            }
        }
    })
    
    $('.benefit4').qtip({
        content: '"i" Bonus na paušál vám uplatníte formou zľavy z mesačného poplatku vo výške 2,50 € počas 20 mesiacov. Bonus na telefón vo výške 50 € vám pošleme ako SMS kupón, ktorý má platnosť 6 mesiacov.  Využijete ho vo forme zľavy z kúpnej ceny telefónu. Môžete ho využiť vy alebo ho niekomu darovať.',
        position: {
            corner: {
                target: 'leftMiddle',
                tooltip: 'topMiddle'
            }
        }
    })
    
    $('.benefit5').qtip({
        content: '"i" Bonus 100 € na paušál vám uplatníme formou zľavy z mesačného poplatku vo výške 5,00 € počas 20 mesiacov. 100 € bonus na telefón dostanete vo forme dvoch 50 € kupónov, ktoré vám pošleme cez SMS. Kupóny sa dajú využiť ako zľava na kúpnu cenu telefónu. Na jeden telefón sa dá uplatniť len jeden kupón. Kupóny majú platnosť 6 mesiacov. Môžete ich využiť vy alebo ich darovať blízkemu.',
        position: {
            corner: {
                target: 'leftMiddle',
                tooltip: 'topMiddle'
            }
        }
    })

    function showContent(id) {
        $('.content-desc').hide();
        $('#' + id).show();

        scrollTo('#' + id, 0);
    }

    $('#sluzby .c-tile-1 .c-tile-more span').click(function (event) {

        event.preventDefault();
        showContent('balicky');

        if (window.matchMedia('(max-width: 896px)').matches) {
        } else {
            $(".c-row-4 .c-tile").css({"border-bottom": "1px solid #d4d4d4"});
            $(".c-row-4 .c-tile-1").css({"border-bottom": "none"});
        }
    });

    $('#sluzby .c-tile-2 .c-tile-more span').click(function (event) {

        event.preventDefault();
        showContent('volania');

        if (window.matchMedia('(max-width: 896px)').matches) {
        } else {
            $(".c-row-4 .c-tile").css({"border-bottom": "1px solid #d4d4d4"});
            $(".c-row-4 .c-tile-2").css({"border-bottom": "none"});

        }

    });

    $('#sluzby .c-tile-3 .c-tile-more span').click(function (event) {

        event.preventDefault();
        showContent('siet100');

        if (window.matchMedia('(max-width: 896px)').matches) {
        } else {
            $(".c-row-4 .c-tile").css({"border-bottom": "1px solid #d4d4d4"});
            $(".c-row-4 .c-tile-3").css({"border-bottom": "none"});

        }

    });

    $('#sluzby .c-tile-4 .c-tile-more span').click(function (event) {

        event.preventDefault();
        showContent('sms');

        if (window.matchMedia('(max-width: 896px)').matches) {
        } else {
            $(".c-row-4 .c-tile").css({"border-bottom": "1px solid #d4d4d4"});
            $(".c-row-4 .c-tile-4").css({"border-bottom": "none"});

        }
    });

    $('#orderphone').click(function(){ happyLink('https://www.telekom.sk/objednavka/-/scenario/e-shop/hw-happy/vyber-hw'); return false; });
    $('#orderprenos').click(function(){ happyLink('https://www.telekom.sk/volania/prenos-cisla'); return false; });
    $('#happyroaming').click(function(){ happyLink('https://www.telekom.sk/volania/roaming#happyroaming '); return false; });

    $('.c-row-7 .box-1').click(function(){ happyLink('https://www.telekom.sk/objednavka/-/scenario/e-shop/happy'); return false; });
    $('.c-row-7 .box-2').click(function(){ happyLink('https://www.telekom.sk/objednavka/-/scenario/e-shop/happy'); return false; });
    $('.c-row-7 .box-3-a').click(function(){ happyLink('https://www.telekom.sk/objednavka/-/scenario/e-shop/happy'); return false; });
    $('.c-row-7 .box-3-b').click(function(){ happyLink('https://www.telekom.sk/objednavka/-/scenario/e-shop/happy'); return false; });

    function happyLink(url) {
        window.location.href = url;
    }

    $('.table-container').animate({scrollLeft: '+=165'}, 1);

    var actual = 3;
    var child = 3;

    $("table.happy").find('td:nth-child(' + child + ')').addClass("activeBox");

    $('#next-column').click(function (event) {

        event.preventDefault();
        $('.table-container').animate({scrollLeft: '+=165'}, 'slow');

        actual = actual + 1;

        if (actual>7) {
            actual = 8;
            child = 8;
        } else if (actual==7) {
            child = 7;
        } else if (actual==6) {
            child = 6;
        } else if (actual==5) {
            child = 5;
        } else if (actual==4) {
            child = 4;
        } else if (actual==3) {
            child = 3;
        }

        $("table.happy").find('td').removeClass("activeBox");
        $("table.happy").find('td:nth-child(' + child + ')').addClass("activeBox");

    });

    $('#previous-column').click(function (event) {

        event.preventDefault();
        $('.table-container').animate({scrollLeft: '-=165'}, 'slow');

        actual = actual-1;

        if (actual<2) {
            actual = 2;
            child = 2;
        } else if (actual==2) {
            child = 2;
        } else if (actual==3) {
            child = 3;
        } else if (actual==4) {
            child = 4;
        } else if (actual==5) {
            child = 5;
        } else if (actual==6) {
            child = 6;
        } else if (actual==7) {
            child = 7;
        }

        $("table.happy").find('td').removeClass("activeBox");
        $("table.happy").find('td:nth-child(' + child + ')').addClass("activeBox");
    });

    var scrollingSpeed = 1200;

    $('#btn-order-4g').click(function () {
        scrollTo('.c-row-4', 0);
        return false;
    });

    $('#order-top').click(function () {
        scrollTo('.c-row-table', 0);
        return false;
    });

    $('.c-row-2 .blue').click(function () {
        scrollTo('.c-row-7', 0);
        return false;
    });

    $('.c-row-2 .header-bottom img').click(function () {
        scrollTo('.c-row-6', 0);
        return false;
    });

    function scrollTo(id, height) {
        height = height ? height : 0;
        $('html,body').animate({
                scrollTop: $(id).offset().top - height
            },
            scrollingSpeed,
            function(){
                disabled = false;
            }
        );
    }

    $("a#activate-order100").fancybox({
        'hideOnContentClick': true
    });

    $("#activate-order500").fancybox({
        'hideOnContentClick': true
    });

    $("#activate-order1000").fancybox({
        'hideOnContentClick': true
    });


});