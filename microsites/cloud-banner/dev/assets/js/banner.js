$(function () {
    // create namespace
    var com = com || {};
    com.psmw = com.psmw || {};

    // class
    com.psmw.Main = {
        //-----------------------//
        //--------VARS-----------//
        //-----------------------//
        motif: null,
        cloud: null,
        cloudText01: null,
        cloudText02: null,
        claim01: null,
        claim02: null,
        claim03: null,
        timerOverOut: null,

        //-----------------------//
        //--------METHODS--------//
        //-----------------------//

        StartAni: function () {
            var that = com.psmw.Main;

            // animate motif
            that.motif.className = "";
            var timeoutID = window.setTimeout(that.OnMotifAniDone, 400);
        },

        //-----------------------//
        //--------HANDLER--------//
        //-----------------------//
        OnMotifAniDone: function () {
            var that = com.psmw.Main;

            // animate cloud
            that.cloud.className = "bgImg";
            var timeoutID = window.setTimeout(that.OnCloudAniDone, 1000);
        },

        OnCloudAniDone: function () {
            var that = com.psmw.Main;

            // animate cloud text02
            that.cloudText02.className = "bgImg";
            var timeoutID = window.setTimeout(that.OnCloudText02AniDone, 1500);
        },

        OnCloudText02AniDone: function () {
            var that = com.psmw.Main;

            // animate claim01
            that.claim01.className = "bgImg";
            // animate motif
            that.motif.className = "bgImg fadeOut";
            // animate cloud
            that.cloud.className = "bgImg left";

            var timeoutID = window.setTimeout(that.OnClaim01AniDone, 150);
        },

        OnClaim01AniDone: function () {
            var that = com.psmw.Main;

            // animate claim02
            that.claim02.className = "bgImg";

            var timeoutID2 = window.setTimeout(that.OnClaim02AniDone, 700);
        },

        OnClaim02AniDone: function () {
            var that = com.psmw.Main;

            // animate claim03
            that.claim03.className = "bgImg";

            document.getElementById("stage").addEventListener("mouseenter", that.OnMouseOver, false);
            document.getElementById("stage").addEventListener("mouseleave", that.OnMouseOut, false);

        },

        OnMouseOver: function (e) {
            var that = com.psmw.Main;

            window.clearTimeout(that.timerOverOut);
            that.timerOverOut = window.setTimeout(that.OnOverAniDone, 350);
            that.cloud.className = "left over";
        },

        OnOverAniDone: function (e) {
            var that = com.psmw.Main;
            that.cloud.className = "left";
        },

        OnMouseOut: function (e) {
            var that = com.psmw.Main;

            window.clearTimeout(that.timerOverOut);
            that.cloud.className = "left";
        },

        OnStart: function () {
            this.motif = document.getElementById("motif");
            this.cloud = document.getElementById("cloud");
            this.cloudText01 = document.getElementById("cloudText01");
            this.cloudText02 = document.getElementById("cloudText02");
            this.claim01 = document.getElementById("claim01");
            this.claim02 = document.getElementById("claim02");
            this.claim03 = document.getElementById("claim03");

            // we have to delay the start a bit, to let the browser apply the initial styles.
            var timeoutID = window.setTimeout(this.StartAni, 100);

            //this.StartAni();
        },

        //-----------------------//
        //--------INIT-----------//
        //-----------------------//
        Init: function () {
            this.OnStart();
        }
    }

    var main = com.psmw.Main;
    main.Init();


    if ($(window).width() < 600) {


        // create namespace
        var com = com || {};
        com.psmw = com.psmw || {};

        // class
        com.psmw.Main = {
            //-----------------------//
            //--------VARS-----------//
            //-----------------------//
            motif: null,
            cloud: null,
            cloudType01: null,
            cloudType02: null,
            claim01: null,
            claim02: null,
            claim03: null,
            seqOver: null,

            //-----------------------//
            //--------METHODS--------//
            //-----------------------//

            StartAni: function () {
                var that = this;

                var seq = new TimelineLite({
                    paused: true
                });

                // motif fade-in
                seq.from(this.motif, 1, {
                    opacity: 0
                });
                seq.from(this.motif, 1, {
                    scale: 1.25
                }, "-=1");

                // cloud fade-in
                seq.from(this.cloud, .5, {
                    opacity: 0
                }, "-=.5");
                seq.from(this.cloud, 1.5, {
                    scale: .2,
                    ease: Back.easeOut,
                    onComplete: this.OnCloudAniComplete,
                    onCompleteParams: [that]
                }, "-=.5");

                // cloud type fade-in
                seq.from(this.cloudType01, 1.5, {
                    opacity: 0,
                    ease: Power2.easeOut
                }, "-=1.25");
                seq.from(this.cloudType02, 1, {
                    opacity: 0,
                    ease: Power2.easeOut
                }, "-=.75");

                // claim fade-in
                seq.from(this.claim01, .65, {
                    top: "+=20",
                    opacity: 0,
                    ease: Power2.easeOut
                }, "-=2");
                seq.from(this.claim02, .65, {
                    top: "+=20",
                    opacity: 0,
                    ease: Power2.easeOut
                }, "-=1.8");
                seq.from(this.claim03, 1, {
                    opacity: 0
                }, "-=1");

                seq.resume();
            },

            //-----------------------//
            //--------HANDLER--------//
            //-----------------------//
            OnStart: function () {
                this.motif = document.getElementById("motif-mob");
                this.cloud = document.getElementById("cloud-mob");
                this.cloudType01 = document.getElementById("cloudText01-mob");
                this.cloudType02 = document.getElementById("cloudText02-mob");
                this.claim01 = document.getElementById("claim01-mob");
                this.claim02 = document.getElementById("claim02-mob");
                this.claim03 = document.getElementById("claim03-mob");

                this.seqOver = new TimelineLite({
                    paused: true
                });
                this.seqOver.to(this.cloud, .4, {
                    scale: 1.25,
                    ease: Power2.easeInOut
                });
                this.seqOver.to(this.cloud, .75, {
                    scale: 1,
                    ease: Back.easeOut
                });

                this.StartAni();
            },

            OnCloudAniComplete: function (scope) {
                var that = scope;
                document.getElementById("stage-mob").addEventListener("mouseenter", that.OnMouseOver, false);
            },

            OnMouseOver: function (e) {
                var that = com.psmw.Main;

                var progress = that.seqOver.progress();

                if (progress == 0) {
                    that.seqOver.resume();
                } else if (progress <= 1) {
                    that.seqOver.restart();
                }
            },

            //-----------------------//
            //--------INIT-----------//
            //-----------------------//
            Init: function () {
                this.OnStart();
            }
        }
        var main = com.psmw.Main;
        main.Init();


    }


});
