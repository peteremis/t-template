
// create namespace
var com = com || {};
com.psmw = com.psmw || {};

// class
com.psmw.Main = {
    //-----------------------//
    //--------VARS-----------//
    //-----------------------//
    motif: null,
    cloud: null,
    cloudType01: null,
    cloudType02: null,
    claim01: null,
    claim02: null,
    claim03: null,
    seqOver: null,

    //-----------------------//
    //--------METHODS--------//
    //-----------------------//

    StartAni: function(){
        var that = this;

        var seq = new TimelineLite({paused: true});

        // motif fade-in
        seq.from(this.motif, 1, {
            opacity: 0
        });
        seq.from(this.motif, 1, {
            scale: 1.25
        }, "-=1");

        // cloud fade-in
        seq.from(this.cloud, .5, {
             opacity: 0
        }, "-=.5");
        seq.from(this.cloud, 1.5, {
            scale: .2,
            ease: Back.easeOut,
            onComplete: this.OnCloudAniComplete,
            onCompleteParams: [that]
        }, "-=.5");

        // cloud type fade-in
        seq.from(this.cloudType01, 1.5, {
            opacity: 0,
            ease: Power2.easeOut
        }, "-=1.25");
        seq.from(this.cloudType02, 1, {
            opacity: 0,
            ease: Power2.easeOut
        }, "-=.75");

        // claim fade-in
        seq.from(this.claim01, .65, {
            top: "+=20",
            opacity: 0,
            ease: Power2.easeOut
        }, "-=2");
        seq.from(this.claim02, .65, {
            top: "+=20",
            opacity: 0,
            ease: Power2.easeOut
        }, "-=1.8");
        seq.from(this.claim03, 1, {
            opacity: 0
        }, "-=1");

        seq.resume();
    },

    //-----------------------//
    //--------HANDLER--------//
    //-----------------------//
    OnStart: function() {
        this.motif = document.getElementById("motif");
        this.cloud = document.getElementById("cloud");
        this.cloudType01 = document.getElementById("cloudText01");
        this.cloudType02 = document.getElementById("cloudText02");
        this.claim01 = document.getElementById("claim01");
        this.claim02 = document.getElementById("claim02");
        this.claim03 = document.getElementById("claim03");

        this.seqOver = new TimelineLite({paused: true});
        this.seqOver.to(this.cloud, .4, {
            scale: 1.25,
            ease: Power2.easeInOut
        });
        this.seqOver.to(this.cloud, .75, {
            scale: 1,
            ease: Back.easeOut
        });

        this.StartAni();
    },

    OnCloudAniComplete: function(scope){
        var that = scope;
        document.getElementById("stage").addEventListener("mouseenter", that.OnMouseOver, false);
    },

    OnMouseOver: function(e){
        var that = com.psmw.Main;

        var progress = that.seqOver.progress();

        if (progress == 0){
            that.seqOver.resume();
        }else if (progress <= 1){
            that.seqOver.restart();
        }
    },

    //-----------------------//
    //--------INIT-----------//
    //-----------------------//
    Init: function(){
        this.OnStart();
    }
}
