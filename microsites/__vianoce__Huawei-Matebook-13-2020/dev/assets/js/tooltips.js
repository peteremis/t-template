/* TOOLTIP */
$("#info-discount").qtip({
    content:
      "Zľavu 30 € získate na nové zariadenie pri prechode a predĺžení viazanosti s Biznis NETom, ako samostatnnou službou alebo v kombinácii s TV alebo Biznis linkou. Zľava sa uplatní po overení objednávky agentom, ktorý Vás bude kontaktovať po jej odoslaní.",
    style: {
      classes: "qtip-tipsy",
    },

    position: {
      my: "bottom center",
      at: "top center",
    },
  });

  $("#info-splatka").qtip({
    content: "Mesačnú splátku budete platiť každý mesiac po dobu 24 mesiacov.",
    style: {
      classes: "qtip-tipsy",
    },

    position: {
      my: "bottom center",
      at: "top center",
    },
  });

  $("#info-zlava").qtip({
    content:
      "Za každú Magio službu kúpenú cez web, získate automaticky zľavu 10 €. Za Magio TV, internet a&nbsp;Pevnú linku tak môžete získať zľavu až 30&nbsp;€. Zľava sa rozdelí na polovicu a&nbsp;bude odrátaná druhý a&nbsp;tretí mesiac z&nbsp;mesačného poplatku.",
    style: {
      classes: "qtip-tipsy",
    },

    position: {
      my: "bottom center",
      at: "top center",
    },
  });
  $("#info-akontacia-b2b").qtip({
    content:
      "Akontácia je úvodná suma, ktorú zaplatíte pri kúpe v predajni alebo cez web.",
    style: {
      classes: "qtip-tipsy",
    },

    position: {
      my: "bottom center",
      at: "top center",
    },
  });