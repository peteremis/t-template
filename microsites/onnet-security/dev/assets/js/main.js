var $header = $('.header');

var $desc1 = $('#desc1');
var $desc2 = $('#desc2');
var $desc3 = $('#desc3');
var $desc4 = $('#desc4');
var $backButton = $('#back');
var $langEn = $('#lang-en');
var $langSk = $('#lang-sk');
var $learnMore = $('#learn-more');
var $foot2 = $('#foot2');

var header_en = 'Your business is safe with OnNet Security';
var desc1_en = 'Dear customer,';
var desc2_en = 'this message is automatically generated by OnNet Security service. Our solution reduces risk of being exposed to malware and phising attacks. The web page you are trying to access is categorized as malicious or blacklisted hence blocked to most secure and reliable internet experience.';
var desc3_en = 'For queries and complaints, please contact our support.';
var desc4_en = 'Slovak Telekom';
var backButton_en = 'Back';
var learnMore_en = '<a href="https://www.telekom.sk/biznis/on-net-security" target="_blank">Learn more</a> about OnNet Security';
var foot2_en = '0800 123 369 option 3';

var header_sk = 'Váš biznis je v bezpečí s OnNet Security';
var desc1_sk = 'Vážený zákazník,';
var desc2_sk = 'táto správa je generovaná službou OnNet Security od spoločnosti Slovak Telekom, ktorá znižuje riziko vystavenia útokom zo strany škodlivého softvéru a internetových hrozieb ako malware a phishing, čo môže spôsobiť poškodenie alebo stratu údajov na vašom zariadení. Webová stránka, ku ktorej sa snažíte získať prístup, je kategorizovaná na základe služby OnNet Security ako potenciálne nebezpečná a preto je prístup na stránku zablokovaný.';
var desc3_sk = 'V prípade otázok a sťažností kontaktujte našu podporu.';
var desc4_sk = 'Slovak Telekom';
var backButton_sk = 'Späť';
var learnMore_sk = '<a href="https://www.telekom.sk/biznis/on-net-security" target="_blank">Viac informácii</a> o OnNet Security';
var foot2_sk = '0800 123 369 voľba 3';

$(function () {

  changeLang('sk');

  function changeLang(lang) {

    if (lang == 'sk') {
      $header.html(header_sk);

      $desc1.html(desc1_sk);
      $desc2.html(desc2_sk);
      $desc3.html(desc3_sk);
      $desc4.html(desc4_sk);

      $backButton.html(backButton_sk);
      $learnMore.html(learnMore_sk);
      $foot2.html(foot2_sk);

      $langSk.addClass('active');
      $langEn.removeClass('active');

    } else {
      $header.html(header_en);

      $desc1.html(desc1_en);
      $desc2.html(desc2_en);
      $desc3.html(desc3_en);
      $desc4.html(desc4_en);

      $learnMore.html(learnMore_en);
      $backButton.html(backButton_en);
      $foot2.html(foot2_en);

      $langEn.addClass('active');
      $langSk.removeClass('active');

    }
  }

  $langSk.on('click', function(e) {
    e.preventDefault();
    changeLang('sk');
  });

  $langEn.on('click', function(e) {
    e.preventDefault();
    changeLang('en');
  });

  $backButton.on('click', function(e) {
    e.preventDefault();
    window.history.go(-1);
  });

});
