$( window ).load(function() {
    var $mainMenu = $('.main-menu'),
    $filterTitle = $('.filter__title'),
    $filterHead = $('#filter__head'),
    $filterOpen = $('.filter_open'),
    $filterClose = $('.filter_close'),
    $optTitle = $('.opt__title'),
    $options = $('.filter-option li');


    $($filterTitle).add($filterOpen).on('click', function(e) {
        e.preventDefault();
        $($filterHead).addClass('opened');
        $($mainMenu).addClass('opaque');
        findSection(getSelectedValues());
    });

    $($filterClose).on('click', function(e) {
        e.preventDefault();
        $($filterHead).removeClass('opened');
        $($optTitle).removeClass('opened');
        $($mainMenu).removeClass('opaque');
    });

    $($optTitle).on('click', function(e) {
        e.preventDefault();
        $(this).toggleClass('opened');
        $($optTitle).not($(this)).removeClass('opened');
    });

    $($options).on('click', function() {
        $(this).closest('ul').siblings('.opt__selected').text($(this)[0].textContent);
        $($optTitle).removeClass('opened');

        findSection(getSelectedValues());
    });

    $(document).click(function(e){
        if( $(e.target).closest("#filter").length > 0 ) {
            return false;
        }

        $($filterHead).removeClass('opened');
        $($optTitle).removeClass('opened');
        $($mainMenu).removeClass('opaque');
    });

    function findSection(selectedValues) {
        switch (selectedValues) {
            case 'ZákladnéNevyužívamNevyužívam':
            showHappyXSMini();
            break;
            case 'ZákladnéVyužívamNevyužívam':
            case 'ZákladnéVyužívamVyužívam':
            case 'ZákladnéNevyužívamVyužívam':
            showHappyXS();
            break;
            case 'ObľúbenéNevyužívamNevyužívam':
            showHappyS();
            break;
            case 'ObľúbenéNevyužívamNevyužívam':
            showHappyL();
            break;
            case 'ObľúbenéVyužívamVyužívam':
            showHappyXL();
            break;
            case 'ObľúbenéNevyužívamVyužívam':
            showHappyXLVolania();
            break;
            case 'Pre náročnýchVyužívamNevyužívam':
            case 'Pre náročnýchNevyužívamNevyužívam':
            showHappyXXL();
            break;
            case 'Pre náročnýchVyužívamVyužívam':
            case 'Pre náročnýchNevyužívamVyužívam':
            showProfi();
            break;
        }
    }

    function getSelectedValues() {
        var selectedValues = $('#opt-1 .opt__selected').text() + $('#opt-2 .opt__selected').text() + $('#opt-3 .opt__selected').text();
        return selectedValues;
    }

    function scrollPageToSecID(secID) {
        $('html, body').animate({
            scrollTop: $('#sec-' + secID).offset().top - ($('#nav-sticky-ext').outerHeight())
        }, 2000);
    }

    function rotateSliderToSlide(sliderID, slideNum, responsiveSlideNum) {
        if ($( window ).width() > 768 || responsiveSlideNum == null) {
            $('#' + sliderID).slick('slickGoTo', slideNum);
        } else {
            $('#' + sliderID).slick('slickGoTo', responsiveSlideNum);
        }
    }

    function blinkBorder(boxID) {
        $('#' + boxID)
        .animate({borderColor:'#e20074'})
        .delay(100)
        .animate({borderColor:'#d0d0d0'})
        .delay(100)
        .animate({borderColor:'#e20074'})
        .delay(100)
        .animate({borderColor:'#d0d0d0'})
        .delay(100)
        .animate({borderColor:'#e20074'})
        .delay(100)
        .animate({borderColor:'#d0d0d0'});
    }

    function moveToBox(sliderID, sliderSlideNum, secID, boxID, responsiveSlideNum) {
        rotateSliderToSlide(sliderID, sliderSlideNum, responsiveSlideNum);
        scrollPageToSecID(secID.toString());
        setTimeout(function () {
            blinkBorder(boxID);
        }, 2500);
    }

    function showHappyXSMini() {
        moveToBox('zakladne_slider', 0, 3, 'happy-xsmini');
    }

    function showHappyXS() {
        moveToBox('zakladne_slider', 1, 3, 'happy-xs');
    }

    function showHappyS() {
        moveToBox('program_slider', 0, 2, 'happy-s');
    }

    function showHappyL() {
        moveToBox('program_slider', 2, 2, 'happy-l');
    }

    function showHappyXL() {
        moveToBox('program_slider', 3, 2, 'happy-xl', 4);
    }

    function showHappyXLVolania() {
        moveToBox('program_slider', 3, 2, 'happy-xl-volania');
    }

    function showHappyXXL() {
        moveToBox('prenarocnych_slider', 0, 4, 'happy-xxl');
    }

    function showProfi() {
        moveToBox('prenarocnych_slider', 1, 4, 'happy-profi');
    }

});
