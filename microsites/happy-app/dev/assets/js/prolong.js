$(window).on('load', function() {

    var $cmbBLock = $('#cmb__block'),
        $input = $('#msisdn'),
        $btn = $('#msisdn-btn'),
        base = 'https://www.telekom.sk/mob/objednavka/-/scenario/e-shop/happy',
        happyURL = '',
        defaultInputVal = '09 _ _ _ _ _ _ _ _',
        $prolongCall = $('.prolong-cta');

    if ($($cmbBLock).is(':visible')) {
        $($prolongCall).show();
    }

    $btn.on('click', function(e){
        e.preventDefault();
        var msisdn = $input.val();
        happyURL = base + '?msisdn=' + msisdn + '&prolong-checker=true';

        if (msisdn != defaultInputVal) {
            window.location.href = happyURL;
        }
    });
});
