'use strict';

angular.
module('happyListApp').
config(['$locationProvider', '$routeProvider',
    function config($locationProvider, $routeProvider) {
        $locationProvider.hashPrefix('!');

        $routeProvider.
        when('/', {
            template: '<happy-list></happy-list>',
            resolve: {
                program: ['$route', 'happyService', '$location', function($route, happyService, $location) {
                    happyService.getAllPrograms().then(function(program) {
                       console.log(program);
                    });
                }]
            }
        });
    }
]);