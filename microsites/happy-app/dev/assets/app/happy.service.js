'use strict';

angular.module('core').service('happyService', function($http, $resource) {
    var service = {
        getAllPrograms: function() {
            return $http.get('../assets/data/program-list.json', { cache: true }).then(function(resp) {
                return resp.data;
            });
        }
    }

    return service;
})
