'use strict';

angular.
module('happyList').
component('happyList', {
    templateUrl: '../assets/app/happy-list.template.html',
    controller: ['happyService', '$filter',
        function HappyListController(happyService, $filter ) {
            var self = this;

            // Get all programs
            self.programsAll = [];
            happyService.getAllPrograms().then(function(response) {
                self.programsAll = response;
                console.log('Happy Programy: ', self.programsAll);
            });

            // set first filter
            // self.defaultFilter = self.zakladnyFilter[0];

            // change device list order basic filter
            self.changeOrder = function(item, mobileCheck) {

                if (mobileCheck === 'mobile') { self.uncheckOther(item); }

                switch (item.value) {
                    case 'nameAZ':
                        self.orderProp = 'name';
                        self.reverse = false;
                        break;
                    case 'nameZA':
                        self.orderProp = 'name';
                        self.reverse = true;
                        break;
                    case 'lowest':
                        self.orderProp = 'price.easy';
                        self.reverse = false;
                        break;
                    case 'highest':
                        self.orderProp = 'price.easy';
                        self.reverse = true;
                        break;
                    case 'order':
                        self.orderProp = 'age';
                        self.reverse = false;
                        break;
                }
            };

            // init
            self.init = function() {
                // self.changeOrder(self.defaultFilter);
            };
            // runs once per controller instantiation
            self.init();

        }
    ]
});
