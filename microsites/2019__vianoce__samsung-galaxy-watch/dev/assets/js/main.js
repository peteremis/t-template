$(document).ready(function() {
  //back to to previous url
  $("#a_referrer a").attr("href", document.referrer);

  /* TABS */
  $(".tabs-menu a").click(function(event) {
    event.preventDefault();
    $(this)
      .parent()
      .addClass("current");
    $(this)
      .parent()
      .siblings()
      .removeClass("current");
    var tab = $(this).attr("href"), // select clicked tab. In example "#tab-1" ..
      parent = $(this)
        .closest("ul")
        .parent()
        .parent(); // <tabs-container>
    parent
      .find(".tab-content")
      .not(tab)
      .css("display", "none"); //hide all not clicked tabs
    $(tab).fadeIn(); //show clicked tab
  });

  /* TOOLTIP */
  $("#info-akontacia").qtip({
    content:
      "Akontácia je úvodná suma, ktorú zaplatíte pri kúpe v predajni alebo cez web.",
    style: {
      classes: "qtip-tipsy"
    },

    position: {
      my: "bottom center",
      at: "top center"
    }
  });
  $("#info-akontacia-b2b").qtip({
    content:
      "Akontácia je úvodná suma, ktorú zaplatíte pri kúpe v predajni alebo cez web.",
    style: {
      classes: "qtip-tipsy"
    },

    position: {
      my: "bottom center",
      at: "top center"
    }
  });
  /* TOGGLE ARROW */
  $(".toggle-arrow, .arrow-right").click(function(event) {
    event.preventDefault();
    if ($(this).hasClass("arrow-right")) {
      return;
    }
    $(event.target)
      .closest(".togleArrow")
      .find(".arrow-content")
      .toggle(200);
    $(this)
      .find(".arrow-right")
      .toggleClass("arrow-rotate");
  });

  $(".toggle-arrow-main, .arrow-right-main").click(function(event) {
    event.preventDefault();
    if ($(this).hasClass("arrow-right-main")) {
      return;
    }
    $(event.target)
      .closest(".togleArrow-main")
      .find(".arrow-content-main")
      .toggle(200);
    $(this)
      .find(".arrow-right-main")
      .toggleClass("arrow-rotate");
  });
  /* TOGGLE ARROW END */

  /* SCROLL */
  $(".scroll-to").click(function() {
    if (
      location.pathname.replace(/^\//, "") ==
        this.pathname.replace(/^\//, "") &&
      location.hostname == this.hostname
    ) {
      var target = $(this.hash);
      target = target.length ? target : $("[name=" + this.hash.slice(1) + "]");
      if (target.length) {
        $("html,body").animate(
          {
            scrollTop:
              target.offset().top - ($("#nav-sticky").outerHeight() - 10)
          },
          1000
        );
        return false;
      }
    }
  });

  /* GET PARAMETERS FROM URL */
  function getUrlParameter(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)");
    var results = regex.exec(location.search);
    return results === null
      ? ""
      : decodeURIComponent(results[1].replace(/\+/g, " "));
  }

  /* FETCH DATA FROM JSON */

  var dataURL = "";
  if (window.location.hostname === "localhost") {
    dataURL = "../assets/js/data.json";
  } else {
    dataURL = "https://www.telekom.sk/documents/10179/16511938/data.json"; // SET JSON URL
  }

  function setPrice2(myrpln) {
    $.ajax({
      url: dataURL,
      cache: false,
      success: function(data) {
        // -------------------------------------------------------------------------------
        data.forEach(function(itemData) {
          if (itemData.id === "samsungWatch") {
            pricelist = itemData;
          }
        }); // !!! SET PROUCT ID !!!
        // -------------------------------------------------------------------------------

        $("#" + myrpln)
          .click()
          .focus(); // SET DEFAULT RATEPLAN (CLICK ON THE BUTTON)
      }
    });
  }

  /* SET NEW PRICES ON PAGE */
  function setNewPrices(newRpln) {
    // EEC //
    $("#eec").attr("src", pricelist.prices[newRpln].eyecatcherURL);
    if (newRpln == "anoS") {
      var my_rp = "S";
    }
    if (newRpln == "anoM") {
      var my_rp = "M";
    }
    if (newRpln == "anoMdata") {
      var my_rp = "M_DATA";
    }
    if (newRpln == "anoL") {
      var my_rp = "L";
    }
    if (newRpln == "anoXL") {
      var my_rp = "XL";
    }
    if (newRpln == "anoXXL") {
      var my_rp = "XXL";
    }
    $("#buyBtn").attr("href", pricelist.buyBtnURL + "?vyberrp=" + my_rp); //SET URL TO BUY BUTTON
    if (pricelist.prices[newRpln].oneTimeStore == null) {
      $("#oneTimeWebPrice").text(
        pricelist.prices[newRpln].oneTimeWeb.toFixed(2) + " €"
      );
      $("#oneTimeRegularPrice").hide();
    } else {
      $("#oneTimeWebPrice").text(
        pricelist.prices[newRpln].oneTimeWeb.toFixed(2) + " € s web zľavou"
      );
      $("#oneTimeRegularPrice").text(
        pricelist.prices[newRpln].oneTimeStore.toFixed(2) + " €"
      );
      $("#oneTimeRegularPrice").show();
    }

    if (pricelist.prices[newRpln].monthlyStore == null) {
      $("#MonthylFeeMobile").text(
        pricelist.prices[newRpln].monthlyWeb.toFixed(2) + " €"
      );
      $("#MonthlyRegularPrice").hide();
    } else {
      $("#MonthylFeeMobile").text(
        pricelist.prices[newRpln].monthlyWeb.toFixed(2) + " € s web zľavou"
      );
      $("#MonthlyRegularPrice").text(
        pricelist.prices[newRpln].monthlyStore.toFixed(2) + " €"
      );
      $("#MonthlyRegularPrice").show();
    }
    $("#MonthlyFeeService").text(
      pricelist.prices[newRpln].rplnPrice.toFixed(2) + " € mesačne"
    );

    // BIZNIS CENY //

    $("#MonthylFeeMobileBiznisDPH").text(
      pricelist.prices[newRpln].monthlyWeb.toFixed(2) + " € s DPH "
    );
    $("#oneTimeWebPriceBiznisDPH").text(
      pricelist.prices[newRpln].oneTimeWeb.toFixed(2) + " € s DPH "
    );
    $("#MonthlyFeeServiceBiznisDPH").text(
      pricelist.prices[newRpln].rplnPrice.toFixed(2) + " € s DPH "
    );
    // BIZNIS CENY BEZ DPH//
    if (pricelist.prices[newRpln].monthlyWeb_nodph != undefined) {
      $("#MonthylFeeMobileBiznis").text(
        pricelist.prices[newRpln].monthlyWeb_nodph.toFixed(2) + " € bez DPH "
      );
    }
    if (pricelist.prices[newRpln].oneTimeWeb_nodph != undefined) {
      $("#oneTimeWebPriceBiznis").text(
        pricelist.prices[newRpln].oneTimeWeb_nodph.toFixed(2) + " € bez DPH "
      );
    }
    if (pricelist.prices[newRpln].rplnPrice_nodph != undefined) {
      $("#MonthlyFeeServiceBiznis").text(
        pricelist.prices[newRpln].rplnPrice_nodph.toFixed(2) + " € bez DPH "
      );
    }
    $("#buyBtnBiznis").attr(
      "href",
      pricelist.buyBtnURL_biznis + "?_trf=" + newRpln
    );
  }

  $(".rpln").click(function() {
    var rateplan = $(this).prop("id"); // GET NEW VALUE FROM BUTTON
    $(".rpln").removeClass("active-plan");
    $(this).addClass("active-plan");
    setNewPrices(rateplan);
  });

  var elem = document.querySelector(".js-switch");
  var init = new Switchery(elem, {
    color: "#6bb324",
    secondaryColor: "#ededed"
  });
  $(".switchery")
    .parent()
    .closest("span")
    .addClass("hide-span");

  var changeCheckbox = document.querySelector(".js-check-change"),
    changeField = document.querySelectorAll(".js-check-change-field"),
    changeFieldNone = document.querySelectorAll(".js-check-change-field-none");

  changeCheckbox.onchange = function() {
    if (changeCheckbox.checked == true) {
      for (i = 0; i < changeField.length; i++) {
        changeField[i].style.display = "inline-block";
      }
      for (i = 0; i < changeFieldNone.length; i++) {
        changeFieldNone[i].style.display = "none";
      }
    } else {
      for (i = 0; i < changeField.length; i++) {
        changeField[i].style.display = "none";
      }
      for (i = 0; i < changeFieldNone.length; i++) {
        changeFieldNone[i].style.display = "inline-block";
      }
    }
  };

  var pricelist = []; //TO PREPARE VARIABLE FOR JSON DATA
  var selectedrp = getUrlParameter("selectedrp"); // GET DATA FROM URL

  var segment = getUrlParameter("category");
  if (segment == "biznis") {
    if (selectedrp == "") {
      selectedrp = "RP1055";
    } // DEFAULT RATEPLAN B2B
    $("#biznis-delivery").hide();
    $("#b2b-price").show();
    $("#b2c-price").hide();
    $("#other-actions").hide();
  }
  if (segment != "biznis") {
    if (selectedrp == "") {
      selectedrp = "anoL";
    } // DEFAULT RATEPLAN B2C
    $("#biznis-delivery").show();
    $("#b2b-price").hide();
    $("#b2c-price").show();
    $("#other-actions").show();
  }
  setPrice2(selectedrp);
});
