$(function() {
    $('.t1').qtip({
        content: {
            text: 'Neobmedzené hovory vám poskytujeme vďaka <br> Zásadám férového používania: <br> 1000 minút - do mobilnej siete Telekom'
        },
        position: {
            my: 'top center',
            at: 'bottom center',
        }
    });
    $('.t2').qtip({
        content: {
            text: 'Neobmedzené hovory vám poskytujeme vďaka<br> Zásadám férového používania:<br> 1000 minút - do mobilných sietí v SR'
        },
        position: {
            my: 'top center',
            at: 'bottom center',
        }
    });
    $('.t3').qtip({
        content: {
            text: 'Neobmedzené hovory vám poskytujeme vďaka <br> Zásadám férového používania:<br> 1000 minút - do mobilných sietí v SR<br> 1000 minút - do EÚ'
        },
        position: {
            my: 'top center',
            at: 'bottom center',
        }
    });


    $(".scroll-to").click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top - ($("#nav-sticky").outerHeight() - 10)
                }, 1000);
                return false;
            }
        }
    });
});
