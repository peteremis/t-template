var loopCounter = 0;
$('script').load(function () {

    loopCounter++;
    if (loopCounter <= 1) {
        var $form__input = $('.finishFormButton'),
            $div__form = $('#formFieldId15393 .formFieldRenderField'),
            $form__title = $('.formComposition h2'),
            $form__text = $('#formFieldId15313 h4'),
            download__link = $("<a />", {
                href: "/documents/10179/609249/PDF_ponuka+kanalov+Magio+SAT+12_15.pdf",
                text: "Stiahnuť",
                id: "form__link"
            });

        $form__title.addClass('form__title');
        $form__text.addClass('form__text');
        $('#formFieldId15313').addClass('form__body');
        $form__title.insertBefore($('#formFieldId15313'));
        $form__input.appendTo($div__form);
        download__link.appendTo($('#formFieldId15393'));
        $('.formComposition').appendTo('#replace_form');
    }
});
