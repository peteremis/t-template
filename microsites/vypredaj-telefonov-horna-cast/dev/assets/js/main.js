$(function() {
    $('.info').qtip({
        content: 'Bezplatné volania získate na 3 čísla, ktoré si sami zvolíte. Čísla môžu byť v akejkoľvek mobilnej sieti v SR.'
    });
    $(".fancybox").fancybox({
        maxWidth: 610
    });

    $(".scroll-to").click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top - ($("#nav-sticky").outerHeight())
                }, 1000);
                return false;
            }
        }
    });

});
