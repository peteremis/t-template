$(document).ready(function () {

  /* ::: TAB CONFIG ::: */
  $('ul.tabs li a').on('click', function (e) {
    var currentAttrValue = $(this).attr('href');
    $('.tab ' + currentAttrValue).show().siblings().hide();
    $(this).parent('li').addClass('current').siblings().removeClass('current');
    e.preventDefault();
  });

  /* URL PARAMETERS */
  if (window.location.hash) {
    var target = window.location.hash.split("?")[0];
    if (target == "#vyhraj-iphone") {
      $("html, body").animate(
        {
          scrollTop: $("#sec-7").offset().top,
        },
        1000
      );
      $(target).click();
    }
  }

  $(".scroll-to").click(function() {
    if (
      location.pathname.replace(/^\//, "") == this.pathname.replace(/^\//, "") &&
      location.hostname == this.hostname
    ) {
      var target = $(this.hash);
      target = target.length ? target : $("[name=" + this.hash.slice(1) + "]");
      if (target.length) {
        $("html,body").animate(
          {
            scrollTop: target.offset().top - $("#nav-sticky").outerHeight()
          },
          1000
        );
        return false;
      }
    }
  });

  /* END URL PARAMETERS */
  $("#nav-sticky-menu").sticky({
    topSpacing: 0,
    widthFromWrapper: true,
  });

  $(document).scroll(function () {
    return;

    var y = $(this).scrollTop();

    if ($(window).width() < 769) {
      if (y > 400) {
        $("#nav-sticky-menu").fadeIn();
      } else {
        $("#nav-sticky-menu").fadeOut();
      }
    }

    if ($(window).width() < 521) {
      if (y > 620) {
        $("#nav-sticky-menu").show();
      } else {
        $("#nav-sticky-menu").hide();
      }
    }

  });
});
