$(function () {

    // GLOBALS AND VARS
    var $device = $('.device'),
        $device__small__first = $('.device__small__first'),
        $device__small__last = $('.device__small__last'),
        $device__screen = $('.device__screen'),
        $device__screen__small__first = $('.device__screen__small__first'),
        $device__screen__small__last = $('.device__screen__small__last'),
        $text__area = $('.text-area'),
        $controls__left = $('.controls__left'),
        $controls__right = $('.controls__right'),
        $stack = $('.stack'),
        $stack__item = $('.stack > .stack__item'),
        $copy__img = $(),
        $copy__img__first = $(),
        $copy__img__last = $(),
        $copy__text = $(),
        $current__images = $(),
        $current__image__first = $(),
        $current__image__last = $(),
        $current__texts = $(),
        direction = 1, // 0 - left, 1 - right
        item__count = $stack__item.length - 1,
        first__item = 0,
        current__item = 0,
        animation = false,
        animation_infinite = false,
        first__load = true,
        loop = 0;

    console.log('Item count: ' + item__count);

    // VERIFY THE ITEM COUNTS FOR FIRST AND LAST PHONE

    function verify_small_devices() {
        console.log('Current item: ' + current__item);
        if (current__item > first__item) {
            $($device__small__first).css('display', 'inline-block');
        } else {
            $($device__small__first).css('display', 'none');
        }

        if (current__item < item__count) {
            $($device__small__last).css('display', 'inline-block');
        } else {
            $($device__small__last).css('display', 'none');
        }
    }

    // RELOAD THE SMALL DEVICE SCREENS
    function reload_small_devices() {

        $current__image__first = $('.device__screen__small__first');
        $current__image__last = $('.device__screen__small__last');

        if (current__item > first__item) {
            $copy__img__first = $stack__item[current__item - 1].children[0];
            if (loop > 0) {

                $($current__image__first[0].children[0]).remove();
            }

        }

        if (current__item < item__count) {
            $copy__img__last = $stack__item[current__item + 1].children[0];
            if (loop > 0) {
                $($current__image__last[0].children[0]).remove();
            }
        }
        
        if (loop == 0) {
            loop++;
        }
    }

    // LOAD THE CONTENT
    function load_content() {
        verify_small_devices();
        reload_small_devices();

        $copy__img = $stack__item[current__item].children[0];
        $copy__text = $stack__item[current__item].children[1];

        $($copy__img).clone().appendTo($device__screen).css({
            '-webkit-animation': 'fadein 1s',
            '-moz-animation': 'fadein 1s',
            '-ms-animation': 'fadein 1s',
            '-o-animation': 'fadein 1s',
            'animation': 'fadein 1s'
        });
        $($copy__text).clone().appendTo($text__area);
        $($copy__img__first).clone().appendTo($device__screen__small__first).css({
            '-webkit-animation': 'fadein 1s',
            '-moz-animation': 'fadein 1s',
            '-ms-animation': 'fadein 1s',
            '-o-animation': 'fadein 1s',
            'animation': 'fadein 1s'
        });
        $($copy__img__last).clone().appendTo($device__screen__small__last).css({
            '-webkit-animation': 'fadein 1s',
            '-moz-animation': 'fadein 1s',
            '-ms-animation': 'fadein 1s',
            '-o-animation': 'fadein 1s',
            'animation': 'fadein 1s'
        });
    }

    // RELOAD CONTENT BASED ON CLICK DIRECTION
    function reload_content(dir) {

        if (dir == 1) {
            if (current__item < item__count) {
                current__item++;
                load_content();
            } else {
                current__item = 0;
                load_content();
            }
        } else if (dir == 0) {
            if (current__item == 0) {
                current__item = item__count;
                load_content();
                $($controls__left).off('click');

            } else {
                current__item--;
                load_content();
            }
        }
    }

    // REMOVE THE ACTUAL CONTENT BEFORE NEW ONE BEING LOADED
    function replace_images(dir) {
        $current__images = $('.device__screen');
        $current__texts = $('.text-area');

        if (dir == 0) {
            $($current__images[0].children[0]).css({
                '-webkit-animation': 'scroll_left 1s',
                '-moz-animation': 'scroll_left 1s',
                '-ms-animation': 'scroll_left 1s',
                '-o-animation': 'scroll_left 1s',
                'animation': 'scroll_left 1s'
            });
        }

        if (dir == 1) {
            $($current__images[0].children[0]).css({
                '-webkit-animation': 'scroll_right 1s',
                '-moz-animation': 'scroll_right 1s',
                '-ms-animation': 'scroll_right 1s',
                '-o-animation': 'scroll_right 1s',
                'animation': 'scroll_right 1s'
            });
        }

        setTimeout(function () {
            $($current__images[0].children[0]).remove();
        }, 1000);

        $($current__texts[0].children[0]).remove();
    }

    // CLICK FUNCTIONS
    function click_functions(dir) {
        if (animation_infinite) {
            if (animation) {
                return false;
            }
        } else {
            if (animation || (dir == 0 && current__item == 0) || (dir == 1 && current__item == item__count)) {
                return false;
            }
        }

        animation = true;

        replace_images(dir);
        reload_small_devices();
        reload_content(dir);

        setTimeout(function () {
            animation = false;
        }, 1000);
    }

    // SETUP THE CLICK FUNCTIONALITY
    $($controls__right).on('click', function (e) {
        e.preventDefault();
        direction = 1;
        click_functions(direction);
    });

    $($controls__right).dblclick(function (e) {
        e.preventDefault();
    });

    $($controls__left).on('click', function (e) {
        e.preventDefault();
        direction = 0;
        click_functions(direction);
    });

    $($controls__left).dblclick(function (e) {
        e.preventDefault();
    });

    // EXECUTE THE CODE
    load_content();

});
