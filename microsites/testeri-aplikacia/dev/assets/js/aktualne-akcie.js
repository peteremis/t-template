/* TABS */
$(document).on('click', '.tabs-menu a', function(event) {
    event.preventDefault();
    $(this).parent().addClass("current");
    $(this).parent().siblings().removeClass("current");
    var tab = $(this).attr("href"), // select clicked tab. In example "#tab-1" ..
        parent = $(this).closest('ul').parent().parent(); // <tabs-container>
    parent.find('.tab-content').not(tab).css("display", "none"); //hide all not clicked tabs
    $(tab).fadeIn(); //show clicked tab


});
/* TABS END */


/* FROM TABS TO ACCORDION */
setTimeout(function(){
   var $accordion = $('.accordion'),
   $heads = $('.tabs-menu a'),
   $contents = $('.tab-content'),
   itemCount = $($heads).length - 1,
   accordionCode = '<div class="item">';

   for (var i = 0; i <= itemCount; i++) {
       accordionCode += '<div class="heading">' + $($heads[i]).text()  + '</div>';
       accordionCode += '<div class="content">' + $($contents[i]).html() + '</div></div>';

       if (i !== itemCount) {
           accordionCode += '<div class="item">';
       }
   }

   $(accordionCode).appendTo($accordion);

   var $items = $('.accordion .item');
   //SET OPENED ITEM
   $($items[0]).addClass('open');
     }, 500);
/* FROM TABS TO ACCORDION END */


/* ACCORDION */
$(document).on('click', '.accordion .item .heading', function(e) {

    var clickedHead = e.target;
    var $item = $(clickedHead).closest('.item');
    var isOpen = $item.hasClass('open');
    var $content = $item.find('.content');
    var $acPrice = $(clickedHead).find('.ac-price');

    if (isOpen) {
        $content.slideUp(200);
        $item.removeClass('open');
        $acPrice.show();
    } else {
        $content.slideDown(200);
        $item.addClass('open');
        $acPrice.hide();
    }
});


$('.accordion .item.open').find('.content').slideDown(200);
/* ACCORDION END */

/* DIV DATA-HREF */
$(document).on('click','#content .tab-acc-sub',function(){
    window.location.href = '/' + $(this).attr('data-href');
});
