var gulp            = require('gulp'),
    sass            = require('gulp-sass'),
    sourcemaps      = require('gulp-sourcemaps'),
    uglify          = require('gulp-uglify'),
    browserSync     = require('browser-sync').create(),
    reload          = browserSync.reload,
    includer        = require('gulp-html-ssi'),
    runSequence     = require('run-sequence'),
    es              = require('event-stream'),
    replace         = require('gulp-replace'),
    config          = require('./config.json'),
    globalTemplates = '../../global-assets/templates/*.html',
    localTemplates  = 'dev/templates/*.html';

//Base tasks html + css + js

gulp.task('htmlSSI', function() {
       es.merge(gulp.src(globalTemplates), gulp.src(localTemplates))
        .pipe(includer())
        .pipe(gulp.dest('dev/server/'));
});

gulp.task('sass', function () {
    return gulp.src('dev/assets/sass/*.scss')
                .pipe(sass({outputStyle: 'compact'}))
                .pipe(sourcemaps.init())
                .pipe(sass.sync().on('error', sass.logError))
                .pipe(sourcemaps.write('/maps'))
                .pipe(gulp.dest('dev/server/assets/css'))
                .pipe(browserSync.stream());
});

// aktualne akcie
gulp.task('sass-aktualneAkcie', function () {
return gulp.src('dev/assets/sass/aktualne-akcie.scss')
      .pipe(sass({outputStyle: 'compact'}))
      .pipe(sourcemaps.init())
      .pipe(sass.sync().on('error', sass.logError))
      .pipe(sourcemaps.write('/maps'))
      .pipe(gulp.dest('dev/server/assets/css'))
      .pipe(browserSync.stream());
});

gulp.task('js', function () {
    return gulp.src('dev/assets/js/*.js')
        .on('error', console.error.bind(console))
        .pipe(gulp.dest('dev/server/assets/js'));
});

//Browser-sync & watcher
//Static server

gulp.task('browser-sync', ['sass', 'sass-aktualneAkcie', 'js', 'htmlSSI'], function() {
   browserSync.init({
        server: {
            baseDir: ["./dev", "./dev/server", "./../../global-assets"]
        }
    });
});

//Novy zaklad: gulp watch

gulp.task('watch', ['browser-sync'], function () {
    gulp.watch('dev/assets/sass/*.scss', ['sass']);
    gulp.watch('../../global-assets/sass/*.scss', ['sass']);
    gulp.watch('dev/assets/js/*.js', ['js']);
    gulp.watch('dev/assets/js/*.js').on('change', reload);
    gulp.watch('dev/templates/*.html', ['htmlSSI']);
    gulp.watch('dev/server/*.html').on('change', reload);
    gulp.src('dev/assets/img/**')
    .pipe(gulp.dest('dev/server/assets/img'));
});
//deploy to dist convert and back
//convert deploy 2 dev html
gulp.task('conv', function(){
    gulp.src(['convert/_input.html'])
    .pipe(replace(config.imgFolder , '../assets/img/'))
    .pipe(replace(config.codeFolder ,'../assets/js/'))
    .pipe(gulp.dest('convert/dev-src/'));
});
//convert dev 2 deploy html
gulp.task('conv-back', function(){
    gulp.src(['convert/dev-src/_input.html'])
    .pipe(replace('../assets/img/', config.imgFolder))
    .pipe(replace('../assets/js/' ,config.codeFolder))
    .pipe(gulp.dest('convert/deploy-src/'));
});

//Deploy helpers

gulp.task('dist-replace-path-html', function(){
    gulp.src(['dev/templates/_main-content.html'])
    .pipe(replace('../assets/img/', config.imgFolder))
    .pipe(replace('../assets/js/', config.codeFolder))
    .pipe(gulp.dest('deploy'));
    gulp.src(['dev/templates/_aktualne-akcie.html'])
    .pipe(replace('../assets/img/', config.imgFolder))
    .pipe(replace('../assets/js/', config.aktualneAkcieJS))
    .pipe(gulp.dest('deploy'));
});

gulp.task('dist-replace-path-css', function(){
  gulp.src(['dev/server/assets/css/*.css'])
    .pipe(replace('../img/', config.imgFolder))
    .pipe(gulp.dest('deploy'));
});

gulp.task('dist-replace-path-js', function(){
  gulp.src(['dev/server/assets/js/*.js'])
    .pipe(replace('../img/', config.imgFolder))
    .pipe(replace('templates/', config.aktualneAkcieHTML))
    .pipe(gulp.dest('deploy'));
});

gulp.task('prepare-deploy', ['sass', 'sass-aktualneAkcie', 'js', 'htmlSSI'], function() {
  return
  gulp.src('dev/templates/_main-content.html'),
  gulp.src('dev/templates/_aktualne-akcie.html')
  .pipe(gulp.dest('deploy'));
  gulp.src('dev/assets/img/**')
  .pipe(gulp.dest('deploy/img'));
  gulp.src('dev/server/assets/js/main.js')
  .pipe(gulp.dest('deploy'));
  gulp.src('dev/server/assets/css/style.css')
  .pipe(gulp.dest('deploy'));
  gulp.src('dev/server/assets/css/aktualne-akcie.css')
  .pipe(gulp.dest('deploy'));
});

gulp.task('dist-replace', ['dist-replace-path-html', 'dist-replace-path-js', 'dist-replace-path-css']);

//Deploy

gulp.task('deploy', function(callback) {
    runSequence('prepare-deploy',
                'dist-replace',
                 callback);
});

//Serve

gulp.task('serve', function () {

    browserSync.init({
        server: {
            baseDir: ["./dev", "./dev/server", "./../../global-assets"]
        }
    });

    gulp.watch('dev/assets/sass/*.scss', ['sass']);
    // gulp.watch('dev/assets/sass/**').on('change', reload);
    gulp.watch('../../global-assets/sass/*.scss', ['sass']);
    //gulp.watch('../../global-assets/sass/*.scss').on('change', reload);
    gulp.watch('dev/assets/js/*.js', ['js']);
    gulp.watch('dev/assets/js/*.js').on('change', reload);
    gulp.watch('dev/templates/*.html', ['htmlSSI']);
    // gulp.watch('dev/templates/*.html').on('change', reload);
    //gulp.watch('dev/server/assets/css/*.scss').on('change', reload);
    gulp.watch('dev/server/*.html').on('change', reload);
    gulp.src('dev/assets/img/**')
    .pipe(gulp.dest('dev/server/assets/img'));
});

// DEFAUL Task

gulp.task('default', function(callback) {
    runSequence(['sass', 'js', 'htmlSSI'],
                'serve',
                 callback);
});
