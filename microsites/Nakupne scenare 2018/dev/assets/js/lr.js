$(document).ready(function () {

    //hide and prechecked VAS EASY BANNER
    var easyCheckbox = $('.setProductParameter-checkbox #uniform-productsByMainCategory\\[MOBILE_TARIFF\\]\\[1\\]\\.products\\[0\\]').eq(0);
    if (!easyCheckbox.eq(0).children().hasClass('checked')) {
           easyCheckbox.find('input').trigger('click');
    }
   
     /* hide pausaly if customer no prolong */
     if (document.location.search != "?prolong-checker=true") {
         //hide carousel headline
         $('.packageSizeSelectorHappyHead').hide();
         //hide carousel
         if (window.location.pathname.contains("/scenario/e-shop/pausal")) {
             $("#packageSizeSelector").hide();
         }
         //show carousel headline
         if (window.location.pathname.contains("/scenario/e-shop/hw-pausal")) {
             $('.packageSizeSelectorHappyHead').show();
         }
     }
   
     /*  REMOVE AB Test Cart from 1.st step on hw-pausal */
     if (window.location.href.indexOf("/hw-pausal/vyber-hw") > -1) {
         $("div.cart").remove();
         $("div.shoppingScenario-bottom-container").remove();
     }
     if (window.location.href.indexOf("www.telekom.sk") < 0) {
         var cartWrapper = $('.cartWrapper');
         if (cartWrapper.hasClass('hidden')) {
             cartWrapper.removeClass('hidden');
             $('.shoppingCartIcon i, .shoppingCartBottom-mainProductLabel .labels, .shoppingCartBottom-mainProduct-price-container div').hide();
         }
     }
 
    /*********** RADIO BUTTONS *************/
   
    var styles = [
       ' <style type="text/css">',
       ' /* ACTIVATION SECTION */',
       ' /* hide 1. number */',
       ' #content .packageSizeSelectStep.number-1 {',
       ' display: none;',
       ' }',
   
       ' /* hide old happyHeader text */',
       ' #content .happyHeader {',
       ' display: none;',
       ' }',
   
       ' /* new happyHeader text */',
       ' #content .packageSizeSelectHeaderText.happyheaderText {',
       ' font-weight: 200;',
       ' margin-left: 0px;',
       ' }',
   
       ' /* radioBtns */',
       ' #section-shoppingScenario.stepHwCatalog .rbCheckbox div.radio label, #section-shoppingScenario.stepHwCatalog .rbCheckbox div.radio span input {',
       ' margin-left: 0px;',
       ' cursor: pointer;',
       ' }',
       ' #content .activation-type-container {',
       ' max-width: auto;',
       ' margin-top: 10px;',
       ' }',
       ' #content .activation-type-container-inner label .radio {',
       ' display: inline-block !important;',
       ' float: left;',
       ' }',
       ' #content .activation-type-container-inner.button-num-4 .label {',
       ' display: block;',
       ' width: 100%;',
       ' height: auto;',
       ' border: 0px;',
       ' background-color: transparent;',
       ' }',
       ' #content .activation-type-container-inner.button-num-4 label {',
       ' display: block;',
       ' width: 100%;',
       ' height: auto;',
       ' border: 0px;',
       ' background-color: transparent;',
       ' }',
       ' #content .activation-type-container label:active {',
       ' transform: scale(1);',
       ' }',
       ' #content .activation-type-container label .label {',
       ' text-transform: none;',
       ' font-size: 24px !important;',
       ' padding: 5px 0 !important;',
       ' }',
       ' @media (max-width: 800px) {',
       ' #content .activation-type-container label .label {',
       '/* font-size: 16px !important;*/',
       ' }',
       ' }',
       ' @media (max-width: 600px) {',
       ' label div.radio {',
       ' margin-right: 0;',
       ' }',
       ' #content .activation-type-container label .label {',
       ' padding: 7px 5px !important;',
       ' }',
       ' }',
       ' #content .activation-type-container-inner.button-num-4 label {',
       ' display: block;',
       ' width: 100%;',
       ' height: auto;',
       ' border: 0px;',
       ' background-color: transparent;',
       ' }',
   
       ' #sms-verification {',
       ' margin-left: 5px;',
       ' margin-bottom: 15px;',
       ' max-width: auto;',
       ' }',
       ' #sms-verification .sms-verification-body #sms-verification-input-number-button, #sms-verification .sms-verification-body #sms-verification-verification-code-button {',
       ' margin-left: 0px;',
       ' margin-top: 10px;',
       ' }',
   
       ' /* set black text color to active btn */',
       ' #content .activation-type-container:not(.magenta-activations) label.selected .label-inner-container {',
       ' color: #e20074;',
       ' }',
   
       ' /* change padding in activation container */',
       ' .step.input-number .container {',
       ' padding: 20px 0;',
       ' }',
   
       ' #content .label-inner-container {',
       ' display: inline-block;',
       ' height: 45px;',
       ' width: 70%;',
       ' text-align: left;',
       ' }',
       ' #content .activation-type-container-inner.button-num-4 {',
       ' display: block;',
       ' padding: 0px;',
       ' margin: 0px;',
       ' }',
   
       ' #content .activation-type-container-inner.button-num-4 .label span {',
       ' //display: block; ',
       ' }',
       ' </style>'].join('\n');
   
    //prepend styles to <head>
    $(document.head).prepend(styles);
   
    /****************************************** Activation section changes ******************************************/
    var headerText = $('.packageSizeSelectStep.number-1').next(),
       radioLabel = $('.activation-type-container-inner.button-num-4').find('label'),
       radioLabelTextSpan = $('.activation-type-container-inner.button-num-4').find('.label').find('span');
   
    //on hover, remove magenta background under buttons
    radioLabel.hover(function(){
       $(this).css({'background-color':'transparent','color':'#6c6c6c'})
    });
   
    //remove green dot from radiobtns and show it only on clicked radiobtn
    $('.label-inner-container').on('click',function(){
       $('.label-inner-container').next().find('span').removeClass('checked') //remove green dot from siblings
       $(this).next().find('span').addClass('checked'); //set green dot to clicked elemeent in activation section
    })
   
    //detach activation infoDiv and insert it after clicked btn
    var btnOne = $('.activation-type-container-inner.button-num-4 label .label-inner-container').eq(0);
    var btnThree = $('.activation-type-container-inner.button-num-4 label .label-inner-container').eq(2);
    $('#sms-verification-robots').remove(); //delete input for robots
   
    $('#sms-verification').hide();
    function insertInfoDiv (btnnumber) {
       var detachedSMSverif = $('#sms-verification').detach();
       $('.activation-type-container-inner.button-num-4 label').eq(btnnumber).after($(detachedSMSverif));
    }

    //insert divInfo after btn 1
    $(btnOne).on('click', function(){
       insertInfoDiv (0);
    })
    //insert divInfo after btn 3
    $(btnThree).on('click', function(){
       insertInfoDiv (2);
    })
   
    //if user click directly on green radio dots
    var radioOne = $('.activation-type-container-inner.button-num-4 label').eq(0).find('.radio');
    var radioTwo = $('.activation-type-container-inner.button-num-4 label').eq(1).find('.radio');
    var radioThree = $('.activation-type-container-inner.button-num-4 label').eq(2).find('.radio')
    var radioFour = $('.activation-type-container-inner.button-num-4 label').eq(3).find('.radio');
   
    function clickOnRadio(radioNumber, number){
       $(radioNumber).on('click', radioNumber, function(){
          $('.activation-type-container-inner.button-num-4 label').eq(number).find('.label-inner-container').click();
       })
    }
   
    clickOnRadio(radioOne, 0);
    clickOnRadio(radioTwo, 1);
    clickOnRadio(radioThree, 2);
    clickOnRadio(radioFour, 3);
   
   
 // Media queries
    var mqls = [
       window.matchMedia("(min-width: 0px) and (max-width: 768px)"), // MOBILE
       window.matchMedia("(min-width: 769px) and (max-width: 940px)") // TABLET
    ]
   
    function mediaqueryresponse(mql){
     // {max-width: 768px} MOBILE
       if (mqls[0].matches){
            $('.rightCart').css({'width':'100%','max-width':'100%'});
            $(".cartWrapper").css({'margin-top':'20px'});
            stickyCart();
            /********* Activation section changes *********/
            //create new happyHeader text
            //headerText.text("Sp�sob aktiv�cie");
            
            //radiobtns new texts
            // radioLabelTextSpan.eq(0).text('Pokra�ova� s existuj�cim Telekom pau��lom');
            // radioLabelTextSpan.eq(1).remove();
            // radioLabelTextSpan.eq(2).text('Zriadi� nov� ��slo');
            // radioLabelTextSpan.eq(3).remove();
            // radioLabelTextSpan.eq(4).text('Prenies� ��slo od in�ho oper�tora');
            // radioLabelTextSpan.eq(5).remove();
            // radioLabelTextSpan.eq(6).text('Prenies� ��slo z Easy karty');
            // radioLabelTextSpan.eq(7).remove();
       }
     // {min-width: 769px} and {max-width: 940px} TABLET
       if (mqls[1].matches){
            //KOSIK
            $('#undefined-sticky-wrapper').css('height', 'auto');
            $('.shoppingCartBottom-nextStep').css('margin-right', '0px');
            $(".cartWrapper").css({'margin-top':'0px'});
            stickyCart();
    
            radioLabel.css('display', 'inline-block');
       }
     // DESKTOP
       if (!mqls[0].matches && !mqls[1].matches){
            //KOSIK
            $('.label-inner-container .label').find('br').remove(); 
            $(".cartWrapper").css({'margin-top':'0px'});
            stickyCart();
       }
    }
   
    for (var i=0; i<mqls.length; i++){
       mediaqueryresponse(mqls[i]) // call listener function explicitly at run time
       mqls[i].addListener(mediaqueryresponse) // attach listener function to listen in on state changes
    }
 
 
 
    /**********************************************/
    /****************** KOSIK ******************/
     var cart = $('.shoppingScenario-bottom-container.PLAN');
         if ($('.row #shoppingScenarioForm').length > 0) { //fix vyber mobilu
         var detachedCart = $('.row #shoppingScenarioForm').eq(1).detach();
     } else {
         var detachedCart = $('#shoppingCart').detach();
     }

     var cartWrapper = $('.cartWrapper');
     cartWrapper.append(detachedCart);
     $('.top-marginer, .shoppingCartBottom-container-left, .shoppingCartBottom-mainProduct-price-container').remove();
     $('.shoppingCartBottom-container.grid-wrapper').attr('style', 'max-width:100% !important; width:auto !important; min-width:auto !important;');
     $('.cartWrapper .cart').css('margin-bottom','20px');
     $('.shoppingCartBottom-container-right').css({'margin-top':'0px','width':'100%'});
     $('.shoppingCartBottom-buttonNextStep').css({'margin-top':'20px','width':'100%'});
     $('.shoppingCartBottom-nextStep').css({'width':'100%','margin-left':'0px'});
     $('.shoppingCartBottom-container-small').hide(); //hide mangeta square on tablet
 
     var wantHW_YES = $('#uniform-order\\.wantHw_YES');
     var wantHW_NO = $('#uniform-order\\.wantHw_NO');
 
     wantHW_YES.on('click', function (){
         cart.prev().hide();
         cart.hide();
     });
 
     wantHW_NO.on('click', function (){
         cart.prev().show();
         cart.show();
     });
 
     $('.cartWrapper').wrapInner('<div class="wrappedCart"></div>'); //wrap cart elements to div

    //sticky cart
    function stickyCart() {
        var windowWidth = $(window).width();
        var url = window.location.href;
        if (windowWidth > 768) {
            if (url.indexOf("plan-sumar") > -1 || $('#section-shoppingScenario.stepSummary').length > 0) {

                $(".wrappedCart").sticky({
                    topSpacing: 0,
                    //widthFromWrapper: true,
                    getWidthFrom: '.cartWrapper'
                });

            }
        } else {
            $(".wrappedCart").unstick();
        }
    }
    stickyCart();

 
    //disabled next button on summary step if agreements not checked
    var url = window.location.href;
    if (url.indexOf("plan-sumar") > -1) {
        var nextBtn = $('.shoppingCartBottom-buttonNextStep.scenarioNavigation');
        function addBtnStatus() {
            if ($('#uniform-order\\.termsAgreement').children().hasClass("checked")) {
                nextBtn.removeClass('disabled');
            } else {
                nextBtn.addClass('disabled');
            }
        }
        addBtnStatus();

        $('#uniform-order\\.termsAgreement').on('click', function () {
            addBtnStatus();
        });
    }

 
 });