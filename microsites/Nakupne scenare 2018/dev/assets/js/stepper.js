//add href attribute to stepps
$("#section-shoppingScenario .stepper .step[data-href]").on("click", function() {
    window.location.href = $(this).data("href");
});

//if safari detected
if (navigator.userAgent.indexOf('Safari') != -1 && 
    navigator.userAgent.indexOf('Chrome') == -1) {
        $(".promo-circle span").removeClass("promo-circle-text");
        $(".promo-circle span").addClass("promo-circle-text-safari");       
}
