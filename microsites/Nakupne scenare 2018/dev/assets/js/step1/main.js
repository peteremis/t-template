$(function() {

    // function smsVerification(){
    //     $('#ajaxLoadingSpinnerOverlay').show(); //show loading
    //     //SMS VERIFICATION
    //     var URL_DATA = 'https://www.telekom.sk/mob/objednavka?p_p_id=shoppingScenario_WAR_portlets&p_p_lifecycle=2&p_p_state=normal&p_p_mode=view&p_p_cacheability=cacheLevelPage&p_p_col_id=column-1&p_p_col_pos=2&p_p_col_count=3&p_p_resource_id=smsVerification';
    //     $.ajax({
    //         url: URL_DATA,
    //         //data: tempData,
    //         dataType: "json",
    //         async: "true",
    //         success: function(data) {
    //         console.log(data);
    //         $('#ajaxLoadingSpinnerOverlay').hide(); //hide loading
            
    //         }, //end of success
    //         error: function() {
    //         console.log("ERROR");
    //         }
    //     });
    // }
 
    //click on RADIO BUTTON
    $('.activation input').on('click', function(){
        showElement("continueWithExisting");
        showElement("numberTransfer");
    })

    // show div with content for parent input
    function showElement(el) { 
        if($("#uniform-"+el+"").find('span').hasClass('checked')) {
            if (("#uniform-"+el+"").slice(9,100) == el) {
                $("."+el).show();
                checkInputLength ("."+el, 'check-number', 'input.number', 9);
                checkInputLength ("."+el, 'sms-code', 'input.sms', 3); 
                clickOnVerify("."+el);
            } else {
                $("."+el).hide();
            }
        } else {
            $("."+el).hide();
        }
    }

    // check input length and enable button
    function checkInputLength (element, inputField, inputButton, validLength) {
        $(element).find( "input."+inputField+"" ).keyup(function() {
            if ($(this).val().length - 1 == validLength) {
                $(element).find(inputButton).removeClass('disabled')
            }
            if ($(this).val().length - 1 < validLength) {
                $(element).find(inputButton).addClass('disabled')
            }
        });
    }

    //disable click event on disabled btns
    $('a.disabled').on('click', function(e){
        e.preventDefault();
    });

    //click on OVERIT
    function clickOnVerify(element) {
        $(element).find('input[type="button"]').on('click', function(){
            if ($(this).hasClass('disabled')) {
                return;
            } else {
                //input fields
                $(element).find('.check-number').hide();
                $(element).find('.sms-code').show();
        
                //buttons
                $(element).find('input.sms').show();
                $(element).find('input.number').hide();
        
                $(element).find('#verifyCode').css({'display':'table'}); //qtip sms
                $(element).find('#checkCustomerNumber').hide(); //qtip number
        
                //click after SMS CODE inserted
                $(element).find('input.sms').on('click', function(){
                    if ($(this).hasClass('disabled')) {
                        return;
                    } else {
                        $(element).find('.wrap').hide(); 
                        //show green text  
                        var inputVal = $(element).find('.check-number').val();
                        $(element).find('.message span').text(inputVal); 
                        $(element).find('.message').show();
                    }
                })  
            }
        })
    }
    
    function changeNumber(element){
        $('input.number, input.sms').addClass('disabled'); //after click on "change number" disable buttons

        $(element).find('.message').hide();
        $(element).find('.wrap').show();

        //phone number input & button
        $(element).find('.check-number').val('09').show();
        $(element).find('input.number').show();

        //sms input & button
        $(element).find('input.sms').hide();
        $(element).find('input.sms-code').val('').hide();

        $(element).find('#checkCustomerNumber').show(); //qtip number
        $(element).find('#verifyCode').hide(); //qtip sms

        // checkInputLength (element, 'check-number', 'input.number', 9);  //validate number
        // checkInputLength (element, 'sms-code', 'input.sms', 4); //validate sms
    }

    //click on ZMENIT CISLO
    $('.change-number').on('click', function(){
        changeNumber('.continueWithExisting');
        changeNumber('.numberTransfer');
    })


    //tooltips
    function generateQtip (element, text) {
        $(element).qtip({
            content: text,
            style: {
                classes: 'qtip-tipsy'
            },
            position: {
                my: 'bottom center',
                at: 'right center',
                adjust: {
                    x: -7,
                    y: -10
                }
            }
        });
    }
    generateQtip ('#checkCustomerNumber', 'Balík aktivujete na webe počas kúpy Happy paušálu.')
    generateQtip ('#verifyCode', 'Zadajte štvormiestný overovací kód bez medzier. Kód sme vám poslali v SMS tvare.')
    generateQtip ('.checkCustomerNumber-transfer', 'Balík aktivujete na webe počas kúpy Happy paušálu.')
    generateQtip ('.verifyCode-transfer', 'Zadajte štvormiestný overovací kód bez medzier. Kód sme vám poslali v SMS tvare.')

    //popup LSB
    //show popup only if agent is logged in
    if ($(".agentbox").length > 0) {
        $.fancybox('#lsbData',{
            padding: 0,
            margin: 0,
            maxWidth: "80%",
            autoSize: true,
            closeBtn: false,
            parent: "#content",
            helpers: {
                overlay: {
                    css: {
                        'background': 'rgba(0, 0, 0, 0.7)'
                    }
                }
            }
        });
    }
    $('.p-close, .link-close, .text-close, .close-btn').on('click', function(e) {
        e.preventDefault();
        $.fancybox.close();
    });

});
