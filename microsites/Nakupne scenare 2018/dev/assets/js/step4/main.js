$(function() {
    //tooltips
    function generateQtip (element, text) {
        $(element).qtip({
            content: text,
            style: {
                classes: 'qtip-tipsy'
            },
            position: {
                my: 'bottom center',
                at: 'right center',
                adjust: {
                    x: -7,
                    y: -10
                }
            }
        });
    }
    
    generateQtip ('#checkCustomerNumber', 'Balík aktivujete na webe počas kúpy Happy paušálu.')




});
