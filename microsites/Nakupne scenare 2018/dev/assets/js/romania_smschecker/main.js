$(function() {
    //summary arrow toggle click
    var clickCount = 0;
    $('.openSummaryCart i').on('click', function(e){
        e.preventDefault();
        if (clickCount === 0) {
            $('.cartSumm').addClass('whiteBg');
            $('.openSummaryCart').css({'position':'absolute','top':'10px'});
            $('.finalizeOrderBtn').addClass('widthFix');
            clickCount = 1;
        } else {
            $('.cartSumm').removeClass('whiteBg');
            $('.openSummaryCart').css({'position':'relative','top':'0px'});
            $('.finalizeOrderBtn').removeClass('widthFix');
            clickCount = 0;
        }
        
        $('.r_cartheadline, .r_cartSummary').toggleClass('closed');
        $(this).toggleClass('rotate');
    });
    
    //odoslat kod
    $('body').on('click', '.phoneNumBtn', function(e){
        e.preventDefault();
        var phoneNum = $('#phoneNum').val();
        $('.checkerHeadline').html("<span class='text'>Zaslali sme kód na číslo " + phoneNum + '</span>' + '<a href="#" class="btn btn_clean changeNum">Zmeniť číslo</a>');
        $('.checkerSubHeadline').text("Po zadaní správneho overovacieho kódu môžete jednoducho pokračovať v nákupe.");
        if ($('.smsChecker').hasClass('verified')) {
            $('.smsChecker').removeClass('verified');
        }
    });

    //zmenit cislo
    $('body').on('click', '.changeNum' , function(e){
        e.preventDefault();
        $('.checkerHeadline').html("<span>Overte prosím vaše existujúce tel. číslo</span>");
        $('.checkerSubHeadline').text("Zadajte, prosím, svoje telefónne číslo. Pošleme vám overovací kód, ktorým preveríme, či ste vlastníkom uvedeného telefónneho čísla.");
        if ($('.smsChecker').hasClass('verified')) {
            $('.smsChecker').removeClass('verified');
        }
    });

    //overit
    $('.phoneSMSBtn').on('click', function(e){
        e.preventDefault();
        var phoneNum = $('#phoneNum').val();
        $('.checkerHeadline').text("Ďakujeme, overili ste číslo " + phoneNum);
        $('.checkerSubHeadline').text("Presmerujeme vás do nákupného košíka, kde si môžete upraviť, alebo doplniť vašu objednávku.");
        $('.smsChecker').addClass('verified');
    });
})