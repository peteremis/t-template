$(function() {
    // Delivery checkbox is checked
    function personalInfo_toggleContent ( checkboxDiv, contentDiv ) {
        $(checkboxDiv+' .checker').on('click', function(){
            if ($(checkboxDiv + ' .checker span').hasClass('checked')) {
                $(contentDiv).show();
            } else {
                $(contentDiv).hide();
            }
        })
    }
    personalInfo_toggleContent ('.deliveryAdressContent', '.deliveryAdressFields' );

    // Order as a company toggle fields
    $('.formContent .checker').on('click', function(){
        if ($('.formContent .formField').hasClass('business')) {
            $('.formField.business').toggleClass('showBusiness');
        }
        $('.firstName, .lastName, .additionalFields').toggleClass('hidden')
    })

    // FormValidations
    $("#personalInfoForm").validate({
        rules: {
            ico: {
                required: true
            },
            dic: {
                required: true
            },
            icdph: {
                required: true
            },
            businessName: {
                required: true
            },
            ownerName: {
                required: true
            },
            firstName: {
                required: true,
                minlength: 2
            },
            lastName: {
                required: true,
                minlength: 2
            },
            email: {
                required: true,
                myEmail: true
            },
            PSC:{
                minlength: 5
            }
        },
        //Form custom messages
        messages: {
            ico:{
                required: "Uveďte vaše IČO"
            },
            dic:{
                required: "Uveďte vaše DIČ"
            },
            icdph:{
                required: "Uveďte vaše IČDPH"
            },
            businessName:{
                required: "Uveďte názov vašej spoločnosti"
            },
            ownerName:{
                required: "Uveďte meno konateľa spoločnosti"
            },
            firstName:{
                required: "Uveďte vaše krstné meno",
                minlength: "Vložte minimálne 2 znaky"
            },
            lastName:{
                required: "Uveďte vaše priezvisko",
                minlength: "Vložte minimálne 2 znaky"
            },
            email:{
                required: "Uveďte email v tvare vas@email.com",
                email: "Email musí byť v tvare vas@email.com"
            },
            mobPhone:{
                required: "Uveďte tel.č. v tvare +421 900 111 222"
            },
            city:{
                required: "Uveďte názov obce alebo mesta"
            },
            street:{
                required: "Uveďte názov ulice"
            },
            streetNo:{
                required: "Uveďte číslo domu"
            },
            PSC:{
                required: "Uveďte PSČ",
                minlength: "PSČ musí obsahovať 5 čísiel"
            }
        },
        // success form field
        success: function(input) {
            input.addClass("valid")
        },
        // error form field
        errorElement : 'div',
        errorClass: 'error error_message',
        errorPlacement: function(error, element) {
          var placement = $(element).data('error');
          if (placement) {
            $(placement).append(error)
          } else {
            error.insertAfter(element);
          }
        },
        submitHandler: function(form) {
            // do other things for a valid form
            form.submit();
        }
     });

    // custom email validationrule
    jQuery.validator.addMethod("myEmail", function(value, element) {
        return this.optional( element ) || ( /^[a-zA-Z0-9]+([-._][a-zA-Z0-9]+)*@([a-zA-Z0-9]+(-[a-zA-Z0-9]+)*\.)+[a-zA-Z]{2,4}$/.test( value ) && /^(?=.{1,64}@.{4,64}$)(?=.{6,100}$).*/.test( value ) );
    }, 'Email musí byť v tvare vas@email.com');

     
    //check if business conditions checked
    var conditionsCheckbox = $('input[type=checkbox]#businessCondChecker');
    var conditionsCheckboxLabel = $('label.businessCondChecker');
    var errorMsgTxt = "Potvrďte prosím súhlas s obchodnými podmienkami."
    var createErrorDiv = '<div class="error checkbox_error">'+errorMsgTxt+'</div>';

    $("#personalInfoForm").submit(function (e) { //on form submit click
        if (!$(conditionsCheckbox).is(':checked')) {
            $(conditionsCheckbox).closest('span').css({border: '1px solid red'})
            if (!$('label.businessCondChecker .error').length) {
                $(conditionsCheckboxLabel).append(createErrorDiv)
            }
            alert(errorMsgTxt)
            return false; //stop the form from submitting
        } else {
            $('.businessConditions .error.checkbox_error').remove();
        }
        return true; //submit form
    });

    //business conditions behavior - on checkbox click
    $('.businessConditions .checker').on('click', function(){
        if ($(conditionsCheckbox).is(':checked')) {
            $(conditionsCheckbox).closest('span').css({border: '1px solid green'})
            $('.businessConditions .error.checkbox_error').remove();
        } else {
            $(conditionsCheckbox).closest('span').css({border: '1px solid red'})
            $(conditionsCheckboxLabel).append(createErrorDiv)
        }
    })


});
