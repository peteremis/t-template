$(function() {

    var overit = '.phoneSMSBtn';
    var odoslatKod = '.phoneNumBtn';
    var zmenitCislo = '.changeNum';
    var smsVerifySection = $('.smsVerify');
    var cartContentSection = $('.shoppingCartLeft');
    var defaultSMSHeading = "Overte prosím vaše existujúce tel. číslo";
    var defaultSMSsubHeading = "Zadajte, prosím, svoje telefónne číslo. Pošleme vám overovací kód, ktorým preveríme, či ste vlastníkom uvedeného telefónneho čísla.";

    function remove_VerifiedStatus() {
        if ($('.smsChecker').hasClass('verified')) { 
            $('.smsChecker').removeClass('verified'); //remove green bg
        }
    }
    function resetSMSchecker() {
        $('.checkerHeadline').html("<span class='text'>"+defaultSMSHeading+'</span>');
        $('.checkerSubHeadline').text(defaultSMSsubHeading);
        remove_VerifiedStatus();
    }

    function smsChecker(buttonElement, headlineText, subheadlineText) {
        $('body').on('click', buttonElement, function(e){
            e.preventDefault();
            var phoneNum = $('#phoneNum').val();

            if (buttonElement == overit) { //overit
                $('.checkerHeadline').html("<span class='text'>"+ headlineText + "&nbsp " + phoneNum + '</span>' + '<a href="#" class="btn btn_clean changeNum">Zmeniť číslo</a>');
                $('#ajaxLoadingSpinnerOverlay').show();
                
                /////////
                //TODO!//
                /////////
                //odpoved z checkera, tento skript je len pre ilustraciu na frontende... 
                setTimeout(() => { //overovanie, success
                    $('#ajaxLoadingSpinnerOverlay').hide();
                    $('.smsChecker').addClass('verified'); //add green bg
                    setTimeout(() => {
                        smsVerifySection.slideUp();
                        cartContentSection.slideToggle();
                        setTimeout(() => { //reset checker to default
                            resetSMSchecker();
                        }, 1000);
                    }, 1000);
                }, 1000);

            } else if (buttonElement == zmenitCislo) { //zmenit cislo
                $('.checkerHeadline').html("<span class='text'>"+ headlineText + '</span>');
                remove_VerifiedStatus();
            } else { //odoslat kod
                $('.checkerHeadline').html("<span class='text'>"+ headlineText + "&nbsp " + phoneNum + '</span>' + '<a href="#" class="btn btn_clean changeNum">Zmeniť číslo</a>');
                remove_VerifiedStatus();
            }
            $('.checkerSubHeadline').text(subheadlineText);
        });
    }

    smsChecker(odoslatKod, //odoslat kod
               "Zaslali sme kód na číslo",
               "Po zadaní správneho overovacieho kódu môžete jednoducho pokračovať v nákupe."
               );
    smsChecker(zmenitCislo, //zmenit cislo
                defaultSMSHeading,
                defaultSMSsubHeading
                );
    smsChecker(overit, //overit
               "Ďakujeme, overili ste číslo",
               "Presmerujeme vás do nákupného košíka, kde si môžete upraviť, alebo doplniť vašu objednávku."
                );


    //if prolong checker is visible, hide cart on mobile devices
    var checkVisibility_smsChecker;
    function start_SMScheckerVisibilityCheck() {
        checkVisibility_smsChecker = setInterval(function () {
            checkerVisibility();
        }, 500);
    }

    var checkVisibility_cart;
    function start_cartVisibilityCheck() {
        checkVisibility_cart = setInterval(function () {
            if ($('.shoppingCartLeft').is(':visible') && !$('.smsVerify').is(':visible')) {
                $('.cartSumm').show();
                console.log("cart_showed")
                clearInterval(checkVisibility_cart);
            }
        }, 500);
    }

    function getWindowWidth_hideCart() {
        if ($('.smsVerify').is(':visible') && window.innerWidth < 1200) {
            $('.cartSumm').hide();
        } else {
            $('.cartSumm').show();
        }
    }

    function checkerVisibility() {
        if ($('.smsVerify').is(':visible')) {
            getWindowWidth_hideCart(); //hide or show cart on init
            $(window).resize(function () {
                getWindowWidth_hideCart(); //hide or show cart if user resize window
            });
            clearInterval(checkVisibility_smsChecker); //clear interval if element is visible
        }
    }

    $('.prolongNum a').bind('click', function(){ //click on prolong btn
        start_SMScheckerVisibilityCheck();
    })

    $('.phoneSMSBtn').bind('click', function(){ //click on verify number on prolong page
        start_cartVisibilityCheck();
    })
})