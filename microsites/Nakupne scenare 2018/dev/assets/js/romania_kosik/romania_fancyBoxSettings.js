  //* POPUP SETTINGS *//
        //init popup chooser
        $('.initPopup').fancybox({
            padding: 0,
            margin: 0,
            closeBtn: false,
            parent: "#content",
            helpers: {
                overlay: {
                    css: {
                        'background': 'rgba(0, 0, 0, 0.7)'
                    },
                    closeClick: false
                }
            },
            keys : {
                close  : null
            }
        }).trigger('click');
        $('.initChooser-close').on('click', function(e) {
            e.preventDefault();
            $.fancybox.close();
        });

        //init popup chooser buttons behavior
        $('.prolongNum').on('click', function(e){
            e.preventDefault();
            $('.smsVerify').show(); //hide sms verification
            $('.shoppingCartLeft').hide(); //show shopping cart content
            $.fancybox.close();
        });

        //more info popups
        $('.moreInfo').fancybox({
            padding: 0,
            margin: 0,
            closeBtn: false,
            parent: "#content",
            helpers: {
                overlay: {
                    css: {
                        'background': 'rgba(0, 0, 0, 0.7)'
                    }
                }
            }
        });
        $('.more-info.close-btn').on('click', function(e) {
            e.preventDefault();
            $.fancybox.close();
        });