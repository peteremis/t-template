$(function() {

    //fixed bottom summary - toggle click
    var clickCount = 0;
    $('.openSummaryCart i, .finalizeOrderBtn .cartheadlineBottom').on('click', function(e){
        e.preventDefault();
        if (clickCount === 0) {
            $('.cartSumm').addClass('whiteBg');
            $('.toggleSummBottom, .cartheadlineBottom').hide();
            $('.toggleSummTop').show();
            $('.finalizeOrderBtn, .saveOrder, .finalizeOrderBtn .btn').addClass('widthFix');
            clickCount = 1;
        } else {
            $('.cartSumm').removeClass('whiteBg');
            $('.toggleSummBottom, .cartheadlineBottom').show();
            $('.toggleSummTop').hide();
            $('.finalizeOrderBtn, .saveOrder, .finalizeOrderBtn .btn').removeClass('widthFix');
            clickCount = 0;
        }
        $('.r_cartheadline, .r_cartSummary').toggleClass('closed');
    });

        //* TOOLTIPS *//
        $('#info-ttip').qtip({
            content: 'Sem dame text...',
            style: {
                classes: 'qtip-tipsy'
            },
            position: {
                corner: {
                    target: 'leftMiddle',
                    tooltip: 'topMiddle'
                }
            }
        });

    // DELETE THIS, ONLY FOR DEV - START //
    //skript len pre DEV potreby, aby sme vedeli ukazat ako sa to bude hybat
    function addOrRemoveItem(itemWrapper, itemDetailDiv) {
        $(itemWrapper+ ' .trash i').on('click', function(e){
            e.preventDefault();
            //var deviceName = $(this).parents('.deviceWrapper').find('.deviceDetail .row.upper .deviceName').eq(0);
            var $this = this;
            var btnText = $(this).attr('class');
                btnText === "fa fa-trash" ? ($(this).removeClass('fa-trash').addClass('fa-plus-circle')) : ($(this).removeClass('fa-plus-circle').addClass('fa-trash'));
            
            var btnTextAfterUpdate = $(this).attr('class');
            console.log(btnTextAfterUpdate)
            if (btnTextAfterUpdate === "fa fa-plus-circle") {
                $($this).parents(itemWrapper).find(itemDetailDiv).toggleClass('inCart');
                $($this).parents(itemWrapper).find(itemDetailDiv).toggleClass('notInCart');
            } else {
                $($this).parents(itemWrapper).find(itemDetailDiv).toggleClass('notInCart');
                $($this).parents(itemWrapper).find(itemDetailDiv).toggleClass('inCart');
            }
        });
    }
    addOrRemoveItem('.programWrapper', '.programDetail'); //add or remove PROGRAM
    addOrRemoveItem('.deviceWrapper', '.deviceDetail'); //add or remove PHONE
    addOrRemoveItem('.singleVas', '.vasDetail'); //add or remove VAS
    addOrRemoveItem('.serviceContent', '.service'); //add or remove VAS
    // DELETE THIS, ONLY FOR DEV - END //


    //sticky summary cart
    function scrollCart() {
        var cartCol = $('.cartSumm');
        var cartButtonWrapper = $('.cartSummary_bottom');
        //on desktop
        if (window.innerWidth >= 1200) {   
            var scrollTop = $(window).scrollTop(),
                windowHeight = $(window).height(),
                docHeight = $(document).height(),
                rightHeight = cartButtonWrapper.height(),
                distanceToTop = rightHeight - windowHeight,
                distanceToBottom = scrollTop + windowHeight - docHeight,
                summaryOffsetTop = $(cartCol).offset().top,
                bottomPositionOfElement = $(cartCol).position().top + $(cartCol).height(),  
                summaryWidth = $(cartCol).width(),
                summaryOffsetBottom = docHeight - bottomPositionOfElement;
            if (scrollTop - summaryOffsetTop > 0 && scrollTop + windowHeight < bottomPositionOfElement ) {
                cartButtonWrapper.css({
                    'position': 'fixed',
                    'bottom': (scrollTop + windowHeight > docHeight ? distanceToBottom  + 'px' : '0px'),
                    'width': summaryWidth,
                    'padding': "0 10px"
                });
            } else if (scrollTop - summaryOffsetTop < 0) {
                cartButtonWrapper.css({
                    'position': 'relative',
                    'width': summaryWidth
                });
            }
            else if (scrollTop + windowHeight >= bottomPositionOfElement - $(cartCol).height() ) {
                cartButtonWrapper.css({
                    'position': 'absolute',
                    'bottom': '0',
                    'max-width' : "100%",
                    'width': summaryWidth
                });
            }
        } else {
        //on tablet and mobile
            cartButtonWrapper.css({
                'position': 'relative',
                'width': '100%'
            });
        }
    }
    $(window).on('scroll', scrollCart);
    $(window).on('resize', scrollCart);

});