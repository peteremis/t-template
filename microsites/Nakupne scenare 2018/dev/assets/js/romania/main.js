$(function() {

    // "shortDesc" toggle after click on pencil
    function showShortDesc(btn, stepSelector) {
        $(btn).on('click', function(e){
            e.preventDefault();
            $(''+stepSelector+' .notifications, '+stepSelector+' .formContent, '+stepSelector+' .shortDesc, '+stepSelector+' .headline .textRight, '+stepSelector+' .headline .icon_done, '+stepSelector+' .headline h3').slideToggle();
            $(stepSelector + ' .headline h1').removeClass('greyText'); //grey step headline
            $('#elCommunication, #generalTerms, #termsAgree').click(); //disable terms and conditions checkboxes

            switch(stepSelector) {
                case ('.step_personalInfo'): //step 1 pencil
                    if ($('.step_delivery .formContent').is(':visible')) {
                        $('.step_delivery .formContent').slideUp();
                        $('.step_delivery .step2_next').hide();
                        $('.step_delivery .shortDesc').slideToggle();
                        $('.step_delivery .textRight').slideToggle();
                        $('.step_delivery .icon_done').slideToggle();
                    }
                    if ($('.step_paymentOptions .formContent').is(':visible')) {
                        $('.step_paymentOptions .formContent').slideUp();
                        $('.step_paymentOptions .step3_next').hide();
                        $('.step_paymentOptions .shortDesc').slideToggle();
                        $('.step_paymentOptions .icon_done').show();  
                        //$('.step_paymentOptions .step3_detailBtn').hide(); 
                    }
                    if(!$('.step_delivery .headline h1').hasClass("greyText")){
                        $('.step_delivery .headline h1').addClass('greyText');
                    }
                    if(!$('.step_paymentOptions .headline h1').hasClass("greyText")){
                        $('.step_paymentOptions .headline h1').addClass('greyText');
                    }
                    $('.step_delivery .step2_detailBtn').slideUp();
                    $('.step_paymentOptions .step3_detailBtn').slideUp(); 
                    $('.step_delivery .headline h3').slideUp();
                    $('.step_paymentOptions .headline h3').slideUp();
                    $('.step_paymentOptions .shortDesc .row').html(""); //empty shortDesc row
                    $('.step_delivery .shortDesc .row').html(""); //empty shortDesc row
                    break;
                case ('.step_delivery'): //step 2 pencil
                    if ($('.step_personalInfo .formContent').is(':visible')) {
                        // $('.step_personalInfo .formContent').slideUp();
                        // $('.step_personalInfo .step1_next').hide();
                        // $('.step_personalInfo .shortDesc').slideToggle();
                        // $('.step_personalInfo .textRight').slideToggle();
                        // $('.step_personalInfo .icon_done').slideToggle();
                    }   
                    if ($('.step_delivery .formContent').is(':visible')) {
                        // $('.step_paymentOptions .formContent').slideUp();
                        // $('.step_paymentOptions .step3_next').hide();   
                        // $('.step_paymentOptions .shortDesc').slideDown();
                         //$('.step_paymentOptions .icon_done').show(); 
                        //$('.step_paymentOptions .step3_detailBtn').hide(); 
                    }
                    if ($('.step_paymentOptions .formContent').is(':visible')) {
                        $('.step_paymentOptions .formContent').slideUp();
                        $('.step_paymentOptions .step3_next').hide();
                        $('.step_paymentOptions .shortDesc').slideToggle();
                        $('.step_paymentOptions .icon_done').show(); 
                    }
                    if(!$('.step_personalInfo .headline h1').hasClass("greyText")){
                        $('.step_personalInfo .headline h1').addClass('greyText');
                    }
                    if(!$('.step_paymentOptions .headline h1').hasClass("greyText")){
                        $('.step_paymentOptions .headline h1').addClass('greyText');
                    }
                    //$('.step_paymentOptions .icon_done').slideToggle();  
                    $('.step_paymentOptions .step3_detailBtn').slideUp();
                    $('.step_paymentOptions .headline h3').slideUp();
                    $('.step_personalInfo .headline h3').slideUp();
                    break;
                case ('.step_paymentOptions'): //step 3 pencil
                    if(!$('.step_personalInfo .headline h1').hasClass("greyText")){
                        $('.step_personalInfo .headline h1').addClass('greyText');
                    }
                    if(!$('.step_delivery .headline h1').hasClass("greyText")){
                        $('.step_delivery .headline h1').addClass('greyText');
                    }
                    $('.step_delivery .headline h3').slideUp();
                    $('.step_personalInfo .headline h3').slideUp();
                    break;
                default:
                    $('.step1_next').show();
            }
        });
    }
    showShortDesc('.step1_detailBtn', '.step_personalInfo')
    showShortDesc('.step2_detailBtn', '.step_delivery')
    showShortDesc('.step3_detailBtn', '.step_paymentOptions')




    // Delivery - toggle billing content
    function toggleBillingContent ( checkboxDiv, contentDiv ) {
        $(checkboxDiv+' .checker').on('click', function(){
            if (!$(checkboxDiv + ' .checker span').hasClass('checked')) {
                $(contentDiv).show();
            } else {
                $(contentDiv).hide();
                var billingFormFieldslength = $(contentDiv +' .formField').length
                for (var index = 0; index < billingFormFieldslength; index++) {
                    var element = $(contentDiv + ' .formField').eq(index).find('input');
                    $(element).val('');
                }
            }
        })
    }
    //toggleBillingContent ('.deliveryForm', '.billingAddress' ); //toggle div - billing to another address
    toggleBillingContent ('.deliveryForm', '.deliveryAddress' ); //toggle div - delivery to another address
    


    // PersonalInfo - checkbox toggle business fields
    $('.step_personalInfo .formContent .checker#uniform-company').on('click', function(){
        if ($('.step_personalInfo .formContent .formField').hasClass('business')) {
            $('.step_personalInfo .formField.business').toggleClass('showBusinessFields');
        }
        $('.firstName, .lastName, .additionalFields').toggleClass('hidden')
    })





    var smsSubscriptCheckbox = $('input[type=checkbox]#smsSubscription');
    var emailSubscriptCheckbox = $('input[type=checkbox]#emailSubscription');
    var billingAddressCheckbox = 'input[type=checkbox]#billingAddress';
    var deliveryAddressCheckbox = 'input[type=checkbox]#deliveryAdress';
    var errorMsgTxt = "Potvrďte prosím súhlas s obchodnými podmienkami."
    var createErrorDiv = '<div class="error checkbox_error">'+errorMsgTxt+'</div>';

    // if delivery address not checked, change text in shortDesc
    $(deliveryAddressCheckbox).on('click', function() {
        if (!$(this).is(':checked')) {
            $('.delivery.shortDesc .billAddress').find('p').eq(1).text('Vaša faktúra bude zaslaná na Vami uvedenú doručovaciu adresu.')
        } else {
            $('.delivery.shortDesc .billAddress').find('p').eq(1).text('Vaša faktúra bude zaslaná na Vašu fakturačnú adresu.')
        }
    })

    //check if sms / email checkbox has been checked
    // function isCheckboxChecked(checkboxID, stopForm) {
    //     $(checkboxID).on('click', function(){
    //         if (!$(checkboxID).is(':checked')) {
    //             $(checkboxID).closest('span').css({border: '1px solid red'});
    //             return stopForm; //stop the form from submitting
    //         } else {
    //             $(checkboxID).closest('span').css({border: '1px solid green'})
    //         }
    //     });
    // }
    // isCheckboxChecked(smsSubscriptCheckbox, false);
    // isCheckboxChecked(emailSubscriptCheckbox, false);


    


    //step onSubmit
    function validateForm(formId, formStepDiv) {
        $(formId).submit(function (e) { 
            $(formId).validate(); //check form validity
            if (!$(formId).valid()) {
                $(formId).removeClass('formValid');
                alert(errorMsgTxt);
            } else {
                // stepps toggleDivs
                $(formStepDiv + ' .notifications' + ',' + formStepDiv + ' .formContent' + ',' + formStepDiv + ' .shortDesc' + ',' + formStepDiv + ' .headline .textRight' + ',' + formStepDiv + ' .headline .icon_done' + ',' + formStepDiv + ' .headline h3').slideToggle();
    
                // generate shortDesc
                if (formId !== "#paymentOptions") { //do not generate shortDesc in last step
                    $(formStepDiv + ' .shortDesc .row').html(""); //empty shortDesc row
                    for (i = 0; i < $(formId + ' .formField').length; i++) { //generate "shorDesc" items
                        if ($(formId + ' .formField').eq(i).find('input').length > 0 && $(formId + ' .formField').eq(i).find('input').val() != "") {
                            //console.log($(formId + ' .formField').eq(i).find('input').val());
                            var shortDescLabelName = $(formId + ' .formField').eq(i).find('label').text();
                            var shortDescVal = $(formId + ' .formField').eq(i).find('input').val();
                            var generateShortDescItem = '<div class="shortDescItem '+shortDescLabelName.replace(/[*\:]/g, '')+'">'+
                                '<p>'+shortDescLabelName+'</p>'+
                                '<p>'+shortDescVal+'</p>'+
                            '</div>'
                            
                            $(formStepDiv+' .shortDesc .row').append(generateShortDescItem)
                        } else {
                            console.log("undefined")
                        }
                    }
                }

                //handle "step_next" buttons behavior
                switch(formId) {
                    case ("#personalInfoForm"):
                        $('.step_delivery .textRight').show();
                        $('.step_delivery .shortDesc').slideUp();
                        $('.step_delivery .notifications').show();
                        $('.step_delivery .icon_done').hide();
                        $('.step_delivery .step2_next').show();
                        $('.step_delivery .deliveryAdressFields').show();

                        $('.step_personalInfo .headline h1').addClass('greyText');
                        $('.step_delivery .headline h3').slideDown();
                        $('.step_delivery .headline h1').removeClass('greyText');
                        $('.step_delivery .formContent').slideToggle();
                        break;

                    case ("#deliveryAdressFields"):
                        //$('.step_delivery .shortDesc').slideDown();
                        $('.step_delivery .step2_detailBtn').slideDown();

                        $('.step_paymentOptions .shortDesc').slideUp();
                        $('.step_paymentOptions .notifications').show();
                        $('.step_paymentOptions .icon_done').hide();
                        $('.step_paymentOptions .step3_next').show();
                        $('.step_paymentOptions .step3_detailBtn').hide();
                        
                        $('.step_delivery .headline h1').addClass('greyText');
                        $('.step_paymentOptions .headline h3').slideDown();
                        $('.step_paymentOptions .headline h1').removeClass('greyText');
                        $('.step_paymentOptions .formContent').slideToggle();
                        break;

                    case ("#paymentOptions"):
                        $('.step_paymentOptions .step3_detailBtn').show();
                        $('.step_paymentOptions .shortDesc').slideDown();
                        $('.step_paymentOptions .headline h1').addClass('greyText');
                        break;
                    default:
                        $('.step1_next').show();
                }
            }

            //on deliveryAdressFields submit, append delivery fields from PersonalInfoForm into it
            // if (formId === "#deliveryAdressFields" && !$('input[type=checkbox]#deliveryAdress').is(':checked')) {
            //     alert("pravda");
            //     $('.step_delivery .shortDesc .row').html("");
            //     var clonedAdresses = $('.shortDescItem.Obec, .shortDescItem.Ulica, .shortDescItem.Číslo, .shortDescItem.PSČ').clone();
                
            //     $('.step_delivery .shortDesc .row').html(clonedAdresses);
            // }
        });
    }
    validateForm("#personalInfoForm", ".step_personalInfo") //step1 onSubmit
    validateForm("#deliveryAdressFields", ".step_delivery") //step2 onSubmit
    //validateForm("#paymentOptions", ".step_paymentOptions") //step3 onSubmit
    


    //**PAYMENT METHOD STEP**//

    //switchery - billing printed or billing electronic
    var switcherElement = document.querySelector('.js-switch');
    //var initSwitcher = new Switchery(switcherElement, { color: '#6bb324', secondaryColor: '#ededed', width: 100 +'px' });
    var switcherLabel = 'label[for="switch"]';
    $('.switchery').parent().closest('span').addClass('hide-span'); //visual span fix

    function changeSwitchery(element, switchCheckboxID, switcherLabel) {
        //console.log( $('[type="checkbox"]'+switchCheckboxID).is(":checked")) //init swtichery status
        $(switcherLabel).eq(1).css({'color':'#e20074'});

        $('.switchery, '+switcherLabel+'').on('click', function(e){
            var clickedEl = this.tagName;
            
            if (element.prop('checked') === true) {
                $(switcherLabel).eq(0).css({'color':'#000'});
                $(switcherLabel).eq(1).css({'color':'#e20074'});
                element.prop( "checked", false)
                if (clickedEl === "SPAN") {
                    $('[type="checkbox"]'+switchCheckboxID).trigger('click')
                }
                if (clickedEl === "LABEL") {
                    $('[type="checkbox"]'+switchCheckboxID).trigger('click');
                    $(switcherLabel).eq(0).css({'color':'#e20074'});
                    $(switcherLabel).eq(1).css({'color':'#000'});
                }
            } else {
                $(switcherLabel).eq(0).css({'color':'#e20074'});
                $(switcherLabel).eq(1).css({'color':'#000'});
                element.prop("checked", true)
                if (clickedEl === "SPAN") {
                    $('[type="checkbox"]'+switchCheckboxID).trigger('click')
                }
                if (clickedEl === "LABEL") {
                    $('[type="checkbox"]'+switchCheckboxID).trigger('click');
                    $(switcherLabel).eq(0).css({'color':'#000'});
                    $(switcherLabel).eq(1).css({'color':'#e20074'});
                }
            }
        })

        $('[type="checkbox"]'+switchCheckboxID).click(function(e) {
            var isChecked = $(this).is(":checked");
            console.log('switcheryChecked: ' + isChecked);
            //$("#switchText").html(isChecked?'Yes checked':'No checked');
        });
    }
    changeSwitchery($(switcherElement), '#switch', switcherLabel);


    //enable next button if all checkboxes are checked and payment method is selected
    var step3btn = '.finalizeOrder';
    var paymentOptionsForm = '#paymentOptions';
    var paymentOptionCard = 'payment-Card';
    var paymentOptionTransfer = 'payment-Transfer';
    var paymentOptionDelivery = 'payment-Delivery';
    var paymentMethodSquare = ".choosePayment div[id*='payment-Delivery']"; // change to [id*='payment-'] if all payment options are eligible
    var paymentIsSelected = false;

    function disableOrderBtn (btn) {
        $(btn).on('click', function(e){
            e.preventDefault();
        });
        if (!$(btn).hasClass('disabled')) {
            $(btn).addClass('disabled');
        }
    }
    function validatePaymentMethod_enableOrderBtn () {
        //if form valid (checkboxes are checked and payment method was clicked)
        if ($(paymentOptionsForm).valid() && paymentIsSelected === true){
            $(paymentOptionsForm).addClass('formValid');
            $(step3btn).unbind('click').removeClass('disabled');
        } else {
            $(paymentOptionsForm).removeClass('formValid');
            disableOrderBtn(step3btn);
        }
    }

    //disable order btn on init
    disableOrderBtn(step3btn); 

    //enable order btn if checkboxes are checked and payment method is selected
    $('#elCommunication, #generalTerms, #termsAgree').on('click', function(){
        validatePaymentMethod_enableOrderBtn();
    })

    //show <div> for selected payment method
    $(paymentMethodSquare).on('click', function() {
        paymentIsSelected = true;
        validatePaymentMethod_enableOrderBtn();
        var paymentMethodName = $(this).find('p').text();
        shortDescForPaymentOption (paymentMethodName);

        $("div[class*='payment-']").hide(); //hide all payment option divs on click
        $(this).siblings().find('.bgWhite').removeClass('magentaBg');
        $(this).siblings().find('.bgWhite p').css({'color':'#000'});
        $(this).find('.bgWhite').addClass('magentaBg');
        $(this).find('.bgWhite p').css({'color':'#fff'});

        var clickedPaymentName = $(this).attr('id'); //get payment attr id
        switch (clickedPaymentName) {
            case (paymentOptionCard):
                $('.'+clickedPaymentName).show();
                break;
            case (paymentOptionTransfer):
                $('.'+clickedPaymentName).show();
                break;
            case (paymentOptionDelivery):
                $('.'+clickedPaymentName).show();
                break;
            default:
                break;
        }
    })

    //generate shortDesc for selected paymentOption
    function shortDescForPaymentOption (paymentName) {
        $('.step_paymentOptions .shortDesc .row').html("");
        var generatePaymentDetail = '<div class="paymentOptionDetails">'+
            '<div class="paymentMethod">'+
                '<p>Ako možnosť platby ste si vybrali:</p>'+
                '<p>'+paymentName+'</p>'+
            '</div>'+
        '</div>';

        $('.step_paymentOptions .shortDesc .row').append(generatePaymentDetail);
    }

    // var eventFired = 0;
    // $(window).on('resize', function() {
    //     if (!eventFired) {
    //         if ($(window).width() < 768) {
    //             //$(".fixedCart").unstick();
    //         } else {
    //             $(".fixedCart").sticky({
    //                 //topSpacing: 20,
    //                 //bottomSpacing: 490,
    //                 widthFromWrapper: true,
    //                 getWidthFrom: '.right_summary',
    //             });
    //         }
    //     }
    // });

    //sticky summary cart
    function scrollCart() {
        var cartButtonWrapper = $('.fixedCart');
        var cartCol = $('.right_summary');
        // var sumHeight = 0;
        // for (i = 0; i < $('.cartItem').length; i++) {	
        //     var height = $('.cartItem').eq(i).outerHeight();
        //     sumHeight += height;
        // }
        // console.log(sumHeight)
        //on desktop
        if (window.innerWidth >= 1200) {   
            var scrollTop = $(window).scrollTop(),
                windowHeight = $(window).height(),
                docHeight = $(document).height(),
                rightHeight = cartButtonWrapper.height(),
                distanceToTop = rightHeight - windowHeight,
                distanceToBottom = scrollTop + windowHeight - docHeight,
                summaryOffsetTop = $(cartCol).offset().top,
                bottomPositionOfElement = $(cartCol).position().top + $(cartCol).height(),  
                summaryWidth = $('.right_summary .summary').width(),
                summaryOffsetBottom = docHeight - bottomPositionOfElement;
                //console.log(scrollTop - summaryOffsetTop);
                //console.log(summaryOffsetBottom);
            if (scrollTop - summaryOffsetTop > 0 && scrollTop + windowHeight < bottomPositionOfElement ) {
                cartButtonWrapper.css({
                    'position': 'fixed',
                    'bottom': (scrollTop + windowHeight > docHeight ? distanceToBottom  + 'px' : '0px'),
                    'width': summaryWidth
                });
            } else if (scrollTop - summaryOffsetTop < 0) {
                cartButtonWrapper.css({
                    'position': 'relative',
                    'width': 'inherit'
                });
            }
            else if (scrollTop + windowHeight >= bottomPositionOfElement - $('.summaryCart').height() ) {
                cartButtonWrapper.css({
                    'position': 'absolute',
                    'bottom': '0',
                    'width': summaryWidth
                });
            }
        } else {
        //on tablet and mobiles
            cartButtonWrapper.css({
                'position': 'relative',
                'width': 'inherit'
            });
        }
    }
    
    $(window).on('scroll', scrollCart);
    $(window).on('resize', scrollCart);

    //toggle shortDesc
    $('.toggleShortDesc').on('click', function(){
        $(this).closest('.shortDesc').find('.shortDescWrapper').slideToggle();
    })

    //step2 onSubmit
    // $("#deliveryAdressFields").submit(function (e) { 
    //     $("#deliveryAdressFields").validate(); //check form validity
    //     if (!$('#deliveryAdressFields').valid()) {
    //         alert(errorMsgTxt)
    //     } else {
    //         // step2 - delivery
    //         $('.step_delivery .notifications, .step_delivery .formContent, .step_delivery .shortDesc, .step_delivery .headline .textRight, .step_delivery .headline .icon_done').slideToggle();
    //         //getValue('#firstName', '.shortDescName'); //get value from inputField to shortDesc div
    //     }
    // });






    //business conditions on checkbox click
    // $('.businessConditions .checker').on('click', function(){
    //     if ($(conditionsCheckbox).is(':checked')) {
    //         $(conditionsCheckbox).closest('span').css({border: '1px solid green'})
    //         $('.businessConditions .error.checkbox_error').remove();
    //     } else {
    //         $(conditionsCheckbox).closest('span').css({border: '1px solid red'})
    //         $(conditionsCheckboxLabel).append(createErrorDiv)
    //     }
    // })


});
