  // FormValidations
  $("#personalInfoForm").validate({
    rules: {
        ico: {
            required: true
        },
        dic: {
            required: true
        },
        icdph: {
            required: true
        },
        businessName: {
            required: true
        },
        ownerName: {
            required: true
        },
        firstName: {
            required: true,
            minlength: 2
        },
        lastName: {
            required: true,
            minlength: 2
        },
        email: {
            required: true,
            myEmail: true
        },
        mobPhone: {
            required: true,
            phoneNum: true
        },
        PSC:{
            minlength: 5
        },
        emailSbuscription: {
            required: true
        }
    },
    //Form custom messages
    messages: {
        ico:{
            required: "Uveďte vaše IČO"
        },
        dic:{
            required: "Uveďte vaše DIČ"
        },
        icdph:{
            required: "Uveďte vaše IČDPH"
        },
        businessName:{
            required: "Uveďte názov vašej spoločnosti"
        },
        ownerName:{
            required: "Uveďte meno konateľa spoločnosti"
        },
        firstName:{
            required: "Uveďte vaše krstné meno",
            minlength: "Vložte minimálne 2 znaky"
        },
        lastName:{
            required: "Uveďte vaše priezvisko",
            minlength: "Vložte minimálne 2 znaky"
        },
        email:{
            required: "Uveďte email v tvare vas@email.com",
            email: "Email musí byť v tvare vas@email.com"
        },
        mobPhone:{
            required: "Uveďte tel.č. v tvare 0944111222",
        },
        city:{
            required: "Uveďte názov obce alebo mesta"
        },
        street:{
            required: "Uveďte názov ulice"
        },
        streetNo:{
            required: "Uveďte číslo domu"
        },
        PSC:{
            required: "Uveďte PSČ",
            minlength: "PSČ musí obsahovať 5 čísiel"
        }
    },
    // success form field
    submitHandler:function(form){
        if (this.valid()){
          $('#personalInfoForm').addClass('formValid');
        }
    },  
    success: function(input) {
        input.addClass("valid")
    },
    // error form field
    errorElement : 'div',
    errorClass: 'error error_message',
    errorPlacement: function(error, element) {
      var placement = $(element).data('error');
      if (placement) {
        $(placement).append(error)
      } else {
        error.insertAfter(element);
      }
    }
 });

 $("#deliveryAdressFields").validate({
    rules: {
        deliveryCity: {
            required: true
        },
        deliveryStreet:{
            required: true
        },
        deliveryStreetNo:{
            required: true
        },
        deliveryPSC:{
            required: true
        }
    },
    //Form custom messages
    messages: {
        deliveryCity: {
            required: "Uveďte názov obce alebo mesta"
        },
        deliveryStreet:{
            required: "Uveďte názov ulice"
        },
        deliveryStreetNo:{
            required: "Uveďte číslo domu"
        },
        deliveryPSC:{
            required: "Uveďte PSČ"
        },
    },
    // success form field
    submitHandler:function(form){
        if (this.valid()){
          $('#deliveryAdressFields').addClass('formValid');
        }
    },  
    success: function(input) {
        input.addClass("valid")
    },
    // error form field
    errorElement : 'div',
    errorClass: 'error error_message',
    errorPlacement: function(error, element) {
      var placement = $(element).data('error');
      if (placement) {
        $(placement).append(error)
      } else {
        error.insertAfter(element);
      }
    }
 });

 $("#paymentOptions").validate({
    rules: {
        elCommunication: {
            required: true
        },
        generalTerms:{
            required: true
        },
        termsAgree:{
            required: true
        }
    },
    //Form custom messages
    messages: {
        elCommunication: {
            required: "Potvrďte prosím súhlas s uzatvorením Dohody o elektronickej komunikácií."
        },
        generalTerms:{
            required: "Potvrďte prosím súhlas so všeobecnými obchodnými podmienkami."
        },
        termsAgree:{
            required: "Potvrďte prosím súhlas so zmluvnými podmienkami."
        }
    },
    // success form field
    submitHandler:function(form){
        if (this.valid()){
          $('#paymentOptions').addClass('formValid');
        }
    },  
    success: function(input) {
        input.addClass("valid")
    },
    highlight: function(element, errorClass, validClass) { //not checked checkbox
        $(element).closest('span').removeClass(validClass).addClass(errorClass);
    },
    unhighlight: function(element, errorClass, validClass) { //checked checkbox
        $(element).closest('span').removeClass(errorClass).addClass(validClass);
    },
    // error form field
    errorElement : 'div',
    errorClass: 'error error_message termsError',
    errorPlacement: function(error, element) {
      var placement = $(element).data('error');
      if (placement) {
        $(placement).append(error);
      } else {
        error.insertAfter(element.closest('.formField').find('label'));         
      }
    }
 });


// custom email validation rule
jQuery.validator.addMethod("myEmail", function(value, element) {
    return this.optional( element ) || ( /^[a-zA-Z0-9]+([-._][a-zA-Z0-9]+)*@([a-zA-Z0-9]+(-[a-zA-Z0-9]+)*\.)+[a-zA-Z]{2,4}$/.test( value ) && /^(?=.{1,64}@.{4,64}$)(?=.{6,100}$).*/.test( value ) );
}, 'Email musí byť v tvare vas@email.com');

//custom phone num validation rule
jQuery.validator.addMethod ("phoneNum", function( value, element ) {
    return this.optional( element ) || /^[0](?:\d(?:\(\d{3}\)|-\d{3})-\d{3}-(?:\d{2}-\d{2}|\d{4})|\d{9})$/.test( value );
}, 'Telefónne číslo musí byť v tvare 0944111222')

