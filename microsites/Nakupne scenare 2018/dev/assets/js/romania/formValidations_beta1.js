  // FormValidations
  $("#personalInfo_form").validate({
    rules: {
        ico: {
            required: true
        },
        dic: {
            required: true
        },
        icdph: {
            required: true
        },
        businessName: {
            required: true
        },
        ownerName: {
            required: true
        },
        firstName: {
            required: true,
            minlength: 2
        },
        lastName: {
            required: true,
            minlength: 2
        },
        email: {
            required: true,
            myEmail: true
        },
        mobPhone: {
            required: true,
            phoneNum: true
        },
        PSC:{
            minlength: 5
        },
        emailSbuscription: {
            required: true
        },
        //step delivery
        deliveryCity: {
            required: true
        },
        deliveryStreet:{
            required: true
        },
        deliveryStreetNo:{
            required: true
        },
        deliveryPSC:{
            required: true
        },
        courier:{
            required: true,
            phoneNum: true
        },
        //step paymentOptions
        elCommunication: {
            required: true
        },
        generalTerms:{
            required: true
        },
        termsAgree:{
            required: true
        }
    },
    //Form custom messages
    messages: {
        ico:{
            required: "Uveďte vaše IČO"
        },
        dic:{
            required: "Uveďte vaše DIČ"
        },
        icdph:{
            required: "Uveďte vaše IČDPH"
        },
        businessName:{
            required: "Uveďte názov vašej spoločnosti"
        },
        ownerName:{
            required: "Uveďte meno konateľa spoločnosti"
        },
        firstName:{
            required: "Uveďte vaše krstné meno",
            minlength: "Vložte minimálne 2 znaky"
        },
        lastName:{
            required: "Uveďte vaše priezvisko",
            minlength: "Vložte minimálne 2 znaky"
        },
        email:{
            required: "Uveďte email v tvare vas@email.com",
            email: "Email musí byť v tvare vas@email.com"
        },
        mobPhone:{
            required: "Uveďte tel.č. v tvare 0944111222",
        },
        city:{
            required: "Uveďte názov obce alebo mesta"
        },
        street:{
            required: "Uveďte názov ulice"
        },
        streetNo:{
            required: "Uveďte číslo domu"
        },
        PSC:{
            required: "Uveďte PSČ",
            minlength: "PSČ musí obsahovať 5 čísiel"
        },
        //step delivery
        deliveryCity: {
            required: "Uveďte názov obce alebo mesta"
        },
        deliveryStreet:{
            required: "Uveďte názov ulice"
        },
        deliveryStreetNo:{
            required: "Uveďte číslo domu"
        },
        deliveryPSC:{
            required: "Uveďte PSČ"
        },
        courier:{
            required: "Uveďte tel.č. v tvare 0944111222"
        },
        //step paymentOptions
        elCommunication: {
            required: "Potvrďte prosím súhlas s uzatvorením Dohody o elektronickej komunikácií."
        },
        generalTerms:{
            required: "Potvrďte prosím súhlas so všeobecnými obchodnými podmienkami."
        },
        termsAgree:{
            required: "Potvrďte prosím súhlas so zmluvnými podmienkami."
        }
    },
    // success form field
    submitHandler:function(form){
        if (this.valid()){
          $('#personalInfoForm').addClass('formValid');
        }
    },  
    success: function(input) {
        input.addClass("valid")
    },
    highlight: function(input, element, errorClass, validClass) { 
        $(input).closest('span').removeClass("valid").addClass("error"); //checkboxes
        $(input).removeClass("valid").addClass("error"); //other inputs
    },
    unhighlight: function(input, element, errorClass, validClass) {
        $(input).closest('span').removeClass("error").addClass("valid"); //checkboxes
        $(input).removeClass("error").addClass("valid"); //other inputs
    },
    // error form field
    errorElement : 'div',
    errorClass: 'error error_message termsError',
    errorPlacement: function(error, element) {
        var placement = $(element).data('error');
      if (placement) {
        //$(placement).append(error)
      } else {
        //error.insertAfter(element);
        for (let index = 0; index < element.length; index++) {
            const el = element[index];
            if ($(el).attr("id") === "termsAgree" ||
                $(el).attr("id") === "generalTerms" ||
                $(el).attr("id") === "elCommunication" 
            ) {
                error.insertAfter($(el).closest('.formField').find('label'));
            } else {
                error.insertAfter($(el).closest('.formField').find('input'));
            }
            
        }
      }
    }
 });

// custom email validation rule
jQuery.validator.addMethod("myEmail", function(value, element) {
    return this.optional( element ) || ( /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test( value ) );
}, 'Email musí byť v tvare vas@email.com');

//custom phone num validation rule
jQuery.validator.addMethod ("phoneNum", function( value, element ) {
    return this.optional( element ) || /^0(9)\d{8}$/.test( value );
}, 'Telefónne číslo musí byť v tvare 0944111222')

