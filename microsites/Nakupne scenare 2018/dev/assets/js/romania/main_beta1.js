$(function () {

    function fixMarginOn_lastInput() { 
        $('.step_personalInfo .formField:not(.additionalFields .formField)').filter(':visible').each(function (i) {
            var modulus = (i + 1) % 3;
            $(this).addClass('m-right');
            if (modulus === 0) {
                $(this).removeClass('m-right');
            } 
        });
    }
    fixMarginOn_lastInput();
    

    /*** STEP PERSONAL INFO ***/
    // PersonalInfo - checkbox toggle business fields
    $('.step_personalInfo .formContent .checker#uniform-company').on('click', function () {
        if ($('.step_personalInfo .formContent .formField').hasClass('business')) {
            $('.step_personalInfo .formField.business').toggleClass('showBusinessFields');
        }
        $('.firstName, .lastName, .additionalFields').toggleClass('hidden');
        fixMarginOn_lastInput();
    })

    /*** STEP DELIVERY ***/
    // Delivery - toggle billing content
    function toggleBillingContent(checkboxDiv, contentDiv) {
        $(checkboxDiv + ' .checker').on('click', function () {
            if (!$(checkboxDiv + ' .checker span').hasClass('checked')) {
                $(contentDiv).show();
            } else {
                $(contentDiv).hide();
                var billingFormFieldslength = $(contentDiv + ' .formField').length
                for (var index = 0; index < billingFormFieldslength; index++) { //reset input values
                    var element = $(contentDiv + ' .formField').eq(index).find('input');
                    $(element).val('');
                }
            }
        })
       
    }
    toggleBillingContent('.deliveryForm', '.deliveryAddress'); //toggle div - delivery to another address
    
    
    /*** STEP PAYMENT METHOD ***/
    //enable next button if all checkboxes are checked and payment method is selected
    var step3btn = '.finalizeOrder';
    var paymentOptionsForm = '#personalInfo_form';
    var paymentOptionCard = 'payment-Card';
    var paymentOptionTransfer = 'payment-Transfer';
    var paymentOptionDelivery = 'payment-Delivery';
    var paymentMethodSquare = ".choosePayment div[id*='payment-Delivery']"; // change to [id*='payment-'] if all payment options are eligible
    var paymentIsSelected = false;

    function disableOrderBtn (btn) {
        $(btn).on('click', function(e){
            e.preventDefault();
        });
        if (!$(btn).hasClass('disabled')) {
            $(btn).addClass('disabled');
        }
    }

    //disable order btn on init
    disableOrderBtn(step3btn); 

    function validatePaymentMethod_enableOrderBtn () {
        //if form valid (checkboxes are checked and payment method was clicked)
        if ($(paymentOptionsForm).valid() && paymentIsSelected === true) {
            $(paymentOptionsForm).addClass('formValid');
            $(step3btn).unbind('click').removeClass('disabled');
        } else {
            $(paymentOptionsForm).removeClass('formValid');
            disableOrderBtn(step3btn);
        }
    }

    //validate all inputs 
    function validateInputs(eventType, inputType) {
        $('form').on(eventType, inputType, function () {
            $(this).valid();
            if ($('#elCommunication').closest('span').hasClass('checked') && $('#generalTerms').closest('span').hasClass('checked')) {
                validatePaymentMethod_enableOrderBtn();
            } else {
                disableOrderBtn(step3btn); 
            }
        });
    }

    validateInputs('keyup','input');
    validateInputs('click','input[type="checkbox"]');


    //show content <div> for selected payment method
    $(paymentMethodSquare).on('click', function() {
        paymentIsSelected = true;
        validatePaymentMethod_enableOrderBtn();

        $("div[class*='payment-']").hide(); //hide all payment option divs on click
        $(this).siblings().find('.bgWhite').removeClass('magentaBg active').addClass('inactive');
        $(this).siblings().find('.bgWhite p').css({'color':'#000'});
        $(this).find('.bgWhite').addClass('magentaBg active');
        $(this).find('.bgWhite p').css({'color':'#fff'});

        var clickedPaymentName = $(this).attr('id'); //get payment attr id
        switch (clickedPaymentName) {
            case (paymentOptionCard):
                $('.'+clickedPaymentName).show();
                break;
            case (paymentOptionTransfer):
                $('.'+clickedPaymentName).show();
                break;
            case (paymentOptionDelivery):
                $('.'+clickedPaymentName).show();
                break;
            default:
                break;
        }
    })



    //sticky summary cart
    function scrollCart() {
        var cartButtonWrapper = $('.fixedCart');
        var cartCol = $('.right_summary');
        // var sumHeight = 0;
        // for (i = 0; i < $('.cartItem').length; i++) {	
        //     var height = $('.cartItem').eq(i).outerHeight();
        //     sumHeight += height;
        // }
        // console.log(sumHeight)
        //on desktop
        if (window.innerWidth >= 1200) {   
            var scrollTop = $(window).scrollTop(),
                windowHeight = $(window).height(),
                docHeight = $(document).height(),
                rightHeight = cartButtonWrapper.height(),
                distanceToBottom = scrollTop + windowHeight - docHeight,
                summaryOffsetTop = $(cartCol).offset().top,
                bottomPositionOfElement = $(cartCol).position().top + $(cartCol).height(),  
                summaryWidth = $('.right_summary .summary').width();
            if (scrollTop - summaryOffsetTop > 0 && scrollTop + windowHeight < bottomPositionOfElement ) {
                cartButtonWrapper.css({
                    'position': 'fixed',
                    'bottom': (scrollTop + windowHeight > docHeight ? distanceToBottom  + 'px' : '0px'),
                    'width': summaryWidth
                });
            } else if (scrollTop - summaryOffsetTop < 0) {
                cartButtonWrapper.css({
                    'position': 'unset',
                    'width': 'inherit'
                });
            }
            else if (scrollTop + windowHeight >= bottomPositionOfElement - $('.summaryCart').height() ) {
                cartButtonWrapper.css({
                    'position': 'absolute',
                    'bottom': '0',
                    'max-width' : "100%",
                    'width': summaryWidth
                });
            }
        } else {
        //on tablet and mobiles
            cartButtonWrapper.css({
                'position': 'relative',
                'width': 'inherit'
            });
        }
    }
    
    $(window).on('scroll', scrollCart);
    $(window).on('resize', scrollCart);

});
