/* TOGGLE ARROW */
$('.toggle-arrow, .arrow-right').click(function(event) {
    event.preventDefault();
    if ($(this).hasClass('arrow-right')) {
        return;
    }
    $(event.target).closest('.togleArrow').find('.arrow-content').toggle(200);
    $(this).find('.arrow-right').toggleClass('arrow-rotate');
});
/* TOGGLE ARROW END */