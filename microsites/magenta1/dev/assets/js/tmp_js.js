$(document).ready(function() {

    if (isMagentaOffice() == 1) {
        $('#sec-2').css('background-image', 'url(https://m.telekom.sk/wb/magenta1/v2/img/bg-main_office.png)');
        $('#hero').css('background-image', 'url(https://m.telekom.sk/wb/magenta1/v2/img/kv_office.jpg)');
    } else {
        $('#sec-2').css('background-image', 'url(https://m.telekom.sk/wb/magenta1/v2/img/bg-main.jpg)');
        $('#hero').css('background-image', 'url(https://www.telekom.sk/documents/10179/1502110/kv_v3.png');
    }

});

