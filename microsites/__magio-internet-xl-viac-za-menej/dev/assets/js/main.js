$(function() {

    var $form = $('#resend');
    var termsCheckBox = $('#termscheck');
    var sendFormBtn = $('.sendfrm');

    // initialize the validation plugin
    var formValidator = $form.validate({ 
        rules: {
            firstName: {
                required: false,
                minlength: 3,
            },
            lastName: {
                required: false,
                minlength: 2,
            },
            useremail: {
                required: true,
                email: true,
                customEmailValidation: true
            },
            phoneNum: {
                required: true,
                customNumberValidation: true,
                maxlength: 10
            },
            termscheck: {
                required: true
            }
        },
        messages: {
            firstName: {
                required: 'Napíšte svoje meno.',
                minlength: 'Meno musí mať aspoň 3 znaky.'
            },
            lastName: {
                required: 'Napíšte svoje priezvisko.',
                minlength: 'Priezvisko musí mať aspoň 3 znaky.'
            },
            useremail: {
                required: 'Napíšte váš e-mail.',
                email: 'Napíšte svoj e-mail v tvare vas@email.sk',
                customEmailValidation: 'Napíšte email v správnom tvare: vas@email.sk'
            },
            phoneNum: {
                required: 'Napíšte svoje mobilné telefónne číslo.',
                customNumberValidation: 'Napíšte tel. číslo v tvare 0911 222 333.',
                maxlength: 'Telefónne číslo môže obsahovať maximálne 10 číslic.'
            },
            termscheck: {
                required: 'Zaškrtnite prosím súhlas s podmienkami.'
            }
        },
        errorClass: "error",
        validClass: "valid",
        onclick: function(element) { //for checkboxes only
            $form.valid();
            if (formValidator.numberOfInvalids() === 0 && $('#termscheck').is(':checked')) {
                $('.doneico').attr('src','/documents/10179/17623083/done-green.png');
            } else {
                $('.doneico').attr('src','/documents/10179/17623083/done.png');
            }
        },
        onfocusout: function(element) {
            this.element(element);
            if (formValidator.numberOfInvalids() === 0 && $('#termscheck').is(':checked')) {
                $('.doneico').attr('src','/documents/10179/17623083/done-green.png');
            } else {
                $('.doneico').attr('src','/documents/10179/17623083/done.png');
            }
        },
        onkeyup: function(element) {
            this.element(element);
            if (formValidator.numberOfInvalids() === 0 && $('#termscheck').is(':checked')) {
                $('.doneico').attr('src','/documents/10179/17623083/done-green.png');
            } else {
                $('.doneico').attr('src','/documents/10179/17623083/done.png');
            } 
        },
        invalidHandler: function(errors) {
           // console.log(formValidator.numberOfInvalids() + " field(s) are invalid")
        },
        highlight: function (element, errorClass, validClass) {
            $(element).parents('.form-group').addClass(errorClass).removeClass(validClass)
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents('.form-group').removeClass(errorClass).addClass(validClass)
        },
        // error form field
        errorPlacement: function(error, element) {
            var placement = $(element).data('error');
            if (placement) {
                //$(placement).append(error)
            } else {
                //error.insertAfter(element);
                for (let index = 0; index < element.length; index++) {
                    const el = element[index];
                    if ($(el).attr('id') === termsCheckBox.attr('id')) {
                        error.insertAfter($(el).closest('.form-group').find('label'));
                    } else {
                        error.insertAfter($(el).closest('.form-group').find('input'));
                    }
                    
                }
            }
        }
    });
    
    // custom phone number validation rule
    $.validator.addMethod("customNumberValidation",
        function (value, element) {
            if ($(element).val() === '') {
                return true;
            } else {
                return /^(((\+4219)|(09))[0-9]{2}\s*[0-9]{3}\s*[0-9]{3})$/.test(value)
            }
        }
    );

    // custom email validation rule
    $.validator.addMethod("customEmailValidation",
        function (value, element) {
            return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value) &&
                /^(?=.{1,64}@.{4,64}$)(?=.{6,100}$).*/.test(value)
        }
    );



    //after formular sent, redirect user to thank you page 
    // if(window.location.search.indexOf('dynamicFormFinish') !== -1) {
    //     window.location.href = '/test_miso/magio-tv-dirctomlp-thankyoupage';
    // }

    // SUBMIT FORM
    sendFormBtn.on("click", function (e) {
        e.preventDefault();
        //check if form is valid
        if ($form.valid()) {
            // SEND FORM ...
            var firstName = $('#resend #firstname').val();
            $('.lrformFirstName').find('input').val(firstName);
            var lastName = $('#resend #lastname').val();
            $('.lrformLastName').find('input').val(lastName);
            var phoneNum = $('#resend #phoneNum').val();
            $('.lrformPhoneNum').find('input').val(phoneNum);
            var useremail = $('#resend #useremail').val();
            $('.lrformEmail').find('input').val(useremail);
            
            setTimeout(function(){
                $('.tlacidlo_odoslat').click();
            }, 500);
        } else {
            e.preventDefault();
        }
    });

    $('a').click(function(e){
        e.preventDefault();
    })
    $(".plan-otherInfo a.btn").click(function(e) {
        e.preventDefault();
        $('html, body').animate({
            scrollTop: parseInt($("#sec-3").offset().top)
        }, 2000);
    });

    $('#info-exclusiveoffer').qtip({
        content: '<p style="text-align:center; font-weight: 200;"><strong>Nakupujte cez web bez obáv!</strong><br> Pri kúpe cez web môžete vrátiť službu alebo tovar bez udania dôvodu až do 14 dní!</p>',
        style: {
            classes: 'qtip-tipsy'
        },
        position: {
            my: 'bottom center',
            at: 'top center'
        }
    });

    $('#info-speed').qtip({
        content: '<p style="text-align:center; font-weight: 200;">Rýchlosť sťahovania a odosielania dát zavísí od technológie pripojenia k internetu na adrese zriadenia služby.</p>',
        style: {
            classes: 'qtip-tipsy'
        },
        position: {
            my: 'bottom center',
            at: 'top center'
        }
    });

    $('#info-connection').qtip({
        content: '<p style="text-align:center; font-weight: 200;">Pripojenie do optickej alebo metalickej siete závisí od toho, ktorá sieť je dostupná na adrese zriadenia služby.</p>',
        style: {
            classes: 'qtip-tipsy'
        },
        position: {
            my: 'bottom center',
            at: 'top center'
        }
    });
});
