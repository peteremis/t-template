import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import {NgxAnalyticsGoogleAnalytics} from "ngx-analytics/ga"; // ngx-analytics

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';


if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.log(err));

// ngx-analytics
export const env = {
  googleAnalytics: {
    domain: 'auto',
    trackingId: 'UA-55238177-1' // replace with your Tracking Id
  }
}
if (env.googleAnalytics) {
  NgxAnalyticsGoogleAnalytics.prototype.createGaSession(env.googleAnalytics); // ngx-analytics
}