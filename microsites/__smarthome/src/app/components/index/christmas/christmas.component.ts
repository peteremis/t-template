import { Component, OnInit } from '@angular/core';
import { config } from './../../../config/config';

@Component({
  selector: 'app-christmas',
  templateUrl: './christmas.component.html',
  styleUrls: ['./christmas.component.scss']
})
export class ChristmasComponent implements OnInit {
  imgsLR: String = config.url.img;

  constructor() { }

  ngOnInit() {
  }

}
