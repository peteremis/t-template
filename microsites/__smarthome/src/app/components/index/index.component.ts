import { Component, OnInit, Input, HostListener, ElementRef, ViewChild } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { Router, ActivatedRoute } from "@angular/router";
import { DataService } from '../../services/data.service';
import { config } from './../../config/config';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss'],
  animations: [
    trigger('toggleContent', [
      state('close', style({height: '0px'})),
      state('open', style ({height: '*'})),
      transition('open => close', animate('400ms ease-in')),
      transition('close => open', animate('400ms ease-in')),
    ])
  ]
})
export class IndexComponent implements OnInit {

  //categories for routing to /shop
  categories: any = null;
  imgsSrc: String = config.url.strapi;
  imgsLR: String = config.url.img;
  pdfLR: String = config.url.pdf;
  expanded: any = null;

  /* TOGGLE ARROW CONTENT - START */
  toggleArrow: any = { // handler for arrows rotation
    arrow1: false,
    arrow2: false,
    arrow3: false,
    arrow4: false,
    arrow5: false,
    arrow6: false,
    arrow7: false
  }

  openClose: any = { // handler for arrow-content show / hide
    content1: 'close',
    content2: 'close',
    content3: 'close',
    content4: 'close',
    content5: 'close',
    content6: 'close',
    content7: 'close'
  };

  hideAndShow(content: any, arrow: any) {
    this.openClose[content] = (this.openClose[content] === 'open') ? 'close' : 'open';
    this.toggleArrow[arrow] = (this.toggleArrow[arrow] === true) ? false : true;
  }
  /* TOGGLE ARROW CONTENT - END */


  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private dataService: DataService,
    private el: ElementRef) {
      //  smooth scroll to hash
      this.route.fragment.subscribe ( f => {
        const element = document.querySelector ( "#" + f )
        if ( element ) {
          element.scrollIntoView({ behavior: 'smooth' })
        }
      });
    }  


  /* routing to categories */
  private changeRoute(param): void {
    this.router.navigate(['shop', param]);
  }

  ngOnInit() {
    if (window.location.href.indexOf('?p_auth=') > 0) {
      this.router.navigate(['thankyou']);
    }    
    if (this.dataService.categories != null) {
      this.categories = this.dataService.categories;  
    } else {
      this.dataService.categoriesFromHttp.subscribe(data => {
        this.categories = data['data']['categories'];
        this.dataService.categories = this.categories;
      },  error => {
        console.error("Error in loading products...");
      });            
    }

  }
}