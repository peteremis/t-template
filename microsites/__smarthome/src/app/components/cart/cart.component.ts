import { Component, OnInit, Input, Output, EventEmitter} from "@angular/core";
import { ShoppingCartService } from "../../services/shoppingCart.service";
import { config } from "./../../config/config";
import { THIS_EXPR } from "@angular/compiler/src/output/output_ast";

@Component({
  selector: "app-cart",
  templateUrl: "./cart.component.html",
  styleUrls: ["./cart.component.scss"]
})
export class CartComponent implements OnInit {
  cartItems: any = [];
  InitQtyInCart = [];
  initPrice = [];
  srcStrapi = config.url.strapi;
  imgsLR: String = config.url.img;
  hasBasicPackage: any;
  basicPackagePrice: number = 0;

  monthlyPrice: number = 0; //final monthly price of all products in cart
  oneTimePayment = 0; //final onetime price of all products in cart

  checked = this._ShoppingCartService.oneTimePayment; //onetimepayment initialy unchecked
  basicPackage = config.basicPackage.urlText; // check if basic package is in cart
  activationFee = this._ShoppingCartService.activationFee;

  constructor(private _ShoppingCartService: ShoppingCartService) {
    this.cartItems = this._ShoppingCartService.cartProducts;
  }

  //calc final monthly price on slider change
  calcMonthlyPrice() {
    this._ShoppingCartService.calcMonthlyPrice()
    this.monthlyPrice = this._ShoppingCartService.fullMonthlyPrice;
  }

  //calc final fullPrice on slider change
  calcFullPrice() {
    this._ShoppingCartService.calcFullPrice();
    this.oneTimePayment = this._ShoppingCartService.oneTimePrice
  }

  //when quantity in cart change, update quantity in _shoppingCart.InitQtyInCart
  updateQty(val, prodIndex) {
     this._ShoppingCartService.updateShoppingCartLength(); //update num of products in cart
    // for (let i = 0; i < this._ShoppingCartService.InitQtyInCart.length; i++) {
    //   if (i == prodIndex) {
    //     this._ShoppingCartService.InitQtyInCart[i] = parseFloat(val)
    //   }
    // }
  }

  removeBasicPackage() {
    this.hasBasicPackage = this._ShoppingCartService.checkBasicPackage_inCart();
  }

  updateLocalStorageQty() {
    this._ShoppingCartService.addToLocaleStorage();
  }

  //on input field change, uncheck slide
  uncheckSlide() {
    this._ShoppingCartService.oneTimePayment = false;
    this.checked = this._ShoppingCartService.oneTimePayment;
  }

  //remove product from cart
  removeItem(index: number) {
    this._ShoppingCartService.remove(index);
    this.uncheckSlide();
    this.monthlyPrice = this._ShoppingCartService.fullMonthlyPrice;
    this.oneTimePayment = this._ShoppingCartService.oneTimePrice;
  }

  //save slider state checked/unchecked when routing back to /cart
  sliderState() {
    //slide unchecked
    if (this.checked === true) {
      this._ShoppingCartService.oneTimePayment = false; //ngOnInit - monthly price slider checked
    } else {
    //slide checked
      this._ShoppingCartService.oneTimePayment = true; //ngOnInit - onetime payment slider checked
    }
  }

  ngOnInit() {
    this.InitQtyInCart = this._ShoppingCartService.InitQtyInCart; //get init quantity for products in cart
    this.checked = this._ShoppingCartService.oneTimePayment; //check for init slider state, checked/unchecked
    this.initPrice = this._ShoppingCartService.initPrices;
    this.monthlyPrice = this._ShoppingCartService.fullMonthlyPrice;
    this.oneTimePayment = this._ShoppingCartService.oneTimePrice;
    this.hasBasicPackage = this._ShoppingCartService.checkBasicPackage_inCart();
    this.basicPackagePrice = this._ShoppingCartService.basicPackagePrice;
  }

  ngAfterViewInit() {
    $("html, body").animate({ scrollTop: 0 }, "slow");
  }
}
