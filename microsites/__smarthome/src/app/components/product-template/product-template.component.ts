import { Component, OnInit } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { ActivatedRoute } from "@angular/router";
import { DataService } from '../../services/data.service';
import { AppService } from "../../services/app.service";
import { ShoppingCartService } from './../../services/shoppingCart.service'
import { config } from "./../../config/config";

import { TabComponent } from './../product-template/tabs/tab/tab.component';
import { TabsComponent } from './../product-template/tabs/tabs.component';

@Component({
    selector: 'app-product-template',
    templateUrl: './product-template.component.html',
    styleUrls: ['./product-template.component.scss'],
    animations: [
        trigger('toggleContent', [
            state('close', style({height: '0px'})),
            state('open', style ({height: '*'})),
            transition('open => close', animate('400ms ease-in')),
            transition('close => open', animate('400ms ease-in')),
        ]) 
    ]
})
export class ProductTemplateComponent implements OnInit {

    product: any;
    mainImgSrc: any = config.url.strapi; //load main image
    imgLR = config.url.img;
    clickedImgurl: any; // change main image url on thumbnail click
    images: any = [];
    changeImage;

    constructor(
        private activatedRoute: ActivatedRoute,
        private dataService: DataService,
        public appService: AppService,
        private _shoppingCartService: ShoppingCartService) {
    }

    //toggle arrow content
    toggleArrow: any = { // handler for arrows rotation
        arrow1: false
    }
    openClose: any = { // handler for arrow-content show / hide
        content1: 'close'
    };
    hideAndShow(content: any, arrow: any) {
        this.openClose[content] = (this.openClose[content] === 'open') ? 'close' : 'open';
        this.toggleArrow[arrow] = (this.toggleArrow[arrow] === true) ? false : true;
    }


    //change img based on clicked thumbnail
    selectImg(img : any) {
        this.images.map((img) => {
          img.active = false;
        })
        img.active = true;
        this.clickedImgurl = this.mainImgSrc + img.url;
    }

    //find if product is in cart and show button "Vlozeny v kosiku"
    checkIfInCart(){
        for (let a = 0; a < this._shoppingCartService._cartProducts.length; a++) {
                if (this.product.name == this._shoppingCartService._cartProducts[a].name) {
                    this.product.isInCart = true;
                }
        }
    }

    addToCart(product) {
        this._shoppingCartService.add(product);
    }

    ngOnInit() {
        /*
        let urlProduct = this.appService.currentProduct;
      
        if (urlProduct === null) {
            urlProduct = this.activatedRoute.snapshot.params.name;
        }
        */
        let urlProduct = this.activatedRoute.snapshot.params.name;

        this.dataService.getProduct(urlProduct).subscribe(data => {
            this.product = data['data']['products'][0];
            this.checkIfInCart(); //onInit check if product is in cart
            this.images = this.product.imgs.reverse();
        },  error => {
            console.error(error);
        });
    }
}
