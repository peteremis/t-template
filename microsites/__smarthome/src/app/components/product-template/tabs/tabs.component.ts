import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'tabs-holder',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.scss']
})
export class TabsComponent implements OnInit {

  tabs = [];
  
  addTab(tab) {
    if (this.tabs.length === 0) {
      tab.active = true;
    }
    this.tabs.push(tab);
  }

  selectTab(tab) {
    this.tabs.map((tab) => {
      tab.active = false;
    })
    tab.active = true;

  }

  constructor() { }

  ngOnInit() {
  }

}
