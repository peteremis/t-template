import { Component, Input, OnInit } from '@angular/core';
import { TabsComponent } from './../tabs.component';

@Component({
  selector: 'tab',
  styleUrls: ['./tab.component.scss'],
  templateUrl: './tab.component.html'
})
export class TabComponent implements OnInit {
  active;
  @Input() tabTitle;
  
  constructor(private tabsComponent: TabsComponent) {}
  
  ngOnInit() {
    //console.log(this)
    this.tabsComponent.addTab(this);
  }
}