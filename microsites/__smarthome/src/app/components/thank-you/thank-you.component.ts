import { Component, OnInit } from '@angular/core';
import { config } from './../../config/config'

@Component({
  selector: 'thankyou',
  templateUrl: './thank-you.component.html',
  styleUrls: ['./thank-you.component.scss']
})
export class ThankYouComponent implements OnInit {

  imgsLR: String = config.url.img;
  
  constructor() { }

  ngOnInit() {
  }

}
