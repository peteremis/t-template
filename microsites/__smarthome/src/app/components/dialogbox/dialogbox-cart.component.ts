import { Component, OnInit } from '@angular/core';
import { ShoppingCartService } from "../../services/shoppingCart.service";
import { ShopComponent } from '../shop/shop.component';
import { config } from './../../config/config';

//dialogbox-shop.html
@Component({
    selector: 'dialogbox-cart',
    templateUrl: 'dialogbox-cart.html',
    styleUrls: ['./dialogbox.component.scss'],
    
  })
  export class dialogboxCart implements OnInit {

    private imgSrcStrapi = config.url.strapi;
    public basicPackage; //data for last product in cart - "Základný balíček"
    additionalProducts = [] //show 3 random products at the bottom of the popup
    imgsLR: String = config.url.img;
  
    constructor(public _shoppingCartService: ShoppingCartService,
                private _shopComponent: ShopComponent){
                
                //push all products to additionalProducts array
                this._shopComponent.products.forEach(element => {
                  this.additionalProducts.push(element);
                })         
    }

    addToCart(product) {
      this._shoppingCartService.add(product);
    }

    ngOnInit() {
      
      //get the last added product in cart, needed for "Základný balíček" to show
      this.basicPackage = this._shopComponent.lastAddedProduct;

      //when popup opens, check if "Základný balíček is in shopping cart
      this._shoppingCartService.checkBasicPackage_inCart();

      //when popup opens and "Základný balíček" is not in shopping cart, add it to cart
      if (this._shoppingCartService.basicPackageInCart != true) {
        //this._shopComponent.getSingleProduct(config.basicPackage.urlText);
      }   

      //when popup opens, randomly sort additionalProducts array;
      this.additionalProducts.sort(function() {
        return .5 - Math.random();
      });
    }
    
  }
  


