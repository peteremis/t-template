import { Component, ViewChild, TemplateRef, OnInit } from "@angular/core";
import { ShoppingCartService } from "../../services/shoppingCart.service";
import { ShopComponent } from "../shop/shop.component";
import { MatDialog } from "@angular/material";
import { config } from './../../config/config';

//dialogbox-shop.html
@Component({
  selector: "dialogbox-shop",
  templateUrl: "dialogbox-shop.html",
  styleUrls: ["./dialogbox.component.scss"]
})
export class dialogboxShop implements OnInit {
  imgsLR: String = config.url.img;
  disableShopPopup() {
    this._shoppingCartService.basicPackageInCart = true;
  }

  constructor(
    private _shoppingCartService: ShoppingCartService,
    private _shopComponent: ShopComponent,
    private dialog: MatDialog
  ) {}

  //make visible ng-templates for "ShopComponent"
  @ViewChild("dialogCartPopup") dialogCartPopup: TemplateRef<any>;
  //open popups
  openCartPopup() {
    // if "Základný balíček" is in cart, open cart popup
    const dialogRef2 = this.dialog.open(this.dialogCartPopup, {
      height: "90%",
      maxWidth: "800px",
      width: "90%"
    });
  }

  ngOnInit() {
   
  }
}
