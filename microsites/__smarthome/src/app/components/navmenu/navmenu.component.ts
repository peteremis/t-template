import { Component, OnInit } from '@angular/core';
import { ShoppingCartService } from './../../services/shoppingCart.service';
import { config } from './../../config/config';

@Component({
  selector: 'app-navmenu',
//   template: `
//   <div id="nav-sticky" class="navsticky">
//   <ul>
//       <li><a class="scroll-to" href="" data-id="sec-1" routerLink="/">O PRODUKTE</a></li>
//       <li><a class="scroll-to" href="" data-id="sec-1" routerLink="/zakladny-balik">ZÁKLADNÝ BALÍK</a></li>
//       <li><a class="scroll-to" href="" data-id="sec-1" routerLink="/instalacia">INŠTALÁCIA</a></li>
//       <li><a class="scroll-to" href="" data-id="sec-1" routerLink="/shop">ESHOP</a></li>
//       <li><a class="scroll-to" href="" data-id="sec-1" routerLink="/cart"><fa [name]="'shopping-cart'" [size]="2"></fa><p class="numProdInCart">0</p></a></li>
//   </ul>
// </div>`,
templateUrl: './navmenu.component.html',
  styleUrls: ['./navmenu.component.scss']
})
export class NavmenuComponent implements OnInit {


  constructor(public _ShoppingCartService: ShoppingCartService) {
  }

  ngOnInit() {
  }

}
