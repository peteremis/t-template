import { Component, OnInit, ViewChild } from '@angular/core';
import { ShoppingCartService } from './../../services/shoppingCart.service';
import { FormBuilder,FormControl, FormGroup, Validators } from '@angular/forms';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { MatStepper } from '@angular/material';
import { Router } from "@angular/router";
import { config } from "../../config/config";

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss'],
  animations: [
    trigger('toggleContent', [
      state('close', style({height: '0px'})),
      state('open', style ({height: '*'})),
      transition('open => close', animate('400ms ease-in')),
      transition('close => open', animate('400ms ease-in')),
    ])
  ]
})
export class CheckoutComponent implements OnInit {

  constructor(private _ShoppingCartService: ShoppingCartService, private _formBuilder: FormBuilder, private router: Router) {
  }

  monthlyPrice: number = 0; //final monthly price of all products in cart
  oneTimePayment = 0; //final onetime price of all products in cart
  checked = this._ShoppingCartService.oneTimePayment; //onetimepayment initialy unchecked, set to false
  activationFee;
  productsInCart;
  qtyInCart; //init Qty of all products in cart
  hideCart: boolean = false; //hide cart on final step
  finalBtn: boolean = false;
  disabled = true;
  business: boolean;
  basicPackage = config.basicPackage.id;
  pdfLR: String = config.url.pdf;
  imgsLR: String = config.url.img;
  basicPackagePrice: number = 0;
  
  zeroStep: FormGroup;
  firstStep: FormGroup;
  secondStep: FormGroup;
  thirdStep: FormGroup;
  
  //form properties 
  streetNumber_value: any;
  streetNumberShip_value: any;
  cityValue: any;
  cityValueShip: any;
  PSCvalue: any;
  PSCvalueShip: any;
  firstName: any;
  lastName: any;
  birthNumberValue: any;
  idValue: any;
  phoneNumber_value: any;
  mail_value: any;
  desc_value: any;
  ico_value: any;
  dic_value: any;
  icdph_value: any;

  /* TOGGLE ARROW CONTENT - START */
  toggleArrow: any = { // handler for arrows rotation
    arrow1: false
  }

  openClose: any = { // handler for arrow-content show / hide
    content1: 'close'
  };

  hideAndShow(content: any, arrow: any) {
    this.openClose[content] = (this.openClose[content] === 'open') ? 'close' : 'open';
    this.toggleArrow[arrow] = (this.toggleArrow[arrow] === true) ? false : true;
  }
  /* TOGGLE ARROW CONTENT - END */

  allowOnlyNumbers(event: any) {
    const pattern = /[0-9\+\-\ ]/;
    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

  goToShop(): void {
    this.router.navigate(['cart']);
  }

  //set active step in stepper
  @ViewChild('stepper') stepper: MatStepper;
  ngAfterViewInit() {
    setTimeout(()=>{
      this.stepper.selectedIndex = 1; 
    },0);
  }
  
  //reset stepper
  resetStepper(stepper: MatStepper){
    this.setDynamicForm();
    
    //stepper.selectedIndex = 1;
    
    this._ShoppingCartService.clear();
    this._ShoppingCartService.updateShoppingCartLength();

  }

  //stepper click "NEXT"
  hideCart_stepNext(stepper: MatStepper){
    if (stepper.selectedIndex = 3) {
      this.hideCart = true;
    }
  }

  setDynamicForm() {
    let msg1 = '';
    let msg2 = ''
    if (this._ShoppingCartService.basicPackageInCart) {
      console.log(this._ShoppingCartService.basicPackageInCart);
      msg1 = 'Objednávka SmartHome (základný balík obsahuje: SmartHome centrála 1 ks, dverový senzor 1 ks).';
      msg2 = 'Produkty:';
    } else {
      msg1 = 'Objednávka SmartHome';
      msg2 = 'Produkty:';
    }
    let formText = '<table border=\'0\'>'
      + '<tr>'
      +   '<td colspan=\'2\'>' + msg1 + '</td>'
      + '</tr>'
      + '<tr>'
      +   '<td colspan=\'2\'>' + msg2 + '</td>'
      + '</tr>';
    for (let i = 0; i < this.productsInCart.length; i++) {
      formText += '<tr class=\'product\'>'
        +   '<td>' + this.productsInCart[i]['name'] + '</td>'
        +   '<td>' + this.qtyInCart[i] + 'ks</td>'
        + '</tr>';
    }
    let activFee = this.activationFee;
    let priceMonthly = this.monthlyPrice;
    let priceOneTime = this.activationFee + this.oneTimePayment;
    if (this._ShoppingCartService.basicPackageInCart) { //basic package in cart
      if (this.checked) { // jednorazovo
        priceMonthly = this.basicPackagePrice;
        priceOneTime = this.activationFee + this.oneTimePayment;
      } else { // mesacne
        priceOneTime = this.activationFee;
        priceMonthly =  this.monthlyPrice; 
      }

    } else { //basic package not in cart
      activFee = 0;
      if (this.checked) { //jednorazovo
        priceMonthly = 0;
        priceOneTime = this.oneTimePayment;
      } else { //mesacne
        priceOneTime = 0;
        priceMonthly = this.monthlyPrice;
      }
    }
    formText += '<tr><td colspan=\'2\'>&nbsp;</td></tr>'
      + '<tr class=\'monthly\'>'
      +   '<td>Mesačne:</td>'
      +   '<td>' + priceMonthly.toFixed(2) + '</td>'
      + '</tr>'
      + '<tr class=\'activationFee\'>'
      +   '<td>Jednorazovo:</td>'
      +   '<td>' + priceOneTime.toFixed(2) + '</td>'
      + '</tr>'
      + '<tr class=\'activationFee\'>'
      +   '<td>Aktivačný poplatok:</td>'
      +   '<td>' + activFee.toFixed(2) + '</td>'
      + '</tr>'
      + '<tr class=\'paymentMethod\'>'
      +   '<td>Spôsob platby:</td>'
      +   '<td>' + (this.checked ? 'jednorazovo' : 'mesačne') + '</td>'
      + '</tr>'                  
      + '<tr><td colspan=\'2\'>&nbsp;</td></tr>'
      + '<tr class=\'name\'>'
      +   '<td>Meno a priezvisko:</td>'
      +   '<td>' + this.firstName + ' ' + this.lastName + '</td>'
      + '</tr>'
      + '<tr class=\'streetNo\'>'
      +   '<td>Ulica a číslo:</td>'
      +   '<td>' + (this.streetNumber_value == undefined ? this.streetNumberShip_value : this.streetNumber_value) + '</td>'
      + '</tr>'
      + '<tr class=\'city\'>'
      +   '<td>Mesto/obec:</td>'
      +   '<td>' + (this.cityValue == undefined ? this.cityValueShip :this.cityValue ) + '</td>'
      + '</tr>'
      + '<tr class=\'psc\'>'
      +   '<td>PSČ:</td>'
      +   '<td>' + (this.PSCvalue == undefined ? this.PSCvalueShip : this.PSCvalue) + '</td>'
      + '</tr>'                  
      + '<tr class=\'phone\'>'
      +   '<td>Telefónne číslo:</td>'
      +   '<td>' + this.phoneNumber_value + '</td>'
      + '</tr>'
      + '<tr class=\'email\'>'
      +   '<td>E-mail:</td>'
      +   '<td>' + this.mail_value + '</td>'
      + '</tr>'
      + '<tr><td colspan=\'2\'>&nbsp;</td></tr>'      
      + '<tr class=\'idNum\'>'
      +   '<td>Čislo OP:</td>'
      +   '<td>' + (this.idValue == undefined ? '' : this.idValue) + '</td>'
      + '</tr>'
      + '<tr class=\'birthdayNum\'>'
      +   '<td>Rodné číslo:</td>'
      +   '<td>' + (this.birthNumberValue == undefined ? '' : this.birthNumberValue) + '</td>'
      + '</tr>';
    if (this.business) {
      formText += '<tr class=\'ico\'>'
        +   '<td>IČO:</td>'
        +   '<td>' + this.ico_value + '</td>'
        + '</tr>'
        + '<tr class=\'dic\'>'
        +   '<td>DIČ:</td>'
        +   '<td>' + this.dic_value + '</td>'
        + '</tr>'
        + '<tr class=\'icodph\'>'
        +   '<td>IČDPH:</td>'
        +   '<td>' + this.icdph_value + '</td>'
        + '</tr>';              
    }
    formText += '<tr class=\'textInfo\'>'
      +   '<td>Poznámka:</td>'
      +   '<td>' + (this.desc_value == undefined ? '' : this.desc_value) + '</td>'
      + '</tr>'
      + '<tr class=\'shippment\'><td colspan=\'2\'>Adresa pre doručenie:</td></tr>'
      + '<tr class=\'shippment_streetNo\'>'
      +   '<td>Ulica a číslo:</td>'
      +   '<td>' + this.streetNumberShip_value + '</td>'
      + '</tr>'
      + '<tr class=\'shippment_city\'>'
      +   '<td>Mesto/obec:</td>'
      +   '<td>' + this.cityValueShip + '</td>'
      + '</tr>'
      + '<tr class=\'shippment_psc\'>'
      +   '<td>PSČ:</td>'
      +   '<td>' + this.PSCvalueShip + '</td>'
      + '</tr>'
      + '</table>';


    //$('#formContent').html(formText);
    //this.formChange('Smarthome', 'form.dynamicFormMainForm .formSmarthome-subject');
    this.formChange(this.mail_value, 'form.dynamicFormMainForm .formSmarthome-email');
    this.formChange(formText, 'form.dynamicFormMainForm .formSmarthome-text');
    //$('.dynamicFormPages .finishFormButton').click();
    
    let query = 'mutation{createSmarthomeorders(input:{data:{text:"' + formText + '"}}){smarthomeorder{_id}}}';
    $.ajax({
      url: config.url.strapi + '/graphql',
      type: 'post',
      data: { query:  query},
      complete: function()
      {
        if ($('div.desktop-agentBox').length) {
          localStorage.setItem("lsbData", formText);
          window.location.href="https://www.telekom.sk/smarthome/formular";
        } else {
          localStorage.removeItem("cartProd");
          $('.dynamicFormPages .finishFormButton').click();
        }
      }    
    });
  }
  
  formChange(value, inputName) {
    $(inputName)
        .find('input').val(value).blur().show().end()
        .find('label').show().end()
        .closest('li').show().end();
  }  

  //stepper click "PREVIOUS"
  showCart_stepPrev(stepper: MatStepper){
      stepper.previous();
      this.hideCart = false;
  }

  //stepper HEADER click
  stepClick(stepper: MatStepper) {
    if (stepper.selectedIndex !== 3) {
      this.hideCart = false;
    } else {
      this.hideCart = true;
    }
  }
  
  //on BUSINESS checkbox click, create business validator
  createBusinessValidators() {
    //if checked, add business validators
    if (this.business === false || 'undefined') {
      this.secondStep.get('ico').setValidators([Validators.required, Validators.minLength(3)]);
      this.secondStep.get('dic').setValidators([Validators.required, Validators.minLength(3)]);
      this.secondStep.get('icdph').setValidators([Validators.required, Validators.minLength(3)]);
    } 
    //if unchecked, clear business validators
    if (this.business === true) {
      this.secondStep.controls['ico'].clearValidators();
      this.secondStep.controls['ico'].updateValueAndValidity();
      this.secondStep.controls['dic'].clearValidators();
      this.secondStep.controls['dic'].updateValueAndValidity();
      this.secondStep.controls['icdph'].clearValidators();
      this.secondStep.controls['icdph'].updateValueAndValidity();
    }
  }

  ngOnInit() {
    this.monthlyPrice = this._ShoppingCartService.fullMonthlyPrice;
    this.oneTimePayment = this._ShoppingCartService.oneTimePrice
    this.checked = this._ShoppingCartService.oneTimePayment; //check for init slider state, checked/unchecked
    this.productsInCart = this._ShoppingCartService._cartProducts;
    this.qtyInCart = this._ShoppingCartService.InitQtyInCart;
    this._ShoppingCartService.checkBasicPackage_inCart(); // je potrebne pre zistenie ceny zakl. balika
    this.activationFee = this._ShoppingCartService.activationFee;
    this.basicPackagePrice = this._ShoppingCartService.basicPackagePrice;
    
    //validations for stepper
    this.firstStep = new FormGroup({
      'streetNumberShip': new FormControl('', [
        Validators.required,
        Validators.minLength(4)
      ]),
      'cityShip': new FormControl('', [
        Validators.required,
        Validators.minLength(2)
      ]),
      'PSCShip': new FormControl('', [
        Validators.required,
        Validators.minLength(5)
      ]),
      'text': new FormControl('', [
      ])
    });

    this.secondStep = new FormGroup({
      'firstName': new FormControl('', [
        Validators.required
      ]),
      'lastName': new FormControl('', [
        Validators.required
      ]),
      'streetNumber': new FormControl('', [
        //Validators.required,
      ]),
      'city': new FormControl('', [
        //Validators.required
      ]),
      'PSC': new FormControl('', [
        //Validators.required
      ]),
      'phoneNumber': new FormControl('', [
        Validators.required
      ]),
      'e-mail': new FormControl('', [
        Validators.required,
        Validators.email
      ]),
      'ico': new FormControl('', [
        //ico validator
      ]),
      'dic': new FormControl('', [
        //dic validator
      ]),
      'icdph': new FormControl('', [
        //icdph validator
      ]),
      'birthNumber': new FormControl('', [
        //birthnumber validator
      ]),
      'id': new FormControl('', [
        //id validator
      ])
    });
    
    this.thirdStep = new FormGroup({
      'thirdStep_input': new FormControl('', [
        Validators.required
      ])
    });

  }

}