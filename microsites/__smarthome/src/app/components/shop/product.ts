export interface Product   {
  _id: string,
  name: string,
  price: string,
  fullPrice: string,
  desc: string,
  shortDesc: string,
  imgs: [{
      imgName: string,
      url: string
  }],
  thumbImgs: [{
    name: string,
    url: string
  }]
  }