import {
  Component,
  OnInit,
  Output,
  Input,
  EventEmitter,
  ViewChild,
  TemplateRef,
  AfterViewInit,
  HostListener,
  Inject,
  ElementRef
} from "@angular/core";
import { ShoppingCartService } from "../../services/shoppingCart.service";
import { AppService } from "../../services/app.service";
import { ActivatedRoute } from "@angular/router";
import { DataService } from "../../services/data.service";
import { config } from "../../config/config";
import { Router } from "@angular/router";
import { OrderPipe } from "ngx-order-pipe";
import {
  trigger,
  state,
  style,
  transition,
  animate,
  keyframes,
  query,
  stagger
} from "@angular/animations";
import { MatDialog } from "@angular/material";
import { DOCUMENT } from "@angular/platform-browser";

@Component({
  selector: "app-shop",
  templateUrl: "./shop.component.html",
  styleUrls: ["./shop.component.scss"],
  animations: [
    trigger("slideInOut", [
      state(
        "opened",
        style({
          //transform: 'translate3d(0, 0, 0)'
          display: "block",
          height: "*",
          top: 0
        })
      ),
      state(
        "closed",
        style({
          //transform: 'translate3d(100%, 0, 0)'
          height: 0 + "px",
          display: "none",
          top: -324 + "px"
        })
      ),
      transition("opened => closed", animate("400ms ease-in-out")),
      transition("closed => opened", animate("400ms ease-in-out"))
    ]),
    trigger("listProducts", [
      transition("* => *", [
        query(":enter", style({ opacity: 0 }), { optional: true }),

        query(
          ":enter",
          stagger("300ms", [
            animate(
              "1s ease-in",
              keyframes([
                style({ opacity: 0, transform: "translateY(-55%)", offset: 0 }),
                style({
                  opacity: 0.5,
                  transform: "translateY(15px)",
                  offset: 0.3
                }),
                style({ opacity: 1, transform: "translateY(0)", offset: 1.0 })
              ])
            )
          ]),
          { optional: true }
        )
      ])
    ])
  ]
})
export class ShopComponent implements OnInit {
  imgsLR: String = config.url.img;

  menuItems: any[] = null; //menu items
  menuOpened: string = "opened";
  isInfiniteScroll: boolean = true;

  products: any;
  productsAll: any;
  numberOfProducts: number;
  productsOnPage: number = 6;
  productStart: number = 0;

  imgsSrc: String = config.url.strapi;

  selected = ""; // selectbox
  prodctsSorting: string[] = ["weight", "name"]; // sort products by lowest price
  reversePrice = false; //lowest price sorting not reversed

  balicekAddedToCart = null; //handler for popup windows;
  lastAddedProduct: any = []; // save here only last added product to cart

  currentCateg = null; //current active category

  constructor(
    private dataService: DataService,
    private appService: AppService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private _shoppingCartService: ShoppingCartService,
    private dialog: MatDialog,
    @Inject(DOCUMENT) private document: Document,
    private elRef: ElementRef
  ) {}

  //make visible popup ng-templates for "ShopComponent"
  @ViewChild("dialogShopPopup") dialogShopPopup: TemplateRef<any>;
  @ViewChild("dialogCartPopup") dialogCartPopup: TemplateRef<any>;
  //open popups
  openDialog() {
    //check if "Základný balíček" is in cart
    this._shoppingCartService.checkBasicPackage_inCart();

    // // if "Základný balíček" is in cart, open cart popup
    // if (this._shoppingCartService.basicPackageInCart == true) {
    //     const dialogRef2 = this.dialog.open(this.dialogCartPopup, {
    //         height: "90%",
    //         maxWidth: "800px",
    //         width: "90%"
    //     });
    // //else open shop popup
    // }
    // else {
    //     const dialogRef = this.dialog.open(this.dialogShopPopup, {
    //         height: "90%",
    //         maxHeight: "686px",
    //         maxWidth: "800px",
    //         width: "90%"
    //     });
    // }

    // open shop popup only one time
    if (this._shoppingCartService.popupClickCount === 1) {
      const dialogRef2 = this.dialog.open(this.dialogCartPopup, {
        height: "90%",
        maxWidth: "800px",
        width: "90%"
      });
      this._shoppingCartService.popupClickCount = 1;
    } else if (this._shoppingCartService.popupClickCount === 0) {
      const dialogRef = this.dialog.open(this.dialogShopPopup, {
        height: "90%",
        maxHeight: "686px",
        maxWidth: "800px",
        width: "90%"
      });
      this._shoppingCartService.popupClickCount = 1;
    }
  }

  toggleMenu() {
    this.menuOpened = this.menuOpened === "closed" ? "opened" : "closed";
  }

  //  ADD PRODUCT TO CART (shoppingCart.service.ts)
  addtoCart(productToCart: any) {
    //console.log(productToCart);
    this._shoppingCartService.add(productToCart);
    this.lastAddedProduct = []; //save last added product to array for dialogboxCart popup
    this.lastAddedProduct.push(productToCart);
  }

  basicPackage_layoutFix(product) {
    return product == config.basicPackage.name;
  }

  redirectToProduct(urlText: String) {
    this.appService.currentProduct = urlText;
    if (urlText === config.basicPackage.urlText) {
      this.router.navigate(["/" + config.basicPackage.urlText]);
    } else {
      this.router.navigate(["/product", urlText]);
    }
  }

  //find if product is in cart and show button "Vlozeny v kosiku"
  checkIfInCart() {
    for (let a = 0; a < this._shoppingCartService._cartProducts.length; a++) {
      this.products.forEach(prod => {
        if (prod.name == this._shoppingCartService._cartProducts[a].name) {
          prod.isInCart = true;
        }
      });
    }
  }

  showCategoryInUrl(categoryName) {
    this.router.navigate(["shop", categoryName]);
    //this.router.navigate(['/shop'], { queryParams: { category: categoryName } });
  }

  //get single product by "product.urlText"
  getSingleProduct(product_url: String = null) {
    this.dataService.getProduct(product_url).subscribe(
      product => {
        this._shoppingCartService.add(product["data"]["products"][0]); //add single product to cart
        //this.checkIfInCart(); //change button of added product
        this.lastAddedProduct.push(product["data"]["products"][0]); //save last added product to array
      },
      error => {
        console.error("Product was not loaded properly. Try again.");
      }
    );
  }

  public filterByhighestPrice(value: boolean) {
    let sortBy = "price";
    let reversePrice: number = 1;

    if (value === null) {
      sortBy = "weight";
      reversePrice = -1;
    } else if (!value) {
      reversePrice = -1;
    }

    this.productsAll.sort(function(a, b) {
      if (parseFloat(a[sortBy]) < parseFloat(b[sortBy])) return reversePrice;
      if (parseFloat(a[sortBy]) > parseFloat(b[sortBy]))
        return reversePrice * -1;
      return 0;
    });

    //console.log(this.productsAll);
    this.productStart = 0;
    this.products = [];
    this.addProductsToArray();
    this.isInfiniteScroll =
      this.numberOfProducts > this.products.length ? false : true;
  }

  private loadProducts() {
    //load all products if category parameter /shop/:category not set
    if (
      this.activatedRoute.snapshot.params.category == null &&
      this.activatedRoute.snapshot.params.category !== "undefined"
    ) {
      this.getProducts();
      //else get products by /:category url parameter
    } else {
      for (let i = 0; i < this.menuItems.length; i++) {
        const category = this.menuItems[i];
        if (category.urlText == this.activatedRoute.snapshot.params.category) {
          this.getProducts(category._id, category.urlText);
        }
      }
    }
  }

  //set class ".highlighted" to current active category
  currentCategory(category: any) {
    if (this.activatedRoute.snapshot.params.category === category) {
      this.currentCateg = category;
    } else {
      this.currentCateg = undefined;
    }
    this.currentCateg = category;
    //console.log(this.currentCateg)
  }

  getProducts(categoryID: String = null, categoryName: any = null) {
    this.products = [];
    this.productStart = 0;
    this.selected = "";
    this.dataService.getProducts(categoryID).subscribe(
      data => {
        this.productsAll = data["data"]["products"]; //get all products in category
        this.productsAll.sort(function(a, b) {
          if (a["weight"] < b["weight"]) return -1;
          if (a["weight"] > b["weight"]) return 1;
          return 0;
        });

        this.numberOfProducts = this.productsAll.length; //onInit get number of loaded products
        this.addProductsToArray();
        //this.checkIfInCart(); //onInit check if product is allready in cart when routing from different page
        this.isInfiniteScroll =
          this.numberOfProducts > this.products.length ? false : true;
      },
      error => {
        console.error("Error in loading products...");
      }
    );

    if (categoryName != null) {
      this.showCategoryInUrl(categoryName);
    } else if (this.activatedRoute.snapshot.params.category !== undefined) {
      this.showCategoryInUrl("");
    }
  }

  private addProductsToArray() {
    for (
      let i = this.productStart;
      i < this.productsOnPage + this.productStart;
      i++
    ) {
      if (this.productsAll[i] !== undefined) {
        this.products.push(this.productsAll[i]);
        for (
          let a = 0;
          a < this._shoppingCartService._cartProducts.length;
          a++
        ) {
          if (
            this.productsAll[i].name ==
            this._shoppingCartService._cartProducts[a].name
          ) {
            this.products[this.products.length - 1].isInCart = true;
          }
        }
      }
    }
    //this.checkIfInCart();
    this.productStart += this.productsOnPage;
  }

  /* floating filter in /shop - START */
  initTopPosition: number; //init position of element from top
  public elementFixed: boolean = false;
  public elementBottom: boolean = false;
  @ViewChild("filter") filter: ElementRef;
  @ViewChild("filterCol") filterCol: ElementRef;
  @HostListener("window:scroll", [])
  onWindowScroll() {
    let rect = this.filter.nativeElement.getBoundingClientRect().top; //px left to reach element viewport
    let filterHeight = this.filter.nativeElement.offsetHeight;
    let scrolledFromTop = this.document.documentElement.scrollTop; //px scrolled from top
    //console.log("scrollTop" , scrolledFromTop)
    //console.log("filter" , rect)
    //console.log("initTop", this.initTopPosition)
    //console.log( window.innerWidth)
    let filterColumnEnd =
      this.filterCol.nativeElement.getBoundingClientRect().top +
      this.filterCol.nativeElement.offsetHeight -
      filterHeight; //px left to reach element viewport

    if (rect < 0) rect = 0;

    if (filterColumnEnd <= 10) {
      this.elementFixed = false;
      this.elementBottom = true;
    } else if (rect == 0 && scrolledFromTop > this.initTopPosition) {
      this.elementFixed = true;
      this.elementBottom = false;
    } else {
      this.elementFixed = false;
      this.elementBottom = false;
    }
  }

  screenWidth: number;
  @HostListener("window:resize", ["$event"])
  onResize(event?) {
    this.screenWidth = window.innerWidth;
    {
      this.initTopPosition =
        parseFloat(this.filter.nativeElement.getBoundingClientRect().top) +
        this.document.documentElement.scrollTop;
      //console.log('resize',this.initTopPosition)
    }
  }

  /* floating filter in /shop - END */

  ngOnInit() {
    //get initial position of ".filter" element;
    this.products = [];
    if (window.innerWidth < 768) {
      this.menuOpened = "closed";
    }

    if (this.dataService.categories != null) {
      this.menuItems = this.dataService.categories;
      this.loadProducts();
    } else {
      this.dataService.categoriesFromHttp.subscribe(
        data => {
          this.menuItems = data["data"]["categories"];
          this.dataService.categories = this.menuItems;
          this.loadProducts();
        },
        error => {
          console.error("Error in loading categories...");
        }
      );
    }

    this.currentCategory(this.activatedRoute.snapshot.params.category); //set active menu category
  }

  ngAfterViewInit() {
    this.initTopPosition =
      parseFloat(this.filter.nativeElement.getBoundingClientRect().top) +
      this.document.documentElement.scrollTop;
  }

  onScroll() {
    if (this.numberOfProducts > this.products.length) {
      this.addProductsToArray();
      this.isInfiniteScroll =
        this.numberOfProducts > this.products.length ? false : true;
    }
  }
}
