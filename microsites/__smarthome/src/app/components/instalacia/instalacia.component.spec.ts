import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstalaciaComponent } from './instalacia.component';

describe('InstalaciaComponent', () => {
  let component: InstalaciaComponent;
  let fixture: ComponentFixture<InstalaciaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstalaciaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstalaciaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
