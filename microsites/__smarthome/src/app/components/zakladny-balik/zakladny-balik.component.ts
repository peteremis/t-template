import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute} from "@angular/router";
import { trigger, state, style, transition, animate } from '@angular/animations';
import { config } from './../../config/config';

@Component({
  selector: 'app-zakladny-balik',
  templateUrl: './zakladny-balik.component.html',
  styleUrls: ['./zakladny-balik.component.scss'],
  animations: [
    trigger('toggleContent', [
      state('close', style({height: '0px'})),
      state('open', style ({height: '*'})),
      transition('open => close', animate('400ms ease-in')),
      transition('close => open', animate('400ms ease-in')),
    ]) 
  ]
})
export class ZakladnyBalikComponent implements OnInit {

  imgsLR: String = config.url.img;

    /* TOGGLE ARROW CONTENT - START */
    toggleArrow: any = { // handler for arrows rotation
      arrow1: false
    }
  
    openClose: any = { // handler for arrow-content show / hide
      content1: 'close'
    };
  
    hideAndShow(content: any, arrow: any) {
      this.openClose[content] = (this.openClose[content] === 'open') ? 'close' : 'open';
      this.toggleArrow[arrow] = (this.toggleArrow[arrow] === true) ? false : true;
    }
    /* TOGGLE ARROW CONTENT - END */



  constructor( private route: ActivatedRoute,
               private router: Router ) {

                //  smooth scroll to hash
                this.route.fragment.subscribe ( f => {
                  const element = document.querySelector ( "#" + f )
                  if ( element ) {
                    element.scrollIntoView({ behavior: 'smooth' })
                  }
                });
               }

  ngOnInit() {
  }

}
