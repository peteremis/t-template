import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import {FormsModule, ReactiveFormsModule } from '@angular/forms';



// all appComponents
import { AppComponent } from './app.component';
import { IndexComponent } from './components/index/index.component';
import { InstalaciaComponent } from './components/instalacia/instalacia.component';
import { ZakladnyBalikComponent } from './components/zakladny-balik/zakladny-balik.component';
import { ShopComponent } from './components/shop/shop.component';
import { NavmenuComponent } from './components/navmenu/navmenu.component';
import { ProductTemplateComponent } from './components/product-template/product-template.component';
import { CartComponent } from './components/cart/cart.component';
import { dialogboxShop } from './components/dialogbox/dialogbox-shop.component';
import { dialogboxCart } from './components/dialogbox/dialogbox-cart.component';
import { CheckoutComponent } from './components/checkout/checkout.component';
import { ThankYouComponent } from './components/thank-you/thank-you.component';
import { ChristmasComponent } from './components/index/christmas/christmas.component';

//services
import { DataService } from './services/data.service';
import { ShoppingCartService } from './services/shoppingCart.service';
import { AppService } from './services/app.service';

//pipes
import { ReplaceLineBreaksPipe } from './services/replaceLineBreaks.pipe';
import { ReplacePipe } from './services/replaceDotToComma.pipe';

//plugins
import { MatTooltipModule, MatSelectModule, MatDialogModule  } from '@angular/material';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import 'hammerjs';
import {NgxPaginationModule} from 'ngx-pagination';
import { OrderModule } from 'ngx-order-pipe';
import { TabsComponent } from './components/product-template/tabs/tabs.component';
import { TabComponent } from './components/product-template/tabs/tab/tab.component';
import { Angular2FontawesomeModule } from 'angular2-fontawesome/angular2-fontawesome';
import { MarkdownModule } from 'angular2-markdown';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { MatStepperModule } from '@angular/material/stepper';
import { MatInputModule } from '@angular/material';
import { MatIconModule } from "@angular/material/icon";
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatMenuModule } from '@angular/material/menu';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import * as $ from 'jquery';

//ngx-analytics
import { NgxAnalyticsModule } from 'ngx-analytics';
import { NgxAnalyticsGoogleAnalytics } from 'ngx-analytics/ga';

// router settings
const appRoutes: Routes = [
  { path: '', component: IndexComponent},
  { path: 'instalacia', component: InstalaciaComponent},
  { path: 'zakladny-balik', component: ZakladnyBalikComponent},
  { path: '**', redirectTo: '/', pathMatch: 'full'},
  // { path: 'shop', component: ShopComponent},
  // { path: 'shop/:category', component: ShopComponent},
  // { path: 'product/:name', component: ProductTemplateComponent},
  // { path: 'cart', component: CartComponent},
  // { path: 'checkout', component: CheckoutComponent},
  // { path: 'thankyou', component: ThankYouComponent},
]

@NgModule({
  declarations: [
    AppComponent,
    IndexComponent,
    InstalaciaComponent,
    ZakladnyBalikComponent,
    ShopComponent,
    NavmenuComponent,
    ProductTemplateComponent,
    CartComponent,
    TabsComponent,
    TabComponent,
    dialogboxShop,
    dialogboxCart,
    ReplaceLineBreaksPipe,
    ReplacePipe,
    CheckoutComponent,
    ThankYouComponent,
    ChristmasComponent
  ],
  imports: [
    BrowserModule,
    MarkdownModule.forRoot(),
    HttpClientModule,
    FormsModule,
    InfiniteScrollModule,
    RouterModule.forRoot(appRoutes),
    NgxAnalyticsModule.forRoot([NgxAnalyticsGoogleAnalytics]), // ngx-analytics
    BrowserAnimationsModule,
    NgxPaginationModule,
    OrderModule,
    Angular2FontawesomeModule,
    ReactiveFormsModule,

    MatTooltipModule,
    MatSelectModule,
    MatDialogModule,
    MatSlideToggleModule,
    MatStepperModule,
    MatInputModule,
    MatIconModule,
    MatCheckboxModule,
    MatMenuModule
  ],
  exports: [ RouterModule ],
  entryComponents: [], //for dialogbox
  providers: [DataService, AppService, 
    ShoppingCartService, 
    ShopComponent, 
    MatSlideToggleModule, IndexComponent, CartComponent, {provide: LocationStrategy, useClass: HashLocationStrategy}],
  //providers: [DataService, AppService, ShoppingCartService, ShopComponent, MatSlideToggleModule, IndexComponent, CartComponent],
  bootstrap: [AppComponent]
})

export class AppModule {
}
