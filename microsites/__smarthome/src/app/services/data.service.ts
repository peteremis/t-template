import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { config } from "../config/config";
import { Product } from "../components/shop/product";

@Injectable()
export class DataService {
  private products: any = null;
  private _categories: any = null;

  constructor(private http: HttpClient) {}

  /* get products */
  public getProducts(categoryId: String = null) {
    this.products = this.getProductsFromHttp(categoryId);
    return this.products;
  }

  private getProductsFromHttp(categoryId: String) {
    let url = "";

    url = config.url.strapi + "/graphql?query=" + "{products(";

    if (categoryId !== null) {
      url += 'where:{category:"' + categoryId + '"},';
    }

    url +=
      'sort:"name"){_id,urlText,name,price,fullPrice,fullDesc,shortDesc,cartDesc,imgs{url,name},weight, priceDeposit}' +
      "}";

    return this.http.get(url);
  }

  public getProduct(product_url: String) {
    const url =
      config.url.strapi +
      "/graphql?query=" +
      "{products" +
      '(where:{urlText:"' +
      product_url +
      '"})' +
      "{_id,urlText,name,price,fullPrice,fullDesc,shortDesc,longDesc,cartDesc,descImg{url,name},imgs{url,name},parameters,installation,purchaseCondition, priceDeposit}" +
      "}";

    return this.http.get(url);
  }

  public get categories() {
    return this._categories;
  }

  public set categories(value) {
    this._categories = value;
  }

  public get categoriesFromHttp() {
    const url =
      config.url.strapi +
      "/graphql?query=" +
      '{categories(sort:"weight:asc")' +
      "{_id,name,urlText,img{url}}" +
      "}";

    return this.http.get(url);
  }
}
