import { Injectable, Output, EventEmitter } from "@angular/core";
import { config } from './../config/config';


@Injectable()
export class ShoppingCartService {

  public _cartProducts: any = [];
  private _shoppingCartLength: number = 0;

  public fullMonthlyPrice: number = 0; //monthly price of all products in cart
  public initPrices = []
  public InitQtyInCart = [];
  public _oneTimePayment = null;

  private fullPrices = [];
  public oneTimePrice = 0; //onetime price of all products in cart

  public basicPackageInCart: boolean = false; //check if basic package is in cart
  public basicPackagePrice: number = 0;

  public popupClickCount = 0;

  public activationFee: number = 0;


  constructor() {
    //if items are loaded from localStorage
    let localStorageProducts = localStorage.getItem('cartProd');
    if (localStorageProducts !== null) {
      this.cartProducts = JSON.parse(localStorageProducts);
      this.shoppingCartLength = this._cartProducts.length;
      
      let localStorageQty = localStorage.getItem('InitQtyInCart');
      this.InitQtyInCart = JSON.parse(localStorageQty);
      let localStorageInitPrices = localStorage.getItem('initPrices');
      this.initPrices = JSON.parse(localStorageInitPrices);
      this.calcMonthlyPrice(); //on product remove, calculate monthly price again
      this.calcFullPrice(); //on product remove, calculate onetime price again
      this.updateShoppingCartLength(); //recalculate cart products length
    }

    if (localStorage.getItem('activationFee') !== null) {
      this.activationFee = JSON.parse(localStorage.getItem('activationFee'));
    }
  }

  public get oneTimePayment(): boolean {
    if (this._oneTimePayment === null) {
      if (localStorage.getItem('priceChecked')) {
        this._oneTimePayment = (localStorage.getItem('priceChecked') == '1' ? true: false);  
      } else {
        this._oneTimePayment = false;
        localStorage.setItem('priceChecked', '0');
      }
    }
    
    return this._oneTimePayment;
  }

  public set oneTimePayment(value: boolean) {
    this._oneTimePayment = value;
    localStorage.setItem('priceChecked', (value ? '1' : '0'));
  }

  public get shoppingCartLength(): number {
    return this._shoppingCartLength;
  }

  public set shoppingCartLength(value: number) {
    this._shoppingCartLength = value;
  }

  public get cartProducts(): any {
    return this._cartProducts;
  }

  public set cartProducts(value: any) {
    this._cartProducts = value;  
  }  

  sum(input) {
    var total = 0;
    for (var i = 0; i < input.length; i++) {
      if (isNaN(input[i])) {
        continue;
      }
      total += Number(input[i]);
    }
    return total;
  }

  //check if "Základný balíček" is in cart
  checkBasicPackage_inCart() {
    let result = false;
    for (let c = 0; c < this._cartProducts.length; c++) {
      const prodInCart = this._cartProducts[c];
      if (prodInCart.urlText == config.basicPackage.urlText) {
        result = true;
        this.basicPackagePrice = prodInCart.price;
      } 
    }

    this.basicPackageInCart = result;
    this.setActivationFee();
    return result;
  }

  setActivationFee() {
    this.activationFee = this.basicPackageInCart === true ? 9.99 : 0; //if zakladny balik is not in cart, set activation fee to 0€
  }


  //recalculate number of all products in cart.
  updateShoppingCartLength() {
    this.shoppingCartLength = this.sum(this.InitQtyInCart);
  }
  
  //add product to cart
  public add(product: any) {
    product['isInCart'] = true; //product was added to cart;
    this.cartProducts.push(product);
    this.qtyInCart(); //get initial qty for products in cart
    this.updateShoppingCartLength(); //update num of products in cart
    this.getInitPrices(product.price); //get price for product added to cart
    this.calcMonthlyPrice(); //calc monthly price
    this.calcFullPrice(); //calc onetime price
    this.checkBasicPackage_inCart();
    this.addToLocaleStorage();
  }

  //init quantity for all products in cart set to 1
  qtyInCart(){
    if (this.InitQtyInCart.length == 0) {
      this.InitQtyInCart = []
      this._cartProducts.forEach(prod => { 
          this.InitQtyInCart.push(1)
      });
    } else {
      //if single product is added to cart set its quantity to 1
      this.InitQtyInCart.push(1)
    }
  }
  
  //get init price for product added to cart
  getInitPrices (product) {
    this.initPrices.push(product)
  }

  //calc final monthly price of all products in cart
  calcMonthlyPrice() {
    this.fullMonthlyPrice = 0;
    for (var i = 0; i < this._cartProducts.length; i++) {
      this.fullMonthlyPrice += this._cartProducts[i].price * this.InitQtyInCart[i];
    }
  }

  //calc oneTime price of all products in cart
  calcFullPrice() {
    this.oneTimePrice = 0;
    this.fullPrices = [];
    this._cartProducts.forEach(element => {
      this.fullPrices.push(parseFloat(element.fullPrice));
    });
    for (var i = 0; i < this._cartProducts.length; i++) {
      this.oneTimePrice += this.fullPrices[i] * this.InitQtyInCart[i];
      //console.log( this.fullPrices[i]* this.InitQtyInCart[i])
    }
  }

  //remove item from cart
  remove(index: number) {
    if (index > -1) {
      this._cartProducts.splice(index, 1);
      this.InitQtyInCart.splice(index, 1);
      this.initPrices.splice(index, 1);
      this.fullPrices.splice(index, 1);
      //this.shoppingCartLength = this._cartProducts.length; //change number of products in cart if product removed
      this.calcMonthlyPrice(); //on product remove, calculate monthly price again
      this.calcFullPrice(); //on product remove, calculate onetime price again
      this.updateShoppingCartLength(); //recalculate cart products length
      this.checkBasicPackage_inCart();
      this.addToLocaleStorage();
    }
  }

  public clear() {
    this._cartProducts = [];
    this.InitQtyInCart = [];
    this.initPrices = [];
  }

  addToLocaleStorage() {
    localStorage.setItem('cartProd', JSON.stringify(this._cartProducts));
    localStorage.setItem('InitQtyInCart', JSON.stringify(this.InitQtyInCart));
    localStorage.setItem('initPrices', JSON.stringify(this.initPrices));
    localStorage.setItem('activationFee', JSON.stringify(this.activationFee));
  }
}
