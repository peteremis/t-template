import { Injectable } from '@angular/core';

@Injectable()
export class AppService {
 
  private data: any = {
    product: null
  };
  
  constructor() { }

  public get currentProduct(): String {
    return this.data.product;
  }

  public set currentProduct(value: String) {
    this.data.product = value;
  }  

}
