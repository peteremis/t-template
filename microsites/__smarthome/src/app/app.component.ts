import { Component, OnInit } from '@angular/core';
import * as AOS from 'aos';
import { Router, Route, NavigationEnd, NavigationStart, ActivatedRoute } from "@angular/router";

//declare google analytics as "ga"
declare var ga: Function;

@Component({
  selector: 'smarthome-app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  constructor(public router: Router, activatedRoute: ActivatedRoute) {
  
  }


  //scroll back to top on route change
  onRouterActivate(event) {
    let scrollToTop = window.setInterval(() => {
        let pos = window.pageYOffset;
        if (pos > 0) {
            window.scroll(0, pos - 20); // how far to scroll on each step
        } else {
            window.clearInterval(scrollToTop);
        }
    }, 16);
  }

  ngOnInit() {
    AOS.init();

    //ngx-analytics
    this.router.events.subscribe(event => {
      let route = "smarthome" + this.router.url;
      try {
        if (typeof ga === 'function') {
          if (event instanceof NavigationEnd) {
            (<any>window).ga('send', {
              'hitType': 'pageview',
              'page' : route,
              'hitCallback': function() {
                  //console.log(route);
              }
          });
          }
        }
      } catch (e) {
        console.log(e);
      }
    });
    }
  }
