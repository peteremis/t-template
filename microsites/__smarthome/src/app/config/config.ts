export const config = {
  url: {
    strapi: "https://www.telekom.sk/smarthomeapi",
    img: "https://www.telekom.sk/documents/10179/16827704",
    pdf: "https://www.telekom.sk/documents/10179/17368337"
  },

  basicPackage: {
    id: "5ba8c503934f2c0177f3f07d",
    urlText: "zakladny-balik",
    name: "Základný balík"
  }
};

/*
  category: [
    {
      id: null,
      name: "Všetky produkty"
    },
    {
      id: "5b0efecd68f6c923a1624663",
      name: "Bezpečnosť"
    },
    {
      id: "5b0efed868f6c923a1624664",
      name: "Efektivita"
    },
    {
      id: "5b0efee068f6c923a1624665",
      name: "Romantika"
    },
    {
      id: "5b0efee968f6c923a1624666",
      name: "Pohodlie"
    },
    {
      id: "5b0efef168f6c923a1624667",
      name: "Požiar"
    }
  ],
  */