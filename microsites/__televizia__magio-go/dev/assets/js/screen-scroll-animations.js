/* screen split scroll */
function splitScroll() {
  const controller = new ScrollMagic.Controller();

  var tween1 = new TimelineMax();
  tween1
    .from(".img-1", 1000, {
      y: "0px",
      rotation: 20,
      ease: Sine.easeInOut
    })
    .to(".img-1", 2000, {
      y: "+450px",
      x: "+60px",
      rotation: 0,
      ease: Sine.easeInOut
    })
    .to(".img-1", 1000, {
      scaleX: 1.5,
      scaleY: 1.5,
      y: "+950px",
      x: "-200%",
      opacity: 0,
      rotation: 33,
      ease: Sine.easeInOut
    })
    .to(".img-1", 1000, {
      opacity: 0,
      scrollTo: $(".device").eq(1)
    });

  var tween2 = new TimelineMax();
  tween2
    .to(".img-2", 1, { y: "+450px", rotation: 0, ease: Sine.easeInOut })
    .to(".img-2", 1.5, { y: "+150px", rotation: 40, ease: Sine.easeInOut });

  var tween3 = new TimelineMax();
  tween3
    .to(".img-3", 0.7, { y: "+=150", x: "-150px", ease: Sine.easeInOut })
    .to(".img-3", 0.7, { y: "+=450", x: "0px", ease: Sine.easeInOut })
    .to(".img-3", 0.9, { rotation: 40, y: "+=250px", ease: Sine.easeInOut });

  new ScrollMagic.Scene({
    duration: "100%",
    triggerElement: ".device-1",
    triggerHook: 0,
    duration: $(window).height() + 100
  })
    .setTween(tween1)
    .setPin(".device-1")
    // .addIndicators()
    .addTo(controller);

  new ScrollMagic.Scene({
    duration: "100%",
    triggerElement: ".device-2",
    triggerHook: 0,
    duration: $(window).height() + 100
  })
    .setTween(tween2)
    .setPin(".device-2")
    // .addIndicators()
    .addTo(controller);

  new ScrollMagic.Scene({
    duration: "100%",
    triggerElement: ".device-3",
    triggerHook: 0,
    duration: $(window).height() + 100
  })
    .setTween(tween3)
    .setPin(".device-3")
    // .addIndicators()
    .addTo(controller);
}

$(window).on("load", function() {
  var win = $(this);
  if (win.innerWidth() > 543) {
    splitScroll();
  }
});
