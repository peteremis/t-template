$(function() {


  $('.info-link-1').qtip({
        content: {
            text: 'Wearables sú inteligentné zariadenia, ktoré užívatelia nosia na svojom tele. Môžu to byť napr. hodinky, náramok alebo časť odevu, ktorá má množstvo praktických funkcií, napr. monitorovanie vášho tepu alebo vytočenie te. čísla.'
        }
    });

    $('.info-link-2').qtip({
        content: {
            text: 'Externá batéria 2600 mAh s výstupom DC5V/1A slúži ako nabíjačka vášho mobilu či tabletu aj tam, kde nie je dostupná elektrická zásuvka.'
        }
    });


});
