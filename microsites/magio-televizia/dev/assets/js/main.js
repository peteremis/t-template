$(function () {

    /* TOOLTIP */

    $('.info-activation').qtip({
        content: {
            text: 'AKO NOVÝ ZÁKAZNÍK: <br />' +
                'Ľubovoľné balíky aktivujete v 3. kroku nákupného košíka. <br />' +
                'AK UŽ MAGIO TV MÁTE: <br />' +
                'Balíky môžete ľahko spravovať priamo cez svoju Magio televíziu.'
        }
    });

    $('.info-1').qtip({
        content: {
            text: 'Pre nahrávanie je potrebný: <br />' +
                '1. Magio box s nahrávaním, ktorý v prípade záujmu zvoľte v nákupnom košíku (max. 4 kusy pre jednu adresu zriadenia). <br />' +
                '2. Mať na adrese zriadenia televízie dostupnú optickú technológiu alebo satelit.'
        }
    });

    $('.info-2').qtip({
        content: {
            text: 'Pre Spustenie programu od začiatku a Magio archív sú dostupné tieto stanice:Jednotka, Dvojka, Markíza, DajTo, JOJ, JOJ Plus, TV Doma, TA 3, ČT1, ČT2'
        }
    });

    $('.info-3').qtip({
        content: {
            text: 'Spustenie programu od začiatku a Magio archív si môžete objednať s Magio TV po výbere programu. Ak už máte Magio TV, túto službu si môžete aktivovať na bezplatnej Zákazníckej linke 0800 123 456.'
        }
    });

    $('.info-4').qtip({
        content: {
            text: 'Pre nahrávanie, zastavenie a pretáčanie programu je potrebné mať Magio box s nahrávaním. Podľa dostupných technológií u vás doma, si môžete vziať až 4 Magio boxy. Magio box si vyberiete  v nákupnom košíku.'
        }
    });

    $('.info-5').qtip({
        content: {
            text: 'Vezmite si k Magio televízii aj internet a/alebo Pevnú linku. Získate tak Chytrý balík služieb za výhodnejšu cenu.Pokračujte zeleným tlačidlom VYBRAŤ. V ďalšom kroku si navolíte internet ako samostatnú službu alebo v kombinácii s ďalšími službami.'
        }
    });

    /* STICKY NAV */

    $("#nav-sticky").sticky({
        topSpacing: 0,
        widthFromWrapper: true
    });

    $(".scroll-to").click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top - ($("#nav-sticky").outerHeight())
                }, 1000);
                return false;
            }
        }
    });

    var contentSections = $("[id*='sec-']"),
        secondaryNav = $("#nav-sticky");

    function updateSecondaryNavigation() {
        contentSections.each(function () {
            var actual = $(this),
                actualHeight = actual.height() + parseInt(actual.css('paddingTop').replace('px', '')) + parseInt(actual.css('paddingBottom').replace('px', '')),
                actualAnchor = secondaryNav.find('a[href="#' + actual.attr('id') + '"]');

            if ((actual.offset().top - secondaryNav.outerHeight() <= $(window).scrollTop()) && (actual.offset().top + actualHeight - secondaryNav.outerHeight() > $(window).scrollTop())) {
                actualAnchor.addClass('active');
            } else {
                actualAnchor.removeClass('active');
            }

        });
    }
    updateSecondaryNavigation();

    $(window).scroll(function (event) {
        updateSecondaryNavigation();
    });

//     /* TABS */
// $(".tabs-menu a").click(function(event) {
//     event.preventDefault();
//     $(this).parent().addClass("current");
//     $(this).parent().siblings().removeClass("current");
//     var tab = $(this).attr("href"), // select clicked tab. In example "#tab-1" ..
//         parent = $(this).closest('ul').parent().parent(); // <tabs-container>
//     parent.find('.tab-content').not(tab).css("display", "none"); //hide all not clicked tabs
//     $(tab).fadeIn(); //show clicked tab
//
//
// });
// /* TABS END */
//
// /* FROM TABS TO ACCORDION */
// var dataSegment = $("[data-segment]").each(function(){
//   //console.log($(this).attr('data-segment')); //zobraz "data-segment" hodnotu pre kazdu <section>
//
//   closestHead = $($(this).find('.tabs-menu a'));
//   closestItemCount = closestHead.length - 1; //vytiahni pocet tabov pre kazdu <section> s atributom "data-segment"
//   //console.log(closestItemCount + "heads");
//
//   closestContent = $($(this).find('.tab-content')); //najdi tab-content v danej <section>
//   //console.log(closestContent);
//
//   closestAccordion = $(this).find('.accordion'); //najdi .accordion v danej <section>
//
//   accordionItem = '<div class="item">';
//
//   for (var i = 0; i <= closestItemCount; i++) {
//       accordionItem += '<div class="heading">' + $(closestHead[i]).text()  + '</div>';
//       accordionItem += '<div class="content">' + $(closestContent[i]).html() + '</div></div>';
//
//       if (i !== closestItemCount) {
//           accordionItem += '<div class="item">';
//       }
//   }
//
//   //if data-segment and data-accordion value match, show accordion data
//   if($(this).attr('data-segment') === closestAccordion.attr('data-accordion')) {
//        $(accordionItem).appendTo(closestAccordion);
//   }
//
//   var $items = $('.accordion .item');
//   //SET OPENED ITEM
//   $($items[0]).addClass('open');
//
// });
// /* FROM TABS TO ACCORDION END */
//
// /* ACCORDION */
// $('.accordion .item .heading').click(function(e) {
//
//     var clickedHead = e.target;
//     var $item = $(clickedHead).closest('.item');
//     var isOpen = $item.hasClass('open');
//     var $content = $item.find('.content');
//     var $acPrice = $(clickedHead).find('.ac-price');
//
//     if (isOpen) {
//         $content.slideUp(200);
//         $item.removeClass('open');
//         $acPrice.show();
//     } else {
//         $content.slideDown(200);
//         $item.addClass('open');
//         $acPrice.hide();
//     }
// });
//
//
// $('.accordion .item.open').find('.content').slideDown(200);
// /* ACCORDION END */

    $('.popup').fancybox({
        padding: 10,
        margin: 0,
        parent: "#content",
        closeBtn: false,
        helpers : {
            overlay : {
                css : {
                    'background' : 'rgba(0, 0, 0, 0.7)'
                }
            }
        }
    });

    $('.close-end a').click(function(){
        $.fancybox.close();
    });

    /* DIV DATA-HREF */
    $('#content .tab-acc-sub').click(function(){
        window.location.href = '/' + $(this).attr('data-href');
    });

    $('#show_legal').click(function(){
        $(this).hide();
        $('#legal_more').show(200);
    });

    $( ".sec-akcie" ).load( "templates/_aktualne-akcie.html", function() {
    });

});
