var jsonURL = 'https://backvm.telekom.sk/data/forum',
testURL = '../assets/js/API.json',
URL = null,
topics = '<div class="ft__head">' +
            '<p>Nové témy</p>' +
          '</div>',
posts = '<div class="ft__head">' +
            // '<p>Najnovšie príspevky</p>' +
         '</div>',
$topicForum = $('.ft__topics'),
$postsForum = $('.ft__posts'),
$forumSection = $('.forum-section .container-fixed');

if (window.location.hostname === 'localhost') {
    URL = testURL;
} else {
    URL = jsonURL;
}

$.ajax({
    type: "GET",
    async: true,
    crossDomain: true,
    dataType: "json",
    url: URL,
    mimeType: "application/json",
    success: function (items) {

        if (items === undefined || items === null) {
            hideForumSection();
        }

        loadForumItems(items);
    },
    error: function () {
        hideForumSection();
    }
});

function loadForumItems(itemObjArray) {
    for (var iOindex = 0; iOindex < itemObjArray.length; iOindex++) {
        buildItem(itemObjArray[iOindex]);
    }
    appendToForum();
}

function buildItem(itemObj) {
    var item =
    '<a href="' + itemObj.url + '" target="_blank" class="forum-link">' +
    '<div class="ft__item">' +
        '<div class="ft__left">' +
            '<span class="fti__title">' + truncate(itemObj.title) + '</span>' +
            // '<p class="fti__cat">' + itemObj.cat + '</p>' +
        '</div>' +
        '<div class="ft__mid">' +
            '<p class="fti__timing"><i class="fa fa-clock-o post-clock" aria-hidden="true"></i>' + itemObj.time + '</p>' +
        '</div>' +
        '<div class="ft__right">' +
          //  '<img src="/documents/10179/9664823/arrow-right.png" alt="">' +
        '</div>' +
    '</div>' +
    '</a>';
    setLists(itemObj.type, item);
}

function setLists(type, item) {
    if (type === 'topic') {
        topics += item;
    } else {
        posts += item;
    }
}

function appendToForum() {
    $(posts).appendTo($postsForum);
    $(topics).appendTo($topicForum);
}

function hideForumSection() {
    $forumSection.hide();
}

// function truncate(string){
//    if (string.length > 25) {
//       return string.substring(0,25)+'...';
//    } else {
//       return string;
//     }
// };

//if init device width is less than 450px or more than 450px, truncate text
function truncate(string) {
  // if width < 450px
  if ($(window).width() < 450) {
    return string;
  }
  //if width <= 768px
  else if ($(window).width() <= 768) {
    if (string.length > 25) {
      return string.substring(0, 25) + '...';
    } else {
      return string;
    }
  }
  //if width > 768px
  else if ($(window).width() > 768) {
    if (string.length > 35) {
      return string.substring(0, 43) + '...';
    } else {
      return string;
    }
  }
}
