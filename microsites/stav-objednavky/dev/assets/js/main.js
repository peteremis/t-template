$(document).ready(function () {

	var redirect = getParameterByName('redirect');

	var orderId = '';

	if (redirect) {
		console.log(redirect);
		orderId = redirect.substr(redirect.lastIndexOf('/') + 1);
	} else {
		orderId = getParameterByName('id');
	}

	function getParameterByName(name, url) {
		if (!url) url = window.location.href;
		name = name.replace(/[\[\]]/g, "\\$&");
		var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
			results = regex.exec(url);
		if (!results) return null;
		if (!results[2]) return '';
		return decodeURIComponent(results[2].replace(/\+/g, " "));
	}

	var apiURL = 'https://m2.telekom.sk';
	if (window.location.hostname == 'localhost') {
		apiURL = 'http://mr.telekom.localhost';
	}

	if (orderId) {
		$("#ajaxLoadingSpinnerOverlay").show();

		apiURL = apiURL + '/api/telekom/get-order';

		$.ajax({
			type: "GET",
			async: true,
			crossDomain: true,
			dataType: "json",
			data: {
				id: orderId
			},
			url: apiURL,

			success: function (response) {
				console.log('hide');
				$(".order-error").hide();
				$(".order-ok").show();

				$("#ajaxLoadingSpinnerOverlay").hide();

				console.log(response);
				if (response['status'] == 'ok') {
					changeStep(response['step']);

					$("#order_status h3").text(response['message']);
					$("#order_id").text(orderId);
					$("#created").text(response['created']);
				} else {
					$("#error-description").text(response['message']);
					$(".order-error").show();
					$(".order-ok").hide();
					$("#ajaxLoadingSpinnerOverlay").hide();
				}

			},
			error: function () {
				console.log('hide');
				$("#error-description").html("Nepodarilo sa overiť stav objednávky " + orderId + ". Skúste to prosím neskôr");
				$(".order-error").show();
				$(".order-ok").hide();
				$("#ajaxLoadingSpinnerOverlay").hide();
			}
		});

	}

	function changeStep(step) {

		var step1 = $("#step1");
		var step2 = $("#step2");
		var step3 = $("#step3");

		if (step == 1) {
			step1.addClass('active');
			step2.removeClass('active');
			step3.removeClass('active');
		}
		if (step == 2) {
			step1.addClass('active');
			step2.addClass('active');
			step2.addClass('step_waiting');
			step3.removeClass('active');

		}
		if (step == 3) {
			step1.addClass('active');

			step2.addClass('active');
			step2.addClass('step_ok');

			step3.addClass('active');
			step3.addClass('step_ok');
		}
	}

});
