$(function () {
    var width = $('#container').width(),
        height = $('#container').height();


    var bombs = [{
            name: 'Joe 4',
            radius: 25,
            yield: 400,
            country: 'USSR',
            fillKey: 'RUS',
            significance: 'First fusion weapon test by the USSR (not "staged")',
            date: '1953-08-12',
            latitude: 48.14,
            longitude: 17.10
    }, {
            name: 'RDS-37',
            radius: 40,
            yield: 1600,
            country: 'USSR',
            fillKey: 'RUS',
            significance: 'First "staged" thermonuclear weapon test by the USSR (deployable)',
            date: '1955-11-22',
            latitude: 48.31,
            longitude: 18.08

    }];


    var map = new Datamap({
        scope: 'svk',
        responsive: true,
        fills: {
            defaultFill: '#fff' // The keys in this object map to the "fillKey" of [data] or [bubbles]
        },
        element: document.getElementById('container'),
        geographyConfig: {
            highlightFillColor: '#f2f2f2'
        },
        setProjection: function (element) {
            var projection = d3.geo.mercator()
                .scale(width * 8.45) //7300
                .center([19.680074266824, 48.63670023565])
                .translate([width / 2, height / 2])

            var path = d3.geo.path()
                .projection(projection);

            return {
                path: path,
                projection: projection
            };
        }
    });

    map.bubbles(bombs, {
        popupTemplate: function (geo, data) {
            return ['<div class="hoverinfo">ddddd</div>'].join('');
        }
    });

    /*map.labels({
        labelColor: '#e20074',
        fontSize: 12
    });*/

    $(window).on('resize', function () {
        map.resize();
    });
});
