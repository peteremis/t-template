$(function () {

    $(".scroll-to").click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });

    $("#slider").owlCarousel({
        loop: true,
        nav: true,
        navText: [
            "<img src='/documents/10179/5867403/arrow-left.png' alt='prev'/>",
            "<img src='/documents/10179/5867403/arrow-right.png' alt='next'/>"
        ],
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 2
            },
            1000: {
                items: 3
            }
        }
    });

    /* SWITCHING / FILLING CONTENT */

    var jobUrl1 = 'predajca-telekom-centrum',
    jobUrl2 = 'predajca-call-centrum',
    youtubeUrl = 'https://www.youtube.com/embed/',
    videoUrl1 = 'fTeh9_WiNfU',
    videoUrl2 = 'nxGbmL0Mt-I',
    videoTitle1 = ['POZRI SI NAŠICH BEST-SELLEROV', 'ZUZKU A RADOVANA'],
    videoTitle2 = ['POZRI SI NAŠICH BEST-SELLEROV', ''],
    $videoTitle1 = $('#vt1'),
    $videoTitle2 = $('#vt2'),
    $jobDesc = $('#jobDesc'),
    $jobBtn1 = $('#jobBtn1'),
    $jobBtn2 = $('#jobBtn2'),
    $benefits = $('#benefits'),
    $workInfo = $('#work-info'),
    $promoVid = $('#promo-video');

    var jobDesc1 = '– Best-sellera (predajcu) do nášho Telekom Centra.',
    jobDesc2 = 'T-asistenta - predajcu aj servismena do nášho tímu kontaktného centra.',
    workInfo1 = 'Úlohou predajcu je... Správne!<br />Budeš predávať, čo uvidíš zákazníkom na očiach, ale aj produkty a služby, po ktorých zatúžia, hneď ako im ich predstavíš. Tiež im poradíš, pomôžeš, poďakuješ za návštevu. Proste biznis – poznáš to, nie? A budeš tiež na sebe pracovať, aby si sa čoskoro stal Best-sellerom.',
    workInfo2 = 'Každodenným chlebíčkom našich T-asistentov v kontaktom centre je starostlivosť o zákazníkov úzko prepojená s aktívnym predajom produktov a služieb Telekomu.' +
                'Spokojnosť zákazníkov je pre nás prvoradá! Našou úlohou je starať sa o zákazníkov, riešiť ich požiadavky a prostredníctvom zisťovania potrieb predávať zákazníkom všetko, čo im na očiach vidíme, ale aj produkty a služby, po ktorých zatúžia akonáhle im ich predstavíš. ' +
                'Magio Televízia, Internet, Happy paušály...? Sme tu preto aby sme našli pre každého zákazníka riešenie šité na mieru!' +
                'Práca na kontaktom centre sa nezaobíde bez bežnej administratívy, zadávania požiadaviek do počítača a spolupráce s ostatnými oddeleniami spoločnosti. Spokojnosť zákazníka súvisí aj s dostupnosťou kontaktného centra, preto pracujeme v dvojzmennej prevádzke od 8,00 – 21,00 hod. (ranné, poobedné v dĺžke 8,15 hod vrátane obeda).';

    var activeBtn;

    function getUrlParameter(sParam) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    }

    function setActiveBtn($activeBtn) {
        activeBtn = $activeBtn;
        $activeBtn.addClass('active');
    }

    function loadContent() {
        var urlParam = getUrlParameter('job');

        if (urlParam === 'predajca-call-centrum') {
            changeText($jobDesc, jobDesc2);
            toggleSectionVisibility($benefits, false);
            changeText($workInfo, workInfo2);
            setActiveBtn($jobBtn2);
            changeVideoTitle(videoTitle2[0], videoTitle2[1]);
        } else {
            changeText($jobDesc, jobDesc1);
            toggleSectionVisibility($benefits, true);
            changeText($workInfo, workInfo1);
            setActiveBtn($jobBtn1);
        }

        changeVideo();
    }

    function reloadContent() {
        if (activeBtn == $jobBtn1) {
            changeText($jobDesc, jobDesc1);
            toggleSectionVisibility($benefits, true);
            changeText($workInfo, workInfo1);
        } else {
            changeText($jobDesc, jobDesc2);
            toggleSectionVisibility($benefits, false);
            changeText($workInfo, workInfo2);
        }

        changeVideo();
    }

    function changeText($hostElem, newText) {
        $hostElem.html(newText);
    }

    function changeVideo() {
        var urlParam = getUrlParameter('job');
        var url = youtubeUrl;

        if (activeBtn == $jobBtn1) {
            changeVideoTitle(videoTitle1[0], videoTitle1[1]);
            url+= videoUrl1;
        } else {
            changeVideoTitle(videoTitle2[0], videoTitle2[1]);
            url+= videoUrl2;
        }

        $promoVid.attr('src', url);

    }

    function changeVideoTitle(text, text2) {
        changeText($videoTitle1, text);
        changeText($videoTitle2, text2);
    }

    function toggleSectionVisibility($elem, hide) {
        if (hide) {
            $elem.hide();
        } else {
            $elem.show();
        }
    }

    $jobBtn1.on('click', function(e) {
        e.preventDefault();
        var $this = $(this);

        if (!$this.hasClass('active')) {
            $this.addClass('active');
            $jobBtn2.removeClass('active');
        }
        activeBtn = $jobBtn1;
        reloadContent();
    });

    $jobBtn2.on('click', function(e) {
        e.preventDefault();
        var $this = $(this);

        if (!$this.hasClass('active')) {
            $this.addClass('active');
            $jobBtn1.removeClass('active');
        }
        activeBtn = $jobBtn2;
        reloadContent();
    });

    function init() {
        loadContent();
    }

    init();
});
