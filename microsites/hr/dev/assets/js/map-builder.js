$(function () {
    var $map = $('#map'),
    $error = $('#data-error'),
    $positionHolder = $('.position__holder'),
    $otherPositions = $('.other-positions'),
    $mobilePositions = $('.mobile-position__holder'),
    mapList = [
		'Bánovce nad Bebravou', 'Banská Bystrica', 'Banská Štiavnica', 'Bardejov', 'Bratislava', 'Brezno', 'Bytča',
		'Čadca',
		'Detva', 'Dolný Kubín', 'Dubnica nad Váhom', 'Dunajská Streda',
		'Galanta',
		'Hlohovec', 'Humenné',
		'Ilava',
		'Kežmarok', 'Komárno', 'Košice', 'Kráľovský Chlmec', 'Krupina',  'Kysucké Nové Mesto',
		'Levice', 'Levoča', 'Liptovský Mikuláš', 'Lučenec',
		'Malacky', 'Martin', 'Michalovce', 'Moldava nad Bodvou', 'Myjava',
		'Námestovo', 'Nitra', 'Nová Baňa', 'Nové Mesto nad Váhom', 'Nové Zámky',
		'Partizánske', 'Pezinok', 'Piešťany', 'Poprad', 'Považská Bystrica', 'Prešov', 'Prievidza', 'Púchov',
		'Revúca', 'Rimavská Sobota', 'Rožňava', 'Ružomberok',
		'Sabinov', 'Senec', 'Senica', 'Sereď', 'Skalica', 'Snina', 'Sobrance', 'Spišská Nová Ves', 'Stará Ľubovňa', 'Stropkov', 'Svidník',
		'Šahy', 'Šaľa', 'Šamorín', 'Štúrovo', 'Šurany',
		'Topoľčany', 'Tornaľa', 'Trebišov', 'Trenčín', 'Trnava', 'Tvrdošín',
		'Veľký Krtíš', 'Vranov nad Topľou', 'Vrútky',
		'Zlaté Moravce', 'Zvolen',
		'Žiar nad Hronom', 'Žilina'
    ],
    jobObject = {},
    testEnv = true;

    if (document.location.host.contains('localhost')) {
        testEnv = false;
    }

    function getObjSize(myObj) {
        if (myObj == null) {
            return 0;
        }
        return Object.keys(myObj).length;
    }

    function isCityAvailable(city) {
        for (var j = 0; j <= mapList.length - 1; j++) {
            if (city == mapList[j]) {
                return true;
            }
        }
        return false;
    }

    function createTooltip(className, tooltipText) {
        $(className).qtip({
            content: {
                text: tooltipText
            },
            show: 'click',
            hide: 'unfocus',
            position: {
                my: 'left center',
                at: 'right center'
            }
        });
    }

    function createCity(city, tooltipCont) {
        var $city = $(document.createElement('div'));
        $city.addClass('city ' + city);
        $($city).appendTo($map);
        createTooltip($city, tooltipCont);
    }

    function buildOtherLocations(location, content) {
        return  '<p class="other-city">' + location + '</p>' + content;
    }

    function buildMobileLocations(location, content) {
        return  '<p class="other-city">' + location + '</p>' + content;
    }

    function getDataFromXml(data) {

        $(data).find('offer').each(function () {

            var $this = $(this),
            jobLocation = $this.find('offerregions').text().trim(),
            jobTitle = $this.find('position').text().trim(),
            jobUrl = $this.find('url').text().trim(),
            objValueSize = getObjSize(jobObject[jobLocation]),
            tempObj = {};

            for (var k = 0; k <= mapList.length - 1; k++) {

                if (mapList[k] !== jobLocation) {
                    if (jobLocation.indexOf(mapList[k]) !== -1) {
                        jobLocation = mapList[k];
                    }
                }
            }

            tempObj = {
                [jobLocation]: {
                    [objValueSize]: {
                        'title': jobTitle,
                        'url': jobUrl
                    }
                }
            }
            $.extend( true, jobObject, tempObj );
        });

    }

    function buildMap() {
        if (testEnv) {

            var testCities = '';

            mapList.forEach(
                function(city, item) {
                    testCities += '<div class="city ' + city + '" data-hasqtip="' + item + '"></div>'
                }
            );
            $map.html(testCities);
        } else {

        for(var city in jobObject) {

            var tempLocationContent = '',
            tempUrl = '',
            tempTitle = '';

            for (var i = 0; i <= getObjSize(jobObject[city]) - 1; i++) {

                tempUrl = jobObject[city][i].url;
                tempTitle = jobObject[city][i].title;

                tempLocationContent += '<a href="' + tempUrl + '" target="_blank">' + tempTitle + '</a>';

            }

            $(buildMobileLocations(city, tempLocationContent)).appendTo($mobilePositions);

            if(isCityAvailable(city)){

                var tempTooltipCont = '',
                tempUrl = '',
                tempTitle = '';

                for (var i = 0; i <= getObjSize(jobObject[city]) - 1; i++) {

                    tempUrl = jobObject[city][i].url;
                    tempTitle = jobObject[city][i].title;

                    tempTooltipCont += '<a href="' + tempUrl + '" target="_blank">' + tempTitle + '</a>';
                }

                createCity(city, tempTooltipCont);

            } else {

                if (!$($otherPositions).is(':visible')) {
                    $($otherPositions).show();
                }
                var tempLocationContent = '',
                tempUrl = '',
                tempTitle = '';

                for (var i = 0; i <= getObjSize(jobObject[city]) - 1; i++) {

                    tempUrl = jobObject[city][i].url;
                    tempTitle = jobObject[city][i].title;

                    tempLocationContent += '<a href="' + tempUrl + '" target="_blank">' + tempTitle + '</a>';

                }

                $(buildOtherLocations(city, tempLocationContent)).appendTo($positionHolder);
            }

        }
    }
    }



    $.ajax({
        url: 'https://m.telekom.sk/hr/api/1007'

    }).done(function (data) {
        getDataFromXml(data);
        buildMap();

    }).fail(function () {
        $map.hide();
        $error.text('Nepodarilo sa načítať dáta. Skúste navštívit našu stránku neskôr.');
    });
});
