dataRatePlanes.sort(function (a, b) {
	if (a.id < b.id) {
		return 1;
	} else {
		return -1;
	}
});

var with_max = false;
for (var i = 0; i < dataRatePlanes.length; i++) {
	if (dataRatePlanes[i].segment === typeSegment) {
		if (dataRatePlanes[i].hero) {
			nbaVars.heroPausaly += getPausalInfo(dataRatePlanes[i], true, [], null);
			if (
				!with_max &&
				(dataRatePlanes[i].rpCode === 'RP1142' ||
					dataRatePlanes[i].rpCode === 'RP1143' ||
					dataRatePlanes[i].rpCode === 'RP1144' ||
					dataRatePlanes[i].rpCode === 'RP1145')
			) {
				with_max = true;
			}
		}

		if (typeof nbaVars.vsetkyPausaly[dataRatePlanes[i].category] === 'undefined')
			nbaVars.vsetkyPausaly[dataRatePlanes[i].category] = '';

		nbaVars.vsetkyPausaly[dataRatePlanes[i].category] += getPausalInfo(dataRatePlanes[i], false, [], null);
	}
}

$(document).ready(function () {
	$('#nav-sticky').sticky({
		topSpacing: 0,
		widthFromWrapper: true,
	});

	$('#hero-slider').html(nbaVars.heroPausaly);

	if ($(window).width() > 767) {
		initSlider('hero-slider');
	}

	addToolTip($('.pausaly'));

	if (with_max) {
		$('.hero-offer .carts-cont').addClass('with-max');
	}

	$('.t-icon.info').each(function () {
		$(this).qtip({
			content: {
				button: 'Close',
				text: $(this).attr('alt'),
			},
			style: {
				classes: 'qtip-new',
			},
			position: {
				corner: {
					target: 'leftMiddle',
					tooltip: 'topMiddle',
				},
			},
		});
	});

	$('#sliderDatBalik').slick({
		infinite: false,
		slidesToShow: 2,
		slidesToScroll: 1,
		infinite: false,
		dots: true,
		responsive: [
			{
				breakpoint: 700,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					infinite: true,
				},
			},
		],
	});

	/* FROM TABS TO ACCORDION */
	$('.to-accordion').each(function () {
		var closestHead = $(this).find('.tabs-menu a');
		var closestItemCount = closestHead.length - 1; //vytiahni pocet tabov pre kazdu <section> s atributom "data-segment"

		var closestContent = $(this).find('.tab-content'); //najdi tab-content v danej <section>

		var accordionItem = '';

		for (var i = 0; i <= closestItemCount; i++) {
			accordionItem +=
				'<div class="item">' +
				'<div class="heading">' +
				$(closestHead[i]).text().trim() +
				'</div>' +
				'<div class="content">' +
				$(closestContent[i]).html().trim() +
				'</div>' +
				'</div>';
		}

		$(this).find('.accordion').html(accordionItem);

		//var $items = $('.accordion .item');
		//SET OPENED ITEM
		//$($items[0]).addClass('open');
		//$($items[1]).addClass('open');
	});
	/* FROM TABS TO ACCORDION END */

	$(document).on('click', '.accordion a', function (e) {
		e.preventDefault();
		window.location.href = $(this).attr('href');
	});

	$('.popup').fancybox({
		padding: 11,
		margin: 0,
		closeBtn: false,
		parent: '#content',
		maxWidth: 70 + '%',
		helpers: {
			overlay: {
				css: {
					background: 'rgba(0, 0, 0, 0.7)',
				},
			},
		},
	});

	if (window.location.hash) {
		var target = window.location.hash;

		if (target.length > 1) {
			target = target.substr(1);

			var elems = ['t-zaklad', 't-data', 't-ideal', 't-nekonecno'];
			var index = elems.indexOf(target) + 1;
			if (index > 0) {
				showCategories(index, 0);
				target = 'vsetky-pausaly';
			}

			elems = ['novy-pausal', 'prenos-dat-zadarmo', 'viac-dat-a-volani'];
			index = elems.indexOf(target) + 1;
			if (index > 0) {
				$('#sec-7 .tabs-menu li.current').removeClass('current');
				$('#sec-7 .tabs-menu li.variant').eq(index).addClass('current');
				$('#sec-7 .tab .tab-content').hide();
				$('#sec-7 .tab .tab-content').eq(index).show();
				target = 'aktualne-akcie';
			}

			setTimeout(function () {
				scrollTo($('.' + target), 500);
			}, 750);
		}
	}
});

$('.category').click(function () {
	if ($(this).hasClass('selected')) return;

	var categoryId = parseInt($(this).attr('data-type'));

	if (categoryId === 2 || categoryId === 4) {
		$('.vsetky-pausaly .carts-cont').addClass('with-max');
	} else {
		$('.vsetky-pausaly .carts-cont').removeClass('with-max');
	}

	$(this).parents().find('.category.selected').removeClass('selected');
	$(this).addClass('selected');

	var type = $(this).attr('data-type');

	$('.categories .categories-header p.active').removeClass('active');
	$('.categories .categories-header p.desc[data-type="' + type + '"]').addClass('active');

	if ($(window).width() > 767) {
		$('#pausal-slider').slick('removeSlide', null, null, true);
		$('#pausal-slider').slick('slickAdd', nbaVars.vsetkyPausaly[type]);
	} else {
		$('#pausal-slider').hide(200).html(nbaVars.vsetkyPausaly[type]).show(200);
	}
	addToolTip($('.categories'));
});

$('.vsetky-pausaly .show-categories').click(function (e) {
	e.preventDefault();
	var categoryId = 4;
	if ($('.categories .rp-categories .category.selected').length) {
		categoryId = $('.categories .rp-categories .category.selected').attr('data-type');
	}
	if (categoryId === 2 || categoryId === 4) {
		$('.vsetky-pausaly .carts-cont').addClass('with-max');
	}
	showCategories(categoryId, 200);
});

function showCategories(startCategoryId, time) {
	if ($('.categories').is(':visible')) {
		$('.vsetky-pausaly .show-categories span').text('Pozrieť');
	} else {
		$('.vsetky-pausaly .show-categories span').text('Skryť');
	}

	$('.categories').toggle(time);

	$('.categories .rp-categories .category[data-type="' + startCategoryId + '"]').addClass('selected');
	$('.categories .categories-header p.desc[data-type="' + startCategoryId + '"]').addClass('active');

	if (!$('#pausal-slider .cart').length) {
		if ($(window).width() > 767) {
			setTimeout(
				function () {
					initSlider('pausal-slider');
					$('#pausal-slider').slick('slickAdd', nbaVars.vsetkyPausaly[startCategoryId]);
					addToolTip($('.categories'));
				},
				time === 0 ? 0 : 150
			);
		} else {
			$('#pausal-slider').html(nbaVars.vsetkyPausaly[startCategoryId]);
			addToolTip($('.categories'));
			var initialSlide = startCategoryId === 4 ? 1 : 0;

			bd_generateCarousel('.categories .categories-header .rp-categories', 3, initialSlide);
		}
	}
}

function bd_generateCarousel(el, slidesToShow, initialSlide) {
	const settings = {
		slidesToShow: slidesToShow,
		slidesToScroll: 1,
		variableWidth: true,
		centerMode: false,
		initialSlide: initialSlide,
		arrows: true,
		infinite: false,
		touchThreshold: 10,
		swipeToSlide: true,
		swipe: true,
		touchMove: true,
		draggable: true,
		prevArrow:
			"<button type='button' class='slick-prev pull-left'><i class='fa fa-angle-left' aria-hidden='true'></i></button>",
		nextArrow:
			"<button type='button' class='slick-next pull-right'><i class='fa fa-angle-right' aria-hidden='true'></i></button>",
	};

	var slider = $(el).slick(settings);
}

/* TOGGLE ARROW */
$(document).on('click', '.togleArrow .toggle-arrow, .togleArrow .arrow-right', function (event) {
	event.preventDefault();
	if ($(this).hasClass('arrow-right')) {
		return;
	}
	$(event.target).closest('.togleArrow').find('.arrow-content').toggle(200);
	$(this).find('.arrow-right').toggleClass('arrow-rotate');
});
/* TOGGLE ARROW END */

/* TABS */
$('.tabs-menu a').click(function (event) {
	event.preventDefault();
	$(this).parent().addClass('current');
	$(this).parent().siblings().removeClass('current');
	var tab = $(this).attr('href'), // select clicked tab. In example "#tab-1" ..
		parent = $(this).closest('ul').parent().parent(); // <tabs-container>
	parent.find('.tab-content').not(tab).css('display', 'none'); //hide all not clicked tabs
	$(tab).fadeIn(); //show clicked tab
});
/* TABS END */

// Accordion
$(document).on('click', '.accordion .item .heading', function (e) {
	var item = $(this).closest('.item');
	var isOpen = item.hasClass('open');
	var content = item.find('.content');

	if (isOpen) {
		content.slideUp(200);
		item.removeClass('open');
	} else {
		content.slideDown(200);
		item.addClass('open');
	}
});
// Accordion end

// popup
$('.p-close, .closePopupWindow').on('click', function (e) {
	e.preventDefault();
	$.fancybox.close();
});
// popup close with scroll to section
$(document).on('click', '.popup-modal-dismiss', function (e) {
	e.preventDefault();
	$.fancybox.close();
});
// popup end

// scroll
$('.scroll-to').click(function (e) {
	e.preventDefault();
	if ($(this).attr('data-scroll') && $(this).attr('data-scroll').length) {
		var target = '.' + $(this).attr('data-scroll');
		if (target === '.nekonecne-pausaly') {
			target = '.vsetky-pausaly';

			$('.vsetky-pausaly .carts-cont').addClass('with-max');
			if (!$('#pausal-slider .cart').length) {
				showCategories(4, 0);
			} else {
				$('.categories').show();
				$('.vsetky-pausaly .show-categories span').text('Skryť');
				$('.vsetky-pausaly .categories .rp-categories .category[data-type="4"]').click();
			}
		}

		scrollTo($(target), 1000);
	} else if ($(this).attr('href') && $(this).attr('href').length) {
		var target = $(this).attr('href');
		scrollTo($('.' + target.substr(1)), 1000);
	}
});

$('.sh-pop').click(function () {
	if ($(this).attr('href') === '#magioGOzadarmo') {
		var content =
			'<div class="p-content">' +
			'<div class="popup-lightbox">' +
			'<h4 class="headline">Magio Go Benefit zadarmo</h4>' +
			'<p>S mobilnou TV Magio GO Benefit môžete <b>zadarmo sledovať atraktívny televízny obsah vo svojom mobile či tablete</b>.</p>' +
			'<p>Nájdete v nej videopožičovňu s filmami, dokumentami z BBC či Viasat, rozprávky z Ťuki TV, športové zostrihy, ale aj obľúbené TV stanice: Jednotku, Dvojku, Trojku, Markízu, Dajto, Doma, JOJ, Plus, Wau a Folkloriku, ktoré môžete sledovať naživo alebo zo 7-dňového archívu.</p>' +
			'<p>Magio Go Benefit zadarmo získate na celú dobu viazanosti paušálu a dá sa aktivovať k paušálu T Ideál 25, T Ideál 27, T Ideál 32, T Ideál 37, T Dáta 25, T Dáta HD, T Nekonečno SD, T Nekonečno HD a T Nekonečno MAX.</p>' +
			'<p>Magio Go Benefit je možné sledovať len na jednom zariadení. Všetky potrebné informácie ohľadom aktivácie Magio Go Benefitu vám pošleme v SMS správe po aktivácii vášho paušálu.</p>' +
			'<a href="#" class="btn btn_mag-outline popup-close">Zavrieť okno</a>' +
			'</div></div>';
		showPopup(content);
	}
});

function scrollTo(target, time) {
	//console.log('target:', target);
	if (target.length) {
		var top = target.offset().top;
		if ($('#nav-sticky').is(':visible')) top -= 50;

		$('html,body').animate(
			{
				scrollTop: top,
			},
			time
		);
	}
}
