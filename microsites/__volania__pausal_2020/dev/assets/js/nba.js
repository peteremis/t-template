if (typeof typeSegment === 'undefined') typeSegment = null;

var nbaVars = {
	config: null,
	withDph: true,
	heroPausaly: '',
	vsetkyPausaly: [],
	nbaOffers: {
		tariffChange: [],
		nonHw: [],
		config: {
			withMax: false,
			hasDiscount: false,
		},
	},
	src_channel: getParamUrl('src_channel'),
};

function getParamUrl(paramName) {
	var result = null;
	var url = window.location.href;

	var start = url.indexOf('?');
	if (start > -1) {
		var params = url.substr(start + 1).split('&');
		for (var i = 0; i < params.length; i++) {
			if (params[i].startsWith(paramName + '=')) result = params[i].substr(paramName.length + 1);
		}
	}

	// ak nie je tento param tak ide z webu
	if (paramName === 'src_channel' && result === null) result = 'web';

	return result;
}

$(document).ready(function () {
	var fromHP = localStorage.getItem('userOfferPN');

	if (fromHP) {
		localStorage.removeItem('userOfferPN');

		var d = new Date();
		var pass = d.getDate() + d.getMonth() + d.getFullYear();

		$('#phoneNumber').val(Decrypt(fromHP, pass));
		$('#checkPhone').click();
	} else {
		$.when(nba(null, null, nbaVars.src_channel))
			.done(function (data, textStatus, jqXHR) {
				if (data.configuration) {
					nbaVars.config = data.configuration;
					setScenarioPath();
				}

				if (
					data.offer.verifyStatus === 'BAD_REQUEST' ||
					(typeSegment === null && nbaVars.src_channel === 'app' && getParamUrl('nav') !== 'sc')
				) {
					return;
				}

				$('#phoneNumber').val(data.offer.msisdn);
				nba_action(data.offer, true);
			})
			.fail(function (error, textStatus, errorThrown) {
				if (error.responseJSON) {
					nbaVars.config = error.responseJSON.configuration;
					setScenarioPath();
				}
			});
	}

	new Switchery(document.querySelector('.js-switch'), {
		color: '#e20074',
		secondaryColor: '#ededed',
	});
	$('.switchery').parent().closest('span').addClass('hide-span');
});

$(document).on('click', '.cart .cart-heading, .cart .cart-content', function (event) {
	if ($(event.target).hasClass('info')) return;
	if ($(window).width() > 767) return;
	var parent = $(this).closest('.carts');
	var comp = $(this).closest('.cart');

	if (comp.hasClass('selected')) {
		comp.find('.cart-buttons')
			.hide()
			.end()
			.find('.cart-content-full')
			.hide(400)
			.end()
			.find('.cart-content')
			.show(400)
			.end()
			.removeClass('selected');
		parent.parent().siblings('.carts-header').css('position', 'sticky');
		return;
	}
	parent.parent().siblings('.carts-header').css('position', 'relative');

	parent
		.find('.cart-buttons')
		.hide()
		.end()
		.find('.cart.selected .cart-content-full')
		.hide(400)
		.end()
		.find('.cart-content')
		.show(400)
		.end()
		.find('.cart')
		.removeClass('selected');

	comp.addClass('selected');
	comp.find('.cart-buttons')
		.show()
		.end()
		.find('.cart-content')
		.hide(400)
		.end()
		.find('.cart-content-full')
		.show(400)
		.end();
});

$(document).on('click', '.viac-info', function () {
	if ($(this).parent().hasClass('full-info')) {
		$(this).find('span').text('Viac').end().parent().removeClass('full-info');
		scrollTo($(this).siblings('.carts-cont'));
	} else {
		$(this).find('span').text('Menej').end().parent().addClass('full-info');
	}
});

$('#checkPin').click(function () {
	var pin = '';
	$('.input-pin .input_text').each(function () {
		pin += $(this).val();
	});

	if (pin.length !== 4) return;

	showPopup(getPopupContent('inProgress').content);
	$('.phoneNumber').removeClass('valid');

	$.when(nba(null, pin, nbaVars.src_channel))
		.done(function (data, textStatus, jqXHR) {
			nba_action(data.offer, false);
			showHideDph();
		})
		.fail(function (error, textStatus, errorThrown) {
			$.fancybox.close();
			showPopup(getPopupContent('error').content);
		});
});

$(document).on('change', '.change-dph', function () {
	nbaVars.withDph = $(this).is(':checked');
	showHideDph(400);
	setSwithes();
});

$(document).on('click', '.show-popup', function () {
	var type = $(this).attr('data-type');
	showPopup(getPopupContent(type).content);
});

$(document).on('click', '.reset-form', function () {
	$.when(nba(null, null, null, true))
		.done(function (data, textStatus, jqXHR) {
			localStorage.removeItem('nbaOfferNonHwType');
			nbaVars.nbaOffers.tariffChange = [];
			nbaVars.nbaOffers.nonHw = [];
			$('.overenie').removeClass('show-offer');
			$('.user-offer .carts-cont').removeClass('discount');
			$('.overenie .form').show();
			$('.overenie .result').hide();
			$('.user-offer').hide();
		})
		.fail(function (error, textStatus, errorThrown) {
			$.fancybox.close();
		});
});

$(document).on('click', '.non-hw', function () {
	var type = parseInt($(this).attr('data-type'));

	$('.overenie').addClass('show-offer');
	$('.overenie .form').hide();

	$('.user-offer').show();

	localStorage.setItem('nbaOfferNonHwType', type);
	setUserOffer(type === 1 ? nbaVars.nbaOffers.tariffChange : nbaVars.nbaOffers.nonHw, type);
});

function nba_action(data, afterLoadPage) {
	if (data.verifyStatus === 'INVALID_OTP') {
		showPopup(getPopupContent('wrongPin').content);
		$('.input-pin .input_text').val('');
		return;
	} else if (data.verifyStatus === 'EXPIRED_OTP') {
		showPopup(getPopupContent('EXPIRED_OTP').content);
		$('.input-pin .input_text').val('');
		$('.input-pin').hide();
		$('.input-phone').show();
		return;
	}

	var status = data.verifyStatus;

	if (status === 'SHOW_OFFER') {
		createCookie('customerChecked', 'true', 1);
		if (!data.productOffers.length) {
			status = 'NO-OFFER';
		} else if (typeSegment !== null && data.productOffers[0].segment.toLowerCase() !== typeSegment) {
			status = 'OTHER-SEGMENT';
		}
	}

	if (status === 'SHOW_OFFER') {
		nbaVars.nbaOffers.tariffChange = [];
		nbaVars.nbaOffers.nonHw = [];
		var withMaxList = ['RP1142', 'RP1143', 'RP1144', 'RP1145', 'RP1155', 'RP1157', 'RP1158'];
		for (var offer = 0; offer < data.productOffers.length; offer++) {
			if (
				!nbaVars.nbaOffers.config.hasDiscount &&
				typeof data.productOffers[offer].discountedPrice !== 'undefined'
			) {
				nbaVars.nbaOffers.config.hasDiscount = true;
			}

			if (!nbaVars.nbaOffers.config.withMax && withMaxList.indexOf(data.productOffers[offer].rpCode) > -1) {
				nbaVars.nbaOffers.config.withMax = true;
			}

			if (data.productOffers[offer].transactionType.toUpperCase() === 'TARIFFCHANGE') {
				nbaVars.nbaOffers.tariffChange.push(data.productOffers[offer]);
			} else if (typeof data.productOffers[offer].has_hardware !== 'undefined') {
				nbaVars.nbaOffers.nonHw.push(data.productOffers[offer]);
			}
		}

		if (nbaVars.nbaOffers.nonHw.length) {
			if (nbaVars.nbaOffers.tariffChange.length) {
				status = 'SHOW_OFFER_NON_HW';
			} else {
				status = 'SHOW_OFFER_NON_HW_ONLY';
			}
		} else {
			nbaVars.nbaOffers.tariffChange = [];
		}
	}

	var nonOfferType = 0;
	var showPopupType = status;
	if (status !== 'SHOW_OFFER' && afterLoadPage) {
		nonOfferType = localStorage.getItem('nbaOfferNonHwType');
		nonOfferType = nonOfferType ? parseInt(nonOfferType) : 0;
		if (nonOfferType === 1) showPopupType = 'SHOW_OFFER';
	}

	var popup = getPopupContent(showPopupType);
	var icon = '>';
	//var icon = status === 'SHOW_OFFER' ? '>' : '>';

	if (status !== 'SHOW_OFFER_NON_HW' && status !== 'SHOW_OFFER_NON_HW_ONLY') {
		localStorage.removeItem('nbaOfferNonHwType');
	}

	if (!afterLoadPage || (status === 'SHOW_OFFER_NON_HW' && localStorage.getItem('nbaOfferNonHwType') === null)) {
		showPopup(popup.content);
	}

	if (popup.result && (status !== 'SHOW_OFFER_NON_HW' || (status === 'SHOW_OFFER_NON_HW' && afterLoadPage))) {
		$('.overenie .result')
			.find('.result-icon i')
			.text(icon)
			.end()
			.find('.result-content')
			.html(popup.result)
			.end()
			.show();
	}

	$('.overenie .form')
		.find('.input-pin .input_text')
		.val('')
		.end()
		.find('.input-phone .input_text')
		.val('')
		.end()
		.find('.input-pin')
		.hide()
		.end()
		.find('.input-phone')
		.show()
		.end();

	if (
		status === 'SHOW_OFFER' ||
		status === 'OTHER-SEGMENT' ||
		status === 'SHOW_OFFER_NON_HW_ONLY' ||
		(status === 'SHOW_OFFER_NON_HW' && afterLoadPage)
	) {
		$('.overenie').addClass('show-offer');
		$('.overenie .form').hide();

		if (status === 'SHOW_OFFER' || status === 'SHOW_OFFER_NON_HW' || status === 'SHOW_OFFER_NON_HW_ONLY') {
			$('.user-offer').show();
			var dataToShow = data.productOffers;
			if (status !== 'SHOW_OFFER') {
				if (nonOfferType === 1) dataToShow = nbaVars.nbaOffers.tariffChange;
				else if (nonOfferType === 2) dataToShow = nbaVars.nbaOffers.nonHw;
			}
			setUserOffer(dataToShow, nonOfferType);
		}
	}
}

function setUserOffer(data, type) {
	var segment = '';
	var userOffer = '';

	if (nbaVars.nbaOffers.config.withMax) {
		if (!$('.user-offer .carts-cont').hasClass('with-max')) $('.user-offer .carts-cont').addClass('with-max');
	} else {
		if ($('.user-offer .carts-cont').hasClass('with-max')) $('.user-offer .carts-cont').removeClass('with-max');
	}

	if (nbaVars.nbaOffers.config.hasDiscount && !$('.user-offer .carts-cont').hasClass('discount'))
		$('.user-offer .carts-cont').addClass('discount');
	else if (!nbaVars.nbaOffers.config.hasDiscount && $('.user-offer .carts-cont').hasClass('discount'))
		$('.user-offer .carts-cont').removeClass('discount');

	for (var offer = 0; offer < data.length; offer++) {
		if (offer < 3) {
			for (var all = 0; all < dataRatePlanes.length; all++) {
				if (data[offer].rpCode === dataRatePlanes[all].rpCode) {
					var addon = data[offer].addon ? data[offer].addon : [];
					userOffer += getPausalInfo(dataRatePlanes[all], false, addon, data[offer]);
					segment = dataRatePlanes[all].segment;
				}
			}
		}
	}

	if (data[0]) {
		var free = '';
		var desc = '';
		if (data[0].segment.toLowerCase() === 'b2b') {
			free = 'Nákup z pohodlia firmy';
			if (data[0].transactionType.toUpperCase() === 'TARIFFCHANGE') {
				desc =
					'Na základe vášho mobilného čísla sme z našej širokej ponuky T&nbsp;Biznis paušálov vybrali niekoľko paušálov ' +
					'ušitých na mieru priamo pre vás. Prejsť na T&nbsp;Biznis paušál môžete už teraz, v rámci viazanosti. Vaše telefónne ' +
					'číslo aj doba viazanosti sa zachová. Ak máte k paušálu smartfón alebo iné zariadenie, poplatok zaň ostáva rovnaký.';
			} else {
				desc =
					'Na základe vášho mobilného čísla sme z našej širokej ponuky T&nbsp;Biznis paušálov vybrali niekoľko ' +
					'paušálov ušitých na mieru priamo pre vás. K novému T&nbsp;Biznis paušálu si môžete vybrať aj nový smartfón ' +
					'alebo iné zariadenie s ceľkovou zľavou až do 50&nbsp;€.';
			}

			if (type !== 1 && nbaVars.nbaOffers.nonHw.length) {
				desc =
					'Na základe vášho mobilného čísla sme vybrali niekoľko paušálov ušitých na mieru priamo pre vás. ' +
					'Prejsť na nový paušál so svojím telefónnym číslom môžete už teraz, počas viazanosti. K&nbsp;novému ' +
					'paušálu si môžete vziať smartfón alebo iné zariadenie s&nbsp;online zľavou až do&nbsp;20&nbsp;€. ' +
					'Týmto nákupom vám začne plynúť nová 24-mesačná viazanosť.';
			}
		} else {
			free = '14 dní bez záväzkov';
			if (data[0].transactionType.toUpperCase() === 'TARIFFCHANGE') {
				desc =
					'Na základe vášho mobilného čísla sme z našej širokej ponuky nových T&nbsp;paušálov vybrali niekoľko paušálov ' +
					'ušitých na mieru priamo pre vás. Prejsť na nový T&nbsp;paušál môžete už teraz, v rámci viazanosti. Vaše telefónne ' +
					'číslo aj doba viazanosti sa zachová. Ak máte k paušálu smartfón alebo iné zariadenie, poplatok zaň ostáva rovnaký.';
			} else {
				desc =
					'Na základe vášho mobilného čísla sme z našej širokej ponuky nových T paušálov vybrali niekoľko paušálov ' +
					'ušitých na mieru priamo pre vás. K novému T paušálu si môžete vybrať aj smartfón alebo iné zariadenie ' +
					's celkovou zľavou až do&nbsp;50&nbsp;€.';
			}

			if (type !== 1 && nbaVars.nbaOffers.nonHw.length) {
				desc =
					'Na základe vášho mobilného čísla sme vybrali niekoľko paušálov ušitých na mieru priamo pre vás. ' +
					'Prejsť na nový paušál so svojím telefónnym číslom môžete už teraz, počas viazanosti. K&nbsp;novému ' +
					'paušálu si môžete vziať smartfón alebo iné zariadenie s&nbsp;online zľavou do&nbsp;20&nbsp;€. ' +
					'Týmto nákupom vám začne plynúť nová 24-mesačná viazanosť.';
			}
		}

		if (type > 0) {
			var popup = getPopupContent(type === 1 ? 'SHOW_OFFER' : 'SHOW_OFFER_NON_HW');
			$('.overenie .result')
				.find('.result-icon i')
				.text('>')
				.end()
				.find('.result-content')
				.html(popup.result)
				.end()
				.show();
		}

		$('.user-offer .hero-pausaly-title .icon-free .i-ch').text(free);
		$('.user-offer .hero-pausaly-title .desc').html(desc).show();
	}

	if (segment.length) {
		$('.user-offer')
			.removeClass('seg-b seg-c')
			.addClass('seg-' + segment.substr(-1));
	}

	if ($(window).width() > 767) {
		if ($('#offer-slider').hasClass('slick-initialized')) {
			$('#offer-slider').slick('removeSlide', null, null, true);
		} else {
			initSlider('offer-slider');
		}
		$('#offer-slider').slick('slickAdd', userOffer);
	} else {
		$('#offer-slider').html(userOffer);
	}
	addToolTip('.user-offer');
}

function initSlider(slider) {
	$('#' + slider).slick({
		slidesToShow: 4,
		slidesToScroll: 1,
		arrows: true,
		infinite: false,
		swipeToSlide: true,
		swipe: true,
		touchMove: true,
		draggable: true,
		dots: true,
		responsive: [
			{
				breakpoint: 1270,
				settings: {
					slidesToShow: 3,
				},
			},
			{
				breakpoint: 1000,
				settings: {
					slidesToShow: 2,
				},
			},
			{
				breakpoint: 767,
				settings: 'unslick',
			},
		],
	});
}

function addToolTip(parent) {
	$(parent)
		.find('.carts .info')
		.each(function () {
			$(this).qtip({
				content: {
					button: 'Close',
					text: $(this).attr('alt'),
				},
				style: {
					classes: 'qtip-new',
				},
				position: {
					corner: {
						target: 'leftMiddle',
						tooltip: 'topMiddle',
					},
					viewport: $(window),
				},
			});
		});
}

function showHideDph(time) {
	if (nbaVars.withDph === true) {
		$('.cart .without-dph').hide(time);
		$('.cart .with-dph').show(time);
	} else {
		$('.cart .with-dph').hide(time);
		$('.cart .without-dph').show(time);
	}
}

function setSwithes() {
	$('.change-dph').each(function () {
		if ($(this).is(':checked') !== nbaVars.withDph) {
			$(this).trigger('click');
		}
	});
}

/* beautify preserve:start */
// prettier-ignore

function getPopupContent(type) {
    var content = '';
    var result = '';
    var phoneNum = $("#phoneNumber").val();
    switch (type) {
        case 'inProgress':
            content = '<div class="p-content">' +
                    '<div class="popup-lightbox">' +
                        '<div class="loader-spinning">' +
                            '<i class="t-icon--magenta d-block s3">o</i>' +
                        '</div>' +
                        '<h4 class="headline">Prebieha overovanie Vášho čísla. Ďakujeme za trpezlivosť.</h4>' +
                    '</div>' +
                '</div>';
            break;

        case 'NO-OFFER':
            if (typeSegment !== null) {
                content = '<div class="p-content">' +
                        '<div class="popup-lightbox">' +
                            '<div class="cont-icon"><i class="t-icon t-info">&gt;</i></div>' +
                            '<h4 class="headline">Overenie prebehlo úspešne. Pozrite si ponuku vybraných paušálov, ktoré odporúčame.</h4>' +
                            '<a href="#" class="btn btn_mag-outline popup-close" data-scroll="odporucame">Pozrieť odporúčané paušály</a>' +
                        '</div>' +
                    '</div>';
                result = 'Pre číslo <b>' + phoneNum + '</b> momentálne nemáme špeciálnu ponuku na mieru. Pozrite si vybrané paušály, ktoré odporúčame.';
            } else {
                content = '<div class="p-content">' +
                        '<div class="popup-lightbox">' +
                            '<img src="../assets/img/verify_failed.png" />' +
                            '<h4 class="headline">Pre číslo ' + phoneNum + ' momentálne nemáme špeciálnu ponuku na mieru.</h4>' +
                            '<a href="#" class="btn btn_mag-outline popup-close">Pokračovať</a>' +
                        '</div>' +
                    '</div>';
            }
            break;

        case 'SHOW_OFFER':
            content = '<div class="p-content">' +
                    '<div class="popup-lightbox">' +
                        '<img src="../assets/img/verify_success.png" />' +
                        '<h4 class="headline">Overenie prebehlo úspešne, pozrite si ponuku šitú na mieru pre Vás.</h4>' +
                        '<a href="#" class="btn btn_mag-outline popup-close">Pokračovať</a>' +
                    '</div>' +
                '</div>';
            result = 'Overenie prebehlo úspešne, pozrite si ponuku šitú na mieru pre toto číslo ' +
                '<p class="num">' + phoneNum + '</p>' +
                '<p>' +
                    '<a class="magenta cursor-p show-popup" data-type="otherNumber">' +
                        'Overiť iné číslo ' +
                        '<i class="t-icon--magenta" >e</i>' +
                    '</a>' +
                '</p>';
            break;

        case 'SHOW_OFFER_NON_HW':
            content =
				'<div class="p-content">' +
				    '<div class="popup-lightbox">' +
				        '<img src="../assets/img/verify_success.png" />' +
				        '<p><b>Overenie prebehlo úspešne.</b></p>' +
				        '<h4 class="headline">Prejdite na nový paušál bez zmeny viazanosti, alebo si k paušálu vezmite aj mobil či iné zariadenie s&nbsp;novou 24&#8722;mesačnou viazanosťou.</h4>' +
                        '<a href="#" class="btn btn_mag-outline non-hw popup-close" data-type="1">Chcem iba paušál</a>' +
                        '<a href="#" class="btn cta_mag non-hw popup-close" data-type="2">Chcem aj nový mobil</a>' +
				    '</div>' +
				'</div>';
			result = 'Overenie čísla prebehlo úspešne. Zvolili ste si možnosť k novému paušálu vybrať aj mobil alebo iné zariadenie s novou 24&#8722;mesačnou viazanosťou.' +
				    '<p class="num">' +
				        phoneNum +
				    '</p>' +
				    '<p>' +
				        '<a class="magenta cursor-p show-popup" data-type="otherNumber">' +
				            'Overiť iné číslo ' +
				            '<i class="t-icon--magenta" >e</i>' +
				        '</a>' +
                    '</p>';
            break; 
            
        case 'SHOW_OFFER_NON_HW_ONLY':
            content =
				'<div class="p-content">' +
				    '<div class="popup-lightbox">' +
				        '<img src="../assets/img/verify_success.png" />' +
				        '<p><b>Overenie prebehlo úspešne.</b></p>' +
				        '<h4 class="headline">Nový mobil či iné zariadenie si k vášmu číslu môžete zobrať už teraz. Vaša aktuálna viazanosť sa nahradí novou. </h4>' +
                        '<a href="#" class="btn btn_mag-outline popup-close">Chcem nový mobil</a>' +
				    '</div>' +
				'</div>';
			result = 'Overenie čísla prebehlo úspešne. Zvolili ste si možnosť k novému paušálu vybrať aj mobil alebo iné zariadenie s novou 24&#8722;mesačnou viazanosťou.' +
				    '<p class="num">' +
				        phoneNum +
				    '</p>' +
				    '<p>' +
				        '<a class="magenta cursor-p show-popup" data-type="otherNumber">' +
				            'Overiť iné číslo ' +
				            '<i class="t-icon--magenta" >e</i>' +
				        '</a>' +
                    '</p>';
            break;            

        case "otherNumber":
            content = '<div class="p-content">' +
                    '<div class="popup-lightbox">' +
                        '<div class="iconDone"><i class="t-icon--magenta--border s3">ä</i></div>' +
                        '<h4 class="headline">Ste si istý, že chcete overiť iné číslo?</h4>' +
                        '<p class="text">Overením iného čísla sa váš nákupný košík vyprázdni a ponuka vynuluje.</p>' +
                        '<a href="#" class="btn cta_mag popup-close reset-form">Overiť iné číslo</a>' +
                        '<a href="#" class="btn btn_mag-outline popup-close" >Zavrieť okno</a>' +
                    '</div>' +
                '</div>';
            result = null;
            break;

        case 'wrongPin':
            content = '<div class="p-content">' +
                    '<div class="popup-lightbox">' +
                        '<img src="../assets/img/re-enter.png" />' +
                        '<h4 class="headline">Ľutujeme, zadaný kód nie je správny.</h4>' +
                        '<p class="text">Skúste ho zadať znova, prosím.</p>' +
                        '<a href="#" class="btn btn_mag-outline popup-close-focus">Zadať kód znova</a>' +
                    '</div>' +
                '</div>';
            result = ''
            break;

        case 'EXPIRED_OTP':
            content = '<div class="p-content">' +
                    '<div class="popup-lightbox">' +
                        '<img src="../assets/img/verify_failed.png" />' +
                        '<h4 class="headline">Ľutujeme, zadaný kód nie je správny.</h4>' +
                        '<p class="text">Zadaný kód z SMS už nie je aktuálny. Zadajte telefónne číslo znova</p>' +
                        '<a href="#" class="btn btn_mag-outline popup-close-focus">Pokračovať</a>' +
                    '</div>' +
                '</div>';
            result = 'Ľutujeme, Váš kód z SMS už nie je aktuálny. Zadajte telefónne číslo znova.'
            break;

        case 'OTHER-SEGMENT':
            if (typeSegment === 'b2c') {
                content = '<div class="p-content">' +
                        '<div class="popup-lightbox">' +
                            '<img src="../assets/img/verify_success.png" />' +
                            '<h4 class="headline">Overenie prebehlo úspešne, pozrite si ponuku šitú na mieru pre Váš biznis.</h4>' +
                            '<a href="/biznis/pausal" class="btn cta_mag">Pozrieť ponuku pre Váš biznis</a>' +
                            '<a href="#" class="btn btn_mag-outline popup-close">Ostať na ponuke pre Vás</a>' +
                        '</div>' +
                    '</div>';
                result = 'Overenie prebehlo úspešne, pozrite si ponuku šitú na mieru pre číslo ' +
                    '<b>' + phoneNum + '</b>' +
                    ' v časti webu pre ' +
                    '<a href="/biznis/pausal" class="magenta cursor-p">Váš biznis</a>.' +
                    '<p>' +
                        '<a class="magenta cursor-p show-popup" data-type="otherNumber">' +
                            'Overiť iné číslo ' +
                            '<i class="t-icon--magenta" >e</i>' +
                        '</a>' +
                    '</p>';
            } else {
                content = '<div class="p-content">' +
                        '<div class="popup-lightbox">' +
                            '<img src="../assets/img/verify_success.png" />' +
                            '<h4 class="headline">Máme pre Vás ponuku šitú na mieru. Pre zobrazenie ponuky pokračujte overením v e-shope Telekom pre nefiremných zákazníkov.</h4>' +
                            '<a href="https://eshop.telekom.sk/category/pausaly-telekom/list/product_listing?productOfferingTerm=agreement24&loginStep=enterCredentials" class="btn cta_mag">Pozrieť ponuku pre Vás</a>' +
                            '<a href="#" class="btn btn_mag-outline popup-close">Ostať na ponuke pre Váš biznis</a>' +
                        '</div>' +
                    '</div>';
                result = 'Overenie prebehlo úspešne. Pre zobrazenie ponuky pokračujte overením čísla ' +
                    '<b>' + phoneNum + '</b>' +
                    ' v sekcii pre ' +
                    '<a href="https://eshop.telekom.sk/category/pausaly-telekom/list/product_listing?productOfferingTerm=agreement24&loginStep=enterCredentials" class="magenta cursor-p">Vás</a>.' +
                    '<p>' +
                        '<a class="magenta cursor-p show-popup" data-type="otherNumber">' +
                            'Overiť iné číslo ' +
                            '<i class="t-icon--magenta" >e</i>' +
                        '</a>' +
                    '</p>';
            }
            break;

        default:
            content = '<div class="p-content">' +
                    '<div class="popup-lightbox">' +
                        '<img src="../assets/img/verify_failed.png" />' +
                        '<h4 class="headline">Ľutujeme, Vaše číslo nebolo možné overiť.</h4>' +
                        '<p class="text"><b>Skúste ho overiť neskôr</b> alebo si pozrite našu štandardnú ponuku.</p>' +
                        '<a href="#" class="btn btn_mag-outline popup-close">Pokračovať</a>' +
                    '</div>' +
                '</div>';
            result = 'Vaše číslo <b>' + phoneNum + '</b> nebolo možné z technických príčin overiť. Skúste to neskôr.'
            break;
    }

    return {
        content: content,
        result: result
    };
}

// prettier-ignore

function getPausalInfo(data, isHero, addon, dataOffer) {
    var infoVoiceM1 = 'Viac volaní získate, ak so svojím paušálom založíte alebo sa pridáte do bezplatnej skupiny Magenta 1, v ktorej je aspoň jedna zo služieb Magio internet, TV alebo Pevná linka M, L alebo XL.';
    var infoSmsM1 = 'Viac SMS a MMS získate, ak so svojím paušálom založíte alebo sa pridáte do bezplatnej skupiny Magenta 1, v ktorej je aspoň jedna zo služieb Magio internet, TV alebo Pevná linka M, L alebo XL.';

    var addons = data.darcek.concat(addon);
    var volania = '<div>Nekonečné</div>';
    var userSwitch = $('.user-offer .dph-switch');
    if (data.segment === 'b2c' && userSwitch.is(':visible')) {
        userSwitch.hide();
    } else if (data.segment === 'b2b' && !userSwitch.is(':visible')) {
        userSwitch.show();
    }

    var cena_sms_bez = typeof data.sms_bez !== 'undefined' ? data.sms_bez[0] : data.sms[0];

    if (data.volania[0] !== 'Nekonečné' || data.sms[0] !== 'Nekonečné') {
        volania =
            '<div>' +
                data.volania[0] +
                '<span class="gray-small"> ' + data.volania[1] + '</span>' +
            '</div>' +

            '<div >' +
            (typeof data.volaniaM1 !== 'undefined' ?
                '<div class="mag1-small">' +
                data.volaniaM1[0] + 
                '<span class="gray-small"> ' + data.volaniaM1[1] + ' s M1 </span>'+ 
                '<div class="t-typ-info gray">' +
                    '<i class="t-icon info" alt="' 
                    + infoVoiceM1 +
                    '">></i>' +
                    '</div>' +
                '</div>': "")  +
            '</div>'+


            '<div class="cena-sms">' +
                (data.sms[1] !== 'SMS'
                    ? '<span class="with-dph">' + data.sms[0] + '</span>' + '<span class="without-dph">' + cena_sms_bez + '</span>'
                    : data.sms[0]) +
                '&nbsp;<span class="gray-small"> ' + data.sms[1] + '</span>' +
            '</div>'+
            '<div>'+
            (typeof data.smsM1 !== 'undefined' ?
               '<div class="mag1-small">' +
               data.smsM1[0] + 
               '<span class="gray" > ' + data.smsM1[1] + ' s M1 </span>'+ 
               '<div class="t-typ-info gray">' +
                   '<i class="t-icon info" alt="' 
                   + infoSmsM1 +
                   '">></i>' +
                   '</div>' +
               '</div>': "") +
           '</div>';
    }

    var non_hw = false;
    if (dataOffer &&
        dataOffer.transactionType === 'PROLONGATION' && 
        typeof dataOffer.has_hardware !== 'undefined'         
        )
        non_hw = true;

    var urlPausal = getPath(data.segment, false);

    var webZlava = 20;
    // var webZlavy = {
    //     30: ['RP1140', 'RP1141', 'RP1142', 'RP1143'],
    //     50: ['RP1144', 'RP1145'],
    // };

    // Object.keys(webZlavy).forEach(function (key) {
    //     var index = webZlavy[key].indexOf(data.rpCode);
    //     if (webZlava === 20 && index > -1) webZlava = key;
    // });

    var cena_discount = null;
    var cena_discount_bez = null;
    var cena_bez = data.segment === 'b2b' ? data.cena_bez : data.cena;
    if (dataOffer) {
        if (typeof dataOffer.discountedPrice !== 'undefined') {
            cena_discount = dataOffer.discountedPrice.toFixed(2).replace('.', ',');
            if (data.segment === 'b2b') {
                cena_discount_bez = (dataOffer.discountedPrice / 1.2).toFixed(2).replace('.', ','); 
            }
        }
        if (dataOffer.transactionType === 'TARIFFCHANGE') {
            urlPausal = (data.segment === 'b2b' ? '/biznis' : '');
            urlPausal += '/volania/zmena-pausalu?id=' + dataOffer.uacId + '&rpCode=' + data.rpCode;
        } else {
            urlPausal += 'uac=' + dataOffer.uacId;
        }
    } else {
        urlPausal += 'tariff=' + data.rpCode;
    }

    //tooltips
    var infoM1 = (data.segment === 'b2b' ?
        'Viac dát získate, ak máte T Biznis paušál a založíte si alebo sa pridáte do skupiny Magenta 1 Biznis, v ktorej je aspoň jedna zo služieb Magio internet, TV alebo Pevná linka.'
        :
        'Viac dát získate, ak so svojím T paušálom založíte alebo sa pridáte do bezplatnej skupiny Magenta 1, v ktorej je aspoň jedna zo služieb Magio internet, TV alebo Pevná linka M, L alebo XL.'
    );
    
    var tooltip_magio_go = '<p class=\'title\'>Magio GO Benefit</p><p>Sledujte vo vašom mobile či tablete atraktívne športové zostrihy, kvalitné dokumenty z BBC Earth, alebo rozprávky z exkluzívneho obsahu Ťuki kdekoľvek sa nachádzate. K tomu pridáme aj 10 kanálov online TV so živým vysielaním a 7-dňovým archívom.</p>';
    var tooltip_t_master = 'Užívajte si prémiovú starostlivosť s osobným agentom, ktorý vám poskytne poradenstvo, konzultácie a vyrieši vaše problémy. T-Master sa postará o to, aby vaša firma dostala ponuku služieb, ktorú potrebujete.';

    var infoNekPrip = 'S bezplatnou službou Nekonečné pripojenie si obnovíte rýchlosť internetu v mobile až do 128 kb/s po tom, čo vyčerpáte voľné dáta zo svojho paušálu. Službu Nekonečné pripojenie si aktivujte jednoducho v Telekom aplikácii.';
    var tooltip_discount =
        "<p class='title'>Zľavy na nový mobil, tablet alebo iné zariadenie</p>" +
        "<p class='title2'>Vernostná zľava</p>" +
        "<p>" +
            "Ako náš verný zákazník získate pri predĺžení " +
            (data.segment === 'b2b' ? "Biznis " : '') +
            "paušálu zľavu až do 30&nbsp;€ na nový mobil, tablet alebo iné zariadenie, ktoré si k paušálu vyberiete." +
        "</p>" +
        "<p class='title2 m-t-5'>Online zľava " + webZlava + "&nbsp;€</p>" +
        "<p>Navyše, ak si tento paušál kúpite cez web, získate na nové zariadenie ďalšiu zľavu " + webZlava + "&nbsp;€.</p>";
    
    var infoNekHDZadarmo = null;
    if (2===3 && data.rpCode !== 'RP1142' && data.rpCode !== 'RP1143' && data.rpCode !== 'RP1144' && data.rpCode !== 'RP1145' && 
        data.rpCode !== 'RP1155' && data.rpCode !== 'RP1157' && data.rpCode !== 'RP1158') {
        infoNekHDZadarmo = 
            "<p class='title'>Dátový balík Nekonečno HD zadarmo</p>" +
            "<p>" +
                "S dátovým balíkom Nekonečno HD na 30 dní zadarmo dátujete aj po prečerpaní dát zo svojho paušálu " +
                "rýchlosťou 10 Mbps.<br />" +
                "Balík získa každý, kto si kúpi alebo prejde na nový Telekom paušál.<br /><br />" +
            "</p>" +
            "<p><b>Členovia skupiny Magenta 1 získajú balík zadarmo až na 90 dní.</b></p>";
    }

    var cart =
        '<div class="cart' +
            (!isHero && data.hero ? ' hero' : '') +
            '" ' +
            'data-id="' + data.id + '" ' +
            'data-rp="' + data.rpCode +
        '">' +
            '<div class="cart-heading">' +
                '<div class="hero sh-d">' +
                    (!isHero && data.hero ? '<span>Odporúčame</span>' : '') +
                '</div>' +
                '<div class="name">' +
                    data.name +
                '</div>' +
                (!isHero && data.hero ? '<div class="hero sh-m"><span>Odporúčame</span></div>' : '') +
                '<div class="sh-d cena">' +
                    '<div class="sum">' +
                        (cena_discount && data.cena != cena_discount ?
                            '<div>' +
                                '<span class="with-dph">' + cena_discount + '</span>' +
                                '<span class="without-dph">' + cena_discount_bez + '</span>' +
                                '&nbsp;€ / mes.' +
                            '</div>' +
                            '<div>' +    
                                '<span class="with-dph line-through-m">' + data.cena + '</span>' + 
                                '<span class="without-dph line-through-m">' + cena_bez + '</span>' +
                                '<span class="line-through-m">&nbsp;€ / mes.</span>' +
                            '</div>'
                            :
                            '<div>' +
                                '<span class="with-dph">' + data.cena + '</span>' +
                                '<span class="without-dph">' + cena_bez +'</span>' +
                                '&nbsp;€ / mes.' +
                            '</div>'    
                        ) +
                    '</div>' +
                    (data.segment === 'b2b'
                        ? '<div class="dph-desc dph-cont">' +
                            '<span class="desc with-dph">s</span>' +
                            '<span class="desc without-dph">bez</span>' +
                            '<span class="desc">&nbsp;DPH</span>' +
                        '</div>'
                        : ''
                    ) +
                '</div>' +
                '<div class="viazanost f-i">pri 24 mesačnej viazanosti</div>' +
                '<div class="web-zlava">' +
                    (dataOffer === null || dataOffer.transactionType !== 'TARIFFCHANGE' ?
                        '<span>zľavy na zariadenie</span>' +
                        '<span class="info" alt="' + tooltip_discount + '">' +
                            '<img class="info img-mag" src="../assets/img/lp_percent.png" /> ' +
                            '<img class="info img-white" src="../assets/img/lp_percent_selected.png" />' +
                        '</span>'
                        : '&nbsp;') +
                '</div>' +
            '</div>' +
            '<div class="cart-content">' +
                '<div class="cart-content-data w-1">' +
                    '<div class="cart-content-data-1"'+
                        (typeof data.tooltips.nekonecno !== 'undefined' && data.rpCode !== 'RP1145' && data.rpCode !== 'RP1158' ?
                            ' style="border-bottom: none;"' : '') +
                        '>' +
                        '<div class="gray f-i">Dáta v SR a EÚ+</div>' +
                        (typeof data.tooltips.nekonecno === 'undefined' ?
                            '<div class="f-20">' +
                                data.data[0] +
                                ' <span class="gray">' + data.data[1] + '</span>' +
                            '</div>' : '') +
                        (typeof data.tooltips.nekonecno !== 'undefined' ?
                            '<div class="nekonecno">' +
                                '<div class="inline">' +
                                    '<img class="nek sh-d" src="../assets/img/osmicka_zavrety-pausal_1366px.png" />' +
                                    '<img class="nek-f sh-d" src="../assets/img/osmicka_otvoreny-pausal_1366px.png" />' +
                                    '<img class="nek sh-m" src="../assets/img/osmicka_zavrety-pausal_360px.png" />' +
                                '</div>' +
                                '<div class="t-typ-info text-right tt-menej">&nbsp;' +
                                    '<i class="t-icon info gray" alt="' + data.tooltips.nekonecno + '">></i>' +
                                '</div>' +
                                (data.rpCode === 'RP1145' || data.rpCode === 'RP1158' ?
                                    '<div class="t-typ-info sh-d text-right tt-viac">&nbsp;' +
                                        '<i class="t-icon info gray" alt="' + data.tooltips.nekonecno + '">></i>' +
                                    '</div>' : '') +
                            '</div>'
                            : '') +
                        (typeof data.tooltips.nek_prip !== 'undefined' ?
                            '<div class="nek-prip magenta">' +
                                '<div>+&nbsp;</div>' +
                                '<div>' +
                                    'Nekonečné pripojenie&nbsp;' +
                                '</div>' +
                                '<div class="t-typ-info text-right">' +
                                    '<i class="t-icon info gray" alt="' + infoNekPrip + '">></i>' +
                                '</div>' +
                            '</div>' :
                            '') +
                        (infoNekHDZadarmo ?
                            '<div class="nek-hd-zadarmo">' +
                                '<div class="text">' +
                                    '+ dátový balík<br >' +
                                    'Nekonečno HD<span> zadarmo</span>' +
                                '</div>' +
                                '<div class="t-typ-info">' +
                                    '<i class="t-icon info gray" alt="' + infoNekHDZadarmo + '">></i>' +
                                '</div>' +
                            '</div>' :
                            '') +
                            
                            
                            '<div class="data-zona2 f-i">' +
                        (data.rpCode === 'RP1145' || data.rpCode === 'RP1158' ?
                        
                            '<div class="gray f-i">Dáta v zóne 2</div>' +
                            '<div class="f-20">' +
                                '500 <span class="gray">MB</span>' +
                            '</div>'
                            : '') +
                        (typeof data.tooltips.nekonecno2 !== 'undefined' ?
                            '<div class="f-20">' +
                                data.data[0] + ' <span class="gray">' + data.data[1] + '</span>' +
                                ' <i class="t-icon info gray" alt="' + data.tooltips.nekonecno2 + '">></i>' +
                            '</div>' +
                            '<div class="gray f-i">plnou rýchlosťou</div>'
                            : '') +
                        '</div>' +
                    '</div>' +
                    '<div class="cart-content-data-m1-f-i f-i magenta">' +
                        '<div>S Magentou 1 <div class="t-typ-info">' +
                            '<i class="t-icon info magenta" alt="' +
                            (typeof data.tooltips.m1 !== 'undefined' ? data.tooltips.m1 : infoM1) +
                            '">></i>' +
                        '</div>' +
                    '</div>' +
                    '<div class="with-m1 f-20">' +
                        '<b>' + data.dataM1[0] + '</b> ' +
                        data.dataM1[1] +
                    '</div>' +
                '</div>' +
                (typeof data.tooltips.nekonecno === 'undefined' || data.rpCode === 'RP1145' || data.rpCode === 'RP1158' ?
                    '<div class="cart-content-data-m1 s-i">' +
                        '<div>' +
                            '<div class="inline">' +
                                data.dataM1[0] +
                            '</div> ' +
                            '<div class="inline gray"> ' +
                                data.dataM1[1] +
                            '</div>' +
                            '<div class="with-m1 gray">s M1</div>' +
                        '</div>' +
                        '<div class="t-typ-info">' +
                            '<i class="t-icon gray info" alt="' +
                                (typeof data.tooltips.m1 !== 'undefined' ? data.tooltips.m1 : infoM1) +
                            '">></i>' +
                        '</div>' +
                    '</div>' : '') +
            '</div>' +
            '<div class="cart-content-volania w-2">' +
                '<div class="s-i">' +
                    volania +
                '</div> ' +
                '<div class="f-i">' +
                    '<div>' +
                        '<p>Volania v SR a EÚ+</p>' + 
                        
                        '<div>' +
                            data.volania[0] +
                            '<span class="gray"> ' + data.volania[1] + '</span>' +
                        '</div>' +
                        
                        '<div class="cart-content-volania-m1 magenta">' +
                        (typeof data.volaniaM1 !== 'undefined' ?
                            '<div class="mag1">' +
                             'S Magentou 1 '+  
                            '<div class="t-typ-info">' +
                                '<i class="t-icon info magenta" alt="' 
                                + infoVoiceM1 +
                                '">></i>' +
                                '</div>' +
                            '</div>' +
                            '<div class="with-m1 f-20">' +
                            '<b>' + data.volaniaM1[0] + '</b> ' + data.volaniaM1[1] +
                            '</div>': "")  +
                        '</div>'+

                    '</div>' +
                    '<div>' +
                        '<p>SMS a MMS v SR v EÚ+</p>' +
                        
                        '<div class="cena-sms">' +
                            (data.sms[0] !== 'Nekonečné'
                                ? '<span class="with-dph">' + data.sms[0] + '</span>' +
                                '<span class="without-dph">' + cena_sms_bez + '</span>' +
                                '&nbsp;<span class="gray">' + data.sms[1] + '</span>&nbsp;' +
                                (data.sms[1] !== 'SMS'
                                    ? '<span class="gray with-dph dph-cont">s</span>' +
                                        '<span class="gray without-dph dph-cont">bez</span>' +
                                        '<span class="gray dph-cont">&nbsp;DPH</span>'
                                    : '')
                                : data.sms[0]) +
                                
                        '</div>' +
                        '<div class="cart-content-data-m1-f-i magenta">' +
                        (typeof data.smsM1 !== 'undefined' ?
                            '<div class="mag1">' +
                             'S Magentou 1 '+  
                            '<div class="t-typ-info">' +
                                '<i class="t-icon info magenta" alt="' 
                                + infoSmsM1 +
                                '">></i>' +
                                '</div>' +
                            '</div>' +
                            '<div class="with-m1 f-20">' +
                            '<b>' + data.smsM1[0] + '</b> ' + data.smsM1[1] +
                            '</div>': "")  +
                        '</div>'+
                    '</div>' +
                    '<div class="">' +
                        (data.rpCode === 'RP1145' || data.rpCode === 'RP1158' ?
                            '<p>zo SR do zóny 2</p>' +
                            '<div>' +
                                '100 <span class="gray">SMS/MMS</span>' +
                            '</div>'
                            : '') +
                    '</div>' +
                '</div>' +
            '</div>' +
            '<div class="cart-content-darcek w-3">';

    cart += '<div class="addon-icon">';

    if (addons.length) {
        for (var a = 0; a < addons.length; a++) {
            var tooltip = addons[a].name;
            var icon = 'default';

            switch (addons[a].code) {
                case 'm-go':
                    icon = addons[a].code;
                    tooltip = tooltip_magio_go;
                    break;
                case 't-master':
                    icon = addons[a].code;
                    tooltip = tooltip_t_master;
                    break;
                default:
                    tooltip = addons[a].tooltip ? addons[a].tooltip : addons[a].name;
            }

            cart += '<img class="info" src="../assets/img/addon-' + icon + '.png" alt="' + tooltip + '" />';
        }
    }

    cart += '</div>';

    cart +=
            '</div>' +
            '<div class="cart-content-cena w-4 sh-m">' +
                '<div class="sum">' +
                    (cena_discount && data.cena != cena_discount ?
                        '<div>' +
                            '<span class="with-dph">' + cena_discount + '</span>' +
                            '<span class="without-dph">' + cena_discount_bez + '</span>' +
                            '<span class="eur">€</span>' +
                        '</div>' +
                        '<div>' +
                            '<span class="with-dph line-through-m">' + data.cena + '</span>' +
                            '<span class="without-dph line-through-m">' + cena_bez + '</span>' +
                            '<span class="eur gray">€</span>' +
                        '</div>'
                    :
                        '<div>' +
                            '<span class="with-dph">' + data.cena + '</span>' +
                            '<span class="without-dph">' + cena_bez + '</span>' +
                            '<span class="eur">€</span>' +
                        '</div>'
                    ) +
                '</div>' +
                '<div class="gray desc dph-cont">' +
                    '<span class="with-dph">s</span>' +
                    '<span class="without-dph">bez</span>' +
                    '<span>&nbsp;DPH</span>' +
                '</div>' +
            '</div>' +
        '</div>' +
        '<div class="cart-content-full">' + // mobil
            '<div class="cart-sec clearfix">' +
                '<div class="row">' +
                    '<div class="col-xs-7">' +
                        'Dáta v SR a EÚ+' +
                        (typeof data.tooltips.nek_prip !== 'undefined' ?
                            '<p>' +
                                '<b>+ Nekonečné pripojenie </b>' +
                                '<i class="t-icon info" alt="' + infoNekPrip + '">></i>' +
                            '</p>'
                            : '') +
                        (infoNekHDZadarmo ?
                            '<p>' +
                                '<span style="color: #7c7c7c;font-weight: 800;">+ dátový balík Nekonečno HD zadarmo </span>' +
                                '<i class="t-icon info" alt="' + infoNekHDZadarmo + '">></i>' +
                            '</p>'
                            : ''
                        ) +
                    '</div>' +
                    '<div class="col-xs-5 text-right">' +
                        (typeof data.tooltips.nekonecno !== 'undefined' ?
                            '<i class="t-icon info gray" alt="' +
                                (typeof data.tooltips.nekonecno2 !== 'undefined' ? data.tooltips.nekonecno2 : data.tooltips.nekonecno) +
                            '">></i> ' +
                            '<div class="t-typ-info">' +
                                '<img class="nek sh-m" src="../assets/img/osmicka_otvoreny-pausal_360px.png" />' +
                            '</div>'
                            : '') +
                        (typeof data.tooltips.nekonecno === 'undefined' ?
                            data.data[0] + ' ' +
                            '<span class="gray">' + data.data[1] + '</span>' : ''
                        ) +
                    '</div>' +
                '</div>' +
                (data.rpCode === 'RP1145' || data.rpCode === 'RP1158' ?
                    '<div class="row">' +
                        '<div class="col-xs-7">' +
                            'Dáta v zóne 2' +
                        '</div>' +
                        '<div class="col-xs-5 text-right">' +
                            '500 <span class="gray">MB</span>' +
                        '</div>' +
                    '</div>'
                    : '') +
                '<div class="row">' +
                    '<div class="col-xs-7 magenta">' +
                        'S Magentou 1 ' +
                        '<div class="t-typ-info">' +
                            '<i class="t-icon info magenta" alt="' +
                                (typeof data.tooltips.m1 !== 'undefined' ? data.tooltips.m1 : infoM1) +
                            '">></i>' +
                        '</div>' +
                    '</div>' +
                    '<div class="col-xs-5 text-right magenta">' +
                        data.dataM1[0] + ' ' +
                        '<span class="gray">' + data.dataM1[1] + '</span>' +
                    '</div>' +
                '</div>' +
            '</div>' +
            '<div class="cart-sec clearfix">' +
                '<div class="row">' +
                    '<div class="col-xs-7">' +
                        'Volania v SR a EÚ+' +
                        
                    '</div>' +
                    '<div class="col-xs-5 text-right">' +
                        (data.volania === null ?
                            'Nekonečné'
                            : data.volania[0] + ' ' + '<span class="gray">' + data.volania[1] + '</span>') +
                    '</div>' +
                '</div>' +


                (typeof data.volaniaM1 !== 'undefined' ?
                '<div class="row">' +
                    '<div class="col-xs-7 magenta">' +     
                        'S Magentou 1 ' +
                        '<div class="t-typ-info">' +
                            '<i class="t-icon info magenta" alt="' +
                            infoVoiceM1 +
                            '">></i>' +
                        '</div>' +
                    '</div>' +
                    '<div class="col-xs-5 text-right magenta">' +
                        data.volaniaM1[0] + ' ' +
                        '<span class="gray">' + data.volaniaM1[1] + '</span>' +
                    '</div>' +
                '</div>' : '') +


            '</div>' +

            '<div class="cart-sec clearfix">' +
                '<div class="row">' +
                    '<div class="col-xs-7">' +
                        'SMS a MMS v SR v EÚ+' +
                        
                    '</div>' +
                    '<div class="col-xs-5 text-right">' +
                        '<div class="cena-sms">' +
                            (data.sms[0] !== 'Nekonečné'
                                ?   '<div>' +
                                        '<span class="with-dph">' + data.sms[0] + '</span>' +
                                        '<span class="without-dph">' + cena_sms_bez + '</span>&nbsp;' +
                                        '<span class="gray">' + (data.sms[1] !== 'SMS' ? '€' : data.sms[1]) + '</span>' +
                                    '</div>' +
                                    (data.sms[1] !== 'SMS'
                                        ? '<div class="dph-cont">' +
                                            '<span class="gray with-dph">s</span>' +
                                            '<span class="gray without-dph">bez</span>' +
                                            '<span class="gray">&nbsp;DPH</span>' +
                                        '</div>'
                                        : '')
                                : data.sms[0]) +
                        '</div>' +
                    '</div>' +
                    (data.rpCode === 'RP1145' || data.rpCode === 'RP1158' ?
                        '<div class="col-xs-7">' +
                            '<p>zo SR do zóny 2</p>' +
                        '</div>' +
                        '<div class="col-xs-5 text-right">' +
                            '100 <span class="gray">SMS/MMS</span>' +
                        '</div>'
                        : '') +
                '</div>' +

                  
                (typeof data.smsM1 !== 'undefined' ?
                '<div class="row">' +
                    '<div class="col-xs-7 magenta">' +     
                        'S Magentou 1 ' +
                        '<div class="t-typ-info">' +
                            '<i class="t-icon info magenta" alt="' +
                            infoSmsM1 +
                            '">></i>' +
                        '</div>' +
                    '</div>' +
                    '<div class="col-xs-5 text-right magenta">' +
                        data.smsM1[0] + ' ' +
                        '<span class="gray">' + data.smsM1[1] + '</span>' +
                    '</div>' +
                '</div>' : '') +



            '</div>';

    if (addons.length) {
        cart += '<div class="cart-sec clearfix">'
        for (var j = 0; j < addons.length; j++) {
            var tooltip = addons[j].tooltip ? addons[j].tooltip : null;

            if (addons[j].code === 'm-go') tooltip = tooltip_magio_go;
            else if (addons[j].code === 't-master') tooltip = tooltip_t_master;

            cart +=
                '<div class="row">' +
                    '<div class="col-xs-9">' +
                        addons[j].name +
                        (tooltip
                            ? ' <div class="t-typ-info"><i class="t-icon gray info" alt="' + tooltip + '">></i></div>'
                            : '') +
                    '</div>' +
                    '<div class="col-xs-3 text-right">' +
                        '<i class="t-icon">V</i>' +
                    '</div>' +
                '</div>';
        }
        cart += '</div>';
    }

    cart +=
            '<div class="cart-sec clearfix">' +
                '<div class="row">' +
                    '<div class="col-xs-7">' +
                        'Online zľava na nové zariadenie' +
                    '</div>' +
                    '<div class="col-xs-5 text-right">' +
                        '- ' + webZlava + ' <span class="gray">€</span>' +
                    '</div>' +
                '</div>' +
            '</div>' +
            '<div class="cart-sec clearfix">' +
                '<div class="row">' +
                    '<div class="col-xs-7">' +
                        'Mesačne' +
                        '<p>pri 24 mesačnej viazanosti</p>' +
                    '</div>' +
                    '<div class="col-xs-5 text-right cena-selected">' +
                        (cena_discount && data.cena != cena_discount ?
                            '<div>' +
                                '<span class="with-dph">' + cena_discount + '</span>' +
                                '<span class="without-dph">' + cena_discount_bez + '</span>' +
                                '<span class="gray">&nbsp;€</span>' +
                            '</div>' +
                            '<div>' +
                                '<span class="with-dph line-through-m">' + data.cena + '</span>' +
                                '<span class="without-dph line-through-m">' + cena_bez + '</span>' +
                                '<span class="gray line-through-m">&nbsp;€</span>' +
                            '</div>'
                        :
                            '<div>' +
                                '<span class="with-dph">' + data.cena + '</span>' +
                                '<span class="without-dph">' + cena_bez + '</span>' +
                                '<span class="gray">&nbsp;€</span>' +
                            '</div>'
                        ) +
                        '<div class="desc dph-cont">' +
                            '<span class="gray with-dph">s</span>' +
                            '<span class="gray without-dph">bez</span>' +
                            '<span class="gray">&nbsp;DPH</span>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
            '</div>' +
        '</div>' +
        '<div class="cart-buttons">' +
            '<div>' +
                (!non_hw ?
                '<div class="pausal"' + (
                    dataOffer && dataOffer.transactionType === 'TARIFFCHANGE' 
                    ? ' style="width:100%;"' 
                    : '') + 
                '>' +
                    '<a href="' + urlPausal + '&nohw" ' +
                        (dataOffer === null || dataOffer.transactionType === 'PROLONGATION'
                            ? 'class="magenta scenario-path">Kúpiť len paušál'
                            : 'class="btn cta_mag">Prejsť na tento paušál') +
                    '</a>' +
                '</div>'
                : '') +
                (dataOffer === null || dataOffer.transactionType === 'PROLONGATION' ?
                '<div class="mobil"' + (non_hw ? ' style="width:100%;"' : '') + '>' +
                    '<a href="' + urlPausal + '" class="btn cta_mag scenario-path">Pridať mobil</a>' +
                '</div>'
                : '') +
            '</div>' +
        '</div>' +
        '</div>';

    return cart;
}
/* beautify preserve:end */

function setScenarioPath() {
	if (typeSegment === null) return;
	var oldPath = getPath(typeSegment, true);
	var newPath = getPath(typeSegment, false);
	oldPath = oldPath.substr(0, oldPath.length - 1);
	newPath = newPath.substr(0, newPath.length - 1);

	$('.scenario-path').each(function () {
		var path = $(this).attr('href');
		var replacePath = path.replace(oldPath, newPath);
		$(this).attr('href', replacePath);
	});

	for (var i = 0; i < nbaVars.vsetkyPausaly.length; i++) {
		if (typeof nbaVars.vsetkyPausaly[i] === 'string') {
			nbaVars.vsetkyPausaly[i] = nbaVars.vsetkyPausaly[i].replace(new RegExp(oldPath, 'g'), newPath);
		}
	}
}

function getPath(segment, getDefaultPath) {
	var path = '';

	if (nbaVars.src_channel === 'app') {
		//path = '/oneapp/predlzenie/sc/-/scenario/e-shop/pre-vas-pausaly?src_channel=app'
		path = '/oneapp/predlzenie/sc';
	} else {
		if (segment === 'b2c') {
			//path = '/mob/objednavka/-/scenario/e-shop/pre-vas-pausaly?';
			path = '/mob';
		} else {
			//path = '/biznis/objednavka/-/scenario/e-shop/biznis-pausaly?';
			path = '/biznis';
		}
		path += '/objednavka';
	}

	path += '/-/scenario/e-shop/';

	if (!getDefaultPath && nbaVars.config !== null) {
		path += nbaVars.config[segment + 'ScenarioPath'] + '?';
	} else {
		if (segment === 'b2c') {
			path += '/pre-vas-pausaly?';
		} else {
			path += '/biznis-pausaly?';
		}
	}

	if (nbaVars.src_channel === 'app') {
		path += 'src_channel=app&';
	}

	return path;
}

// function getPath(segment, getDefaultPath) {
//     var path = null;

//     if (nbaVars.src_channel === 'web') {}

//     if (segment === 'b2c') {
//         path = '/mob/objednavka/-/scenario/e-shop/pre-vas-pausaly?';
//     } else {
//         path = '/biznis/objednavka/-/scenario/e-shop/biznis-pausaly?';
//     }

//     if (!getDefaultPath && nbaVars.config !== null) {
//         if (segment === 'b2c') {
//             path = '/mob/objednavka/-/scenario/e-shop/';
//         } else {
//             path = '/biznis/objednavka/-/scenario/e-shop/';
//         }
//         path += nbaVars.config[typeSegment + 'ScenarioPath'] + '?';
//     }

//     return path;
// }

$(document).on('click', '.popup-close-focus', function (e) {
	e.preventDefault();
	$.fancybox.close();
	$('.input-pin .input_text').eq(0).focus();
});

function nba(phone, pin) {
	if (typeof phone === 'undefined') phone = null;
	if (typeof pin === 'undefined') pin = null;
	var url = '';
	if (phone === null && pin === null) {
		url = 'http://json.rdg-dev.eu/api/bd-init-page-bad-request';
		// url = 'http://json.rdg-dev.eu/api/bd-non-hw';
		//url = 'http://json.rdg-dev.eu/api/bd-prolongation-with-addon';
	} else if (pin === null) {
		url = 'http://json.rdg-dev.eu/api/bd-pin';
	} else if (phone === null) {
		var phoneNum = $('#phoneNumber').val();
		if (phoneNum.startsWith('0911')) url = 'http://json.rdg-dev.eu/api/bd-prolongation';
		else if (phoneNum.startsWith('0912')) url = 'http://json.rdg-dev.eu/api/bd-prolongation-with-addon';
		else if (phoneNum.startsWith('0913')) url = 'http://json.rdg-dev.eu/api/bd-tariff-change';
		else if (phoneNum.startsWith('0914')) url = 'http://json.rdg-dev.eu/api/bd-expired-otp';
		else if (phoneNum.startsWith('0915')) url = 'http://json.rdg-dev.eu/api/bd-b2b';
		else if (phoneNum.startsWith('0916')) url = 'http://json.rdg-dev.eu/api/bd-b2b-max-discount';
		else if (phoneNum.startsWith('0917')) url = 'http://json.rdg-dev.eu/api/error';
		else if (phoneNum.startsWith('0919')) url = 'http://json.rdg-dev.eu/api/bd-non-hw';
		else if (phoneNum.startsWith('0920')) url = 'http://json.rdg-dev.eu/api/bd-non-hw-only';
		else if (phoneNum.startsWith('0921')) url = 'http://json.rdg-dev.eu/api/bd-non-hw2';
		else url = 'http://json.rdg-dev.eu/api/bd-no-offer';
	}

	return $.ajax({
		url: url,
		dataType: 'json',
		type: 'GET',
	});
}

// ********************** USER CHECKER ***************************************
// *** OVERENIE
$('#checkPhone').click(function () {
	var phone = $('#phoneNumber').val();

	$('div.phoneNumber').addClass('valid');

	if (!checkPhoneFormat(phone, true)) return;

	showPopup(getPopupContent('inProgress').content);

	$.when(nba(phone, null, nbaVars.src_channel))
		.done(function (data, textStatus, jqXHR) {
			if (nbaVars.config === null && data.configuration) {
				nbaVars.config = data.configuration;
				setScenarioPath();
			}
			if (data.offer.verifyStatus === 'OTP_SENT') {
				$('.input-phone').hide();
				$('.input-pin').show(200);
				$('.input-pin .input_text').eq(0).focus();
				$.fancybox.close();
			} else {
				$.fancybox.close();
				showPopup(getPopupContent('error').content);
			}
		})
		.fail(function (error, textStatus, errorThrown) {
			$.fancybox.close();
			showPopup(getPopupContent('error').content);
		});
});

function checkPhoneFormat(phone, showErrorMsg) {
	if (phone.length === 10 && phone.match(/09[0-9]{8}/g)) {
		$('div.phoneNumber').removeClass('has-error').find('.error').remove();
		return true;
	} else {
		if (showErrorMsg && !$('div.phoneNumber').hasClass('has-error')) {
			var error = '<span class="error">Mobilné číslo musí byť v tvare 0903123456</span>';
			$('div.phoneNumber').addClass('has-error').append(error);
		}
		return false;
	}
}

$('#phoneNumber').keypress(function (e) {
	if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) return false;
});

$('#phoneNumber').keyup(function (e) {
	if ($('div.phoneNumber').hasClass('valid')) {
		var phone = $('#phoneNumber').val();
		checkPhoneFormat(phone, true);
	}
});

$('#phoneNumber').bind('paste', function (e) {
	var pastedData = e.originalEvent.clipboardData.getData('text');
	var phone = $('#phoneNumber').val();
	if (phone.length + pastedData.length > 10 || !$.isNumeric(pastedData)) {
		e.preventDefault();
	}
});

$('.input-pin .input_text')
	.focus(function () {
		$('.input-pin .placeholder').css('top', '-20px');

		if ($(this).val().length) {
			$(this).next('.input_text').focus();
		} else {
			var count = (focus = $(this).index());
			for (var i = count - 1; i >= 0; i--) {
				var comp = $('.input-pin .input_text').eq(i);
				if (!comp.val().length) focus = i;
			}

			if (count !== focus) $('.input-pin .input_text').eq(focus).focus();
		}
	})
	.focusout(function () {
		var hasVal = false;
		$('.input-pin .input_text').each(function () {
			if ($(this).val().length) hasVal = true;
		});
		if (!hasVal) $('.input-pin .placeholder').css('top', '45px');
	})
	.keypress(function (e) {
		if (e.keyCode < 48 || e.keyCode > 57 || $(this).val().length > 0) return false;
	})
	.keyup(function (e) {
		if (e.keyCode === 8) {
			var attr = $(this).attr('hasVal');
			if (typeof attr === 'undefined') {
				$(this).prev('.input_text').removeAttr('hasVal').val('').focus();
			} else {
				$(this).val('').removeAttr('hasVal');
			}
		} else {
			$(this).attr('hasVal', '').next('.input_text').focus();
		}
	})
	.bind('paste', function (e) {
		e.preventDefault();
		var pin = e.originalEvent.clipboardData.getData('Text');
		//window.clipboardData.getData("Text");

		if (pin.length === 4 && pin.match(/[0-9]{4}/g)) {
			$('.input-pin .input_text').each(function (key) {
				$(this).val(pin.substr(key, 1));
			});
		}
	});
// *** OVERENIE End

$(document).on('click', '.popup-close', function (e) {
	e.preventDefault();
	$.fancybox.close();
	var scroll = $(this).attr('data-scroll');
	if (typeof scroll !== 'undefined') {
		scrollTo($('.' + scroll), 500);
	}
});

function showPopup(content) {
	var ratioValue = 0.5;
	var width = '50%';
	var responsive = 'desktop';

	if ($(document).innerWidth() <= 767) {
		ratioValue = 1;
		width = '100%';
		responsive = 'mobile';
	}

	$.fancybox({
		content: content,
		padding: 0,
		margin: 0,
		width: width,
		height: 'auto',
		autoSize: false,
		topRatio: ratioValue,
		closeBtn: false,
		wrapCSS: 'bd-scenario-popup ' + responsive,
		parent: '#content',
		helpers: {
			overlay: {
				closeClick: false,
				css: {
					background: 'rgba(0, 0, 0, 0.7)',
				},
			},
		},
	});
}

function Decrypt(text, passcode) {
	passcode += '';
	var result = [];
	var str = '';
	var codesArr = JSON.parse(text);
	var passLen = passcode.length;
	for (var i = 0; i < codesArr.length; i++) {
		var passOffset = i % passLen;
		var calAscii = codesArr[i] - passcode.charCodeAt(passOffset);
		result.push(calAscii);
	}
	for (var i = 0; i < result.length; i++) {
		var ch = String.fromCharCode(result[i]);
		str += ch;
	}
	return str;
}

function createCookie(name, value, days) {
	var expires;

	if (days) {
		var date = new Date();
		date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);
		expires = '; expires=' + date.toGMTString();
	} else {
		expires = '';
	}
	document.cookie = encodeURIComponent(name) + '=' + encodeURIComponent(value) + expires + '; path=/';
}
