$(document).ready(function() {
    /* ::: Scroll To ::: */
    $(".scroll-to").click(function() {
        var parentNav = $(this).closest('.main-navigation').attr('id');
        var offset = 0;

        if (parentNav === 'nav-sticky-custom') {
            offset = 68;
        }

        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: (target.offset().top - ($("#nav-sticky").outerHeight()) - offset)
                }, 1000);
                return false;
            }
        }
    });
    /* ::: Sticky Nav-ext ::: */
    $("#nav-sticky").sticky({
        topSpacing: 0,
        widthFromWrapper: true
    });

    var contentSections = $("[id*='sec-']"),
        secondaryNav = $("#nav-sticky");

    function updateSecondaryNavigation() {
        contentSections.each(function() {
            var actual = $(this),
                actualHeight = actual.height() + parseInt(actual.css('paddingTop').replace('px', '')) + parseInt(actual.css('paddingBottom').replace('px', '')),
                actualAnchor = secondaryNav.find('a[href="#' + actual.attr('id') + '"]');

            if ((actual.offset().top - secondaryNav.outerHeight() <= $(window).scrollTop()) && (actual.offset().top + actualHeight - secondaryNav.outerHeight() > $(window).scrollTop())) {
                actualAnchor.addClass('active');
            } else {
                actualAnchor.removeClass('active');
            }

        });
    }

    updateSecondaryNavigation();
    $(window).scroll(function(event) {
        updateSecondaryNavigation();
    });

});

$(function() {
    /* ::: Slider Oblubene ::: */
    $("#program_slider").slick({
        slidesToShow: 2,
        slidesToScroll: 1,
        centerMode: false,
        infinite: false,
        swipeToSlide: true,
        swipe: true,
        arrows: true,
        dots: false,
        autoplay: false,
        initialSlide: 2,
        autoplaySpeed: 5000,
        touchMove: false,
        draggable: false,
        customPaging: function(slider, i) {
            return '<span class="slide-dot">&nbsp;</span>';
        },
        responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
                arrows: true,
                dots: true
            }
        }, {
            breakpoint: 768,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: true,
                infinite: false,
                dots: true
            }
        }]
    });
    /* ::: Slider Zakladne ::: */
    $("#zakladne_slider").slick({
        slidesToShow: 2,
        slidesToScroll: 1,
        centerMode: false,
        infinite: false,
        swipeToSlide: true,
        swipe: true,
        arrows: true,
        dots: false,
        autoplay: false,
        // initialSlide: 1,
        autoplaySpeed: 5000,
        touchMove: false,
        draggable: false,
        customPaging: function(slider, i) {
            return '<span class="slide-dot">&nbsp;</span>';
        },
        responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
                arrows: true,
                dots: true
            }
        }, {
            breakpoint: 768,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: true,
                infinite: false,
                dots: true
            }
        }]
    });
    /* ::: Slider Prenarocnych ::: */
    $("#prenarocnych_slider").slick({
        slidesToShow: 2,
        slidesToScroll: 1,
        centerMode: false,
        infinite: false,
        swipeToSlide: true,
        swipe: true,
        arrows: true,
        dots: true,
        autoplay: false,
        draggable: false,
        autoplaySpeed: 5000,
        customPaging: function(slider, i) {
            return '<span class="slide-dot">&nbsp;</span>';
        },
        responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
                arrows: true,
                dots: true
            }
        }, {
            breakpoint: 768,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: true,
                infinite: false,
                dots: true
            }
        }]
    });

    /* ::: PopUp Conf ::: */
    $('.p-close').on('click', function(e) {
        e.preventDefault();
        $.fancybox.close();
    });
    // popup close with scroll to section
    $(document).on('click', '.popup-modal-dismiss', function(e) {
        e.preventDefault();
        $.fancybox.close();
    });

    $(document).on('click', '.popup-modal-scroll', function(e) {
        e.preventDefault();
        $.fancybox.close();
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top - ($("#nav-sticky").outerHeight())
                }, 1000);
                return false;
            }
        }
    });

    /* ::: PopUp Data ::: */
    $('.popup').fancybox({
        padding: 11,
        margin: 0,
        closeBtn: false,
        parent: "#content",
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.7)'
                }
            }
        }
    });

    $("a#activate-order100").fancybox({
        padding: 11,
        margin: 0,
        closeBtn: false,
        parent: "#content",
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.7)'
                }
            }
        }
    });

    $("#activate-order500").fancybox({
        padding: 11,
        margin: 0,
        closeBtn: false,
        parent: "#content",
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.7)'
                }
            }
        }
    });

    $("#activate-order1000").fancybox({
        padding: 11,
        margin: 0,
        closeBtn: false,
        parent: "#content",
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.7)'
                }
            }
        }
    });

    $("a#info-order1000a").fancybox({
        padding: 11,
        margin: 0,
        closeBtn: false,
        parent: "#content",
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.7)'
                }
            }
        }
    });

    $("#info-order100").fancybox({
        padding: 11,
        margin: 0,
        closeBtn: false,
        parent: "#content",
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.7)'
                }
            }
        }
    });

    $("#info-order500").fancybox({
        padding: 11,
        margin: 0,
        closeBtn: false,
        parent: "#content",
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.7)'
                }
            }
        }
    });

    $("#info-order1000").fancybox({
        padding: 11,
        margin: 0,
        closeBtn: false,
        parent: "#content",
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.7)'
                }
            }
        }
    });

    $("#activate-data100").fancybox({
        padding: 11,
        margin: 0,
        closeBtn: false,
        parent: "#content",
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.7)'
                }
            }
        }
    });

    $("#activate-data500").fancybox({
        padding: 11,
        margin: 0,
        closeBtn: false,
        parent: "#content",
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.7)'
                }
            }
        }
    });

    $("#activate-lightbox-bz").fancybox({
        padding: 11,
        margin: 0,
        closeBtn: false,
        parent: "#content",
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.7)'
                }
            }
        }
    });

    $("#activate-lightbox-zd").fancybox({
        padding: 11,
        margin: 0,
        closeBtn: false,
        parent: "#content",
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.7)'
                }
            }
        }
    });

    // $("#activate-lightbox-young").fancybox({
    //     padding: 11,
    //     margin: 0,
    //     closeBtn: false,
    //     parent: "#content",
    //     helpers: {
    //         overlay: {
    //             css: {
    //                 'background': 'rgba(0, 0, 0, 0.7)'
    //             }
    //         }
    //     }
    // });



    /* ::: ToolTips ::: */
    var generateTooltip = function(element, text) {
        $(element).qtip({
            content: {
                text: text
            },
            position: {
                my: 'bottom center',
                at: 'top center'
            }
        });
    }

    $('.pausaly-info').qtip({
        content: {
            text: 'Vybrané paušály, pre ktoré platí akcia: Happy M, L, XL volania, L, XL, XXL a Profi.'
        },
        position: {
            my: 'left center',
            at: 'right center'
        }
    });

    $('.clip-info').qtip({
        content: 'So službou CLIP uvidíte na displeji svojho mobilu telefónne číslo volajúceho, pokiaľ nemá aktivovanú službu CLIR (zamedzenie zobrazenia telefónneho čísla). Ak máte uložené telefónne číslo volajúceho v pamäti, uvidíte jeho meno.',
        position: {
            corner: {
                target: 'leftMiddle',
                tooltip: 'topMiddle'
            },
            position: {
                my: 'bottom center',
                at: 'top center'
            }
        }
    }).bind('click', function(event) {
        event.preventDefault();
        return false;
    });

    $('.ec-beta').qtip({
        content: {
            text: 'Služba immmr je zatiaľ v pilotnej prevádzke. Pomôžte nám ju zlepšiť. Svoje podnety posielajte na <a href="mailto:data@telekom.sk">data@telekom.sk</a>'
        },
        hide: {
            event: 'unfocus'
        },
        position: {
            my: 'left center',
            at: 'right center'
        }
    });

    $('.roaming-info').qtip({
        content: 'Všetky členské štáty EÚ a Island, Nórsko, Lichtenštajnsko',
        position: {
            corner: {
                target: 'leftMiddle',
                tooltip: 'topMiddle'
            }
        }
    }).bind('click', function(event) {
        event.preventDefault();
        return false;
    });

    $('.tooltip').qtip({
        content: {
            text: 'Vybrané paušály sú:<br> Happy M, L, XL volania, XL, XXL a Profi'
        },
        position: {
            my: 'bottom center',
            at: 'top center'
        }
    });

    $('.akcie-info').qtip({
        content: '<div class="tag-left">Novým zákazníkom objem dát navýšime pri aktivácii paušálu a verným od najbližšieho zúčtovacieho obdobia po 15. 3. 2017, najneskôr však do 9.4.2017.</div>',
        position: {
            corner: {
                target: 'leftMiddle',
                tooltip: 'topMiddle'
            }
        }
    }).bind('click', function(event) {
        event.preventDefault();
        return false;
    });


    $('.happy_xs-info').qtip({
        content: 'Počas pracovných dní v čase od 19.00 hod. do 7.00 hod. Soboty, nedele a štátne sviatky počas celých 24 hodín.',
        position: {
            corner: {
                target: 'leftMiddle',
                tooltip: 'topMiddle'
            }
        }
    }).bind('click', function(event) {
        event.preventDefault();
        return false;
    });

    $('.happy_young-info').qtip({
        content: 'Web zľavu až 20 € na mobil získate iba pri kúpe paušálu na našom webe. Konkrétna výška zľavy bude vypočítaná pri nákupe.',
        position: {
            corner: {
                target: 'leftMiddle',
                tooltip: 'topMiddle'
            }
        }
    }).bind('click', function(event) {
        event.preventDefault();
        return false;
    });




    $('#info-balik-volania, #info-balik-minuty, #info-balik-sms').qtip({
        content: 'Balík aktivujete na webe počas kúpy Happy paušálu.',
        position: {
            corner: {
                target: 'leftMiddle',
                tooltip: 'topMiddle'
            }
        }
    }).bind('click', function(event) {
        event.preventDefault();
        return false;
    });

    /* ::: Order Form for Mobile*/
    var orderUrl = '/mam-zaujem/volania/happy/formular?pausal=';

    $('#order-happy-xs-mini').click(function(event) {
        /*event.preventDefault();*/
        if (window.matchMedia('(max-width: 600px)').matches) {
            $(this).attr("href", orderUrl + 'Happy XS mini');
        } else {}
    });

    $('#order-happy-xs').click(function(event) {
        /*event.preventDefault();*/
        if (window.matchMedia('(max-width: 600px)').matches) {
            $(this).attr("href", orderUrl + 'Happy XS');
        } else {}
    });

    $('#order-happy-s').click(function(event) {
        /*event.preventDefault();*/
        if (window.matchMedia('(max-width: 600px)').matches) {
            $(this).attr("href", orderUrl + 'Happy S');
        } else {}
    });

    $('#order-happy-m').click(function(event) {
        /*event.preventDefault();*/
        if (window.matchMedia('(max-width: 600px)').matches) {
            $(this).attr("href", orderUrl + 'Happy M');
        } else {}
    });

    $('#order-happy-l').click(function(event) {
        /*event.preventDefault();*/
        if (window.matchMedia('(max-width: 600px)').matches) {
            $(this).attr("href", orderUrl + 'Happy L');
        } else {}

    });

    $('#order-happy-xl-call').click(function(event) {
        /*event.preventDefault();*/
        if (window.matchMedia('(max-width: 600px)').matches) {
            $(this).attr("href", orderUrl + 'Happy XL Volania');
        } else {}

    });

    $('#order-happy-xl').click(function(event) {
        /*event.preventDefault();*/
        if (window.matchMedia('(max-width: 600px)').matches) {
            $(this).attr("href", orderUrl + 'Happy XL');
        } else {}

    });

    $('#order-happy-xxl').click(function(event) {
        /*event.preventDefault();*/
        if (window.matchMedia('(max-width: 600px)').matches) {
            $(this).attr("href", orderUrl + 'Happy XXL');
        } else {}

    });

    $('#order-happy-profi').click(function(event) {
        /*event.preventDefault();*/
        if (window.matchMedia('(max-width: 600px)').matches) {
            $(this).attr("href", orderUrl + 'Happy Profi');
        } else {}

    });


    /* Tabs */
    $(".tabs-menu a").click(function(event) {
        event.preventDefault();
        if (!$(this).parent().hasClass("current") && !$(this).parent().hasClass("isVariant")) {
            toggleTabs();
        }

        $(this).parent().addClass("current");
        $(this).parent().siblings().removeClass("current");
        var tab = $(this).attr("href"),
            parent = $(this).closest('ul').parent().parent();
        parent.find('.tab-content').not(tab).css("display", "none");
        $(tab).fadeIn();

    });

    var toggleTabs = function() {
        $('.tab__variant').toggleClass('selected');
    };

    ////////////////////////////////////
    $('.as1').slick({
        infinite: false,
        slidesToShow: 2,
        slidesToScroll: 1,
        infinite: false,
        dots: true,
        responsive: [{
            breakpoint: 700,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: true
            }
        }]
    });

    $('.as2').slick({
        infinite: false,
        slidesToShow: 2,
        slidesToScroll: 1,
        infinite: false,
        dots: true,
        responsive: [{
            breakpoint: 700,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: true
            }
        }]
    });
    /* ACCORDION */

    $('.accordion .item .heading').click(function(e) {

        var clickedHead = e.target;
        var $item = $(clickedHead).closest('.item');
        var isOpen = $item.hasClass('open');
        var $content = $item.find('.content');
        var $acPrice = $(clickedHead).find('.ac-price');

        if (isOpen) {
            $content.slideUp(200);
            $item.removeClass('open');
            $acPrice.show();
        } else {
            $content.slideDown(200);
            $item.addClass('open');
            $acPrice.hide();
        }

    });

    $('.accordion .item.open').find('.content').slideDown(200);

});

jQuery(document).ready(function() {
    jQuery('ul.taby2 li a').on('click', function(e) {
        $('.as2').css("visibility", "hidden");
        var currentAttrValue = jQuery(this).attr('href');

        jQuery('.tab ' + currentAttrValue).show().siblings().hide();

        jQuery(this).parent('li').addClass('active').siblings().removeClass('active');
        $('.as2').slick('setPosition');
        setTimeout(function() {
            $('.as2').css("visibility", "visible");
        }, 200);
        e.preventDefault();
    });
});


/* auto scroll from hash */
jQuery(document).ready(function() {
    // if (window.location.hash) scroll(0, 0);
    // setTimeout(function() {
    //     scroll(0, 0);
    // }, 1);

    if (window.location.hash) {
        var urlHash = window.location.hash;

        function scrollBasedOnURL(scrollto, link) {
            $('html, body').animate({
                scrollTop: ($(scrollto).offset().top - 130) + 'px'
            }, 1000);
            $(link).eq(0).prev().trigger('click');
        }

        function scrollHandlerBasedOnURLParams(scrollto, link) {
            if ($(window).width() > 769) {
                scrollBasedOnURL(scrollto, link);
            } else {
                scrollBasedOnURL(scrollto, link);
            }
        }

        setTimeout(function() {
            switch (urlHash) {
                case '#tab-2':
                    $('html, body').animate({
                        scrollTop: ($("#sec-6").offset().top - 55) + 'px'
                    }, 1000);
                    break;
                case '#happy-xs-mini':
                    scrollHandlerBasedOnURLParams('#prod-0', 'a[href="/objednavka/-/scenario/e-shop/happy-xs-mini"]');
                    break;
                case '#vyberrp=xs':
                    scrollHandlerBasedOnURLParams('#prod-1', 'a[href="/mob/objednavka/-/scenario/e-shop/happy?vyberrp=xs"]');
                    break;
                case '#vyberrp=s':
                    scrollHandlerBasedOnURLParams('#prod-2', 'a[href="/mob/objednavka/-/scenario/e-shop/happy?vyberrp=s"]');
                    break;
                case '#xl_data_premladych':
                    scrollHandlerBasedOnURLParams('#prod-3', 'a[href="/mob/objednavka/-/scenario/e-shop/happy?vyberrp=xl_data_premladych"]');
                    break;
                case '#vyberrp=m':
                    scrollHandlerBasedOnURLParams('#prod-2', 'a[href="/mob/objednavka/-/scenario/e-shop/happy?vyberrp=m"]');
                    break;
                case '#vyberrp=l':
                    scrollHandlerBasedOnURLParams('#prod-2', 'a[href="/mob/objednavka/-/scenario/e-shop/happy?vyberrp=l"]');
                    break;
                case '#vyberrp=xl_call':
                    scrollHandlerBasedOnURLParams('#prod-2', 'a[href="/mob/objednavka/-/scenario/e-shop/happy?vyberrp=xl_call"]');
                    break;
                case '#vyberrp=xl':
                    scrollHandlerBasedOnURLParams('#prod-2', 'a[href="/mob/objednavka/-/scenario/e-shop/happy?vyberrp=xl"]');
                    break;
                case '#vyberrp=xxl':
                    scrollHandlerBasedOnURLParams('#prod-2', 'a[href="/mob/objednavka/-/scenario/e-shop/happy?vyberrp=xxl"]');
                    break;
                case '#vyberrp=profi':
                    scrollHandlerBasedOnURLParams('#prod-2', 'a[href="/mob/objednavka/-/scenario/e-shop/happy?vyberrp=profi"]');
                    break;
                default:
                    $('html, body').animate({
                        scrollTop: ($(window.location.hash).offset().top - 10) + 'px'
                    }, 1000);
            };
        }, 1000);
    }

    // AKTUÁLNE AKCIE click
    if ($(window).width() > 769) {
        $("#activate-lightbox-young").click(function(e) {
            e.preventDefault();
            $('html, body').animate({
                scrollTop: ($(".plp__plan-ribbon:contains(LEN DO 26 ROKOV)").offset().top - 130) + 'px'
            }, 1000);
            $('a[href="/mob/objednavka/-/scenario/e-shop/happy?vyberrp=xl_data_premladych"]').eq(0).prev().trigger('click');
        });
    } else {
        $("#activate-lightbox-young").click(function(e) {
            e.preventDefault();
            $('html, body').animate({
                scrollTop: ($(".plm__plan-ribbon:contains(LEN DO 26 ROKOV)").offset().top - 130) + 'px'
            }, 1000);
            $('a[href="/mob/objednavka/-/scenario/e-shop/happy?vyberrp=xl_data_premladych"]').eq(2).prev().trigger('click');
        });
    }

    /* SCROLLING THE NAV */

    function resetCheckboxes() {
        $('#base').prop('checked', true);
        $('#base').closest('.checker > span').addClass('checked');
        $('#favourite').prop('checked', true);
        $('#favourite').closest('.checker > span').addClass('checked');
        $('#experienced').prop('checked', true);
        $('#experienced').closest('.checker > span').addClass('checked');
    }
});

$(document).ready(function() {
    function checkMobileOS() { var MobileUserAgent = navigator.userAgent || navigator.vendor || window.opera; if (MobileUserAgent.match(/iPad/i) || MobileUserAgent.match(/iPhone/i) || MobileUserAgent.match(/iPod/i)) { return 'iOS'; } else if (MobileUserAgent.match(/Android/i)) { return 'Android'; } else { return 'unknown'; } }
    $(".sms_link").each(function(index) {
        $(this).on("click", function() {
            // event.preventDefault();
            var href = '';
            var message_text = this.dataset.smsText;
            var message_number = this.dataset.smsNumber;
            if (checkMobileOS() == 'iOS') {href = "sms:" + message_number + "&body=" + encodeURI(message_text); } 
            if (checkMobileOS() == 'Android') { href = "sms:" + message_number + "?body=" + encodeURI(message_text); }
            $(this).attr('href', href);
        });
    });
});