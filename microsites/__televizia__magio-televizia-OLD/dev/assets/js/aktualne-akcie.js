$(document).ready(function() {
    if (location.hash) {
        var target = $('.' + location.hash.substr(1));
        if (target.length) {
            $('html,body').animate({
                scrollTop: target.offset().top - ($("#nav-sticky").outerHeight() - 10)
            }, 500);
        } 
    }
});

$('.tv-ph').qtip({
    content: 'Pri kúpe cez web získate za každú službu 10 € zľavu. Za Magio internet, televíziu a Pevnú linku spolu tak získate až 30 €. Zľava vám bude odrátaná z faktúry druhý a tretí mesiac vo výške 15 €.',
    style: {
        classes: 'qtip-tipsy'
    },
    position: {
        corner: {
            target: 'leftMiddle',
            tooltip: 'topMiddle'
        }
    }
    // position: {
    //     my: 'bottom center',
    //     at: 'top center'
    // }
});
    /* TABS */
$(".tabs-menu a").click(function(event) {
    event.preventDefault();
    $(this).parent().addClass("current");
    $(this).parent().siblings().removeClass("current");
    var tab = $(this).attr("href"), // select clicked tab. In example "#tab-1" ..
        parent = $(this).closest('ul').parent().parent(); // <tabs-container>
    parent.find('.tab-content').not(tab).css("display", "none"); //hide all not clicked tabs
    $(tab).fadeIn(); //show clicked tab


});
/* TABS END */

/* FROM TABS TO ACCORDION */
var dataSegment = $("[data-segment]").each(function(){
  //console.log($(this).attr('data-segment')); //zobraz "data-segment" hodnotu pre kazdu <section>

  closestHead = $($(this).find('.tabs-menu a'));
  closestItemCount = closestHead.length - 1; //vytiahni pocet tabov pre kazdu <section> s atributom "data-segment"
  console.log(closestItemCount + "heads");

  closestContent = $($(this).find('.tab-content')); //najdi tab-content v danej <section>
  //console.log(closestContent);

  closestAccordion = $(this).find('.accordion'); //najdi .accordion v danej <section>

  accordionItem = '<div class="item open">';

  for (var i = 0; i <= closestItemCount; i++) {
      accordionItem += '<div class="heading">' + $(closestHead[i]).text()  + '</div>';
      accordionItem += '<div class="content">' + $(closestContent[i]).html() + '</div></div>';

      if (i !== closestItemCount) {
          accordionItem += '<div class="item">';
      }
  }

  //if data-segment and data-accordion value match, show accordion data
  if($(this).attr('data-segment') === closestAccordion.attr('data-accordion')) {
       $(accordionItem).appendTo(closestAccordion);
  }

  var $items = $('.accordion .item');
  //SET OPENED ITEM
  $($items[0]).addClass('open');

});
/* FROM TABS TO ACCORDION END */

/* ACCORDION */

$('.accordion .item .heading').click(function(e) {

    var clickedHead = e.target;
    var $item = $(clickedHead).closest('.item');
    var isOpen = $item.hasClass('open');
    var $content = $item.find('.content');
    var $acPrice = $(clickedHead).find('.ac-price');

    if (isOpen) {
        $content.slideUp(200);
        $item.removeClass('open');
        $acPrice.show();
    } else {
        $content.slideDown(200);
        $item.addClass('open');
        $acPrice.hide();
    }
});



$('.accordion .item.open').find('.content').slideDown(200);
/* ACCORDION END */


/* DIV DATA-HREF */
$(document).on('click','#content .tab-acc-sub',function(){
    window.location.href = '/' + $(this).attr('data-href');
});
