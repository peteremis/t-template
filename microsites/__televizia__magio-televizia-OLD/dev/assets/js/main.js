$(function() {

  $('#info-tvDostupnost').qtip({
    content: 'Televíziu cez optický alebo metalický kábel vám zapojíme, ak je táto technológia dostupná na vašej adrese. Ak nie je dostupná, zapojíme vám televíziu cez satelit.',
    style: {
      classes: 'qtip-tipsy'
    },
    position: {
      corner: {
        target: 'leftMiddle',
        tooltip: 'topMiddle'
      }
    }
    // position: {
    //     my: 'bottom center',
    //     at: 'top center'
    // }
  });


  $('#after-confirmation').qtip({
    content: 'Jeden Magio Box umožňuje nahrávať TV vysielanie. Ostatné Magio Boxy neumožňujú nahrávať TV vysielanie.',
    style: {
      classes: 'qtip-tipsy'
    },
    position: {
      corner: {
        target: 'leftMiddle',
        tooltip: 'topMiddle'
      }
    }
    // position: {
    //     my: 'bottom center',
    //     at: 'top center'
    // }
  });

  $('#magio-tv-tooltip').qtip({
    content: 'Bez funkcie nahrávania zaplatíte iba 1,94€ mesačne',
    style: {
      classes: 'qtip-tipsy'
    },
    position: {
      corner: {
        target: 'leftMiddle',
        tooltip: 'topMiddle'
      }
    }
    // position: {
    //     my: 'bottom center',
    //     at: 'top center'
    // }
  });

  $('#naj_znacka').qtip({
    content: 'Vaša dôvera je u nás na prvom mieste.<br>Magio internet a televízia sú víťazmi Ankety o najdôveryhodnejšiu značku v oblasti poskytovateľov internetových a televíznych služieb za rok 2018, ktorú uskutočnila nezávislá agentúra Nielsen.',
    style: {
        'text-align': 'center'
    },
    //position: {
    //    corner: {
    //        target: 'leftCenter',
    //        tooltip: 'topMiddle'
    //    }
    //}
     position: {
         my: 'left center',
         at: 'right center'
     }
});
$('#naj_znacka_m').qtip({
    content: 'Vaša dôvera je u nás na prvom mieste.<br>Magio internet a televízia sú víťazmi Ankety o najdôveryhodnejšiu značku v oblasti poskytovateľov internetových a televíznych služieb za rok 2018, ktorú uskutočnila nezávislá agentúra Nielsen.',
    style: {
        'text-align': 'center'
    },
    position: {
        corner: {
            target: 'leftCenter',
            tooltip: 'topCenter'
       }
    }
     //position: {
     //    my: 'left center',
     //    at: 'right center'
     //}
});
});

/* scroll to + sticky nav */
$("#nav-sticky").sticky({
  topSpacing: 0,
  widthFromWrapper: true
});

$(".scroll-to").click(function() {
  if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
    var target = $(this.hash);
    target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
    if (target.length) {
      $('html,body').animate({
        scrollTop: target.offset().top - ($("#nav-sticky").outerHeight())
      }, 1000);
      return false;
    }
  }
});

var contentSections = $("[id*='sec-']"),
  secondaryNav = $("#nav-sticky");

function updateSecondaryNavigation() {
  contentSections.each(function() {
    var actual = $(this),
      actualHeight = actual.height() + parseInt(actual.css('paddingTop').replace('px', '')) + parseInt(actual.css('paddingBottom').replace('px', '')),
      actualAnchor = secondaryNav.find('a[href="#' + actual.attr('id') + '"]');

    if ((actual.offset().top - secondaryNav.outerHeight() <= $(window).scrollTop()) && (actual.offset().top + actualHeight - secondaryNav.outerHeight() > $(window).scrollTop())) {
      actualAnchor.addClass('active');
    } else {
      actualAnchor.removeClass('active');
    }

  });
}
updateSecondaryNavigation();

$(window).scroll(function(event) {
  updateSecondaryNavigation();
});
/* scroll to + sticky nav END */

/* radiobtns v2 */
//default magenta color for radio box
var defaultCheckboxColor = $('input[name=rb-pair]:checked', '#secondaryRadio').attr('id');
$('.secondaryRadio').find('label[for=' + defaultCheckboxColor + ']').css({
  'background-color': '#fff',
  'color': '#e20074',
  'border': '1px solid #e20074'
});

//on click set magenta color to selected radio box and make other radio boxes transparent

$(".secondaryRadio input").click(function() {
  var getAttr = $(this).attr('id');
  var label = $('.secondaryRadio').find('label[for=' + getAttr + ']');
  var allCheckboxes = $(".secondaryRadio label").css({
    'background-color': 'transparent',
    'color': '#000',
    'border': '1px solid #ccc'
  })

  if ($(this).is(':checked')) {
    $(label).css({
      'background-color': 'transparent',
      'color': '#e20074',
      'border': '1px solid #e20074'
    });
  }

  //If radiobtn and div IDs match, show div.
  var btnID = $(this).attr('id');
  var checkedDiv = $('.detailHolder[id=' + btnID + ']');
  var divID = $('.detailHolder').attr('id');
  var objLength = $(this).length;
  if (btnID = divID && objLength > 0) {
    $('.detailHolder').hide();
    $(checkedDiv).show();
  }
});
/* radiobtns v2 end */


/* TOGGLE ARROW */
$('.toggle-arrow, .arrow-right').click(function(event) {
  event.preventDefault();
  if ($(this).hasClass('arrow-right')) {
    return;
  }
  $(event.target).closest('.togleArrow').find('.arrow-content').toggle(200);
  $(this).find('.arrow-right').toggleClass('arrow-rotate');
});
/* TOGGLE ARROW END */

/* MAGIOBOX POPUP */
$('.magioBoxPopup').fancybox({
  padding: 10,
  margin: 10,
  closeBtn: false,
  parent: "#content",
  helpers: {
    overlay: {
      css: {
        'background': 'rgba(0, 0, 0, 0.7)'
      }
    }
  }
});

$('.p-close, .link-close, .close-end, .text-close').on('click', function(e) {
  e.preventDefault();
  $.fancybox.close();
});
/* MAGIOBOX POPUP END */

/* HEAPBOX POPUP */
$('.heapboxPopup').fancybox({
  padding: 10,
  margin: 10,
  closeBtn: false,
  parent: "#content",
  helpers: {
    overlay: {
      css: {
        'background': 'rgba(0, 0, 0, 0.7)'
      }
    }
  }
});

$('.p-close, .link-close, .close-end, .text-close').on('click', function(e) {
  e.preventDefault();
  $.fancybox.close();
});
/* HEAPBOX POPUP END */

/* HEAPBOX */
/* heapbox - televizne stanice */
$('#infotxt4').closest('li').hide();
$('#infotxt5').closest('li').hide();

function HeapboxTVstanice(txt1, txt2, txt3, txt3toggle, txt4, txt4toggle, txt5, txt5toggle, heapboxPopup, attr, txt6, txt6toggle){
  $('#infotxt1').text(txt1);
  $('#infotxt2').text(txt2);
  $('#infotxt3').text(txt3).closest('li').toggle(txt3toggle);
  $('#infotxt4').text(txt4).closest('li').toggle(txt4toggle);
  $('#infotxt5').text(txt5).closest('li').toggle(txt5toggle);
  $('#infotxt6').text(txt6).closest('li').toggle(txt6toggle);
  $('.heapboxPopup').text(heapboxPopup);
  $('.heapboxPopup').attr("href", attr);
}

$("#heap").heapbox({
  'onChange': function(value) {
    //console.log('Selected value: ' + value);

    if (value === "tvM") {
      HeapboxTVstanice("Základný balík vrátane HD staníc", "3 programové balíky podľa vlastného výberu", "Ďalšie lubovoľné balíky programov, napr.: HD Rodinný, Športový, ...", true, "", false, "", false, "POZRIEŤ STANICE MAGIO TV M", "/televizia/magio-televizia/detail/#magioM","",false);
    } else if (value === "tvL") {
      HeapboxTVstanice("Základný balík vrátane HD staníc", "6 programových balíkov podľa vlastného výberu", "1 Extra balík L", true, "Exkluzívna detská Ťuki TV", true, "", false, "POZRIEŤ STANICE MAGIO TV L", "/televizia/magio-televizia/detail/#magioL","",false);
    } else if (value === "tvXL") {
      HeapboxTVstanice("Základný balík vrátane HD staníc", "6 programových balíkov podľa vlastného výberu", "1 Extra balík L", true, "1 Extra balík XL", true, "Exkluzívna detská Ťuki TV", true, "POZRIEŤ STANICE MAGIO TV XL", "/televizia/magio-televizia/detail/#magioXL",'',false);
    } else if (value === "satM") {
      HeapboxTVstanice("3 prémiové balíky na výber – HBO + Cinemax + HBO GO, HBO + HBO GO a Maďarský Extra", "funkcionalita Zastav, pretoč, pusti znova", "", false, "", false, "", false, "POZRIEŤ STANICE MAGIO SAT M", "/televizia/magio-televizia/detail-sat/#magioM",'',false);
    } else if (value === "satL") {
      HeapboxTVstanice("Balík Magio SAT L obsahuje všetky kanály z balíka M", "3 prémiové balíky na výber – HBO + Cinemax + HBO GO, HBO + HBO GO a Maďarský Extra", "funkcionalita Zastav, pretoč, pusti znova", true, "", false, "", false, "POZRIEŤ STANICE MAGIO SAT L", "/televizia/magio-televizia/detail-sat/#magioL",'',false);
    } else if (value === "satXL") {
      HeapboxTVstanice("Balík Magio SAT XL obsahuje všetky kanály z balíkov M aj L", "3 prémiové balíky na výber – HBO + Cinemax + HBO GO, HBO + HBO GO a Maďarský Extra", "funkcionalita Zastav, pretoč, pusti znova", true, "", false, "", false, "POZRIEŤ STANICE MAGIO SAT XL", "/televizia/magio-televizia/detail-sat/#magioXL",'',false);
    }
  }
});
/* heapbox - televizne stanice */


/* heapbox - zariadenia k magio tv */
$('#magio-tv-tooltip').hide();

function HeapboxTV(cena, mesacne, hideorshow, text){
  $('#price1').text(cena);
  $('#monthly').text(mesacne);
  $('#magio-tv-tooltip').toggle(hideorshow);
  $('.heapTV-infoText').text(text)
}

$("#heapTv").heapbox({
  'onChange': function(value) {
    //console.log('Selected value: ' + value);
    if (value === "4ks") {
      HeapboxTV("74,94 €", "8,77 €", false, "Štyri Magio Boxy umožňujú pripojiť štyri televízory. Na každom môžete sledovať iný program.");
    } else if (value === "3ks") {
      HeapboxTV("49,96 €", "6,83 €", false, "Tri Magio Boxy umožňujú pripojiť tri televízory. Na každom môžete sledovať iný program.");
    } else if (value === "2ks") {
      HeapboxTV("24,98 €", "4,89 €", false, "Dva Magio Boxy umožňujú pripojiť dva televízory. Na každom môžete sledovať iný program.");
    } else if (value === "1ks") {
      HeapboxTV("0 €", "2,95 €", true, "Jeden Magio Box umožňuje pripojiť jeden televízor.");
    } else if (value === "1kssat") {
      HeapboxTV("29,99 €", "2,95 €", false, "Jeden Magio Box umožňuje pripojiť jeden televízor.");
    } else if (value === "2kssat") {
      HeapboxTV("54,97 €", "5,90 €", false, "Dva Magio Boxy umožňujú pripojiť dva televízory. Na každom môžete sledovať iný program.");
    } else if (value === "3kssat") {
      HeapboxTV("49,96 €", "6,83 €", false, "Tri Magio Boxy umožňujú pripojiť tri televízory. Na každom môžete sledovať iný program.");
    } else if (value === "4kssat") {
      HeapboxTV("74,94 €", "8,77 €", false, "Štyri Magio Boxy umožňujú pripojiť štyri televízory. Na každom môžete sledovať iný program.");
    }
  }
});
/* heapbox - zariadenia k magio tv */
/* HEAPBOX END */

/* zmazat filter / zmenit adresu BTN */
$('.filter').click(function(e) {
  e.preventDefault();
  $('.confirmation-btn').show();
  $('#secondaryRadio').show();
  $('.showtv-p').show();
  $('.reset-confirm').addClass('hidden');

  if (matchMedia) {
    var mediaQueryMobil = window.matchMedia("(max-width: 550px)");
    mediaQueryMobil.addListener(WidthChange);
    WidthChange(mediaQueryMobil);
  }
  // media query change
  function WidthChange(mediaQueryMobil) {
    if (mediaQueryMobil.matches) {
      // window width is less than 550px
      $('.detail-wrapper').css('display', 'none');
      $('.detail-wrapper-mobile').css('display', 'flex');
    } else {
      // window width is more than 550px
      $('.detail-wrapper').css('display', 'flex');
      $('.detail-wrapper-mobile').css('display', 'none');
    }
  }

  $('.errorbox').remove();
  $('.commitbox').remove();
});
/* zmazat filter / zmenit adresu BTN END */

/* zmenit adresu BTN START */
$('.zmenit-adresu').click(function(e) {
  e.preventDefault();
  $('.confirmation-btn').hide();

  $('.errorbox').remove();
  $('.commitbox').remove();
});
/* zmenit adresu BTN END */

/* FANCYBOX OVERENIE DOSTUPNOSTI */
//po kliknuti na "overit dostupnost na mojej adrese"
$('.confirmation-btn').click(function(e) {
  e.preventDefault();
  $('.confirmation-btn').hide();
  $('.showtv-p').hide();

  $('.dostupnostPopup').fancybox({
    padding: 10,
    margin: 10,
    closeBtn: false,
    parent: "#content",
    //width: 450,
    //minHeight: 350,
    height: 'auto',
    autoSize: true,
    helpers: {
      overlay: {
        css: {
          'background': 'rgba(0, 0, 0, 0.7)'
        },
        closeClick: false
      },
    }
  });

  $('.p-close, .link-close, .close-end, .text-close, .sendReq').on('click', function(e) {
    e.preventDefault();
    $.fancybox.close();
  });

  $('#availability .p-head .p-close').show(); //fancybox s X
});

//"prejst k nakupu" btn - fancybox
$('.dostupnostPopup').fancybox({
  padding: 10,
  margin: 10,
  closeBtn: false,
  parent: "#content",
  //width: 450,
  //minHeight: 350,
  height: 'auto',
  autoSize: true,
  helpers: {
    overlay: {
      css: {
        'background': 'rgba(0, 0, 0, 0.7)'
      },
      closeClick: false
    },
  }
});

$('.sendReq').on('click', function(e) {
  e.preventDefault();
  $.fancybox.close();
});

//po kliknuti na X vo fancyboxe, zobraz button "overit dostupnost na mojej adrese"
$('.closebtn-availability').on('click', function() {
  $('.dostupnostPopup').show();
});
/* FANCYBOX OVERENIE DOSTUPNOSTI END */


/* FANCYBOX TV ARCHIV S, M */
$('.archivyPopup').fancybox({
  padding: 10,
  margin: 10,
  closeBtn: false,
  parent: "#content",
  helpers: {
    overlay: {
      css: {
        'background': 'rgba(0, 0, 0, 0.7)'
      }
    },
  }
});

$('.p-close, .link-close, .close-end, .text-close').on('click', function(e) {
  e.preventDefault();
  $.fancybox.close();
});
/* FANCYBOX TV ARCHIV S, M END */

/* load section AKTUALNE AKCIE */
if (window.location.hostname === "localhost") {
  $( ".sec-akcie" ).load( "templates/_aktualne-akcie.html", function() {
  });
} else {
  $( ".sec-akcie" ).load( "/documents/10179/16511938/_aktualne-akcie.html", function() {
  });
}
/* load END */
$( document ).ready(function() {
  var count = 0;
  var myTimer = setInterval(function() {
    if ($('.cont-christ-action .cont-tv-pack-xl.pack-2 > img').length) {
      $('.cont-christ-action .cont-tv-pack-xl.pack-1').html($('.cont-christ-action .cont-tv-pack-xl.pack-2 > img:lt(11)')).show();
      clearInterval(myTimer);
    }
  }, 200);
});
