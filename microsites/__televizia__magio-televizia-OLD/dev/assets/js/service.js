var jsonURL = '/documents/10179/16430314/data.json',
testURL = '../assets/js/data.json',
URL = null,
$detailHolderTV = $('.detailHolder#check-1');
$detailHolderSAT = $('.detailHolder#check-2');

if (window.location.hostname === 'localhost') {
    URL = testURL;
} else {
    URL = jsonURL;
}

function magentaRibbon(i, oblubene) {
  if ((oblubene == true)) {
    $('.detail-wrapper').eq(i).addClass('magenta');
    $('.detail-wrapper-mobile').eq(i).addClass('magenta-mobile');
    $('.ribbon').eq(i).show();
    $('.ribbon-mobile').eq(i).show();
  } else {
    $('.ribbon').eq(i).hide();
    $('.detail-wrapper').eq(i).removeClass('magenta');
    $('.detail-wrapper-mobile').eq(i).removeClass('magenta-mobile');
    $('.ribbon-mobile').eq(i).hide();
  }
}

function dataTV(datatv) {
  var TV = '<div class="detail-wrapper">' +
  '<p class="ribbon">OBĽÚBENÁ TV</p>' +
    '<div class="col-md-3 col-sm-3 text-center separator-right">' +
      '<p class="detailHeading">'+ datatv.heading +'</p>' +
      '<p class="detailPrice">'+ datatv.price +'€</p><img class="discount" src="../assets/img/discount10.png" alt="">' +
    '</div>' +
    '<div class="col-md-2 col-sm-2 text-center separator-right">' +
      '<p class="detailHeading-stanice">'+ datatv.stanice +'</p>' +
      '<img src="../assets/img/'+ datatv.staniceImg +'.png" alt="">' +
    '</div>' +
    '<div class="col-md-4 col-sm-4 text-center separator-right">' +
      '<div class="popis-wrap">' +
        '<p class="detail-popis">'+ datatv.vyhody1 +'</p>' +
        '<p class="detail-popis">'+ datatv.vyhody2 +'</p>' +
        '<p class="detail-popis">'+ datatv.vyhody3 +'</p>' +
      '</div>' +
    '</div>'+
    '<div class="col-md-3 col-sm-3 text-right">' +
      '<div class="cta-wrap">' +
        '<div><a href="'+ datatv.href+'" class="btn cta btnmag dostupnostPopup">OVERIŤ DOSTUPNOSŤ</a></div>' +
        '<div><a class="link" href="'+ datatv.detailHref +'">Zobraziť detail</a></div>' +
      '</div>' +
    '</div>' +
    // end of detail-wrapper
    '</div>' +
    
    // start of mobile-wrapper
    '<div class="detail-wrapper-mobile">' +
    '<p class="ribbon-mobile">OBĽÚBENÁ TV</p>'+
      '<div class="left-mobile">' +
        '<p class="detailHeading-stanice">'+ datatv.stanice +'</p>' +
        '<img src="../assets/img/'+ datatv.staniceImg +'.png" alt="">' +
      '</div>' +
      '<div class="right-mobile">' +
      '<div class="mobile-redirect" data-href="'+datatv.detailHref+'">'+
        '<p class="detailHeading">'+ datatv.heading +'</p>'+
        '<div class="popis-wrap">'+
          '<p class="detail-popis">'+ datatv.vyhody1 +'</p>'+
          '<p class="detail-popis">'+ datatv.vyhody2 +'</p>'+
          '<p class="detail-popis">'+ datatv.vyhody3 +'</p>'+
          '<a class="arrowBtn" href="'+ datatv.detailHref +'"><img src="../assets/img/ico-plus.png"></a>' +
        '</div>'+
        '<div class="priceinfo">'+
          '<div class="price-left">'+
            '<p class="price-mesacne">Mesačne</p>'+
            '<a class="price-zlava" id="'+ datatv.vyhodnejsiBalik +'">2. a 3. mes. s web zľavou</a>'+
            '<p class="price-zvysne">Zvyšné mesiace</p>'+
          '</div>'+
          '<div class="price-right">'+
            '<p></p>'+
            '<p class="price-mesacne-zlava">'+ datatv.priceMobileZlava +' €</p>'+
            '<p class="price-zvysnemesiace">'+ datatv.priceMobileMesacne +' €</p>'+
          '</div>'+
        '</div>'+
      '</div>'+
          '<a href="'+ datatv.href +'" class="btn cta dostupnostPopup">OVERIŤ DOSTUPNOSŤ</a>'+
      '</div>'+
    '</div>'

    return TV;
}

/* DIV DATA-HREF */
$(document).on('click','.mobile-redirect', function(){
  window.location.href = $(this).attr('data-href');
});

var jsonData;

$.ajax({
    type: "GET",
    async: true,
    crossDomain: true,
    dataType: "json",
    url: URL,
    mimeType: "application/json",
    success: function (data) {

      var itemsLength = data.tv.length;
      //console.log(itemsLength)
      for(var i = 0; i < itemsLength; i++) {
        dataTV(data.tv[i])

        var zdroj = data.tv[i].zdroj;
        if (zdroj == "SAT") {
          //append to "cez satelit"
          $detailHolderSAT.append(dataTV(data.tv[i]));
        } else {
          //append to "cez kabel"
          $detailHolderTV.append(dataTV(data.tv[i]));
        }




      //if oblubene = true, add magenta border and ribbon START
      jsonData = {
        "oblubene" : data.tv[i].oblubene,
        "dostupnost" : data.tv[i].dostupnost
      };
      //console.log(jsonData.oblubene);

      magentaRibbon(i, data.tv[i].oblubene);
      //if oblubene = true, add magenta border and ribbon END

      //qtipS for MAGIO SAT
      $('#vyhodnejsiBalikM').qtip({
            content: 'Vezmite si k televízii aj Magio internet a/alebo Pevnú linku a ušetríte! Kombináciu viacerých služieb získate výhodnejšiu cenu. Po overení dostupnosti a prechode k nákupu si môžete vyskladať svoj balík služieb a spoznať výhodné ceny.',
            style: {
                classes: 'qtip-tipsy'
            },
            position: {
                corner: {
                    target: 'leftMiddle',
                    tooltip: 'topMiddle'
                }
            }
        });
      $('#vyhodnejsiBalikL').qtip({
          content: 'Vezmite si k televízii aj Magio internet a/alebo Pevnú linku a ušetríte! Kombináciu viacerých služieb získate výhodnejšiu cenu. Po overení dostupnosti a prechode k nákupu si môžete vyskladať svoj balík služieb a spoznať výhodné ceny.',
          style: {
              classes: 'qtip-tipsy'
          },
          position: {
              corner: {
                  target: 'leftMiddle',
                  tooltip: 'topMiddle'
              }
          }
      });
      $('#vyhodnejsiBalikXL').qtip({
            content: 'Vezmite si k televízii aj Magio internet a/alebo Pevnú linku a ušetríte! Kombináciu viacerých služieb získate výhodnejšiu cenu. Po overení dostupnosti a prechode k nákupu si môžete vyskladať svoj balík služieb a spoznať výhodné ceny.',
            style: {
                classes: 'qtip-tipsy'
            },
            position: {
                corner: {
                    target: 'leftMiddle',
                    tooltip: 'topMiddle'
                }
            }
        });
      //qtipS for MAGIO SAT END

      $('.info-pocetTv').qtip({
        content: 'Počet televízorov, na ktorých môžete sledovať televíziu je závislý od konkrétnej technológie pokrytia vo vašej lokalite.',
        style: {
          classes: 'qtip-tipsy'
        },
        position: {
          corner: {
            target: 'leftMiddle',
            tooltip: 'topMiddle'
          }
        }
      });


    }; //end of for loop

    //reset after click on "zmazat filter"
    $('.filter').click(function(e){
      e.preventDefault();
      $detailHolderSAT.html('');
      $detailHolderTV.html('');

      for(var i = 0; i < itemsLength; i++) {
        if (data.tv[i].zdroj == "SAT") {
          //append to "cez satelit"
          //console.log("zdroj SAT")
          $detailHolderSAT.append(dataTV(data.tv[i]));
          $detailHolderSAT.hide();
        } else {
          //console.log("zdroj TV")
          //append to "cez kabel"
          $detailHolderTV.append(dataTV(data.tv[i]));
        }

        magentaRibbon(i, data.tv[i].oblubene);
        //if oblubene = true, add magenta border and ribbon END
      }
    });



 } //end of ajaxSuccess
}); //end of ajaxCall
