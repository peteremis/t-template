/* checker */
$(function() {
  var address = '',
    addressType = '',
    lat = '',
    long = '',
    internet = '',
    tv = '',
    date = '',
    marker = '',
    availability = '',
    $cityForm = $('#city-form'),
    $cityInput = $('.city-input'),
    $cityResultList = $('.city-form__result_list'),
    $streetForm = $('#street-form'),
    $streetInput = $('.street-input'),
    $streetResultList = $('.street-form__result_list'),
    $numberInput = $('.number-input'),
    $btn = $('.sendReq'),
    $errorMsg = $('.error-msg'),
    $infoMsg = $('.info-msg'),
    $mapHolder = $('.map-holder'),
    $resetBtn = $('.reset'),
    isMapInitialised = false,
    hasStreet = true,
    cityId = '',
    streetId = '',
    isInfoVisible = false,
    isErrorVisible = false,
    boxElem = '',
    /*
    	URL_CITY = 'https://backvm.telekom.sk/shop/ins/?searchTermKey=citySubstring',
    	URL_STREET = 'https://backvm.telekom.sk/shop/ins/?searchTermKey=streetSubstring',
    	URL_FINAL = 'https://backvm.telekom.sk/shop/ins/?searchTermKey=streetNoSubstring',
    */
    URL_CITY = 'https://backvm.telekom.sk/shop/ins/index3.php?searchTermKey=citySubstring',
    URL_STREET = 'https://backvm.telekom.sk/shop/ins/index3.php?searchTermKey=streetSubstring',
    URL_FINAL = 'https://backvm.telekom.sk/shop/ins/index3.php?searchTermKey=streetNoSubstring',
    MIN_KEY_DOWN = 3,
    MSG_CANT_CONNECT = 'Nepodarilo sa načítať informácie, skúste to prosím ešte raz.',
    MSG_CANT_FIND_ADDRESS = 'Na zadanej adrese sme nenašli plánované pripojenia. Pre ďalšie overenie pokračujte na túto <a href="/chytry-balik">stránku</a>.',
    API_KEY = 'AIzaSyD4BHKV9ijIn6nCRo8QBbFfW3eepI3LAl4';


  function hideList($input) {
    var $tempParent = $input.next('.results');
    $tempParent.hide();
  }

  function showList($input) {
    var $tempParent = $input.next('.results');
    $tempParent.show();
  }

  function emptyElem($elem) {
    $($elem).html('');
  }

  function addTextToInput($input, text) {
    $input.val(text);
  }

  function clearInput($input) {
    $input.val('');
  }

  function showErrorMsg(msg) {
    $errorMsg.text(msg);
    isErrorVisible = true;
  }

  function hideErrorMsg() {
    $errorMsg.text('');
    isErrorVisible = false;
  }

  $('.msg-holder').hide();

  function showInfoMsg(msg) {
    $('.msg-holder').show();
    $infoMsg.html(msg);
    isInfoVisible = true;
  }

  function hideInfoMsg() {
    $infoMsg.html('');
    isInfoVisible = false;
  }

  function hideMap() {
    $mapHolder.hide();
  }

  function showMap() {
    $mapHolder.show();
  }

  function preventSubmitOnEnterPress($input) {
    $input.keypress(function(e) {
      var code = (e.keyCode ? e.keyCode : e.which);
      if (code == 13) {
        return false;
      }
    });
  }

  function clearResultOnArrowKey($input, $result) {
    $input.keydown(function(e) {
      var code = (e.keyCode ? e.keyCode : e.which);
      if (code == 38 || code == 40) {
        emptyElem($result);
      }
    });
  }

  function resetOnBackSpace($input, resetCityInput, resetStreetInput) {
    $input.on('keydown', function(e) {
      if (e.which == 8 || e.which == 46) {
        if (resetCityInput) {
          resetFields();
        } else if (resetStreetInput) {
          clearInput($numberInput);

          emptyElem($streetResultList);

          switchInputState($numberInput, true);

          hideMap();
          emptyElem('.box');
        }
      }
    });
  }

  function resetFields() {
    clearInput($cityInput);
    clearInput($streetInput);
    clearInput($numberInput);

    emptyElem($cityResultList);
    emptyElem($streetResultList);

    switchInputState($streetInput, true);
    switchInputState($numberInput, true);

    hideMap();
    emptyElem('.box');
  }

  function bindReset($elem) {
    $elem.on('click', function(e) {
      e.preventDefault();
      resetFields();
    });
  }

  var generateCityList = function(results) {
    var htmlToAppend = '';
    for (var i = 0; i < results.length; i++) {
      //console.log(results);

      if (results[i].numOfStreets == 0) {
        hasStreet = false;
      } else {
        hasStreet = true;
      }

      htmlToAppend += '<li class="' + (hasStreet ? 'hasStreet' : '') + '" data-id="' + results[i].id + '">' + results[i].name + '</li>';
    }

    return htmlToAppend;
  }

  var generateStreetList = function(results) {
    var htmlToAppend = '';
    for (var i = 0; i < results.length; i++) {
      htmlToAppend += '<li data-id="' + results[i].id + '">' + results[i].street_name + '</li>';
    }

    return htmlToAppend;
  }

  function generateBox() {

    var url = '<a id="zaujem" class="button scroll-to" href="#sec-2" style="display: inline-block;">Mám záujem</a>';
    if (addressType == 'real') {
      url = '<a class="button" href="https://www.telekom.sk/fix/objednavka/-/scenario/e-shop/chytry-balik?invoiceNr=&city=' + $cityInput.val() + '&zipCode=&street=' + $streetInput.val() + '&streetNum=' + $numberInput.val() + '&usePlannedTechnologies=false" style="display: inline-block;">Mám záujem</a>';
    }

    boxElem = '<div id="ulicebox" class="ulicebox"><div class="row">' + address + '</div></div>' +
      '<div class="sluzby" style="display: block;"><div id="sluzby-header">DOSTUPNÉ SLUŽBY</div>' +
      '<div id="internet" class="internet"><img src="https://m.telekom.sk/wb/novepokrytie/img/icon_internet.png"><span class="black">Magio Internet</span> ' + internet + '</div>' +
      '<div id="tv" class="tv"><img src="https://m.telekom.sk/wb/novepokrytie/img/icon_tv.png"><span class="black">Magio televízia</span> ' + tv + '</div></div>' +
      '<div class="zaujem" style="display: block;">' +
      url +
      '</div>';
  }

  var dostupneSluzby;
  dostupne = 'Na overenej adrese sú dostupné nižšie uvedené služby.';
  nedostupne = 'Na overenej adrese nie sú dostupné služby.'

  function generateInfoDostupnost(dostupnostSluzby) {
    dostupneSluzby = '<div class="commitbox">' +
      '<p>' + dostupnostSluzby + '</p>' +
      '</div>';
  }

  function generateInfoNedostupnost(dostupnostSluzby) {
    nedostupneSluzby = '<div class="errorbox">' +
      '<p>' + dostupnostSluzby + '</p>' +
      '</div>';
  }


  function bindClickEvent($result, $input) {
    $result.on('click', function(e) {
      e.preventDefault();

      var choosenElem = $(e.target).text();
      $input.val(choosenElem);

      emptyElem($result);
      hideList($input);

      if ($input.hasClass('city-input')) {

        if ($(e.target).hasClass('hasStreet')) {
          switchInputState($streetInput);
          cityId = $(e.target).data('id');
        } else {
          addTextToInput($streetInput, 'Obec nemá ulice');
        }
        switchInputState($numberInput);
      } else if ($input.hasClass('street-input')) {
        streetId = $(e.target).data('id');
        switchInputState($numberInput);
      }

    });
  }

  function bindKeyup($input, $result) {
    $input.on('keyup', function() {
      if (isErrorVisible) {
        hideErrorMsg();
      }
      if (isInfoVisible) {
        hideInfoMsg();
      }

      if ($(this).val() == '') {
        emptyElem($result);

        if ($input.hasClass('city-input')) {
          clearInput($streetInput);
          clearInput($numberInput);
          switchInputState($streetInput, true);
          switchInputState($numberInput, true);
          switchBtnState($btn, true);
        } else if ($input.hasClass('street-input')) {
          clearInput($numberInput);
          switchInputState($numberInput, true);
          switchBtnState($btn, true);
        }
      }
    });
  }

  function bindNumKeyUp() {
    $numberInput.on('keyup', function() {
      if ($(this).val() != '') {
        switchBtnState($btn, false);
      } else {
        switchBtnState($btn, true);
      }
    });
  }

  function switchInputState($input, disable) {
    if (disable) {
      if (!$input.attr('disabled')) {
        $input.attr('disabled', true);
      }
      if (!$input.hasClass('disabled')) {
        $input.addClass('disabled');
      }
    } else {
      if ($input.attr('disabled')) {
        $input.attr('disabled', false);
      }
      if ($input.hasClass('disabled')) {
        $input.removeClass('disabled');
      }
    }
  }

  function switchBtnState($btn, disable) {
    if (disable) {
      if (!$btn.attr('disabled')) {
        $btn.attr('disabled', true);
      }
      if (!$btn.hasClass('disabled')) {
        $btn.addClass('disabled');
      }
    } else {
      if ($btn.attr('disabled')) {
        $btn.attr('disabled', false);
      }

      if ($btn.hasClass('disabled')) {
        $btn.removeClass('disabled');
      }
    }
  }

  function hideDropdownIfClickedElsewhere() {
    $(document).mouseup(function(e) {
      var container = $('.results'),
        elemToHide = $('.results ul');

      if (!container.is(e.target) && container.has(e.target).length === 0) {
        elemToHide.html('');
      }
    });
  }

  function bindAutocomplete($input, $result, addCityId, generateFunction, url, minLength) {
    $input.autocomplete({
      source: function(request, response) {
        $input.addClass('loading');
        emptyElem($result);

        var cityData = {
            searchTermValue: request.term
          },
          streetData = {
            searchTermValue: request.term,
            cityId: cityId
          },
          tempData = {};
        //console.log(tempData);
        addCityId ? $.extend(true, tempData, streetData) : $.extend(true, tempData, cityData)


        $.ajax({
          url: url,
          data: tempData,
          dataType: "json",
          success: function(data) {
            //console.log(data);
            if (data.length == 0) {
              showInfoMsg(MSG_CANT_FIND_ADDRESS);
            }

            var temphtmlToAppend = '';

            temphtmlToAppend = generateFunction(data);

            $(temphtmlToAppend).appendTo($result);

            showList($input);

            $input.removeClass('loading');
          },
          error: function() {
            showErrorMsg(MSG_CANT_CONNECT);
            $input.removeClass('loading');
          }
        });
      },
      minLength: minLength
    });
  }


  function submitAddress() {

    // zobraz "loading squares" pri nacitavani udajov
    setTimeout(function(){
      $('#ajaxLoading-AjaxLoader_assetsListGlobal').show();
    }, 500);

    var tempVal = $numberInput.val();

    $.ajax({
      type: "GET",
      async: true,
      crossDomain: true,
      dataType: "json",
      data: {
        searchTermValue: tempVal,
        cityName: $cityInput.val(),
        streetName: $streetInput.val(),
        cityId: cityId,
        streetId: streetId
      },
      url: URL_FINAL,

      success: function(response) {

      //create cookie if Checker used
        function setCookie(cname, cvalue) {
          var expires = "expires=0";
          document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
      }
      setCookie("CustomerChecked", true);

        //console.log(response);
        // schovaj "loading squares" po nacitani
        setTimeout(function(){
          $('#ajaxLoading-AjaxLoader_assetsListGlobal').hide();
        }, 500);
        
        if (typeof(response.address) === "undefined" || response.address == null) {
          showInfoMsg(MSG_CANT_FIND_ADDRESS);

          $.fancybox({
            href: '#not-found',
            modal: true
          });

          //ak nie je ziadna sluzba dostupna schovaj vsetky nedostupne sluzby a zobraz info so spravou
          $(".detail-wrapper").hide();
          $(".detail-wrapper-mobile").hide();
          generateInfoNedostupnost(nedostupne);
          if ($('.errorbox').length) {
            return;
          } else {
            $(nedostupneSluzby).insertAfter('.ttp'); //cerveny error box
          }
          $('.commitbox').remove(); //zmaz zeleny success box

          return false;

        } else {

          address = response.address;
          addressType = response.addressType;
          internet = response.internet;
          tv = response.tv;
          availability = response.date;

          //zeleny success box - dostupne sluzby
          generateInfoDostupnost(dostupne); //zeleny success box
          $('.errorbox').remove(); //zmaz cerveny error box
          if ($('.commitbox').length) {
            return;
          } else {
            $(dostupneSluzby).insertAfter('.ttp'); //zeleny success box
          }

          //generateBox();
          $('.box').empty();
          //$(boxElem).appendTo('.box');

          showMap();

          //ajax call for data.json
          //console.log(jsonData.dostupnost);
          $.ajax({
            type: "GET",
            async: true,
            crossDomain: true,
            dataType: "json",
            url: URL,
            mimeType: "application/json",
            success: function(data) {


              var $detailWrapper = $(".detail-wrapper");
              var $detailWrapperMobile = $(".detail-wrapper-mobile");

              $detailHolderTV.html('');
              $detailHolderSAT.html('');
              $.each(data.tv, function(i, value) {
                
                //zobraz dostupne sluzby ak sa "dostupnost" z data.json = "response.availability.tv"

                //////////////////////////////////////////////////////////
                // v pripade, ze pevne sluzby su dostupne zobraz ich
                //////////////////////////////////////////////////////////
                if ((value.m == 'yes' && response.availability.tv.M == 'yes') ||
                  (value.l == 'yes' && response.availability.tv.L == 'yes') ||
                  (value.xl == 'yes' && response.availability.tv.XL == 'yes')
                  // (value.satm == 'yes' && response.availability.tv.M == 'yes') ||
                  // (value.satl == 'yes' && response.availability.tv.L == 'yes') ||
                  // (value.satxl == 'yes' && response.availability.tv.XL == 'yes')
                  ){

                  $detailHolderTV.append(dataTV(data.tv[i]));
                  $detailHolderTV.show();

                  magentaRibbon(i, data.tv[i].oblubene);

                  //ak sirka >550px zobraz wrapper ak je menej, zobraz mobile-wrapper
                  if ($(window).width() > 550) {
                    $($detailWrapper[i]).show();
                  } else {
                    $($detailWrapperMobile[i]).show();
                  }

                  // po overeni, zmen URL adresy a text TV buttonov "PREJST K NAKUPU"
                  var tvBuyBtn = $('.detail-wrapper')[i];
                      SATBuyBtn = $('.detail-wrapper-mobile')[i];
                  //$(tvBuyBtn).find('.dostupnostPopup').attr('href', data.tv[i].hrefAfterChecked).text("PREJSŤ K NÁKUPU"); 
                  //$(SATBuyBtn).find('.dostupnostPopup').attr('href', data.tv[i].hrefAfterChecked).text("PREJSŤ K NÁKUPU");
                  $.each($('.detail-wrapper'), function(i) {
                    $(this).find('.dostupnostPopup').attr('href', data.tv[i].hrefAfterChecked).text("PREJSŤ K NÁKUPU");
                    $(this).find('.cta-wrap').attr('href', data.tv[i].detailHref);
                  });
                  $.each($('.detail-wrapper-mobile'), function(i) {
                    $(this).find('.dostupnostPopup').attr('href', data.tv[i].hrefAfterChecked).text("PREJSŤ K NÁKUPU");
                    $(this).find('.arrowBtn').attr('href', data.tv[i].detailHref);
                  });
                  //$('.cta-wrap, .arrowBtn').find('.link').attr('href', data.tv[i].detailHref);

                } else {
                  //schovaj iba nedostupne sluzby
                  $($detailWrapperMobile[i]).hide();
                  $($detailWrapper[i]).hide();
                }


                //////////////////////////////////////////////////////////
                //v pripade ze pevne sluzby nie su dostupne zobraz len SAT
                //////////////////////////////////////////////////////////
                if ((value.satm == 'yes' && response.availability.tv.M == 'no') ||
                  (value.satl == 'yes' && response.availability.tv.L == 'no') ||
                  (value.satxl == 'yes' && response.availability.tv.XL == 'no')) {

                  $detailHolderSAT.append(dataTV(data.tv[i]));
                  $detailHolderSAT.show();

									//magenta border a ribbon pre oblubene
                  for (var i = 0; i < data.tv.length; i++) {
                    magentaRibbon(i, data.tv[i].oblubene);
                  }

                  //ak sirka >550px zobraz wrapper ak je menej, zobraz mobile-wrapper
                  if ($(window).width() > 550) {
                    $($detailWrapper[i]).show();
                  } else {
                    $($detailWrapperMobile[i]).show();
                  }

                  // po overeni, zmen URL adresy a text SAT buttonov "PREJST K NAKUPU"
                  $.each($('.detail-wrapper'), function(i) {
                    $(this).find('.dostupnostPopup').attr('href', data.tv[i+3].hrefAfterChecked).text("PREJSŤ K NÁKUPU");
                    $(this).find('.cta-wrap').attr('href', data.tv[i+3].hrefAfterChecked);
                  });
                  
                  $.each($('.detail-wrapper-mobile'), function(i) {
                    $(this).find('.dostupnostPopup').attr('href', data.tv[i+3].hrefAfterChecked).text("PREJSŤ K NÁKUPU");
                    $(this).find('.arrowBtn').attr('href', data.tv[i+3].hrefAfterChecked);
                  });
                  

                } else {
                  //schovaj iba nedostupne sluzby
                  $($detailWrapperMobile[i]).hide();
                  $($detailWrapper[i]).hide();
                } 


              }); //end of for.each
              //console.log(data)
            } //end of ajaxSuccess
          });
        }
      },
      error: function() {
        showErrorMsg(MSG_CANT_CONNECT);
      }
    });
  }

  // po kliknuti na OVERIT
  $btn.on('click', function() {

    //create sessionStorage with checked city, adress and adressNumber.
    var cityCookie = $('.city-input').val();
    var adressCookie = $('.street-input').val();
    var adressNumberCookie = $('.number-input').val();
    sessionStorage.setItem("cityCookie", cityCookie);
    sessionStorage.setItem("adressCookie", adressCookie);
    sessionStorage.setItem("adressNumberCookie", adressNumberCookie);

//     // nakupny scenar - autocomplete
//     // https://www.telekom.sk/fix/objednavka/-/scenario/e-shop/chytry-balik?televizia=m

//     // if cookie exist
//     if (sessionStorage.getItem("cityCookie").length) {

//       //text-typing function
//       function showLetter(field, text, delay) {
//         //$(field).val(text.substring(0, 1));
//         var interval = setInterval(function() {
//           var len = ($(field).val().length || 0) +1;
//           $(field).val(text.substring(0, len));
          
//           if(len === text.length)
//             window.clearInterval(interval);
//         }, delay);
//       }

//       //type text to cityField
//       setTimeout(function(){
//         $("input[name='order.serviceAddress.city']").val("");
//         showLetter($("input[name='order.serviceAddress.city']"), sessionStorage.getItem("cityCookie"), 5);
//         $("input[name='order.serviceAddress.city']").blur();
//       },100); 

//       //type text to adressField
//        setTimeout(function(){
//         $("input[name='order.serviceAddress.street']").val("");
//         showLetter($("input[name='order.serviceAddress.street']"), sessionStorage.getItem("adressCookie"), 5);
//         $("input[name='order.serviceAddress.street']").blur();
//       },300); 

//       //type text to adressNumberField
//       setTimeout(function(){
//         $("input[name='order.serviceAddress.streetNr']").val("");
//         showLetter($("input[name='order.serviceAddress.streetNr']"), sessionStorage.getItem("adressNumberCookie"), 5);
//         $("input[name='order.serviceAddress.streetNr']").blur();
//       },500); 

//       //refocus all fields
//       $("input[name='order.serviceAddress.city']").blur();
//       $("input[name='order.serviceAddress.street']").blur();
//       $("input[name='order.serviceAddress.streetNr']").blur();

//       //again set adressNumberField
//       setTimeout(function(){
//         showLetter($("input[name='order.serviceAddress.streetNr']"), sessionStorage.getItem("adressNumberCookie"), 5);
//         $("input[name='order.serviceAddress.city']").blur();
//         $("input[name='order.serviceAddress.street']").blur();
//         $("input[name='order.serviceAddress.streetNr']").blur();
//       },600); 

//       //click on "overit"
//       setTimeout(function(){
//         $("#order-orderPurpose-activation").click().click();
//         $("#checker_button").click();
//       },1200);

//       $("#checker_button").click();
// }

    submitAddress();
    $('#availability-module').hide(); //schovaj formular na zadavanie adresy
    $('.confirmation-btn').hide(); //schovaj btn "overit dosupnost na mojej adrese"
    $('.reset-confirm').removeClass('hidden'); //zobraz tlacidla na "zmenu adresy" a "zrusenie filtra"
    $('#secondaryRadio').hide(); //schovaj tlacidla "cez kabel" a "cez satelit"
    $('.showtv-p').hide();

    // append detail-wrapper & detail-wrapper-mobile to Holder
    //$(html).appendTo($detailHolderTV);
  });




  switchInputState($streetInput, true);
  switchInputState($numberInput, true);
  switchBtnState($btn, true);

  bindAutocomplete($cityInput, $cityResultList, false, generateCityList, URL_CITY, MIN_KEY_DOWN);
  bindAutocomplete($streetInput, $streetResultList, true, generateStreetList, URL_STREET, MIN_KEY_DOWN);

  preventSubmitOnEnterPress($cityInput);
  preventSubmitOnEnterPress($streetInput);
  preventSubmitOnEnterPress($numberInput);

  resetOnBackSpace($cityInput, true, false);
  resetOnBackSpace($streetInput, false, true);

  bindClickEvent($cityResultList, $cityInput);
  bindKeyup($cityInput, $cityResultList);

  bindClickEvent($streetResultList, $streetInput);
  bindKeyup($streetInput, $streetResultList);

  bindReset($resetBtn);

  bindNumKeyUp();

  hideDropdownIfClickedElsewhere();

  $('.not-found-form').show();
  $('.not-found-success').hide();

  $('#not-found-send').on('click', function(e) {

    var citiVal = $cityInput.val();
    var streetVal = $streetInput.val();
    var numberVal = $numberInput.val();

    var internetVal = $('#nf-internet').attr('checked');
    var tvVal = $('#nf-tv').attr('checked');
    var pevnaVal = $('#nf-pevna').attr('checked');
    var phoneVal = $('#nf-phone').val();

    $.ajax({
      type: "GET",
      async: true,
      crossDomain: true,
      dataType: "json",
      data: {
        phone: phoneVal,
        mesto: citiVal,
        ulica: streetVal,
        cislo: numberVal,
        tv: tvVal,
        internet: internetVal,
        pevna: pevnaVal
      },
      url: "https://backvm.telekom.sk/www/checker/email.php",

      success: function(response) {
        $('.not-found-form').hide();
        $('.not-found-success').show();
      },
      error: function() {
        showErrorMsg(MSG_CANT_CONNECT);
      }
    });


  });

});
