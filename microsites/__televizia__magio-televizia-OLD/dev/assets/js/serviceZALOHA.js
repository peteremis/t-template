var jsonURL = '../assets/js/data.json',
testURL = '../assets/js/data.json',
URL = null,
$detailHolderTV = $('.detailHolder#check-1');
$detailHolderSAT = $('.detailHolder#check-2');

if (window.location.hostname === 'localhost') {
    URL = testURL;
} else {
    URL = jsonURL;
}


var jsonData;

$.ajax({
    type: "GET",
    async: true,
    crossDomain: true,
    dataType: "json",
    url: URL,
    mimeType: "application/json",
    success: function (data) {

  var itemsLength = data.tv.length;
  console.log(itemsLength)
  for(var i = 0; i < itemsLength; i++) {
      html = $([
        '<div class="detail-wrapper">' +
        '<p class="ribbon">OBĽÚBENÁ TV</p>' +
          '<div class="col-md-3 col-sm-3 text-center separator-right">' +
            '<p class="detailHeading">'+ data.tv[i].heading +'</p>' +
            '<p class="detailPrice">'+ data.tv[i].price +'€</p><img class="discount" src="../assets/img/discount10.png" alt="">' +
          '</div>' +
          '<div class="col-md-2 col-sm-2 text-center separator-right">' +
            '<p class="detailHeading-stanice">'+ data.tv[i].stanice +'</p>' +
            '<img src="../assets/img/'+ data.tv[i].staniceImg +'.png" alt="">' +
          '</div>' +
          '<div class="col-md-4 col-sm-4 text-center separator-right">' +
            '<div class="popis-wrap">' +
              '<p class="detail-popis">▪ '+ data.tv[i].vyhody1 +'</p>' +
              '<p class="detail-popis">▪ '+ data.tv[i].vyhody2 +'</p>' +
              '<p class="detail-popis">▪'+ data.tv[i].vyhody3 +'</p>' +
            '</div>' +
          '</div>'+
          '<div class="col-md-3 col-sm-3 text-right">' +
            '<div class="cta-wrap">' +
              '<div><a href="'+ data.tv[i].href+'" class="btn cta btnmag">PREJSŤ K NÁKUPU</a></div>' +
              '<div><a class="link" href="'+ data.tv[i].detailHref +'">Zobraziť detail</a></div>' +
            '</div>' +
          '</div>'+
          // end of detail-wrapper
          '</div>' +
          // start of mobile-wrapper
          '<div class="detail-wrapper-mobile">' +
          '<p class="ribbon-mobile">OBĽÚBENÁ TV</p>'+
            '<div class="left-mobile">' +
              '<p class="detailHeading-stanice">'+ data.tv[i].stanice +'</p>' +
              '<img src="../assets/img/'+ data.tv[i].staniceImg +'.png" alt="">' +
            '</div>' +
            '<div class="right-mobile">' +
              '<p class="detailHeading">'+ data.tv[i].heading +'</p>'+
              '<div class="popis-wrap">'+
                '<p class="detail-popis">▪ '+ data.tv[i].vyhody1 +'</p>'+
                '<p class="detail-popis">▪ '+ data.tv[i].vyhody2 +'</p>'+
                '<a class="arrowBtn" href="'+ data.tv[i].detailHref +'">></a>' +
              '</div>'+
              '<div class="priceinfo">'+
                '<div class="price-left">'+
                  '<p class="price-mesacne">Mesačne</p>'+
                  '<a class="price-zlava" id="'+ data.tv[i].vyhodnejsiBalik +'">2. a 3. mes. s web zľavou</a>'+
                  '<p class="price-zvysne">Zvyšné mesiace</p>'+
                '</div>'+
                '<div class="price-right">'+
                  '<p></p>'+
                  '<p class="price-mesacne-zlava">'+ data.tv[i].priceMobileZlava +' €</p>'+
                  '<p class="price-zvysnemesiace">'+ data.tv[i].priceMobileMesacne +' €</p>'+
                '</div>'+
              '</div>'+
                '<a href="'+ data.tv[i].detailHref +'" class="btn cta">PREJSŤ K NÁKUPU</a>'+
            '</div>'+
          '</div>'
      ].join("\n"));

      // append itemWrapper to Holder
      //$(html).appendTo($detailHolderSAT);
      if (data.tv[i].zdroj == "SAT") {
        $(html).appendTo($detailHolderSAT);
      } else {
        $(html).appendTo($detailHolderTV);
      }

      //if oblubene = true, add magenta border and ribbon
      jsonData = {
        "oblubene" : data.tv[i].oblubene,
        "dostupnost" : data.tv[i].dostupnost
      };
      //console.log(jsonData.oblubene);

      if (jsonData.oblubene == true) {
        $('.detail-wrapper').eq(i).addClass('magenta');
        $('.detail-wrapper-mobile').eq(i).addClass('magenta-mobile');
        $('.ribbon').eq(i).show();
        $('.ribbon-mobile').eq(i).show();
      } else {
        $('.ribbon').eq(i).hide();
        $('.detail-wrapper').eq(i).removeClass('magenta');
        $('.detail-wrapper-mobile').eq(i).removeClass('magenta-mobile');
        $('.ribbon-mobile').eq(i).hide();
      }
      //if oblubene = true, add magenta border and ribbon END

      //qtipS for MAGIO SAT
      $('#vyhodnejsiBalikM').qtip({
            content: 'Vezmite si k televízii aj Magio internet a/alebo Pevnú linku a ušetríte! Kombináciu viacerých služieb získate výhodnejšiu cenu. Po overení dostupnosti a prechode k nákupu si môžete vyskladať svoj balík služieb a spoznať výhodné ceny.',
            style: {
                classes: 'qtip-tipsy'
            },
            position: {
                corner: {
                    target: 'leftMiddle',
                    tooltip: 'topMiddle'
                }
            }
        });
      $('#vyhodnejsiBalikL').qtip({
          content: 'Vezmite si k televízii aj Magio internet a/alebo Pevnú linku a ušetríte! Kombináciu viacerých služieb získate výhodnejšiu cenu. Po overení dostupnosti a prechode k nákupu si môžete vyskladať svoj balík služieb a spoznať výhodné ceny.',
          style: {
              classes: 'qtip-tipsy'
          },
          position: {
              corner: {
                  target: 'leftMiddle',
                  tooltip: 'topMiddle'
              }
          }
      });
      $('#vyhodnejsiBalikXL').qtip({
            content: 'Vezmite si k televízii aj Magio internet a/alebo Pevnú linku a ušetríte! Kombináciu viacerých služieb získate výhodnejšiu cenu. Po overení dostupnosti a prechode k nákupu si môžete vyskladať svoj balík služieb a spoznať výhodné ceny.',
            style: {
                classes: 'qtip-tipsy'
            },
            position: {
                corner: {
                    target: 'leftMiddle',
                    tooltip: 'topMiddle'
                }
            }
        });
      //qtipS for MAGIO SAT END


    }; //end of for loop

 } //end of ajaxSuccess
}); //end of ajaxCall
