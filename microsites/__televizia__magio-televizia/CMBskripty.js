<div class="fixed-link__full" id="fixed-link">
	<div class="fixed-link__table">
		<div class="fixed-link__row">
			<h5>
				Potrebujete pomôcť?</h5>
			<div class="green-tile green-tile__border cursor__hover" id="livechat">
				<a><img alt="" class="fixed-link__ico" src="/documents/10179/562752/ico-phone.png" /></a></div>
			<div class="green-tile green-tile__border cursor__hover" id="chat">
				<a><img alt="Chat" class="fixed-link__ico_chat" src="/documents/10179/562752/ico-chat.png" /></a></div>
			<div class="cursor__hover" id="toggle-chat">
				<img alt="" src="https://www.telekom.sk/documents/10179/562752/toggle-arrow.png" /></div>
		</div>
	</div>
</div>
<div class="fixed-link__compact cursor__hover hidden" id="fixed-link">
	<div class="fixed-link__table">
		<div class="fixed-link__row">
			<div class="white-tile" id="chat__compact">
				<img alt="" class="fixed-link__ico" src="/documents/10179/562752/ico-helpdesk.png" /></div>
		</div>
	</div>
</div>
<script>

$(function () {

	livechatoo.embed.init({
		lang: 'sk',
		side: 'right',
		layout: 'telekom.sk',
		page: 'hidden',
		departments: '35',
                new_rating : true,
		onload: chat_callback
	})

    var fixedLinkFull = $('.fixed-link__full'),
        fixedLinkCompact = $('.fixed-link__compact');

    if (localStorage.chatStat === 'compact') {
        fixedLinkFull.addClass('hidden');
        fixedLinkCompact.removeClass('hidden');
    }

    $('#toggle-chat').on('click', function () {
        fixedLinkFull.toggleClass('hidden');
        fixedLinkCompact.toggleClass('hidden');
        localStorage.chatStat = 'compact';
    });
    $('.fixed-link__compact').on('click', function () {
        fixedLinkFull.toggleClass('hidden');
        fixedLinkCompact.toggleClass('hidden');
        localStorage.chatStat = 'full';
    });
    $('#chat').on('click', function () {
        fixedLinkFull.toggleClass('hidden');
        livechatoo.embed.page('online');
    });
});

//////// CMB

var fixedLink = document.getElementById('fixed-link'),
    livechat = document.getElementById('livechat'),
    chat = document.getElementById('chat'),
    addZero = function (i) {
        if (i < 10) {
            i = "0" + i;
        }
        return i;
    },
    currentdate = new Date(),
    currentHour = addZero(currentdate.getHours()) + "" + addZero(currentdate.getMinutes()),
    currentDay = currentdate.getDay(),
    offTime2 = "2130",
    offTime2End = "2200",
    callOffTime = function () {
        if (currentHour >= offTime2 && currentHour <= offTime2End) {
            return true;
        } else {
            return false;
        }
    },
    cmb_callback = function (rule, cmb_type, img_name) {

        if ($('#chat-only').length == 0) {

            fixedLink.style.display = 'table-cell';
            livechat.style.display = 'table-cell';
            $(function () {
                var cmb__block = document.getElementById('cmb__block'),
                    cmbBtn = document.getElementById('cmb__btn');
                if (cmb__block !== null) {
                    cmb__block.style.display = 'inline-block';

                    if ($('#refresh__btn').length > 0) {
                        $('#refresh__btn').addClass('hidden');
                    }

                    cmbBtn.onclick = function (e) {
                        e.preventDefault();
                        livechatoo.wininv.show(rule, img_name);
                    };
                }

                if ($('.cmb__link').length > 0) {
                    $('.cmb__link').style.display = 'inline-block';
                    $('.cmb__link').on('click', function (e) {
                        e.preventDefault();
                        livechatoo.wininv.show(rule, img_name);
                    });
                }

            });

            livechat.onclick = function () {
                livechatoo.wininv.show(rule, img_name);
            };

        }
    };

//////// CMB END

//////// CHAT

var chat_callback = function (data) {

    if (data.available) {
        fixedLink.style.display = 'table-cell';
        chat.style.display = 'table-cell';
    }
};

//////// CHAT END

livechatoo.wininv.init('telekom');
livechatoo.wininv.status('telekom_manual', cmb_callback);

</script>
