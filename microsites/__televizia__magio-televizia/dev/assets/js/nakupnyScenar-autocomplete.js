$( document ).ready(function() {
// nakupny scenar - autocomplete
// https://www.telekom.sk/fix/objednavka/-/scenario/e-shop/chytry-balik?televizia=m

var cityInput = $("input[name='order.serviceAddress.city']");
adressInput = $("input[name='order.serviceAddress.street']");
adressNumberInput = $("input[name='order.serviceAddress.streetNr']");

// if session storage exist
if (sessionStorage.getItem("cityCookie").length) {

    //text-typing function
    function showLetter(field, text, delay) {
        //$(field).val(text.substring(0, 1));
        var interval = setInterval(function () {
            var len = ($(field).val().length || 0) + 1;
            $(field).val(text.substring(0, len));

            if (len === text.length)
                window.clearInterval(interval);
        }, delay);
    }

    // text to input field function
    function AddTextToInputField(inputField, sessionStorageItem, delay) {
        setTimeout(function () {
            inputField.val("");
            showLetter(inputField, sessionStorage.getItem(sessionStorageItem), 5);
            inputField.blur();
        }, delay);
    }
    //type text to cityField
    AddTextToInputField(cityInput, "cityCookie", 100);
    //type text to adressField
    AddTextToInputField(adressInput, "adressCookie", 300);
    //type text to adressNumberField
    AddTextToInputField(adressNumberInput, "adressNumberCookie", 500);

    //refocus all inputFields
    cityInput.blur();
    adressInput.blur();
    adressNumberInput.blur();

    //again set adressNumberField
    setTimeout(function(){
        AddTextToInputField(adressNumberInput, "adressNumberCookie", 500);
        cityInput.blur();
        adressInput.blur();
        adressNumberInput.blur();
    },700); 

    //after short delay, click on "checker_button"
    setTimeout(function () {
        $("#order-orderPurpose-activation").click().click();
        $("#checker_button").click();
    }, 1200);
}

});