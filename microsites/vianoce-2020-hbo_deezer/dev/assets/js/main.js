$(document).ready(function () {

  var $userCode = $('#userCode');
  var $userName = $('#userName');
  var $phoneNum = $('#phoneNum');
  var $smsCode = $('#smsCode');
  var $buttonCode = $('#send-code');

  var $otpError = $('#otp-error');
  var $otpInfo = $('#otp-info');

  var $contactForm = $("#contact-form");
  var $codeForm = $("#code-form");

  var $customerError = $("#customer-error");

  var $activated = $("#activated");
  var $activatedInfo = $(".activated-info");

  var $serviceCode = $('#serviceCode');
  var $customerType = $('#customer-type');
  var $actionId = $('#action_id');
  var $ico = $('#ico');
  var $pid = $('#pid');

  var $emailHolder = $('#email-holder');
  var $email = $('#email');

  var $serviceType = $('#service-type');
  // var serviceTypeValue = '';
  var serviceTypeValue = 'mobile';

  var $customerCheckbox = $('#customer-checkbox');

  var $serviceItems = $(".service-items");

  var smsToken = '';
  setAttribute('mobile', 1);

  var apiUrlSms = 'https://m.telekom.sk/api/vianoce2020/sms';
  var apiUrlHboCode = 'https://m.telekom.sk/api/vianoce2020/hbo';
  var apiUrlDeezerCode = 'https://m.telekom.sk/api/vianoce2020/deezer';

  if (window.location.hostname === "localhost") {
    apiUrlSms = 'http://mr.telekom.localhost/api/vianoce2020/sms';
    apiUrlHboCode = 'http://mr.telekom.localhost/api/vianoce2020/hbo';
    apiUrlDeezerCode = 'http://mr.telekom.localhost/api/vianoce2020/deezer';
    setDefault();
  }

  var apiUrlCode = apiUrlDeezerCode;
  var actionId = 'deezer';
  var serviceDesc = 'Vyberte typ služby na ktorú chcete aktivovať službu Deezer';

  $serviceItems.hide();
  $("#service-mobile").show();

  if ($actionId.val() == 'hbo') {
    apiUrlCode = apiUrlHboCode;
    actionId = 'hbo';
    serviceDesc = 'Vyberte typ služby, ku ktorej chcete aktivovať službu HBO GO';
  }

  $.listen('parsley:field:validated', function (fieldInstance) {
    if (fieldInstance.$element.is(":hidden")) {
      // hide the message wrapper
      fieldInstance._ui.$errorsWrapper.css('display', 'none');
      // set validation result to true
      fieldInstance.validationResult = true;
      return true;
    }
  });

  $('#max-price').qtip({
    content: 'Číslo adresáta nájdete  na svojej faktúre napravo hore a v Telekom appke na obrazovke Detail faktúry',
    position: {
      my: 'bottom center',
      at: 'right top'
    }
  });

  $('#service-desc').qtip({
    content: serviceDesc,
    position: {
      my: 'bottom center',
      at: 'right top'
    }
  });

  $('#pid-desc').qtip({
    content: 'Prístupový kód (PID) je jedinečný kód, ktorý je generovaný pre business zákazníkov. <br>Pokiaľ neviete, aký je váš PID, môžete zavolať na infolinku pre firemných zákazníkov na čísle 0800 123 500 alebo kontaktovať vášho obchodného reprezentanta.',
    position: {
      my: 'bottom center',
      at: 'right top'
    }
  });

  $contactForm.parsley();
  $codeForm.parsley();

  $contactForm.submit(function (e) {
    e.preventDefault();

    $customerError.hide();

    $.ajax({
      url: apiUrlSms + '?name=' + $userName.val() + '&msisdn=' + $phoneNum.val() + '&action_id=' + $actionId.val() + '&service_code=' + $serviceCode.val() + '&service_type=' + serviceTypeValue + '&customer=' + $userCode.val() + '&customer_type=' + $customerType.val() + '&email=' + $email.val(),
      type: "GET",
      async: true,
      crossDomain: true,
      dataType: "json",
      success: function (data) {
        $contactForm.hide();
        $codeForm.show();
        smsToken = data.token;
      },
      error: function (error) {
        if (error) {
          console.log('error');
        }
      }
    });
  });

  function setDefault() {
    $userName.val('Janko Hrasko');
    $phoneNum.val('903903903');
    $userCode.val('1234567891');
    $serviceCode.val('1XCLY5YP');
  }

  $customerCheckbox.click(function () {
    if ($(this).prop("checked") == true) {
      $customerType.val('b2b');
      $ico.show();

      if (actionId == 'hbo') {
        $pid.show();
      }

    } else if ($(this).prop("checked") == false) {
      $customerType.val('b2c');
      $ico.hide();
      $pid.hide();
    }
  });

  $serviceType.on('change', function () {
    var selectedOption = $(this).val();
    serviceTypeValue = selectedOption;

    if (selectedOption == 'mobile') {
      setAttribute('mobile', 0);
      $emailHolder.hide();
    } else {
      if (actionId == 'deezer') {
        $emailHolder.show();
      } else {
        $emailHolder.hide();
      }

      setAttribute('fix', 0);
    }

    var $serviceDesc = $('#service-' + serviceTypeValue);

    $serviceItems.hide();
    $serviceDesc.show();

  });

  function setAttribute(attrType, firstTime) {
    if (attrType == 'mobile') {
      $serviceCode.attr('data-parsley-type', 'integer');
      $serviceCode.attr('data-parsley-minlength', 9);
      $serviceCode.attr('data-parsley-maxlength', 10);
      $serviceCode.attr('data-parsley-type-message', 'Telefónne číslo musí obsahovať len čísla');

    } else {
      $serviceCode.attr('data-parsley-type', 'alphanum');
      $serviceCode.attr('data-parsley-minlength', 6);
      $serviceCode.attr('data-parsley-maxlength', 20);
      $serviceCode.attr('data-parsley-type-message', 'ID služby musí mať aspoň 6 znakov');

    }

    if (firstTime != 1) {
      $serviceCode.parsley().validate();
    }

  }

  $codeForm.submit(function (e) {
    e.preventDefault();

    //zobrazime informaciu o cakani na response
    $otpInfo.show();

    //schovame informaciu o OTP error
    $otpError.hide();

    $buttonCode.addClass("disabled").prop("disabled", true);
    // $buttonCode.addClass("disabled");

    $.ajax({
      url: apiUrlCode + '?token=' + smsToken + '&code=' + $smsCode.val(),
      type: "GET",
      async: true,
      crossDomain: true,
      dataType: "json",
      success: function (data) {

        $buttonCode.removeClass("disabled").prop("disabled", false);
        // $buttonCode.removeClass("disabled");

        //schovame informaciu o cakani na response
        $otpInfo.hide();

        if (data.status == 'ok') {

          //schovame OTP stranku
          $codeForm.hide();

          //zobrazime response o uspesnom zriadeni sluzby
          $activatedInfo.html(data.description);
          $activated.show();

          if (actionId == 'hbo') {
            window.location.href = "/vianoce/hbo-go-zadarmo/mam-zaujem";
          } else {
            window.location.href = "/vianoce/deezer-zadarmo/mam-zaujem";
          }

        } else {

          //schovame OTP stranku
          $codeForm.hide();
          $smsCode.val('');

          //zobrazime prvu stranku
          $contactForm.show();

          //zobrazime chybovu hlasku
          if (data.description == '') {
            $customerError.show().html('Nepodarilo sa overiť číslo adresáta. Skúste to prosím neskôr');
          } else {
            $customerError.show().html(data.description);
          }

        }

      },

      error: function (error) {
        if (error) {
          $buttonCode.removeClass("disabled").prop("disabled", false);
          // $buttonCode.removeClass("disabled");

          //schovame informaciu o cakani na response
          $otpInfo.hide();

          //zobrazime chybovu hlasku

          if (error.responseJSON.description == '') {
            $otpError.show().html('Pri orverení kódu nastala chyba. Skúste to prosím neskôr');
          } else {
            $otpError.show().html(error.responseJSON.description);
          }
        }

      }
    });
  });

  //popup
  $(".fa_popup").on("click", function () {
    var clickedBtn = $(this).attr("href");
    $.fancybox([
      {
        href: clickedBtn,
        padding: 0,
        margin: 30,
        closeBtn: false,
        width: 600,
        height: "auto",
        // autoDimensions: false,
        autoSize: false,
        parent: "#content",
        helpers: {
          overlay: {
            css: {
              background: "rgba(0, 0, 0, 0.7)"
            }
          }
        }
      }
    ]);
  });

  //popup
  $(".fa_popup").on("click", function () {
    var clickedBtn = $(this).attr("href");
    $.fancybox([
      {
        href: clickedBtn,
        padding: 0,
        margin: 30,
        closeBtn: false,
        width: 600,
        height: "auto",
        // autoDimensions: false,
        autoSize: false,
        parent: "#content",
        helpers: {
          overlay: {
            css: {
              background: "rgba(0, 0, 0, 0.7)"
            }
          }
        }
      }
    ]);
  });

  //popup
  $(".app_popup").on("click", function () {
    var clickedBtn = $(this).attr("href");
    $.fancybox([
      {
        href: clickedBtn,
        padding: 0,
        margin: 30,
        closeBtn: false,
        width: 350,
        height: "auto",
        // autoDimensions: false,
        autoSize: false,
        parent: "#content",
        helpers: {
          overlay: {
            css: {
              background: "rgba(0, 0, 0, 0.7)"
            }
          }
        }
      }
    ]);
  });

  $(".p-close, .link-close, .text-close, .close-btn").on("click", function (e) {
    e.preventDefault();
    $.fancybox.close();
  });

});