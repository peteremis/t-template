$(function() {
  /* ::: Scroll To ::: */
  $("div").on("click", ".scroll-to", function(e) {
    var parentNav = $(this)
      .closest(".main-navigation")
      .attr("id");
    var offset = 0;

    if (parentNav === "nav-sticky-custom") {
      offset = 68;
    }
    if (
      location.pathname.replace(/^\//, "") ==
        this.pathname.replace(/^\//, "") &&
      location.hostname == this.hostname
    ) {
      var target = $(this.hash);
      target = target.length ? target : $("[name=" + this.hash.slice(1) + "]");
      if (target.length) {
        if ($(window).width() > 550) {
          $("html,body").animate(
            {
              scrollTop:
                target.offset().top - $("#nav-sticky").outerHeight() - offset
            },
            1000
          );
        } else {
          $("html,body").animate(
            {
              scrollTop: target.offset().top - offset
            },
            1000
          );
        }
        return false;
      }
    }
  });

  /* ::: sticky filters ::: */
  // function sticky_relocate() {
  //   var window_top = $(window).scrollTop();
  //   var div_top = $("#sortingOptionsSection").offset().top;
  //   if (window_top > div_top) {
  //     if (!$("#sortingOptionsSection").hasClass("stick")) {
  //       $("#sortingOptionsSection").addClass("stick");
  //     }
  //   } else {
  //     $("#sortingOptionsSection").removeClass("stick");
  //   }
  // }

  // $(window).scroll(sticky_relocate);
  // sticky_relocate();
});
