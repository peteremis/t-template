$(function () {

    /* FANCYBOX */

    $(".fancybox").fancybox();


    /* STICKY NAV */

    $("#nav-sticky").sticky({
        topSpacing: 0,
        widthFromWrapper: true
    });

    $(".scroll-to").click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top - ($("#nav-sticky").outerHeight())
                }, 1000);
                return false;
            }
        }
    });

    var contentSections = $("[id*='sec-']"),
        secondaryNav = $("#nav-sticky");

    function updateSecondaryNavigation() {
        contentSections.each(function () {
            var actual = $(this),
                actualHeight = actual.height() + parseInt(actual.css('paddingTop').replace('px', '')) + parseInt(actual.css('paddingBottom').replace('px', '')),
                actualAnchor = secondaryNav.find('a[href="#' + actual.attr('id') + '"]');

            if ((actual.offset().top - secondaryNav.outerHeight() <= $(window).scrollTop()) && (actual.offset().top + actualHeight - secondaryNav.outerHeight() > $(window).scrollTop())) {
                actualAnchor.addClass('active');
            } else {
                actualAnchor.removeClass('active');
            }

        });
    }
    updateSecondaryNavigation();

    $(window).scroll(function (event) {
        updateSecondaryNavigation();
    });

    $('<div style="text-align: left; margin-left: 70px; margin-bottom: 15px;"><a href="/documents/10179/663129/Zoznam+roamingovych+partnerov.xls.xlsx">Dostupní roamingoví partneri</a></div>').appendTo('#roaming-price');
	$('<div style="text-align: left; margin-left: 70px; margin-bottom: 15px;"><a href="https://www.telekom.sk/wiki/ostatne/roaming-v-eu">Politika primeraného využívania roamingových služieb v rámci EÚ, Nórska, Islandu, Lichtenštajnska, Monaka a Andory</a></div>').appendTo('#roaming-price');
});