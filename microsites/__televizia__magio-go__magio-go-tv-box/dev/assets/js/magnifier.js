var zoomImage = $(".full"),
  thumbnail = $(".preview a"),
  galArray = $(".thumbnails a"),
  galOnPage = $(".gallery").length,
  galSize = galArray.length / galOnPage,
  leftControll = $(".control-left"),
  rightControll = $(".control-right"),
  currentImg = $(".current-img"),
  allImg = $(".all-img"),
  indicatorContent = $(".in__content"),
  galImgArray = [],
  zoomConfig = {
    responsive: true,
    borderSize: 1,
    borderColour: "#2F2F2F",
    tint: true,
    tintColour: "#FFF",
    tintOpacity: 0.6,
    zoomWindowWidth: 300,
    zoomWindowHeight: 350
  };

for (var i = 0; i < galArray.length; i++) {
  galImgArray.push($(galArray[i]).data("full"));
}

function getCurrentImg() {
  var currentImg = $(".full").attr("src");

  for (var i = 0; i < galSize; i++) {
    if (currentImg == galImgArray[i]) {
      return i + 1;
    }
  }
}

function adjustZoom(attr, data) {
  if (window.innerWidth <= 860) {
    $(".zoomContainer").remove();
    zoomImage.removeData("elevateZoom");
  } else {
    zoomImage.attr("src", attr);
    zoomImage.data("zoom-image", data);
    zoomImage.elevateZoom(zoomConfig);
  }
}

function calculateIndicator() {
  var tempCurrentImgPos = getCurrentImg(),
    percent;

  percent = (tempCurrentImgPos / parseFloat(galSize)) * 100;

  return percent;
}

function checkPositionBoundary() {
  var tempCurrImg = getCurrentImg();

  if (tempCurrImg == 1) {
    return -1;
  } else if (tempCurrImg == galSize) {
    return 0;
  }
  return 1;
}

function fillIndicator() {
  $(indicatorContent).css("width", calculateIndicator() + "%");
}

function getNextImg() {
  return galImgArray[getCurrentImg()];
}

function getPrevImg() {
  return galImgArray[getCurrentImg() - 2];
}

function replaceImg(src) {
  zoomImage.attr("src", src);
  zoomImage.data("zoom-image", src);
}

function adjustCurrImgCounter() {
  currentImg.text(getCurrentImg());
}

rightControll.on("click", function(e) {
  e.preventDefault();
  var tempCurrImg = checkPositionBoundary();
  if (tempCurrImg == -1 || tempCurrImg > 0) {
    replaceImg(getNextImg());
    fillIndicator();
    adjustCurrImgCounter();
  }
});

leftControll.on("click", function(e) {
  e.preventDefault();
  var tempCurrImg = checkPositionBoundary();
  if (tempCurrImg >= 0) {
    replaceImg(getPrevImg());
    fillIndicator();
    adjustCurrImgCounter();
  }
});

fillIndicator();
calculateIndicator();

adjustCurrImgCounter();
allImg.text(galSize);

adjustZoom();

thumbnail.on("click", function(e) {
  e.preventDefault();
  var attr, data;

  $(".selected").removeClass("selected");
  $(this).addClass("selected");
  var picture = $(this).data();

  attr = $(this).data("image");
  data = $(this).data("zoom-image");

  adjustZoom(attr, data);

  zoomImage
    .fadeOut(100, function() {
      zoomImage.attr("src", picture.full);
    })
    .fadeIn();
});

$(window).resize(function(e) {
  adjustZoom();
});
