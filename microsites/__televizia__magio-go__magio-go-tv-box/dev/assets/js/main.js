/* popup on page init */
var getUrlParameter = function getUrlParameter(sParam) {
  var sPageURL = window.location.search.substring(1),
    sURLVariables = sPageURL.split("&"),
    sParameterName,
    i;

  for (i = 0; i < sURLVariables.length; i++) {
    sParameterName = sURLVariables[i].split("=");

    if (sParameterName[0] === sParam) {
      return sParameterName[1] === undefined
        ? true
        : decodeURIComponent(sParameterName[1]);
    }
  }
};

var popup_id = getUrlParameter("popup");
function openPopupByURL(popupID) {
  $.fancybox.open({
    href: "#" + popupID
  });
}

if (popup_id) {
  openPopupByURL(popup_id);
}

/* fancybox */
$(".lightbox").fancybox();

/* TABS */
$(".tabs-menu a").click(function(event) {
  event.preventDefault();
  $(this)
    .parent()
    .addClass("current");
  $(this)
    .parent()
    .siblings()
    .removeClass("current");
  var tab = $(this).attr("href"), // select clicked tab. In example "#tab-1" ..
    parent = $(this)
      .closest("ul")
      .parent()
      .parent(); // <tabs-container>
  parent
    .find(".tab-content")
    .not(tab)
    .css("display", "none"); //hide all not clicked tabs
  $(tab).fadeIn(); //show clicked tab
});
/* TABS END */

/* SCROLL TO */
$("div").on("click", ".scroll-to", function(e) {
  var parentNav = $(this)
    .closest(".main-navigation")
    .attr("id");
  var offset = 0;

  if (parentNav === "nav-sticky-custom") {
    offset = 68;
  }
  if (
    location.pathname.replace(/^\//, "") == this.pathname.replace(/^\//, "") &&
    location.hostname == this.hostname
  ) {
    var target = $(this.hash);
    target = target.length ? target : $("[name=" + this.hash.slice(1) + "]");
    if (target.length) {
      if ($(window).width() > 550) {
        $("html,body").animate(
          {
            scrollTop:
              target.offset().top - $("#nav-sticky").outerHeight() - offset
          },
          1000
        );
      } else {
        $("html,body").animate(
          {
            scrollTop: target.offset().top - offset
          },
          1000
        );
      }
      return false;
    }
  }
});

/* stepper - steps fix images */

var step2_initialimg = $(".step2")
  .find(".step-ico")
  .attr("src");
var step2_doneimg = $(".step1")
  .find(".step-ico")
  .attr("src");

$(".step3").on("click", function() {
  $(".step2")
    .find(".step-ico")
    .attr("src", step2_doneimg);
});

$(".step1").on("click", function() {
  $(".step2")
    .find(".step-ico")
    .attr("src", step2_initialimg);
});

/* AOS */
AOS.init({
  anchorPlacement: "center-center"
});
