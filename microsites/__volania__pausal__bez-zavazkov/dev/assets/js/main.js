$(function () {
    $(".tabs-menu a").click(function (event) {
        event.preventDefault();
        if (!$(this).parent().hasClass("current") && !$(this).parent().hasClass("isVariant")) {
            toggleTabs();
        }

        $(this).parent().addClass("current");
        $(this).parent().siblings().removeClass("current");
        var tab = $(this).attr("href"),
            parent = $(this).closest('ul').parent().parent();
        parent.find('.tab-content').not(tab).css("display", "none");
        $(tab).fadeIn();

    });

    var toggleTabs = function () {
        $('.tab__variant').toggleClass('selected');
    };

    $(".scroll-to").click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });


    /* POPUP HANDLER */

    var $activateBtn = $('#activate'),
        $selectBox = $('.heapBox .holder'),
        $info1GB = $('#info-1GB'),
        $info1 = $('#info-1'),
        $info2 = $('#info-2'),
        $select = $('#subs-type');

    $($select).on('change', function() {
        if ($selectBox.attr('rel') == '1GB') {
            $($info1).show();
            $($info2).hide();
            $activateBtn.attr('href', '#data1GB');
        } else if ($selectBox.attr('rel') == '100') {
            $($info1).show();
            $($info2).hide();
            $activateBtn.attr('href', '#data100');
        } else {
            $($info2).show();
            $($info1).hide();
            $activateBtn.attr('href', '#data500');
        }
    });

    $("#activate").fancybox();
    $("#deactivate").fancybox();

    var generateTooltip = function (element, text) {
        $(element).qtip({
            content: {
                text: text
            },
            position: {
                my: 'bottom center',
                at: 'top center'
            }
        });
    }

    generateTooltip('.info-clip', 'So službou CLIP uvidíte na displeji svojho mobilu telefónne číslo volajúceho, pokiaľ nemá aktivovanú službu CLIR (zamedzenie zobrazenia telefónneho čísla).<br/>Ak máte uložené telefónne číslo volajúceho v pamäti, uvidíte jeho meno.');

    generateTooltip('.ec-3', 'Túto akciu môžu využiť len majitelia paušálov Happy M, L, XL volania, L, XL, XXL a Profi.');

    generateTooltip('.ec-4', 'Jednorazový poplatok sa uhrádza pri aktivácii programu Bez záväzkov.');

    generateTooltip('.ec-5', 'S balíčkom 100 minút získate 100 minút na volania do všetkých sietí v SR. Minúty môžete minúť aj na volania z Čiech, Maďarska, Rakúska alebo Poľska na Slovensko a do EÚ.');

    generateTooltip('.ec-cena', 'Výhodná cena 0,035 € za minútu hovoru platí prvých 100 minút.<br>Po prevolaní 100 minút je cena za minútu hovoru 0,12 €.');

    generateTooltip('.info-pausaly', 'Vybrané paušály, pre ktoré platí akcia: Happy M, L, XL volania, L, XL, XXL a Profi.');


    /* TOGGLE ARROW */
    $('.toggle-arrow').click(function(event) {
        event.preventDefault();
      $(event.target).next('div.arrow-content').toggle(200);
      $(this).find('.arrow-right').toggleClass('arrow-rotate');
    });
    /* TOGGLE ARROW END */


});
