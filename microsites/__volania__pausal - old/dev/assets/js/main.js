$(document).ready(function() {
    
	function getParameterByName(name) {
		var found = false;
		if (window.location.href.indexOf(name) > -1) {
			found = true;
		}

		return found;
	}

	if (getParameterByName('biznis')) {
		$('.plan-sale').hide();
		$('.left-desc2').hide();
		$('#ec-1').hide();
		$('#ec-2').hide();
		$('#prod-3').hide();
		$('#nav1-bez-zavazkov').hide();
		$('#nav2-bez-zavazkov').hide();
		$('#sec-6').hide();
    }

	/* ::: Scroll To ::: */
    $("div").on('click', '.scroll-to', function(e) {
        var parentNav = $(this).closest('.main-navigation').attr('id');
        var offset = 0;

        if (parentNav === 'nav-sticky-custom') {
            offset = 68;
        }
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                    if ($(window).width() > 550) {
                        $('html,body').animate({
                            scrollTop: (target.offset().top - ($("#nav-sticky").outerHeight()) - offset)
                        }, 1000);
                    } else {
                        $('html,body').animate({
                            scrollTop: (target.offset().top - offset)
                        }, 1000);
                    }
                return false;
            }
        }
    });

    /* ::: Sticky Nav-ext ::: */
    $("#nav-sticky").sticky({
        topSpacing: 0,
        widthFromWrapper: true
    });

    var contentSections = $("[id*='sec-']"),
        secondaryNav = $("#nav-sticky");

    function updateSecondaryNavigation() {
        contentSections.each(function() {
            var actual = $(this),
                actualHeight = actual.height() + parseInt(actual.css('paddingTop').replace('px', '')) + parseInt(actual.css('paddingBottom').replace('px', '')),
                actualAnchor = secondaryNav.find('a[href="#' + actual.attr('id') + '"]');

            if ((actual.offset().top - secondaryNav.outerHeight() <= $(window).scrollTop()) && (actual.offset().top + actualHeight - secondaryNav.outerHeight() > $(window).scrollTop())) {
                actualAnchor.addClass('active');
            } else {
                actualAnchor.removeClass('active');
            }

        });
    }
    updateSecondaryNavigation();
    $(window).scroll(function(event) {
        updateSecondaryNavigation();
    });
});

$(function() {
    /* ::: Slider Oblubene ::: */
    // $("#program_slider").slick({
    //     slidesToShow: 2,
    //     slidesToScroll: 1,
    //     centerMode: false,
    //     infinite: false,
    //     swipeToSlide: true,
    //     swipe: true,
    //     arrows: true,
    //     dots: false,
    //     autoplay: false,
    //     initialSlide: 2,
    //     autoplaySpeed: 5000,
    //     touchMove: false,
    //     draggable: false,
    //     customPaging: function(slider, i) {
    //         return '<span class="slide-dot">&nbsp;</span>';
    //     },
    //     responsive: [{
    //         breakpoint: 1024,
    //         settings: {
    //             slidesToShow: 2,
    //             slidesToScroll: 1,
    //             arrows: true,
    //             dots: true
    //         }
    //     }, {
    //         breakpoint: 768,
    //         settings: {
    //             slidesToShow: 1,
    //             slidesToScroll: 1,
    //             arrows: true,
    //             infinite: false,
    //             dots: true
    //         }
    //     }]
    // });


    /* ::: PopUp Conf ::: */
    $('.p-close, .closePopupWindow').on('click', function(e) {
        e.preventDefault();
        $.fancybox.close();
    });
    // popup close with scroll to section
    $(document).on('click', '.popup-modal-dismiss', function(e) {
        e.preventDefault();
        $.fancybox.close();
    });

    $(document).on('click', '.popup-modal-scroll', function(e) {
        e.preventDefault();
        $.fancybox.close();
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top - ($("#nav-sticky").outerHeight())
                }, 1000);
                return false;
            }
        }
    });

    /* ::: PopUp Data ::: */
    $('.popup').fancybox({
        padding: 11,
        margin: 0,
        closeBtn: false,
        parent: "#content",
        maxWidth: 70 + "%",
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.7)'
                }
            }
        }
    });

    $("a#activate-order100").fancybox({
        padding: 11,
        margin: 0,
        closeBtn: false,
        parent: "#content",
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.7)'
                }
            }
        }
    });

    $("#activate-order500").fancybox({
        padding: 11,
        margin: 0,
        closeBtn: false,
        parent: "#content",
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.7)'
                }
            }
        }
    });

    $("#activate-order1000").fancybox({
        padding: 11,
        margin: 0,
        closeBtn: false,
        parent: "#content",
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.7)'
                }
            }
        }
    });

    $("a#info-order1000a").fancybox({
        padding: 11,
        margin: 0,
        closeBtn: false,
        parent: "#content",
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.7)'
                }
            }
        }
    });

    $("#info-order100").fancybox({
        padding: 11,
        margin: 0,
        closeBtn: false,
        parent: "#content",
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.7)'
                }
            }
        }
    });

    $("#info-order500").fancybox({
        padding: 11,
        margin: 0,
        closeBtn: false,
        parent: "#content",
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.7)'
                }
            }
        }
    });

    $("#info-order1000").fancybox({
        padding: 11,
        margin: 0,
        closeBtn: false,
        parent: "#content",
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.7)'
                }
            }
        }
    });

    $("#activate-data100").fancybox({
        padding: 11,
        margin: 0,
        closeBtn: false,
        parent: "#content",
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.7)'
                }
            }
        }
    });

    $("#activate-data500").fancybox({
        padding: 11,
        margin: 0,
        closeBtn: false,
        parent: "#content",
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.7)'
                }
            }
        }
    });

    $(".dataNavyse500MB, .dataNavyse1GB, .volaniaNavyse").fancybox({
        padding: 11,
        margin: 0,
        closeBtn: false,
        parent: "#content",
        autoSize: false,
        width: 320,
        height: 480,
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.7)'
                }
            }
        }
    });

    $(".dataNavyse2GB, .dataNavyse4GB").fancybox({
        padding: 11,
        margin: 0,
        closeBtn: false,
        parent: "#content",
        autoSize: false,
        width: 360,
        height: 480,
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.7)'
                }
            }
        }
    });

    $(".volaniaNavyse100").fancybox({
        padding: 11,
        margin: 0,
        closeBtn: false,
        parent: "#content",
        autoSize: false,
        width: 280,
        height: 470,
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.7)'
                }
            }
        }
    });

    $(".volaniaNajblizsim").fancybox({
        padding: 11,
        margin: 0,
        closeBtn: false,
        parent: "#content",
        autoSize: false,
        width: 280,
        height: 395,
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.7)'
                }
            }
        }
    });

    $(".smsAmms").fancybox({
        padding: 11,
        margin: 0,
        closeBtn: false,
        parent: "#content",
        autoSize: false,
        width: 280,
        height: 510,
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.7)'
                }
            }
        }
    });

    $("#activate-lightbox-zd").fancybox({
        padding: 11,
        margin: 0,
        closeBtn: false,
        parent: "#content",
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.7)'
                }
            }
        }
    });


    // $("#activate-lightbox-young").fancybox({
    //     padding: 11,
    //     margin: 0,
    //     closeBtn: false,
    //     parent: "#content",
    //     helpers: {
    //         overlay: {
    //             css: {
    //                 'background': 'rgba(0, 0, 0, 0.7)'
    //             }
    //         }
    //     }
    // });



    /* ::: ToolTips ::: */
    var generateTooltip = function(element, text) {
        $(element).qtip({
            content: {
                text: text
            },
            position: {
                my: 'bottom center',
                at: 'top center'
            }
        });
    }

    $('.pausaly-info').qtip({
        content: {
            text: 'Vybrané paušály, pre ktoré platí akcia: Happy M, L, XL volania, L, XL, XXL a Profi.'
        },
        position: {
            my: 'left center',
            at: 'right center'
        }
    });

    $('.clip-info').qtip({
        content: 'So službou CLIP uvidíte na displeji svojho mobilu telefónne číslo volajúceho, pokiaľ nemá aktivovanú službu CLIR (zamedzenie zobrazenia telefónneho čísla). Ak máte uložené telefónne číslo volajúceho v pamäti, uvidíte jeho meno.',
        position: {
            corner: {
                target: 'leftMiddle',
                tooltip: 'topMiddle'
            },
            position: {
                my: 'bottom center',
                at: 'top center'
            }
        }
    }).bind('click', function(event) {
        event.preventDefault();
        return false;
    });

    $('.ec-beta').qtip({
        content: {
            text: 'Služba immmr je zatiaľ v pilotnej prevádzke. Pomôžte nám ju zlepšiť. Svoje podnety posielajte na <a href="mailto:data@telekom.sk">data@telekom.sk</a>'
        },
        hide: {
            event: 'unfocus'
        },
        position: {
            my: 'left center',
            at: 'right center'
        }
    });

    $('.roaming-info').qtip({
        content: 'Všetky členské štáty EÚ a Island, Nórsko, Lichtenštajnsko',
        position: {
            corner: {
                target: 'leftMiddle',
                tooltip: 'topMiddle'
            }
        }
    }).bind('click', function(event) {
        event.preventDefault();
        return false;
    });

    $('.tooltip').qtip({
        content: {
            text: 'Vybrané paušály sú:<br> Happy M, L, XL volania, XL, XXL a Profi'
        },
        position: {
            my: 'bottom center',
            at: 'top center'
        }
    });

    $('.akcie-info').qtip({
        content: '<div class="tag-left">Novým zákazníkom objem dát navýšime pri aktivácii paušálu a verným od najbližšieho zúčtovacieho obdobia po 15. 3. 2017, najneskôr však do 9.4.2017.</div>',
        position: {
            corner: {
                target: 'leftMiddle',
                tooltip: 'topMiddle'
            }
        }
    }).bind('click', function(event) {
        event.preventDefault();
        return false;
    });

    $('.happy_xs-info').qtip({
        content: 'Počas pracovných dní v čase od 19.00 hod. do 7.00 hod. Soboty, nedele a štátne sviatky počas celých 24 hodín.',
        position: {
            corner: {
                target: 'leftMiddle',
                tooltip: 'topMiddle'
            }
        }
    }).bind('click', function(event) {
        event.preventDefault();
        return false;
    });

    $('.happy_young-info').qtip({
        content: 'Web zľavu až 20 € na mobil získate iba pri kúpe paušálu na našom webe. Konkrétna výška zľavy bude vypočítaná pri nákupe.',
        position: {
            corner: {
                target: 'leftMiddle',
                tooltip: 'topMiddle'
            }
        }
    }).bind('click', function(event) {
        event.preventDefault();
        return false;
    });

    $('.tv-philips').qtip({
        content: {
            text: 'Pri kúpe cez web získate zľavu na televízor 20 €, ak si ho vezmete s paušálom ÁNO M, M Dáta, L, XL alebo XXL. S paušálom ÁNO S získate zľavu 10 €. Zľava bude odrátaná z jednorazového úvodného poplatku za televízor.'
        },
        position: {
            my: 'bottom center',
            at: 'top center'
        }
    });

    /* ::: Order Form for Mobile*/
    var orderUrl = '/mam-zaujem/volania/happy/formular?pausal=';

    $('#order-happy-xs-mini').click(function(event) {
        /*event.preventDefault();*/
        if (window.matchMedia('(max-width: 600px)').matches) {
            $(this).attr("href", orderUrl + 'Happy XS mini');
        } else {}
    });

    $('#order-happy-xs').click(function(event) {
        /*event.preventDefault();*/
        if (window.matchMedia('(max-width: 600px)').matches) {
            $(this).attr("href", orderUrl + 'Happy XS');
        } else {}
    });

    $('#order-happy-s').click(function(event) {
        /*event.preventDefault();*/
        if (window.matchMedia('(max-width: 600px)').matches) {
            $(this).attr("href", orderUrl + 'Happy S');
        } else {}
    });

    $('#order-happy-m').click(function(event) {
        /*event.preventDefault();*/
        if (window.matchMedia('(max-width: 600px)').matches) {
            $(this).attr("href", orderUrl + 'Happy M');
        } else {}
    });

    $('#order-happy-l').click(function(event) {
        /*event.preventDefault();*/
        if (window.matchMedia('(max-width: 600px)').matches) {
            $(this).attr("href", orderUrl + 'Happy L');
        } else {}

    });

    $('#order-happy-xl-call').click(function(event) {
        /*event.preventDefault();*/
        if (window.matchMedia('(max-width: 600px)').matches) {
            $(this).attr("href", orderUrl + 'Happy XL Volania');
        } else {}

    });

    $('#order-happy-xl').click(function(event) {
        /*event.preventDefault();*/
        if (window.matchMedia('(max-width: 600px)').matches) {
            $(this).attr("href", orderUrl + 'Happy XL');
        } else {}

    });

    $('#order-happy-xxl').click(function(event) {
        /*event.preventDefault();*/
        if (window.matchMedia('(max-width: 600px)').matches) {
            $(this).attr("href", orderUrl + 'Happy XXL');
        } else {}

    });

    $('#order-happy-profi').click(function(event) {
        /*event.preventDefault();*/
        if (window.matchMedia('(max-width: 600px)').matches) {
            $(this).attr("href", orderUrl + 'Happy Profi');
        } else {}

    });


    /* TABS */
    $(".tabs-menu a").click(function(event) {
        event.preventDefault();
        $(this).parent().addClass("current");
        $(this).parent().siblings().removeClass("current");
        var tab = $(this).attr("href"), // select clicked tab. In example "#tab-1" ..
            parent = $(this).closest('ul').parent().parent(); // <tabs-container>
        parent.find('.tab-content').not(tab).css("display", "none"); //hide all not clicked tabs
        $(tab).fadeIn(); //show clicked tab


    });
    /* TABS END */

    /* FROM TABS TO ACCORDION */
    var dataSegment = $("[data-segment]").each(function() {
      //console.log($(this).attr('data-segment')); //zobraz "data-segment" hodnotu pre kazdu <section>

      closestHead = $($(this).find('.tabs-menu a'));
      closestItemCount = closestHead.length - 1; //vytiahni pocet tabov pre kazdu <section> s atributom "data-segment"
      //console.log(closestItemCount + "heads");

      closestContent = $($(this).find('.tab-content')); //najdi tab-content v danej <section>
      //console.log(closestContent);

      closestAccordion = $(this).find('.accordion'); //najdi .accordion v danej <section>

      accordionItem = '<div class="item">';

      for (var i = 0; i <= closestItemCount; i++) {
        accordionItem += '<div class="heading">' + $(closestHead[i]).text() + '</div>';
        accordionItem += '<div class="content">' + $(closestContent[i]).html() + '</div></div>';

        if (i !== closestItemCount) {
          accordionItem += '<div class="item">';
        }
      }

      //if data-segment and data-accordion value match, show accordion data
      if ($(this).attr('data-segment') === closestAccordion.attr('data-accordion')) {
        $(accordionItem).appendTo(closestAccordion);
      }

      var $items = $('.accordion .item');
      //SET OPENED ITEM
      $($items[0]).addClass('open');
      //$($items[1]).addClass('open');

    });
    /* FROM TABS TO ACCORDION END */

    /* ACCORDION */
    $('.accordion .item .heading').click(function(e) {

      var clickedHead = e.target;
      var $item = $(clickedHead).closest('item');
      var isOpen = $item.hasClass('open');
      var $content = $item.find('.content');
      var $acPrice = $(clickedHead).find('.ac-price');

      if (isOpen) {
        $content.slideUp(200);
        $item.removeClass('open');
        $acPrice.show();
      } else {
        //$content.slideDown(200);
        $item.addClass('open');
        $acPrice.hide();
      }
    });


    $('.accordion .item.open').find('.content').slideDown(200);
    /* ACCORDION END */

    ////////////////////////////////////
    $('.as1').slick({
        infinite: false,
        slidesToShow: 2,
        slidesToScroll: 1,
        infinite: false,
        dots: true,
        responsive: [{
            breakpoint: 700,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: true
            }
        }]
    });

    $('.as2').slick({
        infinite: false,
        slidesToShow: 2,
        slidesToScroll: 1,
        infinite: false,
        dots: true,
        responsive: [{
            breakpoint: 700,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: true
            }
        }]
    });

    /* ACCORDION */
    $('.accordion .item .heading').click(function(e) {
        var clickedHead = e.target;
        var $item = $(clickedHead).closest('.item');
        var isOpen = $item.hasClass('open');
        var $content = $item.find('.content');
        var $acPrice = $(clickedHead).find('.ac-price');

        if (isOpen) {
            $content.slideUp(200);
            $item.removeClass('open');
            $acPrice.show();
        } else {
            $content.slideDown(200);
            $item.addClass('open');
            $acPrice.hide();
        }
    });

    $('.accordion .item.open').find('.content').slideDown(200);

    $(".show-hide").click(function () {
        var a = $(this).attr("data-container");
        $(this).toggleClass("open-down");
        $("#" + a).slideToggle(500)
    });
});

jQuery(document).ready(function() {
    jQuery('ul.taby2 li a').on('click', function(e) {
        $('.as2').css("visibility", "hidden");
        var currentAttrValue = jQuery(this).attr('href');

        jQuery('.tab ' + currentAttrValue).show().siblings().hide();

        jQuery(this).parent('li').addClass('active').siblings().removeClass('active');
        $('.as2').slick('setPosition');
        setTimeout(function() {
            $('.as2').css("visibility", "visible");
        }, 200);
        e.preventDefault();
    });
});


/* auto scroll from hash */
jQuery(document).ready(function() {

    if (window.location.hash) {
        var urlHash = window.location.hash;
        var cleanedHash = urlHash.substring(0 , urlHash.indexOf('?'));
        var $items = $('.accordion .item');

        function scrollBasedOnURL(scrollto, link) {
            //if ($(window).width() > 768) {
                $('html, body').animate({
                    scrollTop: ($(scrollto).offset().top - 60) + 'px'
                }, 1000);
                if (link != undefined || link != null) {
                    $(link).eq(0).prev().trigger('click');
                }
            //}
        }

        function scrollHandlerBasedOnURLParams(scrollto, link) {
            if ($(window).width() > 768) {
                scrollBasedOnURL(scrollto, link);
            }
        }

        function showPausalOnMobile (id) {
            $($('.plm__plan')).each(function() {
                if ($(this).data('objid') == id && $(window).width() <= 768 ) {
                    $('*[data-objid="'+id+'"]').find('.plan-show-detail_tab').trigger('click');
                }
              });
        }
        
        function scrollToTabAndOpen(tab) {
            $('html, body').animate({
                    scrollTop: ($("#sec-7").offset().top - 85) + 'px'
            }, 1000);
            $(tab).trigger('click');
            //console.log('\"'+ tab +'\"');
        }

        function scrollToAccordionAndOpen() {
            $('html, body').animate({
                scrollTop: ($("#sec-7").offset().top) + 'px'
        }, 1000);
        }

        setTimeout(function() {
            switch (cleanedHash) {
                case '#tab-2':
                    $('html, body').animate({
                        scrollTop: ($("#sec-6").offset().top - 55) + 'px'
                    }, 1000);
                    break;
                case '#tab-3':
                case '#tab-4':
                case '#tab-5':
                case '#tab-11':
                    $("#sec-7 .tabs-container .tabs-menu .variant").removeClass("current");
                    $("#sec-7 .tabs-container .tabs-menu " + cleanedHash + '-li').addClass("current");

                    $("#sec-7 .tab .tab-content").hide(); 
                    $("#sec-7 .tab " + cleanedHash).show();

                    $('html, body').animate({
                        scrollTop: ($("#sec-7").offset().top - 75) + 'px'
                    }, 1000);

	                break;                    
                case '#ano-s':
                    scrollHandlerBasedOnURLParams('#prod-0', 'a[href="/mob/objednavka/-/scenario/e-shop/pausal?vyberrp=s"]');
                    showPausalOnMobile(0);
                    break;
                case '#ano-m':
                    scrollHandlerBasedOnURLParams('#prod-1','a[href="/mob/objednavka/-/scenario/e-shop/pausal?vyberrp=m"]');
                    showPausalOnMobile(1);
                    break;
                case '#ano-mdata':
                    scrollHandlerBasedOnURLParams('#prod-2','a[href="/mob/objednavka/-/scenario/e-shop/pausal?vyberrp=m_data"]');
                    showPausalOnMobile(2);
                    break;
                case '#ano-l':
                    scrollHandlerBasedOnURLParams('#prod-3', 'a[href="/mob/objednavka/-/scenario/e-shop/pausal?vyberrp=l"]');
                    showPausalOnMobile(3);
                    break;
                case '#ano-xl':
                    scrollHandlerBasedOnURLParams('#prod-4', 'a[href="/mob/objednavka/-/scenario/e-shop/pausal?vyberrp=xl"]');
                    showPausalOnMobile(4);
                    break;
                case '#ano-xxl':
                    scrollHandlerBasedOnURLParams('#prod-5', 'a[href="/mob/objednavka/-/scenario/e-shop/pausal?vyberrp=xxl"]');
                    showPausalOnMobile(5);
                    break;
                case '#vyberrp=xl_call':
                    scrollHandlerBasedOnURLParams('#prod-2', 'a[href="/mob/objednavka/-/scenario/e-shop/pausal?vyberrp=xl_call"]');
                    break;
                case '#vyberrp=xl':
                    scrollHandlerBasedOnURLParams('#prod-2', 'a[href="/mob/objednavka/-/scenario/e-shop/pausal?vyberrp=xl"]');
                    break;
                case '#vyberrp=xxl':
                    scrollHandlerBasedOnURLParams('#prod-2', 'a[href="/mob/objednavka/-/scenario/e-shop/pausal?vyberrp=xxl"]');
                    break;
                case '#vyberrp=profi':
                    scrollHandlerBasedOnURLParams('#prod-2', 'a[href="/mob/objednavka/-/scenario/e-shop/pausal?vyberrp=profi"]');
                    break;
                case '#happy2ano':
                    if ($(window).width() <= 550) {
                            scrollToAccordionAndOpen()
                            $($items[1]).removeClass('open');
                        }
                        else {
                            scrollToTabAndOpen('#happy2ano a')
                        }

                    break;
                case '#1gbzadarmo':
                    if ($(window).width() <= 550) {
                            scrollToAccordionAndOpen()
                            $($items[0]).removeClass('open');
                            $($items[1]).addClass('open');
                        }
                        else {
                            scrollToTabAndOpen('#1gbzadarmo a')
                        }

                    break;
                case '#prenosDat':
                    if ($(window).width() <= 550) {
                            scrollToAccordionAndOpen()
                            $($items[0]).removeClass('open');
                            $($items[1]).removeClass('open');
                            $($items[2]).addClass('open');
                        }
                        else {
                            scrollToTabAndOpen('#prenosDat a')
                        }

                    break;
                case '#viacDat':
                    if ($(window).width() <= 550) {
                            scrollToAccordionAndOpen()
                            $($items[0]).removeClass('open');
                            $($items[1]).removeClass('open');
                            $($items[2]).removeClass('open');
                            $($items[3]).addClass('open');
                        }
                        else {
                            scrollToTabAndOpen('#viacDat a')
                        }
                    break;                    
                case '#letnyBalik':
                    if ($(window).width() <= 550) {
                            scrollToAccordionAndOpen()
                            $($items[0]).addClass('open');
                            $($items[1]).removeClass('open');
                            $($items[2]).removeClass('open');
                            $($items[3]).removeClass('open');
                            $($items[4]).removeClass('open');
                        }
                        else {
                            scrollToTabAndOpen('#letnyBalik a');
                        }
                    break;
                default:
                    $('html, body').animate({
                        scrollTop: (($(window.location.hash).offset.top) - 0) + 'px'
                    }, 1000);
            };
        }, 1000);
    }

    // Scroll to tab from HERO and open tab
    $('.open-tab').on('click', function(e) {
        e.preventDefault();
        var $items = $('.accordion .item');
        var href = $(this).attr('href');
        $('html, body').animate({
                scrollTop: ($("#sec-7").offset().top - 85) + 'px'
        }, 1000);
        $(href + " a").trigger('click');
        console.log("Opened")
        $($items[0]).removeClass('open');
        $($items[1]).removeClass('open');
        $($items[2]).addClass('open');
        $($items[3]).removeClass('open');
        $($items[4]).removeClass('open');
    });
});

$(document).ready(function() {
    function checkMobileOS() {
      var MobileUserAgent = navigator.userAgent || navigator.vendor || window.opera;
      if (MobileUserAgent.match(/iPad/i) || MobileUserAgent.match(/iPhone/i) || MobileUserAgent.match(/iPod/i)) {
        return "iOS";
      } else if (MobileUserAgent.match(/Android/i)) {
        return "Android";
      } else {
        return "unknown";
      }
    }
    $(".sms_link").each(function(index) {
        $(this).on("click", function() {
            // event.preventDefault();
            var href = '';
            var message_text = this.dataset.smsText;
            var message_number = this.dataset.smsNumber;
            if (checkMobileOS() == 'iOS') {href = "sms:" + message_number + "&body=" + encodeURI(message_text); }
            if (checkMobileOS() == 'Android') { href = "sms:" + message_number + "?body=" + encodeURI(message_text); }
            $(this).attr('href', href);
        });
    });
});

// generate heapBoxex -> generate both versions, for mob and desktop
$(document).ready(function () {
      var jsonValues = '[{"value":"ano","text":"s paušálmi ÁNO"},{"value":"happy","text":"s paušálmi Happy"}, {"value":"bezZavazkov","text":"k programu Bez záväzkov"}]';
      var mobileHeapbox =  $(".accordion .plandatainfo-selectbox-mobile");
      var desktopHeapbox = $(".plandatainfo-selectbox");

      function generateHeapbox(heapboxWrapperClass) {
            heapboxWrapperClass.heapbox({
                "onChange":function(val, elm) {   
                    var items = $('.plandatainfo-details');
                    if (val) {
                        items.hide();
                        $('.plandatainfo-details.'+val).show();
                    }
                }
            });
            heapboxWrapperClass.heapbox("set", jsonValues);
        }
      
      if ($(document).width() <= 543) {
            generateHeapbox(mobileHeapbox);
        } else {
            generateHeapbox(desktopHeapbox);
        }
});


/* TOGGLE ARROW */
$(document).on('click', '.togleArrow .toggle-arrow, .togleArrow .arrow-right', function(event) {
  event.preventDefault();
  if ($(this).hasClass('arrow-right')) {
    return;
  }
  $(event.target).closest('.togleArrow').find('.arrow-content').toggle(200);
  $(this).find('.arrow-right').toggleClass('arrow-rotate');
});
/* TOGGLE ARROW END */
