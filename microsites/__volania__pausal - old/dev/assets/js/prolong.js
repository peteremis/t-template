$(window).on('load', function() {

    var $cmbBLock = $('#cmb__block'),
        $input = $('#msisdn'),
        $btn = $('#msisdn-btn'),
        base = 'https://www.telekom.sk/mob/objednavka/-/scenario/e-shop/happy',
        happyURL = '',
        defaultInputVal = '09',
        $prolongCall = $('.prolong-cta');

    if ($($cmbBLock).is(':visible')) {
        $($prolongCall).css('display', 'inline-block');
    }

    $btn.on('click', function(e){
        e.preventDefault();
        var msisdn = $input.val();
        // happyURL = base + '?msisdn=' + msisdn + '&prolong-checker=true';
        happyURL = base + '?prolong-checker=true';


        // create session cookie with name 'telefon'
        // store value from input.val() into cookie 'telefon'
        document.cookie = 'telefon='+msisdn+'; expires=0; path=/';

        /* default getCookie() by name */
        function getCookie(cname) {
             var name = cname + "=";
             var ca = document.cookie.split(';');
             for(var i=0; i<ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0)==' ') c = c.substring(1);
                if(c.indexOf(name) == 0) //if the cookie is found
                   return c.substring(name.length,c.length);
             }
             return ""; //if the cookie is not found
        }
        /* default getCookie by name END */

      // get cookie with name telefon and print value to ('#sms-verification-input-number').
      var cookie = getCookie("telefon");
      if (cookie != "") {
          $('#sms-verification-input-number').val(cookie);
        };

        if (msisdn != defaultInputVal) {
            window.location.href = happyURL;
        }
    });
});
