$(document).ready(function() {

	function getParameterByName(name) {
		var found = false;
		if (window.location.href.indexOf(name) > -1) {
			found = true;
		}

		return found;
	}

	if (getParameterByName('biznis')) {
		$('.plan-sale').hide();
		$('.left-desc2').hide();
		$('#ec-1').hide();
		$('#ec-2').hide();
		$('#prod-3').hide();
		$('#nav1-bez-zavazkov').hide();
		$('#nav2-bez-zavazkov').hide();
		$('#sec-6').hide();
	}

	/* ::: Scroll To ::: */
    $(".scroll-to").click(function() {
        var parentNav = $(this).closest('.main-navigation').attr('id');
        var offset = 0;

        if (parentNav === 'nav-sticky-custom') {
            offset = 68;
        }

        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                    if ($(window).width() > 550) {
                        $('html,body').animate({
                            scrollTop: (target.offset().top - ($("#nav-sticky").outerHeight()) - offset)
                        }, 1000);
                    } else {
                        $('html,body').animate({
                            scrollTop: (target.offset().top - offset)
                        }, 1000);
                    }
                return false;
            }
        }
    });
    /* ::: Sticky Nav-ext ::: */
    $("#nav-sticky").sticky({
        topSpacing: 0,
        widthFromWrapper: true
    });

    var contentSections = $("[id*='sec-']"),
        secondaryNav = $("#nav-sticky");

    function updateSecondaryNavigation() {
        contentSections.each(function() {
            var actual = $(this),
                actualHeight = actual.height() + parseInt(actual.css('paddingTop').replace('px', '')) + parseInt(actual.css('paddingBottom').replace('px', '')),
                actualAnchor = secondaryNav.find('a[href="#' + actual.attr('id') + '"]');

            if ((actual.offset().top - secondaryNav.outerHeight() <= $(window).scrollTop()) && (actual.offset().top + actualHeight - secondaryNav.outerHeight() > $(window).scrollTop())) {
                actualAnchor.addClass('active');
            } else {
                actualAnchor.removeClass('active');
            }

        });
    }

    updateSecondaryNavigation();
    $(window).scroll(function(event) {
        updateSecondaryNavigation();
    });

});

$(function() {
    /* ::: Slider Oblubene ::: */
    $("#program_slider").slick({
        slidesToShow: 2,
        slidesToScroll: 1,
        centerMode: false,
        infinite: false,
        swipeToSlide: true,
        swipe: true,
        arrows: true,
        dots: false,
        autoplay: false,
        initialSlide: 2,
        autoplaySpeed: 5000,
        touchMove: false,
        draggable: false,
        customPaging: function(slider, i) {
            return '<span class="slide-dot">&nbsp;</span>';
        },
        responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
                arrows: true,
                dots: true
            }
        }, {
            breakpoint: 768,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: true,
                infinite: false,
                dots: true
            }
        }]
    });
    /* ::: Slider Zakladne ::: */
    $("#zakladne_slider").slick({
        slidesToShow: 2,
        slidesToScroll: 1,
        centerMode: false,
        infinite: false,
        swipeToSlide: true,
        swipe: true,
        arrows: true,
        dots: false,
        autoplay: false,
        // initialSlide: 1,
        autoplaySpeed: 5000,
        touchMove: false,
        draggable: false,
        customPaging: function(slider, i) {
            return '<span class="slide-dot">&nbsp;</span>';
        },
        responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
                arrows: true,
                dots: true
            }
        }, {
            breakpoint: 768,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: true,
                infinite: false,
                dots: true
            }
        }]
    });
    /* ::: Slider Prenarocnych ::: */
    $("#prenarocnych_slider").slick({
        slidesToShow: 2,
        slidesToScroll: 1,
        centerMode: false,
        infinite: false,
        swipeToSlide: true,
        swipe: true,
        arrows: true,
        dots: true,
        autoplay: false,
        draggable: false,
        autoplaySpeed: 5000,
        customPaging: function(slider, i) {
            return '<span class="slide-dot">&nbsp;</span>';
        },
        responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
                arrows: true,
                dots: true
            }
        }, {
            breakpoint: 768,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: true,
                infinite: false,
                dots: true
            }
        }]
    });

    /* ::: PopUp Conf ::: */
    $('.p-close, .closePopupWindow').on('click', function(e) {
        e.preventDefault();
        $.fancybox.close();
    });
    // popup close with scroll to section
    $(document).on('click', '.popup-modal-dismiss', function(e) {
        e.preventDefault();
        $.fancybox.close();
    });

    $(document).on('click', '.popup-modal-scroll', function(e) {
        e.preventDefault();
        $.fancybox.close();
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top - ($("#nav-sticky").outerHeight())
                }, 1000);
                return false;
            }
        }
    });

    /* ::: PopUp Data ::: */
    $('.popup').fancybox({
        padding: 11,
        margin: 0,
        closeBtn: false,
        parent: "#content",
        maxWidth: 70 + "%",
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.7)'
                }
            }
        }
    });

    $("a#activate-order100").fancybox({
        padding: 11,
        margin: 0,
        closeBtn: false,
        parent: "#content",
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.7)'
                }
            }
        }
    });

    $("#activate-order500").fancybox({
        padding: 11,
        margin: 0,
        closeBtn: false,
        parent: "#content",
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.7)'
                }
            }
        }
    });

    $("#activate-order1000").fancybox({
        padding: 11,
        margin: 0,
        closeBtn: false,
        parent: "#content",
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.7)'
                }
            }
        }
    });

    $("a#info-order1000a").fancybox({
        padding: 11,
        margin: 0,
        closeBtn: false,
        parent: "#content",
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.7)'
                }
            }
        }
    });

    $("#info-order100").fancybox({
        padding: 11,
        margin: 0,
        closeBtn: false,
        parent: "#content",
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.7)'
                }
            }
        }
    });

    $("#info-order500").fancybox({
        padding: 11,
        margin: 0,
        closeBtn: false,
        parent: "#content",
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.7)'
                }
            }
        }
    });

    $("#info-order1000").fancybox({
        padding: 11,
        margin: 0,
        closeBtn: false,
        parent: "#content",
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.7)'
                }
            }
        }
    });

    $("#activate-data100").fancybox({
        padding: 11,
        margin: 0,
        closeBtn: false,
        parent: "#content",
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.7)'
                }
            }
        }
    });

    $("#activate-data500").fancybox({
        padding: 11,
        margin: 0,
        closeBtn: false,
        parent: "#content",
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.7)'
                }
            }
        }
    });

    $(".dataNavyse, .volaniaNavyse").fancybox({
        padding: 11,
        margin: 0,
        closeBtn: false,
        parent: "#content",
        autoSize: false,
        width: 280,
        height: 450,
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.7)'
                }
            }
        }
    });

    $(".dataNavyse2GB").fancybox({
        padding: 11,
        margin: 0,
        closeBtn: false,
        parent: "#content",
        autoSize: false,
        width: 280,
        height: 500,
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.7)'
                }
            }
        }
    });

    $(".volaniaNavyse100").fancybox({
        padding: 11,
        margin: 0,
        closeBtn: false,
        parent: "#content",
        autoSize: false,
        width: 280,
        height: 470,
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.7)'
                }
            }
        }
    });

    $(".volaniaNajblizsim").fancybox({
        padding: 11,
        margin: 0,
        closeBtn: false,
        parent: "#content",
        autoSize: false,
        width: 280,
        height: 395,
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.7)'
                }
            }
        }
    });

    $(".smsAmms").fancybox({
        padding: 11,
        margin: 0,
        closeBtn: false,
        parent: "#content",
        autoSize: false,
        width: 280,
        height: 510,
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.7)'
                }
            }
        }
    });

    $("#activate-lightbox-zd").fancybox({
        padding: 11,
        margin: 0,
        closeBtn: false,
        parent: "#content",
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(0, 0, 0, 0.7)'
                }
            }
        }
    });


    // $("#activate-lightbox-young").fancybox({
    //     padding: 11,
    //     margin: 0,
    //     closeBtn: false,
    //     parent: "#content",
    //     helpers: {
    //         overlay: {
    //             css: {
    //                 'background': 'rgba(0, 0, 0, 0.7)'
    //             }
    //         }
    //     }
    // });



    /* ::: ToolTips ::: */
    var generateTooltip = function(element, text) {
        $(element).qtip({
            content: {
                text: text
            },
            position: {
                my: 'bottom center',
                at: 'top center'
            }
        });
    }

    $('.pausaly-info').qtip({
        content: {
            text: 'Vybrané paušály, pre ktoré platí akcia: Happy M, L, XL volania, L, XL, XXL a Profi.'
        },
        position: {
            my: 'left center',
            at: 'right center'
        }
    });

    $('.clip-info').qtip({
        content: 'So službou CLIP uvidíte na displeji svojho mobilu telefónne číslo volajúceho, pokiaľ nemá aktivovanú službu CLIR (zamedzenie zobrazenia telefónneho čísla). Ak máte uložené telefónne číslo volajúceho v pamäti, uvidíte jeho meno.',
        position: {
            corner: {
                target: 'leftMiddle',
                tooltip: 'topMiddle'
            },
            position: {
                my: 'bottom center',
                at: 'top center'
            }
        }
    }).bind('click', function(event) {
        event.preventDefault();
        return false;
    });

    $('.ec-beta').qtip({
        content: {
            text: 'Služba immmr je zatiaľ v pilotnej prevádzke. Pomôžte nám ju zlepšiť. Svoje podnety posielajte na <a href="mailto:data@telekom.sk">data@telekom.sk</a>'
        },
        hide: {
            event: 'unfocus'
        },
        position: {
            my: 'left center',
            at: 'right center'
        }
    });

    $('.roaming-info').qtip({
        content: 'Všetky členské štáty EÚ a Island, Nórsko, Lichtenštajnsko',
        position: {
            corner: {
                target: 'leftMiddle',
                tooltip: 'topMiddle'
            }
        }
    }).bind('click', function(event) {
        event.preventDefault();
        return false;
    });

    $('.tooltip').qtip({
        content: {
            text: 'Vybrané paušály sú:<br> Happy M, L, XL volania, XL, XXL a Profi'
        },
        position: {
            my: 'bottom center',
            at: 'top center'
        }
    });

    $('.akcie-info').qtip({
        content: '<div class="tag-left">Novým zákazníkom objem dát navýšime pri aktivácii paušálu a verným od najbližšieho zúčtovacieho obdobia po 15. 3. 2017, najneskôr však do 9.4.2017.</div>',
        position: {
            corner: {
                target: 'leftMiddle',
                tooltip: 'topMiddle'
            }
        }
    }).bind('click', function(event) {
        event.preventDefault();
        return false;
    });

    $('.happy_xs-info').qtip({
        content: 'Počas pracovných dní v čase od 19.00 hod. do 7.00 hod. Soboty, nedele a štátne sviatky počas celých 24 hodín.',
        position: {
            corner: {
                target: 'leftMiddle',
                tooltip: 'topMiddle'
            }
        }
    }).bind('click', function(event) {
        event.preventDefault();
        return false;
    });

    $('.happy_young-info').qtip({
        content: 'Web zľavu až 20 € na mobil získate iba pri kúpe paušálu na našom webe. Konkrétna výška zľavy bude vypočítaná pri nákupe.',
        position: {
            corner: {
                target: 'leftMiddle',
                tooltip: 'topMiddle'
            }
        }
    }).bind('click', function(event) {
        event.preventDefault();
        return false;
    });

    /* ::: Order Form for Mobile*/
    var orderUrl = '/mam-zaujem/volania/happy/formular?pausal=';

    $('#order-happy-xs-mini').click(function(event) {
        /*event.preventDefault();*/
        if (window.matchMedia('(max-width: 600px)').matches) {
            $(this).attr("href", orderUrl + 'Happy XS mini');
        } else {}
    });

    $('#order-happy-xs').click(function(event) {
        /*event.preventDefault();*/
        if (window.matchMedia('(max-width: 600px)').matches) {
            $(this).attr("href", orderUrl + 'Happy XS');
        } else {}
    });

    $('#order-happy-s').click(function(event) {
        /*event.preventDefault();*/
        if (window.matchMedia('(max-width: 600px)').matches) {
            $(this).attr("href", orderUrl + 'Happy S');
        } else {}
    });

    $('#order-happy-m').click(function(event) {
        /*event.preventDefault();*/
        if (window.matchMedia('(max-width: 600px)').matches) {
            $(this).attr("href", orderUrl + 'Happy M');
        } else {}
    });

    $('#order-happy-l').click(function(event) {
        /*event.preventDefault();*/
        if (window.matchMedia('(max-width: 600px)').matches) {
            $(this).attr("href", orderUrl + 'Happy L');
        } else {}

    });

    $('#order-happy-xl-call').click(function(event) {
        /*event.preventDefault();*/
        if (window.matchMedia('(max-width: 600px)').matches) {
            $(this).attr("href", orderUrl + 'Happy XL Volania');
        } else {}

    });

    $('#order-happy-xl').click(function(event) {
        /*event.preventDefault();*/
        if (window.matchMedia('(max-width: 600px)').matches) {
            $(this).attr("href", orderUrl + 'Happy XL');
        } else {}

    });

    $('#order-happy-xxl').click(function(event) {
        /*event.preventDefault();*/
        if (window.matchMedia('(max-width: 600px)').matches) {
            $(this).attr("href", orderUrl + 'Happy XXL');
        } else {}

    });

    $('#order-happy-profi').click(function(event) {
        /*event.preventDefault();*/
        if (window.matchMedia('(max-width: 600px)').matches) {
            $(this).attr("href", orderUrl + 'Happy Profi');
        } else {}

    });


    // 	/* TABS */
    // 	$(".tabs-menu a").click(function(event) {
    // 	    event.preventDefault();
    // 	    $(this).parent().addClass("current");
    // 	    $(this).parent().siblings().removeClass("current");
    // 	    var tab = $(this).attr("href"), // select clicked tab. In example "#tab-1" ..
    // 	        parent = $(this).closest('ul').parent().parent(); // <tabs-container>
    // 	    parent.find('.tab-content').not(tab).css("display", "none"); //hide all not clicked tabs
    // 	    $(tab).fadeIn(); //show clicked tab


    // 	});
    // 	/* TABS END */

    // 	/* FROM TABS TO ACCORDION */
    // 	var dataSegment = $("[data-segment]").each(function() {
    //   //console.log($(this).attr('data-segment')); //zobraz "data-segment" hodnotu pre kazdu <section>

    //   closestHead = $($(this).find('.tabs-menu a'));
    //   closestItemCount = closestHead.length - 1; //vytiahni pocet tabov pre kazdu <section> s atributom "data-segment"
    //   //console.log(closestItemCount + "heads");

    //   closestContent = $($(this).find('.tab-content')); //najdi tab-content v danej <section>
    //   //console.log(closestContent);

    //   closestAccordion = $(this).find('.accordion'); //najdi .accordion v danej <section>

    //   accordionItem = '<div class="item">';

    //   for (var i = 0; i <= closestItemCount; i++) {
    //     accordionItem += '<div class="heading">' + $(closestHead[i]).text() + '</div>';
    //     accordionItem += '<div class="content">' + $(closestContent[i]).html() + '</div></div>';

    //     if (i !== closestItemCount) {
    //       accordionItem += '<div class="item">';
    //     }
    //   }

    //   //if data-segment and data-accordion value match, show accordion data
    //   if ($(this).attr('data-segment') === closestAccordion.attr('data-accordion')) {
    //     $(accordionItem).appendTo(closestAccordion);
    //   }

    //   var $items = $('.accordion .item');
    //   //SET OPENED ITEM
    //   $($items[0]).addClass('open');
    //   //$($items[1]).addClass('open');

    // });
    // /* FROM TABS TO ACCORDION END */

    // /* ACCORDION */
    // $('.accordion .item .heading').click(function(e) {

    //   var clickedHead = e.target;
    //   var $item = $(clickedHead).closest('item');
    //   var isOpen = $item.hasClass('open');
    //   var $content = $item.find('.content');
    //   var $acPrice = $(clickedHead).find('.ac-price');

    //   if (isOpen) {
    //     $content.slideUp(200);
    //     $item.removeClass('open');
    //     $acPrice.show();
    //   } else {
    //     //$content.slideDown(200);
    //     $item.addClass('open');
    //     $acPrice.hide();
    //   }
    // });


    // $('.accordion .item.open').find('.content').slideDown(200);
    // /* ACCORDION END */

    ////////////////////////////////////
    $('.as1').slick({
        infinite: false,
        slidesToShow: 2,
        slidesToScroll: 1,
        infinite: false,
        dots: true,
        responsive: [{
            breakpoint: 700,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: true
            }
        }]
    });

    $('.as2').slick({
        infinite: false,
        slidesToShow: 2,
        slidesToScroll: 1,
        infinite: false,
        dots: true,
        responsive: [{
            breakpoint: 700,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: true
            }
        }]
    });
    /* ACCORDION */

    // $('.accordion .item .heading').click(function(e) {

    //     var clickedHead = e.target;
    //     var $item = $(clickedHead).closest('.item');
    //     var isOpen = $item.hasClass('open');
    //     var $content = $item.find('.content');
    //     var $acPrice = $(clickedHead).find('.ac-price');

    //     if (isOpen) {
    //         $content.slideUp(200);
    //         $item.removeClass('open');
    //         $acPrice.show();
    //     } else {
    //         $content.slideDown(200);
    //         $item.addClass('open');
    //         $acPrice.hide();
    //     }

    // });

    // $('.accordion .item.open').find('.content').slideDown(200);


	// 	$(".show-hide").click(function () {
	// 		var a = $(this).attr("data-container");
	// 		$(this).toggleClass("open-down");
	// 		$("#" + a).slideToggle(500)
	// 	});

});

jQuery(document).ready(function() {
    jQuery('ul.taby2 li a').on('click', function(e) {
        $('.as2').css("visibility", "hidden");
        var currentAttrValue = jQuery(this).attr('href');

        jQuery('.tab ' + currentAttrValue).show().siblings().hide();

        jQuery(this).parent('li').addClass('active').siblings().removeClass('active');
        $('.as2').slick('setPosition');
        setTimeout(function() {
            $('.as2').css("visibility", "visible");
        }, 200);
        e.preventDefault();
    });
});


/* auto scroll from hash */
jQuery(document).ready(function() {
    // if (window.location.hash) scroll(0, 0);
    // setTimeout(function() {
    //     scroll(0, 0);
    // }, 1);

    if (window.location.hash) {
        var urlHash = window.location.hash;
        var $items = $('.accordion .item');

        function scrollBasedOnURL(scrollto, link) {
            //if ($(window).width() > 768) {
                $('html, body').animate({
                    scrollTop: ($(scrollto).offset().top - 60) + 'px'
                }, 1000);
                $(link).eq(0).prev().trigger('click');
            //}
        }

        function scrollHandlerBasedOnURLParams(scrollto, scrolltoMobile, link) {
            if ($(window).width() > 768) {
                scrollBasedOnURL(scrollto, link);
            }
        }

        function showPausalOnMobile (id, scrollToMobile) {
            $($('.plm__plan')).each(function() {
                if ($(this).data('objid') == id && $(window).width() <= 768 ) {
                    $('*[data-objid="'+id+'"]').find('.plan-show-detail_tab').trigger('click');
                }
              });
        }

        function scrollToTabAndOpen(tab) {
            $('html, body').animate({
                    scrollTop: ($("#sec-7").offset().top - 85) + 'px'
            }, 1000);
            $(tab).trigger('click');
            //console.log('\"'+ tab +'\"');
        }

        function scrollToAccordionAndOpen() {
            $('html, body').animate({
                scrollTop: ($("#sec-7").offset().top) + 'px'
        }, 1000);
        }

        setTimeout(function() {
            switch (urlHash) {
                case '#tab-2':
                    $('html, body').animate({
                        scrollTop: ($("#sec-6").offset().top - 55) + 'px'
                    }, 1000);
                    break;
                case '#tab-3':
                case '#tab-4':
                case '#tab-5':
                case '#tab-11':
                    $("#sec-7 .tabs-container .tabs-menu .variant").removeClass("current");
                    $("#sec-7 .tabs-container .tabs-menu " + urlHash + '-li').addClass("current");

                    $("#sec-7 .tab .tab-content").hide(); 
                    $("#sec-7 .tab " + urlHash).show();

                    $('html, body').animate({
                        scrollTop: ($("#sec-7").offset().top - 75) + 'px'
                    }, 1000);

	                break;                    
                case '#ano-s':
                    scrollHandlerBasedOnURLParams('#prod-0', $('*[data-objid="0"]'), 'a[href="/mob/objednavka/-/scenario/e-shop/pausal?vyberrp=s"]');
                    showPausalOnMobile(0);
                    break;
                case '#ano-m':
                    scrollHandlerBasedOnURLParams('#prod-1', $('*[data-objid="1"]') ,'a[href="/mob/objednavka/-/scenario/e-shop/pausal?vyberrp=m"]');
                    showPausalOnMobile(1, $('*[data-objid="1"]'));
                    break;
                case '#ano-mdata':
                    scrollHandlerBasedOnURLParams('#prod-2', $('*[data-objid="2"]') ,'a[href="/mob/objednavka/-/scenario/e-shop/pausal?vyberrp=m_data"]');
                    showPausalOnMobile(2, $('*[data-objid="2"]'));
                    break;
                case '#ano-l':
                    scrollHandlerBasedOnURLParams('#prod-3', $('*[data-objid="3"]'), 'a[href="/mob/objednavka/-/scenario/e-shop/pausal?vyberrp=l"]');
                    showPausalOnMobile(3);
                    break;
                case '#ano-xl':
                    scrollHandlerBasedOnURLParams('#prod-4', $('*[data-objid="4"]'), 'a[href="/mob/objednavka/-/scenario/e-shop/pausal?vyberrp=xl"]');
                    showPausalOnMobile(4);
                    break;
                case '#ano-xxl':
                    scrollHandlerBasedOnURLParams('#prod-5', $('*[data-objid="5"]'), 'a[href="/mob/objednavka/-/scenario/e-shop/pausal?vyberrp=xxl"]');
                    showPausalOnMobile(5);
                    break;
                case '#vyberrp=xl_call':
                    scrollHandlerBasedOnURLParams('#prod-2', 'a[href="/mob/objednavka/-/scenario/e-shop/pausal?vyberrp=xl_call"]');
                    break;
                case '#vyberrp=xl':
                    scrollHandlerBasedOnURLParams('#prod-2', 'a[href="/mob/objednavka/-/scenario/e-shop/pausal?vyberrp=xl"]');
                    break;
                case '#vyberrp=xxl':
                    scrollHandlerBasedOnURLParams('#prod-2', 'a[href="/mob/objednavka/-/scenario/e-shop/pausal?vyberrp=xxl"]');
                    break;
                case '#vyberrp=profi':
                    scrollHandlerBasedOnURLParams('#prod-2', 'a[href="/mob/objednavka/-/scenario/e-shop/pausal?vyberrp=profi"]');
                    break;
                case '#bezBarier':
                    if ($(window).width() <= 550) {
                            scrollToAccordionAndOpen()
                            $($items[1]).removeClass('open');
                        }
                        else {
                            scrollToTabAndOpen('#bezBarier a')
                        }

                    break;
                case '#magenta1':
                    if ($(window).width() <= 550) {
                            scrollToAccordionAndOpen()
                            $($items[0]).removeClass('open');
                            $($items[1]).addClass('open');
                        }
                        else {
                            scrollToTabAndOpen('#magenta1 a')
                        }

                    break;
                case '#najsiet':
                    if ($(window).width() <= 550) {
                            scrollToAccordionAndOpen()
                            $($items[0]).removeClass('open');
                            $($items[1]).removeClass('open');
                            $($items[2]).addClass('open');
                        }
                        else {
                            scrollToTabAndOpen('#najsiet a')
                        }

                    break;
                case '#streamOn':
                    if ($(window).width() <= 550) {
                            scrollToAccordionAndOpen()
                            $($items[0]).removeClass('open');
                            $($items[1]).removeClass('open');
                            $($items[2]).removeClass('open');
                            $($items[3]).addClass('open');
                        }
                        else {
                            scrollToTabAndOpen('#streamOn a')
                        }

                    break;                    

                default:
                    $('html, body').animate({
                        scrollTop: ($(window.location.hash).offset().top - 0) + 'px'
                    }, 1000);
            };
        }, 1000);
    }

    // AKTUÁLNE AKCIE click
    if ($(window).width() > 769) {
        $("#activate-lightbox-young").click(function(e) {
            e.preventDefault();
            $('html, body').animate({
                scrollTop: ($(".plp__plan-ribbon:contains(LEN DO 26 ROKOV)").offset().top - 130) + 'px'
            }, 1000);
            $('a[href="/mob/objednavka/-/scenario/e-shop/happy?vyberrp=xl_data_premladych"]').eq(0).prev().trigger('click');
        });
    } else {
        $("#activate-lightbox-young").click(function(e) {
            e.preventDefault();
            $('html, body').animate({
                scrollTop: ($(".plm__plan-ribbon:contains(LEN DO 26 ROKOV)").offset().top - 130) + 'px'
            }, 1000);
            $('a[href="/mob/objednavka/-/scenario/e-shop/happy?vyberrp=xl_data_premladych"]').eq(2).prev().trigger('click');
        });
    }

    /* SCROLLING THE NAV */

    function resetCheckboxes() {
        $('#base').prop('checked', true);
        $('#base').closest('.checker > span').addClass('checked');
        $('#favourite').prop('checked', true);
        $('#favourite').closest('.checker > span').addClass('checked');
        $('#experienced').prop('checked', true);
        $('#experienced').closest('.checker > span').addClass('checked');
    }
});

$(document).ready(function() {
    function checkMobileOS() {
      var MobileUserAgent = navigator.userAgent || navigator.vendor || window.opera;
      if (MobileUserAgent.match(/iPad/i) || MobileUserAgent.match(/iPhone/i) || MobileUserAgent.match(/iPod/i)) {
        return "iOS";
      } else if (MobileUserAgent.match(/Android/i)) {
        return "Android";
      } else {
        return "unknown";
      }
    }
    $(".sms_link").each(function(index) {
        $(this).on("click", function() {
            // event.preventDefault();
            var href = '';
            var message_text = this.dataset.smsText;
            var message_number = this.dataset.smsNumber;
            if (checkMobileOS() == 'iOS') {href = "sms:" + message_number + "&body=" + encodeURI(message_text); }
            if (checkMobileOS() == 'Android') { href = "sms:" + message_number + "?body=" + encodeURI(message_text); }
            $(this).attr('href', href);
        });
    });
});

/* TOGGLE ARROW */
$(document).on('click', '.toggle-arrow, .arrow-right', function(event) {
  event.preventDefault();
  if ($(this).hasClass('arrow-right')) {
    return;
  }
  $(event.target).closest('.togleArrow').find('.arrow-content').toggle(200);
  $(this).find('.arrow-right').toggleClass('arrow-rotate');
});
/* TOGGLE ARROW END */



/* LOAD CHRISTMAS 2018 SECTION */
$(document).ready(function() {
    $( ".sec-akcie" ).load( "templates/_aktualne-akcie.html", function() {
        // *** Calculate prices   
        var pricesAll = {
            s: [
                ['10,00','11,00', 'zadarmo', '139,00', '149,00',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=s&zariadenie=irobot_roomba_965',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=s&zariadenie=irobot_roomba_965',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=s&zariadenie=irobot_roomba_965',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=s&zariadenie=irobot_roomba_965',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=s&zariadenie=irobot_roomba_965'
                ],
                ['10,00', '11,00', 'zadarmo', '139,00', '149,00',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=s&zariadenie=sony_kdl50wf665b',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=s&zariadenie=sony_kdl50wf665b',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=s&zariadenie=sony_kdl50wf665b',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=s&zariadenie=sony_kdl50wf665b'
                ],
                ['10,00', '8,00', 'zadarmo', '139,00', '149,00',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=s&zariadenie=playstation_4_3hry_2ovladac',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=s&zariadenie=playstation_4_3hry_2ovladac',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=s&zariadenie=playstation_4_3hry_2ovladac',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=s&zariadenie=playstation_4_3hry_2ovladac'
                ],
                ['10,00', '9,00', 'zadarmo', '139,04', '149,04',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=s&zariadenie=huawei_p20_lite_huawei_mediapad_t3_10',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=s&zariadenie=huawei_p20_lite_huawei_mediapad_t3_10',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=s&zariadenie=huawei_p20_lite_huawei_mediapad_t3_10',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=s&zariadenie=huawei_p20_lite_huawei_mediapad_t3_10'
                ],
                ['10,00','9,00','zadarmo','139,04','149,04',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=s&zariadenie=huawei_p20_lite_huawei_y6_2018',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=s&zariadenie=huawei_p20_lite_huawei_y6_2018',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=s&zariadenie=huawei_p20_lite_huawei_y6_2018',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=s&zariadenie=huawei_p20_lite_huawei_y6_2018'
                ]
            ],
            m: [
                ['20,00','11,00','zadarmo','99,00','119,00',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=m&zariadenie=irobot_roomba_965',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=m&zariadenie=irobot_roomba_965',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=m&zariadenie=irobot_roomba_965',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=m&zariadenie=irobot_roomba_965'
                ],
                ['20,00','11,00','zadarmo','99,00','119,00',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=m&zariadenie=sony_kdl50wf665b',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=m&zariadenie=sony_kdl50wf665b',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=m&zariadenie=sony_kdl50wf665b',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=m&zariadenie=sony_kdl50wf665b'
                ],
                ['20,00','8,00','zadarmo','99,00','119,00',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=m&zariadenie=playstation_4_3hry_2ovladac',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=m&zariadenie=playstation_4_3hry_2ovladac',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=m&zariadenie=playstation_4_3hry_2ovladac',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=m&zariadenie=playstation_4_3hry_2ovladac'
                ],
                ['20,00','9,00','zadarmo','99,04','119,04',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=m&zariadenie=huawei_p20_lite_huawei_mediapad_t3_10',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=m&zariadenie=huawei_p20_lite_huawei_mediapad_t3_10',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=m&zariadenie=huawei_p20_lite_huawei_mediapad_t3_10',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=m&zariadenie=huawei_p20_lite_huawei_mediapad_t3_10'
                ],
                ['20,00','9,00','zadarmo','99,04','119,04',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=m&zariadenie=huawei_p20_lite_huawei_y6_2018',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=m&zariadenie=huawei_p20_lite_huawei_y6_2018',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=m&zariadenie=huawei_p20_lite_huawei_y6_2018',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=m&zariadenie=huawei_p20_lite_huawei_y6_2018'
                ]
            ],
            m_data: [
                ['20,00','11,00','zadarmo','99,00','119,00',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=m_data&zariadenie=irobot_roomba_965',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=m_data&zariadenie=irobot_roomba_965',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=m_data&zariadenie=irobot_roomba_965',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=m_data&zariadenie=irobot_roomba_965'
                ],
                ['20,00','11,00','zadarmo','99,00','119,00',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=m_data&zariadenie=sony_kdl50wf665b',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=m_data&zariadenie=sony_kdl50wf665b',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=m_data&zariadenie=sony_kdl50wf665b',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=m_data&zariadenie=sony_kdl50wf665b'
                ],
                ['20,00','8,00','zadarmo','99,00','119,00',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=m_data&zariadenie=playstation_4_3hry_2ovladac',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=m_data&zariadenie=playstation_4_3hry_2ovladac',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=m_data&zariadenie=playstation_4_3hry_2ovladac',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=m_data&zariadenie=playstation_4_3hry_2ovladac'
                ],
                ['20,00','9,00','zadarmo','99,04','119,00',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=m_data&zariadenie=huawei_p20_lite_huawei_mediapad_t3_10',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=m_data&zariadenie=huawei_p20_lite_huawei_mediapad_t3_10',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=m_data&zariadenie=huawei_p20_lite_huawei_mediapad_t3_10',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=m_data&zariadenie=huawei_p20_lite_huawei_mediapad_t3_10'
                ],
                ['20,00','9,00','zadarmo','99,04','119,04',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=m_data&zariadenie=huawei_p20_lite_huawei_y6_2018',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=m_data&zariadenie=huawei_p20_lite_huawei_y6_2018',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=m_data&zariadenie=huawei_p20_lite_huawei_y6_2018',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=m_data&zariadenie=huawei_p20_lite_huawei_y6_2018'
                ]
            ],
            l: [
                ['30,00','10,00','zadarmo','19,00','39,00',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=l&zariadenie=irobot_roomba_965',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=l&zariadenie=irobot_roomba_965',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=l&zariadenie=irobot_roomba_965',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=l&zariadenie=irobot_roomba_965'
                ],
                ['30,00','10,00','zadarmo','19,00','39,00',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=l&zariadenie=sony_kdl50wf665b',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=l&zariadenie=sony_kdl50wf665b',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=l&zariadenie=sony_kdl50wf665b',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=l&zariadenie=sony_kdl50wf665b'
                ],
                ['30,00','7,00','zadarmo','19,00','39,00',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=l&zariadenie=playstation_4_3hry_2ovladac',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=l&zariadenie=playstation_4_3hry_2ovladac',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=l&zariadenie=playstation_4_3hry_2ovladac',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=l&zariadenie=playstation_4_3hry_2ovladac'
                ],
                ['30,00','8,00','zadarmo','19,04','39,04',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=l&zariadenie=huawei_p20_lite_huawei_mediapad_t3_10',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=l&zariadenie=huawei_p20_lite_huawei_mediapad_t3_10',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=l&zariadenie=huawei_p20_lite_huawei_mediapad_t3_10',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=l&zariadenie=huawei_p20_lite_huawei_mediapad_t3_10'
                ],
                ['30,00','8,00','zadarmo','19,04','39,04',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=l&zariadenie=huawei_p20_lite_huawei_y6_2018',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=l&zariadenie=huawei_p20_lite_huawei_y6_2018',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=l&zariadenie=huawei_p20_lite_huawei_y6_2018',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=l&zariadenie=huawei_p20_lite_huawei_y6_2018'
                ]
            ],
            xl: [
                ['40,00','7,00','zadarmo','1,00','19,00',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=xl&zariadenie=irobot_roomba_965',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=xl&zariadenie=irobot_roomba_965',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=xl&zariadenie=irobot_roomba_965',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=xl&zariadenie=irobot_roomba_965'
                ],
                ['40,00','7,00','zadarmo','1,00','19,00',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=xl&zariadenie=sony_kdl50wf665b',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=xl&zariadenie=sony_kdl50wf665b',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=xl&zariadenie=sony_kdl50wf665b',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=xl&zariadenie=sony_kdl50wf665b'
                ],
                ['40,00','4,00','zadarmo','1,00','19,00',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=xl&zariadenie=playstation_4_3hry_2ovladac',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=xl&zariadenie=playstation_4_3hry_2ovladac',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=xl&zariadenie=playstation_4_3hry_2ovladac',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=xl&zariadenie=playstation_4_3hry_2ovladac'
                ],
                ['40,00','5,00','zadarmo','1,04','19,04',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=xl&zariadenie=huawei_p20_lite_huawei_mediapad_t3_10',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=xl&zariadenie=huawei_p20_lite_huawei_mediapad_t3_10',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=xl&zariadenie=huawei_p20_lite_huawei_mediapad_t3_10',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=xl&zariadenie=huawei_p20_lite_huawei_mediapad_t3_10'
                ],
                ['40,00','5,00','zadarmo','1,04','19,04',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=xl&zariadenie=huawei_p20_lite_huawei_y6_2018',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=xl&zariadenie=huawei_p20_lite_huawei_y6_2018',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=xl&zariadenie=huawei_p20_lite_huawei_y6_2018',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=xl&zariadenie=huawei_p20_lite_huawei_y6_2018'
                ]
            ],
            xxl: [
                ['70,00','2,00','zadarmo','1,00','1,00',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=xxl&zariadenie=irobot_roomba_965',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=xxl&zariadenie=irobot_roomba_965',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=xxl&zariadenie=irobot_roomba_965',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=xxl&zariadenie=irobot_roomba_965'
                ],
                ['70,00','2,00','zadarmo','1,00','1,00',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=xxl&zariadenie=sony_kdl50wf665b',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=xxl&zariadenie=sony_kdl50wf665b',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=xxl&zariadenie=sony_kdl50wf665b',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=xxl&zariadenie=sony_kdl50wf665b'
                ],
                ['70,00','1,00','zadarmo','1,00','1,00',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=xxl&zariadenie=playstation_4_3hry_2ovladac',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=xxl&zariadenie=playstation_4_3hry_2ovladac',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=xxl&zariadenie=playstation_4_3hry_2ovladac',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=xxl&zariadenie=playstation_4_3hry_2ovladac'
                ],
                ['70,00','1,00','zadarmo','1,04','1,04',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=xxl&zariadenie=huawei_p20_lite_huawei_mediapad_t3_10', //set green btn url href
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=xxl&zariadenie=huawei_p20_lite_huawei_mediapad_t3_10', //more info btn
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=xxl&zariadenie=huawei_p20_lite_huawei_mediapad_t3_10', //device headline btn
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=xxl&zariadenie=huawei_p20_lite_huawei_mediapad_t3_10'  //on whole element click
                ],
                ['70,00','1,00','zadarmo','1,04','1,04',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=xxl&zariadenie=huawei_p20_lite_huawei_y6_2018',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=xxl&zariadenie=huawei_p20_lite_huawei_y6_2018',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=xxl&zariadenie=huawei_p20_lite_huawei_y6_2018',
                'https://www.telekom.sk/mob/objednavka/pausal/-/scenario/e-shop/pausal-ano?vyberrp=xxl&zariadenie=huawei_p20_lite_huawei_y6_2018'
                ]
            ]
        }

        var mobileWidth = 550;

        function addPrices(program, mobileWidth) {
            var prices = pricesAll[program];
            var objsize = pricesAll["m"].length;
            console.log(objsize)

            if ($(window).width() <= mobileWidth) {
                //mobile
                $('.accordion .tab-acc .tab-acc-sub').each(function (key) {
                    if (key < objsize) {
                        if (program == "m_data") {
                            $(this).find('.tab-ex-label .tab-ex-label-program').text("M Dáta");
                        } else {
                            $(this).find('.tab-ex-label .tab-ex-label-program').text(program);
                        }
                        //prices or zadarmo
                        $(this).find('.tab-ex-price').each(function (key2) {
                            $(this).text(prices[key][key2] + (prices[key][key2] == "zadarmo" ? '': ' €'));
                        });
                        //2gb more
                        if (program == "s" || program == "m" || program == "m_data") {
                            $(this).find('.2gbmore, .sticker-picture2').hide();
                        } else {
                            $(this).find('.2gbmore, .sticker-picture2').show();
                        }
                        //on whole element click
                        $(this).attr("data-href", prices[key][8]);
                        //device headline btn
                        $(this).find('.a-title').attr('href', prices[key][7]);
                        //more info btn
                        $(this).find('.action-info a').attr('href', prices[key][6]);
                        //set green btn url href
                        $(this).find('.tab-action a').attr('href', prices[key][5]);
                    }
                });
            } else {
                //desktop
                $('#tab-1 .tab-acc .tab-acc-sub').each(function (key) {
                    if (key < objsize) {
                        if (program == "m_data") {
                            $(this).find('.tab-ex-label .tab-ex-label-program').text("M Dáta");
                        } else {
                            $(this).find('.tab-ex-label .tab-ex-label-program').text(program);
                        }
                        //prices or zadarmo
                        $(this).find('.tab-ex-price').each(function (key2) {
                            $(this).text(prices[key][key2] + (prices[key][key2] == "zadarmo" ? '': ' €'));
                        });
                        //2gb more
                        if (program == "s" || program == "m" || program == "m_data") {
                            $(this).find('.2gbmore, .sticker-picture2').hide();
                        } else {
                            $(this).find('.2gbmore, .sticker-picture2').show();
                        }
                        //on whole element click
                        $(this).attr("data-href", prices[key][8]);
                        //device headline btn
                        $(this).find('.a-title').attr('href', prices[key][7]);
                        //more info btn
                        $(this).find('.action-info a').attr('href', prices[key][6]);
                        //set green btn url href
                        $(this).find('.tab-action a').attr('href', prices[key][5]);
                    }
                });
            }

            //hide christmas "full store price" if price = 0;
            var christmasStorePrice = $('.christmas.tab-ex-price');
            $(christmasStorePrice).each(function (key3) {
                if ($(christmasStorePrice).eq(key3).text() === "0 €") {
                    $(christmasStorePrice).eq(key3).closest('.christmas.grey').hide();
                } else {
                    $(christmasStorePrice).eq(key3).closest('.christmas.grey').show();
                }
            });
        }

        var init = "l";
        addPrices(init, mobileWidth); //load prices on page init
        
        //heapboxes on page init
            //heapbox mobile
        $('.accordion #select_program').attr('id','select_program2')
        $('.accordion #select_program2').heapbox({
            "onChange":function(val, elm) {   
                init = $(elm).val(); 
                addPrices($(elm).val(), mobileWidth);
            }
        });
        $('#heapbox_select_program2').show();
        
            //heapbox desktop
        $('#select_program').heapbox({
            "onChange":function(val, elm) {
                init = $(elm).val();
                addPrices($(elm).val(), mobileWidth);              
            }
        });
        $('#heapbox_select_program').show();

        //fix window resize heapbox
        $( window ).resize(function() {
            $('.accordion #select_program2').heapbox({
                "onChange":function(val, elm) {    
                    init = $(elm).val();
                    addPrices($(elm).val(), mobileWidth);
                }
            });
            addPrices(init, mobileWidth);
            $('#heapbox_select_program2').show();
            
                //heapbox desktop
            $('#select_program').heapbox({
                "onChange":function(val, elm) {
                    init = $(elm).val();
                    addPrices($(elm).val(), mobileWidth);
                }
            });
            addPrices(init, mobileWidth);
            $('#heapbox_select_program').show();
        });
        // *** Calculate prices - END
    });
});
/* LOAD CHRISTMAS 2018 SECTION - END */