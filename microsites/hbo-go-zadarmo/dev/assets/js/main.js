$(document).ready(function () {

  $('#max-price').qtip({
    content: 'Číslo adresáta nájdete na svojej faktúre napravo hore',
    position: {
      my: 'bottom center',
      at: 'right top'
    }
  });

  var apiUrlSms = 'https://m.telekom.sk/api/hbo/sms';
  var apiUrlCode = 'https://m.telekom.sk/api/hbo/code';

  if (window.location.hostname === "localhost") {
    apiUrlSms = 'http://mr.telekom.localhost/api/hbo/sms';
    apiUrlCode = 'http://mr.telekom.localhost/api/hbo/code';
  }

  var $userCode = $('#userCode');
  var $userName = $('#userName');
  var $phoneNum = $('#phoneNum');
  var $smsCode = $('#smsCode');
  var $buttonCode = $('#send-code');

  var $otpError = $('#otp-error');
  var $otpInfo = $('#otp-info');

  var $contactForm = $("#contact-form");
  var $codeForm = $("#code-form");

  var $customerError = $("#customer-error");

  var $activated = $("#activated");
  var $activatedInfo = $(".activated-info");

  var smsToken = '';

  $contactForm.parsley();
  $codeForm.parsley();

  $contactForm.submit(function (e) {
    e.preventDefault();

    $customerError.hide();

    $.ajax({
      url: apiUrlSms + '?name=' + $userName.val() + '&msisdn=' + $phoneNum.val() + '&customer=' + $userCode.val(),
      type: "GET",
      async: true,
      crossDomain: true,
      dataType: "json",
      success: function (data) {
        $contactForm.hide();
        $codeForm.show();

        smsToken = data.token;
      },
      error: function (error) {
        if (error) {
          console.log('error');
        }
      }
    });
  });

  $codeForm.submit(function (e) {
    e.preventDefault();

    //zobrazime informaciu o cakani na response
    $otpInfo.show();

    //schovame informaciu o OTP error
    $otpError.hide();

    $buttonCode.addClass("disabled").prop("disabled", true);
    // $buttonCode.addClass("disabled");

    $.ajax({
      url: apiUrlCode + '?token=' + smsToken + '&code=' + $smsCode.val(),
      type: "GET",
      async: true,
      crossDomain: true,
      dataType: "json",
      success: function (data) {

        $buttonCode.removeClass("disabled").prop("disabled", false);
        // $buttonCode.removeClass("disabled");

        //schovame informaciu o cakani na response
        $otpInfo.hide();

        if (data.status == 'ok') {

          //schovame OTP stranku
          $codeForm.hide();

          //zobrazime response o uspesnom zriadeni sluzby
          $activatedInfo.html(data.description);
          $activated.show();

        } else {

          //schovame OTP stranku
          $codeForm.hide();
          $smsCode.val('');

          //zobrazime prvu stranku
          $contactForm.show();

          //zobrazime chybovu hlasku
          if (data.description == '') {
            $customerError.show().html('Nepodarilo sa overiť číslo adresáta. Skúste to prosím neskôr');
          } else {
            $customerError.show().html(data.description);
          }

        }

      },

      error: function (error) {
        if (error) {
          $buttonCode.removeClass("disabled").prop("disabled", false);
          // $buttonCode.removeClass("disabled");

          //schovame informaciu o cakani na response
          $otpInfo.hide();

          //zobrazime chybovu hlasku

          if (error.responseJSON.description == '') {
            $otpError.show().html('Pri orverení kódu nastala chyba. Skúste to prosím neskôr');
          } else {
            $otpError.show().html(error.responseJSON.description);
          }
        }

      }
    });
  });
});