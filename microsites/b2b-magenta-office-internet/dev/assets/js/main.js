$(function () {

    /* TOOLTIP */

    $('.info').qtip({
        content: {
            text: 'V prípade výpadku pevnej siete, zostanete online cez záložný prístup prostredníctvom mobilnej siete'
        }
    });

    /* STICKY NAV */

    $("#nav-sticky").sticky({
        topSpacing: 0,
        widthFromWrapper: true
    });

    $(".scroll-to").click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top - ($("#nav-sticky").outerHeight())
                }, 1000);
                return false;
            }
        }
    });

    var contentSections = $("[id*='sec-']"),
        secondaryNav = $("#nav-sticky");

    function updateSecondaryNavigation() {
        contentSections.each(function () {
            var actual = $(this),
                actualHeight = actual.height() + parseInt(actual.css('paddingTop').replace('px', '')) + parseInt(actual.css('paddingBottom').replace('px', '')),
                actualAnchor = secondaryNav.find('a[href="#' + actual.attr('id') + '"]');

            if ((actual.offset().top - secondaryNav.outerHeight() <= $(window).scrollTop()) && (actual.offset().top + actualHeight - secondaryNav.outerHeight() > $(window).scrollTop())) {
                actualAnchor.addClass('active');
            } else {
                actualAnchor.removeClass('active');
            }

        });
    }
    updateSecondaryNavigation();

    $(window).scroll(function (event) {
        updateSecondaryNavigation();
    });

    /* TABS */

    $(".tabs-menu a").click(function (event) {
        event.preventDefault();
        if (!$(this).parent().hasClass("current") && !$(this).parent().hasClass("isVariant")) {
            toggleTabs();
        }


        if ($(this).parent().hasClass("variant") && !$(this).parent().hasClass("current")) {
            $('.sec__type').html('TECHNOLÓGIE A PROGRAMY SLUŽBY ' + negVarName());
        }

        $(this).parent().addClass("current");
        $(this).parent().siblings().removeClass("current");
        var tab = $(this).attr("href"),
            parent = $(this).closest('ul').parent().parent();
        parent.find('.tab-content').not(tab).css("display", "none");
        $(tab).fadeIn();

    });

    /* SWITCHING THE VARIANTS */

    var getVarNum = function () {
        for (var i = 0; i <= $('.variant').length - 1; i++) {
            if ($('.variant').eq(i).hasClass('current')) {
                var currVarNum = $('.variant').eq(i).data('variant');
                return currVarNum;
            }
        }
    };

    var curVarName = function () {
        for (var i = 0; i <= $('.variant').length - 1; i++) {
            if ($('.variant').eq(i).hasClass('current')) {
                var varName = '<b>' + $('.variant').eq(i).text().toUpperCase() + '</b>';
                return varName;
            }
        }
    };

    var negVarName = function () {
        for (var i = 0; i <= $('.variant').length - 1; i++) {
            if (!$('.variant').eq(i).hasClass('current')) {
                var varName = '<b>' + $('.variant').eq(i).text().toUpperCase() + '</b>';
                return varName;
            }
        }
    };

    var toggleTabs = function () {
        $('.tab__variant').toggleClass('selected');
        $('.oa__table').toggleClass('selected');
    };

    $('.sec__type').html('TECHNOLÓGIE A PROGRAMY SLUŽBY ' + curVarName());

    /* FORM */

    var $formButtons = $('.tab-content .button'),
        $formTextArea = $('.form-msg textarea');

    function toTitleCase(str) {
        return str.replace(/\w\S*/g, function (txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        });
    }

    $formButtons.on('click', function () {
        var $this = $(this),
            titleText = $this.parent().parent().children('.pricing__item__title').text(),
            tabText = $this.parent().parent().parent().parent().parent().parent().children('.tab__holder').children('.tabs-menu').children('.current').text(),
            msgText = 'Mám záujem o ';

        titleText = $.trim(titleText);
        titleText = titleText.toLowerCase();
        titleText = toTitleCase(titleText);
        
        tabText = $.trim(tabText);

        $formTextArea.val(msgText + titleText + ' - ' + tabText);
    });

});