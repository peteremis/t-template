/* GET PARAMETERS FROM URL */
function getUrlParameter(name) {
  name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
  var regex = new RegExp("[\\?&]" + name + "=([^&#]*)");
  var results = regex.exec(location.search);
  return results === null
    ? ""
    : decodeURIComponent(results[1].replace(/\+/g, " "));
}

$(document).ready(function () {
  // GET DATA FROM URL
  product = getUrlParameter("produkt");
  service = getUrlParameter("sluzba");
  device = getUrlParameter("zariadenie");
  var productName = "";
  var deviceName = "";
  var serviceName = "";
  if (converter.product[product]) {
    productName = converter.product[product].name;
    $("#badge-product").text(productName);
  } else {
    $("#badge-product").hide();
  }
  if (converter.device[device]) {
    deviceName = converter.device[device].name;
    $("#badge-device").text(deviceName);
  } else {
    $("#badge-device").hide();
  }
  if (converter.service[service]) {
    serviceName = converter.service[service].name;
    $("#badge-service").text(serviceName);
  } else {
    $("#badge-service").hide();
  }

  var $form = $("#personalDataForm");

  // SUBMIT FORM

  $(".submitbtn").on("click", function (e) {
    e.preventDefault();
    if ($("#personalDataForm").valid()) {
      $("#formFieldId154216 input").val(productName).blur();
      $("#formFieldId154217 input").val(deviceName).blur();
      $("#formFieldId154218 input").val(serviceName).blur();

      var companySsn = $form.find("#companyIco").val();
      $("#formFieldId154220 input").val(companySsn).blur();
      var userName = $form.find("#userName").val();
      $("#formFieldId154221 input").val(userName).blur();
      var userEmail = $form.find("#useremail").val();
      $("#formFieldId154222 input").val(userEmail).blur();
      var phoneNum = $form.find("#phoneNum").val();
      $("#formFieldId154223 input").val(phoneNum).blur();
      var userNote = $form.find("#note").val();
      $("#formFieldId154219 input").val(userNote).blur();

      if ($("#ebill").is(":checked")) {
        $("#formFieldId154224 input").click();
      }

      setTimeout(function () {
        $(".tlacidlo_odoslat").eq(0).click();
      }, 500);
    }
  });
});
