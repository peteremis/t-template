$(function () {

    /* TOOLTIP */

    $('.info-activation').qtip({
        content: {
            text: 'AKO NOVÝ ZÁKAZNÍK: <br />' +
                'Ľubovoľné balíky aktivujete v 3. kroku nákupného košíka. <br />' +
                'AK UŽ MAGIO TV MÁTE: <br />' +
                'Balíky môžete ľahko spravovať priamo cez svoju Magio televíziu.'
        }
    });

    $('.info-1').qtip({
        content: {
            text: 'Pre nahrávanie je potrebný: <br />' +
                '1. Magio box s nahrávaním, ktorý v prípade záujmu zvoľte v nákupnom košíku (max. 4 kusy pre jednu adresu zriadenia). <br />' +
                '2. Mať na adrese zriadenia televízie dostupnú optickú technológiu alebo satelit.'
        }
    });
    
    $('.info-2').qtip({
        content: {
            text: 'Pre Spustenie programu od začiatku a Magio archív sú dostupné tieto stanice:Jednotka, Dvojka, Markíza, DajTo, JOJ, JOJ Plus, TV Doma, TA 3, ČT1, ČT2'
        }
    });
    
    $('.info-3').qtip({
        content: {
            text: 'Spustenie programu od začiatku a Magio archív aktivujete v nákupnom košíku.'
        }
    });
    
    $('.info-4').qtip({
        content: {
            text: 'Pre nahrávanie, zastavenie a pretáčanie programu je potrebné mať Magio box s nahrávaním. Podľa dostupných technológií u vás doma, si môžete vziať až 4 Magio boxy. Magio box si vyberiete  v nákupnom košíku.'
        }
    });
    
    $('.info-5').qtip({
        content: {
            text: 'Vezmite si k Magio televízii aj internet a/alebo Pevnú linku. Získate tak Chytrý balík služieb za výhodnejšu cenu.Pokračujte zeleným tlačidlom VYBRAŤ. V ďalšom kroku si navolíte internet ako samostatnú službu alebo v kombinácii s ďalšími službami.'
        }
    });
    
    /* STICKY NAV */

    $("#nav-sticky").sticky({
        topSpacing: 0,
        widthFromWrapper: true
    });

    $(".scroll-to").click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top - ($("#nav-sticky").outerHeight())
                }, 1000);
                return false;
            }
        }
    });

    var contentSections = $("[id*='sec-']"),
        secondaryNav = $("#nav-sticky");

    function updateSecondaryNavigation() {
        contentSections.each(function () {
            var actual = $(this),
                actualHeight = actual.height() + parseInt(actual.css('paddingTop').replace('px', '')) + parseInt(actual.css('paddingBottom').replace('px', '')),
                actualAnchor = secondaryNav.find('a[href="#' + actual.attr('id') + '"]');

            if ((actual.offset().top - secondaryNav.outerHeight() <= $(window).scrollTop()) && (actual.offset().top + actualHeight - secondaryNav.outerHeight() > $(window).scrollTop())) {
                actualAnchor.addClass('active');
            } else {
                actualAnchor.removeClass('active');
            }

        });
    }
    updateSecondaryNavigation();

    $(window).scroll(function (event) {
        updateSecondaryNavigation();
    });
});
