$(function() {
/* scroll to + sticky nav */
  $("#nav-sticky").sticky({
      topSpacing: 0,
      widthFromWrapper: true
  });

  $(".scroll-to").click(function () {
      if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
          var target = $(this.hash); //vrati #sec- po kliknuti na NAV item
          target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
          if (target.length) {
              $('html,body').animate({
                  scrollTop: target.offset().top - ($("#nav-sticky").height()) //zoscrolluj na sekciu a odrataj vysku NAV
              }, 1000);
              return false;
          }
      }
  });

  var contentSections = $("[id*='sec-']"),
      secondaryNav = $("#nav-sticky");

  function updateSecondaryNavigation() {
      contentSections.each(function () {
          var actual = $(this),
              actualHeight = actual.height() + parseInt(actual.css('paddingTop').replace('px', '')) + parseInt(actual.css('paddingBottom').replace('px', '')),
              actualAnchor = secondaryNav.find('a[href="#' + actual.attr('id') + '"]');

          if ((actual.offset().top - secondaryNav.outerHeight() <= $(window).scrollTop()) && (actual.offset().top + actualHeight - secondaryNav.outerHeight() > $(window).scrollTop())) {
              actualAnchor.addClass('active');
          } else {
              actualAnchor.removeClass('active');
          }

      });
  }
  updateSecondaryNavigation();

    $(window).scroll(function (event) {
        updateSecondaryNavigation();
    });
  /* scroll to + sticky nav END */


  /* SEC-5 TOGGLE ARROW */

  // default toggle arrow behavior
  $('.toggle-arrow').click(function(event) {
      event.preventDefault();
    $(event.target).next('p.arrow-content').toggle(200);
    $(this).find('.arrow-right').toggleClass('arrow-rotate');
  });

  // media query event handler
  if (matchMedia) {
    var mediaQueryMobil = window.matchMedia("(max-width: 500px)");
    mediaQueryMobil.addListener(WidthChange);
    WidthChange(mediaQueryMobil);
  }

  // media query change
  function WidthChange(mediaQueryMobil) {
    if (mediaQueryMobil.matches) {
      // window width is less than 500px
      $('#numberOne').hide();
      $('.arrow-content').hide();
      $('.toggle-arrow').find('.arrow-right').removeClass('arrow-rotate');
    } else {
      // window width is more than 500px
      $('#numberOne').show();
      $('.arrow-content').show();
      $('.toggle-arrow').find('.arrow-right').addClass('arrow-rotate');
    }
  }
    // Variables from url
    function getUrlVariables() {
      var vars = {};
      var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
          vars[key] = value;
      });
      return vars;
  }
    var number = getUrlVariables()["ver"]
    , field = document.querySelectorAll('.js-check-change-field')
    , fieldNone = document.querySelectorAll('.js-check-change-field-none');
  
      if (number == '1') {
        document.getElementById("ver-text").style.display = "none";
        document.getElementById("ver-box").style.display = "none";
      } 

      if (number == '2') {
    
        document.getElementById("box").checked = false;
        for (i = 0; i < field.length; i++) {
          field[i].style.display='none';     
          }
        for (i = 0; i < fieldNone.length; i++) {
          fieldNone[i].style.display='inline-block';
        }  
        }
      if (number == '3') {
        document.getElementById("ver-text").style.display = "none";
        document.getElementById("ver-box").style.display = "none";
        for (i = 0; i < field.length; i++) {
          field[i].style.display='none';     
          }
        for (i = 0; i < fieldNone.length; i++) {
          fieldNone[i].style.display='inline-block';
        }

      }    
      /* TOGGLE ARROW */
      $('.toggle-arrow, .arrow-right').click(function(event) {
        event.preventDefault();
        if ($(this).hasClass('arrow-right')) {
          return;
        }
        $(event.target).closest('.togleArrow').find('.arrow-content').toggle(200);
      
      });
      /* TOGGLE ARROW END */
    });
