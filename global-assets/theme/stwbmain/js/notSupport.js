function getInternetExplorerVersion() {
   var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");
        var rv = -1;
        var tmpOut = 1;
        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer, return version number
        {               
            if (!isNaN(parseInt(ua.substring(msie + 5, ua.indexOf(".", msie))))) {

                //For < IE11
                var ver = (parseInt(ua.substring(msie + 5, ua.indexOf(".", msie))));
                //alert(ver);
                if (ver < 9) tmpOut = 0;
            }
        }
        return tmpOut;
}
function closeNotSupport()
{
  document.cookie='notSupportIE=1';
  $('#notSupport_cont').remove();
}

$( document ).ready(function()
{
  var x = document.cookie;
  if(getInternetExplorerVersion() == 0 && x.indexOf("notSupportIE=1") < 0)
  {
    var notSupportText = '<div class="notSupport_mess">' + 
      '<div style="padding-top: 15px;width:100%;margin: 0 auto;position: relative;">' + 
        '<div style="padding-bottom:10px;max-width: 940px;" class="grid-wrapper">' +
          '<a id="logo" href="http://www.telekom.sk" title="Slovak Telekom" alt="Slovak Telekom"></a>' +
          '<div id="menu-opener"></div>' +
        '</div>' +
      '</div>' +
      '<div style="background-color: #E12F8F; color: white; overflow: auto;max-height: 530px;">' +                            
        '<div style="width:50%;margin: 0 auto;">' +
          '<div style="padding-top: 10px;">' +
            '<h1 style="font-weight: bold;text-align:center;">VÁŠ PREHLIADAČ NIE JE PODPOROVANÝ</h1>' +
            '<h1 style="text-align:center;">ZVOĽTE, ČO HĽADÁTE:</h2>' +
            '<div style="text-align: center;">' +
              '<table><tr><td width="340px" valign="top">' +
                '<div>' +
                  '<img style="width: 50px;height: 45px;" src="https://www.telekom.sk/documents/10179/450952/ikona-lupa.png/1254872b-b569-4e3c-8464-0326442fd85d" />' +
                '</div>' +
                '<div>' +
                  '<h2 style="font-weight: bold;">Hľadám konkrétnu ponuku</h2>' +
                '</div>' +
                '<div>V prípade záujmu o konkrétnu ponuku, vás budeme kontaktovať.</div>' +
                '<div class="button" id="livechat" style="margin-top: 30px; font-size: 24px;background-color: white; color: #E12F8F;">Zavolajte mi</div>' +
              '</td>' +
              '<td width="340px" valign="top">' +
                '<div>' +
                  '<img style="width: 50px;height: 45px;" src="https://www.telekom.sk/documents/10179/450952/ikona-moj-telekom.png/d1a96509-1624-41c2-a7da-ed2c90924f9a" />' +
                '</div>' +
                '<div>' +
                  '<h2 style="font-weight: bold;">Môj Telekom</h2>' +
                '</div>' +
                '<div>Pre prihlásenie do portálu kliknite <a href="https://www.telekom.sk/portal/dt?provider=htmlsk&amp;htmlsk.setSelected=htmlsk/personal&amp;htmlsk/personal.setSelected=htmlsk/personal/myTMobile&amp;htmlsk/personal/myTMobile.setSelected=htmlsk/personal/myTMobile/nul&amp;last=false?_ga=1.216720479.83453526.1436353109">sem</a>.</div>' +
              '</td></tr></table>' +
            '</div>' +
            '<div style="text-align: center; padding: 20px 0; font-size:20px;">Alebo otvorte stránku v inom prehliadači.</div>' +
            '<div style="text-align: center; padding: 20px 0; font-size:16px;">Ak chcete pokračovať v prehliadaní stránky napriek zníženej kvalite grafiky a funkčnosti, <a href="#" onClick="closeNotSupport();">kliknite sem</a>.</div>' +
          '</div>' +
         '</div>' +
        '</div>' +       
     "<div class='notSupport_over'></div>";
    
    $('body').append("<div id='notSupport_cont'>" +
        "<div>" +
          notSupportText +
        "</div>" +
      "</div>");
    livechatoo.wininv.init();
    $("#livechat").click(function(){
      livechatoo.wininv.show(livechatoo.wininv.rules[75]);return false;
      $('#WIN_box1').css('z-index','9999');
    });
  }
});
