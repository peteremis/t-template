// *** OVERENIE
$("#checkPhone").click(function () {
    var phone = $("#phoneNumber").val();

    $('div.phoneNumber')
        .addClass('valid');

    if (!checkPhoneFormat(phone)) return;

    showPopup(getPopupContent("inProgress").content);

    $.when( nba(phone, null) ).then(function( data, textStatus, jqXHR ) {
        if (data.offer.verifyStatus === 'OTP_SENT') {
            $(".input-phone").hide();
            $(".input-pin").show(200);
            $(".input-pin .input_text")
                .eq(0)
                .focus();
            $.fancybox.close();
        } else {
            showPopup(getPopupContent('error').content);
        }
    });
    
/*
    nba(phone, null).then(function (json) {
        if (json.verifyStatus === 'OTP_SENT') {
            $(".input-phone").hide();
            $(".input-pin").show(200);
            $(".input-pin .input_text")
                .eq(0)
                .focus();
            $.fancybox.close();
        } else {
            showPopup(getPopupContent('error').content);
        }
    });
*/    
});

function checkPhoneFormat(phone) {
    if (phone.length === 10 && phone.match(/09[0-9]{8}/g)) {
        $('div.phoneNumber')
            .removeClass('has-error')
            .find('.error')
            .remove();
        return true;
    } else {
        if (!$('div.phoneNumber').hasClass('has-error')) {
            var error = '<span class="error">Mobilné číslo musí byť v tvare 0903123456</span>'
            $('div.phoneNumber')
                .addClass('has-error')
                .append(error);
        }
        return false;
    }
}

$("#phoneNumber").keypress(function (e) {
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) return false;
});

$("#phoneNumber").keyup(function (e) {
    if ($('div.phoneNumber').hasClass('valid')) {
        var phone = $("#phoneNumber").val();
        checkPhoneFormat(phone);
    }
});

$(".input-pin .input_text")
    .focus(function () {
        $(".input-pin .placeholder").css("top", "-20px");

        if ($(this).val().length) {
            $(this)
                .next(".input_text")
                .focus();
        } else {
            var count = (focus = $(this).index());
            for (var i = count - 1; i >= 0; i--) {
                var comp = $(".input-pin .input_text").eq(i);
                if (!comp.val().length) focus = i;
            }

            if (count !== focus)
                $(".input-pin .input_text")
                .eq(focus)
                .focus();
        }
    })
    .focusout(function () {
        var hasVal = false;
        $(".input-pin .input_text").each(function () {
            if ($(this).val().length) hasVal = true;
        });
        if (!hasVal) $(".input-pin .placeholder").css("top", "45px");
    })
    .keypress(function (e) {
        if (e.keyCode < 48 || e.keyCode > 57 || $(this).val().length > 0)
            return false;
    })
    .keyup(function (e) {
        if (e.keyCode === 8) {
            var attr = $(this).attr("hasVal");
            if (typeof attr === "undefined") {
                $(this)
                    .prev(".input_text")
                    .removeAttr("hasVal")
                    .val("")
                    .focus();
            } else {
                $(this)
                    .val("")
                    .removeAttr("hasVal");
            }
        } else {
            $(this)
                .attr("hasVal", "")
                .next(".input_text")
                .focus();
        }
    })
    .bind('paste', function (e) {
        e.preventDefault();
        var pin = e.originalEvent.clipboardData.getData('Text');
        //window.clipboardData.getData("Text");

        if (pin.length === 4 && pin.match(/[0-9]{4}/g)) {
            $(".input-pin .input_text").each(function (key) {
                $(this).val(pin.substr(key, 1));
            });
        }
    });
// *** OVERENIE End

$(document).on("click", ".popup-close", function (e) {
    e.preventDefault();
    $.fancybox.close();
});

function showPopup(content) {
    var ratioValue = 0.5;
    var width = "50%";
    var responsive = "desktop";

    if ($(document).innerWidth() <= 767) {
        ratioValue = 1;
        width = "100%";
        responsive = "mobile";
    }

    $.fancybox({
        content: content,
        padding: 0,
        margin: 0,
        width: width,
        height: "auto",
        autoSize: false,
        topRatio: ratioValue,
        closeBtn: false,
        wrapCSS: "bd-scenario-popup " + responsive,
        parent: "#content",
        helpers: {
            overlay: {
                closeClick: false,
                css: {
                    background: "rgba(0, 0, 0, 0.7)"
                }
            }
        }
    });
}