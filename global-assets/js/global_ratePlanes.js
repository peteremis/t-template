var dataRatePlanes = [
  {
    id: 1,
    hero: false,
    category: 1,
    segment: "b2c",
    name: "T Základ 11",
    rpCode: "RP1129",
    volania: ["100", "minút"],
    volaniaM1: ["200", "minút"],
    volaniaIn: ["Nekonečné", ""],
    sms: ["0,07", "€ / SMS"],
    data: ["500", "MB"],
    dataM1: ["1", "GB"],
    cena: "11,00",
    darcek: [],
    tooltips: {},
  },
  {
    id: 2,
    hero: true,
    category: 1,
    segment: "b2c",
    name: "T Základ 14",
    rpCode: "RP1130",
    volania: ["150", "minút"],
    volaniaM1: ["300", "minút"],
    volaniaIn: ["Nekonečné", ""],
    sms: ["150", "SMS"],
    smsM1: ["300", "SMS"],
    data: ["500", "MB"],
    dataM1: ["1", "GB"],
    cena: "14,00",
    darcek: [],
    tooltips: {},
  },
  {
    id: 3,
    hero: false,
    category: 1,
    segment: "b2c",
    name: "T Základ 17",
    rpCode: "RP1131",
    volania: ["200", "minút"],
    volaniaM1: ["400", "minút"],
    volaniaIn: ["Nekonečné", ""],
    sms: ["200", "SMS"],
    smsM1: ["400", "SMS"],
    data: ["500", "MB"],
    dataM1: ["1", "GB"],
    cena: "17,00",
    darcek: [],
    tooltips: {},
  },
  {
    id: 4,
    hero: false,
    category: 2,
    segment: "b2c",
    name: "T Dáta 17",
    rpCode: "RP1132",
    volania: ["100", "minút"],
    volaniaM1: ["200", "minút"],
    volaniaIn: ["Nekonečné", ""],
    sms: ["0,07", "€ / SMS"],
    data: ["3", "GB"],
    dataM1: ["5", "GB"],
    cena: "17,00",
    darcek: [],
    tooltips: {},
  },
  {
    id: 5,
    hero: false,
    category: 3,
    segment: "b2c",
    name: "T Ideál 20",
    rpCode: "RP1133",
    volania: ["Nekonečné", ""],
    volaniaIn: ["Nekonečné", ""],
    sms: ["Nekonečné", ""],
    data: ["500", "MB"],
    dataM1: ["1", "GB"],
    cena: "20,00",
    darcek: [],
    tooltips: {},
  },
  {
    id: 6,
    hero: true,
    category: 3,
    segment: "b2c",
    name: "T Ideál 21",
    rpCode: "RP1134",
    volania: ["Nekonečné", ""],
    volaniaIn: ["Nekonečné", ""],
    sms: ["Nekonečné", ""],
    data: ["1", "GB"],
    dataM1: ["3", "GB"],
    cena: "21,00",
    darcek: [],
    tooltips: {},
  },
  {
    id: 7,
    hero: false,
    category: 2,
    segment: "b2c",
    name: "T Dáta 21",
    rpCode: "RP1135",
    volania: ["100", "minút"],
    volaniaM1: ["200", "minút"],
    volaniaIn: ["Nekonečné", ""],
    sms: ["0,07", "€ / SMS"],
    data: ["6", "GB"],
    dataM1: ["9", "GB"],
    cena: "21,00",
    darcek: [],
    tooltips: {},
  },
  {
    id: 8,
    hero: false,
    category: 3,
    segment: "b2c",
    name: "T Ideál 23",
    rpCode: "RP1136",
    volania: ["Nekonečné", ""],
    volaniaIn: ["Nekonečné", ""],
    sms: ["Nekonečné", ""],
    data: ["2", "GB"],
    dataM1: ["4", "GB"],
    cena: "23,00",
    darcek: [],
    tooltips: {},
  },
  {
    id: 9,
    hero: true,
    category: 3,
    segment: "b2c",
    name: "T Ideál 25",
    rpCode: "RP1137",
    volania: ["Nekonečné", ""],
    volaniaIn: ["Nekonečné", ""],
    sms: ["Nekonečné", ""],
    data: ["3", "GB"],
    dataM1: ["5", "GB"],
    cena: "25,00",
    darcek: [
      {
        code: "m-go",
        name: "Bezplatný video a TV obsah",
      },
    ],
    tooltips: {
      nek_prip: true,
    },
  },
  {
    id: 10,
    hero: true,
    category: 2,
    segment: "b2c",
    name: "T Dáta 25",
    rpCode: "RP1138",
    volania: ["100", "minút"],
    volaniaM1: ["200", "minút"],
    volaniaIn: ["Nekonečné", ""],
    sms: ["0,07", "€ / SMS"],
    data: ["10", "GB"],
    dataM1: ["15", "GB"],
    cena: "25,00",
    darcek: [
      {
        code: "m-go",
        name: "Bezplatný video a TV obsah",
      },
    ],
    tooltips: {
      nek_prip: true,
    },
  },
  {
    id: 11,
    hero: false,
    category: 3,
    segment: "b2c",
    name: "T Ideál 27",
    rpCode: "RP1139",
    volania: ["Nekonečné", ""],
    volaniaIn: ["Nekonečné", ""],
    sms: ["Nekonečné", ""],
    data: ["5", "GB"],
    dataM1: ["8", "GB"],
    cena: "27,00",
    darcek: [
      {
        code: "m-go",
        name: "Bezplatný video a TV obsah",
      },
    ],
    tooltips: {
      nek_prip: true,
    },
  },
  {
    id: 12,
    hero: true,
    category: 3,
    segment: "b2c",
    name: "T Ideál 32",
    rpCode: "RP1140",
    volania: ["Nekonečné", ""],
    volaniaIn: ["Nekonečné", ""],
    sms: ["Nekonečné", ""],
    data: ["12", "GB"],
    dataM1: ["18", "GB"],
    cena: "32,00",
    darcek: [
      {
        code: "m-go",
        name: "Bezplatný video a TV obsah",
      },
    ],
    tooltips: {
      nek_prip: true,
    },
  },
  {
    id: 13,
    hero: false,
    category: 3,
    segment: "b2c",
    name: "T Ideál 37",
    rpCode: "RP1141",
    volania: ["Nekonečné", ""],
    volaniaIn: ["Nekonečné", ""],
    sms: ["Nekonečné", ""],
    data: ["17", "GB"],
    dataM1: ["25", "GB"],
    cena: "37,00",
    darcek: [
      {
        code: "m-go",
        name: "Bezplatný video a TV obsah",
      },
    ],
    tooltips: {
      nek_prip: true,
    },
  },
  {
    id: 14,
    hero: false,
    category: 2,
    segment: "b2c",
    name: "T Dáta HD",
    rpCode: "RP1142",
    volania: ["100", "minút"],
    volaniaM1: ["200", "minút"],
    volaniaIn: ["Nekonečné", ""],
    sms: ["0,07", "€ / SMS"],
    data: ["25", "GB"],
    dataM1: ["40", "GB"],
    cena: "42,00",
    darcek: [
      {
        code: "m-go",
        name: "Bezplatný video a TV obsah",
      },
    ],
    tooltips: {
      nekonecno:
        "<p class='title'>S paušálom získate 25 GB dát v plnej rýchlosti</p>" +
        "<p>Následne surfujete a streamujete videá aj v HD kvalite rýchlosťou 10 Mbps.</p>" +
        "<p>&nbsp;</p>" +
        "<p class='title'>S Magenta 1 získate 40 GB dát v plnej rýchlosti</p>" +
        "<p>K 25 GB v paušále získate 15 GB navyše, ak so svojím paušálom založíte alebo sa pridáte do bezplatnej skupiny Magenta 1, v ktorej je aspoň jedna zo služieb Magio internet, TV alebo Pevná linka M, L alebo XL.</p>",
      nekonecno2:
        "<p class='title'>S paušálom získate 25 GB dát v plnej rýchlosti</p>" +
        "<p>Následne surfujete rýchlosťou 10 Mbps, ktorá vám umožní sledovať online videá aj v HD kvalite, pohodlne chatovať, prijímať a odosielať e-maily s väčšími prílohami a prezerať svoje obľúbené web stránky.</p>",
      m1:
        "<p class='title'>S Magenta 1 získate 40 GB dát v plnej rýchlosti</p>" +
        "<p>K 25 GB v paušále získate 15 GB navyše, ak so svojím paušálom založíte alebo sa pridáte do bezplatnej skupiny Magenta 1, v ktorej je aspoň jedna zo služieb Magio internet, TV alebo Pevná linka M, L alebo XL.</p>",
    },
  },
  {
    id: 15,
    hero: false,
    category: 4,
    segment: "b2c",
    name: "T Nekonečno SD",
    rpCode: "RP1143",
    volania: ["Nekonečné", ""],
    volaniaIn: ["Nekonečné", ""],
    sms: ["Nekonečné", ""],
    data: ["20", "GB"],
    dataM1: ["30", "GB"],
    cena: "42,00",
    darcek: [
      {
        code: "m-go",
        name: "Bezplatný video a TV obsah",
      },
    ],
    tooltips: {
      nekonecno:
        "<p class='title'>S paušálom získate 20 GB dát v plnej rýchlosti</p>" +
        "<p>Následne surfujete a streamujete videá v SD kvalite rýchlosťou 3 Mbps.</p>" +
        "<p>&nbsp;</p>" +
        "<p class='title'>S Magenta 1 získate 30 GB dát v plnej rýchlosti</p>" +
        "<p>K 20 GB v paušále získate 10 GB navyše, ak so svojím paušálom založíte alebo sa pridáte do bezplatnej skupiny Magenta 1, v ktorej je aspoň jedna zo služieb Magio internet, TV alebo Pevná linka M, L alebo XL.</p>",
      nekonecno2:
        "<p class='title'>S paušálom získate 20 GB dát v plnej rýchlosti</p>" +
        "<p>Následne surfujete rýchlosťou 3 Mbps, ktorá vám umožní sledovať online videá v SD kvalite, pohodlne chatovať, prijímať a odosielať e-maily so stredne veľkými prílohami a prezerať svoje obľúbené web stránky.</p>",
      m1:
        "<p class='title'>S Magenta 1 získate 30 GB dát v plnej rýchlosti</p>" +
        "<p>K 20 GB v paušále získate 10 GB navyše, ak so svojím paušálom založíte alebo sa pridáte do bezplatnej skupiny Magenta 1, v ktorej je aspoň jedna zo služieb Magio internet, TV alebo Pevná linka M, L alebo XL.</p>",
    },
  },
  {
    id: 16,
    hero: true,
    category: 4,
    segment: "b2c",
    name: "T Nekonečno HD",
    rpCode: "RP1144",
    volania: ["Nekonečné", ""],
    volaniaIn: ["Nekonečné", ""],
    sms: ["Nekonečné", ""],
    data: ["50", "GB"],
    dataM1: ["65", "GB"],
    cena: "48,00",
    darcek: [
      {
        code: "m-go",
        name: "Bezplatný video a TV obsah",
      },
    ],
    tooltips: {
      nekonecno:
        "<p class='title'>S paušálom získate 50 GB dát v plnej rýchlosti</p>" +
        "<p>Následne surfujete a streamujete videá aj v HD kvalite rýchlosťou 10 Mbps.</p>" +
        "<p>&nbsp;</p>" +
        "<p class='title'>S Magenta 1 získate 65 GB dát v plnej rýchlosti</p>" +
        "<p>K 50 GB v paušále získate 15 GB navyše, ak so svojím paušálom založíte alebo sa pridáte do bezplatnej skupiny Magenta 1, v ktorej je aspoň jedna zo služieb Magio internet, TV alebo Pevná linka M, L alebo XL.</p>",
      nekonecno2:
        "<p class='title'>S paušálom získate 50 GB dát v plnej rýchlosti</p>" +
        "<p>Následne surfujete rýchlosťou 10 Mbps, ktorá vám umožní sledovať online videá aj v HD kvalite, pohodlne chatovať, prijímať a odosielať e-maily s väčšími prílohami a prezerať svoje obľúbené web stránky.</p>",
      m1:
        "<p class='title'>S Magenta 1 získate 65 GB dát v plnej rýchlosti</p>" +
        "<p>K 50 GB v paušále získate 15 GB navyše, ak so svojím paušálom založíte alebo sa pridáte do bezplatnej skupiny Magenta 1, v ktorej je aspoň jedna zo služieb Magio internet, TV alebo Pevná linka M, L alebo XL.</p>",
    },
  },
  {
    id: 17,
    hero: false,
    category: 4,
    segment: "b2c",
    name: "T Nekonečno MAX",
    rpCode: "RP1145",
    volania: ["Nekonečné", ""],
    volaniaIn: ["Nekonečné", ""],
    sms: ["Nekonečné", ""],
    data: ["Nekonečné", ""],
    dataM1: ["20 GB", "pre&nbsp;darovanie"],
    cena: "70,00",
    darcek: [
      {
        code: "m-go",
        name: "Bezplatný video a TV obsah",
      },
    ],
    tooltips: {
      nekonecno:
        "<p>S paušálom získate úplne nekonečné dáta s&nbsp;maximálnou možnou rýchlosťou 4G siete</p>",
      m1:
        "<p>Získate 20 GB na darovanie, ak so svojím paušálom založíte alebo sa pridáte do bezplatnej skupiny Magenta 1, v ktorej je aspoň jedna zo služieb Magio internet, TV alebo Pevná linka M, L alebo XL.</p>",
    },
  },
  {
    id: 18,
    hero: false,
    category: 1,
    segment: "b2b",
    name: "Biznis Mini&nbsp;12",
    rpCode: "RP1146",
    volania: ["100", "minút"],
    volaniaM1: ["200", "minút"],
    volaniaIn: ["Nekonečné", ""],
    sms: ["0,07", "€ / SMS"],
    sms_bez: ["0,06", "€ / SMS"],
    data: ["500", "MB"],
    dataM1: ["1", "GB"],
    cena: "12,00",
    cena_bez: "10,00",
    darcek: [
      {
        code: "t-master",
        name: "Prémiová starostlivosť s T-Mastrom",
      },
    ],
    tooltips: {},
  },
  {
    id: 19,
    hero: false,
    category: 1,
    segment: "b2b",
    name: "Biznis Mini&nbsp;15",
    rpCode: "RP1147",
    volania: ["150", "minút"],
    volaniaM1: ["300", "minút"],
    volaniaIn: ["Nekonečné", ""],
    sms: ["150", "SMS"],
    smsM1: ["300", "SMS"],
    data: ["500", "MB"],
    dataM1: ["1", "GB"],
    cena: "15,00",
    cena_bez: "12,50",
    darcek: [
      {
        code: "t-master",
        name: "Prémiová starostlivosť s T-Mastrom",
      },
    ],
    tooltips: {},
  },
  {
    id: 20,
    hero: false,
    category: 1,
    segment: "b2b",
    name: "Biznis Mini&nbsp;18",
    rpCode: "RP1148",
    volania: ["200", "minút"],
    volaniaM1: ["400", "minút"],
    volaniaIn: ["Nekonečné", ""],
    sms: ["200", "SMS"],
    smsM1: ["400", "SMS"],
    data: ["500", "MB"],
    dataM1: ["1", "GB"],
    cena: "18,00",
    cena_bez: "15,00",
    darcek: [
      {
        code: "t-master",
        name: "Prémiová starostlivosť s T-Mastrom",
      },
    ],
    tooltips: {},
  },
  {
    id: 21,
    hero: true,
    category: 2,
    segment: "b2b",
    name: "Biznis Premium&nbsp;23",
    rpCode: "RP1149",
    volania: ["Nekonečné", ""],
    volaniaIn: ["Nekonečné", ""],
    sms: ["Nekonečné", ""],
    data: ["2", "GB"],
    dataM1: ["4", "GB"],
    cena: "23,00",
    cena_bez: "19,17",
    darcek: [
      {
        code: "t-master",
        name: "Prémiová starostlivosť s T-Mastrom",
      },
    ],
    tooltips: {},
  },
  {
    id: 22,
    hero: false,
    category: 2,
    segment: "b2b",
    name: "Biznis Premium&nbsp;26",
    rpCode: "RP1150",
    volania: ["Nekonečné", ""],
    volaniaIn: ["Nekonečné", ""],
    sms: ["Nekonečné", ""],
    data: ["4", "GB"],
    dataM1: ["6", "GB"],
    cena: "26,00",
    cena_bez: "21,67",
    darcek: [
      {
        code: "t-master",
        name: "Prémiová starostlivosť s T-Mastrom",
      },
    ],
    tooltips: {
      nek_prip: true,
    },
  },
  {
    id: 23,
    hero: true,
    category: 2,
    segment: "b2b",
    name: "Biznis Premium&nbsp;28",
    rpCode: "RP1151",
    volania: ["Nekonečné", ""],
    volaniaIn: ["Nekonečné", ""],
    sms: ["Nekonečné", ""],
    data: ["6", "GB"],
    dataM1: ["9", "GB"],
    cena: "28,00",
    cena_bez: "23,33",
    darcek: [
      {
        code: "t-master",
        name: "Prémiová starostlivosť s T-Mastrom",
      },
    ],
    tooltips: {
      nek_prip: true,
    },
  },
  {
    id: 24,
    hero: false,
    category: 2,
    segment: "b2b",
    name: "Biznis Premium&nbsp;32",
    rpCode: "RP1152",
    volania: ["Nekonečné", ""],
    volaniaIn: ["Nekonečné", ""],
    sms: ["Nekonečné", ""],
    data: ["12", "GB"],
    dataM1: ["18", "GB"],
    cena: "32,00",
    cena_bez: "26,67",
    darcek: [
      {
        code: "t-master",
        name: "Prémiová starostlivosť s T-Mastrom",
      },
    ],
    tooltips: {
      nek_prip: true,
    },
  },
  {
    id: 25,
    hero: true,
    category: 2,
    segment: "b2b",
    name: "Biznis Premium&nbsp;35",
    rpCode: "RP1153",
    volania: ["Nekonečné", ""],
    volaniaIn: ["Nekonečné", ""],
    sms: ["Nekonečné", ""],
    data: ["15", "GB"],
    dataM1: ["22", "GB"],
    cena: "35,00",
    cena_bez: "29,17",
    darcek: [
      {
        code: "t-master",
        name: "Prémiová starostlivosť s T-Mastrom",
      },
    ],
    tooltips: {
      nek_prip: true,
    },
  },
  {
    id: 26,
    hero: false,
    category: 2,
    segment: "b2b",
    name: "Biznis Premium&nbsp;38",
    rpCode: "RP1154",
    volania: ["Nekonečné", ""],
    volaniaIn: ["Nekonečné", ""],
    sms: ["Nekonečné", ""],
    data: ["18", "GB"],
    dataM1: ["27", "GB"],
    cena: "38,00",
    cena_bez: "31,67",
    darcek: [
      {
        code: "t-master",
        name: "Prémiová starostlivosť s T-Mastrom",
      },
    ],
    tooltips: {
      nek_prip: true,
    },
  },
  {
    id: 27,
    hero: false,
    category: 3,
    segment: "b2b",
    name: "Biznis Nekonečno&nbsp;SD",
    rpCode: "RP1155",
    volania: ["Nekonečné", ""],
    volaniaIn: ["Nekonečné", ""],
    sms: ["Nekonečné", ""],
    data: ["25", "GB"],
    dataM1: ["35", "GB"],
    cena: "43,00",
    cena_bez: "35,83",
    darcek: [
      {
        code: "t-master",
        name: "Prémiová starostlivosť s T-Mastrom",
      },
    ],
    tooltips: {
      nekonecno:
        "<p class='title'>S paušálom získate 25 GB dát v plnej rýchlosti</p>" +
        "<p>Následne surfujete a streamujete videá v SD kvalite rýchlosťou 3 Mbps." +
        "<p>&nbsp;</p>" +
        "<p class='title'>S Magenta 1 získate 35 GB dát v plnej rýchlosti</p>" +
        "<p>K 25 GB v paušále získate 10 GB navyše, ak so svojím paušálom založíte alebo sa pridáte do bezplatnej skupiny Magenta 1, v ktorej je aspoň jedna zo služieb Magio internet, TV alebo Pevná linka M, L alebo XL.</p>",
      nekonecno2:
        "<p class='title'>S paušálom získate 25 GB dát v plnej rýchlosti</p>" +
        "<p>Následne surfujete rýchlosťou 3 Mbps, ktorá vám umožní sledovať online videá v SD kvalite, pohodlne chatovať, prijímať a odosielať e-maily so stredne veľkými prílohami a prezerať svoje obľúbené web stránky.</p>",
      m1:
        "<p class='title'>S Magenta 1 získate 35 GB dát v plnej rýchlosti</p>" +
        "<p>K 25 GB v paušále získate 10 GB navyše, ak so svojím paušálom založíte alebo sa pridáte do bezplatnej skupiny Magenta 1, v ktorej je aspoň jedna zo služieb Magio internet, TV alebo Pevná linka M, L alebo XL.</p>",
    },
  },
  {
    id: 28,
    hero: true,
    category: 3,
    segment: "b2b",
    name: "Biznis Nekonečno&nbsp;HD",
    rpCode: "RP1157",
    volania: ["Nekonečné", ""],
    volaniaIn: ["Nekonečné", ""],
    sms: ["Nekonečné", ""],
    data: ["60", "GB"],
    dataM1: ["75", "GB"],
    cena: "50,00",
    cena_bez: "41,67",
    darcek: [
      {
        code: "t-master",
        name: "Prémiová starostlivosť s T-Mastrom",
      },
    ],
    tooltips: {
      nekonecno:
        "<p class='title'>S paušálom získate 60 GB dát v plnej rýchlosti</p>" +
        "<p>Následne surfujete a streamujete videá aj v HD kvalite rýchlosťou 10 Mbps." +
        "<p>&nbsp;</p>" +
        "<p class='title'>S Magenta 1 získate 75 GB dát v plnej rýchlosti</p>" +
        "<p>K 60 GB v paušále získate 15 GB navyše, ak so svojím paušálom založíte alebo sa pridáte do bezplatnej skupiny Magenta 1, v ktorej je aspoň jedna zo služieb Magio internet, TV alebo Pevná linka M, L alebo XL.</p>",
      nekonecno2:
        "<p class='title'>S paušálom získate 60 GB dát v plnej rýchlosti</p>" +
        "<p>Následne surfujete rýchlosťou 10 Mbps, ktorá vám umožní sledovať online videá aj v HD kvalite, pohodlne chatovať, prijímať a odosielať e-maily s väčšími prílohami a prezerať svoje obľúbené web stránky.</p>",
      m1:
        "<p class='title'>S Magenta 1 získate 75 GB dát v plnej rýchlosti</p>" +
        "<p>K 60 GB v paušále získate 15 GB navyše, ak so svojím paušálom založíte alebo sa pridáte do bezplatnej skupiny Magenta 1, v ktorej je aspoň jedna zo služieb Magio internet, TV alebo Pevná linka M, L alebo XL.</p>",
    },
  },
  {
    id: 29,
    hero: false,
    category: 3,
    segment: "b2b",
    name: "Biznis Nekonečno&nbsp;MAX",
    rpCode: "RP1158",
    volania: ["Nekonečné", ""],
    volaniaIn: ["Nekonečné", ""],
    sms: ["Nekonečné", ""],
    data: ["Nekonečné", ""],
    dataM1: ["20 GB", "pre&nbsp;darovanie"],
    cena: "70,00",
    cena_bez: "58,33",
    darcek: [
      {
        code: "t-master",
        name: "Prémiová starostlivosť s T-Mastrom",
      },
    ],
    tooltips: {
      nekonecno:
        "<p>S paušálom získate úplne nekonečné dáta s maximálnou možnou rýchlosťou 4G siete</p>",
      m1:
        "<p>Získate 20 GB na darovanie, ak so svojím paušálom založíte alebo sa pridáte do bezplatnej skupiny Magenta 1, v ktorej je aspoň jedna zo služieb Magio internet, TV alebo Pevná linka M, L alebo XL.</p>",
    },
  },
];
